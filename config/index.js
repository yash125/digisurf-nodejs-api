'use strict'
const processType = process.env.PROCESS_TYPE
let config
config = require(`./${processType}`)

module.exports = config
