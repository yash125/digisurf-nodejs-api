'use strict'

const common = require('./components/common');
const logger = require('./components/logger');
const server = require('./components/server');
const mongodb = require('./components/mongodb');
const twilio = require('./components/twilio');
const mailgun = require('./components/mailgun');
const fcm = require('./components/fcm');
const mqtt = require('./components/mqtt');
const redis = require('./components/redis');
const rebitMQ = require('./components/rebitMQ');
const localization = require('./components/localization');
const cloudinary = require('./components/cloudinary');
const AWS = require('./components/AWS');

module.exports = Object.assign({}, common, logger, server, mongodb, twilio, mailgun, rebitMQ,
    fcm, mqtt, localization, redis, cloudinary, AWS);