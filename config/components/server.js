'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
  SERVER_PORT: joi.number().required(),
  AUTH_SERVER: joi.string().required(),
  AUTH_MULTI_LOGIN: joi.boolean().required(),
  AUTH_IMMEDIATE_REVOKE: joi.boolean().required(),
  AUTH_ACCESS_EXPIRY_TIME: joi.any().required(),
  AUTH_REFRESH_EXPIRY_TIME: joi.any().required()
}).unknown()
  .required()

const { error, value: envVars } = joi.validate(process.env, envVarsSchema)
if (error) {
  throw new Error(`Config validation error: ${error.message}`)
}

const config = {
  server: {
    port: envVars.SERVER_PORT,
    API_URL: process.env.API_URL
  },
  auth: {
    server: envVars.AUTH_SERVER,
    multiLogin: envVars.AUTH_MULTI_LOGIN,
    immediateRevoke: envVars.AUTH_IMMEDIATE_REVOKE,
    accessExpiry: envVars.AUTH_ACCESS_EXPIRY_TIME,
    refreshExpiry: envVars.AUTH_REFRESH_EXPIRY_TIME
  }
}

module.exports = config