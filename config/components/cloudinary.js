'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
    CLOUDINARY_CLOUD_NAME: joi.string().required(),
    CLOUDINARY_API_KEY: joi.string().required(),
    CLOUDINARY_API_SECRET: joi.string().required()
}).unknown()

const { error, value: envVars } = joi.validate(process.env, envVarsSchema)
if (error) {
    throw new Error(`Config validation error: ${error.message}`)
}

const config = {
    cloudinary: {
        CLOUD_NAME: envVars.CLOUDINARY_CLOUD_NAME,
        API_KEY: envVars.CLOUDINARY_API_KEY,
        API_SECRET: envVars.CLOUDINARY_API_SECRET
    }
}

module.exports = config