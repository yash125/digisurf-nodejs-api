'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
 
  IdentityPoolId: joi.string().allow(),
  AccessKeyId: joi.string().allow(),
  SecretAccessKey: joi.string().allow(),
  CognitoProvider: joi.string().allow(),
  BucketRegion: joi.string().allow(),
  TokenDuration: joi.string().allow(),
  Bucket: joi.string().allow(),
 


}).unknown()
  .required()

const {
  error,
  value: envVars
} = joi.validate(process.env, envVarsSchema)
if (error) {
  throw new Error(`Config validation error: ${error.message}`)
}

const config = {

  IdentityPoolId: envVars.IdentityPoolId,
  AccessKeyId: envVars.AccessKeyId,
  SecretAccessKey: envVars.SecretAccessKey,
  CognitoProvider: envVars.CognitoProvider,
  BucketRegion: envVars.BucketRegion,
  TokenDuration: envVars.TokenDuration,
  Bucket: envVars.Bucket, 
}

module.exports = config