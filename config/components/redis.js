
'use strict'

const joi = require('joi')

const envVarsSchema = joi.object({
    REDIS_URI: joi.string().required(),
    REDIS_IP: joi.string().required(),
    REDIS_PORT: joi.number().required(),
    REDIS_PASS: joi.string().required()
}).unknown()
    .required()

const { error, value: envVars } = joi.validate(process.env, envVarsSchema)
if (error) {
    throw new Error(`Config validation error: ${error.message}`)
}

const config = {
    redis: {
        REDIS_URI: envVars.REDIS_URI,
        REDIS_IP: envVars.REDIS_IP,
        REDIS_PASS: envVars.REDIS_PASS,
        REDIS_PORT: envVars.REDIS_PORT
    }
}

module.exports = config