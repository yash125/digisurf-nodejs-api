'use strict'

var async = require("async");
const logger = require('winston');
const { ObjectID } = require('mongodb');
const mongodb = require('../../models/mongodb');
const chatList = require('../../models/chatList');
const rabbitMq = require('../../library/rabbitMq');
const postCollection = require('../../models/posts');
const messagesCollection = require('../../models/messages');
const userListCollection = require('../../models/userList');
const fcm = require('../../library/fcm');
// var sendPushUtility = require("../../library/iosPush/sendPush.js");
var messageTypes = ["text", "image", "video", "location", "contact", "audio", "sticker", "doodle", "giphy", "Document", "postInMessage",
    "...", "...", "...", "...", "Payment", "...", "call"];


rabbitMq.connect(() => {
    mongodb.connect(() => {
        prepareConsumer(rabbitMq.getChannel(), rabbitMq.mqtt_insert_Message, rabbitMq.get());
    });//create a connection to mongodb
});

/**
 * Preparing Consumer for Consuming Booking from locationUpdate Booking Queue
 * @param {*} channel location Booking Channel
 * @param {*} queue  location Booking Queue
 * @param {*} amqpConn RabbitMQ connection
 */
function prepareConsumer(channel, queue, amqpConn) {
    channel.assertQueue(queue.name, queue.options, function (err) {
        if (err) {
            throw err;
            // process.exit();
        } else {
            channel.consume(queue.name, function (msg) {

                var MessageData = JSON.parse(msg.content.toString());

                try {
                    // logger.silly("MessageData : ", MessageData);
                    switch (MessageData.topic) {
                        case String(MessageData.topic.match(/^GroupChatAcknowledgement.*/)):
                            {
                                GroupChatAcknowledgement(MessageData);
                                break;
                            }
                        case String(MessageData.topic.match(/^Acknowledgement.*/)):
                            {
                                Acknowledgement(MessageData);
                                break;
                            }
                        case String(MessageData.topic.match(/^Message.*/)):
                            {
                                // message = JSON.parse(MessageData);
                                insertMessage(MessageData);
                                break;
                            }
                        case String(MessageData.topic.match(/^GroupChat\/.*/)):
                            {
                                // message = JSON.parse(MessageData);
                                createGroupMessage(MessageData);
                                break;
                            }
                    }
                } catch (error) {
                    logger.error("mqtt-rabbitmq-worker ", error)
                }

            }, { noAck: true }, function () {
                if (queue.worker.alwaysRun) {
                    // keep worker running
                } else {
                    //To check if need to exit worker
                    rabbitMq.exitWokerHandler(channel, queue, amqpConn);
                }
            });
        }
    });
}

function insertMessage(message) {

    var userId, targetUserId, payload, type, messageId, secretId, thumbnail, dTime,
        userImage, toDocId, name, dataSize, extension, fileName, mimeType,
        previousReceiverIdentifier, previousFrom, previousPayload, previousType, previousId, replyType, previousFileType;

    userId = message.from;
    targetUserId = message.to;
    payload = message.payload;
    type = message.type;
    messageId = message.id;
    secretId = (message.secretId) ? message.secretId : "";
    dTime = (message.dTime || message.dTime == 0) ? message.dTime : -1;
    thumbnail = message.thumbnail;
    mimeType = message.mimeType;
    extension = message.extension;
    fileName = message.fileName;

    previousReceiverIdentifier = message.previousReceiverIdentifier;
    previousFrom = message.previousFrom;
    previousPayload = message.previousPayload;
    previousType = message.previousType;
    previousId = message.previousId;
    replyType = message.replyType;
    previousFileType = message.previousFileType;

    userImage = message.userImage;
    toDocId = message.toDocId;
    name = message.name;



    dataSize = message.dataSize
    let targetId = targetUserId;
    let msg = "new message from " + name;
    message["alert"] = msg;
    message["msg"] = (type == 0 || type == "0")
        ? new Buffer.from(message.payload, 'base64').toString('ascii')
        : messageTypes[Number(type)];

    if (type == "0" && message["payload"] == "") {
        let dtime = (message.dTime == -1) ? "off" : message.dTime;
        if (dtime == "off") {
            message["msg"] = "Destruct timer is set off";
        } else {
            message["msg"] = "Destruct timer is set to " + dtime + " seconds";
        }
    }
    message["thumbnail"] = message["payload"];
    // sendPushUtility.iosPushIfRequiredOnMessage({ targetId: targetId, message: message, userId: userId, type: type });
    userListCollection.SelectOne({ _id: ObjectID(targetUserId) }, (err, result) => {
        var deviceType = (result && result.deviceInfo && result.deviceInfo.deviceType) ? result.deviceInfo.deviceType : "2";
        fcm.notifyFcmTpic({
            fcmTopic: targetUserId,
            // action: 1,
            pushType: 2,
            title: process.env.APP_NAME,
            msg: message["msg"],
            data: { "type": "ChatMSG" },
            deviceType: deviceType
        }, (err, result) => {
            if (err) console.log("error : KOAOEORP : ", err);
        })
    });



    async.waterfall([
        function (callback) {

            var userExists = "members." + userId;
            var targetExists = "members." + targetUserId;
            var condition = {
                [userExists]: { $exists: true },
                [targetExists]: { $exists: true },
                secretId: secretId,
                chatType: "NormalChat"
            };

            chatList.Select(condition, (err, result) => {
                if (err) {
                    callback(err, null);
                } else {
                    (result[0] && result[0]._id) ? callback(null, result[0]._id) : callback(null, 0);
                }
            });
        },
        function (chatId, callback) {

            var messageIdDB = (message._id) ? ObjectID(message._id.toString()) : ObjectID();

            if (chatId && type != "11" && type != "12") {

                var data = { ["members." + userId + ".inactive"]: "" };
                var condition = { _id: chatId.toString() };

                chatList.UpdateByIdWithUnSet(condition, data, (getChatErr) => {
                    if (getChatErr) logger.error('ASASAASA : ', getChatErr)

                    var data = { "message": messageIdDB };
                    var condition = { _id: chatId.toString() };

                    chatList.UpdateByIdWithPush(condition, data, (getChatErr) => {
                        if (getChatErr) logger.error('ASASAZZZ : ', getChatErr)
                    })
                });

                chatList.UpdateByIdWithUnSet({ _id: chatId.toString() },
                    { ["members." + targetUserId + ".inactive"]: "" },
                    (getChatErr) => {
                        if (getChatErr) logger.error('ASASAZZZASA : ', getChatErr)
                    });

            } else {
                chatId = ObjectID();
                var chatDB = {
                    "_id": chatId,
                    "message": [messageIdDB],
                    "members": {
                        [userId]: {
                            "memberId": ObjectID(userId),
                            "status": "NormalMember"
                        },
                        [targetUserId]: {
                            "memberId": ObjectID(targetUserId),
                            "status": "NormalMember"
                        }
                    },
                    "senderId": ObjectID(userId),
                    "targetId": ObjectID(targetUserId),
                    "initiatedBy": ObjectID(userId),
                    "createdAt": new Date().getTime(),
                    "chatType": "NormalChat",
                    "secretId": secretId
                }
                var userExists = "members." + userId;
                var targetExists = "members." + targetUserId;
                condition = {
                    [userExists]: { $exists: true },
                    [targetExists]: { $exists: true },
                    secretId: secretId,
                    chatType: "NormalChat"
                };

                chatList.Insert(chatDB, (err) => {
                    if (err) logger.error("error 202 WKD9SI-WED : ", err);
                });
            }
            var messageDB = {
                "_id": messageIdDB,
                "messageId": messageId,
                "secretId": secretId,
                "dTime": dTime,
                "senderId": ObjectID(userId),
                "receiverId": ObjectID(targetUserId),
                "payload": payload,
                "messageType": type,
                "timestamp": new Date().getTime(),
                "expDtime": 0,
                "chatId": chatId,
                "userImage": userImage,
                "toDocId": toDocId,
                "name": name,
                "dataSize": dataSize,
                "thumbnail": thumbnail,
                "mimeType": mimeType,
                "extension": extension,
                "fileName": fileName,
                "postId": ObjectID(message.postId),
                "postType": message.postType,
                "postTitle": message.postTitle,
                "members": {
                    [userId]: {
                        "memberId": ObjectID(userId),
                        // "status": ""
                        // "deleteAt": 0
                    },
                    [targetUserId]: {
                        "memberId": ObjectID(targetUserId),
                        "status": 1,
                        // "readAt": 0,
                        // "deliveredAt": new Date().getTime()
                        // "deleteAt": 0
                    }
                }

            };

            if (type == "16" || type == "17") messageDB["callType"] = message.typeOfCall || "0";
            if (type == "16" || type == "17") messageDB["callId"] = message.callId;
            if (type == "17") messageDB["duration"] = message.duration;
            if (message && message.type == "15") messageDB["amount"] = message.amount
            if (type == "15") messageDB["amount"] = message.amount;
            if (type == "15") messageDB["toTxnId"] = message.toTxnId;
            if (type == "15") messageDB["fromTxnId"] = message.fromTxnId;
            if (type == "15") messageDB["fromName"] = message.fromName;
            if (type == "15") messageDB["toName"] = message.toName;
            if (type == "15") messageDB["currencySymbol"] = message.currencySymbol;

            if (type == '10') {
                messageDB.previousReceiverIdentifier = previousReceiverIdentifier;
                messageDB.previousFrom = previousFrom;
                messageDB.previousPayload = previousPayload;
                messageDB.previousType = previousType;
                messageDB.previousId = previousId;
                messageDB.replyType = replyType;
                if (previousType == '9') {
                    messageDB.previousFileType = previousFileType;
                }

            } else if (type == "11") {
                let removedAt = new Date().getTime();
                if (message.removedAt) {
                    removedAt = message.removedAt;
                }
                let condition = { messageId: messageId, senderId: ObjectID(userId) };
                let dataToUpdate = { payload: payload, messageType: "11", removedAt: removedAt }
                messagesCollection.Update(condition, dataToUpdate, (err, result) => {
                    if (err) {
                        logger.error("AKEODP : ", err)
                    }
                })
            } else if (type == "12") {
                let editedAt = new Date().getTime();
                if (message.editedAt) {
                    editedAt = message.editedAt;
                }
                let condition = { messageId: messageId, senderId: ObjectID(userId) };
                let dataToUpdate = { payload: payload, wasEdited: true, editedAt: editedAt }
                messagesCollection.Update(condition, dataToUpdate, (err, result) => {
                    if (err) {
                        logger.error("AKEAASDFODP : ", err)
                    }
                })
            }
            if (type != "11" && type != "12" && type != "XX15") {
                // dasboardModule.dashboard({ "subject": "msg", "type": messageDB.messageType });
                /** to avoid duplicate messages when the internet is not fast */
                messagesCollection.UpdateWithUpsert({
                    chatId: chatId, senderId: ObjectID(userId),
                    messageId: messageId
                }, messageDB, function (err) {
                    if (err) logger.error("1AS221ADLPS", err);
                })
            }
            if (type == "16") {

                postCollection.UpdateWithIncrice({ _id: ObjectID(message.postId) }, { shareCount: 1 }, (err) => {
                    if (err) logger.error("1AS2KKOP2901S", err);
                })
            }
            callback(null, "done2");
        }
    ]);
}

function createGroupMessage(message) {
    /**
     * to: group chat id
     */
    var userId, GroupId, payload, type, messageId, secretId, thumbnail, creation,
        userImage, toDocId, name, dataSize, extension, fileName, mimeType, receiverIdentifier, totalMembers,
        members = {}, previousReceiverIdentifier, previousFrom, previousPayload, previousType, previousId, replyType, previousFileType;

    userId = message.from;
    GroupId = message.to;// targetUserId
    receiverIdentifier = message.receiverIdentifier;
    totalMembers = message.totalMembers;
    payload = message.payload;
    type = message.type;
    messageId = message.id;
    secretId = (message.secretId) ? message.secretId : "";
    creation = new Date().getTime();
    thumbnail = message.thumbnail;
    mimeType = message.mimeType;
    extension = message.extension;
    fileName = message.fileName;

    previousReceiverIdentifier = message.previousReceiverIdentifier;
    previousFrom = message.previousFrom;
    previousPayload = message.previousPayload;
    previousType = message.previousType;
    previousId = message.previousId;
    replyType = message.replyType;
    previousFileType = message.previousFileType;

    userImage = message.userImage;
    toDocId = message.toDocId;
    name = message.name;
    dataSize = message.dataSize;


    async.waterfall([
        function (callback) {

            /** find the group */
            var condition = {
                _id: ObjectID(GroupId),
                ["members." + userId]: { $exists: true },
                ["members." + userId + ".inactive"]: { $exists: false }
            };

            chatList.Select(condition, (err, result) => {
                if (err) {
                    logger.error("KELODPS : ", err)
                    callback(err, null);
                } else {
                    if (result && result[0]) {
                        for (let key in result[0]["members"]) {
                            members[key] = {
                                memberId: ObjectID(key),
                                status: 1
                            }
                        }
                        members[userId]["readAt"] = new Date().getTime();
                        members[userId]["status"] = 3;

                        userListCollection.Select({ _id: ObjectID(userId) }, (err, userData) => {
                            if (err) {
                                callback(err, null);
                            } else {
                                let message = {};

                                message["alert"] = (userData && userData[0]) ? userData[0]["userName"] + " @ " + name : "You have a new message. @ " + name;
                                // message["msg"] = payload;
                                message["GroupId"] = GroupId;

                                message["msg"] = (type == 0 || type == "0")
                                    ? new Buffer(payload, 'base64').toString('ascii')
                                    : messageTypes[Number(type)];

                                for (let key in result[0]["members"]) {
                                    if (userId != key) {
                                        userListCollection.SelectOne({ _id: ObjectID(key) }, (err, result) => {
                                            var deviceType = (result && result.deviceInfo && result.deviceInfo.deviceType) ? result.deviceInfo.deviceType : "2";
                                            fcm.notifyFcmTpic({
                                                fcmTopic: key,
                                                // action: 1,
                                                pushType: 2,
                                                title: process.env.APP_NAME,
                                                msg: message["alert"],
                                                data: { "type": "ChatMSG" },
                                                deviceType: deviceType
                                            }, (err, result) => {
                                                if (err) console.log("error : KOAOEORP : ", err);
                                            })

                                        });


                                        // sendPushUtility.iosPushIfRequiredOnMessage({ targetId: key, message: message, groupId: GroupId, type: type });
                                    }
                                }
                            }
                        });

                    }
                    (result[0]) ? callback(null, result[0]._id) : callback(null, 1);
                }
            });
        },
        function (chatId, callback) {

            var messageIdDB = ObjectID(); // msgStatusId = ObjectID();

            if (chatId) {
                /** group chat found */
                var data = {
                    "message": messageIdDB
                };
                var condition = { _id: chatId };

                if (type != "11" && type != "12") {
                    chatList.UpdateByIdWithPush(condition, data, (err) => {
                        if (err) logger.error('ALPOPASASW223: ', err)
                    });
                }

                var messageDB = {
                    "_id": messageIdDB,
                    "totalMembers": totalMembers,
                    "messageId": messageId,
                    "secretId": secretId,
                    "senderId": ObjectID(userId),
                    "receiverId": ObjectID(userId),
                    "receiverIdentifier": receiverIdentifier,
                    "payload": payload,
                    "messageType": type,
                    "timestamp": new Date().getTime(),
                    "expDtime": 0,
                    "chatId": chatId,
                    "userImage": userImage,
                    "toDocId": toDocId,
                    "name": name,
                    "dataSize": dataSize,
                    "thumbnail": thumbnail,
                    "mimeType": mimeType,
                    "extension": extension,
                    "fileName": fileName,
                    "postId": ObjectID(message.postId),
                    "postType": message.postType,
                    "postTitle": message.postTitle,
                };

                if (type == '10') {
                    messageDB.previousReceiverIdentifier = previousReceiverIdentifier;
                    messageDB.previousFrom = previousFrom;
                    messageDB.previousPayload = previousPayload;
                    messageDB.previousType = previousType;
                    messageDB.previousId = previousId;
                    messageDB.replyType = replyType;
                    if (previousType == '9')
                        messageDB.previousFileType = previousFileType;
                } else if (type == "11") {
                    let removedAt = new Date().getTime();
                    if (message.removedAt) {
                        removedAt = message.removedAt;
                    }
                    let condition = { messageId: messageId, senderId: ObjectID(userId) };
                    let dataToUpdate = { payload: payload, messageType: "11", removedAt: removedAt }
                    messagesCollection.Update(condition, dataToUpdate, (err, result) => {
                        if (err) logger.error('ALPOP0W223: ', err)
                    })
                } else if (type == "12") {
                    let editedAt = new Date().getTime();
                    if (message.editedAt) {
                        editedAt = message.editedAt;
                    }
                    let condition = { messageId: messageId, senderId: ObjectID(userId) };
                    let dataToUpdate = { payload: payload, wasEdited: true, editedAt: editedAt }

                    messagesCollection.Update(condition, dataToUpdate, (err, result) => {
                        if (err) logger.error('ALPOPAA0W223: ', err)
                    })
                } else if (type == "13") {
                    /* for save the post in message */
                    messageDB["postId"] = message.postId || "";
                    messageDB["postType"] = message.postType;
                    messageDB["postTitle"] = message.postTitle || "";
                }
                if (type != "11" && type != "12") {
                    /** to avoid duplicate messages when the internet is not fast */
                    messagesCollection.UpdateWithUpsert({ chatId: chatId, senderId: ObjectID(userId), messageId: messageId },
                        messageDB, (err, result) => {
                            if (err) logger.error("KAOLWOEPR : ", err);
                        });
                }
            }
            callback(null, " done2");
        }
    ]);
}

/** Group chat acknowledgment */
function GroupChatAcknowledgement(data) {

    var userId = data.from;
    var messageId = data.msgIds;
    var status = parseInt(data.status);
    var updaetStatus = "members." + userId + ".status";

    /**
     * status: 2- delivered, 3- read
     */
    async.waterfall([
        function (messageCB) {

            /** fetch all the messages for which the delivered acknowledgment is sent */
            messagesCollection.Select({ messageId: { $in: messageId } }, (getMsgErr, getMsgResult) => {
                if (getMsgErr) {
                    logger.error("LKOPOWODO : ", getMsgErr)
                    messageCB(null, null)
                } else if (getMsgResult.length > 0) {
                    var msgIdArr = [], latestMsg = 0;
                    async.eachSeries(getMsgResult, function (msgObjj, msgCB) {
                        // if (msgObjj.statusId)
                        if (latestMsg < msgObjj.timestamp)
                            latestMsg = msgObjj.timestamp
                        msgIdArr.push(msgObjj._id)
                        msgCB(null)
                    }, function (msgLoopErr) {
                        if (msgLoopErr) logger.error(msgLoopErr.message)

                        messageCB(null, msgIdArr, latestMsg)
                    })
                } else {
                    /** nothing to do */
                    messageCB(null, null)
                }
            })

        },
        function (msgsIdArr, latestMsg) {

            if (msgsIdArr) {
                switch (parseInt(status)) {
                    case "2":
                    case 2: {
                        /**
                         * Message Delivered to the recipients
                         */
                        var deliveredAt = "members." + userId + ".deliveredAt";

                        var dataToUpdate = {
                            [updaetStatus]: 2
                        }
                        dataToUpdate[deliveredAt] = new Date().getTime();

                        if (messageId && messageId.length) {
                            messagesCollection.Update({ messageId: { "$in": messageId } }, dataToUpdate, (err, result) => {
                                if (err) logger.error(err)
                            });
                        }

                        break;
                    }
                    case "3":
                    case 3: {
                        /**
                         * Message Read by the recipients
                         */
                        messagesCollection.UpdateWithUpsert({
                            ["members." + userId]: { $exists: true },
                            ["members." + userId + ".readAt"]: { $exists: false },
                            timestamp: { $lte: latestMsg }
                        }, {
                            ["members." + userId + ".status"]: 3,
                            ["members." + userId + ".readAt"]: new Date().getTime()
                        }, (err, result) => {
                            if (err) logger.error("LAOPEOSOC : ", err);
                        })
                        break;
                    }
                }
            }
        }
    ])

}
/** single chat message acknowledgement  */
function Acknowledgement(data) {

    var userId = data.from;
    var messageId = data.msgIds;
    var status = parseInt(data.status);
    var targetId = data.to;
    var updaetStatus = "members." + userId + ".status";
    var secretId = data.secretId;

    /**
     * status: 2- delivered, 3- read
     */
    switch (parseInt(status)) {
        case "2":
        case 2: {
            /**
             * Message Delivered to the recipients
             */
            var deliveredAt = "members." + userId + ".deliveredAt";

            var dataToUpdate = {
                [updaetStatus]: 2
            }
            dataToUpdate[deliveredAt] = new Date().getTime();

            if (messageId && messageId.length) {
                messagesCollection.Update({ messageId: { "$in": messageId } }, dataToUpdate, (err, result) => {
                    if (err) logger.error("KFJFOOFGO ", err)
                });
            }
            break;
        }
        case "3":
        case 3: {
            /**
             * Message Read by the recipients
             */
            if (data.secretId && data.secretId != "") {
                /**
                 * secret chat
                 * get all the documents to update and update thier expiry time
                 */
                var condition = {
                    $and: [
                        { $and: [{ "receiverId": ObjectID(userId) }, { "senderId": ObjectID(targetId) }, { secretId: secretId }] },
                        // { ["members." + userId + ".status"]: { $exists: true } },
                        { ["members." + userId + ".status"]: { $ne: 3 } },
                        { "timestamp": { "$lte": new Date().getTime() } },
                        { "payload": { $ne: "" } }
                    ]
                }

                messagesCollection.Select(condition, (getMsgErr, getMsgResult) => {
                    if (getMsgResult.length > 0) {

                        async.each(getMsgResult, function (msgObj, msgCB) {
                            /**
                             * update the each doc
                             * get the 'dTime' and then calculate the 'expDtime'
                             */
                            var dataToUpdate = { ["members." + userId + ".status"]: 3, ["members." + userId + ".readAt"]: new Date().getTime() }
                            if (msgObj.dTime > 0) {
                                dataToUpdate['expDtime'] = new Date().getTime() + (msgObj.dTime * 1000)
                            }
                            messagesCollection.Update({ _id: msgObj._id }, dataToUpdate, (updtErr) => {
                                if (updtErr) {
                                    logger.error("KOLWOPSOP", err);
                                }
                            });
                            msgCB(null);
                        })
                    }
                })

            }
            else {
                /**
                 * normal chat
                 * update all the document in single query
                 */
                condition = {
                    $and: [
                        { $and: [{ "receiverId": ObjectID(userId) }, { "senderId": ObjectID(targetId) }, { secretId: "" }] },
                        //{ ["members." + userId + ".status"]: { $exists: true } },
                        { ["members." + userId + ".status"]: { $ne: 3 } },
                        { "timestamp": { "$lte": new Date().getTime() } }
                    ]
                }
                dataToUpdate = { ["members." + userId + ".status"]: 3, ["members." + userId + ".readAt"]: new Date().getTime() }

                messagesCollection.Update(condition, dataToUpdate, function (err) {
                    if (err) {
                        logger.error("KIOPAODOE : ", err);
                    }
                });
            }
            break;
        }
    }
}
