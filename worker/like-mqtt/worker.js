'use strict'

const logger = require('winston');
const { ObjectID } = require('mongodb');

const mqtt = require("../../library/mqtt");
const i18n = require('../../locales/locales');
const mongodb = require('../../models/mongodb');
const userList = require('../../models/userList');
const rabbitMq = require('../../library/rabbitMq');
const addReward = require('../../models/addReward');
const RUCReward = require("../../config/components/RUCReward.json");
// var sendPushUtility = require("../../library/iosPush/sendPush.js");

rabbitMq.connect(() => {
    mongodb.connect(() => {
        prepareConsumer(rabbitMq.getChannel(), rabbitMq.like_mqtt_queue, rabbitMq.get());
    });//create a connection to mongodb
});

/**
 * Preparing Consumer for Consuming Booking from locationUpdate Booking Queue
 * @param {*} channel location Booking Channel
 * @param {*} queue  location Booking Queue
 * @param {*} amqpConn RabbitMQ connection
 */
function prepareConsumer(channel, queue, amqpConn) {

    channel.assertQueue(queue.name, queue.options, function (err) {
        if (err) {
            throw err;
            // process.exit();
        } else {
            channel.consume(queue.name, function (msg) {
                var data = JSON.parse(msg.content.toString());

                userList.SelectOne({ _id: ObjectID(data.starUserId) }, (err, result) => {
                    if (err) logger.error("IN LIKE-MQTT WORKER - QOWKDOK : ", err)
                    if (result === null) logger.error("IN LIKE-MQTT WORKER STAR-USER NOT FOUND- QOWKDAAOK : _id = " + data.starUserId)

                    if (result && result._id && result.starRequest && result.starRequest.starUserProfileStatusCode == 4) {

                        /* for liker */
                        i18n.setLocale(data.userLang);
                        let payloadForLiker = {
                            type: RUCReward.LIKE.TYPE_LIKER,
                            topic: i18n.__("RUC_REWARD")["LIKE"]["TOPIC_FOR_LIKER"],
                            message: i18n.__(i18n.__("RUC_REWARD")["LIKE"]["ForLiker"], process.env.RUC_LIKER, process.env.CURRENCY_TYPE, result.userName)
                        };
                        /* send mqtt notification to Liker user */
                        // mqtt.publish(data.userId, JSON.stringify(payloadForLiker), { qos: 1 }, () => { });
                        /* send in ios if required */
                        // let message = { alert: payloadForLiker.topic, msg: payloadForLiker.message };
                        // sendPushUtility.iosPushIfRequiredOnMessage({
                        //     targetId: data.userId, message: message, userId: data.starUserId,
                        //     type: payloadForLiker.type
                        // });
                        try {
                            /* addReward for like */
                            // addReward(data.userId, process.env.RUC_LIKER, i18n.__(i18n.__("RUC_REWARD")["LIKE"]["ForLiker"], process.env.RUC_LIKER, process.env.CURRENCY_TYPE, result.userName || "Star User"), "CREDIT").then(() => { });
                        } catch (error) {
                            logger.error(error)
                        }

                        /* for starUser */
                        i18n.setLocale((result && result.lang) ? i18n.setLocale(result.lang) : process.env.DEFAULT_LANGUAGE);
                        userList.SelectOne({ _id: ObjectID(data.userId) }, (err, userData) => {
                            if (userData && userData.userName) {
                                let payloadForStarUser = {
                                    type: RUCReward.LIKE.TYPE_STAR_USER,
                                    topic: i18n.__("RUC_REWARD")["LIKE"]["TOPIC_FOR_STARUSER"],
                                    message: i18n.__(i18n.__("RUC_REWARD")["LIKE"]["ForStarUser"], process.env.RUC_LIKE_STARUSER, process.env.CURRENCY_TYPE, userData.userName)
                                };
                                /* send mqtt notification to star user */
                            //    mqtt.publish(data.starUserId, JSON.stringify(payloadForStarUser), { qos: 1 }, () => { });
                                /* send in ios if required */
                                // let messageA = { alert: payloadForStarUser.topic, msg: payloadForStarUser.message };
                              //  sendPushUtility.iosPushIfRequiredOnMessage({
                              //      targetId: data.starUserId, message: messageA, userId: data.userId,
                               //     type: payloadForStarUser.type
                               // });
                                try {
                                    /* addReward for like */
                               //     addReward(data.starUserId, process.env.RUC_LIKE_STARUSER, i18n.__(i18n.__("RUC_REWARD")["LIKE"]["ForStarUser"], process.env.RUC_LIKE_STARUSER, process.env.CURRENCY_TYPE, userData.userName), "CREDIT").then(() => { });
                                } catch (error) {
                                    logger.error(error)
                                }
                            } else {
                                logger.error("IN LIKE-MQTT WORKER USER NOT FOUND AS LIKER - QOWKDAAAAOASQK : _id = " + data.userId)
                            }
                        });
                    } else {
                        logger.error("IN LIKE-MQTT WORKER USER NOT FOUND AS STAR - QOWKDAAOASQK : _id = " + data.starUserId)
                    }
                });

            }, { noAck: true }, function () {
                if (queue.worker.alwaysRun) {
                    // keep worker running
                } else {
                    //To check if need to exit worker
                    rabbitMq.exitWokerHandler(channel, queue, amqpConn);
                }
            });
        }
    });
}