'use strict'

const logger = require('winston');
const { ObjectID } = require('mongodb');

const mqtt = require("../../library/mqtt");
const i18n = require('../../locales/locales');
const mongodb = require('../../models/mongodb');
const userList = require('../../models/userList');
const rabbitMq = require('../../library/rabbitMq');
const addReward = require('../../models/addReward');
const RUCReward = require("../../config/components/RUCReward.json");
var sendPushUtility = require("../../library/iosPush/sendPush.js");

rabbitMq.connect(() => {
    mongodb.connect(() => {
        prepareConsumer(rabbitMq.getChannel(), rabbitMq.view_post_mqtt_queue, rabbitMq.get());
    });//create a connection to mongodb
});

/**
 * Preparing Consumer for Consuming Booking from locationUpdate Booking Queue
 * @param {*} channel location Booking Channel
 * @param {*} queue  location Booking Queue
 * @param {*} amqpConn RabbitMQ connection
 */
function prepareConsumer(channel, queue, amqpConn) {

    channel.assertQueue(queue.name, queue.options, function (err) {
        if (err) {
            throw err;
            // process.exit();
        } else {
            channel.consume(queue.name, function (msg) {

                var data = JSON.parse(msg.content.toString());

                if (data.starUserId != data.userId) {
                    userList.SelectOne({ _id: ObjectID(data.starUserId) }, (err, result) => {
                        if (err) logger.error("IN VIEW_POST-MQTT WORKER - QOWKDOK : ", err)
                        if (result === null) logger.error("IN VIEW_POST-MQTT WORKER STAR-USER NOT FOUND- QOWKDAAOK : _id = " + data.starUserId)

                        if (result && result._id && result.starRequest && result.starRequest.starUserProfileStatusCode == 4) {
                            /* for starUser */
                            i18n.setLocale((result && result.lang) ? i18n.setLocale(result.lang) : process.env.DEFAULT_LANGUAGE);

                            let payloadForStarUser = {
                                type: RUCReward.VIEW_POST.TYPE_STAR_USER,
                                topic: i18n.__("RUC_REWARD")["VIEW_POST"]["TOPIC_FOR_STARUSER"],
                                message: i18n.__(i18n.__("RUC_REWARD")["VIEW_POST"]["FOR_STAR_USER"], process.env.RUC_VIEW_POST_STARUSER,process.env.CURRENCY_TYPE, data.userName)
                            };
                            /* send mqtt notification to star user */
                            // mqtt.publish(data.starUserId, JSON.stringify(payloadForStarUser), { qos: 1 }, () => { });
                            /* send in ios if required */
                            let messageA = { alert: payloadForStarUser.topic, msg: payloadForStarUser.message };
                            // sendPushUtility.iosPushIfRequiredOnMessage({
                            //     targetId: data.starUserId, message: messageA, userId: data.userId,
                            //     type: payloadForStarUser.type
                            // });
                            try {
                                /* addReward for VIEW_POST */
                                // addReward(data.starUserId, process.env.RUC_VIEW_POST_STARUSER, i18n.__(i18n.__("RUC_REWARD")["VIEW_POST"]["FOR_STAR_USER"], process.env.RUC_VIEW_POST_STARUSER,process.env.CURRENCY_TYPE, data.userName), "CREDIT").then(() => { });
                            } catch (error) {
                                logger.error(error)
                            }

                            /* for user */
                            i18n.setLocale(data.userLang);
                            let payloadForLiker = {
                                type: RUCReward.VIEW_POST.TYPE_USER,
                                topic: i18n.__("RUC_REWARD")["VIEW_POST"]["TOPIC_FOR_USER"],
                                message: i18n.__(i18n.__("RUC_REWARD")["VIEW_POST"]["FOR_USER"], process.env.RUC_VIEW_POST_USER,process.env.CURRENCY_TYPE)
                            };
                            /* send mqtt notification to Liker user */
                            // mqtt.publish(data.userId, JSON.stringify(payloadForLiker), { qos: 1 }, () => { });
                            /* send in ios if required */
                            // let message = { alert: payloadForLiker.topic, msg: payloadForLiker.message };
                            // sendPushUtility.iosPushIfRequiredOnMessage({
                            //     targetId: data.userId, message: message, userId: data.starUserId,
                            //     type: payloadForLiker.type
                            // });
                            try {
                                /* addReward for VIEW_POST */
                                // addReward(data.userId, process.env.RUC_VIEW_POST_USER, i18n.__(i18n.__("RUC_REWARD")["VIEW_POST"]["FOR_USER"], process.env.RUC_VIEW_POST_USER,process.env.CURRENCY_TYPE), "CREDIT").then(() => { });
                            } catch (error) {
                                logger.error(error)
                            }
                        }
                    });
                }


            }, { noAck: true }, function () {
                if (queue.worker.alwaysRun) {
                    // keep worker running
                } else {
                    //To check if need to exit worker
                    rabbitMq.exitWokerHandler(channel, queue, amqpConn);
                }
            });
        }
    });
}