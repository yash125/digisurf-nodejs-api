'use strict'

const logger = require('winston');
const { ObjectID } = require('mongodb');

const mqtt = require("../../library/mqtt");
const i18n = require('../../locales/locales');
const mongodb = require('../../models/mongodb');
const userList = require('../../models/userList');
const rabbitMq = require('../../library/rabbitMq');
const addReward = require('../../models/addReward');
const RUCReward = require("../../config/components/RUCReward.json");
// var sendPushUtility = require("../../library/iosPush/sendPush.js");


rabbitMq.connect(() => {
    mongodb.connect(() => {
        prepareConsumer(rabbitMq.getChannel(), rabbitMq.follow_mqtt_queue, rabbitMq.get());
    });//create a connection to mongodb
});

/**
 * Preparing Consumer for Consuming Booking from locationUpdate Booking Queue
 * @param {*} channel location Booking Channel
 * @param {*} queue  location Booking Queue
 * @param {*} amqpConn RabbitMQ connection
 */
function prepareConsumer(channel, queue, amqpConn) {
    channel.assertQueue(queue.name, queue.options, function (err) {
        if (err) {
            throw err
            // process.exit();
        } else {
            channel.consume(queue.name, function (msg) {

                var data = JSON.parse(msg.content.toString());
            
                userList.SelectOne({ _id: ObjectID(data.starUserId) }, (err, result) => {
                    if (err) logger.error("IN FOLLOW-MQTT WORKER - QOWKDOK : ", err)

                    if (result && result._id && result.starRequest && result.starRequest.starUserProfileStatusCode == 4) {
                        /* for starUser */
                        logger.info("IN FOLLOW-MQTT WORKER STAR-USER  FOUND - WQWQERRWKDAAOK : _id = " + data.starUserId)
                        // i18n.setLocale((result && result.lang) ? i18n.setLocale(result.lang) : process.env.DEFAULT_LANGUAGE);
                        i18n.setLocale(process.env.DEFAULT_LANGUAGE);

                        let payloadForStarUser = {
                            type: RUCReward.FOLLOW.TYPE_STAR_USER,
                            topic: i18n.__("RUC_REWARD")["FOLLOW"]["TOPIC_FOR_STARUSER"],
                            message: i18n.__(i18n.__("RUC_REWARD")["FOLLOW"]["FOR_STAR_USER"], process.env.RUC_FOLLOW_STARUSER, process.env.CURRENCY_TYPE, data.userName)
                        };
                        /* send mqtt notification to star user */
                  //      mqtt.publish(data.starUserId, JSON.stringify(payloadForStarUser), { qos: 1 }, () => { });
                        /* send in ios if required */
                        // let message = { alert: payloadForStarUser.topic, msg: payloadForStarUser.message };
                        // sendPushUtility.iosPushIfRequiredOnMessage({
                        //     targetId: data.starUserId, message: message, userId: data.userId,
                        //     type: payloadForStarUser.type
                        // });
                        try {
                            /* addReward for FOLLOW */
                      //      addReward(data.starUserId, process.env.RUC_FOLLOW_STARUSER, i18n.__(i18n.__("RUC_REWARD")["FOLLOW"]["FOR_STAR_USER"], process.env.RUC_FOLLOW_STARUSER, process.env.CURRENCY_TYPE,data.userName), "CREDIT").then(() => { });
                        } catch (error) {
                            logger.error(error)
                        }
                        /* for user */
                        i18n.setLocale(data.userLang);
                        let payloadForLiker = {
                            type: RUCReward.FOLLOW.TYPE_USER,
                            topic: i18n.__("RUC_REWARD")["FOLLOW"]["TOPIC_FOR_USER"],
                            message: i18n.__(i18n.__("RUC_REWARD")["FOLLOW"]["FOR_USER"], process.env.RUC_FOLLOW_USER,process.env.CURRENCY_TYPE)
                        };
                        /* send mqtt notification to Liker user */
                  //      mqtt.publish(data.userId, JSON.stringify(payloadForLiker), { qos: 1 }, () => { });
                        /* send in ios if required */
                        // let messageA = { alert: payloadForLiker.topic, msg: payloadForLiker.message };
                        // sendPushUtility.iosPushIfRequiredOnMessage({
                        //     targetId: data.userId, message: messageA, userId: data.starUserId,
                        //     type: payloadForLiker.type
                        // });
                        try {
                            /* addReward for FOLLOW */
                            // addReward(data.userId, process.env.RUC_FOLLOW_USER, i18n.__(i18n.__("RUC_REWARD")["FOLLOW"]["FOR_USER"], process.env.RUC_FOLLOW_USER,process.env.CURRENCY_TYPE, result.userName ), "CREDIT").then(() => { });
                        } catch (error) {
                            logger.error(error)
                        }
                    } else {
                        logger.error("IN FOLLOW-MQTT WORKER STAR-USER NOT FOUND-WERRWKDAAOK : _id = " + data.starUserId)
                    }
                });
            }, { noAck: true }, function () {
                if (queue.worker.alwaysRun) {
                    // keep worker running
                } else {
                    //To check if need to exit worker
                    rabbitMq.exitWokerHandler(channel, queue, amqpConn);
                }
            });
        }
    });
}