'use strict'

const logger = require('winston');
const mqtt = require("../../library/mqtt");
const i18n = require('../../locales/locales');
const mongodb = require('../../models/mongodb');
const userList = require('../../models/userList');
const rabbitMq = require('../../library/rabbitMq');
const addReward = require('../../models/addReward');
const RUCReward = require("../../config/components/RUCReward.json");
// var sendPushUtility = require("../../library/iosPush/sendPush.js");

rabbitMq.connect(() => {
    mongodb.connect(() => {
        prepareConsumer(rabbitMq.getChannel(), rabbitMq.referral_mqtt_queue, rabbitMq.get());
    });//create a connection to mongodb
});

/**
 * Preparing Consumer for Consuming Booking from locationUpdate Booking Queue
 * @param {*} channel location Booking Channel
 * @param {*} queue  location Booking Queue
 * @param {*} amqpConn RabbitMQ connection
 */
function prepareConsumer(channel, queue, amqpConn) {

    channel.assertQueue(queue.name, queue.options, function (err) {
        if (err) {
            throw err;
            // process.exit();
        } else {
            channel.consume(queue.name, function (msg) {

                var data = JSON.parse(msg.content.toString());


                userList.SelectOne({ referralCode: data.userReferralCode.toUpperCase() }, (err, result) => {
                    if (err) logger.error("IN REFERRAL-MQTT WORKER - QOWKDOK : ", err)
                    if (result === null) logger.error("IN REFERRAL-MQTT WORKER USER NOT FOUND-WERRWKDAAOK : _id = " + data.userReferralCode)

                    if (result && result._id && result.starRequest && result.starRequest.starUserProfileStatusCode == 4) {
                        /* for starUser */
                        i18n.setLocale((result && result.lang) ? i18n.setLocale(result.lang) : process.env.DEFAULT_LANGUAGE);

                        let payloadForStarUser = {
                            type: RUCReward.REFERRAL.TYPE_STAR_USER,
                            topic: i18n.__("RUC_REWARD")["REFERRAL"]["TOPIC_FOR_STARUSER"],
                            message: i18n.__(i18n.__("RUC_REWARD")["REFERRAL"]["FOR_STAR_USER"], process.env.RUC_REFERRAL_STARUSER, process.env.CURRENCY_TYPE, data.userName)
                        };
                        /* send mqtt notification to star user */
                        // mqtt.publish(result._id.toString(), JSON.stringify(payloadForStarUser), { qos: 1 }, () => { });
                        /* send in ios if required */
                        // let messageA = { alert: payloadForStarUser.topic, msg: payloadForStarUser.message };
                        // sendPushUtility.iosPushIfRequiredOnMessage({
                        //     targetId: result._id.toString(), message: messageA,
                        //     type: payloadForStarUser.type
                        // });
                        try {
                            /* addReward for REFERRAL */
                            // addReward(result._id.toString(), process.env.RUC_REFERRAL_STARUSER, i18n.__(i18n.__("RUC_REWARD")["REFERRAL"]["FOR_STAR_USER"], process.env.RUC_REFERRAL_STARUSER,process.env.CURRENCY_TYPE, data.userName), "CREDIT").then(() => { });
                        } catch (error) {
                            logger.error(error)
                        }


                        // try {
                        //      /* for user */
                        // i18n.setLocale(process.env.DEFAULT_LANGUAGE);
                        // let payload = {
                        //     type: RUCReward.REFERRAL.TYPE_USE_USER,
                        //     topic: i18n.__("RUC_REWARD")["REFERRAL"]["TOPIC_FOR_USER"],
                        //     message: i18n.__(i18n.__("RUC_REWARD")["REFERRAL"]["FOR_USER"], process.env.RUC_REFERRAL_USER)
                        // };
                        // /* send mqtt notification to Liker user */
                        // mqtt.publish(data.userId, JSON.stringify(payload), { qos: 1 }, () => { });
                        //     /* addReward for REFERRAL */
                        //     addReward(data.userId, process.env.RUC_REFERRAL_USER, "REFERRAL", "CREDIT").then(() => { });
                        // } catch (error) {
                        //     logger.error("OKOKOAKSA : ",error)
                        // }



                    } else if (result && result._id) {
                        /* for starUser */
                        i18n.setLocale((result && result.lang) ? i18n.setLocale(result.lang) : process.env.DEFAULT_LANGUAGE);

                        let payloadForNormalUser = {
                            type: RUCReward.REFERRAL.TYPE_NORMAL_USER,
                            topic: i18n.__("RUC_REWARD")["REFERRAL"]["TOPIC_FOR_NORMAL_USER"],
                            message: i18n.__(i18n.__("RUC_REWARD")["REFERRAL"]["FOR_NORMAL_USER"], process.env.RUC_REFERRAL_NORMAL_USER, process.env.CURRENCY_TYPE,data.userName)
                        };
                        /* send mqtt notification to star user */
                        // mqtt.publish(result._id.toString(), JSON.stringify(payloadForNormalUser), { qos: 1 }, () => { });
                        /* send in ios if required */
                        // let message = { alert: payloadForNormalUser.topic, msg: payloadForNormalUser.message };
                        // sendPushUtility.iosPushIfRequiredOnMessage({
                        //     targetId: result._id.toString(), message: message,
                        //     type: payloadForNormalUser.type
                        // });
                      
                        try {
                            /* addReward for REFERRAL */
                            // addReward(result._id.toString(), process.env.RUC_REFERRAL_NORMAL_USER, i18n.__(i18n.__("RUC_REWARD")["REFERRAL"]["FOR_NORMAL_USER"], process.env.RUC_REFERRAL_NORMAL_USER, process.env.CURRENCY_TYPE,data.userName), "CREDIT").then(() => { });
                        } catch (error) {
                            logger.error(error)
                        }

                        // /* for user */
                        // i18n.setLocale(process.env.DEFAULT_LANGUAGE);
                        // let payload = {
                        //     type: RUCReward.REFERRAL.TYPE_USE_USER,
                        //     topic: i18n.__("RUC_REWARD")["REFERRAL"]["TOPIC_FOR_USER"],
                        //     message: i18n.__(i18n.__("RUC_REWARD")["REFERRAL"]["FOR_USER"], process.env.RUC_REFERRAL_USER)
                        // };
                        // /* send mqtt notification to Liker user */
                        // mqtt.publish(data.userId, JSON.stringify(payload), { qos: 1 }, () => { });
                        // try {
                        //     /* addReward for REFERRAL */
                        //     addReward(data.userId, process.env.RUC_REFERRAL_USER, "REFERRAL ON STARUSER", "CREDIT").then(() => { });
                        // } catch (error) {
                        //     logger.error(error)
                        // }
                    } else {
                        logger.error("referralCode not found : " + data.userReferralCode);
                    }
                });
            }, { noAck: true }, function () {
                if (queue.worker.alwaysRun) {
                    // keep worker running
                } else {
                    //To check if need to exit worker
                    rabbitMq.exitWokerHandler(channel, queue, amqpConn);
                }
            });
        }
    });
}