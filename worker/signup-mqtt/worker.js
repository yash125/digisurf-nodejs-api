'use strict'

const logger = require('winston');
const { ObjectID } = require('mongodb');
var QRCode = require('qrcode')
const request = require('request');
var followAPI = require('../../web/routes/follow/Post');

const mqtt = require("../../library/mqtt");
const i18n = require('../../locales/locales');
const mongodb = require('../../models/mongodb');
const rabbitMq = require('../../library/rabbitMq');
const RUCReward = require("../../config/components/RUCReward.json");
const RUCWallet = require("../../models/RUCWallet");
const customerWallet = require("../../models/customerWallet");
// var sendPushUtility = require("../../library/iosPush/sendPush.js");
var uploadFile = require("../../library/uploadImage").upload;

rabbitMq.connect(() => {
    mongodb.connect(() => {
        prepareConsumer(rabbitMq.getChannel(), rabbitMq.signup_mqtt_queue, rabbitMq.get());
    });//create a connection to mongodb
});

/**
 * Preparing Consumer for Consuming Booking from locationUpdate Booking Queue
 * @param {*} channel location Booking Channel
 * @param {*} queue  location Booking Queue
 * @param {*} amqpConn RabbitMQ connection
 */
function prepareConsumer(channel, queue, amqpConn) {

    channel.assertQueue(queue.name, queue.options, function (err) {
        if (err) {
            throw err;
            // process.exit();
        } else {
            channel.consume(queue.name, function (msg) {

                var data = JSON.parse(msg.content.toString());
                // console.log("in SIGNUP worker", data)


                // if (uploadProfilePic) {
                //     /** if req.payload.uploadProfilePic == true means upload image */
                //     let image = "data:image/png;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxEHEBISEBASFhIVEBAQEBMPEhAQEBAQFhIWFxUVFhoYHiggGBolGxMVITEhJikrLi4uFyAzODMtNygtLisBCgoKDQ0OFRAPFisdFR0tLS4tLS0tNy0rKystLS0rLSstLSsrKy0tLSstLS0rLS0tKy0rLS0tLSstLSstLS4tK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABQYBAwQCB//EADcQAQACAAMFBQUIAQUBAAAAAAABAgMEEQUhMUFREjJhcZETIoGhwRRCUnKSsdHhYhUzQ4KiBv/EABUBAQEAAAAAAAAAAAAAAAAAAAAB/8QAFhEBAQEAAAAAAAAAAAAAAAAAABEB/9oADAMBAAIRAxEAPwD7iAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMTaK8Z9Wq2ZpXjevrANw01zWHbhevrDbW0W4TE+QMgAAAAAAAAAAAAAAAAAAAAADEzohNobUnE1rhzpXnbnPl0gEhm9o0y27XW3Sv16IrH2riYvCezH+PH1cIDN7zfjMz5zMsAAzW004TMeUzDADtwNqYmFxntR/lx9UrlNp0zG6fdt0tz8pV0BbxA5Dac4Pu331686/zCdpaLxrE6xPCYBkAAAAAAAAAAAAAAAAEftjNewp2Y71tY8YjnIOLa2e9tM0rPuxO+fxT/CNAAAAAAAAAB3bLz32aezbuTP6Z6+ThAW+N4jNi5r2texM768PGv9JMAAAAAAAAAAAAAABWdo4/2jEtPKJ7MeULBm8T2WHa3Ss+vJVgAAAAAAAAAAAAbspjfZ71t0nf5TxWlUFm2die1wqT4aT8NwOkAAAAAAAAAAAAAHDtmdMG3jNY/wDUK8sG2/8AZn81f3V8AAAAAAAAAAAABPbCnXCnwvMftP1QKc2DGmHaet5/aASYAAAAAAAAAAAAAOXadPaYV48NfSdforS3Wr2omJ5xoqeLhzhWms8pmAeQAAAAAAAAAAAFi2PTsYUeMzb5q9Ws3mIjjM6QteFh+yrFY5REA9gAAAAAAAAAAAAAIXbmW7MxeOE7refKU08Y2FGNWazwmNAVMbc1gTlrTWfhPWOrUAAAAAAAAAD3gYU49orXjPyjqDu2Ll/aX7c8K8PzJ5qy2BGXrFY5fOectoAAAAAAAAAAAAAAAAObO5SM3XSd0x3Z6f0ruPgWy89m0aT8p8YWtqzGXrmY0tGvTrHkCqjvzey74O+vvV8O9HwcAAAAAAO3KbMvj7592vWeM+UA5cLCnGmIrGsrDkMlGUjrae9P0jwbMrla5WNKx5zPGW8AAAAAAAAAAAAAAAHm9opGszERHGZ3QD01Y2YpgRra0R58UVndrzbdh7o/FPH4Qi7Wm86zMzPWZ1lBbKXjEiJidYnhMPSr5TOXys+7O7nWeE/wnMptGmZ3a9m3S306g7GnHytMfvVifHhPq3CiLxNi0nu2tHnpMNM7En8cekpoBC/6Jb8cekt2HsWsd69p8tISgDRgZPDwO7WNes759W8AGLWisazw56ubN5+mW4zrb8Mcfj0Qeczt83x3V5Vjh8eqCx4WNXGjWtomPCdXtUsO84c61mYnrG5LZLa+u7E/VHD4wCXGInXgyoAAAAAAAAAxaezGs8OYPOLiRgxNrTpEcVdz+etm56V5R9Z8Wdo5z7Xbd3Y7sdfFyIACgADqy+0MTA4W1jpbfH8u/C21We/SY8a6TCGEgslNpYV/vxHnrDbGaw7cL1/VCrCi0zmaR9+v6oar7RwqffifLWf2VsBNYu2qx3azPnpEODMbRxMf72kdK7vnxcgkABQAB27P2hOVnSd9OnOvjH8LBS8YkRMTrE74mFSd+y899mns27kz+mevkgsACgAAAAAAh9t5v/jr53+kJLNY8Zek2nlw8Z5Qq97TeZmeMzrPmgwAoAAAAAAAAAAAAAAAAAAm9jZv2kdi0747uvOvT4JRU8HEnBtFo4xOq04GLGNWLRwmIlB7AUAAAYtbsxMzwiNZBC7dx+1aKRy96fOeHy/dFveNie2ta085mf4eAAAAAAAAAAAAAAAAAAAAAExsLH71J/NX6/RDtuUxvYXrbpMa+XMFqCN4AAA4tr4ns8K3j7vrx+WrtRH/ANBfuV/Nb00j6ghwAAAAAAAAAAAAAAAAAAAAAAAWXZuL7bCrPPTSfON30dSL2DfWto6W19Y/pKGAAAhNu9+v5Z/cARoAAAAAAAAAAAAAAAAAAAAAAAJXYPG//X6pgAAAf//Z"
                //     uploadFile(image, userId);
                // }
                // let image = "data:image/png;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxEHEBISEBASFhIVEBAQEBMPEhAQEBAQFhIWFxUVFhoYHiggGBolGxMVITEhJikrLi4uFyAzODMtNygtLisBCgoKDQ0OFRAPFisdFR0tLS4tLS0tNy0rKystLS0rLSstLSsrKy0tLSstLS0rLS0tKy0rLS0tLSstLSstLS4tK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABQYBAwQCB//EADcQAQACAAMFBQUIAQUBAAAAAAABAgMEEQUhMUFREjJhcZETIoGhwRRCUnKSsdHhYhUzQ4KiBv/EABUBAQEAAAAAAAAAAAAAAAAAAAAB/8QAFhEBAQEAAAAAAAAAAAAAAAAAABEB/9oADAMBAAIRAxEAPwD7iAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMTaK8Z9Wq2ZpXjevrANw01zWHbhevrDbW0W4TE+QMgAAAAAAAAAAAAAAAAAAAAADEzohNobUnE1rhzpXnbnPl0gEhm9o0y27XW3Sv16IrH2riYvCezH+PH1cIDN7zfjMz5zMsAAzW004TMeUzDADtwNqYmFxntR/lx9UrlNp0zG6fdt0tz8pV0BbxA5Dac4Pu331686/zCdpaLxrE6xPCYBkAAAAAAAAAAAAAAAAEftjNewp2Y71tY8YjnIOLa2e9tM0rPuxO+fxT/CNAAAAAAAAAB3bLz32aezbuTP6Z6+ThAW+N4jNi5r2texM768PGv9JMAAAAAAAAAAAAAABWdo4/2jEtPKJ7MeULBm8T2WHa3Ss+vJVgAAAAAAAAAAAAbspjfZ71t0nf5TxWlUFm2die1wqT4aT8NwOkAAAAAAAAAAAAAHDtmdMG3jNY/wDUK8sG2/8AZn81f3V8AAAAAAAAAAAABPbCnXCnwvMftP1QKc2DGmHaet5/aASYAAAAAAAAAAAAAOXadPaYV48NfSdforS3Wr2omJ5xoqeLhzhWms8pmAeQAAAAAAAAAAAFi2PTsYUeMzb5q9Ws3mIjjM6QteFh+yrFY5REA9gAAAAAAAAAAAAAIXbmW7MxeOE7refKU08Y2FGNWazwmNAVMbc1gTlrTWfhPWOrUAAAAAAAAAD3gYU49orXjPyjqDu2Ll/aX7c8K8PzJ5qy2BGXrFY5fOectoAAAAAAAAAAAAAAAAObO5SM3XSd0x3Z6f0ruPgWy89m0aT8p8YWtqzGXrmY0tGvTrHkCqjvzey74O+vvV8O9HwcAAAAAAO3KbMvj7592vWeM+UA5cLCnGmIrGsrDkMlGUjrae9P0jwbMrla5WNKx5zPGW8AAAAAAAAAAAAAAAHm9opGszERHGZ3QD01Y2YpgRra0R58UVndrzbdh7o/FPH4Qi7Wm86zMzPWZ1lBbKXjEiJidYnhMPSr5TOXys+7O7nWeE/wnMptGmZ3a9m3S306g7GnHytMfvVifHhPq3CiLxNi0nu2tHnpMNM7En8cekpoBC/6Jb8cekt2HsWsd69p8tISgDRgZPDwO7WNes759W8AGLWisazw56ubN5+mW4zrb8Mcfj0Qeczt83x3V5Vjh8eqCx4WNXGjWtomPCdXtUsO84c61mYnrG5LZLa+u7E/VHD4wCXGInXgyoAAAAAAAAAxaezGs8OYPOLiRgxNrTpEcVdz+etm56V5R9Z8Wdo5z7Xbd3Y7sdfFyIACgADqy+0MTA4W1jpbfH8u/C21We/SY8a6TCGEgslNpYV/vxHnrDbGaw7cL1/VCrCi0zmaR9+v6oar7RwqffifLWf2VsBNYu2qx3azPnpEODMbRxMf72kdK7vnxcgkABQAB27P2hOVnSd9OnOvjH8LBS8YkRMTrE74mFSd+y899mns27kz+mevkgsACgAAAAAAh9t5v/jr53+kJLNY8Zek2nlw8Z5Qq97TeZmeMzrPmgwAoAAAAAAAAAAAAAAAAAAm9jZv2kdi0747uvOvT4JRU8HEnBtFo4xOq04GLGNWLRwmIlB7AUAAAYtbsxMzwiNZBC7dx+1aKRy96fOeHy/dFveNie2ta085mf4eAAAAAAAAAAAAAAAAAAAAAExsLH71J/NX6/RDtuUxvYXrbpMa+XMFqCN4AAA4tr4ns8K3j7vrx+WrtRH/ANBfuV/Nb00j6ghwAAAAAAAAAAAAAAAAAAAAAAAWXZuL7bCrPPTSfON30dSL2DfWto6W19Y/pKGAAAhNu9+v5Z/cARoAAAAAAAAAAAAAAAAAAAAAAAJXYPG//X6pgAAAf//Z"
                // uploadFile(image, "background_image/" + userId);

                // QRCode.toDataURL("" + userId, function (err, url) {
                //     uploadFile(url, "qr_code/" + userId);
                // });


                try {
                    if (process.env && process.env.DEFAULT_FOLLOW && process.env.DEFAULT_FOLLOW.length) {
                        let DEFAULT_FOLLOW = process.env.DEFAULT_FOLLOW.split(",");
                        DEFAULT_FOLLOW.forEach(ele => {
                            followAPI.handler1({
                                headers: { lang: "en" },
                                payload: { followingId: ele },
                                auth: { credentials: { _id: data["_id"] } }
                            }, {})
                        })
                    }
                } catch (error) {
                    logger.error('KLAPWAZAXE : ', error);
                }

                /**
                * call stream signUp api using request package
                */
                request.post({
                    headers: { 'content-type': 'application/json', 'lan': 'en' },
                    url: `${process.env.LIVE_STREAM_API}/user`,
                    body: data.liveStreamData,
                    json: true
                }, function (error, response, body) {
                    if (error) console.log("eror : KOIPLOIHDH ", error)
                });

                //group call api
                request.post({
                    headers: { 'content-type': 'application/json', 'lan': 'en' },
                    url: `${process.env.GROUP_CALL_API}/loginSignUp`,
                    body: data.groupCall,
                    json: true
                }, function (error) {
                    if (error) {
                        logger.error('KLAPWDDASDE : ', error);
                    }
                });



                // i18n.setLocale(process.env.DEFAULT_LANGUAGE);
                // let payloadForUser = {
                //     type: RUCReward.SIGNUP.TYPE_USER,
                //     topic: i18n.__("RUC_REWARD")["SIGNUP"]["TOPIC_USER"],
                //     message: i18n.__(i18n.__("RUC_REWARD")["SIGNUP"]["FOR_USER"], process.env.RUC_ON_SIGNUP,process.env.CURRENCY_TYPE,process.env.APP_NAME)
                // };
                /* send mqtt notification to star user */
                //   mqtt.publish(data.userId, JSON.stringify(payloadForUser), { qos: 1 }, () => { });
                /* send in ios if required */
                //   let messageA = { alert: payloadForUser.topic, msg: payloadForUser.message };
                //   sendPushUtility.iosPushIfRequiredOnMessage({
                //       targetId:  data.userId, message: messageA, userId: data.userId,
                //       type: payloadForUser.type
                //   });
                try {

                    /**
                     *  call Ecome API
                     */
                    // request.post({
                    //     headers: {
                    //         'content-type': 'application/json',
                    //         'authorization': '{"userId":"' + data.userId + '","userType":"user","metaData":{}}',
                    //         'platform': 1,
                    //         'currencysymbol': '$',
                    //         'currencycode': 'USD',
                    //         'language': 'en'
                    //     },
                    //     url: `${process.env.ECOME_API}/v1/ecomm/customerSyncDatum`,
                    //     body: {
                    //         "userId": data.userId,
                    //         "firstName": data.liveStreamData.firstName,
                    //         "lastName": data.liveStreamData.lastName,
                    //         "email": data.liveStreamData.email,
                    //         "password": "123456",
                    //         "countryCode": data.liveStreamData.countryCode,
                    //         "mobile": data.liveStreamData.mobile.replace(data.liveStreamData.countryCode, ""),
                    //         "dateOfBirth": "1997-06-08",
                    //         "profilePicture": data.liveStreamData.profilePic,
                    //         "latitude": "0",
                    //         "longitude": "0",
                    //         "ipAddress": "0.0.0.0",
                    //         "city": "Bangalore",
                    //         "country": "India"
                    //     },
                    //     json: true
                    // }, function (error, response, body) {
                    //     if (error) console.log("eror : KOIPLOIHDH ", error)
                    // });
                    /* addReward for SIGNUP */
                    // addReward(data.userId, process.env.RUC_SIGNUP_USER, i18n.__(i18n.__("RUC_REWARD")["SIGNUP"]["FOR_USER"], process.env.RUC_SIGNUP_USER), "CREDIT").then(() => { });
                    // let customerWalletData = {
                    //     txnId: "TXN-ID-" + new Date().getTime().toString(),
                    //     userId: ObjectID(data.userId),
                    //     txnType: "CREDIT",
                    //     txnTypeCode: 1,
                    //     trigger: i18n.__(i18n.__("RUC_REWARD")["SIGNUP"]["FOR_USER"], process.env.RUC_ON_SIGNUP,process.env.CURRENCY_TYPE,process.env.APP_NAME),
                    //     currency: process.env.CURRENCY_TYPE,
                    //     currencySymbol: process.env.CURRENCY_TYPE,
                    //     coinType: process.env.CURRENCY_TYPE,
                    //     coinOpeingBalance: 0,
                    //     cost: 0,
                    //     coinAmount: Number(process.env.RUC_ON_SIGNUP),
                    //     coinClosingBalance: Number(process.env.RUC_ON_SIGNUP),
                    //     paymentType: process.env.CURRENCY_TYPE,
                    //     timestamp: new Date().getTime(),
                    //     initatedBy: process.env.APP_NAME
                    // };
                    // customerWallet.Insert(customerWalletData, () => { })
                    // RUCWallet.Insert({ _id: ObjectID(data.userId), RUCs: { RUC: Number(process.env.RUC_ON_SIGNUP) } }, () => { });
                } catch (error) {
                    logger.error(error)
                }


            }, { noAck: true }, function () {
                if (queue.worker.alwaysRun) {
                    // keep worker running
                } else {
                    //To check if need to exit worker
                    rabbitMq.exitWokerHandler(channel, queue, amqpConn);
                }
            });
        }
    });
}