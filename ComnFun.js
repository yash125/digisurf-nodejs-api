var mqtt = require('mqtt');
var conf = require('./config');

var config = {
    mqtt: {
        options: {
            keepalive: 0,
            clientId: "Comanfunction_Clinet_1001_"+new Date().getTime(),
            username: process.env.MQTT_USERNAME,
            password: process.env.MQTT_PASSWORD,
            clean: true
        }
    }
};
const mqtt_client = mqtt.connect(process.env.MQTT_URL, config.mqtt.options);

module.exports = {

    sendFCMPush: function (pushToken, message) {

        var FCM = require('fcm-node');

        var fcm = new FCM(conf.SERVER_FCM_KEY);

         message = {
            to: pushToken,
            collapse_key: 'your_collapse_key',

            notification: {
                //   title: 'send Push',
                body: message
            },

            data: {  //you can send only notification or only data(or include both)
                body: { test: 123 }
                // my_another_key: 'my another value'
            }

        };

        //callback style
        fcm.send(message, function (err, response) {
            if (err) {
                console.log("Something has gone wrong!", err);
            } else {
                console.log("Successfully sent with response: ", response);

            }
        });

    },
    publishMqtt: function (topic, message) {
        mqtt_client.publish(topic, message, { qos: 1 })
    }
}