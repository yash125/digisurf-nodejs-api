"use strict"
// var fs = require('fs');//import fs for file operations

var MongoClient = require('mongodb').MongoClient;

var config = require('../config');
var Messages = require('./statusMessages.json');//import messages for status response

var db;

MongoClient.connect(config.mongodb.url,
    function (err, db2) {
        if (err) { console.error(err); }
        console.log("MongoDb connected");
        db = db2;
    });


/**
 * MongoDB Class
 */
class MongoDB {
    constructor() { }
    /**Insert function is use to insert parameters to database.
     * It's a generic function, tablename is use for Tablename in the db.
     * data is array to insert documents in table.
     * if inserted then go callback function else error.
     * **/
    Insert(tablename, data, extra, callback) {  // expiry(redis key) not using here
        var collection = db.collection(tablename);
        collection.insert([data], function (err, result) {
            return callback(err, result);
        });
    }

    /**Select function is use to fetch data from database.
    * It's a generic function, tablename is use for Tablename in the db.
    * data is array to provide where condition in table.
    * if success then go callback function else error.
    * Result is use to store all data resulted from select function.
    * **/
    Select(tablename, data, extras, callback) {
        var collection = db.collection(tablename);
        if (extras.length) {
            collection.findOne(data, extras, (function (err, result) {
                return callback(err, result);
            }));
        } else {
            if (data.length) {
                collection.findOne(data, (function (err, result) {
                    return callback(err, result);
                }));
            }
            collection.find(data).sort({ _id: -1 }).toArray(function (err, result) {
                return callback(err, result);
            });
        }
    }

    AggreGate(tablename, data, callback) {
        var collection = db.collection(tablename);

        collection.aggregate(data, function (err, result) {
            return callback(err, result);
        });
    }

    /**
     * FindAndModify
     */
    FindAndModify(tablename, data, condition, projection, option, callback) {
        var collection = db.collection(tablename);
        collection.findAndModify(condition, [], data, {
            new: (option.new) ? option.new : false,
            upsert: (option.upsert) ? option.upsert : false,
            fields: projection
        }, function (err, result) {
            return callback(err, result);
        });

    }

    /**Update function is use to Update parameters to database.
     * It's a generic function, tablename is use for Tablename in tnullhe db.
     * data is array to Update documents in table.
     * if Updated then go callback function else error.
     * **/
    Update(tablename, data, condition, callback) {
        var collection = db.collection(tablename);
        collection.update(condition, { $set: data }, { multi: true }, (function (err, result) {
            return callback(err, result);
        }));
    }

    UpdateAdvanced(tablename, data, condition, options, callback) {
        var collection = db.collection(tablename);
        collection.update(condition, data, { upsert: (options.upsert) ? options.upsert : false, multi: (options.multi) ? options.multi : false }, (function (err, result) {
            return callback(err, result);
        }));
    }

    UpdateWithPush(tablename, data, condition, callback) {
        var collection = db.collection(tablename);
        collection.update(condition, data, { $multi: true }, (function (err, result) {
            return callback(err, result);
        }));
    }

    /**Delete function is use to delete parameters to database.
     * It's a generic function, tablename is use for Tablename in the db.
     * data is array to give the condition.
     * if deleted then go callback function else error.
     * **/
    Delete(tablename, condition, callback) {
        var collection = db.collection(tablename);
        collection.remove(condition, { multi: 1 }, function (err, numberOfRemovedDocs) {
            return callback(err, numberOfRemovedDocs);
        });
    }
}


module.exports = function (name) {
    switch (name) {
        case 'Mongo': return new MongoDB;
        // default: return fs.appendFile("errLogs.txt", "System Error", Messages.WRONG_DB + name, function (err) {
        //     if (err) {
        //         console.log("error in dbConfig : ", err);
        //     }
        // })
    }
};