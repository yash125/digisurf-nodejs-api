const mqtt = require('mqtt');
const path = require('path');
const logger = require('winston')
const config = require('../../config');
const replace = require('replace-in-file');


// get ip address
var os = require('os');
var interfaces = os.networkInterfaces();
var addresses = [];
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            addresses.push(address.address);
        }
    }
}

var MqttClientId = "MQTT_CLIENT_" + addresses[0] + "_" + new Date().getTime();
/**
 * options:
 *  - clientId
 *  - username
 *  - password
 *  - keepalive: number
 *  - clean:
 *  - will: 
 */
var mqtt_options = {
    clientId: MqttClientId,
    keepalive: 0,
    clean: true,
    username: config.mqtt.MQTT_USERNAME,
    password: config.mqtt.MQTT_PASSWORD
};

var client = mqtt.connect(config.mqtt.MQTT_URL, mqtt_options);

client.on('connect', function () {
    logger.silly("MQTT connected ")
    client.subscribe("ENV", { qos: 0 });
});


client.on('error', (err) => {
    console.log("error ", err);
});

/**
 * options: object
 *  - qos (integer): 0 > fire and forget
 *          1 > guaranteed delivery
 *          2 > guaranteed delivery with awk
 */
function mqtt_publish(topic, message, options, callback) {
    try {
        client.publish(topic, message, { qos: (options.qos) ? options.qos : 0, retain: false })
        callback(null, { err: 0, message: 'Publish.' })
    } catch (exec) {
        callback({ err: 1, message: exec.message })
    }

}


/**
 * topic : string
 * options: object
 *  - qos : 0 > fire and forget
 *          1 > guaranteed delivery
 *          2 > guaranteed delivery with awk
 */
function mqtt_subscribe(topic, options, callback) {
    try {
        client.subscribe(topic, { qos: (options.qos) ? options.qos : 0 });
    } catch (exec) {
        callback({ err: 1, message: exec.message })
    }
    callback(null, { err: 0, message: 'Subscribed.' })
}

client.on('message', (topic, message) => {
    /**
     * match the pattern (start with) 
     */
    
    
    switch (topic) {
        case String(topic.match("ENV")):
            try {
                require('dotenv').config({ path: path.resolve(".env") });

                var objectData = JSON.parse(message);
                for (const key in objectData) {

                    // var regex = new RegExp(key + '="' + [0 - 9]* + '"', 'g');
                    replace({
                        files: path.resolve(".env"),
                        from: key + '="' + process.env[key] + '"',
                        to: key + '="' + objectData[key] + '"',
                        disableGlobs: true
                    }).then(() => {
                        process.env[key] = objectData[key];
                    }).catch(error => {
                        console.error('Error occurred:', error);
                    });
                }
                require('dotenv').config({ path: path.resolve(".env") });

            } catch (error) {
                console.error('Error :', error);
            }

            break;
        default:
    }
})

exports.publish = mqtt_publish
exports.subscribe = mqtt_subscribe