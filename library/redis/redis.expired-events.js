const PubSub = require("./pubsub");
const expireTransfer = require('./../../models/expireTransfer');

function RedisExpiredEvents() {
  PubSub.subscribe("__keyevent@0__:expired");
  PubSub.on("message", async (channel, message) => {

    const [type, key] = message.split(":");
    switch (type) {

      case "reminder": {
        await expireTransfer(key);   
        break;
        }

      }
  });
}

module.exports = RedisExpiredEvents