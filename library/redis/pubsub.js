var Redis = require('redis');

const config = {
  port: process.env.REDIS_PORT, host: process.env.REDIS_IP, password: process.env.REDIS_PASS, db: 0
};


const subscriber = new Redis.createClient(config);
const publisher = new Redis.createClient(config);

class PubSub {
  publish(channel, message) {
    publisher.publish(channel, message);
  }
  subscribe(channel) {
    subscriber.subscribe(channel);
  }

  on(event, callback) {
    subscriber.on(event, (channel, message) => {
      callback(channel, message);
    });
  }
}

var pubSub = new PubSub();

module.exports = pubSub;