const bcrypt = require('bcrypt');
const saltRound = 10;

let encrypt = async(password)=>{

    return new Promise((resolve,reject)=>{
        bcrypt.hash(password,saltRound)
        .then((hashedPassword)=>{
            resolve(hashedPassword);
        })
        .catch((err)=>{
            console.log(err);
            reject(err);
        })
    })   
}

let verify = (password,hash)=>{

    return new Promise((resolve,reject)=>{

        bcrypt.compare(password,hash)
        .then((result)=>resolve(result))
        .catch((err)=>reject(err))
    })
}

/* encrypt('newpassword').then((pass)=>console.log(pass))
.catch((err)=>console.log('error')); */

/* verify('pass','$2b$10$qjbHz5fnpQYp9BpTHdywBeME1vgo1U/V9dpFy9Rc/5wmYwdhw2Eki')
.then((result)=>console.log(result))
.catch((err)=>console.log(err)) */

module.exports = {
    encrypt,
    verify
}