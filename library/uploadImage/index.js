

var accessKeyId = process.env.AWS_ACCESSKEY_ID;
var secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;
var bucketName = process.env.AWS_S3_BUCKET_NAME;


// You can either "yarn add aws-sdk" or "npm i aws-sdk"
const AWS = require('aws-sdk')
// Configure AWS with your access and secret key. I stored mine as an ENV on the server
// ie: process.env.ACCESS_KEY_ID = "abcdefg"
AWS.config.update({ accessKeyId: accessKeyId, secretAccessKey: secretAccessKey });

// Create an s3 instance
const s3 = new AWS.S3();


var upload = (base64Image, filename) => {
    // base64Image = "data:image/png;base64," + base64Image;
    // var base64Image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMEAAAEFCAMAAABtknO4AAABhlBMVEX////
    // Ensure that you POST a base64 data to your server.
    // Let's assume the variable "base64" is one.
    const base64Data = new Buffer.from(base64Image.replace(/^data:image\/\w+;base64,/, ""), 'base64')

    // Getting the file type, ie: jpeg, png or gif
    const type = "jpg"//base64Image.split(';')[0].split('/')[1]

    // Generally we'd have a userId associated with the image
    // For this example, we'll simulate one
    const userId = filename;
    var Key = (userId && userId.length == 24) ? `profile_image/${userId}` : userId;
    // With this setup, each time your user uploads an image, will be overwritten.
    // To prevent this, use a unique Key each time.
    // This won't be needed if they're uploading their avatar, hence the filename, userAvatar.js.
    const params = {
        Bucket: bucketName,
        Key: Key,
        Body: base64Data,
        ACL: 'public-read',
        ContentEncoding: 'base64', // required
        ContentType: `image/${type}` // required. Notice the back ticks
    }

    // The upload() is used instead of putObject() as we'd need the location url and assign that to our user profile/database
    // see: http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#upload-property
    s3.upload(params, (err) => {
        if (err) { return console.log(err) }

        // Continue if no error
        // Save data.Location in your database
        console.log('Image successfully uploaded.');
    });
}

module.exports = { upload }