const logger = require('winston');
const moment = require('moment-timezone');

const path = require('path');
const AWS = require("aws-sdk");

const config = require('../../config');


const getCognitoTokens = (user) => new Promise((resolve, reject) => {
    AWS.config.update({
        region: config.BucketRegion, //Region name
        credentials: new AWS.Credentials({
            accessKeyId: config.AccessKeyId, //Aws Access key ID
            secretAccessKey: config.SecretAccessKey // Aws Secrer Key ID
        })
    });
    // if authenticated, initialize a cognito client with your AWS developer credentials
    let cognitoidentity = new AWS.CognitoIdentity({ apiVersion: '2014-06-30' });

    // create a new request to retrieve the token for your end user
    var params = {
        IdentityPoolId: config.IdentityPoolId, /* required Cognito Identity Pool ID*/
        Logins: { /* required */
            //Authenticated user : User Id
            [config.CognitoProvider]: user, // user ID as a value
        },
        // IdentityId: 'STRING_VALUE',
        TokenDuration: parseInt(config.TokenDuration)//Toke Expiration time (Second)
    };
    cognitoidentity.getOpenIdTokenForDeveloperIdentity(params, function (err, data) {
        if (err) {
            reject(err)
        } else {
            data.bucket = config.Bucket;
            data.region = config.BucketRegion;
            //You will get IdentityId and Token
            resolve(data)
        }

    });
});


const uploadImageInS3 = (user, req) => new Promise((resolve, reject) => {
    getCognitoTokens(user)
        .then((data) => {
            AWS.config.update({
                region: config.BucketRegion,
                credentials: new AWS.CognitoIdentityCredentials({
                    IdentityId: data.IdentityId,
                    Logins: {
                        'cognito-identity.amazonaws.com': data.Token
                    }
                })
            });
            var s3 = new AWS.S3({
                apiVersion: "2006-03-01",
                params: { Bucket: config.Bucket }
            });

            let fileData = req.payload;
            const file = fileData['file'];
            let fileName = req.payload.fileName ? req.payload.fileName : moment().valueOf();
	   // let imageName = req.payload.folder + '/' + fileName + path.extname(file.hapi.fileName.toString());
            let imageName = req.payload.folder + '/' + fileName;

            var params = {
                Body: file._data,
                Bucket: config.Bucket,
                Key: imageName,
                StorageClass: "STANDARD",
                ACL: "public-read",
//                ContentType: file.hapi.headers['content-type'],
ContentType:"image"
            };
            s3.putObject(params, function (err, res) {
                if (err) {
                    logger.error("uploadImageInS3 error =>", err)
                    return reject(err);
                }
                else {
                    let path = "https://" + config.Bucket + ".s3." + config.BucketRegion + ".amazonaws.com/" + imageName;
                    return resolve({ imageUrl: path });
                }
            });
        }).catch((e) => {
            logger.error("uploadImageInS3 error =>", e)
            return reject(e);
        });//get token
});

module.exports = {
    getCognitoTokens,
    uploadImageInS3,
};
