

let bussinessEmail = (data) => {


    let value = {
        "subject": "Email Verification code for " + process.env.APP_NAME + " bussiness Profile",
        "body": "<p> Hello" + ',' + data.userName + '<br/><br/>Your bussiness email verification code is : ' + data.randomCode + '.' + '<br/>Please enter this on the app to confirm your bussiness email address.' + '<br/><br/><br/>Cheers, <br/> Team ' + process.env.APP_NAME + '.</p>'
    }

    return value



}
module.exports = {

    bussinessEmail

}

