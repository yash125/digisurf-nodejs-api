    
'use strict'

const request = require('request');
const logger = require('winston');

let sendEmail = (data)=>{
        let sendMailParams = {
            "toEmail": data.emailId,
            "subject": data.subject,
            "body": data.body
        };
        //sendGrid
        request.post({
            headers: { 'content-type': 'application/json' },
            url: `http://localhost:5000/sendEmail`,
            body: sendMailParams,
            json: true
        }, function (error, response, body) {

            if (error)
                logger.info('0000',error);
            else {
                logger.info(body);
                
            }

        });
    return 1
}
module.exports = {
    sendEmail
}