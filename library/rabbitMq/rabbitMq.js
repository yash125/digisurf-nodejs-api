'use strict'

/** 
 * This const requires the mongo url
 * @const
 * @requires module:config 
 */
const config = require('../../config');
const logger = require('winston');
const worker = require('./worker');

logger.default.transports.console.colorize = true;
logger.default.transports.console.timestamp = true;

//For the RabbitMQ connection
// const amqp = require('amqplib/callback_api');
const amqp = require('amqplib/callback_api');

//For the chhanel corresponding to the high traffic apis
let channel = null;

//set up a connection
/**
 * @amqpConn will hold the connection and channels will be set up in the connection.
 */
var state = { amqpConn: null };

/**
 * Method to connect to the mongodb
 * @param {*} rabitmq url
 * @returns connection object
 */

let connect = (callback) => {
    if (state.amqpConn) {
        return callback();
    }
    amqp.connect(config.RABBITMQ.RABBITMQURL + "?heartbeat=60", (err, conn) => {
        // amqp.connect("amqp://3embed:3embed@13.234.25.102:5672?heartbeat=60", (err, conn) => {
        if (err) {
            return callback(err);
        }
        conn.on("error", (err) => {
            if (err.message !== "Connection closing") {
                logger.error("[AMQP] conn error", err.message);
                return callback(err);
            }
        });
        conn.on("close", () => {
            logger.info("[AMQP] reconnecting");
        });
        state.amqpConn = conn;

        preparePublisher();
        return callback();
    });
}
exports.connect = connect;

/**
 * Method to get the connection object of the mongodb
 * @returns db object
 */
exports.get = () => {
    return state.amqpConn;
}

/**
 * Method to Prepare Publisher
 */
function preparePublisher() {
    channel = state.amqpConn.createChannel((err, ch) => {
        if (closeOnErr(err)) return;
        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });
        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });
    });
}

/**
 * Closing RabbitMQ connection on error
 * @param {*} err Error Object
 */
function closeOnErr(err) {

    if (!err) return false;

    logger.error("[AMQP] error", err);
    state.amqpConn.close();
    return true;
}

exports.getChannel = () => {
    return channel;
}

//For the queues corresponding to the high traffic apis
exports.like_mqtt_queue = {
    name: 'like_mqtt_queue',
    threshold: 100,
    options: { durable: true },
    worker: worker.like_mqtt
};

exports.comment_mqtt_queue = {
    name: 'comment_mqtt_queue',
    threshold: 100,
    options: { durable: true },
    worker: worker.comment_mqtt
};

exports.view_post_mqtt_queue = {
    name: 'view_post_mqtt_queue',
    threshold: 100,
    options: { durable: true },
    worker: worker.view_post_mqtt
};

exports.follow_mqtt_queue = {
    name: 'follow_mqtt_queue',
    threshold: 100,
    options: { durable: true },
    worker: worker.follow_mqtt
};

exports.signup_mqtt_queue = {
    name: 'signup_mqtt_queue',
    threshold: 100,
    options: { durable: true },
    worker: worker.signup_mqtt
};

exports.referral_mqtt_queue = {
    name: 'referral_mqtt_queue',
    threshold: 100,
    options: { durable: true },
    worker: worker.referral_mqtt
};

exports.mqtt_insert_Message = {
    name: 'mqtt_insert_Message',
    threshold: 100,
    options: { durable: true },
    worker: worker.mqtt_insert_Message
};

exports.sendToQueue = (queue, data) => {
    if (channel) {
        channel.assertQueue(queue.name, queue.options, function () {
            channel.sendToQueue(queue.name, Buffer.from(JSON.stringify(data)));
            worker.checkWorker(queue);
        });
    } else {
        logger.error("channal not found ", channel);
        connect(() => {
            if (channel) {
                channel.assertQueue(queue.name, queue.options, function () {
                    channel.sendToQueue(queue.name, Buffer.from(JSON.stringify(data)));
                    worker.checkWorker(queue);
                });
            } else {
                logger.error("channal not found...2");
            }
        });
    }
}

exports.exitWokerHandler = (channel, queue) => {
    channel.assertQueue(queue.name, queue.options, function (err, amqpQueue) {
        if (err) {
            process.exit();
        } else if (typeof amqpQueue.messageCount != "undefined" && amqpQueue.messageCount == 0) {
            if (queue.worker.alwaysRun) {
                // keep worker running
            } else {
                // stop worker
                // channel.connection.close();
                // amqpConn.close();
                logger.warn(queue.name + " worker stopped");
                // process.exit();
            }
        }
    });
}