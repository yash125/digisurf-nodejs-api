'use strict'

const path = require('path');
const fork = require('child_process').fork;
let logger = require('winston');

// let like_mqtt = {
//     name: "Like Worker",
//     path: path.join(__dirname, '../../worker/like-mqtt/'),
//     worker: null,
//     flag: true,
//     alwaysRun: true
// };

// let comment_mqtt = {
//     name: "Comment Worker",
//     path: path.join(__dirname, '../../worker/comment-mqtt/'),
//     worker: null,
//     flag: true,
//     alwaysRun: true
// };

// let view_post_mqtt = {
//     name: "View Post Worker",
//     path: path.join(__dirname, '../../worker/view-post-mqtt/'),
//     worker: null,
//     flag: true,
//     alwaysRun: true
// };

// let follow_mqtt = {
//     name: "follow Worker",
//     path: path.join(__dirname, '../../worker/follow-mqtt/'),
//     worker: null,
//     flag: true,
//     alwaysRun: true
// };

let signup_mqtt = {
    name: "signup Worker",
    path: path.join(__dirname, '../../worker/signup-mqtt/'),
    worker: null,
    flag: true,
    alwaysRun: true
};

// let referral_mqtt = {
//     name: "referral Worker",
//     path: path.join(__dirname, '../../worker/referral-mqtt/'),
//     worker: null,
//     flag: true,
//     alwaysRun: true
// };

let mqtt_insert_Message = {
    name: "mqtt_insert_Message Worker",
    path: path.join(__dirname, '../../worker/mqtt_insert_Message/'),
    worker: null,
    flag: true,
    alwaysRun: true
};



function startWorkerProcess(workerData) {
    if (workerData.worker == null) {
        workerData.worker = fork(workerData.path);
        workerData.worker.on('exit', function (code, signal) {
            logger.error(`${workerData.name} worker exited with code ${code} and signal ${signal}`);
            workerData.worker = null;
            if (workerData.flag) {
                if (workerData.alwaysRun)
                    startWorkerProcess(workerData);
            }
        });
    } else {
        logger.warn(`${workerData.name} worker is already running`);
    }
}

function stopWorkerProcess(workerData) {
    if (workerData.worker == null) {
        logger.warn(`${workerData.name} worker is not running`);
    } else {
        workerData.flag = false;
        workerData.worker.kill();
        workerData.worker = null;
    }
}

function checkWorker(queue) {
    if (queue.worker.worker == null) {
        startWorkerProcess(queue.worker);
    }
}

function startWorker() {
    // startWorkerProcess(like_mqtt);
    // startWorkerProcess(comment_mqtt);
    // startWorkerProcess(view_post_mqtt);
    // startWorkerProcess(follow_mqtt);
    startWorkerProcess(signup_mqtt);
    // startWorkerProcess(referral_mqtt);
    startWorkerProcess(mqtt_insert_Message);
}

function exitHandler() {
    // stopWorkerProcess(like_mqtt);
    // stopWorkerProcess(comment_mqtt);
    // stopWorkerProcess(view_post_mqtt);
    // stopWorkerProcess(follow_mqtt);
    stopWorkerProcess(signup_mqtt);
    // stopWorkerProcess(referral_mqtt);
    stopWorkerProcess(mqtt_insert_Message);
    process.exit();
}

module.exports = {
    startWorker,
    startWorkerProcess,
    stopWorkerProcess,
    exitHandler,
    checkWorker,
    mqtt_insert_Message,
    signup_mqtt
};