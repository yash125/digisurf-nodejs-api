var grpc = require('grpc');
var protoLoader = require('@grpc/proto-loader');

// implement Unary call
var Unary_proto = grpc.loadPackageDefinition(protoLoader.loadSync(
    __dirname + '/sms.proto',
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    })).sms;

var smsClient = new Unary_proto.sms_service('127.0.0.1:5002', grpc.credentials.createInsecure());
function sendSMS({ to, otp, body, countryCode, hashKey }) {
    
    smsClient.sendSms({
        smsService: "MSG91",
        to: to,
        userName: "",
        body: "",
        trigger: "",
        countryCode: countryCode,
        otp: otp,
        hashKey: hashKey,
        appName: "Dub.ly",
    }, (err, res) => {
        console.log("err : ", err);
    });
}

module.exports = { sendSMS }

