var grpc = require('grpc');
var protoLoader = require('@grpc/proto-loader');

var Email = grpc.loadPackageDefinition(protoLoader.loadSync(
    __dirname + '/email.proto',
    {
        keepCase: true,
        longs: String,
        enums: String,
        defaults: true,
        oneofs: true
    })).email;

var emailClient = new Email.email_service(process.env.GRPC_Email_MICRO_SERVICE, grpc.credentials.createInsecure());

function sendEmail({ to, subject, body, userName, trigger }) {
    emailClient.sendEmail({
        emailService: process.env.EMAIL_SERVICE, toEmail: to,
        userName, trigger, subject, body
    }, (err) => {
        console.log("err : ", err);
    });
}

module.exports = { sendEmail }