const apn = require("apn");
var objectId = require("mongodb").ObjectID;
var middleWare = require("../../Controller/dbMiddleware.js");
// var messageTypes = ["text", "image", "video", "location", "contact", "audio", "sticker", "doodle", "giphy", "Document", "postInMessage",
//     "...", "...", "...", "...", "Payment", "...", "call"];
const IsProduction = process.env.IS_IOS_NOTIFICATION_IN_PRODUCTION;
const IsVOIPNotification = process.env.IS_IOS_VOIP_IN_PRODUCTION;


var Notification = {
    cert: __dirname + "/notificationCertificates/Certificates.pem",
    key: __dirname + "/notificationCertificates/Certificates_key.pem",
    production: IsProduction
};
var VoIP_Notification = {
    cert: __dirname + "/VoIPCertificates/Certificates.pem",
    key: __dirname + "/VoIPCertificates/Certificates_key.pem",
    production: IsVOIPNotification
};
const topic = process.env.IOS_TOPIC;//"com.mobifyillc.dochat";

var sendOn = {};
sendOn.iosPush = function iosPush(dataInJSON) {
    console.log("dataInJSON : ", dataInJSON)
}
sendOn.iosPushIfRequiredOnAudioCall = function iosPushIfRequiredOnAudioCall(dataInJSON) {
    try {
        
        middleWare.Select("customer", { _id: objectId(dataInJSON.targetId), "deviceInfo.deviceType": "1" }, "Mongo", {}, function (err, result) {
            if (result[0] && result[0].iosAudioCallPush) {

                //value : doChat || dubly  || picoadda
                if (result[0] && result[0].appName && result[0].appName == "doChat") {
                    VoIP_Notification = {
                        cert: __dirname + "/VoIPCertificates/Certificates.pem",
                        key: __dirname + "/VoIPCertificates/Certificates_key.pem",
                        production: IsVOIPNotification
                    }
                } else if (result[0] && result[0].result && request[0].appName == "dubly") {
                    VoIP_Notification = {
                        cert: __dirname + "/VoIPCertificates/dubly/Certificates.pem",
                        key: __dirname + "/VoIPCertificates/dubly/Certificates_key.pem",
                        production: IsVOIPNotification
                    }
                } else if (result[0] && result[0].appName && result[0].appName == "picoadda") {
                    VoIP_Notification = {
                        cert: __dirname + "/VoIPCertificates/picoadda/Certificates.pem",
                        key: __dirname + "/VoIPCertificates/picoadda/Certificates_key.pem",
                        production: IsVOIPNotification
                    }
                } else {
                    VoIP_Notification = {
                        cert: __dirname + "/VoIPCertificates/Certificates.pem",
                        key: __dirname + "/VoIPCertificates/Certificates_key.pem",
                        production: IsVOIPNotification
                    }
                }

                let service = new apn.Provider(VoIP_Notification);
                var note = new apn.Notification({
                    alert: dataInJSON["message"].alert,
                    payload: { data: dataInJSON },
                    "content-available": 1,
                    sound: "ringtone.wav"
                });

                if (process.env.IS_ADD_TOPIC_ON_AUDIO_CALL == "true") {
                    
                    note = new apn.Notification({
                        alert: dataInJSON["message"].alert,
                        payload: { data: dataInJSON },
                        "content-available": 1,
                        sound: "ringtone.wav",
                        topic: topic
                    });
                }

                try {
                    service.send(note, result[0].iosAudioCallPush).then(result => {
                        console.log("sent:", result.sent.length);
                        console.log("failed:", result.failed.length);
                        console.log(result.failed);
                    });
                } catch (error) {
                    console.log("error", error)
                }
                service.shutdown();
            }
        })
    } catch (error) {
        console.log("Exception : ", error)
    }

}
sendOn.iosPushIfRequiredOnVideoCall = function iosPushIfRequiredOnVideoCall(dataInJSON) {
    try {
        
        middleWare.Select("customer", { _id: objectId(dataInJSON.targetId), "deviceInfo.deviceType": "1" }, "Mongo", {}, function (err, result) {
            if (result[0] && result[0].iosVideoCallPush) {

                if (result[0] && result[0].appName && result[0].appName == "doChat") {
                    Notification = {
                        cert: __dirname + "/notificationCertificates/Certificates.pem",
                        key: __dirname + "/notificationCertificates/Certificates_key.pem",
                        production: IsProduction
                    };
                } else if (result[0] && result[0].result && request[0].appName == "dubly") {
                    Notification = {
                        cert: __dirname + "/notificationCertificates/dubly/Certificates.pem",
                        key: __dirname + "/notificationCertificates/dubly/Certificates_key.pem",
                        production: IsProduction
                    };
                } else if (result[0] && result[0].appName && result[0].appName == "picoadda") {
                    Notification = {
                        cert: __dirname + "/notificationCertificates/picoadda/Certificates.pem",
                        key: __dirname + "/notificationCertificates/picoadda/Certificates_key.pem",
                        production: IsProduction
                    };
                } else {
                    Notification = {
                        cert: __dirname + "/notificationCertificates/Certificates.pem",
                        key: __dirname + "/notificationCertificates/Certificates_key.pem",
                        production: IsProduction
                    };
                }
                let service = new apn.Provider(Notification);
                var note = new apn.Notification({
                    alert: dataInJSON["message"].alert,
                    payload: { data: dataInJSON },
                    sound: "ringtone.wav"
                });

                if (process.env.IS_ADD_TOPIC_ON_VIDEO_CALL == "true") {
                    
                    note = new apn.Notification({
                        alert: dataInJSON["message"].alert,
                        payload: { data: dataInJSON },
                        sound: "ringtone.wav",
                        topic: topic
                    });
                }

                try {
                    service.send(note, result[0].iosVideoCallPush).then(result => {
                        console.log("sent:", result.sent.length);
                        console.log("failed:", result.failed.length);
                        console.log(result.failed);
                    });
                } catch (error) {
                    console.log("error ", error)
                }
                service.shutdown();
            } else {
                console.log("notification not goes")
            }
        })
    } catch (error) {
        console.log("Exception : ", error)
    }

}
sendOn.iosPushIfRequiredOnMessage = function iosPushIfRequiredOnMessage(dataInJSON) {
    try {

        
        dataInJSON["thumbnail"] = dataInJSON["payload"];
        middleWare.Select("customer", { _id: objectId(dataInJSON.targetId) }, "Mongo", {}, function (err, result) {
            var isInactiveNotification = 0;
            var bage = 0;
            try {
                bage = (result[0] && result[0].bage) ? result[0].bage + 1 : 1;
                let _id = (dataInJSON.userId) ? dataInJSON.userId : dataInJSON.groupId;
                isInactiveNotification = (result && result[0]["inActiveUsersNotification"] && result[0]["inActiveUsersNotification"] != undefined &&
                    result[0]["inActiveUsersNotification"].map(id => id.toString()).includes(_id)) ? 1 : 0;
            } catch (error) {
                console.log('error : ', error)
            }

            if (result[0] && !result[0].onlineStatus) {
                middleWare.Update("customer", { "bage": bage }, { _id: objectId(result[0]._id.toString()) }, "Mongo", () => { });
            }


            if ((result[0] && (result[0]["deviceInfo"] && result[0]["deviceInfo"]["deviceType"] == "1")) ||
                (result[0] && (result[0]["deviceInfo"] && result[0]["deviceInfo"]["deviceType"] == 1))
                && result[0].iosVideoCallPush
                && isInactiveNotification == 0) {

                if (result[0] && result[0].appName && result[0].appName == "doChat") {
                    Notification = {
                        cert: __dirname + "/notificationCertificates/Certificates.pem",
                        key: __dirname + "/notificationCertificates/Certificates_key.pem",
                        production: IsProduction
                    };
                } else if (result[0] && result[0].result && request[0].appName == "dubly") {
                    Notification = {
                        cert: __dirname + "/notificationCertificates/dubly/Certificates.pem",
                        key: __dirname + "/notificationCertificates/dubly/Certificates_key.pem",
                        production: IsProduction
                    };
                } else if (result[0] && result[0].appName && result[0].appName == "picoadda") {
                    Notification = {
                        cert: __dirname + "/notificationCertificates/picoadda/Certificates.pem",
                        key: __dirname + "/notificationCertificates/picoadda/Certificates_key.pem",
                        production: IsProduction
                    };
                } else {
                    Notification = {
                        cert: __dirname + "/notificationCertificates/Certificates.pem",
                        key: __dirname + "/notificationCertificates/Certificates_key.pem",
                        production: IsProduction
                    };
                }
                let service = new apn.Provider(Notification);
                var note = new apn.Notification({
                    // alert: dataInJSON["message"].alert,
                    alert: { "title": dataInJSON["message"].alert, "body": "" + dataInJSON["message"].msg },
                    payload: { data: dataInJSON },
                    badge: bage,
                    sound: "sms-received.wav"
                });

                if (process.env.IS_ADD_TOPIC_ON_MESSAGE == "true") {
                    
                    note = new apn.Notification({
                        // alert: dataInJSON["message"].alert,
                        alert: { "title": dataInJSON["message"].alert, "body": "" + dataInJSON["message"].msg },
                        payload: { data: dataInJSON },
                        badge: bage,
                        sound: "sms-received.wav",
                        topic: topic
                    });
                }

                try {

                    service.send(note, result[0].iosVideoCallPush).then(result => {
                        console.log("iosPushIfRequiredOnMessage sent:", result.sent.length);
                        console.log("failed:", result.failed.length);
                        console.log(result.failed);
                    });
                } catch (error) {
                    console.log("error : ", error);
                }


                service.shutdown();
            } else {
                console.log(22222222)
            }
        })
    } catch (error) {
        console.log("Error: ", error)
    }

}
sendOn.iosPushCutCall = function iosPushCutCall(dataInJSON) {
    try {
        
        middleWare.Select("customer", { _id: objectId(dataInJSON.targetId), "deviceInfo.deviceType": "1" }, "Mongo", {}, function (err, result) {
            if (result[0] && result[0].iosVideoCallPush) {

                let service = new apn.Provider(Notification);
                var note = new apn.Notification({
                    alert: dataInJSON["message"].alert,
                    payload: { data: dataInJSON },
                    sound: "defualt"
                });

                if (process.env.IS_ADD_TOPIC_ON_CUTCALL == "true") {
                 
                    note = new apn.Notification({
                        alert: dataInJSON["message"].alert,
                        payload: { data: dataInJSON },
                        sound: "defualt",
                        topic: topic
                    });
                }
                try {
                    service.send(note, result[0].iosVideoCallPush).then(result => {
                        console.log("sent:", result.sent.length);
                        console.log("failed:", result.failed.length);
                        console.log(result.failed);
                    });
                } catch (error) {
                    console.log("error : ", error);
                }


                service.shutdown();
            }
        })
    } catch (error) {
        console.log("Exception : ", error)
    }

}
module.exports = sendOn