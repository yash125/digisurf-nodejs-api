'use strict'

const elasticClient = require('../elasticSearch');
const tablename = 'story';
const indexName = "starchatstory";
const version = 382;

function Select(condition, skip, limit, followAry, userId, callback) {

    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            from: skip, size: limit,
            "query": {
                "bool": condition
                /* e.g. : condition  = {
                    "must":[{
                        "match":{"key":"value"}
                    }]
                }*/
            },
            "sort": [{ "firstName": "asc" }]
        }
    }, function (err, result) {
        result = (result.hits && result.hits.hits) ? result.hits.hits : [];
        result.forEach(function (element) {
            element["_source"]["_id"] = element["_id"];
            element = element["_source"];
            element["followStatus"] = (followAry && followAry.map(e => e.toString()).includes(element["_id"])) ? 1 : 0;
        }, this);
        result = result.map(e => e._source);
        return callback(err, result);
    });
}
function SelectWithSort(condition, skip, limit, sort, callback) {

    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            from: skip, size: limit,
            "query": {
                "bool": {
                    "must": [{ "match": { "postStatus": 1 } }],
                    "should": condition
                }
            }
            , "sort": [{ "userName": "asc" }]
        }
    }, function (err, result) {
        callback(err, result);
    });
}

function getDetails(condition, skip, limit, followAry, userId, callback) {

    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            from: skip, size: limit,
            "query": {
                "bool": condition
            },
            "sort": [{ "starRequest.starUserKnownBy": "asc" }]
        }
    }, function (err, result) {
        result = (result.hits && result.hits.hits) ? result.hits.hits : [];
        result.forEach(function (element) {
            element["_source"]["_id"] = element["_id"]
            element = element["_source"]
        }, this);
        result = result.map(e => e._source);
        return callback(err, result);
    });
}

function SelectAll(callback) {
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            from: 0, size: 200,
            "query": {
                "match_all": {}
            }
        }
    }, function (err, result) {
        callback(err, result);
    });
}

function CustomeSelect(data, callback) {
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: data
    }, function (err, result) {
        callback(err, result);
    });
}
function Insert(data, callback) {
    let _id = "" + data._id;
    delete data._id;
    elasticClient.get().index({
        index: indexName,
        type: tablename,
        id: _id,
        body: data
    }, (err, result) => {
        callback(err, result);
    });
}

function UpdateWithPush(_id, field, value, callback) {

    elasticClient.get().update({
        index: indexName,
        type: tablename,
        id: _id,
        retry_on_conflict: 5,
        body: {
            "script": "ctx._source." + field + ".add('" + value + "')"
        }

    }, (err, results) => {
        callback(err, results)
    })
}
function UpdateWithPull(_id, field, value, callback) {

    elasticClient.get().update({
        index: indexName,
        type: tablename,
        id: _id,
        retry_on_conflict: 5,
        body: {
            "script": "ctx._source." + field + ".remove(ctx._source." + field + ".indexOf('" + value + "'))"
        }
    }, (err, results) => {
        callback(err, results);
    })
}
function Update(_id, data, callback) {

    elasticClient.get().update({
        index: indexName,
        type: tablename,
        id: _id,
        retry_on_conflict: 5,
        body: {
            doc: data,
            doc_as_upsert: true
        }
    }, (err, results) => {
        callback(err, results)
    })
}

function updateByQuery(condition, fieldName, fieldValue, callback) {
    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {
            query: { "match": condition },
            "script": { "source": `ctx._source.${fieldName}=${fieldValue}` }
        }
    }, (err, results) => {
        callback(err, results)
    })
}

function removeField(_id, fieldName, callback) {
    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {
            "script": `ctx._source.remove("${fieldName}")`,
            "query": {
                "bool": {
                    "must": [
                        {
                            "exists": {
                                "field": `${fieldName}`
                            }
                        }
                    ]
                }
            }
        }
    }, (err, results) => {
        callback(err, results)
    })
}

function setOffline(timestamp, callback) {
    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {
            query: {
                "range": {
                    "lastOnline": {
                        "lt": timestamp
                    }
                },
                // "match":{"onlineStatus":1}
            },
            "script": { "source": "ctx._source.onlineStatus=0" }
        }
    }, (err, results) => {
        callback(err, results)
    })
}

function Delete(tablename, condition, callback) {
    elasticClient.get().deleteByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {

            query: {
                match: condition
            }
        }
    }, (err, results) => {
        callback(err, results)
    })
}
function Bulk(dataInArray, callback) {
    elasticClient.get().bulk({
        body: dataInArray
    }, (err, results) => {
        callback(err, results)
    })
}
function updateByQueryWithArray(condition, callback) {
    elasticClient.updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: condition
    }, (err, results) => {
        callback(err, results)
    })
}

module.exports = {
    updateByQueryWithArray, setOffline, Bulk, removeField,
    CustomeSelect, Select, Insert, Update, updateByQuery, Delete,
    UpdateWithPush, UpdateWithPull, SelectAll, SelectWithSort,
    getDetails
};