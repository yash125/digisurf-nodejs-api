'use strict'

const db = require('../mongodb');
const tablename = 'resetPasswordCode';


function SelectOne(data, callback) {
    db.get().collection(tablename)
        .findOne(data, { sort: { timeStamp: -1 } }, (err, result) => {
            return callback(err, result);
        });
}
function Insert(data, callback) {
    db.get().collection(tablename)
        .insert(data, (err, result) => {
            return callback(err, result);
        });
}
function SelectWithSort(data, sortBy = {}, porject = {}, skip = 0, limit = 20, callback) {
    db.get().collection(tablename)
        .find(data)
        .sort(sortBy)
        .project(porject)
        .skip(skip)
        .limit(limit)
        .toArray((err, result) => {
            return callback(err, result);
        });

}
module.exports = { Insert, SelectOne, SelectWithSort };