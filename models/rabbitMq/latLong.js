const path = require('path');
const ipc = require('node-ipc');
const fork = require('child_process').fork;
let logger = require('winston');
const cluster = require('cluster');
ipc.config.id = 'rabbitmqserverIptoLatLong';
ipc.config.silent = true;
var cpus = {};




function InsertQueue(channel, queue, data, callback) {

    if (channel) {
        channel.assertQueue(queue, { durable: false }, function () {
            channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
            if (!cpus[cluster.worker.id]) {
                cpus[cluster.worker.id] = true;
                startIPCServerForRabbitMQWorker(data);
                callback(null,true);
            }

        });
    }
    else {
        startIPCServerForNormalWorker(data);
        callback(null,true);
    }
}


function startIPCServerForRabbitMQWorker() {


    var file = path.join(__dirname, '../../worker/ipToLatLong/worker.js');
    fork(file);


    ipc.serve(() => {
        ipc.server.on('consumer.connected', (data) => {
            logger.info('RabbitMQ consumer is connected and ready to execute', data);
        });

        ipc.server.on('message.consumed', (data) => {
            logger.info('RabbitMQ message consumed callback', data);


        });

        ipc.server.on('consumer.exiting', (data) => {
            logger.info('RabbitMQ message consumer exiting', data);
            ipc.server.stop();
        });

    });

    try {
        ipc.server.start();
    } catch (err) {
        logger.error(err);

    }

}




module.exports = { InsertQueue };