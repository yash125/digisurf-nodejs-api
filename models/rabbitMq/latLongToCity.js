const path = require('path');
const ipc = require('node-ipc');
const fork = require('child_process').fork;
let logger = require('winston');

const cluster = require('cluster');
ipc.config.id = 'rabbitmqserverLatLongToCity';
ipc.config.silent = true;
var cpus = {};




function InsertQueue(channel, queue, data, callback) {

   
    if (channel) {
        channel.assertQueue(queue, { durable: false }, function (err, queueList) {

            channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
            if (!cpus[cluster.worker.id]) {
                logger.info("in IF");
                cpus[cluster.worker.id] = true;
                startIPCServerForRabbitMQWorker(data);
            } else {
                logger.info("in ELSE");
            }
callback(err, queueList);
        });
    }
   
}


function startIPCServerForRabbitMQWorker() {

    var file = path.join(__dirname, '../../worker/latLongToCity/worker.js');
    fork(file);

    ipc.serve(() => {
        ipc.server.on('consumer.connected', (data) => {
            logger.info('RabbitMQ consumer is connected and ready to execute', data);
        });

        ipc.server.on('message.consumed', (data) => {
            logger.info('RabbitMQ message consumed callback', data);

            
        });

        ipc.server.on('consumer.exiting', (data) => {
            logger.info('RabbitMQ message consumer exiting', data);
            ipc.server.stop();
        });

    });

    try {
        ipc.server.start();
    } catch (err) {
        logger.error(err);

    }

}




module.exports = { InsertQueue };