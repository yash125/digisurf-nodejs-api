'use strict'
/** 
 * This const requires the mongo url
 * @const
 * @requires module:config 
 */
const url = require('../../config/components/rebitMQ');
const logger = require('winston');

// ipc.config.id = 'rabbitmqworker';
// ipc.config.silent = true;


//For the RabbitMQ connection
// const amqp = require('amqplib/callback_api');
const amqp = require('amqplib');

//For the connection the amqp server
//const amqpConn = null;

//For the chhanel corresponding to the high traffic apis
let channelEmail = null;
let channelLatLong = null;
let channelSearchResult = null;
let channelSMS = null;
let channelLatLongToCity = null;
//let channelTwilio = null;

//For the queues corresponding to the high traffic apis
let queueEmail = 'QueueEmail';
let queueLatLong = 'QueueLatLong';
let queueSearchResult = 'QueueSearchResult';
let queueSMS = 'QueueSMS';
let queueLatLongToCity = 'QueueLatLongToCity';


//For the threshold of messages that can be executed by one worker corresponding to the high traffic api call
let thresholdEmail = 100;
let thresholdLatLong = 100;
let thresholdSearchResult = 100;
let thresholdSMS = 100;
let thresholdLatLongToCity = 100;



//set up a connection
/**
 * @amqpConn will hold the connection and channels will be set up in the connection.
 */
var state = { amqpConn: null };


/**
 * Method to connect to the mongodb
 * @param {*} url
 * @returns connection object
 */
exports.connect = (callback) => {
    if (state.amqpConn) return callback();
    amqp.connect(url + "?heartbeat=60", (err, conn) => {
        if (err) {
            logger.info("[AMQP]", err.message);
            //return setTimeout(connectRabbitMQServer, 1000);
            return callback(err);
        }
        conn.on("error", (err) => {
            if (err.message !== "Connection closing") {
                logger.error("[AMQP] conn error", err.message);
                return callback(err);
            }
        });
        conn.on("close", () => {
            logger.info("[AMQP] reconnecting");
            //return setTimeout(connectRabbitMQServer, 1000);
        });
        logger.info("[AMQP] connected");
        state.amqpConn = conn;

        preparePublisher();
        return callback();

    });

}

/**
 * Method to get the connection object of the mongodb
 * @returns db object
 */
exports.get = () => {
    return state.amqpConn;
}


function preparePublisher() {


    channelEmail = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });

        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });

    });

    channelLatLong = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });

        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });

    });
    channelSearchResult = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });

        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });

    });

    channelSMS = state.amqpConn.createChannel((err, ch) => {

        if (closeOnErr(err)) return;

        ch.on("error", (err) => {
            logger.error("[AMQP] channel error", err.message);
        });

        ch.on("close", () => {
            logger.info("[AMQP] channel closed");
        });

    });

    channelLatLongToCity = state.amqpConn.createChannel((err, ch) => {
        
                if (closeOnErr(err)) return;
        
                ch.on("error", (err) => {
                    logger.error("[AMQP] channel error", err.message);
                });
        
                ch.on("close", () => {
                    logger.info("[AMQP] channel closed");
                });
        
            });

}

function closeOnErr(err) {

    if (!err) return false;

    logger.error("[AMQP] error", err);
    state.amqpConn.close();
    return true;
}


exports.getChannelEmail = () => {
    return channelEmail;
}
exports.getChannelLatLong = () => {
    return channelLatLong;
}
exports.getChannelSearchResult = () => {
    return channelSearchResult;
}
exports.getChannelSMS = () => {
    return channelSMS;
}
exports.getChannelLatLongToCity = () => {
    return channelLatLongToCity;
}


exports.queueEmail = queueEmail;
exports.thresholdEmail = thresholdEmail;

exports.queueLatLong = queueLatLong;
exports.thresholdLatLong = thresholdLatLong;

exports.queueSearchResult = queueSearchResult;
exports.thresholdSearchResult = thresholdSearchResult;

exports.queueSMS = queueSMS;
exports.thresholdSMS = thresholdSMS;

exports.queueLatLongToCity = queueLatLongToCity;
exports.thresholdLatLongToCity = thresholdLatLongToCity;