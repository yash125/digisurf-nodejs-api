'use strict'
const db = require('../mongodb')

const tablename = "collection";

function Insert(data, callback) {
    db.get().collection(tablename)
        .insert(data, (err, result) => {
            return callback(err, result);
        });
}

function Update(condition,data,callback){
    db.get().collection(tablename)
        .updateOne(condition, { $set: data },(err,result)=>{
            return callback(err,result);
        })
}
function UpdateToArray(condition,data,callback){
    db.get().collection(tablename)
        .updateOne(condition,  { $addToSet:{postIds:{$each:data} } },(err,result)=>{
            return callback(err,result);
        })
}


function Delete(condition,callback){
    db.get().collection(tablename)
        .remove(condition,(err,result)=>{
            return callback(err,result);
        })
}


function Select(condition,callback){
    db.get().collection(tablename)
        .findOne(condition,(err,result)=>{
            return callback(err,result);
        })
}

function Aggregate(condition, callback) {
    db.get().collection(tablename)
        .aggregate(condition, (err, result) => {
            return callback(err, result);
        });
}

function SelectAll(condition,offset,limit,callback){
    db.get().collection(tablename)
        .find(condition)
        .skip(offset)
        .limit(limit)
        .toArray((err, result) => {
            return callback(err, result);
        });
}
function DeleteFromArray(condition,data,callback){
    db.get().collection(tablename)
        .update(condition,  { $pull:{postIds:data } },{multi: true},(err,result)=>{
            return callback(err,result);
        })
}
module.exports = {
    Insert,Update,Delete,Select,UpdateToArray,Aggregate,SelectAll,DeleteFromArray
};