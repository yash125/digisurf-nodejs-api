'use strict'
const elasticClient = require('../elasticSearch');
const tablename = 'postList';
const indexName = "posts";
const version = 382;


function Select(data, callback) {
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            "query": {
                "match": data
            }
        }
    }, function (err, result) {
        callback(err, result);
    });
}
function SelectWithSort(condition, skip, limit, sort, callback) {

    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            from: skip, size: limit,
            "query": {
                "bool": {
                    "must": [{ "match": { "postStatus": 1 } }],
                    "should": condition
                }
            }
            , "sort": [{ "timeStamp": "desc" }]
        }
    }, function (err, result) {
        callback(err, result);
    });
}

function getPostDetails(condition, skip, limit, followAry, userId, callback) {

    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            from: skip, size: limit,
            "query": {
                "bool": condition
            }
            , "sort": [{ "timeStamp": "desc" }]
        }

    }, function (err, result) {
        if (result && result.hits && result.hits.hits) {
            result = (result.hits && result.hits.hits) ? result.hits.hits : [];
            result.forEach(function (element) {
                element["_source"]["_id"] = element["_id"]
                element = element["_source"]
                element["distinctViews"] = (element["distinctViews"]) ? element["distinctViews"].length : 0;
                element["totalComments"] = (element["commentCount"]) ? element["commentCount"] : 0;
                element["profilePic"] = element["profilepic"];
                element["comments"] = (element["comments"]) ? element["comments"].slice(element["comments"].length - 3).reverse() : [];
                element["hashTags"] = [];
                element["totalViews"] = [];
                element["mentionedUsers"] = [];
                element["firstName"] = (element["firstName"] != "null" && element["firstName"] != null && typeof element["firstName"] != 'undefined') ? element["firstName"] : "";
                element["lastName"] = (element["lastName"] != "null" && element["lastName"] != null && typeof element["lastName"] != 'undefined') ? element["lastName"] : "";
                element["postId"] = element._id
                if (element.musicId == "") delete element.musicData
                element["postId"] = element._id
                element["followStatus"] = (followAry && followAry.map(e => e.toString()).includes(element["userId"])) ? 1 : 0;
                element["liked"] = (element["likes"]) ? element["likes"].map(id => id.toString()).includes(userId.toString()) : false;
                element["likesCount"] = (element["likes"]) ? element["likes"].length : 0;
                element["orientation"] = element.orientation || 0;
                delete element["likes"];
                element["place"] = (element["place"]) ? element["place"].split(",")[0] : "";
                element["isStar"] = (element && element["isStar"]) ? element["isStar"] : false;
            }, this);
            result = result.map(e => e._source);
            return callback(err, result);
        } else {

            return callback(err, []);
        }
    });
}

function getPostDetailsWithTotalNumbers(condition, skip, limit, followAry, userId, callback) {

    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            from: skip, size: limit,
            "query": {
                "bool": condition
            }
            , "sort": [{ "timeStamp": "desc" }]
        }
    }, function (err, result) {

        if (result && result.hits && result.hits.hits) {

            let total = (result.hits && result.hits.total) ? result.hits.total : 0;
            result = (result.hits && result.hits.hits) ? result.hits.hits : [];
            result.forEach(function (element) {
                element["_source"]["_id"] = element["_id"]
                element = element["_source"]
                element["distinctViews"] = (element["distinctViews"]) ? element["distinctViews"].length : 0;
                element["totalComments"] = (element["commentCount"]) ? element["commentCount"] : 0;
                element["profilePic"] = element["profilepic"];
                element["comments"] = (element["comments"]) ? element["comments"].slice(element["comments"].length - 3).reverse() : [];
                element["hashTags"] = [];
                element["totalViews"] = [];
                element["mentionedUsers"] = [];
                element["firstName"] = (element["firstName"] != "null" && element["firstName"] != null && typeof element["firstName"] != 'undefined') ? element["firstName"] : "";
                element["lastName"] = (element["lastName"] != "null" && element["lastName"] != null && typeof element["lastName"] != 'undefined') ? element["lastName"] : "";
                element["postId"] = element._id
                if (element.musicId == "") delete element.musicData
                element["postId"] = element._id
                element["followStatus"] = (followAry && followAry.map(e => e.toString()).includes(element["userId"])) ? 1 : 0;
                element["liked"] = (element["likes"]) ? element["likes"].map(id => id.toString()).includes(userId.toString()) : false;
                delete element["likes"];
                element["orientation"] = element.orientation || 0;
                element["place"] = (element["place"]) ? element["place"].split(",")[0] : "";
                element["isStar"] = (element && element["isStar"]) ? element["isStar"] : false;
            }, this);
            result = result.map(e => e._source);
            return callback(err, { result, total });
        } else {
            return callback(err, []);
        }

    });
}
function SelectAll(callback) {
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: {
            from: 0, size: 200,
            "query": {
                "match_all": {}
            }
        }
    }, function (err, result) {
        callback(err, result);
    });
}

function CustomeSelect(data, followAry, userId, callback) {
    elasticClient.get().search({
        index: indexName,
        type: tablename,
        body: data
    }, function (err, result) {

        if (result && result.hits && result.hits.hits) {

            result = (result.hits && result.hits.hits) ? result.hits.hits : [];
            result.forEach(function (element) {
                element["_source"]["_id"] = element["_id"]
                element = element["_source"]
                element["distinctViews"] = (element["distinctViews"]) ? element["distinctViews"].length : 0;
                element["totalComments"] = (element["commentCount"]) ? element["commentCount"] : 0;
                element["profilePic"] = element["profilepic"];
                element["comments"] = (element["comments"]) ? element["comments"].slice(element["comments"].length - 3).reverse() : [];
                element["hashTags"] = [];
                element["totalViews"] = [];
                element["mentionedUsers"] = [];
                element["firstName"] = (element["firstName"] != "null" && element["firstName"] != null && typeof element["firstName"] != 'undefined') ? element["firstName"] : "";
                element["lastName"] = (element["lastName"] != "null" && element["lastName"] != null && typeof element["lastName"] != 'undefined') ? element["lastName"] : "";
                element["postId"] = element._id
                if (element.musicId == "") delete element.musicData
                element["postId"] = element._id
                element["orientation"] = element.orientation || 0;
                element["followStatus"] = (followAry && followAry.map(e => e.toString()).includes(element["userId"])) ? 1 : 0;
                element["liked"] = (element["likes"]) ? element["likes"].map(id => id.toString()).includes(userId.toString()) : false;
                element["isStar"] = (element && element["isStar"]) ? element["isStar"] : false;
                delete element["likes"];
            }, this);
            result = result.map(e => e._source);
            return callback(err, result);
        } else {

            return callback(err, []);
        }
    });
}
function Insert(_id, data, callback) {
    elasticClient.get().index({
        index: indexName,
        type: tablename,
        id: _id,
        body: data
    }, (err, result) => {
        callback(err, result);
    });
}

function UpdateWithIncrease(_id, field, value, callback) {

    elasticClient.get().update({
        index: indexName,
        type: tablename,
        id: _id,
        retry_on_conflict: 5,
        body: {
            "script": `ctx._source.${field}+=${value}`
        }

    }, (err, results) => {
        callback(err, results)
    })
}
function UpdateWithPush(_id, field, value, callback) {

    elasticClient.get().update({
        index: indexName,
        type: tablename,
        id: _id,
        retry_on_conflict: 5,
        body: {
            "script": "ctx._source." + field + ".add('" + value + "')"
        }

    }, (err, results) => {
        callback(err, results)
    })
}
function UpdateWithPull(_id, field, value, callback) {

    elasticClient.get().update({
        index: indexName,
        type: tablename,
        id: _id,
        retry_on_conflict: 5,
        body: {
            "script": "ctx._source." + field + ".remove(ctx._source." + field + ".indexOf('" + value + "'))"
        }
    }, (err, results) => {
        callback(err, results);
    })
}
function Update(_id, data, callback) {

    elasticClient.get().update({
        index: indexName,
        type: tablename,
        id: _id,
        retry_on_conflict: 5,
        body: {
            doc: data,
            doc_as_upsert: true
        }
    }, (err, results) => {
        callback(err, results)
    })
}
function updateByQueryMultiple(condition, updateScript, callback) {
    // "script" : {
    //     "inline" : "ctx._source.item_name= 'new_name'; ctx._source.item_price= 10000;", 
    //     "lang"   : "painless"
    //     }

    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        conflicts: "proceed",
        body: {
            "query": {
                "bool": {
                    "filter": {
                        "term": condition
                    }
                }
            },
            "script": updateScript
        }
    }, (err, results) => {
        callback(err, results)
    })

}


function updateByQuery(condition, fieldName, fieldValue, callback) {

    var theScript = {
        "source": "ctx._source." + fieldName + "='" + fieldValue + "'", "lang": "painless"
    }

    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        conflicts: "proceed",
        body: {
            query: { "match": condition },
            "script": theScript
        }
    }, (err, results) => {
        callback(err, results)
    })
}


function removeField(_id, fieldName, callback) {
    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {
            "script": `ctx._source.remove("${fieldName}")`,
            "query": {
                "bool": {
                    "must": [
                        {
                            "exists": {
                                "field": `${fieldName}`
                            }
                        }
                    ]
                }
            }
        }
    }, (err, results) => {
        callback(err, results)
    })
}
function setOffline(timestamp, callback) {
    elasticClient.get().updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {
            query: {
                "range": {
                    "lastOnline": {
                        "lt": timestamp
                    }
                },
                // "match":{"onlineStatus":1}
            },
            "script": { "source": "ctx._source.onlineStatus=0" }
        }
    }, (err, results) => {
        callback(err, results)
    })
}
function Delete(tablename, condition, callback) {
    elasticClient.get().deleteByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: {
            query: {
                match: condition
            }
        }
    }, (err, results) => {
        callback(err, results)
    })
}
function Bulk(dataInArray, callback) {
    elasticClient.get().bulk({
        body: dataInArray
    }, (err, results) => {
        callback(err, results)
    })
}
function updateByQueryWithArray(condition, callback) {
    elasticClient.updateByQuery({
        index: indexName,
        type: tablename,
        version: version,
        body: condition
    }, (err, results) => {
        callback(err, results)
    })
}

module.exports = {
    updateByQueryWithArray, setOffline, Bulk, removeField,
    CustomeSelect, Select, Insert, Update, updateByQuery, Delete,
    UpdateWithPush, UpdateWithPull, SelectAll, SelectWithSort,
    getPostDetails, UpdateWithIncrease, updateByQueryMultiple,
    getPostDetailsWithTotalNumbers
};
