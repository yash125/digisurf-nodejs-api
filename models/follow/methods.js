'use strict'
const db = require('../mongodb');
const ObjectID = require('mongodb').ObjectID;

const tablename = "follow";

function Select(data, callback) {
    db.get().collection(tablename)
        .find(data)
        .toArray((err, result) => {
            return callback(err, result);
        });
}
function GetFolloweeCount(userId, callback) {
    let condition = [{ "$match": { "follower": userId, "end": false, "type.status": 1 } },
    { "$match": { "followee": { "$ne": userId } } },
    { "$lookup": { "from": 'customer', "localField": 'followee', "foreignField": '_id', "as": 'user' } },
    { "$unwind": "$user" },
    { "$match": { "user.userStatus": 1 } },
    {
        "$group": {
            "_id": null,
            "total": { $sum: 1 }
        }
    }];

    db.get().collection(tablename)
        .aggregate(condition, (err, result) => {
            return callback(err, result);
        });
}


function GetFollowerCount(userId, callback) {
    let condition = [{ "$match": { "followee": userId, "end": false, "type.status": 1 } },
    { "$match": { "follower": { "$ne": userId } } },
    { "$lookup": { "from": 'customer', "localField": 'follower', "foreignField": '_id', "as": 'user' } },
    { "$unwind": "$user" },
    { "$match": { "user.userStatus": 1 } },
    {
        "$group": {
            "_id": null,
            "total": { $sum: 1 }
        }
    }];

    db.get().collection(tablename)
        .aggregate(condition, (err, result) => {
            return callback(err, result);
        });
}
function GetCount(data, callback) {
    db.get().collection(tablename)
        .find(data)
        .count((err, result) => {
            return callback(err, result);
        });
}
function matchList(data, callback) {
    db.get().collection(tablename)
        .aggregate(data, (err, result) => {
            return callback(err, result);
        });
}

function SelectOne(data, callback) {
    db.get().collection(tablename)
        .findOne(data, (err, result) => {
            return callback(err, result);
        });
}


function SelectById(condition, requiredFeild, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .findOne(condition, requiredFeild, ((err, result) => {
            return callback(err, result);
        }));
}


function Insert(data, callback) {
    db.get().collection(tablename)
        .insert(data, (err, result) => {
            return callback(err, result);
        });
}


function FollowerBulkInsert(data, callback) {

    var bulk = db.get().initializeOrderedBulkOp();
    bulk.find({
        follower: data.userId,
        followee: { $in: data.followeeId }
    }).upsert().update({
        $set: {
            follower: data.userId,
            start: Date.now(),
            end: false,
            type: {
                status: 1,
                message: "started following"
            }
        }
    });

    bulk.execute(function (err, updateResult) {
        return callback(err, updateResult);
    });
}

function Update(condition, data, callback) {
    db.get().collection(tablename)
        .update(condition, { $set: data }, { multi: true }, (err, result) => {
            return callback(err, result);
        });
}

function UpdateWithUpsert(condition, data, callback) {
    db.get().collection(tablename)
        .update(condition, { $set: data }, { upsert: true }, (err, result) => {
            return callback(err, result);
        });
}


function UpdateById(_id, data, callback) {
    db.get().collection(tablename)
        .update({ _id: ObjectID(_id) }, { $set: data }, (err, result) => {
            return callback(err, result);
        });
}

function UpdateByIdWithAddToSet(condition, data, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .update(condition, { $addToSet: data }, (err, result) => {
            return callback(err, result);
        });
}

function UpdateByIdWithPush(condition, data, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .update(condition, { $push: data }, (err, result) => {
            return callback(err, result);
        });
}
function UpdateByIdWithPull(condition, data, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .update(condition, { $pull: data }, (err, result) => {
            return callback(err, result);
        });
}

function Delete(condition, callback) {
    db.get().collection(tablename)
        .remove(condition, (err, result) => {
            return callback(err, result);
        });
}
function Aggregate(condition, callback) {
    db.get().collection(tablename)
        .aggregate(condition, (err, result) => {
            return callback(err, result);
        });
}


module.exports = {
    Aggregate, FollowerBulkInsert, UpdateWithUpsert, GetCount, GetFollowerCount, GetFolloweeCount,
    Select, matchList, SelectOne, Insert, Update, SelectById, UpdateById,
    Delete, UpdateByIdWithAddToSet, UpdateByIdWithPush, UpdateByIdWithPull
};