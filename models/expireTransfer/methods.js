"use strict";
const Promise = require("bluebird");
const { ObjectID } = require('mongodb');
const _ = require('lodash');

const mqtt = Promise.promisifyAll(require('./../../library/mqtt'));
const RUCWallet = Promise.promisifyAll(require("../RUCWallet"));
const messages = Promise.promisifyAll(require("../messages"));
const customerWallet = Promise.promisifyAll(require("../customerWallet"));
const transfers = Promise.promisifyAll(require("../transfers"));
const userList = Promise.promisifyAll(require('../userList'));
var gRPC_Email_service = require("./../../library/gRPC/Email");

var moment = require('moment');
var now = moment();

async function expireTransfer(transferId) {
  try {

    

    var transfer = await transfers.SelectOneAsync({ _id: ObjectID(transferId) });

    if (transfer && transfer.currentTransferStatus == "pending") {
    

      var currentBalance = await RUCWallet.SelectOneAsync({ _id: ObjectID(transfer.senderId) });

      var currentClosingBalance = Number(
        Number(currentBalance.RUCs.RUC) + Number(transfer.Amount)
      );

     

      var transferData = {
        currentTransferStatus: "expired",
        isExpired: true
      };

      var messageData = {
        transferStatus: 3,
        transferStatusText: "timeout"
      };

      var customerWalletData = {
        txnId: "TXN-ID-" + new Date().getTime().toString(),
        userId: ObjectID(transfer.senderId),
        txnType: "CREDIT",
        txnTypeCode: 1,
        trigger: "Payment Refund",
        currency: process.env.CURRENCY_TYPE,
        currencySymbol: process.env.CURRENCY_TYPE,
        coinType: process.env.CURRENCY_TYPE,
        coinOpeingBalance: currentBalance.RUCs.RUC,
        cost: 0,
        coinAmount: transfer.Amount,
        coinClosingBalance: currentClosingBalance,
        paymentType: process.env.CURRENCY_TYPE,
        timestamp: new Date().getTime(),
        paymentTxnId: ObjectID(transferId),
        initatedBy: "customer"
      };


      let isRecoredFound = await customerWallet.SelectOneAsync({ paymentTxnId: ObjectID(transferId), txnTypeCode: 1 });
      if (!isRecoredFound) {
        let createdCustomerWallet = await customerWallet.InsertAsync(customerWalletData);

        await transfers.UpdateAsync({ _id: ObjectID(transferId) }, transferData);

        await messages.UpdateAsync({ _id: ObjectID(transfer.messageId) }, messageData);

        var expiredMessage = await messages.SelectOneAsync({ _id: ObjectID(transfer.messageId) });
        var sender = await userList.SelectByIdAsync({ _id: ObjectID(expiredMessage.senderId) }, {});
        var receiver = await userList.SelectByIdAsync({ _id: ObjectID(expiredMessage.receiverId) }, {});
        let mqttMessage = _.pick(expiredMessage, ['messageId', 'chatId', 'secretId', 'dTime', 'amount', 'messageType', 'timestamp',
          'expDtime', 'userImage', 'toDocId', 'transferStatus', 'transferStatusText', 'name', 'members']);

        mqttMessage.type = expiredMessage.messageType.toString();
        mqttMessage.amount = mqttMessage.amount.toString();
        mqttMessage.transferStatus = mqttMessage.transferStatus.toString();
        mqttMessage.receiverIdentifier = sender.mobile;
        mqttMessage.from = expiredMessage.senderId.toString();
        mqttMessage.to = expiredMessage.receiverId.toString();
        mqttMessage.id = expiredMessage._id.toString();

        await mqtt.publishAsync("Message/" + mqttMessage.from, JSON.stringify(mqttMessage), { qos: 1 });
        await mqtt.publishAsync("Message/" + mqttMessage.to, JSON.stringify(mqttMessage), { qos: 1 });

        let updatedBalance = await RUCWallet.UpdateAsync({ _id: ObjectID(transfer.senderId) }, { "RUCs.RUC": createdCustomerWallet.ops[0].coinClosingBalance });
        return updatedBalance
      }
      var expiredMessage = await messages.SelectOneAsync({ _id: ObjectID(transfer.messageId) });
      var sender = await userList.SelectOneAsync({ _id: ObjectID(expiredMessage.senderId) });
      var receiver = await userList.SelectOneAsync({ _id: ObjectID(expiredMessage.receiverId) });


      try {
        gRPC_Email_service.sendEmail({
          userName: sender.userName,
          trigger: "Money transfer expired",
          to: sender.email.id,
          subject: `Your money transfer has expired.`,
          body: '<p> Hello ' + sender.userName + ',' + '<br/><br/>Your money transfer to ' + receiver.userName + ' has expired.' + 'The details are as follows:' + '<br/>AMOUNT:' + process.env.CURRENCY_TYPE + ' ' + transfer.Amount + '<br/>TRANSACTION ID: ' + customerWalletData.paymentTxnId + '<br/>RECEIVER NAME: ' + receiver.userName + '<br/>RECEIVER PHONE: ' + receiver.mobile + '<br/>EXPIRY DATE & TIME: ' + now.format("Do MMMM YYYY, h:mm A") + '<br/>Please do contact our support team in case you dont recognize this transfer. ' + '<br/><br/><br/>Cheers, <br/> Team ' + process.env.APP_NAME + '.</p>'
        });

      } catch (error) {
        console.log("error : KOKKOKSW ", error)
      }



      try {
        let receiverData = {
          userName: receiver.userName,
          trigger: "Money transfer expired",
          to: receiver.email.id,
          subject: `Money transfer expired.`,
          body: '<p> Hello ' + receiver.userName + ',' + '<br/><br/>Money transfer from ' + sender.userName + ' has expired.' + 'The details are as follows:' + '<br/>AMOUNT:' + process.env.CURRENCY_TYPE + ' ' + transfer.Amount + '<br/>TRANSACTION ID: ' + customerWalletData.paymentTxnId + '<br/>SENDER NAME: ' + sender.userName + '<br/>SENDER PHONE: ' + sender.mobile + '<br/>EXPIRY DATE & TIME: ' + now.format("Do MMMM YYYY, h:mm A") + '<br/>Please do contact our support team in case you dont recognize this transfer. ' + '<br/><br/><br/>Cheers, <br/> Team ' + process.env.CURRENCY_TYPE + '.</p>'
        }
        gRPC_Email_service.sendEmail(receiverData)

      } catch (error) {
        console.log("error : KOKAAKOKSW ", error)
      }

    } else {
      return true;
    }

  } catch (error) {
    console.log("expire error", error);
    throw error;
  }
}

module.exports = expireTransfer;