"use strict";

const db = require("../mongodb");
const tablename = "customer";

let insertDoc = (payload, callback) => {
  return new Promise(() => {
    db.get()
      .collection(tablename)
      .insert({
        _id: payload._id,
        mobile: payload.number.replace(payload.countryCode, ""),
        countryCode: payload.countryCode,
        password: payload.password,
        password_original: payload.password_original,
        originalPassword: payload.password_original,
        userName: payload.userName.trim().toLowerCase(),
        firstName: payload.firstName,
        lastName: payload.lastName,
        "email": payload.email.toLowerCase(),
        profilePic: payload.profilePic,
        profileCoverImage: payload.profileCoverImage,
        qrCode: payload.qrCode,
        referredby: payload.userReferralCode,
        referralCode: payload.referralCode,
        private: payload.isPrivate,
        accessKey: payload.accessKey,
        "type": "user",
        "userStatus": 1,
        "statusBio": payload.statusBio,
        registeredOn: new Date().getTime(),
        creationDate: new Date(),
        countryName: payload.countryName,

        "googleId": payload.googleId,
        "facebookId": payload.facebookId,
        "appleId": payload.appleId,
        "loginType": payload.loginType,

        deviceInfo: {
          "userId": payload._id,
          "deviceName": payload.deviceName,
          "deviceOs": payload.deviceOs,
          "modelNumber": payload.modelNumber,
          "deviceType": payload.deviceType,
          "appVersion": payload.appVersion,
          "deviceId": payload.deviceId,
          "timestamp": new Date().getTime(),
          "creationDate": new Date()
        },

        "friendList": [],
        "follow": [],
        "name": payload.firstName + " " + payload.lastName,
        "userType": payload.userType,
        "userTypeText": payload.userTypeText,
        "role": payload.role,
        "currencySymbol": "",
        "currencyCode": "",
        "loginTypeText": "",
        "mqttTopic": payload["_id"].toString(),
        "fcmTopic": payload["_id"].toString(),
        "promoterAffiliateId": "",
        "lastLogin": "",
        "activeDeviceData": payload["activeDeviceData"],
        "user_id": payload["_id"].toString(),
        "first_time_login": new Date().getTime(),
        "stripe_customer_id": "",
        "updated_at": new Date().getTime(),
        "password_reset_token": "",
        "registration_step": "",
        "trial_status": "",
        "remember_token": "",
        "password_reset_token_created_on": "",

        "storeId": "0",
        "userType": 1,
        "userTypeText": "Retailer",
        "userId": payload["_id"].toString(),
        "isStoreCustomer": 0,
        "isRegisteredCustomer": 1,
        "userType": 1,
        "userTypeText": "Retailer",
        "customerType": 1,
        "customerTypeText": "B2C customer",
        "status": 1,
        "statusMsg": "approved & active",
        "statusLogs": [
          {
            "action": "approved & active",
            "status": 0,
            "actionByUserType": "self entry user",
            "actionByUserId": "0",
            "timestamp": new Date().getTime() / 1000
          },
          {
            "action": "approved & active",
            "status": 1,
            "reason": "",
            "actionByUserType": "admin",
            "actionByUserId": "0",
            "timestamp": new Date().getTime() / 1000,
            "isoDate": new Date().getTime()
          }
        ],
        "coordinates": {
          "longitude": 0,
          "latitude": 0
        },
        "termsAndCondition": 1,
        "identityCard": {
          "url": "",
          "verified": false
        },
        "mmjCard": {
          "url": "",
          "verified": false
        },
        "createdDate": new Date().getTime() / 1000,
        "createdTimestamp": new Date().getTime(),
        "createdISOdate": new Date(),
        "seqId": payload.seqId || 1,
        "mobileDevices": {
          "appVersion": "0.0",
          "browserName": "3",
          "browserVersion": "",
          "currentlyActive": true,
          "deviceId": "0",
          "deviceOsVersion": "0",
          "deviceType": 3,
          "deviceTypeMsg": "web",
          "lastISOdate": new Date(),
          "lastLogin": new Date().getTime() / 1000,
          "lastTimestamp": new Date().getTime(),
          "pushToken": ""
        },
        cityName: "",
        "lastStatusLog": {
          "action": "approved & active",
          "status": 1,
          "reason": "",
          "actionByUserType": "admin",
          "actionByUserId": "0",
          "timestamp": new Date(),
          "isoDate": new Date().getTime()
        },
        "statusUpdatedOn": new Date().getTime() / 1000

      }, (err, result) => {
        return callback(err, result);
      })

  })
}

let verifyUniqueness = (data, callback) => {

  db.get()
    .collection(tablename)
    .find({ "status": 1, $or: [{ "email.id": data.email }, { userName: data.userName }, { mobile: data.countryCode + data.number }] })
    .toArray((err, result) => {
      return callback(err, result);
    });
}
/* let checkPhoneNumber = (data, callback) => {

  db.get()
    .collection(tablename)
    .find({ number: data.countryCode + data.number } )
    .toArray((err, result) => {
      logger.info(result)
      return callback(err, result);
    });
} */
/* let checkUser = (data, callback) => {
  let condition = {};
  condition = { "$or": [{ userName: data.userName }, { number: data.number }] }
  logger.info('iiiiiiiii',condition);
  db.get()
    .collection(tablename)
    .find(condition)
    .toArray((err, result) => {
      return callback(err, result);
    });
} */
let checkUser = (data, callback) => {

  db.get()
    .collection(tablename)
    .find(data)
    .toArray((err, result) => {
      return callback(err, result);
    });
}

let addAccessCode = (data, callback) => {

  db.get()
    .collection(tablename)
    .updateOne({ _id: data._id }, { $set: { accessKey: data.accessKey, type: data.type, userStatus: data.userStatus } }, (err, result) => {
      return callback(err, result.result);
    })
}

module.exports = {
  insertDoc,
  verifyUniqueness,
  checkUser,
  addAccessCode
  // checkPhoneNumber
};