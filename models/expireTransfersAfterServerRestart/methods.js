"use strict";
const Promise = require("bluebird");
const { ObjectID } = require('mongodb');
const _ = require('lodash');
const mqtt = Promise.promisifyAll(require('./../../library/mqtt'));
const RUCWallet = Promise.promisifyAll(require("../RUCWallet"));
const messages = Promise.promisifyAll(require("../messages"));
const userList = Promise.promisifyAll(require("../userList"));
const customerWallet = Promise.promisifyAll(require("../customerWallet"));
const transfers = Promise.promisifyAll(require("../transfers"));

async function restoreExpiryToRedis() {
  try {
    let today = new Date().getTime();

    let transferList = await transfers.SelectAsync({ $and: [{ expiresOn: { $lt: today } }, { currentTransferStatus: "pending" }, { isExpired: false }] });

    // let transferIds = transferList.map((transfer) => {return { _id: transfer._id, differenceOfSecounds: Math.round((transfer.expiresOn-today)/1000)} });

    transferList.forEach(async (transfer) => {
      if (transfer.currentTransferStatus == "pending") {
        var currentBalance = await RUCWallet
          .SelectOneAsync({ _id: ObjectID(transfer.senderId) });

        var currentClosingBalance = Number(
          Number(currentBalance.RUCs.RUC) + Number(transfer.Amount)
        );

        var transferData = {
          currentTransferStatus: "expired",
          isExpired: true
        };

        var messageData = {
          transferStatus: 3,
          transferStatusText: "timeout"
        };

        var customerWalletData = {
          txnId: "TXN-ID-" + new Date().getTime().toString(),
          userId: ObjectID(transfer.senderId),
          txnType: "CREDIT",
          txnTypeCode: 1,
          trigger: "Payment Refund",
          currency: process.env.CURRENCY_TYPE,
          currencySymbol: process.env.CURRENCY_TYPE,
          coinType: process.env.CURRENCY_TYPE,
          coinOpeingBalance: currentBalance.RUCs.RUC,
          cost: 0,
          coinAmount: transfer.Amount,
          coinClosingBalance: currentClosingBalance,
          paymentType: process.env.CURRENCY_TYPE,
          timestamp: new Date().getTime(),
          paymentTxnId: transfer._id,
          initatedBy: "customer"
        };

        let isRecoredFound = await customerWallet.SelectOneAsync({ paymentTxnId: ObjectID(transfer._id.toString()), txnTypeCode: 1 });
        if (!isRecoredFound) {
          let createdCustomerWallet = await customerWallet.InsertAsync(customerWalletData);

          await transfers.UpdateAsync({ _id: transfer._id }, transferData);

          await messages.UpdateAsync({ _id: ObjectID(transfer.messageId) }, messageData);

          let expiredMessage = await messages.SelectOneAsync({ _id: ObjectID(transfer.messageId) });
          let sender = await userList.SelectByIdAsync({ _id: expiredMessage.senderId }, {});
          let mqttMessage = _.pick(expiredMessage, ['messageId', 'chatId', 'secretId', 'dTime', 'amount', 'messageType', 'timestamp',
            'expDtime', 'userImage', 'toDocId', 'transferStatus', 'transferStatusText', 'name', 'members']);

          mqttMessage.type = expiredMessage.messageType.toString();
          mqttMessage.amount = mqttMessage.amount.toString();
          mqttMessage.transferStatus = mqttMessage.transferStatus.toString();
          mqttMessage.receiverIdentifier = sender.mobile;
          mqttMessage.from = expiredMessage.senderId.toString();
          mqttMessage.to = expiredMessage.receiverId.toString();
          mqttMessage.id = expiredMessage._id.toString();

          await mqtt.publishAsync("Message/" + mqttMessage.from, JSON.stringify(mqttMessage), { qos: 1 });
          await mqtt.publishAsync("Message/" + mqttMessage.to, JSON.stringify(mqttMessage), { qos: 1 });

          let updatedBalance = await RUCWallet.UpdateAsync({ _id: ObjectID(transfer.senderId) }, { "RUCs.RUC": createdCustomerWallet.ops[0].coinClosingBalance });

          return updatedBalance;
        }
      }
    });
  } catch (error) {
    console.log("expire error", error);
    throw error;
  }
}

module.exports = restoreExpiryToRedis;