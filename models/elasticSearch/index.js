'use strict'

var elasticsearch = require('elasticsearch');
var elasticClient;

var state = {
  connection: null,
}

exports.connect = function (done) {
  if (state.connection) return done()

  elasticClient = new elasticsearch.Client({
    host: process.env.ELASTIC_SEARCH,
    // log: 'info'
  });
  // logger.info("elasticSearch connection successfully established to  http://13.233.64.64.102:9200")
  state.connection = elasticClient
  done()
}

exports.get = function () {
  return state.connection
}
