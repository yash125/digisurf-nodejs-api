'use strict'
const db = require('../mongodb');
const ObjectID = require('mongodb').ObjectID;

const tablename = "posts";

function Select(data, callback) {
    db.get().collection(tablename)
        .find(data)
        .toArray((err, result) => {
            return callback(err, result);
        });
}
function SelectWithProject(data, skip, limit, sortBy, callback) {
    db.get().collection(tablename)
        .find(data)
        .skip(skip)
        .limit(limit)
        .sort(sortBy)
        .toArray((err, result) => {
            return callback(err, result);
        });
}

function GetPostCount(data, callback) {
    db.get().collection(tablename)
        .find(data)
        .count((err, result) => {
            return callback(err, result);
        });
}
function matchList(data, callback) {
    db.get().collection(tablename)
        .aggregate(data, (err, result) => {
            return callback(err, result);
        });
}

function SelectOne(data, callback) {
    db.get().collection(tablename)
        .findOne(data, (err, result) => {
            return callback(err, result);
        });
}


function SelectById(condition, requiredFeild, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .findOne(condition, requiredFeild, ((err, result) => {
            return callback(err, result);
        }));
}


function Insert(data, callback) {
    db.get().collection(tablename)
        .insert(data, (err, result) => {
            return callback(err, result);
        });
}


function Update(condition, data, callback) {
    db.get().collection(tablename)
        .update(condition, { $set: data }, { multi: 1 }, (err, result) => {
            return callback(err, result);
        });
}

function UpdateWithIncrice(condition, data, callback) {
    db.get().collection(tablename)
        .update(condition, { $inc: data }, (err, result) => {
            return callback(err, result);
        });
}

function UpdateById(_id, data, callback) {
    db.get().collection(tablename)
        .update({ _id: ObjectID(_id) }, { $set: data }, (err, result) => {
            return callback(err, result);
        });
}

function UpdateByIdWithAddToSet(condition, data, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .update(condition, { $addToSet: data }, (err, result) => {
            return callback(err, result);
        });
}

function UpdateByIdWithPush(condition, data, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .update(condition, { $push: data }, (err, result) => {
            return callback(err, result);
        });
}
function UpdateByIdWithPull(condition, data, callback) {
    condition["_id"] = ObjectID(condition._id);
    db.get().collection(tablename)
        .update(condition, { $pull: data }, (err, result) => {
            return callback(err, result);
        });
}

function Delete(condition, callback) {
    db.get().collection(tablename)
        .remove(condition, (err, result) => {
            return callback(err, result);
        });
}
function Aggregate(condition, callback) {
    db.get().collection(tablename)
        .aggregate(condition, (err, result) => {
            return callback(err, result);
        });
}

function SelectWithFilter(condition, requiredField, offset, limit, callback) {
    db.get().collection(tablename)
        .find(condition, requiredField)
        .skip(offset)
        .limit(limit)
        .toArray((err, result) => {
            return callback(err, result);
        });
}

function getPostDetails(condition, skip, limit, followAry, userId, callback) {

    db.get().collection(tablename)
        .find(condition)
        .skip(skip)
        .limit(limit)
        .sort({ trending_score: -1 })
        .toArray((err, result) => {
            if (result && result.length) {
                result = (result && result.length) ? result : [];
                result.forEach(function (element) {

                    
                    element["distinctViews"] = (element["distinctViews"]) ? element["distinctViews"].length : 0;
                    element["totalComments"] = (element["commentCount"]) ? element["commentCount"] : 0;
                    element["profilePic"] = element["profilepic"];
                    element["comments"] = (element["comments"]) ? element["comments"].slice(element["comments"].length - 3).reverse() : [];
                    element["hashTags"] = [];
                    element["totalViews"] = [];
                    element["mentionedUsers"] = [];
                    element["firstName"] = (element["firstName"] != "null" && element["firstName"] != null && typeof element["firstName"] != 'undefined') ? element["firstName"] : "";
                    element["lastName"] = (element["lastName"] != "null" && element["lastName"] != null && typeof element["lastName"] != 'undefined') ? element["lastName"] : "";
                    element["postId"] = element._id
                    if (element.musicId == "") delete element.musicData
                    element["postId"] = element._id
                    element["followStatus"] = (followAry && followAry.map(e => e.toString()).includes(element["userId"])) ? 1 : 0;
                    element["liked"] = (element["likes"]) ? element["likes"].map(id => id.toString()).includes(userId.toString()) : false;
                    element["likesCount"] = (element["likes"]) ? element["likes"].length : 0;
                    element["orientation"] = element.orientation || 0;
                    delete element["likes"];
                    element["place"] = (element["place"]) ? element["place"].split(",")[0] : "";
                    element["isStar"] = (element && element["isStar"]) ? element["isStar"] : false;
                }, this);
                
                return callback(err, result);
            } else {
                return callback(err, []);
            }
        });
}

module.exports = {
    SelectWithFilter, getPostDetails,
    Aggregate, UpdateWithIncrice, GetPostCount, SelectWithProject,
    Select, matchList, SelectOne, Insert, Update, SelectById, UpdateById,
    Delete, UpdateByIdWithAddToSet, UpdateByIdWithPush, UpdateByIdWithPull
};