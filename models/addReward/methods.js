"use strict";
const Promise = require("bluebird");
const { ObjectID } = require("mongodb");

const RUCWallet = Promise.promisifyAll(require("../RUCWallet"));
const customerWallet = Promise.promisifyAll(require("../customerWallet"));

async function addRewards(userId, amount, trigger, type) {
  try {
    amount = (amount) ? amount : 0;
    let currentClosingBalance;
    let txnTypeCode;

    if (!ObjectID.isValid(userId)) throw Error("Invalid UserId");

    let currentBalance = await RUCWallet.SelectOneAsync({
      _id: ObjectID(userId)
    });

    if (!currentBalance || currentBalance.RUCs == undefined || Number.isNaN(currentBalance.RUCs.RUC)) {
      currentBalance = { RUCs: { RUC: Number(process.env.RUC_ON_SIGNUP) } };
      amount = process.env.RUC_ON_SIGNUP;
      trigger = process.env.RUC_ON_SIGNUP + process.env.CURRENCY_TYPE + "  credited for signing up on " + process.env.APP_NAME
      await RUCWallet.UpdateWithUpsertAsync({ _id: ObjectID(userId) }, { RUCs: { RUC: Number(process.env.RUC_ON_SIGNUP) } });
    }

    if (currentBalance && currentBalance.RUCs && currentBalance.RUCs.RUC) {
      if (type == "CREDIT") {
        currentClosingBalance = Number(
          Number(currentBalance.RUCs.RUC) + Number(amount)
        );
        txnTypeCode = 1;
      } else if (type == "DEBIT") {
        currentClosingBalance = Number(
          Number(currentBalance.RUCs.RUC) - Number(amount)
        );
        txnTypeCode = 2;
      } else {
        throw Error("Invalid Type");
      }

      let customerWalletData = {
        txnId: "TXN-ID-" + new Date().getTime().toString(),
        userId: ObjectID(userId),
        txnType: type,
        txnTypeCode,
        trigger,
        currency: process.env.CURRENCY_TYPE,
        currencySymbol: process.env.CURRENCY_TYPE,
        coinType: process.env.CURRENCY_TYPE,
        coinOpeingBalance: currentBalance.RUCs.RUC,
        cost: 0,
        coinAmount: amount,
        coinClosingBalance: currentClosingBalance,
        paymentType: process.env.CURRENCY_TYPE,
        timestamp: new Date().getTime(),
        initatedBy: process.env.APP_NAME
      };

      let createdCustomerWallet = await customerWallet.InsertAsync(
        customerWalletData
      );

      await RUCWallet.UpdateAsync(
        { _id: ObjectID(userId) },
        { "RUCs.RUC": createdCustomerWallet.ops[0].coinClosingBalance }
      );

      return "Successfull";
    } else {
      throw Error("No Wallet Found");
    }
  } catch (error) {
    console.log("Reward error", error);
    throw error;
  }
}


module.exports = addRewards;
