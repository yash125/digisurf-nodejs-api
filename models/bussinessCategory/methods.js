'use strict'
const db = require('../mongodb')
const tablename = "bussinessCategories";

function Select(data, callback) {
    db.get().collection(tablename)
        .find(data)
        .toArray((err, result) => {
            return callback(err, result);
        });

}
function Find(data, callback) {
    db.get().collection(tablename)
        .find({_id:data.businessCategory})
        .toArray((err, result) => {
            return callback(err, result);
        });

}
function findLang(data, callback) {
    db.get().collection(tablename)
        .aggregate([
            {
                $addFields: {
                    category: { "$categoryInOtherLang.data": "$homework" } ,
                  totalQuiz: { $sum: "$quiz" }
                }
              }
        ])
        .project({statusCode:0,statusText:0})
        .skip(data.offset)
        .limit(data.limit)
        .sort({_id:1})
        .toArray((err, result) => {
            return callback(err, result);
        });

}


module.exports = {
    Select,findLang,Find
};