#!/bin/bash

sshpass -p ${pwd} ssh -o StrictHostKeyChecking=no ${username}@${ip}  << EOF
echo "1. pull code from bitbucket......"
cd ${path} 
echo ${path}
git checkout --force ${branch}
git reset --hard
git clean -fd
git pull origin ${branch}
echo "2. Restart node server......app-server-1"
killall node
cd library/node-trace
npm i
cd ../..
npm i
forever start index.js
cd ..
cd gRPC/SMS-micro-service/
forever start Server.js
cd ../..
#cd gRPC/Email-micro-service/
#forever start Server.js
docker rmi appscrip007/dochat
docker rmi appscrip007/dochat
docker rmi dochat
cd dochat-nodejs-api
docker build -t dochat .
docker tag dochat appscrip007/dochat
docker push appscrip007/dochat
echo '----------------------------------Done App Server 1 !----------------------------------'
EOF
