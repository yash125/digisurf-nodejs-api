const Joi = require('joi');

var moment = require('moment');
var gRPC_SMS_service = require("../../library/gRPC/SMS");
var middleWare = require("../../Controller/dbMiddleware.js");

module.exports = [
    {
        method: 'POST',
        path: '/RequestOTP',
        config: {
            handler: function (req, reply) {
                // console.log("req.payload", req.payload);
                /**
                 * here we check all mendatory fields
                 */
                let otpType = 1;
                if (!req.payload.deviceId) {
                    return reply({ Message: "mandatory DeviceId is missing" }).code(400);
                }

                if (!req.payload.phoneNumber) {
                    return reply({ Message: "mandatory phoneNumber is missing" }).code(400);
                }
                if (!req.payload.countryCode) {
                    return reply({ Message: "mandatory countryCode is missing" }).code(400);
                }
                if (req.payload.type) {
                    otpType = req.payload.type;
                    // verifyUser: 1,
                    // verifyStarUserUser: 2
                }

                var number = req.payload.countryCode + req.payload.phoneNumber;

                var deviceId = req.payload.deviceId;
                /**
                 * create randomCode for send to user for OTP.
                 */
                
                var randomCode = 1111;

                if (process.env.NODE_ENV == "production" && (req.payload.development == 'false' || req.payload.development == false)) {
                    randomCode = (process.env.NODE_ENV == "production") ? Math.floor(1000 + Math.random() * 9000) : 1111;
                }

                if (process.env.NODE_ENV == "production" && (req.payload.development == 'false' || req.payload.development == false)) {

                    gRPC_SMS_service.sendSMS({
                        to: req.payload.phoneNumber, otp: randomCode,
                        hashKey: req.payload.hashKey || "",
                        countryCode: req.payload.countryCode.replace("+", ""), body: randomCode + ` is your registration code on ${process.env.APP_NAME}.`
                    })
                    // twilio.sendSms({
                    //     "from": process.env.CELL_PHONE_NUMBER,
                    //     "to": number,
                    //     "body": randomCode + ` is your registration code on ${process.env.APP_NAME}.`
                    // }, (e, r) => { });

                }

                // var OtpTypes = {
                //     verifyUser: 1,
                //     verifyStarUserUser: 2
                // }
                var dataToInsert = {
                    deviceId: deviceId,
                    type: otpType,
                    phone: number,
                    otp: randomCode,
                    time: moment().valueOf()
                }
                middleWare.Insert('otps', dataToInsert, {}, "Mongo", (err) => {
                    if (err) {
                        return reply({ message: "internal server error." }).code(500);
                    }
                });


                return reply({ message: "success", code: 200, response: {} }).code(200);
            },
            validate: {
                payload: Joi.object({
                    phoneNumber: Joi.string().description("phoneNumber is required"),
                    countryCode: Joi.string().description("CountryCode is required"),
                    deviceId: Joi.string().description("CountryCode is required"),
                    development: Joi.any().description("true / false").allow(""),
                    hashKey: Joi.string().description("hashKey").allow(""),
                    type: Joi.number().default(1).description("type is required, 1 : verify User, 2 : verify Star User, 3 : veryfy number"),
                }).unknown(),
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success',
                        },
                        '400': {
                            'description': 'Bad requset'
                        }
                    }
                }
            },
            description: 'validate the phone number by requesting an OTP.',
            notes: 'Header input: KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj',
            tags: ['api', "login"]
        }

    }
];
