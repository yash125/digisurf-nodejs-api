var Joi = require('joi');
var async = require("async");
var logger = require('winston');
var request = require('request');
var objectId = require("mongodb").ObjectID;

var ComnFun = require("../../ComnFun.js");
var fcm = require("../../library/fcm");
var userList = require('../../models/userList');
var middleWare = require("../../Controller/dbMiddleware.js");
var jwtValidator = require('../../web/middleware/auth');
/**
 * @method  POST verifyOtp 
 * @description this api is use for verify OTP by email id
 * 
 * @property {number} countryCode
 * @property {number} phoneNumber	
 * @property {number} deviceId	
 * @property {number} otp	
 * @property {string} deviceName	
 * @property {string} deviceOs	
 * @property {string} modelNumber	
 * @property {number} deviceType	
 * @property {number} appVersion
* @property {number} type	
* @property {boolean} isVisible	
* @property {boolean} isSignUp	
* 
* @returns 200 : Success
* @returns 422 : Parameters missing           
* @returns 412 : OTP has expired,please try again           
* @returns 500 : Internal Server Error
* 
* @author DipenAhir,karthik
* @since 07-june-2019
*/


module.exports = [
    {
        method: 'POST',
        path: '/verifyOtp',
        config: {
            handler: function (req, reply) {
                var otpType = 1;

                if (!req.payload.countryCode) {
                    return reply({ Message: "mandatory countryCode is missing" }).code(400);
                }
                if (!req.payload.phoneNumber) {
                    return reply({ Message: "mandatory mobileNo is missing" }).code(400);
                }

                if (!req.payload.otp) {
                    return reply({ Message: "mandatory otp is missing" }).code(400);
                }
                if (!req.payload.deviceId) {
                    return reply({ Message: "mandatory deviceId is missing" }).code(400);
                }
                if (req.payload.type) {
                    otpType = req.payload.type;

                }


                var countryCode = req.payload.countryCode + "";
                var number = countryCode + req.payload.phoneNumber;
                var otp = req.payload.otp;


                async.series([
                    function (callback) {

                        var condition = {
                            phone: number,
                            otp: parseInt(otp)
                        };
                        /**
                          * check if the otp exists & also check if it is expired
                          */
                        middleWare.Select('otps', condition, "Mongo", {}, (err, result) => {
                            if (err)
                                return reply({ code: 503, message: 'Database error' }).code(503);

                            if ((result && result[0] && result[0].time) || otp == "1111") {
                                //                                if (moment(result[0].time).add(5, 'minutes').valueOf() >= new Date().getTime()) {
                                callback(null, 'valid');
                                //                               } else {
                                //                                   console.log("reply with : 138 - Otp expired ")
                                //                                   return reply({ code: 138, message: 'Otp expired' }).code(200);
                                //                               }
                            } else {

                                return reply({ code: 138, message: 'Invalid otp' }).code(200);
                            }
                        });
                    }
                ],
                    function () {
                        var cond = { "mobile": req.payload.phoneNumber };
                        /**
                         * Check requested user is exists in ower db or not.
                         * if yes then push device details in deviceLog.
                         * if no then check in inactiveUserList, 
                         * ** if user exist on inactiveUserList then get DB field as "existInUsers" and send MQTT message to "existInUsers"  for requested new user is now join ower App
                         * ** if user not exists in inactiveUserList then only add user in DB (here DB Maintain in ElasticSearch and mongoDB).
                         * then user call post OTP api to verify account.
                         */
                        middleWare.Select("customer", cond, "Mongo", {}, function (err, result) {
                            if (err) {
                                return reply({ data: "unknown error occurred" }).code(500)
                            }
                            else if (result[0]) {



                                try {
                                    if (result[0]["userStatus"] == 0 || result[0]["userStatus"] == 2 || result[0]["userStatus"] == 3) {
                                        return reply({ message: "admin has " + result[0]["userStatusText"] + " you", code: 403 }).code(200);
                                    }
                                } catch (error) {
                                    console.log("error", error)
                                }

                                if (result[0] && result[0].parentId) {

                                    let parentId = result[0]._id;
                                    middleWare.Select("customer", { _id: objectId(result[0].parentId.toString()) }, "Mongo", {}, function (err, result_B) {
                                        if (err) {
                                            return reply({ data: "unknown error occurred" }).code(500)
                                        } else if (result_B[0] && result_B[0].isActiveBusinessProfile &&
                                            result_B[0].businessProfile &&
                                            result_B[0].businessProfile.find(e => e.businessProfileId.toString() == parentId.toString()) &&
                                            result_B[0].businessProfile.find(e => e.businessProfileId.toString() == parentId.toString()).isActive) {

                                            login(result, req, reply, otpType); //login with business profile
                                        } else if (result_B) {

                                            login(result_B, req, reply, otpType);//login with personal profile
                                        } else {
                                            return reply({ data: "data not found" }).code(204)
                                        }
                                    })
                                } else if (result[0] && result[0].businessProfile && result[0].isActiveBusinessProfile
                                    && result[0].businessProfile.find(e => e.statusCode == 1) &&
                                    result[0].businessProfile.find(e => e.statusCode == 1).businessProfileId &&
                                    result[0].businessProfile.find(e => e.statusCode == 1).businessProfileId.toString()) {

                                    let businessProfileId = result[0].businessProfile.find(e => e.statusCode == 1).businessProfileId;
                                    middleWare.Select("customer", { _id: objectId(businessProfileId) }, "Mongo", {}, function (err, result_bussiness) {
                                        if (err) {
                                            return reply({ data: "unknown error occurred" }).code(500)
                                        } else if (result_bussiness[0]) {
                                            login(result_bussiness, req, reply, otpType);
                                        } else {

                                            middleWare.Update("customer", { isActiveBusinessProfile: false }, { _id: objectId(result[0]._id.toString()) }, "Mongo", function (err, result_bussiness) {
                                                if (err) {
                                                    return reply({ data: "unknown error occurred" }).code(500)
                                                } else {
                                                    login(result, req, reply, otpType);
                                                }
                                            })
                                            // return reply({ data: "data not found" }).code(204)
                                        }
                                    });
                                } else {
                                    login(result, req, reply, otpType);
                                }
                            } else {


                                if (req.payload.isSignUp) {

                                    return reply({
                                        code: 200, message: 'success', response: {
                                            userId: objectId(),
                                            private: 0,
                                            token: objectId(),
                                        }
                                    }).code(200);
                                } else {

                                    return reply({
                                        code: 204, message: 'success', response: {
                                            userId: objectId(),
                                            private: 0,
                                            token: objectId(),
                                        }
                                    }).code(204);
                                }
                            }
                        });
                    });
            },
            validate: {
                payload: Joi.object({
                    countryCode: Joi.string().required().description("countryCode is required, Eg. +91, +1 etc.").error(new Error("countryCode is missing")),
                    phoneNumber: Joi.string().required().description("phoneNumber (*without country code) is required, Eg. 9034567867").error(new Error("phoneNumber is missing")),
                    deviceId: Joi.string().required().description("deviceId is required").error(new Error("deviceId is missing")),
                    otp: Joi.string().required().description("otp is required").error(new Error("otp is missing")),
                    deviceName: Joi.string().required().description("deviceName is required").error(new Error("deviceName is missing")),
                    deviceOs: Joi.string().required().description("deviceOs is required").error(new Error("deviceOs is missing")),
                    modelNumber: Joi.string().required().description("modelNumber is required").error(new Error("modelNumber is missing")),
                    deviceType: Joi.string().required().description("deviceType is required").error(new Error("deviceType is missing")),
                    appVersion: Joi.string().required().description("appVersion is required").error(new Error("appVersion is missing")),
                    type: Joi.number().default(1).description("type is required, 1 : verify User, 2 : verify Star User").error(new Error("type is missing")),
                    isVisible: Joi.bool().default(false).description("isVisible").error(new Error("isVisible is missing")),
                    isSignUp: Joi.bool().default(false).description("isSignUp").error(new Error("isSignUp is missing")),
                    countryName: Joi.string().description("countryName is required").error(new Error("countryName is missing")),
                    // username: Joi.string().required().description("username is mandatory")
                }).unknown()
            },
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form',
                    responses: {
                        '200': {
                            'description': 'Success',
                        },
                        '400': {
                            'description': 'Bad requset'
                        }
                    }
                }
            },
            description: 'User Authentication',
            notes: 'Enter the four digit otp for login / registration. Autorization Header: KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj',
            tags: ['api', 'login']
        }
    }
];

function login(result, req, reply, otpType) {
    let deviceInfo = {
        "userId": objectId(result[0]._id),
        "deviceName": req.payload.deviceName,
        "deviceOs": req.payload.deviceOs,
        "modelNumber": req.payload.modelNumber,
        "deviceType": req.payload.deviceType,
        "appVersion": req.payload.appVersion,
        "deviceId": req.payload.deviceId,
        "timestamp": new Date().getTime(),
        "creationDate": new Date(),
        "_id": new objectId()
    }

    fcm.notifyFcmTpic({
        fcmTopic: "" + result[0]._id,
        // action: 1,
        pushType: 2,
        title: process.env.APP_NAME,
        msg: "login",
        data: { "type": "login", "deviceId": req.payload.deviceId },
        deviceType: req.payload.deviceType
    }, (err, result) => {
        if (err) console.log(err)
    });

    let datatoupdate = { deviceInfo: deviceInfo };
    let condition = { _id: objectId(result[0]._id) };
    middleWare.Update("customer", datatoupdate, condition, "Mongo", () => { });
    middleWare.Insert("deviceInfo", deviceInfo, {}, "Mongo", function () { });
    var token = "", refreshToken = "", accessExpiry = 0;

    result[0].private = (result[0].private) ? 1 : 0;
    var dataToSend = {
        userId: result[0]._id.toString(),
        countryCode: result[0].countryCode,
        number: result[0].countryCode + result[0].mobile, token: token, refreshToken: refreshToken,
        private: result[0].private, accessExpiry: accessExpiry,
        email: (result[0].email && result[0].email.id) ? result[0].email.id : ""
    };
    dataToSend["businessProfile"] = (result[0].businessProfile) ? result[0].businessProfile : [];
    dataToSend["isActiveBusinessProfile"] = (result[0].isActiveBusinessProfile) ? true : false


    var mqttData = { _id: result[0]._id.toString(), number:  result[0].countryCode + result[0].mobile };
    dataToSend["isStar"] = (result[0] && result[0].starRequest && result[0].starRequest.starUserProfileStatusCode == 4)

    if (result[0].qrCode) {
        dataToSend["qrCode"] = result[0].qrCode.url
    }
    dataToSend["private"] = (result[0].private) ? 1 : 0;

    if (result[0].profilePic) {
        dataToSend.profilePic = result[0].profilePic;
        mqttData.profilePic = result[0].profilePic;
    }

    if (result[0].userName) {
        dataToSend.userName = result[0].userName;
    }
    if (result[0].firstName) {
        dataToSend.firstName = result[0].firstName;
    }
    if (result[0].lastName) {
        dataToSend.lastName = result[0].lastName;
    }
    if (result[0].socialStatus) {
        dataToSend.socialStatus = result[0].socialStatus;
        mqttData.socialStatus = result[0].socialStatus;
    }

    dataToSend.profileVideo = result[0].profileVideo || "";
    dataToSend.profileVideoThumbnail = result[0].profileVideoThumbnail || "";
    dataToSend.profilePic = result[0].profilePic || "";
    dataToSend["currencySymbol"] = process.env.CURRENCY_SYMBOL || "";
    dataToSend["currency"] = process.env.CURRENCY_CODE || "";
    dataToSend["groupCallStreamId"] = result[0].groupCallStreamId || "";

    dataToSend["accountId"] = process.env.accountId
    dataToSend["keysetId"] = process.env.keysetId
    dataToSend["projectId"] = process.env.projectId
    dataToSend["licenseKey"] = process.env.licenseKey
    dataToSend["keysetName"] = process.env.keysetName
    dataToSend["rtcAppId"] = process.env.rtcAppId
    dataToSend["arFiltersAppId"] = process.env.arFiltersAppId



    if (typeof result[0].existInUsers != 'undefined') {
        for (index = 0; index < result[0].existInUsers.length; index++) {
            ComnFun.publishMqtt("UserUpdate/" + result[0].existInUsers[index], JSON.stringify({ "contacts": [mqttData], type: 3 }));
        }
    }

    /**
     * search user with phone number in database
     */
    let conditionPhone = {
        mobile: result[0].mobile
    }
    userList.Select(conditionPhone, (err, result) => {

        //stream
        if (err) {
            logger.info('-------------', err);
        }

        /** 
        * stream data to be sent to loginSignUp api                                    
        * */
        let streamData = {
            userName: result[0].userName,
            firstName: result[0].firstName,
            id: result[0]._id,
            lastName: result[0].lastName || result[0].firstName,
            userType: 1,
            profilePic: result[0].profilePic,
            deviceType: deviceInfo.deviceType,
            mqttTopic: result[0]._id,
            fcmTopic: result[0]._id

        }



        /** 
           * calling loginSignUp api of stream module to signUp 
           * */
        request.post({
            headers: { 'content-type': 'application/json', 'lan': 'en' },
            url: `${process.env.LIVE_STREAM_API}/user`,
            body: streamData,
            json: true
        }, function (error, response, body) {

            if (error)
                logger.error('0000', error);
            else {
                if (response.statusCode == 200) {
                    dataToSend["stream"] = body.data;
                } else {
                    logger.error('error body,mostly payload is not proper------------------->', body)
                }
            }
            if (otpType == 3) {

                return reply({ code: 200, message: 'success', response: dataToSend }).code(200);

            } else if (otpType != 2) {
                jwtValidator.generateTokens(
                    {
                        userId: result[0]._id.toString(),
                        userType: "user",
                        multiLogin: "true",
                        allowedMax: "1",
                        immediateRevoke: "true",
                        metaData: {
                            "deviceId": req.payload.deviceId,
                            "sessionId": "",
                            "storeId": (result[0] && result[0].storeId) ? result[0].storeId : ""
                        },
                        accessTTL: process.env.AUTH_ACCESS_EXPIRY_TIME,
                        refreshTTL: process.env.AUTH_REFRESH_EXPIRY_TIME
                    }).then((data) => {
                        dataToSend["refreshToken"] = data.refreshToken;
                        dataToSend["token"] = data.accessToken;
                        dataToSend["accessExpiry"] = data.accessExpiry;

                        dataToSend["isStar"] = (result[0] && result[0].starRequest && result[0].starRequest.starUserProfileStatusCode == 4) ? true : false;
                        return reply({ code: 200, message: 'success', response: dataToSend }).code(200);
                    });
            } else {
                return reply({ code: 200, message: 'success', response: dataToSend }).code(200);
            }
        });

        try {
            //group call api
            request.post({
                headers: { 'content-type': 'application/json', 'lan': 'en' },
                url: `${process.env.GROUP_CALL_API}/loginSignUp`,
                body: {
                    id: result[0]["_id"].toString(),
                    userType: 1,
                    firstName: result[0].firstName,
                    lastName: result[0].lastName || result[0].firstName,
                    profilePic: result[0].profilePic,
                    deviceType: result[0].deviceInfo.deviceType,
                    pushKitToken: result[0]._id.toString(),
                    mqttTopic: result[0]._id.toString(),
                    fcmTopic: result[0]._id.toString()
                },
                json: true
            }, function (error) {
                if (error) {
                    logger.error('KLAPWDDASDE : ', error);
                }
            });
        } catch (error) {
            logger.error('KLAPWZZSDE : ', error);
        }

        try {
            /**
               *  call Ecome API
             */
            // request.post({
            //     headers: {
            //         'content-type': 'application/json',
            //         'authorization': '{"userId":"' + result[0]["_id"].toString() + '","userType":"user","metaData":{}}',
            //         'platform': 1,
            //         'currencysymbol': '$',
            //         'currencycode': 'USD',
            //         'language': 'en'
            //     },
            //     url: `${process.env.ECOME_API}/v1/ecomm/customerSyncDatum`,
            //     body: {
            //         "userId": result[0]["_id"].toString(),
            //         "firstName": result[0].firstName,
            //         "lastName": result[0].lastName,
            //         "email": result[0].email.id,
            //         "password": "123456",
            //         "countryCode": result[0].countryCode,
            //         "mobile": result[0].mobile.replace(result[0].countryCode, ""),
            //         "dateOfBirth": "1997-06-08",
            //         "profilePicture": result[0].profilePic,
            //         "latitude": "0",
            //         "longitude": "0",
            //         "ipAddress": "0.0.0.0",
            //         "city": "Bangalore",
            //         "country": "India"
            //     },
            //     json: true
            // }, function (error, response, body) {
            //     if (error) console.log("eror : KOIPLOIHDH ", error)
            // });

        } catch (error) {
            logger.error('KLAPWZZSDE : ', error);
        }

    });
}