
var Joi = require('joi');
var async = require("async");
var ObjectID = require('mongodb').ObjectID

var middleWare = require("../../Controller/dbMiddleware.js");

/**
 * Define all related routes
 */
module.exports = [
    {
        method: 'DELETE',
        path: '/GroupChat',
        handler: function (req, res) {
            /**
             * call the handler function
             */
            deleteGroupHandler(req, res);
        },
        config: {
            description: 'Delete the group.',
            notes: 'Header input: KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj',
            tags: ['api', 'messages'],
            auth: "user",
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string().description('api authorization').required()
                }).unknown(),
                query: Joi.object({
                    chatId: Joi.string().description('group chat id').required()
                }).unknown().required(),
                
                failAction: function (req, res, source, error) {
                    /**
                     * Common response format, in case the validation fails
                     */
                    return res({ message: error.message }).code(400);
                }
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '400': {
                            'description': 'Bad requset'
                        },
                        '404': {
                            'description': 'Not found'
                        },
                        '403': {
                            'description': 'Cannot leave  group.'
                        }
                    }
                }

            }
        }
    }];


/**
 * API Name: GET /Chats
 */
function deleteGroupHandler(req, reply) {
    async.waterfall([
        function (validateCB) {
            validateCB(null, req.decoded);
        },
        function (JWTDecoded, updtChatsCB) {
            /**
              * update the 'chatList' with 'members.{req.auth.credentials._id}.inactive: true
              * 
              * fetch all the message ids in 'message' and update from the bottom till we reach last active message 
              */
            middleWare.FindAndModify("chatList",
                { $set: { ["members." + req.auth.credentials._id + ".inactive"]: true } },
                { _id: ObjectID(req.query.chatId), ["members." + req.auth.credentials._id + ".inactive"]: { $exists: false } },
                "Mongo",
                { message: 1 }, { new: true },
                function (getChatErr, getChatRes) {
                    if (getChatErr) {
                        return reply({ code: 503, message: getChatErr.message }).code(503);
                    } else if (getChatRes.value) {

                        /**  iterate from last message till the one wich is already deleted */
                        async.eachSeries((getChatRes.value.message).reverse(), function (msgId, msgCB) {
                            /**
                             * update the message marking its deleted for the specified member 
                             */
                            middleWare.FindAndModify("messages",
                                { $set: { ["members." + req.auth.credentials._id + ".del"]: true } },
                                { _id: msgId, ["members." + req.auth.credentials._id + ".del"]: { $exists: false } },
                                "Mongo",
                                { messageId: 1 }, { new: true },
                                function (updtErrr, updtResult) {
                                    if (updtErrr) {
                                        console.log('updtErrr: ', updtErrr)
                                    } else {
                                        /**
                                         * check if the findAndModify found any document to update
                                         * if yes then continue else skip message updates
                                         */
                                        if (updtResult.lastErrorObject.updatedExisting) {
                                            msgCB(null)
                                        } else {
                                            updtChatsCB(null)
                                        }
                                    }
                                })
                        }, function (msgUpdtErr) {

                            /** Message loop completes */
                            if (msgUpdtErr) {
                                return reply({ code: 200, message: msgUpdtErr.message }).code(503)
                            }
                            /** All OK */
                            updtChatsCB(null)
                        });

                    }
                    else {
                        /** No messages to update */
                        updtChatsCB(null)
                    }
                })

        }, function () {

            /** final response to the user */
            return reply({ code: 200, message: "Group has been deleted." }).code(200);
        }

    ])

}

