
var Joi = require('joi');
var async = require("async");
var ObjectID = require('mongodb').ObjectID

var middleWare = require("../../Controller/dbMiddleware.js");


/**
 * Define all related routes
 */
module.exports = [
    {
        method: 'GET',
        path: '/GroupChat/Member',
        handler: function (req, res) {
            /**
             * call the handler function
             */
            getGroupMemberHandler(req, res);
        },
        config: {
            auth: "user",
            description: 'List all the member in a chat group including self.',
            notes: 'Header input: KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj',
            tags: ['api', 'messages'],
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string()
                }).unknown(),
                query: Joi.object({
                    chatId: Joi.string().description('Group chat id').required()
                }).unknown().required(),
                failAction: function (req, res, source, error) {
                    /**
                     * Common response format, in case the validation fails
                     */
                    return res({ message: error.message }).code(400);
                }
            },
        }
    }];


/**
 * API Name: GET /Chats
 */
function getGroupMemberHandler(req, reply) {
    var selChatObj = {}
    async.waterfall([
        function (validateCB) {
            validateCB(null, req.decoded);
        },
        function (JWTDecoded, validate_admin_cb) {

            /** Is the API caller a member of the group ?  */
            var getGroup, mem_exists = "members." + req.auth.credentials._id;
            try {
                getGroup = {
                    _id: ObjectID(req.query.chatId),
                    [mem_exists]: { $exists: true },
                    [mem_exists + ".inactive"]: { $exists: false },
                    [mem_exists + ".left"]: { $exists: false }
                }
            } catch (exec) {
                return res({ code: 503, message: "Invalid group chat id" }).code(503);
            }

            /** check for the group chat list by id 
             * and this user is one of the admin of the group
             */
            middleWare.Select("chatList", getGroup, "Mongo", {}, function (err, result) {
                if (err) {
                    return res({ code: 503, message: "Unknown error occurred" }).code(503);
                }
                else if (result.length > 0) {
                    selChatObj.id = result[0]._id.toString()
                    selChatObj.initiator = result[0].initiatedBy.toString()
                    selChatObj.created = result[0].createdAt
                    validate_admin_cb(null, result[0])
                }
                else {
                    return reply({ code: 204, message: "Group doesn't exist / Permission denied." }).code(200);
                }
            })
        },
        function (chatObj) {

            /** Is the members present in our db */

            var memArr = []

            /** prepare the members array for future query */
            async.eachSeries(Object.keys(chatObj.members), function (memObj, memCB) {

                /** check if the member not deleted / left the group  */
                if (!chatObj.members[memObj].inactive && !chatObj.members[memObj].left) {
                    memArr.push(ObjectID(memObj))
                }
                memCB(null)
            }, function (memLoopErr) {
                if (memLoopErr) return reply({ code: 500, message: memLoopErr.message }).code(500)

                memArr.push(ObjectID(selChatObj.initiator))
                /** get the group members details */
                middleWare.Select("customer", { _id: { $in: memArr } }, "Mongo", {}, function (err, doc) {
                    if (err)
                        return reply({ code: 500, message: err.message }).code(500)

                    if (doc.length > 0) {

                        /** found some data */
                        var grpMemArr = []
                        async.eachSeries(doc, function (grpMemObj, grpMemCB) {
                            /** 
                             * - capture the identifier, username, profile pic and status
                             * - mark if the user is one of the admin of the group
                             */
                            if (grpMemObj._id.toString() == selChatObj.initiator) {
                                selChatObj.initiatorIdentifier = grpMemObj.mobile
                            }

                            if ((chatObj.members)[grpMemObj._id.toString()] && (!(chatObj.members)[grpMemObj._id.toString()].left && !(chatObj.members)[grpMemObj._id.toString()].inactive)) {
                                grpMemArr.push({
                                    isAdmin: ((chatObj.members)[grpMemObj._id.toString()].status == 'Admin') ? true : false,
                                    userId: grpMemObj._id.toString(),
                                    userIdentifier: grpMemObj.mobile, // 'mobile' in this app
                                    userName: grpMemObj.userName,
                                    profilePic: grpMemObj.profilePic,
                                    socialStatus: grpMemObj.socialStatus,
                                    isStar: (grpMemObj.starRequest && grpMemObj.starRequest.starUserProfileStatusCode == 4) ? true : false
                                })
                            }
                            grpMemCB(null)
                        }, function (grpMemLoopErr) {
                            if (grpMemLoopErr) return reply({ code: 500, message: grpMemLoopErr.message }).code(500)
                            /** return final output */
                            return reply({
                                code: 200,
                                message: "success.",
                                data: {
                                    members: grpMemArr,
                                    createdByMemberId: selChatObj.initiator,
                                    createdByMemberIdentifier: selChatObj.initiatorIdentifier,
                                    createdAt: selChatObj.created
                                }
                            }).code(200);
                        })

                    } else {

                        /** no valid members found */
                        return reply({ code: 204, message: "No valid group members found." }).code(200);
                    }
                })
            })

        }
    ])

}

