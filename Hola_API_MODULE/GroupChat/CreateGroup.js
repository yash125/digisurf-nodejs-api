var Joi = require('joi');
var async = require("async");
var ObjectID = require('mongodb').ObjectID

var middleWare = require("../../Controller/dbMiddleware.js");

/**
 * Define all related routes
 */
module.exports = [
    {
        method: 'POST',
        path: '/GroupChat',
        handler: function (req, res) {
            /**
             * call the handler function
             */
            createGroupchatListHandler(req, res);
        },
        config: {
            description: 'Create a group for chat',
            notes: 'Header input: KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj',
            tags: ['api', 'messages'],
            auth: "user",
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string().description('api authorization').required()
                }).unknown(),
                payload: Joi.object({
                    members: Joi.array().description('array of user ids').required(),
                    subject: Joi.string().description("group subject").allow(''),
                    image: Joi.string().description("group profile pic").allow('')
                }).unknown().required(),
                failAction: function (req, res, source, error) {
                    /**
                     * Common response format, in case the validation fails
                     */
                    return res({ message: error.message }).code(400);
                }
            },
        }
    }];


/**
 * API Name: GET /Chats
 */
function createGroupchatListHandler(req, reply) {

    async.waterfall([
        function (validateCB) {
            validateCB(null, req.decoded);
        },
        function () {
            var memArr = []
            /**
             * loop through the ids and prepare the query
             */
            if (req.payload.members && (Array.isArray(req.payload.members) && (req.payload.members).length > 0)) {
                async.eachSeries(req.payload.members, function (memId, memCB) {
                    if (memId != req.auth.credentials._id) {
                        try {
                            memArr.push(ObjectID(memId))
                        } catch (exec) { console.log(exec.message) }
                    }
                    memCB(null)
                }, function (memLoopErr) {
                    if (memLoopErr) return reply({ code: 500, message: memLoopErr.message }).code(500)

                    if (memArr.length > 0) {

                        /** include the group creator as well  */
                        memArr.push(ObjectID(req.auth.credentials._id))
                        var membersObj = {};

                        /** find the valid users from the list specified */
                        middleWare.Select("customer", { _id: { $in: memArr } }, "Mongo", {}, function (err, doc) {

                            if (err)
                                return reply({ code: 500, message: err.message }).code(500)

                            if (doc.length > 0) {

                                var adminData = {}
                                /** prepare the members to be added to the group to be created. */
                                async.eachSeries(doc, function (dbMem, dbMemCB) {
                                    if (dbMem._id.toString() == req.auth.credentials._id) {
                                        /** admin */
                                        adminData.id = dbMem._id.toString()
                                        adminData.identifier = dbMem.mobile

                                        membersObj[dbMem._id.toString()] = {
                                            memberId: dbMem._id,
                                            status: "Admin",
                                            added: new Date().getTime()
                                        }
                                    } else {
                                        /** normal members */
                                        membersObj[dbMem._id.toString()] = {
                                            memberId: dbMem._id,
                                            status: "NormalMember",
                                            added: new Date().getTime()
                                        }
                                    }

                                    // msgMemObj[dbMem._id.toString()] = {
                                    //     memberId: dbMem._id
                                    // }
                                    dbMemCB(null)
                                }, function (dbMemLoopErr) {
                                    if (dbMemLoopErr) return reply({ code: 500, message: dbMemLoopErr.message }).code(500)

                                    /** prepare self to be added as admin member. */
                                    // membersObj[req.auth.credentials._id] = {
                                    //     memberId: ObjectID(req.auth.credentials._id),
                                    //     status: "Admin",
                                    //     added: new Date().getTime()
                                    // }
                                    // msgMemObj[req.auth.credentials._id] = {
                                    //     memberId: ObjectID(req.auth.credentials._id)
                                    // }

                                    const messageDbId = ObjectID()
                                    var chatObj = {
                                        subject: req.payload.subject,
                                        image: (req.payload.image) ? req.payload.image : '',
                                        members: membersObj,
                                        initiatedBy: ObjectID(req.auth.credentials._id),
                                        chatType: "GroupChat",
                                        createdAt: new Date().getTime(),
                                        secretId: "",
                                        message: [messageDbId]
                                    }

                                    /** create a chatlist for the group with members data */
                                    middleWare.Insert("chatList", chatObj, {}, "Mongo", function (err) {
                                        if (err) return reply({ code: 500, message: err.message }).code(500)
                                        else {
                                            // dasboardModule.dashboard({ "subject": "chatLog", "type": "groupChat" });
                                            /**
                                             * add a chat initiator message into message collection
                                             * {
                                                    "_id" : ObjectId("599488b0a69826395554ed0e"),
                                                    "messageId" : "1502906543894",
                                                    "secretId" : "",  // NO NEED    
                                                    "senderId" : ObjectId("5994881d8b9fd4395cb02d34"), // NO NEED
                                                    "receiverId" : null, // NO NEED
                                                    "payload" : "", 

                                                    "messageType" : "98"
                                                    "type" : "0", // 0 -group create, 2 - remove member, 3 - make admin
                                                    "initiatorId":"", // 0, 1, 2, 3, 4, 5, 6
                                                    "initiatorIdentifier":"",  // 0, 1, 2, 3, 4, 5, 6
                                                    "groupName":"" // 0
                                                    "memberId":"" // 1, 2, 3
                                                    "memberIdentifier":"" // 1, 2, 3

                                                    "timestamp" : 1502906544960.0,
                                                    "expDtime" : 0, //NO NEED
                                                    "chatId" : ObjectId("599488b0a69826395554ed0f"),
                                                    "userImage" : "", // NO NEED
                                                    "toDocId" : "4490ad33-2e6f-40e4-ab18-50b5bf2d7cc9", 
                                                    "name" : "Shivangi", 
                                                    "dataSize" : 11675, // NO NEED
                                                    "createdByMemberId": req.auth.credentials._id, // 0(create entry), 1(get from existing entry)
                                                    "createdByMemberIdentifier":"", // 0, 1
                                                    "createdAt": new Date().getTime() // 0, 1
                                                }
                                             */

                                            var insMsgObj = {
                                                "_id": messageDbId,
                                                "messageId": '' + new Date().getTime(),
                                                "payload": "",
                                                "messageType": "0",
                                                "type": "0", // 0- group create, 1- add member 2- remove member, 3- make admin, 4- update grop name, 5 -update profilepic, 6 -leave group
                                                "initiatorId": adminData.id, // 0, 1, 2, 3
                                                "initiatorIdentifier": adminData.identifier,  // 0, 1, 2, 3
                                                "groupName": req.payload.subject, // 0
                                                "timestamp": new Date().getTime(),
                                                "chatId": chatObj._id,
                                                "toDocId": "",
                                                "name": "",
                                                "senderId": ObjectID(adminData.id),
                                                "receiverId": ObjectID(adminData.id), // as there is no reciever in group chat
                                                "createdByMemberId": adminData.id,
                                                "createdByMemberIdentifier": adminData.identifier,
                                                "createdAt": new Date().getTime()
                                            }


                                            /** create a chatlist for the group with members data */
                                            middleWare.Insert("messages", insMsgObj, {}, "Mongo", function (mErr) {
                                                if (mErr) return reply({ code: 500, message: err.message }).code(500)
                                                else {

                                                    /** final response to the user with the created chat id */
                                                    return reply({
                                                        code: 200, message: 'Group created', data: {
                                                            chatId: chatObj._id.toString()
                                                        }
                                                    }).code(200)
                                                }
                                            })

                                        }
                                    });
                                })
                            } else {

                                /** no valid members found */
                                return reply({ code: 201, message: 'No members found' }).code(201)
                            }
                        })

                    } else {
                        return reply({ code: 202, message: 'No valid members', valid: [], invalid: req.payload.members }).code(200)
                    }

                })

            } else {

                /** either members array not set or is empty */
                return reply({ code: 201, message: 'No members to add.' }).code(201)
            }

        }
    ])

}


















