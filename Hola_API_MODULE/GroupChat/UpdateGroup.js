var Joi = require('joi');
var async = require("async");
var ObjectID = require('mongodb').ObjectID

var middleWare = require("../../Controller/dbMiddleware.js");
const _ = require('underscore')

/**
 * Define all related routes
 */
module.exports = [
    {
        method: 'PUT',
        path: '/GroupChat',
        handler: function (req, res) {
            /**
             * call the handler function
             * payload: chatId, subject, image
             */
            updateGroupHandler(req, res);
        },
        config: {
            auth: "user",
            description: 'Update group details (subject / profile pic).',
            notes: 'Header input: KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj',
            tags: ['api', 'messages'],
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string()
                }).unknown(),
                payload: Joi.object({
                    chatId: Joi.string().description('Group chat id').required()
                }).unknown().required(),
                failAction: function (req, res, source, error) {
                    /**
                     * Common response format, in case the validation fails
                     */
                    return res({ message: error.message }).code(400);
                }
            },
        }
    }];


/**
 * API Name: GET /Chats
 */
function updateGroupHandler(req, reply) {

    var selChatObj = {}
    async.waterfall([
        function (validateCB) {
            validateCB(null, req.decoded);
        },
        function (JWTDecoded, validate_admin_cb) {

            /** Is the API caller is a group member?  */
            var getGroup, admin_mem_exists = "members." + req.auth.credentials._id;
            try {
                getGroup = { _id: ObjectID(req.payload.chatId), [admin_mem_exists]: { $exists: true } }
            } catch (exec) {
                return res({ code: 503, message: "Invalid group chat id" }).code(503);
            }

            /** check for the group chat list by id 
             * and this user is one of the admin of the group
             */
            middleWare.Select("chatList", getGroup, "Mongo", {}, function (err, result) {
                if (err) {
                    return res({ code: 503, message: "Unknown error occurred" }).code(503);
                }
                else if (result.length > 0) {
                    selChatObj.id = result[0]._id.toString()
                    selChatObj.initiator = result[0].initiatedBy.toString()
                    validate_admin_cb(null, JWTDecoded, result[0])
                }
                else {
                    return reply({ code: 204, message: "Group doesn't exist / Permission denied.", response: {} }).code(200);
                }
            })
        },
        function (JWTDecoded, ChatObj, validate_members_cb) {

            /** find the valid members from the list specified */
            middleWare.Select("customer", { _id: ObjectID(req.auth.credentials._id) }, "Mongo", {}, function (err, doc) {
                if (err)
                    return reply({ code: 500, message: err.message }).code(500)

                if (doc.length > 0) {

                    /** prepare the members to be added to the group. */
                    async.eachSeries(doc, function (dbMem, dbMemCB) {

                        if (dbMem._id.toString() == req.auth.credentials._id) {
                            selChatObj.initiatorIdentifier = dbMem.mobile
                        }

                        dbMemCB(null)
                    }, function (dbMemLoopErr) {
                        if (dbMemLoopErr) return reply({ code: 500, message: dbMemLoopErr.message }).code(500)

                        /** continue to final function with members array */
                        validate_members_cb(null, JWTDecoded)
                    })
                }
            })

        },
        function () {

            /** Is the members present in our db */
            var updateData = {}
            if (req.payload.subject) {
                updateData.subject = req.payload.subject
            }
            if (req.payload.image) {
                updateData.image = req.payload.image
            }

            if (!_.isEmpty(updateData)) {

                const messageDbId = ObjectID()
                var messageType = ''

                /** update the group chatList with the new members */
                middleWare.UpdateAdvanced("chatList", { $set: updateData, $push: { message: messageDbId } }, { _id: ObjectID(req.payload.chatId) }, {}, "Mongo", function (err) {
                    if (err) return reply({ code: 500, message: err.message }).code(500)

                    if (req.payload.subject) {
                        messageType = '4'
                    } else if (req.payload.image) {
                        messageType = '5'
                    }
                    var insMsgObj = {
                        "_id": messageDbId,
                        "messageId": '' + new Date().getTime(),
                        "payload": "",
                        "messageType": messageType, //(removed 'type') 0- group create, 1- add member 2- remove member, 3- make admin, 4- update grop name, 5 -update profilepic, 6 -leave group
                        "initiatorId": req.auth.credentials._id, // one who 0, 1, 2, 3  
                        "initiatorIdentifier": selChatObj.initiatorIdentifier,  // 0, 1, 2, 3
                        "timestamp": new Date().getTime(),
                        "chatId": ObjectID(req.payload.chatId),
                        "groupName": (req.payload.subject) ? req.payload.subject : '',
                        "toDocId": "",
                        "name": "",
                        "senderId": ObjectID(req.auth.credentials._id),
                        "receiverId": ObjectID(req.auth.credentials._id)   // as there is no reciever in group chat
                    }

                    middleWare.Insert("messages", insMsgObj, {}, "Mongo", function (mErr) {
                        if (mErr) return reply({ code: 500, message: err.message }).code(500)

                        /** final response to the user */
                        return reply({ code: 200, message: "Group details updated successfully." }).code(200);
                    })

                });
            } else {

                /** nothing to update */
                return reply({ code: 200, message: "Group details updated successfully." }).code(200);
            }

        }
    ])

}

