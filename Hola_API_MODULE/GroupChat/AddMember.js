var Joi = require('joi');
var async = require("async");
var ObjectID = require('mongodb').ObjectID

var middleWare = require("../../Controller/dbMiddleware.js");
/**
 * Define all related routes
 */
module.exports = [
    {
        method: 'POST',
        path: '/GroupChat/Member',
        handler: function (req, res) {
            /**
             * call the handler function
             */
            addGroupMemberHandler(req, res);
        },
        config: {
            auth: "user",
            description: 'Add a member to a chat group.',
            notes: 'Header input: KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj',
            tags: ['api', 'messages'],
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string()
                }).unknown(),
                payload: Joi.object({
                    chatId: Joi.string().description('Group chat id').required(),
                    members: Joi.array().description('Array of user ids').required(),
                }).unknown().required(),
                failAction: function (req, res, source, error) {
                    /**
                     * Common response format, in case the validation fails
                     */
                    return res({ message: error.message }).code(400);
                }
            },
        }
    }];


/**
 * API Name: GET /Chats
 */
function addGroupMemberHandler(req, reply) {
    var selChatObj = {}
    async.waterfall([
        function (validateCB) {
            validateCB(null, req.decoded);
        },
        function (JWTDecoded, validate_admin_cb) {

            /** Is the API caller have permission to add a member?  */
            var getGroup, admin_mem_status = "members." + req.auth.credentials._id + ".status";
            try {
                getGroup = { _id: ObjectID(req.payload.chatId), [admin_mem_status]: "Admin" }
            } catch (exec) {
                return res({ code: 503, message: "Invalid group chat id" }).code(503);
            }

            /** check for the group chat list by id 
             * and this user is one of the admin of the group
             */
            middleWare.Select("chatList", getGroup, "Mongo", {}, function (err, result) {
                if (err) {
                    return res({ code: 503, message: "Unknown error occurred" }).code(503);
                }
                else if (result.length > 0) {
                    selChatObj.id = result[0]._id.toString()
                    selChatObj.subject = result[0].subject
                    selChatObj.initiator = result[0].initiatedBy.toString()
                    validate_admin_cb(null, JWTDecoded)
                }
                else {
                    return reply({ code: 204, message: "Group doesn't exist / Permission denied.", response: {} }).code(200);
                }
            })
        },
        function (JWTDecoded, validate_members_cb) {

            /** Is the members present in our db */
            var memArr = []
            if (req.payload.members && (Array.isArray(req.payload.members) && (req.payload.members).length > 0)) {
                async.eachSeries(req.payload.members, function (memId, memCB) {
                    try {
                        memArr.push(ObjectID(memId))
                    } catch (exec) { console.log(exec.message) }
                    memCB(null)
                }, function (memLoopErr) {
                    if (memLoopErr) return reply({ code: 500, message: memLoopErr.message }).code(500)

                    /** add group initiator and the createdbymember */
                    memArr.push(ObjectID(selChatObj.initiator))
                    memArr.push(ObjectID(req.auth.credentials._id))

                    if (memArr.length > 0) {

                        var membersArr = []
                        /** find the valid members from the list specified */
                        middleWare.Select("customer", { _id: { $in: memArr } }, "Mongo", {}, function (err, doc) {
                            if (err)
                                return reply({ code: 500, message: err.message }).code(500)

                            if (doc.length > 0) {

                                /** prepare the members to be added to the group. */
                                async.eachSeries(doc, function (dbMem, dbMemCB) {

                                    if (dbMem._id.toString() == selChatObj.initiator) {
                                        selChatObj.initiatorIdentifier = dbMem.mobile
                                    }

                                    if (dbMem._id.toString() == req.auth.credentials._id) {
                                        selChatObj.identifier = dbMem.mobile
                                    }

                                    if (((req.payload.members).indexOf(dbMem._id.toString()) >= 0) || dbMem._id.toString() != selChatObj.initiator && dbMem._id.toString() != req.auth.credentials._id) {

                                        membersArr.push({
                                            memberId: dbMem._id,
                                            memberIdentifier: dbMem.mobile,
                                            status: "NormalMember",
                                            added: new Date().getTime()
                                        })
                                    }
                                    dbMemCB(null)
                                }, function (dbMemLoopErr) {
                                    if (dbMemLoopErr) return reply({ code: 500, message: dbMemLoopErr.message }).code(500)

                                    /** continue to final function with members array */
                                    validate_members_cb(null, membersArr, JWTDecoded)
                                })
                            }
                        })

                    } else {
                        return reply({ code: 204, message: "No valid members to add" }).code(200);
                    }

                })
            }
        },
        function (finalMembers) {

            /** add members to group */
            var updateData = {}, insMsgArr = [];
            const messageDbId = ObjectID()

            async.forEach(finalMembers, function (finalMem, membCB) {

                var insMsgObj = {
                    "_id": messageDbId,
                    "messageId": '' + new Date().getTime(),
                    "payload": "",
                    "messageType": "1", //(removed 'type') 0- group create, 1- add member 2- remove member, 3- make admin, 4- update grop name, 5 -update profilepic, 6 -leave group
                    "initiatorId": selChatObj.initiator, // one who 0, 1, 2, 3  
                    "initiatorIdentifier": selChatObj.initiatorIdentifier,  // 0, 1, 2, 3
                    "groupName": req.payload.subject, // 0
                    "timestamp": new Date().getTime(),
                    "chatId": ObjectID(req.payload.chatId),
                    "toDocId": "",
                    "name": "",
                    "memberId": finalMem.memberId,
                    "memberIdentifier": finalMem.memberIdentifier,
                    "senderId": ObjectID(req.auth.credentials._id),
                    "receiverId": ObjectID(req.auth.credentials._id), // as there is no reciever in group chat
                    "createdByMemberId": req.auth.credentials._id,  // one who created the group
                    "createdByMemberIdentifier": selChatObj.identifier,
                    "createdAt": new Date().getTime()
                }
                insMsgArr.push(insMsgObj)

                updateData['members.' + (finalMem.memberId).toString()] = finalMem
                membCB(null)
            }, function (finalMemLoopErr) {
                if (finalMemLoopErr) return reply({ code: 500, message: finalMemLoopErr.message }).code(500)

                /** update the group chatList with the new members */
                middleWare.UpdateAdvanced("chatList", { $set: updateData, $push: { message: messageDbId } }, { _id: ObjectID(req.payload.chatId) }, {}, "Mongo", function (err) {
                    if (err) return reply({ code: 500, message: err.message }).code(500)

                    async.eachSeries(insMsgArr, function (insertMsgObj, insertMsgCB) {
                        /** create a chatlist for the group with members data */
                        middleWare.Insert("messages", insertMsgObj, {}, "Mongo", function (mErr) {
                            if (mErr) return reply({ code: 500, message: err.message }).code(500)
                        })
                        insertMsgCB(null)
                    }, function (insertMsgLoopErr) {
                        if (insertMsgLoopErr) console.log(insertMsgLoopErr)
                        /** final response to the user */
                        return reply({ code: 200, message: "Memebers successfully added." }).code(200);
                    })
                });
            })
        }
    ])
}