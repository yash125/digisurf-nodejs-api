var ObjectID = require('mongodb').ObjectID
var async = require("async");
var Joi = require('joi');
var middleWare = require("../../Controller/dbMiddleware.js");
var ComnFun = require('../../ComnFun.js');

module.exports = [
    {
        method: 'GET',
        path: '/GroupChat/Messages',
        handler: function (req, res) {
            GetMessagesHendler(req, res);
        },
        config: {
            tags: ['api', 'messages'],
            description: 'Get the messages for the specified group chat',
            notes: 'Header input: Basic aG91c2U6ZVNTNzlCakVncVlYbmlXNHJTYzI3UDdMOUcyN2p3WFFZMlA0dGZCZW5tekcwb21iRnVKZVNYQ2pYQ3JaNVAwOGIxODBRc3RROTE2ZWtqS3FpZDBMTWRHMmRLTmk1cDRBS1FiSUUxUFZFcnF1RTJuYUx6U0JxR1dYT0hFd0pmR2hKb2daNzdTT0lJYXdEQVpIM1JhUDlablZwQ0NxRVZJRHZRY1ZYaFFRS0tIcTRQaktsRmlubHVkbzBoOXQ3RE9UUklyMndqNmZXV1VHWmZyUTc4UVFURFBTbm1vTFB0U0FhbWxWbEZHZW5waGZYR1dTNks2QTJkQW1nSjlwcTZmbg==',
            auth: "user",
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string()
                }).unknown(),
                query: {
                    chatId: Joi.string().description("Unique id for chat"),
                    timestamp: Joi.string().description("timestamp"),
                    pageSize: Joi.string().description("page size"),
                }
            },

        },

    }];


function GetMessagesHendler(req, res) {
    var finalChatObj = {}
    async.waterfall([
        function (validateCB) {

            validateCB(null, req.decoded);
        },
        function (JWTDecoded, getChatCB) {


            middleWare.Select("chatList", { _id: ObjectID(req.query.chatId) }, "Mongo", {}, function (err, result) {
                if (err) {
                    return res({ code: 503, message: err.message }).code(503);
                }
                else if (result.length > 0) {

                    finalChatObj.subject = result[0].subject
                    finalChatObj.image = result[0].image

                    getChatCB(null, JWTDecoded, result[0])
                }
                else {
                    return res({ code: 204, message: "Group chat doesn't exist", response: {} }).code(200);
                }
            })


        },
        function (JWTDecoded, ChatObj) {

            var pagesize = 1000;
            try {
                pagesize = parseInt(req.query.pageSize) || 1000;
            } catch (error) {
                pagesize = 1000;
            }
            var usr_msg_del = "members." + req.auth.credentials._id + ".del"
            var getMesg = [
                {
                    "$match": {
                        "chatId": ObjectID(req.query.chatId),
                        // "timestamp": { $gte: ChatObj.members[req.auth.credentials._id].added, $lt: parseInt(req.query.timestamp) },
                        [usr_msg_del]: { "$exists": false }
                    }
                },
                {
                    "$project": {
                        _id: 0, messageId: 1, secretId: 1, senderId: 1, receiverId: 1, payload: 1, messageType: 1,
                        timestamp: 1, chatId: 1, userImage: 1, toDocId: 1, name: 1, dTime: 1, dataSize: 1, thumbnail: 1,
                        mimeType: 1, fileName: 1, extension: 1, members: 1, previousReceiverIdentifier: 1, previousFrom: 1,
                        previousPayload: 1, previousType: 1, previousId: 1, replyType: 1, previousFileType: 1,
                        receiverIdentifier: 1, initiatorId: 1, initiatorIdentifier: 1, groupName: 1, memberId: 1,
                        memberIdentifier: 1, removedAt: 1, wasEdited: 1, postId: 1, postType: 1, postTitle: 1
                    }
                },
                { "$lookup": { "from": "customer", "localField": "initiatorIdentifier", "foreignField": "mobile", as: "userData" } },
                { "$unwind": "$userData" },
                {
                    "$project": {
                        _id: 0, messageId: 1, secretId: 1, senderId: 1, receiverId: 1, payload: 1, messageType: 1,
                        timestamp: 1, chatId: 1, userImage: 1, toDocId: 1, name: 1, dTime: 1, dataSize: 1, thumbnail: 1,
                        mimeType: 1, fileName: 1, extension: 1, members: 1, previousReceiverIdentifier: 1, previousFrom: 1,
                        previousPayload: 1, previousType: 1, previousId: 1, replyType: 1, previousFileType: 1,
                        receiverIdentifier: 1, initiatorId: 1, initiatorIdentifier: "$userData.userName", groupName: 1, memberId: 1,
                        memberIdentifier: 1, removedAt: 1, wasEdited: 1, postId: 1, postType: 1, postTitle: 1
                    }
                },

                // adding memberIdentifier = username form here
                {
                    "$lookup": {
                        "from": "customer", "localField": "memberIdentifier",
                        "foreignField": "mobile",
                        "as": "memberData"
                    }
                },
                {
                    "$unwind": {
                        "path": "$memberData",
                        "preserveNullAndEmptyArrays": true
                    }
                },
                { "$match": { $or: [{ "memberData.userStatus": 1 }, { "messageType": { "$ne": "2" } }] } },
                {
                    "$project": {
                        "_id": 0, "messageId": 1, "secretId": 1, "senderId": 1, "receiverId": 1,
                        "payload": 1, "messageType": 1, "timestamp": 1, "chatId": 1, "userImage": 1, "toDocId": 1,
                        "name": 1, "dTime": 1, "dataSize": 1, "thumbnail": 1, "mimeType": 1, "fileName": 1, "extension": 1, "members": 1,
                        "previousReceiverIdentifier": 1, "previousFrom": 1, "previousPayload": 1, "previousType": 1, "previousId": 1,
                        "replyType": 1, "previousFileType": 1, "receiverIdentifier": 1, "initiatorId": 1,
                        "initiatorIdentifier": 1, "groupName": 1, "memberId": 1,
                        "memberIdentifier": "$memberData.userName", "removedAt": 1, "wasEdited": 1, "postId": 1,
                        "postType": 1, "postTitle": 1
                    }
                },
                {
                    "$group": {
                        "_id": "$messageId",
                        "messageId": { "$last": "$messageId" },
                        "secretId": { "$last": "$secretId" },
                        "senderId": { "$last": "$senderId" },
                        "receiverId": { "$last": "$receiverId" },
                        "payload": { "$last": "$payload" },
                        "messageType": { "$last": "$messageType" },
                        "timestamp": { "$last": "$timestamp" },
                        "chatId": { "$last": "$chatId" },
                        "userImage": { "$last": "$userImage" },
                        "toDocId": { "$last": "$toDocId" },
                        "name": { "$last": "$name" },
                        "dTime": { "$last": "$dTime" },
                        "dataSize": { "$last": "$dataSize" },
                        "thumbnail": { "$last": "$thumbnail" },
                        "mimeType": { "$last": "$mimeType" },
                        "fileName": { "$last": "$fileName" },
                        "extension": { "$last": "$extension" },
                        "members": { "$last": "$members" },
                        "previousReceiverIdentifier": { "$last": "$previousReceiverIdentifier" },
                        "previousFrom": { "$last": "$previousFrom" },
                        "previousPayload": { "$last": "$previousPayload" },
                        "previousType": { "$last": "$previousType" },
                        "previousId": { "$last": "$previousId" },
                        "replyType": { "$last": "$replyType" },
                        "previousFileType": { "$last": "$previousFileType" },
                        "receiverIdentifier": { "$last": "$receiverIdentifier" },
                        "initiatorId": { "$last": "$initiatorId" },
                        "initiatorIdentifier": { "$last": "$initiatorIdentifier" },
                        "groupName": { "$last": "$groupName" },
                        "memberId": { "$last": "$memberId" },
                        "memberIdentifier": { "$last": "$memberIdentifier" },
                        "removedAt": { "$last": "$removedAt" },
                        "wasEdited": { "$last": "$wasEdited" },
                        "postId": { "$last": "$postId" },
                        "postType": { "$last": "$postType" },
                        "postTitle": { "$last": "$postTitle" }
                    }
                },

                // adding memberIdentifier = username end  here
                { "$sort": { "timestamp": -1 } },
                { $limit: pagesize }
            ];


            middleWare.AggreGate("messages", getMesg, "Mongo", function (err, result) {
                if (err) {
                    return res({ message: err.message }).code(503);
                }
                else if (result.length) {
                    
                    for (let index = 0; index < result.length; index++) {
                        if (result[index].messageType != "1" && result[index].messageType != "2" && result[index].messageType != "7" && result[index].replyType != "1" && result[index].replyType != "2" && result[index].replyType != "7") {
                            delete result[index].thumbnail;
                        }
                        if (result[index].dataSize == null) {
                            delete result[index].dataSize;
                        }
                    }

                    var finalRes = [], secretId = '';
                    /**
                     * Loop through the result and prepare the final result 
                     */
                    async.eachSeries(result, function (resObj, resCB) {

                        /**
                         * keeping the common fields out of the response array
                         * OponentID and SecretID are common for all messages
                         */
                        var msgObj = {
                            "messageId": resObj.messageId,
                            "senderId": resObj.senderId,
                            "receiverId": resObj.receiverId,
                            "receiverIdentifier": resObj.receiverIdentifier,
                            "messageType": resObj.messageType,
                            "timestamp": resObj.timestamp,
                            "userImage": resObj.userImage,
                            "toDocId": resObj.toDocId,
                            "name": resObj.name,
                            // "status": (resObj.members[resObj.receiverId].status) ? resObj.members[resObj.receiverId].status : 1,
                            "status": 1,
                            // "delivered": (resObj.members[resObj.receiverId].deliveredAt) ? resObj.members[resObj.receiverId].deliveredAt : -1,
                            "delivered": -1,
                            // "read": (resObj.members[resObj.receiverId].readAt) ? resObj.members[resObj.receiverId].readAt : -1,
                            "read": -1,
                            "removedAt": resObj.removedAt,
                            "wasEdited": resObj.wasEdited
                        }

                        /** to filter out payload for action messages */
                        if (resObj.payload != "") {
                            msgObj.payload = resObj.payload
                        } else {
                            msgObj.groupSubject = resObj.groupName
                            msgObj.initiatorId = resObj.initiatorId
                            msgObj.initiatorIdentifier = resObj.initiatorIdentifier
                            msgObj.memberId = resObj.memberId
                            msgObj.memberIdentifier = resObj.memberIdentifier
                        }


                        /** add dTime only if the its a secret chat */
                        // if (secretId != '')
                        //     msgObj.dTime = resObj.dTime

                        /**
                         * check if the message type is of image / video / audio / doodle
                         * and add the additional fields (thumbnail, dataSize)
                         */
                        switch (resObj.messageType) {
                            case '1':
                            case '2':
                            case '7':
                                msgObj.thumbnail = resObj.thumbnail
                                msgObj.dataSize = resObj.dataSize
                                break;
                            case '5':
                                msgObj.dataSize = resObj.dataSize
                                break;
                            case '9':
                                msgObj.dataSize = resObj.dataSize
                                msgObj.extension = resObj.extension
                                msgObj.fileName = resObj.fileName
                                msgObj.mimeType = resObj.mimeType
                                break;
                            case '10':
                                msgObj.dataSize = resObj.dataSize
                                if (resObj.replyType == '9') {
                                    msgObj.extension = resObj.extension
                                    msgObj.fileName = resObj.fileName
                                    msgObj.mimeType = resObj.mimeType
                                }

                                msgObj.previousReceiverIdentifier = resObj.previousReceiverIdentifier
                                msgObj.previousFrom = resObj.previousFrom
                                msgObj.previousPayload = resObj.previousPayload
                                msgObj.previousType = resObj.previousType
                                msgObj.previousId = resObj.previousId
                                msgObj.replyType = resObj.replyType
                                if (resObj.replyType == '1' || resObj.replyType == '2' || resObj.replyType == '7')
                                    msgObj.thumbnail = resObj.thumbnail
                                if (resObj.previousType == '9')
                                    msgObj.previousFileType = resObj.previousFileType
                                break;
                            case '13':
                                msgObj.postId = resObj.postId
                                msgObj.postType = resObj.postType
                                msgObj.postTitle = resObj.postTitle
                                break;
                        }

                        finalRes.push(msgObj);
                        resCB(null);
                    }, function (FinalLoopErr) {
                        if (FinalLoopErr) {
                            return res({ message: finalLoopErr.message }).code(503);
                        }
                        /**
                         * Success status goes with API response. 
                         * Actual data goes through MQTT
                         */
                        res({
                            code: 200,
                            message: "Success"
                        }).code(200);
                        ComnFun.publishMqtt("GetMessages/" + req.auth.credentials._id, JSON.stringify({
                            "chatId": req.query.chatId,
                            "receiverName": finalChatObj.subject,
                            "userImage": finalChatObj.image,
                            "opponentUid": '',
                            "secretId": secretId,
                            "dTime": 0,
                            "messages": finalRes
                        }))

                    })
                }
                else {
                    /** No messages to send */
                    
                    res({ code: 200, message: "No Data Found.", data: {} }).code(200);

                    ComnFun.publishMqtt("GetMessages/" + req.auth.credentials._id, JSON.stringify({
                        "chatId": req.query.chatId,
                        "receiverName": finalChatObj.subject,
                        "userImage": finalChatObj.image,
                        "opponentUid": '',
                        "secretId": '',
                        "dTime": 0,
                        "messages": []
                    }))
                }
            })
        }
    ])
}