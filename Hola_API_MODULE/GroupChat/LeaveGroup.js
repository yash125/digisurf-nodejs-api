var Joi = require('joi');
var async = require("async");
var ObjectID = require('mongodb').ObjectID

var middleWare = require("../../Controller/dbMiddleware.js");

/**
 * Define all related routes
 */
module.exports = [
    {
        method: 'DELETE',
        path: '/GroupChat/Member',
        handler: function (req, res) {
            /**
             * call the handler function
             */
            leaveGroupHandler(req, res);
        },
        config: {
            description: 'Leave the chat group.',
            notes: 'Header input: KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj',
            tags: ['api', 'messages'],
            auth: "user",
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string().description('api authorization').required(),
                }).unknown(),
                query: Joi.object({
                    chatId: Joi.string().description('group chat id').required()
                }).unknown().required(),
                failAction: function (req, res, source, error) {
                    /**
                     * Common response format, in case the validation fails
                     */
                    return res({ message: error.message }).code(400);
                }
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '400': {
                            'description': 'Bad requset'
                        },
                        '404': {
                            'description': 'Not found'
                        },
                        '403': {
                            'description': 'Cannot leave  group.'
                        }
                    }
                }

            }
        }
    }];


/**
 * API Name: GET /Chats
 */
function leaveGroupHandler(req, reply) {

    var selChatObj = {}
    async.waterfall([
        function (validateCB) {
            validateCB(null, { userId: req.auth.credentials._id });
        },
        function (JWTDecoded, validate_member_cb) {

            /** Is the API caller a member of the group?  */
            var getGroup, mem_exists = "members." + req.auth.credentials._id;
            try {
                getGroup = { _id: ObjectID(req.query.chatId), [mem_exists]: { $exists: true } }
            } catch (exec) {
                return reply({ code: 503, message: "Invalid group chat id" }).code(503);
            }

            /** check for the group chat list by id 
             * and this user is one of the admin of the group
             */
            middleWare.Select("chatList", getGroup, "Mongo", {}, function (err, result) {
                if (err) {
                    return reply({ code: 503, message: "Unknown error occurred" }).code(503);
                }
                else if (result.length > 0) {
                    selChatObj.id = result[0]._id.toString()
                    selChatObj.initiator = result[0].initiatedBy.toString()
                    validate_member_cb(null, JWTDecoded, result[0])
                }
                else {
                    return reply({ code: 204, message: "Invalid group member.", response: {} }).code(200);
                }
            })
        },
        function (JWTDecoded, ChatObj, validate_members_cb) {

            /** find the valid members from the list specified */
            middleWare.Select("customer", { _id: ObjectID(req.auth.credentials._id) }, "Mongo", {}, function (err, doc) {
                if (err)
                    return reply({ code: 500, message: err.message }).code(500)

                if (doc.length > 0) {

                    /** prepare the members to be added to the group. */
                    async.eachSeries(doc, function (dbMem, dbMemCB) {

                        if (dbMem._id.toString() == req.auth.credentials._id) {
                            selChatObj.initiatorIdentifier = dbMem.mobile
                        }

                        dbMemCB(null)
                    }, function (dbMemLoopErr) {
                        if (dbMemLoopErr) return reply({ code: 500, message: dbMemLoopErr.message }).code(500)

                        /** continue to final function with members array */
                        validate_members_cb(null, ChatObj, JWTDecoded)
                    })
                }

            })
        },
        function (chatObj, api_caller, mainCB) {

            /** check if the API caller is the admin of the group */
            if ((chatObj.members[api_caller.userId].status).toLowerCase() == "admin") {
                var adminCnt = 0
                /** check if there are any other admin available */
                async.forEach(Object.keys(chatObj.members), function (item, callback) {
                    if ((chatObj.members[item].status).toLowerCase() == "admin") {
                        adminCnt++
                    }
                    callback();

                }, function () {
                    if (adminCnt > 1) {
                        /** good to go */
                        mainCB(null, api_caller)
                    } else {
                        /** cannot leave the group */
                        return reply({ code: 403, message: 'Cannot leave the group.' }).code(200);
                    }
                });

            } else {
                /** continue - remove the member from the group */
                mainCB(null, api_caller)
            }

        }, function () {

            const messageDbId = ObjectID()
            /** update the group chatList with the new members */
            middleWare.UpdateAdvanced("chatList",
                { $set: { ["members." + req.auth.credentials._id + ".left"]: true }, $push: { message: messageDbId } },
                { _id: ObjectID(req.query.chatId) },
                {}, "Mongo",
                function (err) {
                    if (err) return reply({ code: 500, message: err.message }).code(500)

                    var insMsgObj = {
                        "_id": messageDbId,
                        "messageId": '' + new Date().getTime(),
                        "payload": "",
                        "messageType": "6", //(removed 'type') 0- group create, 1- add member 2- remove member, 3- make admin, 4- update grop name, 5 -update profilepic, 6 -leave group
                        "initiatorId": req.auth.credentials._id, // one who 0, 1, 2, 3  
                        "initiatorIdentifier": selChatObj.initiatorIdentifier,  // 0, 1, 2, 3
                        "timestamp": new Date().getTime(),
                        "chatId": ObjectID(req.query.chatId),
                        "toDocId": "",
                        "name": "",
                        "senderId": ObjectID(req.auth.credentials._id),
                        "receiverId": ObjectID(req.auth.credentials._id)   // as there is no reciever in group chat
                    }

                    middleWare.Insert("messages", insMsgObj, {}, "Mongo", function (mErr) {
                        if (mErr) return reply({ code: 500, message: err.message }).code(500)
                        /** final response to the user */
                        
                        return reply({ code: 200, message: "Memebers left the group." }).code(200);
                    })
                });
        }
    ])
}