
var Joi = require('joi');
var async = require("async");
var ObjectID = require('mongodb').ObjectID

var middleWare = require("../../Controller/dbMiddleware.js");

/**
 * Define all related routes
 */
module.exports = [
    {
        method: 'GET',
        path: '/GroupChat/Message/Status',
        handler: function (req, res) {
            /**
             * call the handler function
             */
            getGroupMemberHandler(req, res);
        },
        config: {
            description: 'List all the member in a chat group including self.',
            notes: 'Header input: KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj',
            tags: ['api', 'messages'],
            auth: "user",
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string()
                }).unknown(),
                query: Joi.object({
                    chatId: Joi.string().description('Group chat id').required(),
                    messageId: Joi.string().description('Group chat message id').required()
                }).unknown().required(),
                failAction: function (req, res, source, error) {
                    /**
                     * Common response format, in case the validation fails
                     */
                    return res({ message: error.message }).code(400);
                }
            }
        }
    }];


/**
 * API Name: GET /Chats
 */
function getGroupMemberHandler(req, reply) {
    var finalMsgStatus = {
        totalMembers: 0,
        deliveredTo: [],
        readBy: []
    }
    async.waterfall([
        function (validateCB) {
            validateCB(null, req.decoded);
        },
        function () {

            
            /** get message and check if its sent by same user */
            try {
                var getMsg = {
                    "chatId": ObjectID(req.query.chatId),
                    "messageId": req.query.messageId,
                    "senderId": ObjectID(req.auth.credentials._id)
                }
            } catch (ex) {
                return reply({ code: 503, message: "Invalid group chat id" }).code(503);
            }
            middleWare.Select("messages", getMsg, "Mongo", {}, function (err, result) {
                if (err) return reply({ code: 503, message: err.message }).code(503);

                if (result[0]) {
                    if (result[0].members) {
                        if (result[0].totalMembers) finalMsgStatus.totalMembers = parseInt(result[0].totalMembers)

                        var getMemIdArr = [];
                        /** loop through the members and prepare the Array of Ids(for future queries) */
                        async.forEach(Object.keys(result[0].members), function (membObj, membCB) {
                            getMemIdArr.push(ObjectID(membObj))
                            membCB(null)
                        }, function (memLoopErr) {
                            if (memLoopErr) return reply({ code: 500, message: memLoopErr.message }).code(500)

                            middleWare.Select("customer", { _id: { $in: getMemIdArr } }, "Mongo", {}, function (err, doc) {
                                if (err)
                                    return reply({ code: 500, message: err.message }).code(500)

                                if (doc.length > 0) {

                                    /** found some data */
                                    var grpMemObjTemp = {}
                                    async.eachSeries(doc, function (grpMemObj, grpMemCB) {
                                        /** 
                                         * - capture the identifier, username, profile pic and status
                                         * - mark if the user is one of the admin of the group
                                         */
                                        grpMemObjTemp[grpMemObj._id.toString()] = {
                                            userIdentifier: grpMemObj.mobile, // 'mobile' in this app
                                            profilePic: grpMemObj.profilePic
                                        }

                                        grpMemCB(null)
                                    }, function (grpMemLoopErr) {
                                        if (grpMemLoopErr) return reply({ code: 500, message: grpMemLoopErr.message }).code(500)

                                        var getMemIdArr = [];

                                        /** prepare the delivered and read array */
                                        async.forEach(Object.keys(result[0].members), function (memObj, memCB) {

                                            getMemIdArr.push(ObjectID(memObj))

                                            var msgStatsObj = {
                                                memberId: memObj,
                                                profilePic: grpMemObjTemp[memObj].profilePic,
                                                memberIdentifier: grpMemObjTemp[memObj].userIdentifier
                                            };

                                            /** 
                                             * check if its read 
                                             * if yes then capture both delivered and read timestamp
                                             * else capture only the delivered timestamp
                                             */
                                            if (result[0].members[memObj].status == 3) {
                                                msgStatsObj.deliveredAt = result[0].members[memObj].deliveredAt;
                                                msgStatsObj.readAt = result[0].members[memObj].readAt;
                                                finalMsgStatus.readBy.push(msgStatsObj)
                                            } else if (result[0].members[memObj].status == 2) {
                                                msgStatsObj.deliveredAt = result[0].members[memObj].deliveredAt;
                                                finalMsgStatus.deliveredTo.push(msgStatsObj)
                                            }
                                            memCB(null)
                                        }, function (memLoopErr) {
                                            if (memLoopErr) return reply({ code: 500, message: memLoopErr.message }).code(500)

                                            /** final response to user */
                                            return reply({
                                                code: 200,
                                                message: "success",
                                                data: finalMsgStatus
                                            }).code(200);
                                        })

                                    })

                                } else {

                                    /** no valid members found */
                                    return reply({ code: 204, message: "No valid group members found." }).code(200);
                                }
                            })

                        })

                    } else {
                        /** send empty response */
                        /** final response to user */
                        return reply({
                            code: 200,
                            message: "success",
                            data: finalMsgStatus
                        }).code(200);
                    }
                } else {
                    return reply({ code: 404, message: "Message not found / Message not sent by this user." }).code(200);
                }
            })
        }
    ])
}