var Joi = require('joi');
var async = require("async");
var ObjectID = require('mongodb').ObjectID
var middleWare = require("../../Controller/dbMiddleware.js");

/**
 * Define all related routes
 */
module.exports = [
    {
        method: 'DELETE',
        path: '/GroupChat/ByMember',
        handler: function (req, res) {
            /**
             * call the handler function
             */
            removeGroupMemberHandler(req, res);
        },
        config: {
            description: 'Removes a member from a chat group.',
            notes: 'Header input: KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj',
            tags: ['api', 'messages'],
            auth: "user",
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string().description('api authorization').required()
                }).unknown(),
                query: {
                    chatId: Joi.string().description('group chat id').required(),
                    memberId: Joi.string().description('member id to remove').required()
                },
                failAction: function (req, res, source, error) {
                    /**
                     * Common response format, in case the validation fails
                     */
                    return res({ message: error.message }).code(400);
                }
            },
        }
    }];


/**
 * API Name: GET /Chats
 */
function removeGroupMemberHandler(req, reply) {

    var selChatObj = {}
    async.waterfall([
        function (validateCB) {
            validateCB(null, req.decoded);
        },
        function (JWTDecoded, validate_admin_cb) {
            // console.log('>> JWTDecoded: ', req.auth.credentials._id)

            /** Is the API caller have permission to add a member?  */
            var getGroup, admin_mem_status = "members." + req.auth.credentials._id + ".status";
            try {
                getGroup = { _id: ObjectID(req.query.chatId), [admin_mem_status]: "Admin" }
            } catch (exec) {
                return res({ code: 503, message: "Invalid group chat id" }).code(503);
            }

            /** check for the group chat list by id 
             * and this user is one of the admin of the group
             */
            middleWare.Select("chatList", getGroup, "Mongo", {}, function (err, result) {
                if (err) {
                    return res({ code: 503, message: "Unknown error occurred" }).code(503);
                }
                else if (result.length > 0) {
                    /** valid group and user is admin of the group */
                    selChatObj.id = result[0]._id.toString()
                    selChatObj.subject = result[0].subject
                    selChatObj.initiator = result[0].initiatedBy.toString()
                    validate_admin_cb(null, JWTDecoded, result[0])
                }
                else {
                    return reply({ code: 204, message: "Group doesn't exist / Permission denied.", response: {} }).code(200);
                }
            })
        },
        function (JWTDecoded, ChatObj, validate_members_cb) {

            /** Is the members present in our db */
            var memArr = []
            if (req.query.memberId in ChatObj.members) {

                memArr.push(ObjectID(req.query.memberId))
                memArr.push(ObjectID(req.auth.credentials._id))

                /** find the valid members from the list specified */
                middleWare.Select("customer", { _id: { $in: memArr } }, "Mongo", {}, function (err, doc) {
                    if (err)
                        return reply({ code: 500, message: err.message }).code(500)

                    if (doc.length > 0) {

                        /** prepare the members to be added to the group. */
                        async.eachSeries(doc, function (dbMem, dbMemCB) {

                            if (dbMem._id.toString() == req.query.memberId) {
                                selChatObj.identifier = dbMem.mobile
                            }

                            if (dbMem._id.toString() == req.auth.credentials._id) {
                                selChatObj.initiatorIdentifier = dbMem.mobile
                            }

                            dbMemCB(null)
                        }, function (dbMemLoopErr) {
                            if (dbMemLoopErr) return reply({ code: 500, message: dbMemLoopErr.message }).code(500)

                            /** continue to final function with members array */
                            validate_members_cb(null, JWTDecoded)
                        })
                    }
                })

            } else {
                return reply({ code: 404, message: "Memebers not found in group." }).code(200);
            }

        },
        function () {

            const messageDbId = ObjectID()
            /** check if the user not removing himself */
            if (req.query.memberId !== req.auth.credentials._id) {

                /** update the group chatList by removing the member */
                const unset_mem = "members." + req.query.memberId;
                middleWare.UpdateAdvanced("chatList", { $unset: { [unset_mem]: "" }, $push : { message: messageDbId } }, { _id: ObjectID(req.query.chatId) }, {}, "Mongo", function (err) {
                    if (err) return reply({ code: 500, message: err.message }).code(500)

                    var insMsgObj = {
                        "_id": messageDbId,
                        "messageId": '' + new Date().getTime(),
                        "payload": "",
                        "messageType": "2", //(removed 'type') 0- group create, 1- add member 2- remove member, 3- make admin, 4- update grop name, 5 -update profilepic, 6 -leave group
                        "initiatorId": req.auth.credentials._id, // one who 0, 1, 2, 3  
                        "initiatorIdentifier": selChatObj.initiatorIdentifier,  // 0, 1, 2, 3
                        "timestamp": new Date().getTime(),
                        "chatId": ObjectID(req.query.chatId),
                        "toDocId": "",
                        "name": "",
                        "memberId": req.query.memberId,
                        "memberIdentifier": selChatObj.identifier,
                        "senderId": ObjectID(req.auth.credentials._id),
                        "receiverId": ObjectID(req.auth.credentials._id)   // as there is no reciever in group chat
                    }

                    middleWare.Insert("messages", insMsgObj, {}, "Mongo", function (mErr) {
                        if (mErr) return reply({ code: 500, message: err.message }).code(500)

                        /** final response to the user */
                        return reply({ code: 200, message: "Memebers successfully removed." }).code(200);
                    })

                });

            } else {
                return reply({ code: 500, message: "Cannot remove yourself." }).code(200);
            }

        }
    ])

}

