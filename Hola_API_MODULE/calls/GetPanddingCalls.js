const Joi = require("joi");
var redis = require("redis");
var config = require("../../config");

var userList = require("../../models/userList")

module.exports = [
    {
        method: 'Get',
        path: '/pendingCalls',
        handler: function (req, res) {
            myHendler(req, res);
        },
        config: {
            description: 'This API used to get Preferences.',
            tags: ['api', 'calls'],
            auth: "user",
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string(),
                }).unknown(),
            },
            response: {
                status: {
                    200: { message: Joi.any(), data: Joi.any() },
                    204: { message: Joi.any() },
                    400: { message: Joi.any().description("Mandatory feild is missing") },
                    401: { message: Joi.any().description("headers is Invalid") }
                }
            }
        }
    }
];


function myHendler(req, res) {

    try {


        var redisClient = redis.createClient(config.redis.REDIS_PORT, config.redis.REDIS_IP, {
            retry_strategy: function (options) {
            
                // reconnect after
                return Math.min(options.attempt * 100, 3000);
            }
        });
        redisClient.auth(config.redis.REDIS_PASS);
        redisClient.on('connect', function () { console.log('Redis connected from Client'); });

        let key = "callerId:" + req.auth.credentials._id || "--";
        redisClient.hgetall(key, (err, data) => {
            if (data != null) {
                return res({ message: "user found", data: data }).code(200);
            } else {
                return res({ message: "no have Data" }).code(204);
            }
        });
        userList.UpdateById(req.auth.credentials._id, { bage: 0 }, (err, result) => {
            console.log("error APXLSSWP : ", err)
        })
    } catch (error) {
        console.log("error APXLWP : ", error)
    }

}