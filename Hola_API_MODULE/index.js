
var GetChat = require('./Messages/GetChat.js');
var DeleteChat = require('./Messages/DeleteChat.js');
var GetMessages = require('./Messages/GetMessages.js');
var GetCallLogs = require('./Messages/GetCallLogs.js');
var DeleteCallLogs = require('./Messages/DeleteCallLogs.js');
var DeleteMessages = require('./Messages/DeleteMessges.js');
var VerifyOTP = require('./Login/VerifyOTP.js');
var UpdateProfile = require('./Profile/UpdateProfile.js');
var ChangeSocialStatus = require('./Profile/ChangeSocialStatus.js');
var PhoneNumber = require('./Login/PhoneNumber.js');
var GetProfile = require('./Profile/GetProfile.js');
var PostInactiveNotification = require('./notifications/PostInactiveNotification');

const GetParticipantProfile = require('./Profile/GetParticipantProfile.js')
const CreateGroupChat = require('./GroupChat/CreateGroup.js');
const AddMembersToGroup = require('./GroupChat/AddMember.js')
const GetGroupMembers = require('./GroupChat/GetGroupMembers.js')
const RemoveGroupMember = require('./GroupChat/RemoveMember.js')
const MakeAdmin = require('./GroupChat/MakeGroupAdmin.js')
const UpdateGroup = require('./GroupChat/UpdateGroup.js')
const LeaveGroup = require('./GroupChat/LeaveGroup.js')
const DeleteGroup = require('./GroupChat/DeleteGroup.js')
const GetGroupMessages = require('./GroupChat/GetGroupMessages.js')
const UserBlocking = require('./Profile/UserBlocking.js')
const IsUserBlocked = require('./Profile/IsUserBlocked.js')
const PatchiosCallPush = require('./Profile/PatchiosCallPush.js')
var PostIosPush = require('./Profile/PostIosPush.js');
const GroupMessageStatus = require('./GroupChat/GetGroupMessageStatus')
var GetPanddingCalls = require('./calls/GetPanddingCalls');

module.exports = [].concat(GetChat, DeleteChat, GetMessages, GetCallLogs, VerifyOTP,
    UpdateProfile, ChangeSocialStatus, PhoneNumber, GetProfile, GetParticipantProfile,
    CreateGroupChat, AddMembersToGroup, GetGroupMembers, RemoveGroupMember, MakeAdmin, UserBlocking, IsUserBlocked, GroupMessageStatus,
    UpdateGroup, LeaveGroup, DeleteGroup, GetGroupMessages, DeleteCallLogs, PatchiosCallPush,
    GetPanddingCalls, PostIosPush,PostInactiveNotification,DeleteMessages);