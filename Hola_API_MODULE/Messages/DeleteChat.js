var Joi = require('joi');
var async = require("async");
var middleWare = require("../../Controller/dbMiddleware.js");
/**
 * Define all related routes
 */
module.exports = [
    {
        method: 'DELETE',
        path: '/Chats',
        handler: function (req, res) {
            /**
             * call the handler function
             */
            deleteChatListHandler(req, res);
        },
        config: {
            description: 'Delete the chat from chatlist for the user.',
            notes: "Delete the chat from chatlist for the user. <br>Header input: Basic aG91c2U6ZVNTNzlCakVncVlYbmlXNHJTYzI3UDdMOUcyN2p3WFFZMlA0dGZCZW5tekcwb21iRnVKZVNYQ2pYQ3JaNVAwOGIxODBRc3RROTE2ZWtqS3FpZDBMTWRHMmRLTmk1cDRBS1FiSUUxUFZFcnF1RTJuYUx6U0JxR1dYT0hFd0pmR2hKb2daNzdTT0lJYXdEQVpIM1JhUDlablZwQ0NxRVZJRHZRY1ZYaFFRS0tIcTRQaktsRmlubHVkbzBoOXQ3RE9UUklyMndqNmZXV1VHWmZyUTc4UVFURFBTbm1vTFB0U0FhbWxWbEZHZW5waGZYR1dTNks2QTJkQW1nSjlwcTZmbg==",
            tags: ['api', 'messages'],
            auth: "user",
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string().required()
                }).unknown(),
                query: {
                    recipientId: Joi.string().description("Recipient Id"),
                    secretId: Joi.string().description("Secret Id").optional()
                }
            },

        },

    }];

function deleteChatListHandler(req, res) {

    async.waterfall([
        function (updtChatsCB) {
            /**
             * update the 'chatList' with 'members.{req.auth.credentials._id}.inactive: true
             * 
             * fetch all the message ids in 'message' and update from the bottom till we reach last active message 
             */
            var getChat = {
                ["members." + req.auth.credentials._id]: { $exists: true },
                ["members." + req.query.recipientId]: { $exists: true },
                "secretId": (req.query.secretId != "null") ? req.query.secretId : ""
            }

            /**
             * find and modify
             * mark the member as inactive for the specified chat list
             * get the members array for updating
             */
            middleWare.FindAndModify("chatList",
                { $set: { ["members." + req.auth.credentials._id + ".inactive"]: true } },
                getChat,
                "Mongo",
                { message: 1 }, { new: true },
                function (getChatErr, getChatRes) {
                    if (getChatErr) {
                        return res({ code: 503, message: getChatErr.message }).code(503);
                    } else if (getChatRes.value) {

                        middleWare.Delete("chatList", getChat, "Mongo", () => { })

                        /**  iterate from last message till the one wich is already deleted */
                        async.eachSeries((getChatRes.value.message).reverse(), function (msgId, msgCB) {

                            /**
                             * update the message marking its deleted for the specified member 
                             */
                            middleWare.FindAndModify("messages",
                                { $set: { ["members." + req.auth.credentials._id + ".del"]: true } },
                                { _id: msgId, ["members." + req.auth.credentials._id + ".del"]: { $exists: false } },
                                "Mongo",
                                { messageId: 1 }, { new: true },
                                function (updtErrr, updtResult) {
                                    if (updtErrr) {
                                        console.log('updtErrr: ', updtErrr)
                                    } else {
                                        /**
                                         * check if the findAndModify found any document to update
                                         * if yes then continue else skip message updates
                                         */
                                        if (updtResult.lastErrorObject.updatedExisting) {
                                            msgCB(null)
                                        } else {
                                            updtChatsCB(null)
                                        }
                                    }

                                })

                        }, function (msgUpdtErr) {
                            /** Message loop completes */
                            if (msgUpdtErr) {
                                return res({ code: 200, message: msgUpdtErr.message }).code(503)
                            }

                            updtChatsCB(null)
                        });

                    }
                    else {
                        /** No messages to update */
                        updtChatsCB(null)
                    }
                })
        }
    ], function (asyncErr) {
        if (asyncErr) {
            return res({ code: 200, message: asyncErr.message }).code(503)
        } else {
            return res({ code: 200, message: "chat deleted." }).code(200)
        }
    });
}