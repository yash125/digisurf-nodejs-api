var Joi = require('joi');
var ObjectID = require('mongodb').ObjectID
var middleWare = require("../../Controller/dbMiddleware.js");

/**
 * Define all related routes
 */
module.exports = [
    {
        method: 'DELETE',
        path: '/Messages',
        handler: function (req, res) {
            /**
             * call the handler function
             */
            var or_clause = [];
            var usr_msg_del = "";
            if (req.query.all && req.query.all == 'true') {
                /**
                 * delete all messages
                 */
                let targetUserId = req.query.messageIds;

                or_clause = [];
                try {
                    or_clause.push({ senderId: ObjectID(req.auth.credentials._id), receiverId: ObjectID(targetUserId) })
                    or_clause.push({ receiverId: ObjectID(req.auth.credentials._id), senderId: ObjectID(targetUserId) })
                } catch (exec) {
                    return res({ message: exec.message }).code(400);
                }

                usr_msg_del = "members." + req.auth.credentials._id + ".del"
                middleWare.Update("messages",
                    { [usr_msg_del]: true },
                    { $or: or_clause },
                    "Mongo",
                    function (updtErr) {
                        if (updtErr) {
                            console.log("Error : messages 101 ", err);
                        }
                    });
            } else {
                /**
                 * delete selected messages
                 */
                if (req.query.messageIds && (Array.isArray(req.query.messageIds) && ((req.query.messageIds).length > 0))) {
                    
                    or_clause = []
                    try {
                        or_clause.push({ senderId: ObjectID(req.auth.credentials._id) })
                        or_clause.push({ receiverId: ObjectID(req.auth.credentials._id) })
                    } catch (exec) {
                        return res({ message: exec.message }).code(400);
                    }

                    usr_msg_del = "members." + req.auth.credentials._id + ".del"
                    middleWare.Update("messages",
                        { [usr_msg_del]: true },
                        { $and: [{ messageId: { $in: req.query.messageIds } }, { $or: or_clause }] },
                        "Mongo",
                        function () {
                        });
                }

            }
            /**
             * update the status of the message as deleted
             * and publish to the same user (multi device support)
             */
            return res({ code: 200, message: "In progress.." }).code(200);
        },
        config: {
            description: 'Delete a specific message.',
            notes: 'Delete a specific message. Header input: Basic aG91c2U6ZVNTNzlCakVncVlYbmlXNHJTYzI3UDdMOUcyN2p3WFFZMlA0dGZCZW5tekcwb21iRnVKZVNYQ2pYQ3JaNVAwOGIxODBRc3RROTE2ZWtqS3FpZDBMTWRHMmRLTmk1cDRBS1FiSUUxUFZFcnF1RTJuYUx6U0JxR1dYT0hFd0pmR2hKb2daNzdTT0lJYXdEQVpIM1JhUDlablZwQ0NxRVZJRHZRY1ZYaFFRS0tIcTRQaktsRmlubHVkbzBoOXQ3RE9UUklyMndqNmZXV1VHWmZyUTc4UVFURFBTbm1vTFB0U0FhbWxWbEZHZW5waGZYR1dTNks2QTJkQW1nSjlwcTZmbg==',
            tags: ['api', 'messages'],
            auth: "user",
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string()
                }).unknown(),
                query: {
                    all: Joi.string().description("Delete all / selected"),
                    messageIds: Joi.any().description("Array of message ids").allow([]),
                }
            }
        }
    }];