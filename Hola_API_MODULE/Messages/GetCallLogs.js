var Joi = require('joi');
var ObjectId = require('mongodb').ObjectID
var middleWare = require("../../Controller/dbMiddleware.js");
/**
 * Define all related routes
 */
module.exports = [
    {
        method: 'GET',
        path: '/CallLogs',
        handler: function (req, res) {
            /**
             * call the handler function
             */
            var timeStamp = parseInt(req.query.timeStamp);
            var userId = req.auth.credentials._id;
            var userParams = "members." + userId + ".deleteAt";

            var condition = [
                { $match: { $or: [{ "userId": ObjectId(userId) }, { "targetId": ObjectId(userId) }] } },
                { $match: { [userParams]: { $exists: false }, "creation": { $gt: timeStamp } } },
                {
                    $project: {
                        "opponetId": {
                            "$cond": {
                                if: { $eq: ["$userId", ObjectId(userId)] },
                                then: "$targetId", else: "$userId"
                            }
                        },
                        "callInitiated": {
                            "$cond": {
                                if: { $eq: ["$userId", ObjectId(userId)] },
                                then: true, else: false
                            }
                        },
                        "_id": 0, "calltime": "$creation", "callType": "$typeOfCall", "callId": "$callId", "secondsDiff": "$secondsDiff"
                    }
                },
                { "$lookup": { "from": "customer", "localField": "opponetId", "foreignField": "_id", "as": "user" } },
                { $unwind: "$user" },
                {
                    $project: {
                        "calltime": "$calltime",
                        "callInitiated": 1,
                        "callType": "$callType",
                        "callId": "$callId",
                        "callDuration": "$secondsDiff",
                        "opponentUid": "$user._id",
                        "opponentProfilePic": "$user.profilePic",
                        "userName": "$user.userName",
                        "opponentNumber": "$user.mobile"
                    }
                },
                { $sort: { calltime: -1 } }];
            middleWare.AggreGate("callLogs", condition, "Mongo", function (err, result) {


                if (err) {
                    return res({ code: 503, message: "success", response: [] }).code(503);
                } else {
                    return res({ code: 200, message: "success", response: result }).code(200);
                }
            });
        },
        config: {
            description: 'Get the Call Logs for the user.',
            notes: 'Get the call logs for the user. Header input: Basic aG91c2U6ZVNTNzlCakVncVlYbmlXNHJTYzI3UDdMOUcyN2p3WFFZMlA0dGZCZW5tekcwb21iRnVKZVNYQ2pYQ3JaNVAwOGIxODBRc3RROTE2ZWtqS3FpZDBMTWRHMmRLTmk1cDRBS1FiSUUxUFZFcnF1RTJuYUx6U0JxR1dYT0hFd0pmR2hKb2daNzdTT0lJYXdEQVpIM1JhUDlablZwQ0NxRVZJRHZRY1ZYaFFRS0tIcTRQaktsRmlubHVkbzBoOXQ3RE9UUklyMndqNmZXV1VHWmZyUTc4UVFURFBTbm1vTFB0U0FhbWxWbEZHZW5waGZYR1dTNks2QTJkQW1nSjlwcTZmbg==',
            tags: ['api', 'messages'],
            auth: "user",
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string()
                }).unknown(),
                query: {
                    timeStamp: Joi.string().description("timeStamp"),
                }
            }
        }
    }];