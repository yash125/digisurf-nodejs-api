var Joi = require('joi');
var middleWare = require("../../Controller/dbMiddleware.js");

/**
 * Define all related routes
 */
module.exports = [
    {
        method: 'DELETE',
        path: '/CallLogs',
        handler: function (req, res) {
            /**
             * call the handler function
             */            
            var userId = req.auth.credentials._id;
            var params = "members." + [userId] + ".deleteAt";
            var data = {
                [params]: new Date().getTime()
            };

            var isDeleteAll = (req.headers.deleteAll) ? parseInt(req.headers.deleteAll) : parseInt(req.headers.deleteall);

            if (isDeleteAll) {
                var userExists = "members." + userId;
                condition = { [userExists]: { $exists: true }, params: { $exists: false } };
                middleWare.Update("callLogs", data, condition, "Mongo", function (err) {
                    if (err) {
                        return res({ code: 503, message: "Error" }).code(503);
                    }
                });

            } else {
                var callIds = (req.headers.callIds) ? req.headers.callIds : req.headers.callids;

                callIds = callIds.replace(/'/g, '');
                callIds = JSON.parse(callIds);
                var condition = {};
                for (index = 0; index < callIds.length; index++) {
                    condition = { "callId": callIds[index] };
                    middleWare.Update("callLogs", data, condition, "Mongo", function (err) {
                        if (err) {
                            return res({ code: 503, message: "Error" }).code(503);
                        }
                    });
                }
            }
            return res({ code: 200, message: "Success" }).code(200);            
        },
        config: {
            description: 'Get the Call Logs for the user.',
            notes: 'Get the call logs for the user. Header input: Basic aG91c2U6ZVNTNzlCakVncVlYbmlXNHJTYzI3UDdMOUcyN2p3WFFZMlA0dGZCZW5tekcwb21iRnVKZVNYQ2pYQ3JaNVAwOGIxODBRc3RROTE2ZWtqS3FpZDBMTWRHMmRLTmk1cDRBS1FiSUUxUFZFcnF1RTJuYUx6U0JxR1dYT0hFd0pmR2hKb2daNzdTT0lJYXdEQVpIM1JhUDlablZwQ0NxRVZJRHZRY1ZYaFFRS0tIcTRQaktsRmlubHVkbzBoOXQ3RE9UUklyMndqNmZXV1VHWmZyUTc4UVFURFBTbm1vTFB0U0FhbWxWbEZHZW5waGZYR1dTNks2QTJkQW1nSjlwcTZmbg==',
            tags: ['api', 'messages'],
            auth: "user",
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string(),
                    callIds: Joi.string().description("enter callIds in JSON array"),
                    deleteAll: Joi.string(),
                }).unknown()
            }
        }
    }];