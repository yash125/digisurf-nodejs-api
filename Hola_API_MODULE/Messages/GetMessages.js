var ObjectID = require('mongodb').ObjectID
var async = require("async");
var Joi = require('joi');
var middleWare = require("../../Controller/dbMiddleware.js");
var ComnFun = require('../../ComnFun.js');
// var dasboardModule = require("../../soketMqtt/entrypoint.js");

module.exports = [
    {
        method: 'GET',
        path: '/Messages',
        handler: function (req, res) {
            GetMessagesHendler(req, res);
        },
        config: {
            tags: ['api', 'messages'],
            description: 'Get the messages for the specified chat',
            auth: "user",
            notes: 'Get the messages for the specified chat. Header input: Basic aG91c2U6ZVNTNzlCakVncVlYbmlXNHJTYzI3UDdMOUcyN2p3WFFZMlA0dGZCZW5tekcwb21iRnVKZVNYQ2pYQ3JaNVAwOGIxODBRc3RROTE2ZWtqS3FpZDBMTWRHMmRLTmk1cDRBS1FiSUUxUFZFcnF1RTJuYUx6U0JxR1dYT0hFd0pmR2hKb2daNzdTT0lJYXdEQVpIM1JhUDlablZwQ0NxRVZJRHZRY1ZYaFFRS0tIcTRQaktsRmlubHVkbzBoOXQ3RE9UUklyMndqNmZXV1VHWmZyUTc4UVFURFBTbm1vTFB0U0FhbWxWbEZHZW5waGZYR1dTNks2QTJkQW1nSjlwcTZmbg==',
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string()
                }).unknown(),
                query: {
                    chatId: Joi.string().description("Unique id for chat"),
                    timestamp: Joi.any().description("timestamp"),
                    pageSize: Joi.any().description("page size"),
                }
            }
        }
    }];



function GetMessagesHendler(req, res) {
    async.waterfall([

        function () {

            var pagesize = parseInt(req.query.pageSize) || 100;

            var usr_msg_del = "members." + req.auth.credentials._id + ".del"
            var getMesg = [
                { "$match": { "chatId": ObjectID(req.query.chatId) } },
                { "$match": { "timestamp": { $lt: parseInt(req.query.timestamp) } } },
                { "$match": { $or: [{ "expDtime": 0 }, { "expDtime": { $gt: new Date().getTime() } }] } },
                { "$match": { [usr_msg_del]: { "$exists": false } } },
                {
                    "$project": {
                        _id: 0, messageId: 1, secretId: 1, senderId: 1, receiverId: 1, payload: 1,
                        messageType: 1, timestamp: 1, chatId: 1, userImage: 1, toDocId: 1, name: 1,
                        dTime: 1, dataSize: 1, thumbnail: 1, mimeType: 1, fileName: 1, extension: 1,
                        members: 1, previousReceiverIdentifier: 1, previousFrom: 1, previousPayload: 1,
                        previousType: 1, previousId: 1, replyType: 1, previousFileType: 1, removedAt: 1,
                        wasEdited: 1, postId: 1, postType: 1, postTitle: 1, callType: 1,
                        amount: 1, transferStatus: 1, transferStatusText: 1, duration: 1
                    }
                },
                {
                    "$lookup": {
                        "from": "customer", "localField": "senderId", "foreignField": "_id", as: "userDetails"
                    }
                },
                { "$unwind": "$userDetails" },
                { "$match": { "userDetails.userStatus": 1 } },
                {
                    "$project": {
                        _id: 0, messageId: 1, secretId: 1, senderId: 1, receiverId: 1, payload: 1,
                        messageType: 1, timestamp: 1, chatId: 1, userImage: 1, toDocId: 1, name: 1,
                        dTime: 1, dataSize: 1, thumbnail: 1, mimeType: 1, fileName: 1, extension: 1,
                        members: 1, previousReceiverIdentifier: 1, previousFrom: 1, previousPayload: 1,
                        previousType: 1, previousId: 1, replyType: 1, previousFileType: 1, removedAt: 1,
                        wasEdited: 1, postId: 1, postType: 1, postTitle: 1, callType: 1, duration: 1,
                        amount: 1, transferStatus: 1, transferStatusText: 1, isStar: "$userDetails.starRequest.starUserProfileStatusCode"
                    }
                },
                { "$sort": { "timestamp": -1 } },
                { "$limit": pagesize }
            ];

            middleWare.AggreGate("messages", getMesg, "Mongo", function (err, result) {
                if (err) {
                    console.log("err : ", err)
                    return res({ message: "Unknown error occurred" }).code(503);
                }
                else if (result.length) {
                    
                    for (index = 0; index < result.length; index++) {
                        if (result[index].messageType != "1" && result[index].messageType != "2" && result[index].messageType != "7" && result[index].replyType != "1" && result[index].replyType != "2" && result[index].replyType != "7") {
                            delete result[index].thumbnail;
                        }
                        if (result[index].dataSize == null) {
                            delete result[index].dataSize;
                        }
                    }

                    var finalRes = [], oponentUid = '', secretId = '', dTime = ''
                    /**
                     * Loop through the result and prepare the final result 
                     */
                    async.eachSeries(result, function (resObj, resCB) {

                        /**
                         * keeping the common fields out of the response array
                         * OponentID and SecretID are common for all messages
                         */
                        if (oponentUid == '') {
                            oponentUid = (resObj.senderId == req.auth.credentials._id) ? resObj.receiverId : resObj.senderId
                            secretId = resObj.secretId
                            dTime = resObj.dTime
                        }

                        var msgObj = {
                            "initiated": (resObj.senderId == req.auth.credentials._id),
                            "messageId": resObj.messageId,
                            "senderId": resObj.senderId,
                            "receiverId": resObj.receiverId,
                            "payload": resObj.payload,
                            "messageType": resObj.messageType,
                            "type": resObj.messageType,
                            "timestamp": resObj.timestamp,
                            "userImage": resObj.userImage,
                            "toDocId": resObj.toDocId,
                            "name": resObj.name,
                            "status": (resObj.members[resObj.receiverId] && resObj.members[resObj.receiverId].status) ? resObj.members[resObj.receiverId].status : 1,
                            "delivered": (resObj.members[resObj.receiverId] && resObj.members[resObj.receiverId].deliveredAt) ? resObj.members[resObj.receiverId].deliveredAt : -1,
                            "read": (resObj.members[resObj.receiverId] && resObj.members[resObj.receiverId].readAt) ? resObj.members[resObj.receiverId].readAt : -1,
                            "removedAt": resObj.removedAt,
                            "wasEdited": resObj.wasEdited,
                            "isStar": (resObj.isStar == 4)
                        }

                        if (resObj.messageType == "16" || resObj.messageType == "17") msgObj["typeOfCall"] = resObj.callType || "0"
                        if (resObj.messageType == "16" || resObj.messageType == "17") msgObj["callId"] = resObj.callId || ""
                        if (resObj.messageType == "17") {
                            msgObj["duration"] = resObj.duration;
                            msgObj["isIncommingCall"] = resObj.receiverId == req.auth.credentials._id;
                        }

                        if (resObj.messageType == "15") {
                            msgObj["amount"] = "" + resObj.amount;
                            msgObj["transferStatus"] = "" + resObj.transferStatus;
                            msgObj["transferStatusText"] = resObj.transferStatusText;
                        }
                        /**
                         * add dTime only if the its a secret chat
                         */
                        if (secretId != '') {
                            msgObj.dTime = resObj.dTime
                        }

                        /**
                         * check if the message type is of image / video / audio / doodle
                         * and add the additional fields (thumbnail, dataSize)
                         */
                        switch (resObj.messageType) {
                            case '1':
                            case '2':
                            case '7':
                                msgObj.thumbnail = resObj.thumbnail
                                msgObj.dataSize = resObj.dataSize
                                break;
                            case '5':
                                msgObj.dataSize = resObj.dataSize
                                break;
                            case '9':
                                msgObj.dataSize = resObj.dataSize
                                msgObj.extension = resObj.extension
                                msgObj.fileName = resObj.fileName
                                msgObj.mimeType = resObj.mimeType
                                break;
                            case '10':
                                msgObj.dataSize = resObj.dataSize
                                if (resObj.replyType == '9') {
                                    msgObj.extension = resObj.extension
                                    msgObj.fileName = resObj.fileName
                                    msgObj.mimeType = resObj.mimeType
                                }

                                msgObj.previousReceiverIdentifier = resObj.previousReceiverIdentifier
                                msgObj.previousFrom = resObj.previousFrom
                                msgObj.previousPayload = resObj.previousPayload
                                msgObj.previousType = resObj.previousType
                                msgObj.previousId = resObj.previousId
                                msgObj.replyType = resObj.replyType
                                if (resObj.replyType == '1' || resObj.replyType == '2' || resObj.replyType == '7')
                                    msgObj.thumbnail = resObj.thumbnail
                                if (resObj.previousType == '9')
                                    msgObj.previousFileType = resObj.previousFileType
                                break;
                            case '13':
                                msgObj.postId = resObj.postId || "";
                                msgObj.postType = resObj.postType;
                                msgObj.postTitle = resObj.postTitle || "";
                                break;
                        }

                        finalRes.push(msgObj);
                        resCB(null);
                    }, function (FinalLoopErr) {
                        if (FinalLoopErr) {
                            console.log("FinalLoopErr : ", FinalLoopErr)
                            return res({ message: finalLoopErr.message }).code(503);
                        }

                        /**
                         * Success status goes with API response. 
                         * Actual data goes through MQTT
                         */
                        res({ code: 200, message: "Success", data: finalRes }).code(200);

                        ComnFun.publishMqtt("GetMessages/" + req.auth.credentials._id, JSON.stringify({
                            "chatId": req.query.chatId, "opponentUid": oponentUid, "secretId": secretId,
                            "dTime": dTime, "messages": finalRes
                        }));
                        // dasboardModule.WabApp_GetMessages({ "topic": "GetMessages", "data": finalRes, "userId": req.auth.credentials._id, "chatId": req.query.chatId });
                    })
                }
                else {
                    /** No messages to send */
                    res({ code: 200, message: "No Data Found.", data: {} }).code(200);

                    ComnFun.publishMqtt("GetMessages/" + req.auth.credentials._id, JSON.stringify({
                        "chatId": req.query.chatId, "opponentUid": '', "secretId": '', "dTime": 0, "messages": []
                    }))
                    // dasboardModule.WabApp_GetMessages({ "topic": "GetMessages", "data": [], "chatId": req.query.chatId, "userId": req.auth.credentials._id });
                }
            })
        }
    ])
}