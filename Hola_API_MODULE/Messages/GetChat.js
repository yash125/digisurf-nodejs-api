var ObjectID = require('mongodb').ObjectID
var Joi = require('joi');
var async = require("async");
var middleWare = require("../../Controller/dbMiddleware.js");
var ComnFun = require('../../ComnFun.js');
/**
 * Define all related routes
 */
module.exports = [
    {
        method: 'GET',
        path: '/Chats',
        handler: function (req, res) {
            /**
             * call the handler function
             */
            getChatListHandler(req, res);
        },
        config: {
            description: 'Get the chat lists for the user.',
            notes: 'Get the chat lists for the user. Header input: Basic aG91c2U6ZVNTNzlCakVncVlYbmlXNHJTYzI3UDdMOUcyN2p3WFFZMlA0dGZCZW5tekcwb21iRnVKZVNYQ2pYQ3JaNVAwOGIxODBRc3RROTE2ZWtqS3FpZDBMTWRHMmRLTmk1cDRBS1FiSUUxUFZFcnF1RTJuYUx6U0JxR1dYT0hFd0pmR2hKb2daNzdTT0lJYXdEQVpIM1JhUDlablZwQ0NxRVZJRHZRY1ZYaFFRS0tIcTRQaktsRmlubHVkbzBoOXQ3RE9UUklyMndqNmZXV1VHWmZyUTc4UVFURFBTbm1vTFB0U0FhbWxWbEZHZW5waGZYR1dTNks2QTJkQW1nSjlwcTZmbg==',
            tags: ['api', 'messages'],
            auth: "user",
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string()
                }).unknown(),
                query: {
                    pageNo: Joi.string().description("PageNo"),
                }
            },
        }
    }];


/**
 * API Name: GET /Chats
 */
function getChatListHandler(req, res) {
    async.waterfall([
        function () {

            var limit = 100, skip = 0;
            if (req.query && req.query.pageNo && req.query.pageNo.toString() != "") {
                skip = req.query.pageNo * 100;
                limit = 100 + skip;
            }

            var userID = "members." + req.auth.credentials._id;
            var chatListAggr = [
                { "$match": { [userID]: { "$exists": true }, [userID + ".inactive"]: { "$exists": false }, [userID + ".left"]: { "$ne": false } } },
                { "$project": { "id": 1, "initiatedBy": 1, "chatMembers": "$members", "subject": 1, "image": 1, "messageDBId": { $slice: ["$message", -1] }, "chatType": 1 } },
                { "$unwind": "$messageDBId" },
                {
                    "$lookup": {
                        "from": "messages", "localField": "messageDBId", "foreignField": "_id", as: "messageDetails"
                    }
                },
                { "$unwind": "$messageDetails" },
                {
                    "$project": {
                        "initiatedBy": 1, "chatMembers": 1, "subject": 1, "image": 1, "chatType": 1, "chatId": "$messageDetails.chatId", "secretId": "$messageDetails.secretId", "dTime": "$messageDetails.dTime", "messageId": "$messageDetails.messageId",
                        "payload": "$messageDetails.payload", "memberId": "$messageDetails.memberId", "memberIdentifier": "$messageDetails.memberIdentifier", "messageType": "$messageDetails.messageType", "timestamp": "$messageDetails.timestamp",
                        "previousReceiverIdentifier": "$messageDetails.previousReceiverIdentifier", "previousFrom": "$messageDetails.previousFrom", "previousPayload": "$messageDetails.previousPayload",
                        "previousType": "$messageDetails.previousType", "previousId": "$messageDetails.previousId", "replyType": "$messageDetails.replyType", "previousFileType": "$messageDetails.previousFileType",
                        "receiverId": "$messageDetails.receiverId", "members": "$messageDetails.members", "senderId": "$messageDetails.senderId",
                        "initiatorId": "$messageDetails.initiatorId", "initiatorIdentifier": "$messageDetails.initiatorIdentifier",
                        "postId": "$messageDetails.postId", "postType": "$messageDetails.postType", "postTitle": "$messageDetails.postTitle",
                        "targetUserId": { "$cond": { if: { $eq: ["$messageDetails.senderId", ObjectID(req.auth.credentials._id)] }, then: "$messageDetails.receiverId", else: "$messageDetails.senderId" } },
                        "removedAt": "$messageDetails.removedAt", "callType": "$messageDetails.callType",
                        "amount": "$messageDetails.amount", "transferStatus": "$messageDetails.transferStatus",
                        "transferStatusText": "$messageDetails.transferStatusText", "duration": "$messageDetails.duration"
                    },
                },
                {
                    "$lookup": {
                        "from": "customer", "localField": "targetUserId", "foreignField": "_id", as: "targetDetails"
                    }
                },
                { "$unwind": "$targetDetails" },
                {
                    "$project": {
                        "_id": 0, "initiatedBy": 1, "chatMembers": 1, "subject": 1, "image": 1, "chatType": 1, "chatId": 1, "secretId": 1, "dTime": 1, "messageId": 1, "payload": 1, "members": 1, "messageType": 1, "timestamp": 1, "senderId": 1, "receiverId": 1,
                        "previousReceiverIdentifier": 1, "previousFrom": 1, "previousPayload": 1, "previousType": 1, "previousId": 1, "replyType": 1, "previousFileType": 1,
                        "initiatorId": 1, "initiatorIdentifier": 1, "memberId": 1, "memberIdentifier": 1,
                        "profilePic": "$targetDetails.profilePic", "number": "$targetDetails.mobile", "userName": "$targetDetails.userName",
                        "recipientId": "$targetDetails._id", "profileVideo": "$targetDetails.profileVideo",
                        "profileVideoThumbnail": "$targetDetails.profileVideoThumbnail",
                        "removedAt": 1, "postId": 1, "postType": 1, "postTitle": 1, "callType": 1,
                        "amount": 1, "transferStatus": 1, "transferStatusText": 1, "duration": 1,
                        "isStar": "$targetDetails.starRequest.starUserProfileStatusCode"
                    }
                },
                { "$sort": { "timestamp": -1 } },
                { "$skip": skip }, { "$limit": limit },
                { "$sort": { "messageId": -1 } }
            ];
            middleWare.AggreGate("chatList", chatListAggr, "Mongo", function (err, result) {

                if (err) {
                    return res({ message: "Unknown error occurred" }).code(503);
                }
                else if (result.length) {

                    var condition, userId, targetId, index = 0;

                    async.each(result, function (val, callbackloop) {

                        userId = "members." + req.auth.credentials._id;
                        targetId = "messageDetails.members." + req.auth.credentials._id + ".readAt";
                        if (val.chatType == 'GroupChat') {
                            /** query for group chat */
                            condition = [
                                { "$match": { "_id": ObjectID(val.chatId) } },
                                // { "$match": { [userId]: { "$exists": true }, secretId: val.secretId } },
                                { "$unwind": "$message" },
                                { "$lookup": { "from": "messages", "localField": "message", "foreignField": "_id", as: "messageDetails" } },
                                { "$unwind": "$messageDetails" },
                                { "$match": { "messageDetails.payload": { $ne: "" } } },
                                // { "$match": { "messageDetails.senderId": ObjectID(val.senderId) } },
                                // { "$match": { "messageDetails.receiverId": ObjectID(req.auth.credentials._id) } },
                                { "$match": { ["messageDetails.members." + req.auth.credentials._id]: { $exists: true } } },
                                { "$match": { [targetId]: { "$exists": false } } },
                                { "$group": { _id: req.auth.credentials._id, "totalUnread": { $sum: 1 } } }
                            ];
                        } else {

                            condition = [
                                { "$match": { "_id": ObjectID(val.chatId) } },
                                { "$match": { [userId]: { "$exists": true }, secretId: val.secretId } },
                                { "$unwind": "$message" },
                                { "$lookup": { "from": "messages", "localField": "message", "foreignField": "_id", as: "messageDetails" } },
                                { "$unwind": "$messageDetails" },
                                { "$match": { "messageDetails.payload": { $ne: "" } } },
                                { "$match": { "messageDetails.senderId": ObjectID(val.senderId) } },
                                { "$match": { "messageDetails.receiverId": ObjectID(req.auth.credentials._id) } },
                                { "$match": { [targetId]: { "$exists": false } } },
                                { "$group": { _id: req.auth.credentials._id, "totalUnread": { $sum: 1 } } }
                            ];
                        }
                        middleWare.AggreGate("chatList", condition, "Mongo", function (err, resultA) {
                            if (err) {
                                val["totalUnread"] = 0;
                            } else if (resultA[0]) {
                                val["totalUnread"] = resultA[0].totalUnread;
                            } else {
                                val["totalUnread"] = 0;
                            }
                            if (result[index].messageType != "1" && result[index].messageType != "2" && result[index].messageType != "7") {
                                delete result[index].thumbnail;
                            }
                            index++;
                            callbackloop(err, resultA);
                        });

                    }, function (err) {
                        if (err) {
                            return res({ code: 200, message: "DB Error" }).code(500);
                        } else {

                            /**
                             * Success status goes with API response. 
                             * Actual data goes through MQTT
                             */
                            var finalRes = []
                            async.eachSeries(result, function (resObj, resCB) {
                                /**
                                 * Loop throught the result and prepare the final data
                                 */

                                var finalChatObj = {
                                    "initiated": ((resObj.initiatedBy).toString() == req.auth.credentials._id) ? true : false,
                                    "chatId": resObj.chatId,
                                    "groupChat": (resObj.chatType == "GroupChat") ? true : false,
                                    "userName": (resObj.chatType == "GroupChat") ? resObj.subject : resObj.userName,
                                    "profilePic": (resObj.chatType == "GroupChat") ? resObj.image : resObj.profilePic,
                                    "secretId": resObj.secretId,
                                    "dTime": resObj.dTime,
                                    "messageId": resObj.messageId,
                                    "payload": resObj.payload,
                                    "messageType": resObj.messageType,
                                    "timestamp": resObj.timestamp,
                                    "receiverId": resObj.receiverId,
                                    "senderId": resObj.senderId,
                                    "number": resObj.mobile,
                                    "recipientId": resObj.recipientId,
                                    "memberId": resObj.memberId,
                                    "memberIdentifier": resObj.memberIdentifier,
                                    "previousReceiverIdentifier": resObj.previousReceiverIdentifier,
                                    "previousFrom": resObj.previousFrom,
                                    "previousPayload": resObj.previousPayload,
                                    "previousType": resObj.previousType,
                                    "previousId": resObj.previousId,
                                    "replyType": resObj.replyType,
                                    "previousFileType": resObj.previousFileType,
                                    "status": (resObj.members) ? ((resObj.members[resObj.receiverId] && resObj.members[resObj.receiverId].status) ? resObj.members[resObj.receiverId].status : 1) : 1,
                                    "totalUnread": resObj.totalUnread,
                                    "removedAt": resObj.removedAt,
                                    "postId": resObj.postId,
                                    "postType": resObj.postType,
                                    "postTitle": resObj.postTitle,
                                    "profileVideo": resObj.profileVideo,
                                    "profileVideoThumbnail": resObj.profileVideoThumbnail,
                                    "isStar": (resObj.isStar == 4)
                                }

                                if (resObj.messageType == "16" || resObj.messageType == "17") finalChatObj["typeOfCall"] = resObj.callType || "0"
                                if (resObj.messageType == "16" || resObj.messageType == "17") finalChatObj["callId"] = resObj.callId || ""
                                if (resObj.messageType == "17") {
                                    finalChatObj["duration"] = resObj.duration;
                                    finalChatObj["isIncommingCall"] = resObj.receiverId == req.auth.credentials._id;
                                }

                                if (resObj.messageType == "15") {
                                    finalChatObj["amount"] = "" + resObj.amount;
                                    finalChatObj["transferStatus"] = "" + resObj.transferStatus;
                                    finalChatObj["transferStatusText"] = resObj.transferStatusText;
                                }


                                if (resObj.chatType == "GroupChat") {

                                    if (finalChatObj.payload == "")
                                        delete finalChatObj.payload

                                    finalChatObj.groupSubject = resObj.subject
                                    finalChatObj.initiatorId = resObj.initiatorId
                                    finalChatObj.initiatorIdentifier = resObj.initiatorIdentifier
                                }

                                if (resObj.chatMembers[req.auth.credentials._id].left) {
                                    finalChatObj.leftGroup = true
                                }

                                finalRes.push(finalChatObj)
                                resCB(null);
                            },
                                function (finalLoopErr) {
                                    if (finalLoopErr) {
                                        return res({ message: finalLoopErr.message }).code(503);
                                    }

                                    var IdsToDelete = [];

                                    finalRes.forEach(ele => {
                                        if (finalRes.filter(e =>
                                            e.groupChat == false &&
                                            e.secretId == ele.secretId &&
                                            (e.receiverId.toString() == ele.receiverId.toString() || e.senderId.toString() == ele.receiverId.toString()) &&
                                            (e.senderId.toString() == ele.senderId.toString() || e.senderId.toString() == ele.receiverId.toString()) &&
                                            e.userName == ele.userName) &&
                                            finalRes.filter(e =>
                                                e.groupChat == false &&
                                                e.secretId == ele.secretId &&
                                                (e.receiverId.toString() == ele.receiverId.toString() || e.senderId.toString() == ele.receiverId.toString()) &&
                                                (e.senderId.toString() == ele.senderId.toString() || e.senderId.toString() == ele.receiverId.toString()) &&
                                                e.userName == ele.userName
                                            ).length >= 2) {
                                            let filterArray = finalRes.filter(e =>
                                                e.groupChat == false &&
                                                e.secretId == ele.secretId &&
                                                (e.receiverId.toString() == ele.receiverId.toString() || e.senderId.toString() == ele.receiverId.toString()) &&
                                                (e.senderId.toString() == ele.senderId.toString() || e.senderId.toString() == ele.receiverId.toString()) &&
                                                  e.userName == ele.userName
                                            ).map(e => e.chatId)
                                            for (let index = 1; index < filterArray.length; index++) {
                                                IdsToDelete.push(filterArray[index])
                                            }
                                        }
                                        if (ele.groupChat == false && ele.receiverId == ele.senderId) {
                                            IdsToDelete.push(ele.chatId.toString())
                                        }
                                    })

                                    if (IdsToDelete && IdsToDelete.length) {
                                        middleWare.Delete("chatList", { _id: { "$in": IdsToDelete.map(e => ObjectID(e)) } }, "Mongo", (err, result) => { if (err) console.log("AKOLEOVP : ", err) });
                                        middleWare.Delete("messages", { chatId: { "$in": IdsToDelete.map(e => ObjectID(e)) } }, "Mongo", (err, result) => { if (err) console.log("AKOLEOVP : ", err) });
                                        finalRes = finalRes.filter(e => !IdsToDelete.includes(e.chatId))
                                    }


                                    ComnFun.publishMqtt("GetChats/" + req.auth.credentials._id, JSON.stringify({ "chats": finalRes }))
                                    // dasboardModule.WabApp_GetChatList({ "topic": "GetChat", "data": finalRes, "userId": req.auth.credentials._id });
                                    // return res({ code: 200, message: "success", response: result }).code(200);
                                    return res({ code: 200, message: "success", response: finalRes }).code(200);
                                })
                        }
                    })

                }
                else {
                    ComnFun.publishMqtt("GetChats/" + req.auth.credentials._id, JSON.stringify({ "chats": [] }))
                    // dasboardModule.WabApp_GetChatList({ "data": [] });
                    return res({ code: 404, message: "No data Found." }).code(200);
                }
            });
        }
    ]);
}