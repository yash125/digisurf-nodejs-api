const Joi = require('joi');
const ObjectID = require('mongodb').ObjectID
const middleWare = require("../../Controller/dbMiddleware.js");

module.exports = [
    {

        method: 'POST',
        path: '/inactiveNotification',
        handler: function (req, reply) {
            var _id = req.auth.credentials._id;
            var targetUserId = ObjectID(req.payload.targetUserId);
            var isMuteNotification = req.payload.isMuteNotification;
        
            if (isMuteNotification) {
                middleWare.UpdateWithPush("customer", { "$push": { "inActiveUsersNotification": targetUserId } }, { _id: ObjectID(_id) }, "Mongo", (err) => {
                    if (err) {
                        console.log("Error : ", err);
                        return reply({ message: "DB Error" }).code(500);
                    } else {
                        return reply({ message: "Data Successfully Update" }).code(200);
                    }
                });
            } else {
                middleWare.UpdateWithPush("customer", { "$pull": { "inActiveUsersNotification": targetUserId } }, { _id: ObjectID(_id) }, "Mongo", (err) => {
                    if (err) {
                        console.log("Error : ", err);
                        return reply({ message: "DB Error" }).code(500);
                    } else {
                        return reply({ message: "Data Successfully Update" }).code(200);
                    }
                });
            }
        },
        config: {

            description: 'Get user Details with  jsonwebtoken.',
            notes: 'This API is used to Get user Details with  jsonwebtoken.',
            tags: ['api'],
            auth: "user",
            validate: {
                payload: {
                    targetUserId: Joi.string().default("targetUserId"),
                    isMuteNotification: Joi.boolean().default(false)
                },
                headers: Joi.object({
                    'authorization': Joi.string()
                }).unknown()
            }
        }
    }];