var ObjectID = require('mongodb').ObjectID
var Joi = require('joi');
var middleWare = require("../../Controller/dbMiddleware.js");

module.exports = [
    {
        method: 'POST',
        path: '/User/Block',
        handler: function (req, res) {
            userBlockingHendler(req, res);
        },
        config: {
            tags: ['api', 'messages'],
            description: 'Block / Unblock the opponent user.',
            notes: 'Header input: KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj',
            auth: "user",
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string()
                }).unknown(),
                payload: {
                    opponentId: Joi.string().description("Unique id for opponent"),
                    type: Joi.string().description("block / unblock")
                }
            },

        },

    }];

function userBlockingHendler(req, reply) {
    var updtQry;
    if (req.payload.type == 'block') {
        updtQry = { $push: { blocked: ObjectID(req.payload.opponentId) } }
    } else if (req.payload.type == 'unblock') {
        updtQry = { $pull: { blocked: ObjectID(req.payload.opponentId) } }
    } else {
        return reply({ code: 403, message: "Invalid 'type'" }).code(403);
    }

    middleWare.UpdateAdvanced("customer", updtQry, { _id: ObjectID(req.auth.credentials._id) }, {}, "Mongo", function (err) {
        if (err) return reply({ code: 500, message: err.message }).code(500)

        return reply({ code: 200, message: "User " + req.payload.type + "ed successfully." }).code(200);
    });
}