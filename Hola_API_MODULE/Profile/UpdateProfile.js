const Joi = require('joi');
const logger = require('winston');
const request = require('request');
var ObjectID = require("mongodb").ObjectID;

const postES = require('../../models/postES');
const mqtt = require('../../library/mqtt');
const userListES = require('../../models/userListES');
const followCollection = require('../../models/follow');
const postsCollection = require('../../models/posts');
const userListCollection = require('../../models/userList');

module.exports = [
    {
        method: 'PUT',
        path: '/profile',
        config: {
            handler: function (req, reply) {
                updateProfileHandler(req, reply)
            },
            auth: "user",
            validate: {
                payload: Joi.object({
                    deviceId: Joi.string().description("deviceId is required"),
                    deviceType: Joi.number().description("deviceType is required"),
                    deviceModel: Joi.string().description("deviceModel is required"),
                    deviceMake: Joi.string().description("deviceMake is required"),
                    deviceOs: Joi.string().description("deviceOs is required"),
                    pushToken: Joi.string().description("pushToken is required"),
                    appVersion: Joi.string().description("appVersion is required"),
                    // profilePic: Joi.string().description("profilePic is required"),
                    userName: Joi.string().description("userName is required"),
                    versionCode: Joi.number().description("versionCode is required"),
                    private: Joi.number().description("private is required"),
                    // profileCoverImage: Joi.string().allow("").description("profileCoverImage  is required"),
                    profileVideo: Joi.string().allow("").description("profileVideo  is required"),
                    profileVideoThumbnail: Joi.string().allow("").description("profileVideo  is required"),
                    starUserKnownBy: Joi.string().allow("").description("starUserKnownBy  is required"),

                    BusinessName: Joi.string().allow("").description("BusinessName  is required"),
                    BusinessAddress: Joi.string().allow("").description("BusinessAddress  is required"),
                    BusinessWebsite: Joi.string().allow("").description("BusinessWebsite  is required"),
                    bussinessEmail: Joi.string().allow("").description("bussinessEmail  is required"),
                    bussinessPhone: Joi.string().allow("").description("bussinessPhone  is required"),
                    bussinessCountryCode: Joi.string().allow("").description("bussinessCountryCode  is required"),
                    bussinessCategoryId: Joi.string().allow("").description("bussinessCategoryId  is required"),
                    bussinessBio: Joi.string().allow("").description("bussinessCategoryId  is required"),

                    businessStreet: Joi.string().default("").allow("").description("businessStreet"),
                    businessCity: Joi.string().default("").allow("").description("businessCity"),
                    businessZipCode: Joi.string().default("").allow("").description("businessZipCode"),
                    businessLat: Joi.string().default("").allow("").description("businessLat"),
                    businessLng: Joi.string().default("").allow("").description("businessLng")
                }).unknown(),
                headers: Joi.object({
                    'authorization': Joi.string().required().description("authorization is required"),
                    'authToken': Joi.string().description("authToken is required"),
                    // 'token': Joi.string().description("token is required"),
                }).unknown()
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success',
                        },
                        '400': {
                            'description': 'Bad requset'
                        },
                        '404': {
                            'description': 'Not found'
                        },
                        '422': {
                            'description': 'Email already exists'
                        }
                    }
                }

            },
            description: 'Update user profile details.',
            notes: 'Checks the email and password and if correct logs in. Header input: Basic aG91c2U6ZVNTNzlCakVncVlYbmlXNHJTYzI3UDdMOUcyN2p3WFFZMlA0dGZCZW5tekcwb21iRnVKZVNYQ2pYQ3JaNVAwOGIxODBRc3RROTE2ZWtqS3FpZDBMTWRHMmRLTmk1cDRBS1FiSUUxUFZFcnF1RTJuYUx6U0JxR1dYT0hFd0pmR2hKb2daNzdTT0lJYXdEQVpIM1JhUDlablZwQ0NxRVZJRHZRY1ZYaFFRS0tIcTRQaktsRmlubHVkbzBoOXQ3RE9UUklyMndqNmZXV1VHWmZyUTc4UVFURFBTbm1vTFB0U0FhbWxWbEZHZW5waGZYR1dTNks2QTJkQW1nSjlwcTZmbg==',
            tags: ['api']

        }
    }
];

function updateProfileHandler(req, res) {

    const userId = ObjectID(req.auth.credentials._id);

    const checkUserName = () => {
        return new Promise((resolve, reject) => {
            let condition = { userName: req.payload.userName.toLowerCase().trim(), userStatus: 1, _id: { "$ne": userId } };
            userListCollection.SelectOne(condition, (e, d) => {
                if (e) {
                    let responseObj = { code: 500, message: 'internal server error while verifying username', error: e };
                    reject(responseObj);
                } else if (d) {
                    if (d && d.parentId) {
                        resolve(true);
                    } else {
                        let usernameUnique = userId.toString() === d._id.toString() ? true : false;
                        if (!usernameUnique) reject({ code: 409, message: 'This username is already taken , please try using another one.' });
                        else resolve(true);
                    }
                } else {
                    resolve(true);
                }
            });
        });
    };
    const updateOnPostCollection = () => {
        return new Promise((resolve, reject) => {

            let dataToUpdate = {};
            let scriptES = "";
            // req.payload.email = req.payload.email.toLowerCase();

            if (req.payload.firstName) {
                dataToUpdate["firstName"] = req.payload.firstName;
                scriptES += "ctx._source.firstName= '" + req.payload.firstName + "';"
            }
            if (req.payload.lastName) {
                dataToUpdate["lastName"] = req.payload.lastName;
                scriptES += "ctx._source.lastName= '" + req.payload.lastName + "';"
            }
            if (req.payload.userName) {
                dataToUpdate["userName"] = req.payload.userName;
                scriptES += "ctx._source.userName= '" + req.payload.userName + "';"
            }
            if (req.payload.private == 0) {
                dataToUpdate["isPrivate"] = req.payload.private;
                scriptES += "ctx._source.isPrivate=0;"
            }
            if (req.payload.private == 1) {
                dataToUpdate["isPrivate"] = req.payload.private;
                scriptES += "ctx._source.isPrivate=1;"
            }

            if (scriptES != "") {
                postES.updateByQueryMultiple({ userId: userId.toString() }, scriptES, (e) => {
                    if (e) logger.error(e)
                });
            }

            userListES.Update(userId.toString(), dataToUpdate, (e) => { if (e) logger.error(e) });
            postsCollection.Update({ userId: userId }, dataToUpdate, (e) => {
                if (e) {
                    let responseObj = { code: 500, message: 'internal server error while verifying username', error: e };
                    reject(responseObj);
                } else {
                    let responseObj = { code: 200, message: 'success' };
                    resolve(responseObj);
                }
            });

        });
    };
    const update = () => {
        return new Promise((resolve, reject) => {
            let condition = { _id: userId };

            let dataToUpdate = {
                userName: req.payload.userName.trim().toLowerCase(),
                starChatId: req.payload.userName.trim().toLowerCase(),
                // profilePic: `${process.env.AWS_S3_PROFILEPIC_BASE_URL}${userId.toString()}.jpg`, //payload.profilePic.replace(/upload.*\//gi, "upload/profile/"); ,//|| "https://res.cloudinary.com/dafszph29/image/upload/c_thumb,w_200,g_face/v1551361911/default/profile_pic.png",
                status: req.payload.status || "Hey! I am using " + process.env.APP_NAME + " 😊",
                private: (req.payload.private == 1) ? 1 : 0,
                profileVideo: req.payload.profileVideo || "",
                profileVideoThumbnail: req.payload.profileVideoThumbnail || "",
            };
            if (req.payload.firstName) dataToUpdate["firstName"] = req.payload.firstName;
            if (req.payload.lastName) dataToUpdate["lastName"] = req.payload.lastName;
            if (req.payload.email) dataToUpdate["email"] = { id: req.payload.email.toLowerCase() || "", verified: 0 };
            // if (req.payload.profileCoverImage) dataToUpdate["profileCoverImage"] = req.payload.profileCoverImage;
            if (req.payload.starUserKnownBy) dataToUpdate["starRequest.starUserKnownBy"] = req.payload.starUserKnownBy;

            if (req.payload.firstName) {
                //group call api
                request.post({
                    headers: { 'content-type': 'application/json', 'lan': 'en' },
                    url: `${process.env.GROUP_CALL_API}/user`,
                    body: {
                        id: userId.toString(),
                        userType: 1,
                        firstName: req.payload.firstName,
                        lastName: req.payload.lastName || req.payload.firstName,
                    },
                    json: true
                }, function (error) {
                    if (error) {
                        logger.error('KLAPWDD : ', error);
                    }
                });
            }

            if (req.payload.BusinessName) {
                let bus = 'businessProfile.' + 0;
                dataToUpdate[bus + '.businessName'] = req.payload.BusinessName || "";
                dataToUpdate[bus + '.address'] = req.payload.BusinessAddress || "";
                dataToUpdate[bus + '.email.id'] = req.payload.bussinessEmail || "";
                dataToUpdate[bus + '.phone.mobile'] = req.payload.bussinessPhone || "";
                dataToUpdate[bus + '.phone.countryCode'] = req.payload.bussinessCountryCode || "";
                dataToUpdate[bus + '.websiteURL'] = req.payload.BusinessWebsite || "";
                dataToUpdate[bus + '.businessBio'] = req.payload.bussinessBio || "";

                // if (req.payload.businessProfilePic) dataToUpdate[bus + '.businessProfilePic'] = req.payload.businessProfilePic;
                // if (req.payload.businessProfileCoverImage) dataToUpdate[bus + '.businessProfileCoverImage'] = req.payload.businessProfileCoverImage;

                dataToUpdate[bus + '.businessStreet'] = req.payload.businessStreet || '';
                dataToUpdate[bus + '.businessCity'] = req.payload.businessCity || '';
                dataToUpdate[bus + '.businessZipCode'] = req.payload.businessZipCode || '';
                dataToUpdate[bus + '.businessLat'] = req.payload.businessLat || '';
                dataToUpdate[bus + '.businessLng'] = req.payload.businessLng || '';

                if (req.payload.bussinessCategoryId) dataToUpdate[bus + ".businessCategoryId"] = ObjectID(req.payload.bussinessCategoryId);
            }

            userListCollection.Update(condition, dataToUpdate, (e, d) => {
                if (e) {
                    let responseObj = { code: 500, message: 'internal server error while verifying username', error: e };
                    reject(responseObj);
                } else {
                    let responseObj = { code: 200, message: 'success', data: d };
                    resolve(responseObj);
                }
            });

            userListCollection.SelectOne({ _id: userId }, (err, result) => {
                if (err) {
                    console.log(err);
                } else if (result && result._id) {


                    let condition_getData = [
                        {
                            "$match": {
                                "$or": [
                                    { "followee": userId, "type.status": 1 },
                                    { "follower": userId, "type.status": 1 }
                                ]
                            }
                        },
                        {
                            "$project": {
                                "type": 1,
                                "targetId": {
                                    "$cond": {
                                        "if": { "$eq": ["$followee", userId] },
                                        "then": "$follower", "else": "$followee"
                                    }
                                }
                            }
                        },
                        {
                            "$group": {
                                "_id": "$targetId"
                            }
                        }
                    ];

                    followCollection.Aggregate(condition_getData, (err, result_follow) => {
                        if (err) {
                            console.log("err ", err)
                            return reject({ code: 500, message: local['genericErrMsg']['500'] });
                        } else if (result_follow && result_follow.length) {
                            result_follow.forEach(element => {
                                mqtt.publish("UserUpdate/" + element._id, JSON.stringify({
                                    profilePic: result.profilePic, type: 2,
                                    number: result.mobile, userId: userId.toString()
                                }), { qos: 1 }, (e, r) => {

                                });
                            });
                        }
                    });


                    let streamData = {
                        userName: result.userName,
                        id: ObjectID(userId.toString()),
                        userType: 1,
                        email: req.payload.email,
                        firstName: result.firstName || result.userName,
                        lastName: result.lastName || "",
                        countryCode: result.countryCode,
                        mobile: result.mobile,
                        deviceType: 'Android',
                        profilePic: result.profilePic,
                        mqttTopic: userId.toString(),
                        fcmTopic: userId.toString()
                    }
                    //stream
                    request.post({
                        headers: { 'content-type': 'application/json', 'lan': 'en' },
                        url: `${process.env.LIVE_STREAM_API}/user`,
                        body: streamData,
                        json: true
                    }, function (error, response, body) {
                        if (error)
                            logger.error('0000', error);
                    });


                    //https://api.dubly.xyz/v1/ecomm/customerSyncDatum/editProfile -get it from Karan , Rinkesh for ecom
                    request.post({
                        headers: {
                            'content-type': 'application/json', 'lan': 'en',
                            "currencycode":"USD","currencysymbol":"$","platform":"web","language":"en",
                            "authorization":req.headers.authorization
                        },
                        url: process.env.ECOM_UPDATE_PROFILE,
                        body: {
                            "userId": userId.toString(),
                            "firstName": result.firstName,
                            "lastName": result.lastName || "",
                            "email": (result.email && result.email.id) ? result.email.id : ""
                        },
                        json: true
                    }, function (error, response, body) {
                        if (error)
                            logger.error('0000', error);
                    });

                }
            });

        });
    };

    checkUserName()
        .then(() => { return updateOnPostCollection(); })
        .then(() => { return update(); })
        .then((data) => { return res(data).code(200); })
        .catch((error) => { return res(error).code(error.code); });

}