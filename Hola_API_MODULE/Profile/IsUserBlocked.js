var ObjectID = require('mongodb').ObjectID
var async = require("async");
var Joi = require('joi');
var middleWare = require("../../Controller/dbMiddleware.js");


module.exports = [
    {
        method: 'GET',
        path: '/User/Block',
        handler: function (req, res) {
            userBlockingHendler(req, res);
        },
        config: {
            tags: ['api', 'messages'],
            description: 'check if the opponent user is blocked or not(and vice versa).',
            notes: 'Header input: KMajNKHPqGt6kXwUbFN3dU46PjThSNTtrEnPZUefdasdfghsaderf1234567890ghfghsdfghjfghjkswdefrtgyhdfghj',
            auth: "user",
            validate: {
                headers: Joi.object({
                    'authorization': Joi.string(),
                }).unknown(),
                query: {
                    opponentId: Joi.string().description("Unique id for opponent")
                }
            },

        },

    }];

function userBlockingHendler(req, reply) {

    async.waterfall([
     
        function () {

            /**  check if the opponent is in blocked list */
            const validatQry = {
                $or: [
                    { "_id": ObjectID(req.auth.credentials._id), "blocked": ObjectID(req.query.opponentId) },
                    { "_id": ObjectID(req.query.opponentId), "blocked": ObjectID(req.auth.credentials._id) }
                ]
            }

            middleWare.Select("customer", validatQry, "Mongo", {}, function (err, doc) {
                if (err)
                    return reply({ code: 500, message: err.message }).code(500)

                if (doc.length > 0) {
                    /** found - blocked */
                    var blk_self = false
                    async.eachSeries(doc, function (userObj, userCB) {
                        if (userObj._id.toString() == req.auth.credentials._id) {
                            /** add a flag notifying this user added the opponent to blocked list */
                            blk_self = true
                        }
                        userCB(null)
                    }, function () {
                        return reply({ code: 200, message: "Success", data: { blocked: true, self: blk_self } }).code(200);
                    })

                } else {
                    /** not focund - not blocked */
                    return reply({ code: 200, message: "Success", data: { blocked: false } }).code(200);
                }
            })

        }
    ])
}