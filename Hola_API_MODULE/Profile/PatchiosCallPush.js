var ObjectID = require('mongodb').ObjectID
var Joi = require('joi');
var middleWare = require("../../Controller/dbMiddleware.js");


module.exports = [
    {
        method: 'PATCH',
        path: '/iosCallPush',
        config: {
            handler: function (req) {

                var userId = req.auth.credentials._id;
                middleWare.Update("customer", { iosCallPush: req.payload.callPush }, { _id: ObjectID(userId) }, "Mongo", function (err) {
                    if (err) {
                        return res({ message: "unkown error occered " }).code(500);
                    } else {
                        return res({ message: "data update successfully." }).code(200);
                    }
                });
            },
            auth: "user",
            validate: {
                payload: {
                    callPush: Joi.string().description("callPush is required"),
                },
                headers: Joi.object({
                    'authorization': Joi.string().description("authorization is required")
                }).unknown()
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success',
                        },
                        '400': {
                            'description': 'Bad requset'
                        },
                        '404': {
                            'description': 'Not found'
                        },
                        '422': {
                            'description': 'Email already exists'
                        },
                        '500': {
                            'description': 'unkown error occered.'
                        },
                    }
                }

            },
            description: 'Update callPush token for IOS.',
            tags: ['api']
        }
    }];