
var ObjectID = require('mongodb').ObjectID
var Joi = require('joi');
var middleWare = require("../../Controller/dbMiddleware.js");

module.exports = [
    {
        method: 'GET',
        path: '/Participant/Profile',
        handler: function (req, res) {
            /** Call the handler function */
            var getUser;
            try {
                getUser = { _id: ObjectID(req.query.participantId) }
            } catch (exec) {
                return res({ code: 503, message: "Invalid participantId" }).code(503);
            }
        
            middleWare.Select("customer", getUser, "Mongo", {}, function (err, result) {
                if (err) {
                    return res({ code: 503, message: "Unknown error occurred" }).code(503);
                }
                else if (result.length > 0) {
        
                    var dataToSend = {
                        userId: result[0]._id.toString(),
                        userIdentifier: result[0].mobile,
                        userName: result[0].userName,
                        firstName: result[0].firstName,
                        lastName: result[0].lastName,
                        profilePic: result[0].profilePic,
                        socialStatus: result[0].socialStatus
                    };
        
                    return res({ code: 200, message: "Success", response: dataToSend }).code(200);
                }
                else {
                    return res({ code: 204, message: "User doesn't exist", response: {} }).code(200);
                }
            });
        },
        config: {
            description: 'Get participant details.',
            notes: 'This API is used to Get participant details.',
            tags: ['api'],
            auth: "user",
            validate: {
                query: {
                    participantId: Joi.string().description("participantId"),
                },
                headers: Joi.object({
                    'authorization': Joi.string()
                }).unknown(),
            }
        }
    }];