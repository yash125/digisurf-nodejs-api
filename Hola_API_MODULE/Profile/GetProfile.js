var ObjectID = require('mongodb').ObjectID
var Joi = require('joi');
var middleWare = require("../../Controller/dbMiddleware.js");

module.exports = [
    {
        method: 'GET',
        path: '/User/Profile',
        handler: function (req, res) {

            getProfileHendler(req, res);
        },
        config: {

            description: 'Get user Details with  jsonwebtoken.',
            notes: 'This API is used to Get user Details with  jsonwebtoken.',
            tags: ['api'],
            auth: "user",
            validate: {
                query: {
                    userId: Joi.string().description(" UserId"),
                },
                headers: Joi.object({
                    'authorization': Joi.string(),
                }).unknown(),
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '400': {
                            'description': 'Bad requset'
                        },
                        '401': {
                            'description': 'header is invalid'
                        },
                        '404': {
                            'description': 'Not found'
                        },
                        '403': {
                            'description': 'Cannot leave  group.'
                        },
                        '409': {
                            'description': 'User already exists use other email id to signUp'
                        }
                    }
                }

            }
        }
    }];


function getProfileHendler(req, res) {


    var getUser = { _id: ObjectID(req.query.userId) }

    middleWare.Select("customer", getUser, "Mongo", {}, function (err, result) {
        if (err) {
            return res({ code: 503, message: "Unknown error occurred" }).code(503);
        } else if (result.length > 0) {
            var dataToSend = {
                userName: result[0].userName,
                profilePic: result[0].profilePic,
                socialStatus: result[0].socialStatus,
                profileVideo: result[0].profileVideo || "",
                profileVideoThumbnail: result[0].profileVideo || ""
            };
            return res({ code: 200, message: "Success", response: dataToSend }).code(200);
        } else {
            return res({ code: 204, message: "User doesn't exist", response: {} }).code(200);
        }
    });
}