var ObjectID = require('mongodb').ObjectID
var Joi = require('joi');
var middleWare = require("../../Controller/dbMiddleware.js");
const request = require('request');


module.exports = [
    {
        method: 'POST',
        path: '/iosPush',
        config: {
            handler: function (req, reply) {
                var userId = req.auth.credentials._id;
                var iosAudioCallPush = req.payload.iosAudioCallPush;
                var iosVideoCallPush = req.payload.iosVideoCallPush;
                var iosMassageCallPush = req.payload.iosMassageCallPush;
                var isdevelopment = req.payload.isdevelopment;

                middleWare.Update("customer", {
                    iosAudioCallPush: iosAudioCallPush,
                    iosVideoCallPush: iosVideoCallPush,
                    iosMassageCallPush: iosMassageCallPush,
                    isdevelopment: isdevelopment,
                    appName: req.payload.appName
                }, { _id: ObjectID(userId) }, "Mongo", function (err) {
                    if (err) {
                        return reply({ message: "unkown error occered " }).code(500);
                    } else {
                        return reply({ message: "data update successfully." }).code(200);
                    }
                });
                //group call api
                request.patch({
                    headers: { 'content-type': 'application/json', 'lan': 'en' },
                    url: `${process.env.GROUP_CALL_API}/iospush`,
                    body: {
                        id: userId,
                        iosAudioCallPush: iosAudioCallPush,
                        iosVideoCallPush: iosVideoCallPush,
                        iosMassageCallPush: iosMassageCallPush
                    },
                    json: true
                }, function (error) {
                    if (error) {
                        logger.error('KLAPWDDASDE : ', error);
                    }
                });
            },
            auth: "user",
            validate: {
                payload: {
                    iosAudioCallPush: Joi.string().allow([null, ""]).description("iosAudioCallPush is required"),
                    iosVideoCallPush: Joi.string().allow([null, ""]).description("iosVideoCallPush is required"),
                    iosMassageCallPush: Joi.string().allow([null, ""]).description("iosMassageCallPush is required"),
                    isdevelopment: Joi.boolean().default(false).description("isdevelopment is required"),
                    appName: Joi.string().default("doChat").allow([null, ""]).description("appName is required")
                },
                headers: Joi.object({
                    'authorization': Joi.string().description("authorization is required")
                }).unknown()
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success',
                        },
                        '400': {
                            'description': 'Bad requset'
                        },
                        '404': {
                            'description': 'Not found'
                        },
                        '422': {
                            'description': 'Email already exists'
                        },
                        '500': {
                            'description': 'unkown error occered.'
                        },
                    }
                }

            },
            description: 'Update callPush token for IOS.',
            tags: ['api']
        }
    }];