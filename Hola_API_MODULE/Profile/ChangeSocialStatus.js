const Joi = require('joi');
var ComnFun = require('../../ComnFun.js');
var middleWare = require("../../Controller/dbMiddleware.js");
var async = require("async");
var objectId = require("mongodb").ObjectID;


module.exports = [
    {
        method: 'PUT',
        path: '/User/SocialStatus',
        config: {
            handler: function (req, reply) {

                var _id = req.auth.credentials._id;

                async.parallel([
                    function (callback) {
                        if (req.payload.socialStatus == "" || req.payload.socialStatus == null || req.payload.socialStatus == undefined) {
                            middleWare.UpdateWithPush("customer", { $unset: { socialStatus: "" } }, { _id: objectId(_id) }, "Mongo", function (err, result) {
                                callback(err, result);
                            });
                        } else {
                            middleWare.Update("customer", { socialStatus: req.payload.socialStatus }, { _id: objectId(_id) }, "Mongo", function (err, result) {
                                callback(err, result);
                            });
                        }
                    },
                    function (callback) {
                        middleWare.Select("customer", { _id: objectId(_id) }, "Mongo", {}, function (err, result) {
                            if (result[0].existInUsers) {
                                for (index = 0; index < result[0].existInUsers.length; index++) {
                                    ComnFun.publishMqtt("UserUpdate/" + result[0].existInUsers[index], JSON.stringify({
                                        socialStatus: req.payload.socialStatus, type: 1,
                                        number: result[0].mobile, userId: _id
                                    }));
                                }
                                callback(err, result);
                            } else {
                                callback(err, result);
                            }

                        });
                    }
                ], function (err) {
                    switch (err) {
                        case undefined || null: {
                            return reply({ code: 200, message: "Data Successfully Update" }).code(200);
                        }
                        default: {
                            return reply({ code: 503, message: "DB Error" }).code(503);
                        }
                    }

                });
            },
            auth: "user",
            validate: {
                payload: {
                    socialStatus: Joi.string()
                },
                headers: Joi.object({
                    'authorization': Joi.string(),
                    'token': Joi.string(),
                }).unknown()
            },
            plugins: {
                'hapi-swagger': {
                    responses: {
                        '200': {
                            'description': 'Success',
                        },
                        '400': {
                            'description': 'Bad requset'
                        },
                        '404': {
                            'description': 'Not found'
                        },
                        '422': {
                            'description': 'Email already exists'
                        }
                    }
                }

            },
            description: 'Update user Social Status.',
            notes: 'Checks the email and password and if correct logs in. Header input: Basic aG91c2U6ZVNTNzlCakVncVlYbmlXNHJTYzI3UDdMOUcyN2p3WFFZMlA0dGZCZW5tekcwb21iRnVKZVNYQ2pYQ3JaNVAwOGIxODBRc3RROTE2ZWtqS3FpZDBMTWRHMmRLTmk1cDRBS1FiSUUxUFZFcnF1RTJuYUx6U0JxR1dYT0hFd0pmR2hKb2daNzdTT0lJYXdEQVpIM1JhUDlablZwQ0NxRVZJRHZRY1ZYaFFRS0tIcTRQaktsRmlubHVkbzBoOXQ3RE9UUklyMndqNmZXV1VHWmZyUTc4UVFURFBTbm1vTFB0U0FhbWxWbEZHZW5waGZYR1dTNks2QTJkQW1nSjlwcTZmbg==',
            tags: ['api']
        }

    }
];

