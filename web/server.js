require('dotenv').config();
const Hapi = require('hapi');
var heapdump = require('heapdump');
// const memwatch = require('memwatch-next');


const Server = new Hapi.Server();
Server.realm.modifiers.route.prefix = '/v1'
const logger = require('winston');
const config = require('../config')
const cluster = require('cluster');

var trace = require("../library/node-trace")
var express = require("express")
const db = require('../models/mongodb');
const rabbitMq = require('../library/rabbitMq');
const rabbitMqWorker = require('../library/rabbitMq/worker');
const middleware = require('./middleware');
const numCPUs = require('os').cpus().length;
// const redisExpiredEvent = require('./../library/redis/redis.expired-events');
// const checkReconnection = require('./../library/redis/redis.reconnecting');
var elasticSearchDB = require('../models/elasticSearch');

if (cluster.isMaster) {
    // Fork workers.
    for (let i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    // Listen for dying workers
    cluster.on('exit', function (worker) {
        // Replace the dead worker,
        // we're not sentimental
        logger.error(`worker ${worker.process.pid} died`);
        cluster.fork();

    });

    db.connect(() => {
        // redisExpiredEvent();
        // checkReconnection();
    });

    elasticSearchDB.connect(() => { });
    var metricsServer = express();
    metricsServer.listen(4003, () => {
        // logger.info("express server is start on 4003")
    });
    metricsServer.use('', trace.MetricsCluster(metricsServer, express));
    metricsServer.use('', trace.SnapshotExpress(metricsServer, express));


} else {
    Server.connection({

        port: config.server.port,
        routes: {
            cors: true
        }
    });
    Server.register(
        [
            middleware.good,
            middleware.swagger.inert,
            middleware.swagger.vision,
            middleware.swagger.swagger,
            // middleware.auth.authJWT,
            middleware.auth,
            middleware.localization.i18n
        ], function (err) {
            if (err) Server.log(['error'], 'hapi-swagger load error: ' + err)

            // else Server.log(['start'], 'hapi-swagger interface loaded')
        });


    // Server.auth.strategy('accessToken', 'jwt', middleware.auth.accessToken);
    // Server.auth.strategy('userJWT', 'jwt', middleware.auth.accessToken);
    Server.auth.strategy('accessToken', '3Embed');
    Server.auth.strategy('userJWT', '3Embed');
    Server.auth.strategy('user', '3Embed');
    Server.auth.strategy('basic', '3Embed');
    Server.auth.strategy('Admin', '3Embed');
    Server.auth.strategy('guest', '3Embed');

    // Server.auth.strategy('myJWT','jwt',middleware.myAuth.strategyObj);
    Server.route(require('./routes/index'));

    // if (process.env.IS_CLIENT_JS_REQUIRED && cluster.isMaster) {
    //     Server.route(require('./routes/index'), require("./routes/webhook/mqtt"));
    // } else {
    //     Server.route(require('./routes/index'));
    // }

    const trace_endpoint = require('../library/node-trace').Endpoint;
    Server.on('response', (req) => {
        trace_endpoint.onComplete(req.info.received, req.route.method.toUpperCase(), req.route.path, req.response.statusCode)
    });
}
const initialize = () => {
    Server.start(() => {
        logger.info(`Server is listening on port `, config.server.port)
        db.connect(() => {
            // redisExpiredEvent();
            // checkReconnection();
        });//create a connection to mongodb
        elasticSearchDB.connect(() => { });
        rabbitMq.connect(() => {
            rabbitMqWorker.startWorker()
        });
    });// Add the route
}
//do something when app is closing
process.on('exit', function (code, signal) {
    heapdump.writeSnapshot(function (err, filename) {
        if (err) console.error(err);
        else console.error('Wrote snapshot: ' + filename);
    })
    logger.error(`exit main process exited with code $ and signal ${signal}`);
    rabbitMqWorker.exitHandler()
});

//catches ctrl+c event
process.on('SIGINT', function (code, signal) {
    heapdump.writeSnapshot(function (err, filename) {
        if (err) console.error(err);
        else console.error('Wrote snapshot: ' + filename);
    })
    logger.error(`SIGINT main process exited with code $ and signal ${signal}`);
    rabbitMqWorker.exitHandler();
});

//catches uncaught exceptions
process.on('uncaughtException', function (err, code, signal) {
    heapdump.writeSnapshot(function (err, filename) {
        if (err) console.error(err);
        else console.error('Wrote snapshot: ' + filename);
    })
    logger.error(`uncaughtException ${err} main process exited with code ${code} and signal ${signal}`);
    rabbitMqWorker.exitHandler();
});
// memwatch.on('leak', (info) => {
    // console.error('Memory leak detected:\n', info);
    // var filename = "./logs/" + new Date().getTime();


    // heapdump.writeSnapshot(function (err, filename) {
    //     if (err) console.error(err);
    //     else console.error('Wrote snapshot: ' + filename);
    // })
// })

process.setMaxListeners(0);

module.exports = { initialize };
