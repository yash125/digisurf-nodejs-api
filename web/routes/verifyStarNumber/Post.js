'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userCollection = require("../../../models/userList");
const otpsCollection = require("../../../models/otps");

/**
 * @description API to verify if the phone number has already been registerd with another user
 * @property {string} authorization - authorization
 * @property {string} lang - language

 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @author DipenAhir
 * @since 23-May-2019
 */

const payloadValidator = Joi.object({
    numberWithOutCountryCode: Joi.string().required().description("numberWithOutCountryCode").error(new Error("numberWithOutCountryCode field is missing")),
    countryCode: Joi.string().required().description("countryCode").error(new Error("countryCode field is missing")),
    otp: Joi.number().required().description("otp").error(new Error("otp field is missing")),
    isVisible: Joi.bool().default(false).description("isVisible").error(new Error("isVisible field is missing")),
}).unknown();

const handler = (req, res) => {

    const userId = ObjectID(req.auth.credentials._id);
    var numberWithOutCountryCode = req.payload.numberWithOutCountryCode;
    var countryCode = req.payload.countryCode;
    var number = countryCode + numberWithOutCountryCode;
    var otp = req.payload.otp;
    var isVisible = req.payload.isVisible


    const VerifyNumber = () => {
        return new Promise((resolve, reject) => {

            let condition = { phone: number, otp: otp, type: 2 };

            otpsCollection.SelectWithSort(condition, { timestamp: -1 }, {}, 0, 1, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result && result.length) {
                    userCollection.Update({ _id: userId }, {
                        "verified.phoneNumber": numberWithOutCountryCode,
                        "verified.countryCode": countryCode,
                        "verified.number": number,
                        // "starRequest.isNumberVisible": (isVisible) ? 1 : 0
                        "starRequest.isNumberVisible": isVisible
                    }, (e) => { if (e) logger.error("OKOKASDAS : ", e) })
                    return resolve({ code: 200, message: req.i18n.__('PostVerifyStarNumber')['200'] });
                } else {
                    return reject({ code: 412, message: req.i18n.__('PostVerifyStarNumber')['412'] });
                }
            })
        });
    };

    VerifyNumber()
        .then((data) => { return res({ message: data.message }).code(data.code); })
        .catch((data) => { return res({ message: data.message }).code(data.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['PostVerifyStarNumber']['200']) },
        412: { message: Joi.any().default(local['PostVerifyStarNumber']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { payloadValidator, handler, response };