const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const followCollection = require("../../../models/follow")
const userListCollection = require("../../../models/userList")


/**
 * @description API to subscribe to a channel
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token
 * @property {mongodb object} channelId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const validator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit")
}).required();

const handler = (req, res) => {
    const userId = ObjectID(req.auth.credentials._id);

    const getFollowing = () => {
        return new Promise((resolve, reject) => {
            let condition = { follower: userId };
            followCollection.Select(condition, (err, data) => {
                if (err) {
                    let responseObj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseObj);
                } else if (data.length === 0) {
                    return resolve(false);
                } else {
                    return resolve(data);
                }
            });
        });
    };

    const explorePosts = (followeeData) => {
        return new Promise((resolve, reject) => {
            let arr = [];
            followeeData.forEach((element) => {
                arr.push(element.followee)
            });
            let condition = [
                { "$match": { "private": 0, "_id": { "$nin": arr } } },
                { "$lookup": { "from": "posts", "localField": "_id", "foreignField": "userId", "as": "postData" } },
                { "$unwind": '$postData' },
                {
                    "$project": {
                        "_id": 0, 'userId': '$_id', "private": 1, "userName": 1, "postId": '$postData._id',
                        "imageUrl1": 1, "thumbnailUrl1": 1, "mediaType1": 1, "postedByUserId": '$postData.userId',
                        "postCreatedOn": '$postData.createdOn', "postTimeStamp": '$postData.timeStamp', "hashTags": '$postData.hashTags',
                        "postLocation": '$postData.location',"postPlace": '$postData.place',"postCountryName": '$postData.countrySname',
                        "postCity": '$postData.city', "title": '$postData.title', "categoryId": '$postData.categoryId',
                        "channelId": '$postData.channelId',

                        "comments": { "$ifNull": ['$postData.comments', []] },
                        "totalComments": { "$size": { '$ifNull': ['$postData.comments', []] } },
                        "likesCount": { "$ifNull": ['$postData.likesCount', 0] }
                    }
                }
            ];

            userListCollection.Aggregate(condition, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {
                    return resolve({ code: 200, message: req.i18n.__('GetSearchExplore')['200'], data: d });
                }
            });
        });
    };

    getFollowing()
        .then((followeeData) => { return explorePosts(followeeData); })
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((data) => { return res({ message: data.message }).code(data.code); })
};


const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetSearchExplore']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
};//swagger response code

module.exports = {
    validator,
    response,
    handler
};