'use strict'
var Joi = require("joi");
var QRCode = require('qrcode')
var logger = require('winston');
var Promise = require('promise');
var bcrypt = require("bcryptjs")

var ObjectID = require('mongodb').ObjectID;
var randomstring = require("randomstring");

var rabbitMq = require('../../../library/rabbitMq');


var local = require('../../../locales');
var userListES = require('../../../models/userListES');
var userList = require('../../../models/userList');
const deviceInfo = require("../../../models/deviceInfo");
var uploadFile = require("../../../library/uploadImage").upload;
var gRPC_Email_service = require("../../../library/gRPC/Email");


const request = require('request');

//models
var signUpModel = require('./../../../models/signUp');
// var authentication = require('./../../middleware/authentication');
var authentication = require('./../../middleware/auth');

/**
 * @method signUp 
 * @property {string} userName unique user name
 * @property {string} firstName first name of the user
 * @property {email} email email of the user
 * @property {string} lastName lastName of the user 
 * @property {boolean} uploadProfilePic profile pic uploaded or not
 * @property {string} userReferralCode Referal code
 * @property {string} password password of the user
 * @property {number} number phone number 
 * @property {string} countryCode country code of the user 
 * @property {boolean} isPrivate is the account private or not
 * @returns 201 : signedUp sucessfully
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 * @author Karthik
 * @since 19-June-2019
 */

/**
 * payload Validator of the api
 */
const payloadValidator = Joi.object({
    userName: Joi.string().min(3).required().description("please enter a unique username").error(new Error('username must be atleast 3 charector and is required')),
    firstName: Joi.string().required().description("please enter a  firstName").error(new Error('firstName  is required')),
    email: Joi.string().required().description("please enter your email").error(new Error('enter a valid email')),
    lastName: Joi.string().allow("").default("").description("please enter a lastName").error(new Error('lastname  is required')),
    uploadProfilePic: Joi.boolean().default(true).description("please enter uploadProfilePic").error(new Error('uploadProfilePic  is required')),
    // profilePic: Joi.string().required().description("please enter profilePic").error(new Error('lastName is required')),
    // userReferralCode: Joi.string().description("please enter a userReferralCode").allow("").default(""),
    password: Joi.string().min(6).required().description("please choose a alphanumeric password").error(new Error('password must be minimum 6 charector and must contain an alphanumeric variable')),
    number: Joi.string().required().description("PhoneNumber without countrycode").error(new Error('number must be of 10 digits')),
    countryCode: Joi.string().required().description("country code").error(new Error('countryCode must be valid')),
    isPrivate: Joi.boolean().default(false).required().description("isPrivate").error(new Error('isPrivate field is required')),

    deviceId: Joi.string().default("1").description("deviceId is required").error(new Error("deviceId is missing")),
    deviceName: Joi.string().default("1").description("deviceName is required").error(new Error("deviceName is missing")),
    deviceOs: Joi.string().default("1").description("deviceOs is required").error(new Error("deviceOs is missing")),
    modelNumber: Joi.string().default("1").description("modelNumber is required").error(new Error("modelNumber is missing")),
    deviceType: Joi.string().default("1").description("deviceType is required").error(new Error("deviceType is missing")),
    appVersion: Joi.string().default("1").description("appVersion is required").error(new Error("appVersion is missing")),
    countryName: Joi.string().description("countryName is required").error(new Error("countryName is missing")),

    googleId: Joi.string().default("").allow([null, ""]).description("googleId is required"),
    facebookId: Joi.string().default("").allow([null, ""]).description("facebookId is required"),
    appleId: Joi.string().default("").allow([null, ""]).description("appleId  is required"),
    loginType: Joi.string().default("").allow([null, ""]).description("loginType   is required"),

    status: Joi.string().default("Hey I am using " + process.env.APP_NAME + "!").allow([null, ""]).description("status   is required")

}).unknown();

/**
 * Handler of API
 */
const handler = (req, res) => {

    var dataToSend = { "code": 201, "message": "success", "response": {} };
    try {
        req.payload.userReferralCode = (req.payload.userReferralCode) ? req.payload.userReferralCode.toUpperCase() : "";
    } catch (error) {
        logger.silly("error :  ", error)
    }

    var seqId = 1;
    logger.silly("payload :  ", req.payload)

    let checkUserNameAndPhoneNumber = () => {
        return new Promise((resolve, reject) => {

            signUpModel.verifyUniqueness(req.payload, (err, result) => {
                if (err) return reject();

                if (result && result.length) {
                    if (result[0].userName == req.payload.userName) {
                        return reject({ code: 422, message: req.i18n.__('signUp')['422'] });
                    } else if (result[0].number == req.payload.countryCode + req.payload.number) {
                        return reject({ code: 409, message: req.i18n.__('signUp')['409'] });
                    } else {
                        return reject({ code: 412, message: req.i18n.__('signUp')['412'] });
                    }
                } else {
                    return resolve(true)
                }
            })
        });
    }

    let checkReferralCode = () => {
        return new Promise((resolve, reject) => {

            return resolve(true)

            if (process.env.IS_REFERRAL_CODE_REQUIRED == "true" || process.env.IS_REFERRAL_CODE_REQUIRED == true) {
                logger.silly("in if IS_REFERRAL_CODE_REQUIRED ", process.env.IS_REFERRAL_CODE_REQUIRED)
                userList.SelectOne({ "userStatus": 1, referralCode: req.payload.userReferralCode.toUpperCase() }, (err, result) => {
                    if (err) return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 })

                    if (result && result._id) {
                        return resolve(true)
                    } else {
                        return reject({ code: 406, message: req.i18n.__('signUp')['406'] });
                    }
                });
            } else {
                logger.silly("in else IS_REFERRAL_CODE_REQUIRED ", process.env.IS_REFERRAL_CODE_REQUIRED)
                return resolve(true)
            }

        });
    }

    let hashPassword = () => {
        return new Promise((resolve, reject) => {

            let payload = req.payload
            try {
                payload["number"] = payload.countryCode + payload.number
                payload["password_original"] = payload.password
                payload.password = bcrypt.hashSync(payload.password);

                return resolve(payload)
            } catch (err) {
                return reject(err)
            }
        })

    }

    let getSeqId = () => {
        return new Promise((resolve, reject) => {

            try {
                userList.SelectWithSort({ "seqId": { "$ne": ["", null] } }, { "seqId": -1 }, {}, 0, 1, (err, result) => {
                    if (err) {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] })
                    } else if (result && result[0] && result[0].seqId) {
                        seqId += parseInt(result[0] && result[0].seqId)
                    }
                    return resolve()
                });
            } catch (err) {
                return reject(err)
            }
        })

    }

    let signUp = (payload) => {
        return new Promise((resolve, reject) => {
            let referralCode = "REF" + payload["number"][1].toUpperCase();
            const userId = ObjectID()
            payload["_id"] = userId;

            if (req.payload.uploadProfilePic) {
                /** if req.payload.uploadProfilePic == true means upload image */
                let image = "data:image/png;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxEHEBISEBASFhIVEBAQEBMPEhAQEBAQFhIWFxUVFhoYHiggGBolGxMVITEhJikrLi4uFyAzODMtNygtLisBCgoKDQ0OFRAPFisdFR0tLS4tLS0tNy0rKystLS0rLSstLSsrKy0tLSstLS0rLS0tKy0rLS0tLSstLSstLS4tK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABQYBAwQCB//EADcQAQACAAMFBQUIAQUBAAAAAAABAgMEEQUhMUFREjJhcZETIoGhwRRCUnKSsdHhYhUzQ4KiBv/EABUBAQEAAAAAAAAAAAAAAAAAAAAB/8QAFhEBAQEAAAAAAAAAAAAAAAAAABEB/9oADAMBAAIRAxEAPwD7iAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMTaK8Z9Wq2ZpXjevrANw01zWHbhevrDbW0W4TE+QMgAAAAAAAAAAAAAAAAAAAAADEzohNobUnE1rhzpXnbnPl0gEhm9o0y27XW3Sv16IrH2riYvCezH+PH1cIDN7zfjMz5zMsAAzW004TMeUzDADtwNqYmFxntR/lx9UrlNp0zG6fdt0tz8pV0BbxA5Dac4Pu331686/zCdpaLxrE6xPCYBkAAAAAAAAAAAAAAAAEftjNewp2Y71tY8YjnIOLa2e9tM0rPuxO+fxT/CNAAAAAAAAAB3bLz32aezbuTP6Z6+ThAW+N4jNi5r2texM768PGv9JMAAAAAAAAAAAAAABWdo4/2jEtPKJ7MeULBm8T2WHa3Ss+vJVgAAAAAAAAAAAAbspjfZ71t0nf5TxWlUFm2die1wqT4aT8NwOkAAAAAAAAAAAAAHDtmdMG3jNY/wDUK8sG2/8AZn81f3V8AAAAAAAAAAAABPbCnXCnwvMftP1QKc2DGmHaet5/aASYAAAAAAAAAAAAAOXadPaYV48NfSdforS3Wr2omJ5xoqeLhzhWms8pmAeQAAAAAAAAAAAFi2PTsYUeMzb5q9Ws3mIjjM6QteFh+yrFY5REA9gAAAAAAAAAAAAAIXbmW7MxeOE7refKU08Y2FGNWazwmNAVMbc1gTlrTWfhPWOrUAAAAAAAAAD3gYU49orXjPyjqDu2Ll/aX7c8K8PzJ5qy2BGXrFY5fOectoAAAAAAAAAAAAAAAAObO5SM3XSd0x3Z6f0ruPgWy89m0aT8p8YWtqzGXrmY0tGvTrHkCqjvzey74O+vvV8O9HwcAAAAAAO3KbMvj7592vWeM+UA5cLCnGmIrGsrDkMlGUjrae9P0jwbMrla5WNKx5zPGW8AAAAAAAAAAAAAAAHm9opGszERHGZ3QD01Y2YpgRra0R58UVndrzbdh7o/FPH4Qi7Wm86zMzPWZ1lBbKXjEiJidYnhMPSr5TOXys+7O7nWeE/wnMptGmZ3a9m3S306g7GnHytMfvVifHhPq3CiLxNi0nu2tHnpMNM7En8cekpoBC/6Jb8cekt2HsWsd69p8tISgDRgZPDwO7WNes759W8AGLWisazw56ubN5+mW4zrb8Mcfj0Qeczt83x3V5Vjh8eqCx4WNXGjWtomPCdXtUsO84c61mYnrG5LZLa+u7E/VHD4wCXGInXgyoAAAAAAAAAxaezGs8OYPOLiRgxNrTpEcVdz+etm56V5R9Z8Wdo5z7Xbd3Y7sdfFyIACgADqy+0MTA4W1jpbfH8u/C21We/SY8a6TCGEgslNpYV/vxHnrDbGaw7cL1/VCrCi0zmaR9+v6oar7RwqffifLWf2VsBNYu2qx3azPnpEODMbRxMf72kdK7vnxcgkABQAB27P2hOVnSd9OnOvjH8LBS8YkRMTrE74mFSd+y899mns27kz+mevkgsACgAAAAAAh9t5v/jr53+kJLNY8Zek2nlw8Z5Qq97TeZmeMzrPmgwAoAAAAAAAAAAAAAAAAAAm9jZv2kdi0747uvOvT4JRU8HEnBtFo4xOq04GLGNWLRwmIlB7AUAAAYtbsxMzwiNZBC7dx+1aKRy96fOeHy/dFveNie2ta085mf4eAAAAAAAAAAAAAAAAAAAAAExsLH71J/NX6/RDtuUxvYXrbpMa+XMFqCN4AAA4tr4ns8K3j7vrx+WrtRH/ANBfuV/Nb00j6ghwAAAAAAAAAAAAAAAAAAAAAAAWXZuL7bCrPPTSfON30dSL2DfWto6W19Y/pKGAAAhNu9+v5Z/cARoAAAAAAAAAAAAAAAAAAAAAAAJXYPG//X6pgAAAf//Z"
                uploadFile(image, "profile_image/" + payload["_id"]);
            }
            let image = "data:image/png;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxEHEBISEBASFhIVEBAQEBMPEhAQEBAQFhIWFxUVFhoYHiggGBolGxMVITEhJikrLi4uFyAzODMtNygtLisBCgoKDQ0OFRAPFisdFR0tLS4tLS0tNy0rKystLS0rLSstLSsrKy0tLSstLS0rLS0tKy0rLS0tLSstLSstLS4tK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABQYBAwQCB//EADcQAQACAAMFBQUIAQUBAAAAAAABAgMEEQUhMUFREjJhcZETIoGhwRRCUnKSsdHhYhUzQ4KiBv/EABUBAQEAAAAAAAAAAAAAAAAAAAAB/8QAFhEBAQEAAAAAAAAAAAAAAAAAABEB/9oADAMBAAIRAxEAPwD7iAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMTaK8Z9Wq2ZpXjevrANw01zWHbhevrDbW0W4TE+QMgAAAAAAAAAAAAAAAAAAAAADEzohNobUnE1rhzpXnbnPl0gEhm9o0y27XW3Sv16IrH2riYvCezH+PH1cIDN7zfjMz5zMsAAzW004TMeUzDADtwNqYmFxntR/lx9UrlNp0zG6fdt0tz8pV0BbxA5Dac4Pu331686/zCdpaLxrE6xPCYBkAAAAAAAAAAAAAAAAEftjNewp2Y71tY8YjnIOLa2e9tM0rPuxO+fxT/CNAAAAAAAAAB3bLz32aezbuTP6Z6+ThAW+N4jNi5r2texM768PGv9JMAAAAAAAAAAAAAABWdo4/2jEtPKJ7MeULBm8T2WHa3Ss+vJVgAAAAAAAAAAAAbspjfZ71t0nf5TxWlUFm2die1wqT4aT8NwOkAAAAAAAAAAAAAHDtmdMG3jNY/wDUK8sG2/8AZn81f3V8AAAAAAAAAAAABPbCnXCnwvMftP1QKc2DGmHaet5/aASYAAAAAAAAAAAAAOXadPaYV48NfSdforS3Wr2omJ5xoqeLhzhWms8pmAeQAAAAAAAAAAAFi2PTsYUeMzb5q9Ws3mIjjM6QteFh+yrFY5REA9gAAAAAAAAAAAAAIXbmW7MxeOE7refKU08Y2FGNWazwmNAVMbc1gTlrTWfhPWOrUAAAAAAAAAD3gYU49orXjPyjqDu2Ll/aX7c8K8PzJ5qy2BGXrFY5fOectoAAAAAAAAAAAAAAAAObO5SM3XSd0x3Z6f0ruPgWy89m0aT8p8YWtqzGXrmY0tGvTrHkCqjvzey74O+vvV8O9HwcAAAAAAO3KbMvj7592vWeM+UA5cLCnGmIrGsrDkMlGUjrae9P0jwbMrla5WNKx5zPGW8AAAAAAAAAAAAAAAHm9opGszERHGZ3QD01Y2YpgRra0R58UVndrzbdh7o/FPH4Qi7Wm86zMzPWZ1lBbKXjEiJidYnhMPSr5TOXys+7O7nWeE/wnMptGmZ3a9m3S306g7GnHytMfvVifHhPq3CiLxNi0nu2tHnpMNM7En8cekpoBC/6Jb8cekt2HsWsd69p8tISgDRgZPDwO7WNes759W8AGLWisazw56ubN5+mW4zrb8Mcfj0Qeczt83x3V5Vjh8eqCx4WNXGjWtomPCdXtUsO84c61mYnrG5LZLa+u7E/VHD4wCXGInXgyoAAAAAAAAAxaezGs8OYPOLiRgxNrTpEcVdz+etm56V5R9Z8Wdo5z7Xbd3Y7sdfFyIACgADqy+0MTA4W1jpbfH8u/C21We/SY8a6TCGEgslNpYV/vxHnrDbGaw7cL1/VCrCi0zmaR9+v6oar7RwqffifLWf2VsBNYu2qx3azPnpEODMbRxMf72kdK7vnxcgkABQAB27P2hOVnSd9OnOvjH8LBS8YkRMTrE74mFSd+y899mns27kz+mevkgsACgAAAAAAh9t5v/jr53+kJLNY8Zek2nlw8Z5Qq97TeZmeMzrPmgwAoAAAAAAAAAAAAAAAAAAm9jZv2kdi0747uvOvT4JRU8HEnBtFo4xOq04GLGNWLRwmIlB7AUAAAYtbsxMzwiNZBC7dx+1aKRy96fOeHy/dFveNie2ta085mf4eAAAAAAAAAAAAAAAAAAAAAExsLH71J/NX6/RDtuUxvYXrbpMa+XMFqCN4AAA4tr4ns8K3j7vrx+WrtRH/ANBfuV/Nb00j6ghwAAAAAAAAAAAAAAAAAAAAAAAWXZuL7bCrPPTSfON30dSL2DfWto6W19Y/pKGAAAhNu9+v5Z/cARoAAAAAAAAAAAAAAAAAAAAAAAJXYPG//X6pgAAAf//Z"
            uploadFile(image, "background_image/" + payload["_id"]);


            if (payload["number"][2]) referralCode += payload["number"][2].toUpperCase()
            if (payload["number"][3]) referralCode += payload["number"][3].toUpperCase()
            if (payload["number"][4]) referralCode += payload["number"][4].toUpperCase()


            referralCode += "" + Math.floor(100000 + Math.random() * 900000)
            payload["referralCode"] = referralCode;
            payload["seqId"] = seqId;
            try {

                // if (process.env.IS_REFERRAL_CODE_REQUIRED == "true" || process.env.IS_REFERRAL_CODE_REQUIRED == true) {
                //     rabbitMq.sendToQueue(rabbitMq.referral_mqtt_queue, {
                //         "userId": payload["_id"].toString(), "userName": payload.userName, "userReferralCode": req.payload.userReferralCode
                //     }, () => { })
                // }

                var deviceInformation = {
                    "userId": ObjectID(payload._id.toString()),
                    "deviceName": req.payload.deviceName,
                    "deviceOs": req.payload.deviceOs,
                    "modelNumber": req.payload.modelNumber,
                    "deviceType": req.payload.deviceType,
                    "appVersion": req.payload.appVersion,
                    "deviceId": req.payload.deviceId,
                    "timestamp": new Date().getTime(),
                    "creationDate": new Date(),
                    "_id": new ObjectID()
                };

                deviceInfo.Insert(deviceInformation, (e) => {
                    if (e) {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] })
                    }
                });

                QRCode.toDataURL("" + payload["_id"], function (err, url) {
                    uploadFile(url, "qr_code/" + payload["_id"]);
                });

                payload["qrCode"] = { url: `${process.env.AWS_S3_PROFILEPIC_BASE_URL}qr_code/${payload["_id"].toString()}` };
                const accessKey = randomstring.generate({
                    length: 6,
                    charset: 'numeric'
                });

                payload["profilePic"] = `${process.env.AWS_S3_PROFILEPIC_BASE_URL}profile_image/${payload["_id"].toString()}`; //payload.profilePic.replace(/upload.*\//gi, "upload/profile/");
                payload["profileCoverImage"] = `${process.env.AWS_S3_PROFILEPIC_BASE_URL}background_image/${payload["_id"].toString()}`; //payload.profilePic.replace(/upload.*\//gi, "upload/profile/");

                payload["accessKey"] = String(accessKey);

                dataToSend["response"]["token"] = "";
                dataToSend["response"]["refreshToken"] = "";
                dataToSend["response"]["accessExpiry"] = "";

                payload.userName = payload.userName.trim().toLowerCase();
                dataToSend["response"]["userId"] = payload["_id"]
                payload.isPrivate = (payload.isPrivate) ? 1 : 0;
                dataToSend["response"]["private"] = (payload.isPrivate) ? 1 : 0;
                dataToSend["response"]["profilePic"] = payload.profilePic
                dataToSend["response"]["profileCoverImage"] = `${process.env.AWS_S3_PROFILEPIC_BASE_URL}background_image/${payload["_id"].toString()}`
                dataToSend["response"]["userName"] = payload.userName.trim().toLowerCase()
                dataToSend["response"]["firstName"] = payload.firstName
                dataToSend["response"]["lastName"] = payload.lastName
                dataToSend["response"]["profileVideo"] = payload.profileVideo
                dataToSend["response"]["profileVideoThumbnail"] = payload.profileVideoThumbnail
                dataToSend["response"]["authToken"] = dataToSend["response"]["token"]
                dataToSend["response"]["willTopic"] = dataToSend["response"]["token"]
                dataToSend["response"]["qrCode"] = payload.qrCode.url;
                dataToSend["response"]["email"] = payload.email.toLowerCase();
                dataToSend["response"]["currencySymbol"] = process.env.CURRENCY_SYMBOL || "";
                dataToSend["response"]["currency"] = process.env.CURRENCY_CODE || "";
                dataToSend["response"]["countryCode"] = payload.countryCode;
                dataToSend["response"]["number"] = payload.number;
                dataToSend["response"]["stream"] = {
                    "id": userId.toString(),
                    "userName": dataToSend["response"]["userName"],
                    "firstName": dataToSend["response"]["firstName"],
                    "lastName": dataToSend["response"]["lastName"],
                    "profilePic": dataToSend["response"]["profilePic"],
                    "fcmTopic": userId.toString(),
                    "mqttTopic": userId.toString(),
                    "accessToken": "",
                    "refreshToken": "",
                    "willTopic": "lastWill"
                }

                dataToSend["response"]["googleId"] = payload.googleId;
                dataToSend["response"]["facebookId"] = payload.facebookId;
                dataToSend["response"]["appleId"] = payload.appleId;
                dataToSend["response"]["loginType"] = payload.loginType;

                /**
                * insert user into database
                */

                payload["friendList"] = [];
                payload["follow"] = [];

                payload["statusBio"] = (req.payload && req.payload.status) ? req.payload.status : "Hey I am using " + process.env.APP_NAME + "!";
                // payload["status"] = 1;


                signUpModel.insertDoc(payload, (err) => {
                    if (err) {
                        logger.info('ASDOMWD :', err);
                        return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 })
                    } else {

                        payload["email"] = payload["email"];
                        payload["type"] = "user";
                        payload["userStatus"] = 1;
                        payload["statusBio"] = (req.payload && req.payload.status) ? req.payload.status : "Hey I am using " + process.env.APP_NAME + "!";
                        payload["status"] = 1;
                        payload["registeredOn"] = new Date().getTime();
                        payload["creationDate"] = new Date();



                        userListES.Insert(payload, (err) => {
                            if (err) logger.error("AOSKDOSKD : ", err)
                        });
                    }
                });

                rabbitMq.sendToQueue(rabbitMq.signup_mqtt_queue, {
                    "userId": payload["_id"].toString(),
                    deviceType: req.payload.deviceType,
                    uploadProfilePic: req.payload.uploadProfilePic,
                    liveStreamData: {
                        userName: req.payload.userName,
                        id: ObjectID(payload["_id"].toString()),
                        userType: 1,
                        email: req.payload.email,
                        firstName: req.payload.firstName || req.payload.userName,
                        lastName: req.payload.lastName || "",
                        countryCode: req.payload.countryCode,
                        mobile: req.payload.number,
                        deviceType: 'Android',
                        profilePic: payload["profilePic"],
                        mqttTopic: payload["_id"].toString(),
                        fcmTopic: payload["_id"].toString()
                    },
                    groupCall: {
                        id: ObjectID(payload["_id"].toString()),
                        userType: 1,
                        firstName: req.payload.firstName,
                        firstName: req.payload.firstName,
                        lastName: req.payload.lastName || req.payload.firstName,
                        profilePic: payload.profilePic,
                        deviceType: req.payload.deviceType,
                        pushKitToken: payload["_id"].toString(),
                        mqttTopic: payload["_id"].toString(),
                        fcmTopic: payload["_id"].toString()
                    }

                }, () => { })

                /**
                    *  call Ecome API
                    */
                // request.post({
                //     headers: {
                //         'content-type': 'application/json',
                //         'authorization': '{"userId":"' + payload["_id"].toString() + '","userType":"user","metaData":{}}',
                //         'platform': 1,
                //         'currencysymbol': '$',
                //         'currencycode': 'USD',
                //         'language': 'en'
                //     },
                //     url: `${process.env.ECOME_API}/v1/ecomm/customerSyncDatum`,
                //     body: {
                //         "userId": payload["_id"].toString(),
                //         "firstName": req.payload.firstName,
                //         "lastName": req.payload.lastName,
                //         "email": req.payload.email,
                //         "password": "123456",
                //         "countryCode": req.payload.countryCode,
                //         "mobile": req.payload.number,
                //         "dateOfBirth": "1997-06-08",
                //         "profilePicture": req.payload.profilePic,
                //         "latitude": "0",
                //         "longitude": "0",
                //         "ipAddress": "0.0.0.0",
                //         "city": "Bangalore",
                //         "country": "India"
                //     },
                //     json: true
                // }, function (error, response, body) {
                //     if (error) console.log("eror : KOIPLOIHDH ", error)
                // });


                request.post({
                    headers: {
                        'content-type': 'application/json', 'lan': 'en', 'licenseKey': 'lic-IMKy7l6gykqxtca+oMZq5dOHLJGnTinbdWW'
                    },
                    url: process.env.GroupCallStreaming_API,
                    body: {
                        "accountId": "5eb3db9ba9252000014f82ff",
                        "projectId": "59070129-4c67-4d70-8303-1434824898bb",
                        "keysetId": "732ed863-bc30-43be-970b-86f6d5a5933f",
                        "userIdentifier": dataToSend["response"]["number"],
                        "userName": dataToSend["response"]["firstName"] + " " + dataToSend["response"]["lastName"],
                        "userProfilePic": dataToSend["response"]["profilePic"]
                    },
                    json: true
                }, function (error, response, body) {
                    if (error)
                        logger.error('0000', error);
                    else if (body && body.userId) {
                        userList.Update({ _id: ObjectID(dataToSend["response"]["userId"]) }, { groupCallStreamId: body.userId }, (e, r) => {
                            if (e) {
                                console.log("PLOEPSOFP ", e)
                            }
                        })
                        dataToSend["response"]["groupCallStreamId"] = body.userId || "";

                    }
                    authentication.generateTokens(
                        {
                            userId: userId.toString(),
                            userType: "user",
                            multiLogin: 'true',
                            allowedMax: "1",
                            immediateRevoke: 'true',
                            metaData: {
                                "deviceId": req.payload.deviceId,
                                "sessionId": ""
                            },
                            accessTTL: process.env.AUTH_ACCESS_EXPIRY_TIME,
                            refreshTTL: process.env.AUTH_REFRESH_EXPIRY_TIME
                        }).then((data) => {
                            dataToSend["response"]["token"] = data.accessToken;
                            dataToSend["response"]["refreshToken"] = data.refreshToken;
                            dataToSend["response"]["accessExpiry"] = data.accessExpiry;
                            return resolve(dataToSend);
                        });
                });


                // for group streaming                      
                dataToSend["response"]["accountId"] = process.env.accountId
                dataToSend["response"]["keysetId"] = process.env.keysetId
                dataToSend["response"]["projectId"] = process.env.projectId
                dataToSend["response"]["licenseKey"] = process.env.licenseKey
                dataToSend["response"]["keysetName"] = process.env.keysetName
                dataToSend["response"]["rtcAppId"] = process.env.rtcAppId
                dataToSend["response"]["arFiltersAppId"] = process.env.arFiltersAppId

                /**
                 * using request package to send welcome email to the new user using send grid
                 */
                //code to send welcome email to the new user
                let data = {
                    userName: payload.userName,
                    trigger: "Email Verification",
                    to: payload.email,
                    subject: `Thank you for signing up on ` + process.env.APP_NAME,
                    body: '<p> Hello ' + payload.userName + ',' + '<br/><br/> Thank you for registering with ' + process.env.APP_NAME + '.' + '<br/>You can now create DUBBED posts on ' + process.env.APP_NAME + '.' + '<br/>We really hope you enjoy the time you spend on ' + process.env.APP_NAME + '<br/><br/><br/>Cheers, <br/> Team ' + process.env.APP_NAME + '.</p>'
                }
                gRPC_Email_service.sendEmail(data)

            } catch (error) {
                logger.error(error)
                return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 })
            }
        })
    }

    checkUserNameAndPhoneNumber()
        .then(() => checkReferralCode())
        .then(() => getSeqId())
        .then(() => hashPassword())
        .then((data) => signUp(data))
        .then((data) => {
            return res(dataToSend).code(data.code)
        })
        .catch((data) => { return res({ message: data.message }).code(data.code); })
}

const response = {
    status: {
        201: { code: Joi.any(), message: Joi.any().default(local['signUp']['201']), response: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        409: { message: Joi.any().default(local['signUp']['409']) },
        406: { message: Joi.any().default(local['signUp']['406']) },
        412: { message: Joi.any().default(local['signUp']['412']) },
        422: { message: Joi.any().default(local['signUp']['422']) },
    }
}//swagger response code

module.exports = { payloadValidator, handler, response };