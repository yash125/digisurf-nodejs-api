let PostAPI = require('./Post');
let headerValidator = require("../../middleware/validator")

module.exports = [
    {
        method: 'POST',
        path: '/signUp',
        handler: PostAPI.handler,
        config: {
            description: `This API used for signUp`,
            tags: ['api', 'signUp'],
            auth: {
                strategies: ['basic', 'guest']
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                // failAction: (req, reply, source, error) => {
                //     failAction: headerValidator.faildAction(req, reply, source, error)
                // }
            },
            response: PostAPI.response
        }
    }
];