'use strict'
const chai = require('chai');
const { expect } = require('chai');
const chaiHttp = require('chai-http');
const faker = require('faker');
const logger = require('winston');
const request = require("request");
chai.use(chaiHttp);
/*
 * Test the /signUp
 */
describe('/POST signUp', () => {
    it('it should successfully signUp the user with status code 201', (done) => {

        // let name = faker.name.findName()
        let name = faker.fake("{{name.firstName}}");
        let data = {
            userName: name,
            firstName: name,
            email: faker.internet.email(),
            lastName: faker.fake("{{name.lastName}}"),
            uploadProfilePic: false,
            userReferralCode: 'REF9197746736',
            // password: faker.internet.password(),
            password: '123456',
            number: JSON.stringify(Math.floor(100000000 + Math.random() * 9000000000)),
            countryCode: '+91',
            isPrivate: false,

        }

        logger.silly('dataaaaaaaaaaaaaaaaaa=>',data);
        request.post({
            // headers: { 'content-type': 'application/json', 'authorization': '123', 'lan': '1' },
            headers: { 'content-type': 'application/json', 'lan': '1' },
            url: 'http://localhost:5007/signUp',
            body: data,
            json: true
        }, function (error, response, body) {
            logger.silly("body------",body);
            logger.error("error------>", error);
            expect(response.statusCode).to.eql(201);
            done();
        });

    });
    it("should say user name already exists with status code 422", function (done) {
        const data = {
            userName: "stream1",
            firstName: "stream1",
            email: faker.internet.email(),
            lastName: faker.fake("{{name.lastName}}"),
            uploadProfilePic: false,
            userReferralCode: 'REF9197746736',
            password: '123456',
            number: JSON.stringify(Math.floor(100000000 + Math.random() * 9000000000)),
            countryCode: '+91',
            isPrivate: false,

        }

        request.post({
            headers: { 'content-type': 'application/json', 'lan': '1' },
            url: 'http://localhost:5007/signUp',
            body: data,
            json: true
        }, function (error, response, body) {
            logger.silly("body------",body);
            logger.error("error------>", error);
            expect(response.statusCode).to.eql(422);
            done();
        });
    });

    it("should say phone number already exists with status code 409", function (done) {

        let name = faker.fake("{{name.firstName}}");
        const data = {
            userName: name,
            firstName: name,
            email: faker.internet.email(),
            lastName: faker.fake("{{name.lastName}}"),
            uploadProfilePic: false,
            userReferralCode: 'REF9197746736',
            password: faker.internet.password(),
            number: '4444444444',
            countryCode: '+91',
            isPrivate: false,

        }

        request.post({
            headers: { 'content-type': 'application/json', 'lan': '1' },
            url: 'http://localhost:5007/signUp',
            body: data,
            json: true
        }, function (error, response, body) {
            logger.silly("body------",body);
            logger.error("error------>", error);
            expect(response.statusCode).to.eql(409);
            done();
        });
    });


});

