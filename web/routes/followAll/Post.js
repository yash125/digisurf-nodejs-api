const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const fcm = require("../../../library/fcm");
const followCollection = require("../../../models/follow");
const userCollection = require("../../../models/userList");


const validator = Joi.object({
    followeeId: Joi.array().required().description("followeeIds in array"),
}).required();

const handler = (req, res) => {
    let userId = ObjectID(req.auth.credentials._id);
    let followeeId = [], privateUsers = [], publicUsers = [];
    req.payload.followeeId.forEach((val) => {
        followeeId.push(ObjectID(val));
    });



    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }
    const getUsersToPrivateOrPublic = () => {
        return new Promise((resolve, reject) => {
            userCollection.Select({ _id: { "$in": followeeId } }, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else if (result) {
                    result.map(element => {
                        if (element.private) {
                            privateUsers.push(element._id);
                        } else {
                            publicUsers.push(element._id);
                        }
                    })
                }
                return resolve(true);
            });
        });
    }
    const followRequestToPrivateUsers = () => {
        return new Promise((resolve, reject) => {

            privateUsers.forEach(function (element) {
                let condition = { "follower": userId, "followee": ObjectID(element) };
                let dataToInsert = {
                    "follower": userId,
                    "followee": ObjectID(element),
                    "start": new Date().getTime(),
                    "end": false,
                    "type": {
                        "status": 2,
                        "message": "  requested to follow you"
                    }
                };
                followCollection.UpdateWithUpsert(condition, dataToInsert, (e) => {
                    if (e) {
                        reject(e);
                    }
                });
                sendNotification(element, " wants to follow you on Star.");
            }, this);

            return resolve("----");
        });
    }
    const followToPublicUsers = () => {
        return new Promise((resolve, reject) => {

            publicUsers.forEach(function (element) {
                let condition = { "follower": userId, "followee": ObjectID(element) };
                let dataToInsert = {
                    "follower": userId,
                    "followee": ObjectID(element),
                    "start": new Date().getTime(),
                    "end": false,
                    "type": {
                        "status": 1,
                        "message": " started following you"
                    }
                };
                followCollection.UpdateWithUpsert(condition, dataToInsert, (e) => { if (e) { reject(e) } });
                userCollection.UpdateByIdWithAddToSet({ _id: userId.toString() }, { "follow": ObjectID(element) }, (e) => { if (e) { reject(e) } });
                userCollection.UpdateByIdWithAddToSet({ _id: element }, { "follow": userId }, (e) => { if (e) { reject(e) } });
                sendNotification(element, " started following you");
            }, this);

            return resolve(true);
        });
    }
    function sendNotification(targetUser, message) {
        let request = {
            "fcmTopic": targetUser,
            "action": 1,
            "pushType": 2,
            "title": process.env.APP_NAME,
            "msg": userData.userName + message,
            "data": { "type": "following", "userId": userId },
            "deviceType": 1
        }
        fcm.notifyFcmTpic(request, () => { })

    }

    getUserData()
        .then(getUsersToPrivateOrPublic)
        .then(followRequestToPrivateUsers)
        .then(followToPublicUsers)
        .then(() => { return res({ message: local['PostFollowAll']['200'], code: 200, isAllFollow: true }).code(200); })
        .catch((error) => { return res(error).code(error.code); });


};

const response = {
    status: {
        200: { code: 200, message: Joi.any().default(local['PostFollowAll']['200']), isAllFollow: Joi.any() },
        400: { code: Joi.any(), message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { validator, handler, response };