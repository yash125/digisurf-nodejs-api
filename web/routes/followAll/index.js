let headerValidator = require("../../middleware/validator")
let PostAPI = require('./Post');

module.exports = [
    {
        method: 'Post',
        path: '/follow/all',
        handler: PostAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'follow'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: PostAPI.response
        }
    }
];