'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../locales');
const channelCollection = require("../../../models/channel");
const postsCollection = require("../../../models/posts");
const postES = require("../../../models/postES");

/**
 * @description API use to delete channel db
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} channelId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const validator = Joi.object({
    channelId: Joi.string().min(24).max(24).description("channelId").required().error(new Error("channelId is missing or invalid it must be 24 char")),
}).unknown();

const handler = (req, res) => {

    const checkChannel = () => {
        return new Promise((resolve, reject) => {
            let channelId = req.query.channelId;
            channelCollection.SelectOne({ _id: ObjectID(channelId) }, (err, data) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'], error: err });
                } else if (data && data._id) {
                    if (data.userId.toString() == req.auth.credentials._id) {
                        return resolve({ code: 200, message: req.i18n.__('genericErrMsg')['200'] });
                    } else {
                        return reject({ code: 405, message: req.i18n.__('genericErrMsg')['405'] });
                    }
                } else {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                }
            });
        });
    };

    const uppdateStatusInMongoDB = () => {
        return new Promise((resolve, reject) => {
            let channelId = req.query.channelId;
            channelCollection.Update({ _id: ObjectID(channelId) }, {
                "channelStatus": 3, "channelStatusText": "Deleted",
                deletedOnTs: new Date().getTime(),
                deletedOn: new Date()
            }, (err) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'], error: err });
                } else {

                    postES.updateByQuery({ channelId: channelId }, "postStatusText", "Deleted", (err) => {
                        if (err) {
                            console.log("err : ", err);
                        }
                    });

                    return resolve({ code: 200, message: req.i18n.__('genericErrMsg')['200'] });
                }
            });


            postsCollection.Update({ channelId: ObjectID(channelId) }, {
                "postStatus": 2, "postStatusText": "Deleted",
                "channelStatus": 3, "channelStatusText": "Deleted",
                deletedOnTs: new Date().getTime(),
                deletedOn: new Date()
            }, (err) => {
                if (err) {
                    console.log("err : ", err);
                }
                postES.updateByQuery({ channelId: channelId }, "channelStatusText", "Deleted", (err) => {
                    if (err) {
                        console.log("err : ", err);
                    }

                });
            });

            postES.updateByQuery({ channelId: channelId }, "postStatus", 2, (err) => {
                if (err) {
                    console.log("err : ", err);
                }

            });
            postsCollection.Update({ channelId: ObjectID(channelId) }, {
                "postStatus": 2, "postStatusText": "Deleted",
                "channelStatus": 3, "channelStatusText": "Deleted",
                deletedOnTs: new Date().getTime(),
                deletedOn: new Date()
            }, (err) => {
                if (err) {
                    console.log("err : ", err);
                }
                postES.updateByQuery({ channelId: channelId }, "channelStatus", 3, (err) => {
                    if (err) {
                        console.log("err : ", err);
                    }

                });
            });


        });
    };

    checkChannel()
        .then(uppdateStatusInMongoDB)
        .then((data) => { return res({ message: data.message }).code(data.code); })
        .catch((data) => { return res({ message: data.message }).code(data.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']) },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { validator, response, handler };