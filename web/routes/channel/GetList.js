'use strict'
const Joi = require("joi");
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const channelCollection = require("../../../models/channel");

/**
 * @description API to fetch hashtag suggestions
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const validator = Joi.object({
    searchText: Joi.string().allow("").description("hashtag to be searched"),
    skip: Joi.string().description("skip"),
    limit: Joi.string().description("limit"),
}).required();

const handler = (req, res) => {

    let searchText = (req.query.searchText) ? req.query.searchText.trim().toLowerCase() : "";
    let userId = ObjectID(req.auth.credentials._id);
    let skip = parseInt(req.query.skip) || 0;
    let limit = parseInt(req.query.limit) || 20;


    let condition = [
        { "$match": { "channelStatus": 1 } },
        {
            "$project": {
                "confirm": 1, "reject": 1, "request": 1, "channelImageUrl": 1, "channelName": 1, "categoryId": 1, "userId": 1,
                "private": 1, "subscriber": { "$size": "$confirm" }
            }
        },
        { "$sort": { "subscriber": -1, "_id": -1 } },
        { $skip: skip },
        { $limit: limit }
    ];

    if (searchText != "") {
        condition = [
            { "$match": { "channelName": { $regex: searchText, "$options": "i" }, "channelStatus": 1 } },
            {
                "$project": {
                    "confirm": 1, "reject": 1, "request": 1, "channelImageUrl": 1, "channelName": 1, "categoryId": 1,
                    "userId": 1, "private": 1, "subscriber": { "$size": "$confirm" }
                }
            },
            { "$sort": { "subscriber": -1, "_id": -1 } },
            { $skip: skip },
            { $limit: limit }
        ];
    }

    channelCollection.Aggregate(condition, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result.length === 0) {
            return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);
        } else {
            /* 
                subscribeStatus 
                0 : none,
                1 : confirm,
                2 : request
                3 : reject
            */
            result.forEach(function (element) {
                element["subscribeStatus"] = 0;
                if (element["reject"] && element["reject"].map(id => id.userId.toString()).includes(userId.toString())) element["subscribeStatus"] = 3;
                if (element["confirm"] && element["confirm"].map(id => id.userId.toString()).includes(userId.toString())) element["subscribeStatus"] = 1;
                if (element["request"] && element["request"].map(id => id.userId.toString()).includes(userId.toString())) element["subscribeStatus"] = 2;

                delete element["confirm"];
                delete element["request"];
                delete element["reject"];
            }, this);
        }
        return res({ message: req.i18n.__('GetChannelList')['200'], data: result }).code(200);
    });
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetChannelList']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { validator, response, handler };