let headerValidator = require("../../middleware/validator")
let PostAPI = require('./Post');
let GetAPI = require('./Get');
let GetListAPI = require('./GetList');
let PutAPI = require('./Put');
let Delete = require('./Delete');


module.exports = [

    {
        method: 'Delete',
        path: '/channel',
        handler: Delete.handler,
        config: {
            description: `This API use for change status active to delete in DB`,
            tags: ['api', 'channel'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: Delete.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: Delete.response
        }
    },
    {
        method: 'GET',
        path: '/channelList',
        handler: GetListAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'channel'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetListAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetListAPI.response
        }
    },
    {
        method: 'PUT',
        path: '/channel',
        handler: PutAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'channel'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.validator,
                // params: PutAPI.paramsValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response
        }
    },
    {
        method: 'POST',
        path: '/channel',
        handler: PostAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'channel'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
    {
        method: 'GET',
        path: '/channel',
        handler: GetAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'channel'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];