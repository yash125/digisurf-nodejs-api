'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../locales');
const channelCollection = require("../../../models/channel");

/**
 * @description API to fetch user's channel
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} offset
 * @property {string} limit
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const validator = Joi.object({
    offset: Joi.string().description("offset"),
    limit: Joi.string().description('limit')
}).required();

const handler = (req, res) => {

    const get = () => {
        return new Promise((resolve, reject) => {
            let offset = parseInt(req.query.offset) || 0;
            let limit = parseInt(req.query.limit) || 20;
            let userId = ObjectID(req.auth.credentials._id);

            let condition = [
                { "$match": { "userId": userId, "channelStatus": 1 } },
                { "$sort": { "createdOn": -1 } },
                { "$lookup": { "from": "category", "localField": "categoryId", "foreignField": "_id", "as": "categoryData" } },
                { "$unwind": { "path": "$categoryData", "preserveNullAndEmptyArrays": true } },
                {
                    "$project": {
                        "channelId": "$_id", "channelName": 1, "createdOn": 1, "private": 1, "categoryId": 1, "userId": 1, "confirm": 1,
                        "unfollow": 1, "channelImageUrl": 1, "categoryName": "$categoryData.categoryName",
                        "categoryIconUrl": "$categoryData.categoryActiveIconUrl",
                        "description": { "$ifNull": ["$description", ""] }
                    }
                },
                { "$skip": offset }, { "$limit": limit },
            ];

            channelCollection.Aggregate(condition, (err, data) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'], error: err });
                } else if (data.length === 0) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {
                    return resolve({ code: 200, message: req.i18n.__('GetChannel')['200'], data: data });
                }
            });
        });
    };

    get()
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((data) => { return res({ message: data.message }).code(data.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetChannel']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { validator, response, handler };