'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../locales');
const fcm = require("../../../library/fcm");
const channelCollection = require("../../../models/channel")
const userCollection = require("../../../models/userList")
const postCollection = require("../../../models/posts");
const postES = require("../../../models/postES");
// var sendPushUtility = require("../../../library/iosPush/sendPush.js");

/**
 * @description API to check create a update channel
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token
 * @property {mongodb object} categoryId
 * @property {string} channelImageUrl
 * @property {string} channelName
 * @property {string} description
 * @property {boolean} private
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const validator = Joi.object({
    channelName: Joi.string().description("channelName"),
    channelImageUrl: Joi.string().description('channelImageUrl'),
    description: Joi.string().description('description'),
    isPrivate: Joi.boolean().description('make channel public or private'),
    categoryId: Joi.string().description('category id'),
    channelId: Joi.string().min(24).max(24).required().description('channelId'),
}).unknown();

const paramsValidator = Joi.object({
    channelId: Joi.string().min(24).max(24).required().description('channelId'),
}).required();

const handler = (req, res) => {
    let userId = ObjectID(req.auth.credentials._id);
    let channelDetails = { "oldChannel": {}, "newChannel": {} };

    const checkUpdateAllowed = () => {
        return new Promise((resolve, reject) => {
            channelCollection.SelectOne({ _id: ObjectID(req.payload.channelId), userId: userId }, (err, result) => {
                if (err) {
                    let responseObj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseObj);
                } else if (result) {
                    channelDetails.oldChannel = result;
                    return resolve(result);
                } else {
                    let responseObj = { code: 405, message: req.i18n.__('genericErrMsg')['405'] };
                    return reject(responseObj);
                }
            });
        });
    };
    const updateChannel = (channelData) => {
        return new Promise((resolve, reject) => {
            let dataToUpdate = { lastModify: new Date().getTime() };
            if (req.payload.channelName) dataToUpdate["channelName"] = req.payload.channelName.trim().toLowerCase()
            if (req.payload.description) dataToUpdate["description"] = req.payload.description.trim()
            if (req.payload.isPrivate == true || req.payload.isPrivate == false) dataToUpdate["private"] = req.payload.isPrivate
            if (req.payload.categoryId) dataToUpdate["categoryId"] = ObjectID(req.payload.categoryId)
            if (req.payload.channelImageUrl) dataToUpdate["channelImageUrl"] = req.payload.channelImageUrl

            postCollection.Update({ categoryId: ObjectID(req.payload.categoryId) }, {
                channelImageUrl: req.payload.channelImageUrl,
                channelName: req.payload.channelName.trim().toLowerCase()
            }, () => { });




            if (req.payload.channelImageUrl) postES.updateByQuery({ channelId: req.payload.channelId }, "channelImageUrl", req.payload.channelImageUrl,
                (err, result) => { if (err) console.log(err) });

            setTimeout(() => {
                if (req.payload.channelName) postES.updateByQuery({ channelId: req.payload.channelId }, "channelName", req.payload.channelName.trim().toLowerCase(),
                    (err, result) => { if (err) console.log(err); });
            }, 1000)

            channelDetails.newChannel = dataToUpdate

            channelCollection.UpdateById(req.payload.channelId, dataToUpdate, (err) => {
                if (err) {
                    let responseObj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseObj);
                } else {
                    return resolve(channelData);
                }
            });

        });
    };
    function sendNotification(channelData) {
        return new Promise((resolve, reject) => {

            let messageToSend = channelDetails.oldChannel.channelName + " has some update.";
            if (channelDetails.newChannel.channelName) {
                messageToSend = channelDetails.oldChannel.channelName + " name has changed to " + channelDetails.newChannel.channelName;
            } else if (channelDetails.newChannel.private && !channelDetails.newChannel.private) {
                messageToSend = channelDetails.oldChannel.channelName + " has updated their privacy settings";
            } else if (channelDetails.newChannel.channelImageUrl) {
                messageToSend = channelDetails.oldChannel.channelName + "  has changed their profile picture";
            }

            userCollection.SelectOne({ _id: userId }, (err, result) => {

                channelData.confirm.forEach(element => {
                    let request = {
                        fcmTopic: element.userId,
                        // action: 1,
                        pushType: 2,
                        title: process.env.APP_NAME,
                        msg: messageToSend,
                        data: { "type": "postLiked", "postId": "1" },
                        deviceType: (result && result.deviceInfo && result.deviceInfo.deviceType) ? result.deviceInfo.deviceType : "2"
                    }
                    fcm.notifyFcmTpic(request, (e) => {
                        if (e) {
                            logger.error(e)
                            reject(e)
                        }
                    })
                    // let message = { alert: request.title, msg: request.msg };
                    // sendPushUtility.iosPushIfRequiredOnMessage({
                    //     targetId: request.fcmTopic, message: message, userId: userId.toString(),
                    //     "type": "postLiked", "postId": "1"
                    // });
                });
            })



            let responseObj = { code: 200, message: req.i18n.__('PutChannel')['200'] };
            return resolve(responseObj);

        });
    }

    checkUpdateAllowed()
        .then((data) => { return updateChannel(data) })
        .then((data) => { return sendNotification(data) })
        .then((data) => { return res({ message: data.message }).code(data.code); })
        .catch((data) => { return res({ message: data.message }).code(data.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['PutChannel']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { validator, response, handler, paramsValidator };