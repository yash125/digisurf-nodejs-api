'use strict'
const Joi = require("joi");
const moment = require('moment');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../locales');
const channelCollection = require("../../../models/channel");

/**
 * @description API to check create a new channel
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token
 * @property {mongodb object} categoryId
 * @property {string} channelImageUrl
 * @property {string} channelName
 * @property {string} description
 * @property {boolean} private
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const validator = Joi.object({
    channelName: Joi.string().required().description("channelName"),
    channelImageUrl: Joi.string().required().description('channelImageUrl'),
    private: Joi.boolean().description('make channel public or private'),
    categoryId: Joi.string().allow("").description('category id'),
    description: Joi.string().description('description'),
}).unknown();

const handler = (req, res) => {
    
    let userId = ObjectID(req.auth.credentials._id);

    const add = () => {
        return new Promise((resolve, reject) => {

            let channelName = req.payload.channelName.trim().toLowerCase();
            let isPrivate = req.payload.private || false;
            let categoryId = (req.payload.categoryId) ? ObjectID(req.payload.categoryId) : "";

            let dataToInsert = {
                channelName: channelName,
                createdOn: moment().valueOf(),
                private: isPrivate,
                categoryId: categoryId,
                userId: userId,
                "confirm": [],
                "unfollow": [],
                "channelStatus": 1,
                "channelStatusText": "active"
            };
            if (req.payload.channelImageUrl) dataToInsert.channelImageUrl = req.payload.channelImageUrl.trim();
            if (dataToInsert.private) dataToInsert["reject"] = [];
            if (dataToInsert.private) dataToInsert["request"] = [];
            channelCollection.Insert(dataToInsert, (err) => {
                if (err) {
                    let responseObj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseObj);
                } else {
                    let responseObj = { code: 200, message: req.i18n.__('PostChannel')['200'] };
                    return resolve(responseObj);
                }
            });
        });
    };

    add()
        .then((data) => { return res({ message: data.message }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};


const response = {
    status: {
        200: { message: Joi.any().default(local['PostChannel']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { validator, response, handler };