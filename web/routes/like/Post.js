const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const fcm = require("../../../library/fcm");

const postES = require("../../../models/postES");
// const rabbitMq = require('../../../library/rabbitMq');
const postsCollection = require("../../../models/posts");
const likesCollection = require("../../../models/likes");
const userCollection = require("../../../models/userList");
const notificationCollection = require("../../../models/notification");
// var sendPushUtility = require("../../../library/iosPush/sendPush.js");


/**
 * @description API to like a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} userId
 * @property {string} postId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const payloadValidator = Joi.object({
    userId: Joi.string().required().description("userId, mongo id of the user"),
    ip: Joi.string().allow("").description("ip"),
    city: Joi.string().allow("").description("city"),
    country: Joi.string().allow("").description("country"),
    postId: Joi.string().required().description("postId, mongo id of the post")
}).unknown();



const handler = (req, res) => {

    const userId = ObjectID(req.auth.credentials._id.toString());
    const postId = ObjectID(req.payload.postId);
    var memberId = ObjectID(req.payload.userId);
    var imageUrl = "";
    var username = "";
    var needToSendFCM = false;

    postsCollection.SelectOne({ _id: postId }, (err, result) => {
        if (result && result.userId) {
            
            memberId = ObjectID(result.userId.toString());
            imageUrl = result.imageUrl1
            username = result.userName
        } else {
            
            return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204)
        }

        checkIfAlreadyLiked()
            .then(likePost)
            .then(createNotification)
            .then(incrementLikeCountInPostCollection)
            .then(sendPushnotification)
            .then((data) => { return res(data).code(200); })
            .catch((error) => { console.log("error ", error); return res({ message: error.message }).code(error.code); })
    });

    const checkIfAlreadyLiked = () => {
        return new Promise((resolve, reject) => {
            likesCollection.SelectOne({ postId: postId, likedBy: userId }, (e, d) => {
                if (e) {
                    console.log("stap 1 ", e);
                    let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else if (d) {
                    postsCollection.UpdateByIdWithAddToSet({ _id: postId }, { "likes": userId }, (e) => {
                        if (e) {
                            let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
                            return reject(responseobj);
                        }
                    });
                    let responseobj = { message: req.i18n.__('genericErrMsg')['409'], code: 409 };
                    try {
                        postES.Bulk([
                            { "update": { "_id": postId.toString(), "_type": "postList", "_index": "posts", "retry_on_conflict": 3 } },
                            { "script": { "source": "ctx._source.likes.add(" + userId.toString() + ")", "lang": "painless" } }
                        ], (e) => {
                            if (e) { logger.error(e) }
                        });

                    } catch (error) {
                        logger.error("error : ", error)
                    }
                    return reject(responseobj);
                } else {
                    return resolve(true);
                }
            });
        });
    };
    const likePost = () => {
        return new Promise((resolve, reject) => {

            let dataToInsert = {
                postId: postId,
                postedBy: memberId,
                likedBy: userId,
                timeStamp: new Date().getTime(),
                ip: req.payload.ip,
                city: req.payload.city,
                country: req.payload.country
            };
            try {
                // postES.UpdateWithPush(postId.toString(), "likes", userId.toString(), () => { })
                // postES.UpdateWithIncrease(postId.toString(), "likesCount", 1, (e, r) => { if (e) { logger.error(e) } });
                postES.Bulk([
                    { "update": { "_id": postId.toString(), "_type": "postList", "_index": "posts", "retry_on_conflict": 3 } },
                    { "script": { "source": "ctx._source.likes.add('" + userId.toString() + "')", "lang": "painless" } },
                    { "update": { "_id": postId.toString(), "_type": "postList", "_index": "posts", "retry_on_conflict": 3 } },
                    { "script": { "source": "ctx._source.likesCount+=1", "lang": "painless" } },
                ], (e) => {
                    if (e) { logger.error(e) }
                });

            } catch (error) {
                logger.error("error : ", error)
            }

            likesCollection.Insert(dataToInsert, (e) => {
                if (e) {
                    console.log("stap 2 ", e);
                    let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    let responseobj = { message: req.i18n.__('PostLike')['200'] };
                    return resolve(responseobj);
                }
            });

        });
    };
    const createNotification = () => {
        return new Promise((resolve, reject) => {
            let condition = {
                from: userId,
                to: memberId,
                postId: postId,
                "type.status": 2,
                end: true
            };
            let dataToUpdate = {
                from: userId,
                to: memberId,
                postId: postId,
                fromCollection: 'customer',
                toCollection: 'posts',
                start: new Date().getTime(),
                end: false,
                type: {
                    status: 2,
                    message: 'liked'
                }
            };

            if (userId.toString() != memberId.toString()) {
                notificationCollection.UpdateWithUpsert(condition, dataToUpdate, (e, d) => {
                    if (e) {
                        console.log("stap ASOAKS ", e);
                        let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
                        return reject(responseobj);
                    } else {
                        d = JSON.parse(d);
                        
                        if (d.nModified == 0) {
                            needToSendFCM = true;
                            // rabbitMq.sendToQueue(rabbitMq.like_mqtt_queue, { "userId": userId.toString(), "userName": username, "userLang": req.headers.lang, "starUserId": memberId.toString() },
                            //     (err) => {
                            //         if (err) logger.error(err);
                            //     });
                        }
                        let responseobj = { message: req.i18n.__('PostLike')['200'] };
                        return resolve(responseobj);
                    }
                });
            } else {
                let responseobj = { message: req.i18n.__('PostLike')['200'] };
                return resolve(responseobj);
            }
        });
    };
    const incrementLikeCountInPostCollection = () => {
        return new Promise((resolve, reject) => {
            postsCollection.UpdateByIdWithAddToSet({ _id: postId }, { "likes": userId }, (e) => {
                if (e) {
                    let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                }
            });

            let updateData = { likesCount: 1 };
            postsCollection.UpdateWithIncrice({ _id: postId }, updateData, (e) => {
                if (e) {
                    let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    let responseobj = { message: req.i18n.__('PostLike')['200'] };
                    return resolve(responseobj);
                }
            });

        });
    };
    const sendPushnotification = () => {
        return new Promise((resolve, reject) => {

            if (needToSendFCM && memberId.toString() != userId.toString()) {

                userCollection.SelectOne({ _id: ObjectID(memberId.toString()) }, (err, result) => {

                    if (err) {
                        let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
                        return reject(responseobj);
                    }
                    var deviceType = (result && result.deviceInfo && result.deviceInfo.deviceType) ? result.deviceInfo.deviceType : "2";

                    userCollection.SelectOne({ _id: userId }, (err, result) => {

                        if (err) {
                            let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
                            return reject(responseobj);
                        }

                        if (result && result.userName) {
                            let request = {
                                fcmTopic: memberId.toString(),
                                // action: 1,
                                pushType: 2,
                                title: process.env.APP_NAME,
                                msg: result.userName + " liked your post on " + process.env.APP_NAME,
                                data: { "type": "postLiked", "postId": postId.toString(), "imageUrl": imageUrl },
                                deviceType: deviceType
                            }
                            fcm.notifyFcmTpic(request, () => { });

                            // let message = { alert: 'PostLiked', msg: request.msg };
                            // sendPushUtility.iosPushIfRequiredOnMessage({
                            //     targetId: request.fcmTopic, message: message, userId: userId.toString(),
                            //     "type": "postLiked", "postId": postId.toString(), "imageUrl": imageUrl
                            // });
                        }
                    });
                });


            }
            let responseobj = { message: req.i18n.__('PostLike')['200'] };
            return resolve(responseobj);
        });
    }
};

const response = {
    status: {
        // 200: {
        //     message: Joi.any().default(local['PostLike']['200']), data: Joi.any()
        // },
        // 204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        // 409: { message: Joi.any().default(local['genericErrMsg']['409']) },
        // 400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        // 500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}//swagger response code


module.exports = { payloadValidator, handler, response };