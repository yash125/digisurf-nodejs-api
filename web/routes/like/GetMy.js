'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const postsCollection = require("../../../models/posts");
const userCollection = require("../../../models/userList");
const followCollection = require("../../../models/follow");

const validator = Joi.object({
    "skip": Joi.string().description('skip'),
    "limit": Joi.string().description('limit'),
    "userId": Joi.string().description('skip')
}).unknown();

const handler = (req, res) => {
    const userId = ObjectID(req.query.userId);
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.skip) || 0;
    var followeeIds = [], followStatus = {};


    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(userId), "end": false, "type.status": 1, "followee": { "$nin": userData.blocked || [] } };
            followCollection.Select(condition, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        followeeIds.push(ObjectID(element.followee));
                        followStatus[element.followee] = element.type
                    }, this);
                    followeeIds.push(userId);
                    return resolve(true);
                }
            });
        });
    }
    function getPost() {
        return new Promise((resolve, reject) => {

            postsCollection.SelectWithProject({ "likes": userId, "postStatus": 1 }, offset, limit, { "timeStamp": -1 }, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    

                    result.forEach(function (element) {

                        element["profilePic"] = (element["profilepic"] && element["profilepic"].includes("http")) ? element["profilepic"] : "https://res.cloudinary.com/dafszph29/image/upload/c_thumb,w_200,g_face/v1551361911/default/profile_pic.png";
                        element["profileVideoThumbnail"] = (element["profileVideoThumbnail"] && element["profileVideoThumbnail"].includes("http")) ? element["profileVideoThumbnail"] : "";
                        element["profileVideo"] = (element["profileVideo"] && element["profileVideo"].includes("http")) ? element["profileVideo"] : "";
                        element["private"] = (element["private"]) ? 1 : 0;


                        element["distinctViews"] = (element["distinctViews"]) ? element["distinctViews"].length : 0;
                        element["totalComments"] = (element["totalComments"]) ? element["totalComments"] : 0;
                        element["profilePic"] = element["profilepic"];
                        element["comments"] = (element["comments"]) ? element["comments"].slice(element["comments"].length - 3).reverse() : [];
                        element["hashTags"] = [];
                        element["totalViews"] = [];
                        element["mentionedUsers"] = [];
                        element["firstName"] = (element["firstName"] != "null" && element["firstName"] != null && typeof element["firstName"] != 'undefined') ? element["firstName"] : "";
                        element["lastName"] = (element["lastName"] != "null" && element["lastName"] != null && typeof element["lastName"] != 'undefined') ? element["lastName"] : "";
                        element["postId"] = element._id
                        if (element.musicId == "") delete element.musicData
                        element["postId"] = element._id
                        
                        element["followStatus"] = (userData.follow && userData.follow.map(e => e.toString()).includes(element["userId"].toString())) ? 1 : 0;
                        element["liked"] = true;
                        element["orientation"] = element.orientation || 0;
                        delete element["likes"];
                    }, this);
                    let responseobj = { code: 200, message: req.i18n.__('GetHome')['200'], data: result };
                    return resolve(responseobj);
                }
            });
        });
    }

    getUserData()
        .then(() => { return getFolloweeIds(); })
        .then(() => { return getPost(); })
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); });
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetMyLikesPosts']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code

module.exports = { validator, handler, response };