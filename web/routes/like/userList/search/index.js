let headerValidator = require("../../../../middleware/validator")
let GetMyAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/likedUserList',
        handler: GetMyAPI.handler,
        config: {
            description: `This API use for search data in likers`,
            tags: ['api', 'like'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetMyAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: PostAPI.response
        }
    }
];