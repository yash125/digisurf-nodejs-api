'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../../locales');
const likesCollection = require("../../../../../models/likes");
const followCollection = require("../../../../../models/follow");

const validator = Joi.object({
    "skip": Joi.string().description('skip'),
    "limit": Joi.string().description('limit'),
    "searchText": Joi.string().description('Search text'),
    "postId": Joi.string().description('PostId'),
}).unknown();

const handler = (req, res) => {
    let _id = req.auth.credentials._id;

    let myFollowers = {};
    let searchText = req.query.searchText || "";

    let postId = ObjectID(req.query.postId);
    const skip = parseInt(req.query.skip) || 0;
    const limit = parseInt(req.query.limit) || 20;

    const getMyFollowers = () => {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(_id) };
            followCollection.Select(condition, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else {
                    for (let index = 0; index < result.length; index++) {
                        myFollowers[result[index]["followee"]] = result[index]["type"];
                    }
                    return resolve("--");
                }
            });
        });
    }

    const getLikedUserListFromPost = () => {
        return new Promise((resolve, reject) => {
            var condition = [
                { "$match": { "postId": postId } },
                { "$lookup": { "from": "customer", "localField": "likedBy", "foreignField": "_id", "as": "customer" } },
                { "$unwind": "$userList" },
                {
                    "$project": {
                        "userId": "$userList._id",
                        "private": "$userList.private", "profilePic": "$userList.profilePic",
                        "userName": "$userList.userName", "businessProfile": "$userList.businessProfile",
                        "firstName": "$userList.firstName", "lastName": "$userList.lastName",
                        "fullName": { "$concat": ["$userList.firstName", "$userList.lastName"] },
                        "fullWithSpaceName": { "$concat": ["$userList.firstName", " ", "$userList.lastName"] },
                        "timeStamp": 1
                    }
                }
            ];
            if (searchText && searchText.length) {
                condition.push({
                    "$match": {
                        "userName": { "$exists": true },
                        "$or": [
                            { "userName": { "$regex": searchText, "$options": "gi" } },
                            { "firstName": { "$regex": searchText, "$options": "gi" } },
                            { "lastName": { "$regex": searchText, "$options": "gi" } },
                            { "fullName": { "$regex": searchText, "$options": "gi" } },
                            { "fullWithSpaceName": { "$regex": searchText, "$options": "gi" } }
                        ]

                    }
                })
            }
            condition.push({ "$sort": { "timeStamp": -1 } },
                { "$skip": skip }, { "$limit": limit });

            likesCollection.Aggregate(condition, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {

                    d.forEach(function (element) {
                        element["type"] = (myFollowers[element["userId"]]) ? myFollowers[element["userId"]] : { "status": 0, "message": "unfollowed" };
                        element["end"] = (element["type"]["status"] == 1) ? false : true;
                        element["follower"] = _id;
                        element["followee"] = (element["userId"] != _id) ? element["userId"] : "";
                        element["firstName"] = (element["firstName"]) ? element["firstName"] : element["userName"];
                        element["lastName"] = (element["lastName"]) ? element["lastName"] : "";
                        element["profilePic"] = (element["profilePic"] && element["profilePic"].includes("http")) ? element["profilePic"] : "https://res.cloudinary.com/dafszph29/image/upload/c_thumb,w_200,g_face/v1551361911/default/profile_pic.png";
                        element["profileVideoThumbnail"] = (element["profileVideoThumbnail"] && element["profileVideoThumbnail"].includes("http")) ? element["profileVideoThumbnail"] : "";
                        element["profileVideo"] = (element["profileVideo"] && element["profileVideo"].includes("http")) ? element["profileVideo"] : "";
                        element["private"] = (element["private"]) ? 1 : 0;

                    }, this);

                    return resolve({ code: 200, message: req.i18n.__('GetFollowers')['200'], data: d });
                }
            });
        });
    };

    getMyFollowers()
        .then(() => { return getLikedUserListFromPost() })
        .then((data) => {
            return res({ message: data.message, data: data.data }).code(data.code);
        })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetMyLikesPosts']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code

module.exports = { validator, handler, response };