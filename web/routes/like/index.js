let headerValidator = require("../../middleware/validator")
let PostAPI = require('./Post');
let GetMyAPI = require('./GetMy');
let userlist = require("./userList")

module.exports = [
    {
        method: 'POST',
        path: '/like',
        handler: PostAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'like'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: PostAPI.response
        }
    },
    {
        method: 'GET',
        path: '/MyLikedPost',
        handler: GetMyAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'like'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: GetMyAPI.payloadValidator,
                query: GetMyAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: PostAPI.response
        }
    }
].concat(userlist);
