'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const channelCollection = require("../../../models/channel");
const userListCollection = require("../../../models/userList");


const handler = (req, res) => {

    let _id = req.auth.credentials._id
    let channelId = req.payload.channelId;

    function unSubscribe() {
        return new Promise((resolve, reject) => {

            channelCollection.UpdateByIdWithPull({ _id: ObjectID(channelId) },
                { "confirm": { "userId": ObjectID(_id) } }, (err) => { if (err){ logger.error(err); reject(err);} });

            channelCollection.UpdateByIdWithPull({ _id: ObjectID(channelId) },
                { "request": { "userId": ObjectID(_id) } }, (err) => { if (err){ logger.error(err); reject(err);} });

            channelCollection.UpdateByIdWithAddToSet({ _id: ObjectID(channelId) },
                { "unfollow": { "userId": ObjectID(_id), "timestamp": new Date().getTime() } },
                (err) => { if (err){ logger.error(err); reject(err);} });

            userListCollection.UpdateByIdWithPull({ _id: _id }, { "subscribeChannels": ObjectID(channelId) }, (err) => { if (err){ logger.error(err); reject(err);}  })
            return resolve(true)
        });
    }

    unSubscribe()
        .then(() => { return res({ message: req.i18n.__('PutUnSubscribeChannel')['200'] }).code(200); })
        .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204); })
};


const validator = Joi.object({
    channelId: Joi.string().description("channelId")
}).required();

const response = {
    status: {
        200: { message: Joi.any().default(local['PutUnSubscribeChannel']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { validator, handler, response };