const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const userListCollection = require("../../../models/userListES")
const userListCollectionMonogo = require("../../../models/userList")
const followCollection = require("../../../models/follow")


/**
 * @description API to subscribe to a channel
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token
 * @property {mongodb object} channelId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const validator = Joi.object({
})

const QueryValidator = Joi.object({
    searchText: Joi.string().allow("").default("").description("searchText to be searched"),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit")
})

const handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    let username = (req.query.searchText) ? req.query.searchText.trim().toLowerCase() : "";
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;
    var followeeIdObj = {};

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollectionMonogo.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }
    const getMyFollowee = (users) => {
        return new Promise((resolve, reject) => {

            followCollection.Select({ follower: userId, followee: { "$in": users.map(e => ObjectID(e._id)) } }, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result && result.length) {
                    for (let index = 0; index < result.length; index++) {
                        followeeIdObj[result[index].followee.toString()] = result[index]["type"];
                    }
                }
                let dataToSend = [];
                let data = {};
                users.forEach(element => {
                    data = {
                        _id: element._id,
                        firstName: element.firstName,
                        registeredOn: element.registeredOn,
                        private: (element.private) ? 1 : 0,
                        businessProfile: element.businessProfile,
                        userName: element.userName,
                        profilePic: element.profilePic,
                        knownBy: element.starRequest.starUserKnownBy,
                        lastName: element.lastName,
                        followStatus: (followeeIdObj[element._id]) ? followeeIdObj[element._id]["status"] : 0,
                        isStar: true,
                        friendStatusCode: (userData.friendList && userData.friendList.map(e => e.toString()).includes(element._id)) ? 2 : (userData.myFriendRequest && userData.myFriendRequest.map(e => e.toString()).includes(element._id)) ? 3 : 1
                    };
                    data["friendStatusText"] = (data["friendStatusCode"] == 2) ? "friends" : (data["friendStatusCode"] == 3) ? "userSentARequestToTargetUser" : "notAFirend";
                    data["followStatus"] = (data["followStatus"] == 3) ? 0 : data["followStatus"]
                    dataToSend.push(data);
                });
                return resolve({ code: 200, message: req.i18n.__('GetStarUser')['200'], data: dataToSend });
            })
        });
    };

    const getUserList = () => {
        return new Promise((resolve, reject) => {
            var condition = {
                "must": [
                    { "term": { "starRequest.starUserProfileStatusCode": 4 } },
                    { "term": { "userStatus": 1 } }
                ],
                "must_not": [
                    { "match": { "userName": " " } },
                    { "match": { "userName": "null" } },
                ]
            };

            if (username !== "") {
                condition = {
                    "must": [
                        { "term": { "starRequest.starUserProfileStatusCode": 4 } },
                        { "term": { "userStatus": 1 } },
                        { "match_phrase_prefix": { "userName": username } }
                    ],

                    "must_not": [
                        { "match": { "userName": " " } },
                        { "match": { "userName": "null" } },
                    ]
                };
            }

            userListCollection.getDetails(condition, offset, limit, userData.follow, req.auth.credentials._id, (e, d) => {
                if (e) {
                    logger.error("error ", e);
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {
                    return resolve(d);
                    // return resolve({ code: 200, message: req.i18n.__('GetStarUser')['200'], data: dataToSend });
                }
            });

        })
    };

    getUserData()
        .then(getUserList)
        .then(getMyFollowee)
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetStarUser']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { validator, QueryValidator, response, handler };