let headerValidator = require("../../middleware/validator")
let GetAPI = require('./GetFromES');
let GetTopAPI =  require('./GetTopFromES'); //require('./GetTop');
let whomIfollow = require('./whomIfollow');


module.exports = [
    // {
    //     method: 'Get',
    //     path: '/starUsers',
    //     handler: GetAPI.handler,
    //     config: {
    //         description: `This API use for check referralCode is exists in db or not`,
    //         tags: ['api', 'starUser'],
    //         auth: 'userJWT',
    //         validate: {
    //             headers: headerValidator.headerAuthValidator,
    //             query: GetAPI.QueryValidator,
    //             failAction: (req, reply, source, error) => {
    //                 failAction: headerValidator.faildAction(req, reply, source, error)
    //             }
    //         },
    //         response: GetAPI.response
    //     }
    // },
    {
        method: 'Get',
        path: '/starUsers',
        handler: GetAPI.handler,
        config: {
            description: `This API use for check referralCode is exists in db or not`,
            tags: ['api', 'starUser'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.QueryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
    // {
    //     method: 'Get',
    //     path: '/topStarUsers',
    //     handler: GetTopAPI.handler,
    //     config: {
    //         description: `This API use for check referralCode is exists in db or not`,
    //         tags: ['api', 'starUser'],
    //         auth: 'userJWT',
    //         validate: {
    //             headers: headerValidator.headerAuthValidator,
    //             query: GetTopAPI.QueryValidator,
    //             failAction: (req, reply, source, error) => {
    //                 failAction: headerValidator.faildAction(req, reply, source, error)
    //             }
    //         },
    //         response: GetTopAPI.response
    //     }
    // },
    {
        method: 'Get',
        path: '/topStarUsers',
        handler: GetTopAPI.handler,
        config: {
            description: `This API use for check referralCode is exists in db or not`,
            tags: ['api', 'starUser'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetTopAPI.QueryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetTopAPI.response
        }
    }
].concat(whomIfollow);