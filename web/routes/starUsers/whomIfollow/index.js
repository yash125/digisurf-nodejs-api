let headerValidator = require("../../../middleware/validator")
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'Get',
        path: '/starUsersWhomIFollow',
        handler: GetAPI.handler,
        config: {
            description: `This API use for check referralCode is exists in db or not`,
            tags: ['api', 'starUser'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.QueryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];