const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../../locales');
const userListCollection = require("../../../../models/userList");

/**
 * @description API to subscribe to a channel
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token
 * @property {mongodb object} channelId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const QueryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit")
})

const handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    const getUserList = () => {
        return new Promise((resolve, reject) => {

            var condition = [
                { "$match": { "_id": userId } },
                { "$unwind": "$follow" },
                { "$lookup": { "from": "customer", "localField": "follow", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                { "$match": { "userData.starRequest.starUserProfileStatusCode": 4, "userName": { "$nin": ["", null, " "] } } },
                {
                    "$project": {
                        "_id": "$userData._id", "firstName": "$userData.firstName", "lastName": { '$ifNull': ['$userData.lastName', " "] },
                        "private": "$userData.private", "businessProfile": "$userData.businessProfile",
                        "userName": "$userData.userName", "profilePic": "$userData.profilePic",
                        "userStatus": "$userData.userStatus", "blocked": "$userData.blocked",
                        "knownBy": { '$ifNull': ['$userData.starRequest.starUserKnownBy', ""] },
                        "profileCoverImage": "$userData.profileCoverImage"
                    }
                }, { "$skip": offset }, { "$limit": limit }
            ];

            userListCollection.Aggregate(condition, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {

                    for (let index = 0; index < d.length; index++) {
                        d[index]["lastName"] = d[index]["lastName"] || "";
                        d[index]["followStatus"] = 1
                        d[index]["private"] = (d[index] && d[index]["private"]) ? 1 : 0;
                        d[index]["friendStatusCode"] = (userData.friendList && userData.friendList.map(e => e.toString()).includes(d[index]["_id"].toString())) ? 2 : (userData.myFriendRequest && userData.myFriendRequest.map(e => e.toString()).includes(d[index]["_id"].toString())) ? 3 : 1;
                        d[index]["friendStatusText"] = (d[index]["friendStatusCode"] == 2) ? "friends" : (d[index]["friendStatusCode"] == 3) ? "userSentARequestToTargetUser" : "notAFirend";
                        d[index]["isStar"] = true;
                    }
                    return resolve({ code: 200, message: req.i18n.__('GetStarUserWhomIFollow')['200'], data: d });
                }
            });

        })
    };

    getUserData()
        .then(() => { return getUserList() })
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); });
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetStarUserWhomIFollow']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { QueryValidator, response, handler };

