const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const userListCollection = require("../../../models/userList")
const followCollection = require("../../../models/follow")


/**
 * @description API to subscribe to a channel
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token
 * @property {mongodb object} channelId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const QueryValidator = Joi.object({
    searchText: Joi.string().allow("").default("").description("searchText to be searched"),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit")
})

const handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    let username = (req.query.searchText) ? req.query.searchText.trim().toLowerCase() : "";
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;
    var followeeIdObj = {};

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    const getMyFollowee = () => {
        return new Promise((resolve, reject) => {
            followCollection.Select({ follower: userId }, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    for (let index = 0; index < result.length; index++) {
                        followeeIdObj[result[index].followee.toString()] = result[index]["type"];
                    }
                    return resolve(true);
                }
            })
        });
    };
    const getUserList = () => {
        return new Promise((resolve, reject) => {

            var condition = [
                { "$match": { "starRequest.starUserProfileStatusCode": 4, "userName": { "$nin": ["", null, " "] } } },
                {
                    "$project": {
                        "_id": 1, "firstName": 1, "registeredOn": 1, "private": 1, "businessProfile": 1,
                        "userName": 1, "profilePic": 1, "userStatus": 1, "blocked": 1,
                        "lastName": { '$ifNull': ['$lastName', " "] },
                        "knownBy": { '$ifNull': ['$starRequest.starUserKnownBy', ""] },
                        "fullName": { "$concat": ["$firstName", " ", "$lastName"] },
                        "legacyFullName": { "$concat": ["$firstName", "$lastName"] }

                    }
                }
            ];


            if (username !== "") {
                condition.push({
                    "$match": {
                        "userName": { "$exists": true },
                        "$or": [
                            // { "userName": { "$regex": username, "$options": "i" } },
                            { "firstName": { "$regex": username, "$options": "i" } },
                            { "knownBy": { "$regex": username, "$options": "i" } },
                            { "fullName": { "$regex": username, "$options": "i" } },
                            { "legacyFullName": { "$regex": username, "$options": "i" } },
                        ],
                        "userStatus": 1, "blocked": { "$ne": userId }
                    }
                });
            }
            condition.push({
                "$project": {
                    "_id": 1, "firstName": 1, "registeredOn": 1, "private": 1, "businessProfile": 1,
                    "userName": 1, "profilePic": 1, "knownBy": 1,
                    "lastName": { '$ifNull': ['$lastName', " "] },

                }
            });
            condition.push({ "$sort": { "userName": 1 } });
            condition.push({ "$skip": offset });
            condition.push({ "$limit": limit });

            userListCollection.Aggregate(condition, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {

                    for (let index = 0; index < d.length; index++) {
                        d[index]["lastName"] = d[index]["lastName"] || "";
                        d[index]["followStatus"] = (followeeIdObj[d[index]._id.toString()]) ? followeeIdObj[d[index]._id.toString()]["status"] : 0
                        d[index]["followStatus"] = (d[index]["followStatus"] == 3) ? 0 : d[index]["followStatus"];
                        d[index]["private"] = (d[index] && d[index]["private"]) ? 1 : 0;
                        d[index]["friendStatusCode"] = (userData.friendList && userData.friendList.map(e => e.toString()).includes(d[index]["_id"].toString())) ? 2 : (userData.myFriendRequest && userData.myFriendRequest.map(e => e.toString()).includes(d[index]["_id"].toString())) ? 3 : 1;
                        d[index]["friendStatusText"] = (d[index]["friendStatusCode"] == 2) ? "friends" : (d[index]["friendStatusCode"] == 3) ? "userSentARequestToTargetUser" : "notAFirend";
                        d[index]["isStar"] = true;
                    }

                    return resolve({ code: 200, message: req.i18n.__('GetStarUser')['200'], data: d });
                }
            });

        })
    };

    getUserData()
        .then(getMyFollowee)
        .then(getUserList)
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetStarUser']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { QueryValidator, response, handler };

