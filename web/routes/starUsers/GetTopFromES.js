const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const userListCollection = require("../../../models/userListES")
const followCollection = require("../../../models/follow")


/**
 * @description API to subscribe to a channel
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token
 * @property {mongodb object} channelId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const validator = Joi.object({
})

const QueryValidator = Joi.object({
    searchText: Joi.string().allow("").default("").description("searchText to be searched"),
    category: Joi.string().default("0").description(" categorie : 0 = All, 1 = Actor, 2 = Actress"),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit")
})

const handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    var categorie = (req.query.category) ? parseInt(req.query.category) : 0;

    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;
    var followeeIdObj = {};

    switch (categorie) {
        case "1":
        case 1: {
            categorie = "Actor";
            break;
        }
        case "2":
        case 2: {
            categorie = "Actress";
            break;
        }
    }
    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    const getMyFollowee = () => {
        return new Promise((resolve, reject) => {
            followCollection.Select({ follower: userId }, (err, result) => {
                if (err) {
                    logger.error("OKASOKQQOSK : ", err);
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    for (let index = 0; index < result.length; index++) {
                        followeeIdObj[result[index].followee.toString()] = result[index]["type"];
                    }
                    return resolve(true);
                }
            })
        });
    };
    const getUserList = () => {
        return new Promise((resolve, reject) => {

            var condition = {
                "must": [
                    { "match": { "starRequest.starUserProfileStatusCode": 4 } },
                    { "match": { "starRequest.categorieName": categorie } },
                    { "match": { "userStatus": 1 } }
                ],
                "must_not": [
                    { "match": { "userName": " " } },
                    { "match": { "userName": "null" } },
                ]
            };

            userListCollection.getDetails(condition, offset, limit, req.follow, userId.toString(), (e, d) => {
                if (e) {
                    logger.error("OKASOKAQQQOSK : ", e);
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {

                    for (let index = 0; index < d.length; index++) {
                        d[index]["lastName"] = d[index]["lastName"] || "";
                        d[index]["followStatus"] = (followeeIdObj[d[index]._id.toString()]) ? followeeIdObj[d[index]._id.toString()]["status"] : 0
                        d[index]["followStatus"] = (d[index]["followStatus"] == 3) ? 0 : d[index]["followStatus"];
                        d[index]["private"] = (d[index] && d[index]["private"]) ? 1 : 0;
                        d[index]["friendStatusCode"] = (userData.friendList && userData.friendList.map(e => e.toString()).includes(d[index]["_id"].toString())) ? 2 : (userData.myFriendRequest && userData.myFriendRequest.map(e => e.toString()).includes(d[index]["_id"].toString())) ? 3 : 1;
                        d[index]["friendStatusText"] = (d[index]["friendStatusCode"] == 2) ? "friends" : (d[index]["friendStatusCode"] == 3) ? "userSentARequestToTargetUser" : "notAFirend";
                        d[index]["isStar"] = true;
                    }

                    return resolve({ code: 200, message: req.i18n.__('GetStarUser')['200'], data: d });
                }
            });

        })
    };

    getUserData()
        .then(getMyFollowee)
        .then(getUserList)
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetStarUser']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { validator, QueryValidator, response, handler };