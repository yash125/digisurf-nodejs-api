'use strict'
const Joi = require("joi");
const Promise = require('promise');
const local = require('../../../locales');
const userList = require("../../../models/userList");
var bcrypt = require("bcryptjs")
var ObjectID = require('mongodb').ObjectID;

const validator = Joi.object({
    // currentPassword: Joi.string().allow("").required().description("currentPassword").error(new Error("currentPassword is required")),
    newPassword: Joi.string().required().description("newPassword").error(new Error("newPassword is required"))
}).unknown();

const hander = (req, res) => {

    // var currentPassword = req.payload.currentPassword;
    var newPassword = req.payload.newPassword;

    const updatePassword = () => {
        return new Promise((resolve, reject) => {

            // console.log("userData.password : ", bcrypt.compareSync(req.payload.currentPassword, userData.password));
            // console.log("req.payload.currentPassword : ", req.payload.currentPassword);

            userList.Update({ _id: ObjectID(req.auth.credentials._id.toString()) }, {
                password: bcrypt.hashSync(newPassword)
            }, (err) => {
                if(err){
                    return reject(err);
                }
                return resolve({ message: req.i18n.__('PatchPassword')['200'], code: 200 });
            });

            // if (userData && userData.password && bcrypt.compareSync(req.payload.currentPassword, userData.password)) {
            //     return reject({ message: req.i18n.__('PatchPassword')['412'], code: 412 });
            // }

        });
    };

    updatePassword()
        .then((data) => { return res({ message: data.message }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};


const response = {
    status: {
        200: { message: Joi.any().default(local['PatchPassword']['200']) },
        412: { message: Joi.any().default(local['PatchPassword']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code


module.exports = { validator, hander, response };