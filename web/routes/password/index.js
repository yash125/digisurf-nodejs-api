let headerValidator = require("../../middleware/validator")
let PatchAPI = require('./Patch');

module.exports = [
    {
        method: 'PATCH',
        path: '/password',
        handler: PatchAPI.hander,
        config: {
            description: `This API use for give update password `,
            tags: ['api', 'password'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PatchAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PatchAPI.response
        }
    }
];