let headerValidator = require("../../middleware/validator")
let postAPI = require('./Post');
let putAPI = require('./Put');
let editAPI = require('./Patch');
let deleteAPI = require('./Delete');
let getAPI = require('./GetById');
let getAllAPi = require('./Get');

module.exports = [
    {
        method: 'POST',
        path: '/collection',
        handler: postAPI.hander,
        config: {
            description: `This API is used to create collection`,
            tags: ['api', 'collection'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: postAPI.response
        }
    },
    {
        method: 'PUT',
        path: '/addToCollection',
        handler: putAPI.hander,
        config: {
            description: `This API is used to add posts to collection`,
            tags: ['api', 'collection'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload:putAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: putAPI.response
        }
    },
    {
        method: 'PUT',
        path: '/collection',
        handler: editAPI.hander,
        config: {
            description: `This API is used to edit collection`,
            tags: ['api', 'collection'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: editAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: editAPI.response
        }
    },
    {
        method: 'DELETE',
        path: '/collection',
        handler: deleteAPI.hander,
        config: {
            description: `This API is used to delete collection`,
            tags: ['api', 'collection'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: deleteAPI.validator,
                query:deleteAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: deleteAPI.response
        }
    },
    {
        method: 'GET',
        path: '/collection',
        handler: getAPI.hander,
        config: {
            description: `This API is used to get collection by Id collection`,
            tags: ['api', 'collection'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query:getAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: getAPI.response
        }
    },
    {
        method: 'GET',
        path: '/collections',
        handler: getAllAPi.hander,
        config: {
            description: `This API is used to get all collection of an user`,
            tags: ['api', 'collection'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query:getAllAPi.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: getAllAPi.response
        }
    }
];