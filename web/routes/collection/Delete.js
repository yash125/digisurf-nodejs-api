'use strict'
/** initialize  variables for this API  */
const Joi = require("joi");
Joi.objectId = require('joi-objectid')(Joi);
const logger = require('winston');
var ObjectID = require('mongodb').ObjectID
const Promise = require('bluebird');
const collectionDb = Promise.promisifyAll(require('../../../models/collection'));

const local = require('../../../locales');



/** query Validators */
const queryValidator = Joi.object({
    collectionId: Joi.objectId().required().description("collectionId").error(new Error("collection id is missing or invalid"))
}).unknown()

/**
 * @method  PUT deletecollection 
 * @description This API is used to delete collection
 * 
 * @property {string} collectionId
 * 
 * @returns 200 : Success
 * @returns 400 : Parameters missing
 * @returns 500 : Internal Server Error
 * 
 * @author Karthikb@mobifyi.com
 * @since 08-july-2019
 */



/** start API Handler */
const hander = async (req, res) => {

    const collectionId = req.query.collectionId;

    try {
        /** 
            * this function deletes collection from db
            * */

        /**_id must match the given collectionId */
        let condition = {
            _id: ObjectID(collectionId),
            userId: ObjectID(req.auth.credentials._id)
        }


        const result = await collectionDb.DeleteAsync(condition)

        /**if collectionId is not found collection is not updated and 204 is sent back */
        if (result.result.n == 1)
            return res({ message: req.i18n.__('deleteCollection')['200'] }).code(200);
        else
            return res({ message: req.i18n.__('deleteCollection')['204'] }).code(204);
    }
    catch (err) {
        logger.silly('collection delete error', err);
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }
};/** End API Handler */

const response = {
    status: {
        200: { message: Joi.any().default(local['deleteCollection']['200']).example(local['deleteCollection']['200']).required() },
        204: { message: Joi.any().default(local['deleteCollection']['204']).example(local['deleteCollection']['204']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']).example(local['genericErrMsg']['500']) }
    }
}//swagger response code


module.exports = { hander, response, queryValidator };