'use strict'
/** initialize  variables for this API  */
const Joi = require("joi");
const logger = require('winston');
const Promise = require('bluebird');
const ObjectID = require('mongodb').ObjectID
const collectionDb = Promise.promisifyAll(require('../../../models/collection'));

const local = require('../../../locales');


/** API Validators */
const validator = Joi.object({
    coverImage: Joi.string().description("coverImage url"),
    collectionName: Joi.string().required().description("name of the collection").error(new Error("collection name is required")),
    postId: Joi.array().description('Array post ids ["5d12328246acdc2b10e7697a","5d122e48db6918294deee1e7"] ').error(new Error("postId is missing or invalid")),
}).unknown()

/**
 * @method  POST createCollection 
 * @description This API is used to create collection,this is to be used only for creating a collection and not to add items to an existing collection
 * 
 * @property {string} collectionName
 * @property {array} postId
 * 
 * @returns 200 : Success
 * @returns 400 : Parameters missing
 * @returns 500 : Internal Server Error
 * 
 * @author Karthikb@mobifyi.com
 * @since 04-july-2019
 */



/** start API Handler */
const hander = (req, res) => {

    const _id = ObjectID(req.auth.credentials._id);
    const coverImage = req.payload.coverImage || '';
    var postIds = (req.payload.postId) ? req.payload.postId.map(e => ObjectID(e)) : [];
    const collectionName = req.payload.collectionName;
/* will use in futur
    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'], code: 204 });
                }
            })
        });
    }
    */
  
    /** 
     * this function inserts the posts into collection database
     * */
    const addPostsToDb = async () => {

        try {
            let data = {
                userId: _id,
                coverImage: coverImage,
                collectionName: collectionName,
                postIds: postIds
            }
            /**insert postIds into collection  */
            let result = await collectionDb.InsertAsync(data);
            
            return res({ message: req.i18n.__('collection')['200'], collectionId: result.ops[0]._id }).code(200);
        }
        catch (err) {
            logger.silly('err', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }

    }
    addPostsToDb();

};

const response = {
    status: {
        200: { message: Joi.any().default(local['collection']['200']), collectionId: Joi.any() },
        422: { message: Joi.any().default(local['collection']['422']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}//swagger response code


module.exports = { validator, hander, response };