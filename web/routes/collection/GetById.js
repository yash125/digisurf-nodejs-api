'use strict'
/** initialize  variables for this API  */
const Joi = require("joi");
Joi.objectId = require('joi-objectid')(Joi);
const logger = require('winston');
var ObjectID = require('mongodb').ObjectID
const Promise = require('bluebird');
const collectionDb = Promise.promisifyAll(require('../../../models/collection'));

const local = require('../../../locales');


/** query Validators */
const queryValidator = Joi.object({
    collectionId: Joi.objectId().required().description("collectionId").error(new Error("collection id is missing or invalid")),
    offset: Joi.string().description("pagination offset"),
    limit: Joi.string().description("pagination limit"),

}).unknown()

/**
 * @method  GET Collection 
 * @description This api is used to get collection by its id
 * 
 * @property {string} collectionId
 * @property {string} offset
 * @property {string} limit
 * 
 * @returns 200 : data fetched successfully
 * @returns 400 : Parameters missing
 * @returns 500 : Internal Server Error
 * 
 * @author Karthikb@mobifyi.com
 * @since 08-july-2019
 */



/** start API Handler */
const hander = async (req, res) => {

    let collectionId = ObjectID(req.query.collectionId);
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;

    /** 
     * fetch data from database 
     * */

    try {
        /**aggregation condition to fetch cover image of all the post in postIds array of the given collectionId */
        let condition = [
            { $match: { "_id": collectionId } },
            { $unwind: "$postIds" },
            {
                $lookup: {
                    from: "posts",
                    localField: "postIds",
                    foreignField: "_id",
                    as: "postDetail"
                }
            },
            { "$unwind": "$postDetail" },
            { $project: { _id: 0, postId: "$postDetail._id", collectionName: 1, thumbnailUrl1: "$postDetail.thumbnailUrl1" } },
            { "$skip": offset },
            { "$limit": limit }
        ];

        const result = await collectionDb.AggregateAsync(condition);
        if (result && result.length) {

            collectionDb.Update({ _id: collectionId }, { coverImage: result[0].thumbnailUrl1 }, (e, r) => {
                if (e) console.log("error : ", e)
            })
            return res({

                message: req.i18n.__('getByIdCollection')['200'], data: result.map(e => {
                    return {
                        "collectionName": e.collectionName,
                        "postId": e.postId,
                        "thumbnailUrl1": e.thumbnailUrl1,
                        _id: e.postId
                    }
                })
            }).code(200);
        } else {
            return res({}).code(204);
        }
    }
    catch (err) {
        logger.error('get collection api error', err);
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }





};/** End API Handler */

const response = {
    status: {
        200: { message: Joi.any().default(local['getByIdCollection']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['getByIdCollection']['204']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}//swagger response code


module.exports = { hander, response, queryValidator };