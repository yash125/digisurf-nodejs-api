'use strict'
/** initialize  variables for this API  */
const Joi = require("joi");
Joi.objectId = require('joi-objectid')(Joi);
var ObjectID = require('mongodb').ObjectID
const Promise = require('bluebird');
const collectionDb = Promise.promisifyAll(require('../../../models/collection'));

const local = require('../../../locales');


/** payload Validators */
const validator = Joi.object({
    coverImage: Joi.string().description('cover image of the collection'),
    collectionName: Joi.string().description('new name of the collection').required().error(new Error("collection name is missing")),
    collectionId: Joi.objectId().required().description("collectionId").error(new Error("collection Id must be valid"))

}).unknown()



/**
 * @method  PUT Collection 
 * @description This API is used to edit collection
 * 
 * @property {string} coverImage
 * @property {array} name
 * @property {string} collectionID
 * 
 * @returns 200 : Success
 * @returns 400 : Parameters missing
 * @returns 500 : Internal Server Error
 * 
 * @author Karthikb@mobifyi.com
 * @since 05-july-2019
 */



/** start API Handler */
const hander = async(req, res) => {

    let coverImage = req.payload.coverImage || '';
    let collectionId = req.payload.collectionId;
    let collectionName = req.payload.collectionName;

    /** 
     * this function adds the updated postids to the collection db
     * */

        try {
            /**_id must match the given collectionId */
            let condition = {

                _id: ObjectID(collectionId),
                userId: ObjectID(req.auth.credentials._id)
            }
            let data = {
                coverImage,
                collectionName
            }


            const result = await collectionDb.UpdateAsync(condition, data)

            /**if collectionId is not found collection is not updated and 204 is sent back */
            if (result.result.n == 1)
                return res({ message: req.i18n.__('patchCollection')['200'] }).code(200);
            else
                return res({ message: req.i18n.__('patchCollection')['204'] }).code(204);
        } catch (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }





};/** End API Handler */

const response = {
    status: {
        200: { message: Joi.any().default(local['patchCollection']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['patchCollection']['204']), data: Joi.any() },
        500: { message: Joi.any().default(local['genericErrMsg']['500']), data: Joi.any() }
    }
}//swagger response code


module.exports = { validator, hander, response };