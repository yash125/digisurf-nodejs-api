'use strict'
/** initialize  variables for this API  */
const Joi = require("joi");
Joi.objectId = require('joi-objectid')(Joi);
const logger = require('winston');
var ObjectID = require('mongodb').ObjectID
const Promise = require('bluebird');
const collectionDb = Promise.promisifyAll(require('../../../models/collection'));
const userList = Promise.promisifyAll(require('../../../models/userList'));

const local = require('../../../locales');

const validator = Joi.object({
    // postId: Joi.array().description('Array post ids ex:["5d12328246acdc2b10e7697a","5d122e48db6918294deee1e7"] ').required().error(new Error("postId is missing or invalid")), 
    postIds: Joi.array().items(Joi.objectId()).description('Array post ids ex:["5d12328246acdc2b10e7697a","5d122e48db6918294deee1e7"] ').required().error(new Error("postId is missing or invalid")),
    collectionId: Joi.objectId().required().description("collectionId").error(new Error("collectionId is missing or invalid")),

}).unknown()


/**
 * @method  PUT Collection 
 * @description This API is used to add postIds to collection
 * 
 * @property {string} collectionId
 * @property {array} postId
 * 
 * @returns 200 : Success
 * @returns 400 : Parameters missing
 * @returns 500 : Internal Server Error
 * 
 * @author Karthikb@mobifyi.com
 * @since 04-july-2019
 */



/** start API Handler */
const hander = async (req, res) => {

    const postIds = req.payload.postIds.map(e => ObjectID(e));
    const collectionId = req.payload.collectionId;


    /** 
     * this function adds the updated postids to the collection db
     * */

    try {
        /**_id must match the given collectionId */
        let condition = {
            _id: ObjectID(collectionId),
            userId: ObjectID(req.auth.credentials._id)
        }

         await userList.UpdateToArrayAsync({ _id: ObjectID(req.auth.credentials._id) }, postIds);
        const result = await collectionDb.UpdateToArrayAsync(condition, postIds);
        /**if collectionId is not found collection is not updated and 409 is sent back */
        if (result.result.n == 1 && result.result.nModified == 1)
            return res({ message: req.i18n.__('putCollection')['200'] }).code(200);
        else if (result.result.n == 1 && result.result.nModified == 0)
            return res({}).code(204);
        else
            return res({ message: req.i18n.__('putCollection')['409'] }).code(409);
    } catch (err) {
        logger.error('put collection error', err);
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }





};/** End API Handler */

const response = {
    status: {
        200: { message: Joi.any().default(local['putCollection']['200']) },
        409: { message: Joi.any().default(local['putCollection']['409']) },
        209: { message: Joi.any().default(local['putCollection']['209']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}//swagger response code


module.exports = { validator, hander, response };