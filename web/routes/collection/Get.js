'use strict'
/** initialize  variables for this API  */
const Joi = require("joi");
Joi.objectId = require('joi-objectid')(Joi);
const logger = require('winston');
var ObjectID = require('mongodb').ObjectID
const Promise = require('bluebird');

const local = require('../../../locales');
const check = require('./../../../library/checkEmpty');
const collectionDb = Promise.promisifyAll(require('../../../models/collection'));
const userListCollection = Promise.promisifyAll(require('../../../models/userList'));
const postsCollection = Promise.promisifyAll(require('../../../models/posts'));


/** query Validators */
const queryValidator = Joi.object({
    offset: Joi.string().description("pagination offset"),
    limit: Joi.string().description("pagination limit")
}).unknown()

/**
 * @method  GET AllCollection 
 * @description This api is used to get all collection of an user
 * 
 * 
 * @returns 200 : data fetched successfully
 * @returns 500 : Internal Server Error
 * 
 * @author Karthikb@mobifyi.com
 * @since 08-july-2019
 */

/** start API Handler */
const hander = async (req, res) => {

    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;

    /** 
     * fetch data from database 
     **/
    try {
        var dataToSend = [{ "collectionName": "AllPost", "images": [] }];
        var bookmarks = await userListCollection.SelectOneAsync({ _id: ObjectID(req.auth.credentials._id) });
        bookmarks = (bookmarks && bookmarks.bookmarks && bookmarks.bookmarks.length) ? bookmarks.bookmarks : [];
        var data = await postsCollection.SelectAsync({ _id: { "$in": bookmarks.map(e => ObjectID(e)) }, postStatus: 1 });
        data.forEach(element => {
            if (dataToSend[0]["images"].length < 4) {
                dataToSend[0]["images"].push(element.thumbnailUrl1)
            }
        });
        /**get collection of the user*/
        // const result = await collectionDb.SelectAll({ userId: ObjectID(req.auth.credentials._id) }, offset, limit);
        const result = await collectionDb.AggregateAsync([
            { "$match": { "userId": ObjectID(req.auth.credentials._id) } },
            {
                "$unwind": {
                    "path": "$postIds",
                    "preserveNullAndEmptyArrays": true
                }
            }, {
                "$lookup": {
                    "from": "posts",
                    "localField": "postIds",
                    "foreignField": "_id",
                    "as": "postData"
                }
            },
            {
                "$unwind": {
                    "path": "$postData",
                    "preserveNullAndEmptyArrays": true
                }
            },
            {
                "$group": {
                    "_id": "$_id",
                    "userId": { "$last": "$userId" },
                    "coverImage": { "$last": "$postData.thumbnailUrl1" },
                    "collectionName": { "$last": "$collectionName" },
                    "postIds": { "$push": "$postIds" },
                }
            },
            { "$skip": offset }, { "$limit": limit }
        ]);


        // console.log('result', dataToSend[0].images);
        if (check.isEmpty(dataToSend[0].images) && check.isEmpty(result))
            return res({}).code(204);
        else {
            dataToSend = dataToSend.concat(result)
            return res({ message: req.i18n.__('getCollection')['200'], data: dataToSend }).code(200);
        }


    }
    catch (err) {
        logger.error('get collection api error', err);
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }
};/** End API Handler */

const response = {
    status: {
        200: { message: Joi.any().default(local['getCollection']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['getCollection']['204']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}//swagger response code


module.exports = { hander, response, queryValidator };