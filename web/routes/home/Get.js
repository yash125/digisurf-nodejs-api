'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const postsCollection = require("../../../models/posts");
const followCollection = require("../../../models/follow");
const userListCollection = require("../../../models/userList");


const validator = Joi.object({
    "offset": Joi.string().description('skip'),
    "limit": Joi.string().description('limit'),
}).required();

const handler = (req, res) => {
    const userId = ObjectID(req.auth.credentials._id);
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    var userIds = [], userIDES = [];

    followCollection.GetFollowerCount(userId, (err, result) => {
        if (err) {
            return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
        } else {
            let followerCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
            userListCollection.UpdateById(userId.toString(), { "count.followerCount": followerCount }, () => { })
        }
    })
    followCollection.GetFolloweeCount(userId, (err, result) => {
        if (err) {
            return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
        } else {
            let followeeCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
            userListCollection.UpdateById(userId.toString(), { "count.followeeCount": followeeCount }, () => { })
        }
    });
    postsCollection.GetPostCount({ userId: userId, "postStatus": 1, channelId: "" }, (err, result) => {
        if (err) {
            return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
        } else {
            userListCollection.UpdateById(userId.toString(), { "count.postsCount": result }, (e) => { logger.error((e) ? e : "") });
        }
    });

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    function getFolloweeIds() {
        return new Promise((resolve) => {

            userIds.push({ userId: userId });
            userIDES.push({ "match": { userId: userId.toString() } })
            if (userData && userData.follow) {
                userData.follow.forEach(function (element) {
                    userIds.push({ userId: ObjectID(element.toString()) });
                    userIDES.push({ "match": { userId: element.toString() } })
                }, this);
            }
            if (userData && userData.subscribeChannels) {
                userData.subscribeChannels.forEach(function (element) {
                    userIDES.push({ "match": { userId: element.toString() } })
                    userIds.push({ userId: ObjectID(element.toString()) });
                }, this);
            }


            return resolve(true);

        });
    }
    function getPost() {
        return new Promise((resolve, reject) => {

            var condition = {
                "postStatus": 1,
                "$or": userIds
            };
            postsCollection.SelectWithProject(condition, offset, limit, { _id: -1 }, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        element["distinctViews"] = (element["distinctViews"]) ? element["distinctViews"].length : 0;
                        element["totalComments"] = (element["totalComments"]) ? element["totalComments"] : 0;
                        element["profilePic"] = element["profilepic"];
                        element["comments"] = (element["comments"]) ? element["comments"].slice(element["comments"].length - 3).reverse() : [];
                        element["hashTags"] = [];
                        element["totalViews"] = [];
                        element["mentionedUsers"] = [];
                        element["firstName"] = (element["firstName"] != "null" && element["firstName"] != null && typeof element["firstName"] != 'undefined') ? element["firstName"] : "";
                        element["lastName"] = (element["lastName"] != "null" && element["lastName"] != null && typeof element["lastName"] != 'undefined') ? element["lastName"] : "";
                        element["postId"] = element._id
                        if (element.musicId == "") delete element.musicData
                        element["postId"] = element._id

                        element["followStatus"] = (userData.follow && userData.follow.map(e => e.toString()).includes(element["userId"])) ? 1 : 0;
                        element["liked"] = (element["likes"]) ? element["likes"].map(id => id.toString()).includes(userId.toString()) : false;
                        delete element["likes"];
                    }, this);
                    
                    let responseobj = { code: 200, message: req.i18n.__('GetHome')['200'], data: result };
                    return resolve(responseobj);
                }
            });
        });
    }


    getUserData()
        .then(() => { return getFolloweeIds(); })
        .then(() => { return getPost(); })
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); });
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetHome']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code

module.exports = { validator, handler, response };