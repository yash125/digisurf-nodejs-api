'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const postsCollection = require("../../../models/posts");
const postESCollection = require("../../../models/postES");
const followCollection = require("../../../models/follow");
const userListCollection = require("../../../models/userList");
const channelCollection = require("../../../models/channel");
const businessProductTypeCollection = require("../../../models/businessProductType");

const validator = Joi.object({
    "offset": Joi.string().description('skip'),
    "limit": Joi.string().description('limit'),
}).unknown()

const handler = (req, res) => {
    const userId = ObjectID(req.auth.credentials._id);
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    var userIDES = [], filter = [], follow = [], Must_not = [], businessPostTypeLabel = {};

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            followCollection.GetFollowerCount(userId, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    let followerCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                    userListCollection.UpdateById(userId.toString(), { "count.followerCount": followerCount }, () => { })
                }
            })
            followCollection.GetFolloweeCount(userId, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    let followeeCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                    userListCollection.UpdateById(userId.toString(), { "count.followeeCount": followeeCount }, () => { })
                }
            });
            postsCollection.GetPostCount({ userId: userId, "postStatus": 1, channelId: "" }, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    userListCollection.UpdateById(userId.toString(), { "count.postsCount": result }, (e) => { logger.error((e) ? e : "") });
                }
            });


            channelCollection.Select({ userId: userId, "private": true, "channelStatus": 1 }, (err, data) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (data && data.length) {
                    userListCollection.UpdateById(userId.toString(), { "count.privateChannel": data.length }, (e) => { logger.error((e) ? e : "") });
                } else {
                    userListCollection.UpdateById(userId.toString(), { "count.privateChannel": 0 }, (e) => { logger.error((e) ? e : "") });
                }
            });
            channelCollection.Select({ userId: userId, "private": false, "channelStatus": 1 }, (err, data) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (data && data.length) {
                    userListCollection.UpdateById(userId.toString(), { "count.publicChannel": data.length }, (e) => { logger.error((e) ? e : "") });
                } else {
                    userListCollection.UpdateById(userId.toString(), { "count.publicChannel": 0 }, (e) => { logger.error((e) ? e : "") });
                }
            });
            channelCollection.Select({ userId: userId, "channelStatus": 1 }, (err, data) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (data && data.length) {
                    userListCollection.UpdateById(userId.toString(), { "count.totalChannel": data.length }, (e) => { logger.error((e) ? e : "") });
                } else {
                    userListCollection.UpdateById(userId.toString(), { "count.totalChannel": 0 }, (e) => { logger.error((e) ? e : "") });
                }
            });


            channelCollection.Select({ "confirm.userId": userId, "channelStatus": 1 }, (err, data) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (data && data.length) {
                    data.forEach(function (element) {
                        filter.push({ "match": { "channelId": element._id.toString() } });
                    }, this);
                }
            });
            //          filter.push({ "match": { "channelId": new ObjectID() } });
            //             filter.push({ "match": { "channelId": "" } });

            channelCollection.Select({ "userId": userId, "channelStatus": 1 }, (err, data) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (data && data.length) {
                    data.forEach(function (element) {
                        filter.push({ "match": { "channelId": element._id.toString() } });
                    }, this);
                }
            });

            followCollection.Select({ "follower": userId, "type.status": 1, "end": false }, (err, result) => {
                if (result && result[0]) {
                    result.forEach(function (element) {
                        userIDES.push({ "match": { userId: element.followee.toString() } });
                        follow.push(element.followee.toString());
                    }, this);
                }
                userIDES.push({ "match": { userId: userId.toString() } })
                // filter.push({ "match": { userId: userId.toString() } })

                userListCollection.Select({ "blocked": userId }, (err, result) => {
                    if (result && result[0]) {
                        result.forEach(function (element) {
                            Must_not.push({ "match": { "userId": element._id.toString() } });
                        }, this);
                    }
                    return resolve(true);
                })
            });
        });
    }

    function getChannelData() {
        return new Promise((resolve, reject) => {

            let conditionchannels = {
                "_id": { "$nin": filter.map(e => ObjectID(e.match.channelId)) },
                "userId": { "$in": userIDES.map(e => ObjectID(e.match.userId)) }
            }
            
            channelCollection.Select(conditionchannels, (err, data) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (data && data.length) {
                    data.forEach(function (element) {
                        Must_not.push({ "match": { "channelId": element._id.toString() } });
                    }, this);
                    return resolve(true);
                } else {
                    return resolve(true);
                }
            });
        });
    }

    function getBusinessPostTypeLabel() {
        return new Promise((resolve, reject) => {
            businessProductTypeCollection.Select({}, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    result.forEach(element => {
                        businessPostTypeLabel[element._id.toString()] = element.text[req.headers.lang]
                    });
                }
                return resolve(true);
            });
        });
    }

    function getPost() {
        return new Promise((resolve, reject) => {

            let condition = {
                // "must": {
                //     "bool": {
                //         //                     "must": { "match": { "postStatus": 1 } },
                //         "should": userIDES.concat(filter)
                //     }
                // },
                "filter": {
                    "bool": {
                        "must": {
                            "bool": {
                                "must": [{ "term": { "postStatus": 1 } }],
                                "should": {
                                    "bool": {
                                        "should": userIDES.concat(filter),
                                        "must_not": Must_not
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            postESCollection.getPostDetails(condition, offset, limit, follow, userId, (err, result) => {
                if (err) {
                    console.log("error ", err)
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else if (result && result.length) {

                    result.forEach(element => {
                        element["isBookMarked"] = (userData && userData.bookmarks && userData.bookmarks.map(e => e.toString()).includes(element.postId)) ? true : false;
                        if (element["business"] && element["business"]["businessPostType"]) element["business"]["businessPostTypeLabel"] = businessPostTypeLabel[element["business"]["businessPostType"]] || "";
                        if (element["comments"] && element["comments"].length) element["comments"].reverse();
                        if (element["channelId"] && element["channelId"].length) element["isChannelSubscribed"] = 1;//send 1 default, becuse post are here cause user has subscribe a channel
                    });


                    let responseobj = { code: 200, message: req.i18n.__('GetHome')['200'], data: result };
                    return resolve(responseobj);
                } else {
                    let responseobj = { code: 204, message: req.i18n.__('genericErrMsg')['204'] };
                    return reject(responseobj);
                }
            });
        });
    }

    getUserData()
        .then(() => { return getFolloweeIds(); })
        .then(() => { return getChannelData(); })
        .then(() => { return getBusinessPostTypeLabel() })
        .then(() => { return getPost(); })
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); });
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetHome']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code

module.exports = { validator, handler, response };