'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const postsCollection = require("../../../../models/posts");
const postESCollection = require("../../../../models/postES");
const postCollection = require("../../../../models/posts");
const followCollection = require("../../../../models/follow");
const userListCollection = require("../../../../models/userList");
const channelCollection = require("../../../../models/channel");
const businessProductTypeCollection = require("../../../../models/businessProductType");

const validator = Joi.object({
    "offset": Joi.string().description('skip'),
    "limit": Joi.string().description('limit'),
}).unknown()

const handler = (req, res) => {
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    var follow = [], businessPostTypeLabel = {};


    function getBusinessPostTypeLabel() {
        return new Promise((resolve, reject) => {
            businessProductTypeCollection.Select({}, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    result.forEach(element => {
                        businessPostTypeLabel[element._id.toString()] = element.text[req.headers.lang]
                    });
                }
                return resolve(true);
            });
        });
    }

    function getPost() {
        return new Promise((resolve, reject) => {

            postCollection.getPostDetails({ postStatus: 1 }, offset, limit, follow, new ObjectID(), (err, result) => {
                if (err) {
                    console.log("error ", err)
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else if (result && result.length) {

                    result.forEach(element => {
                        element["isBookMarked"] = false;
                        if (element["business"] && element["business"]["businessPostType"]) element["business"]["businessPostTypeLabel"] = businessPostTypeLabel[element["business"]["businessPostType"]] || "";
                        if (element["comments"] && element["comments"].length) element["comments"].reverse();
                        if (element["channelId"] && element["channelId"].length) element["isChannelSubscribed"] = 1;//send 1 default, becuse post are here cause user has subscribe a channel
                    });

                    let responseobj = { code: 200, message: req.i18n.__('GetHome')['200'], data: result };
                    return resolve(responseobj);
                } else {
                    let responseobj = { code: 204, message: req.i18n.__('genericErrMsg')['204'] };
                    return reject(responseobj);
                }
            });
        });
    }

    getBusinessPostTypeLabel()
        .then(() => { return getPost(); })
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); });
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetHome']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code

module.exports = { validator, handler, response };