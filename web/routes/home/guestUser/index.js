let headerValidator = require("../../../middleware/validator")
let GetAPI = require('./Get');// require('./Get');

module.exports = [
    {
        method: 'Get',
        path: '/home/guestUser',
        handler: GetAPI.handler,
        config: {
            description: `This API use for get posts`,
            tags: ['api', 'home'],
            auth: "basic",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];