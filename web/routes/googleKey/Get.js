'use strict'
//require npm modules
const Joi = require("joi");

//require dependency
const local = require('../../../locales');
const googleKeysCollection = require("../../../models/googleKeys");

//validator for this API
const validator = Joi.object({
    googleAPIKey: Joi.string().description("offset, query parameter")
}).unknown();

/**
 * @method GET googleKey
 * @description This API returns google api Key
 * 
 * @headers {string} authorization : token
 * @headers {string} lang by defualt en
 * 
 * @author DipenAhir
 * @since 10-Sep-2019
 */

//start API hender
const hander = (req, res) => {

    //API variables
    let googleAPIKey = req.query.googleAPIKey || "";

    googleKeysCollection.Update({ "key": googleAPIKey }, { status: 0 }, (err) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            googleKeysCollection.SelectOne({ status: 1 }, (err, result) => {
                if (err) {
                    return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
                } else if (result && result._id) {
                    return res({ message: req.i18n.__('genericErrMsg')['200'], data: { key: result.key } }).code(200);
                } else {
                    return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);
                }
            });
        }
    });
};//end API hender

//start API response
const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//end API response

//Export all constance
module.exports = { validator, hander, response };