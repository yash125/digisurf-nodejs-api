let headerValidator = require("../../middleware/validator")
let GetAPI = require('./Get');
let PostAPI = require('./Post');
let DeleteAPI = require('./Delete');

module.exports = [
    {
        method: 'Delete',
        path: '/comment',
        handler: DeleteAPI.handler,
        config: {
            description: `This API use for signup or login from facebook with facebook tokenXXX`,
            tags: ['api', 'comment'],
            auth: "user",
            validate: {
                query: DeleteAPI.validator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
    {
        method: 'GET',
        path: '/comment',
        handler: GetAPI.handler,
        config: {
            description: `This API use for signup or login from facebook with facebook tokenXXX`,
            tags: ['api', 'comment'],
            auth: "user",
            validate: {
                query: GetAPI.queryValidator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }, {
        method: 'POST',
        path: '/comment',
        handler: PostAPI.handler,
        config: {
            tags: ['api', 'comment'],
            description: 'API to comment on a post',
            auth: 'userJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                payload: PostAPI.payloadValidator,
                // query : PostAPI.paramsValidator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    }
].concat(require("./like"));