'use strict'
const Joi = require("joi");

const moment = require('moment');
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../../locales');
const fcm = require("../../../../library/fcm");
const comments_like = require("../../../../models/comments_like");

/**
 * @description API used to make like or dis like on comment
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} commentId
 * @property {boolean} isNewLike
 * @returns 200 : Success
 * @returns 500 : Internal Server Error
 * @returns 400 : required field is missing
 */


const Validator = Joi.object({
    skip: Joi.string().default("0").description("skip"),
    limit: Joi.string().default("20").description("limit"),
    commentId: Joi.string().min(24).max(24).required().description("mongodb id of comment(eg.5a78059fdcf23c1284ea6581)"),
}).unknown();

const handler = (req, res) => {


    const userId = ObjectID(req.auth.credentials._id);
    const commentId = ObjectID(req.query.commentId);
    const skip = (req.query && req.query.skip) ? parseInt(req.query.skip) : 0;
    const limit = (req.query && req.query.limit) ? parseInt(req.query.limit) : 20;

    const getList = () => {
        return new Promise((resolve, reject) => {

            let condition = [
                { "$match": { "commentId": commentId } },
                {
                    "$lookup": {
                        "localField": "likedBy",
                        "from": "customer",
                        "foreignField": "_id",
                        "as": "userData"
                    }
                }, { "$unwind": "$userData" }, { "$match": { "userData.userStatus": 1 } },
                {
                    "$project": {
                        "commentId": 1, "likedBy": 1, "timestamp": 1,
                        "number": "$userData.number", "userName": "$userData.userName",
                        "firstName": "$userData.firstName", "lastName": "$userData.lastName",
                        "profilePic": "$userData.profilePic", "number": "$userData.number"
                    }
                },
                { "$sort": { "timestamp": -1 } },
                { "$skip": skip }, { "$limit": limit }
            ];

            comments_like.Aggregate(condition, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                } else if (result && result.length) {
                    return resolve({ message: req.i18n.__('genericErrMsg')['200'], data: result });
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'], code: 204 });
                }
            });
        });
    }

    getList()
        .then((data) => {
            return res({ message: req.i18n.__('genericErrMsg')['200'], data: data.data }).code(200);
        })
        .catch((error) => {
            console.log("error : ", error)
            return res({ message: error.message }).code(error.code);
        });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code


module.exports = { Validator, response, handler };