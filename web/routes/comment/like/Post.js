'use strict'
const Joi = require("joi");

const moment = require('moment');
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../../locales');
const fcm = require("../../../../library/fcm");
const comments_like = require("../../../../models/comments_like");

/**
 * @description API used to make like or dis like on comment
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} commentId
 * @property {boolean} isNewLike
 * @returns 200 : Success
 * @returns 500 : Internal Server Error
 * @returns 400 : required field is missing
 */


const payloadValidator = Joi.object({
    isNewLike: Joi.boolean().required().description("isNewLike true / false"),
    commentId: Joi.string().min(24).max(24).required().description("mongodb id of comment(eg.5a78059fdcf23c1284ea6581)"),
}).unknown();

const handler = (req, res) => {


    const userId = ObjectID(req.auth.credentials._id);
    const commentId = ObjectID(req.payload.commentId);

    const makeLikeOrDislike = () => {
        return new Promise((resolve, reject) => {
            if (req.payload.isNewLike) {
                comments_like.Insert({
                    commentId: commentId,
                    likedBy: userId,
                    timestamp: new Date().getTime()
                }, (err, result) => {
                    if (err) {
                        return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                    }
                    return resolve();
                });
            } else {
                comments_like.Delete({
                    commentId: commentId,
                    likedBy: userId
                }, (err, result) => {
                    if (err) {
                        return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                    }
                    return resolve();
                });
            }
        });
    }



    makeLikeOrDislike()
        .then((data) => {
            return res({ message: req.i18n.__('genericErrMsg')['200'] }).code(200);
        })
        .catch((error) => { return res(error).code(error.code); });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code


module.exports = { payloadValidator, response, handler };