let headerValidator = require("../../../middleware/validator")
let PostAPI = require('./Post');
let GetAPI = require('./Get');


module.exports = [
    {
        method: 'POST',
        path: '/comment/like',
        handler: PostAPI.handler,
        config: {
            tags: ['api', 'comment'],
            description: 'API to make a like or dislike  on comment',
            auth: 'user',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                payload: PostAPI.payloadValidator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
    {
        method: 'Get',
        path: '/comment/like',
        handler: GetAPI.handler,
        config: {
            tags: ['api', 'comment'],
            description: 'API to get list of users which liked a comment',
            auth: 'user',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                query: GetAPI.Validator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }

];