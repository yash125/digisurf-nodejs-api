'use strict'
const Joi = require("joi");

const moment = require('moment');
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../locales');
const fcm = require("../../../library/fcm");
const postES = require("../../../models/postES");
// const rabbitMq = require('../../../library/rabbitMq');
const postCollection = require("../../../models/posts");
const commentCollection = require("../../../models/comments");
const hashTagCollection = require("../../../models/hashTags");
const userListCollection = require("../../../models/userList");
const notificationCollection = require("../../../models/notification");
// var sendPushUtility = require("../../../library/iosPush/sendPush.js");


/**
 * @description API to comment on a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} postId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const payloadValidator = Joi.object({
    comment: Joi.string().required().description("comment"),
    postedBy: Joi.string().required().description("postedBy userId Eg. 5a815483041eec3ac8e4daa9"),
    hashTags: Joi.string().allow("").description("hashTags"),
    ip: Joi.string().allow("").description("ip"),
    city: Joi.string().allow("").description("city"),
    country: Joi.string().allow("").description("country"),
    userTags: Joi.array().allow(null).description("userTag"),
    postId: Joi.string().min(24).max(24).required().description("mongodb id of post(eg.5a78059fdcf23c1284ea6581)"),
}).unknown();

const paramsValidator = Joi.object({
    postId: Joi.string().min(24).max(24).required().description("mongodb id of post(eg.5a78059fdcf23c1284ea6581)"),
}).required();

const handler = (req, res) => {


    const userId = ObjectID(req.auth.credentials._id);
    const postId = ObjectID(req.payload.postId);
    const commentString = req.payload.comment.trim();
    var userTagIds = [];


    var re3 = /\B@([^.\s]+)/gi;
    var str = commentString;
    var m, n;
    var taggedUsers = [];
    var userNameArr = [];
    const hashtags = [];
    const hashtaRegex = /\B#([^.\s]+)/gi;
    while ((n = hashtaRegex.exec(str)) !== null) {
        hashtags.push(n[1]);
    }

    while ((m = re3.exec(str)) !== null) {
        taggedUsers.push(m[1]);
    }

    taggedUsers.forEach(function (item) {
        item = item.replace(/[^a-zA-Z0-9]/g, ""); //remove all the special characters
        userNameArr.push(item);
    });

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'], code: 204 });
                }
            })
        });
    }


    const sendPushOnTagUsers = () => {
        return new Promise((resolve, reject) => {
            if (req.payload.userTags) {
                userListCollection.Select({ "userName": { "$in": req.payload.userTags } }, (err, result) => {
                    if (err) {
                        logger.error("at sendPushOnTagUsers: ", err);
                        return reject(true);
                    }
                    postCollection.Select({ _id: postId }, (err, postData) => {
                        if (err) {
                            logger.error("at sendPushOnTagUsers: ", err);
                        }
                        result.forEach(element => {
                            if (element._id != userId) {
                                userTagIds.push({ userName: element.userName, userId: ObjectID(element._id.toString()) })
                                let request = {
                                    fcmTopic: element._id,
                                    // action: 1,
                                    pushType: 2,
                                    title: process.env.APP_NAME,
                                    msg: userData.userName + " mentioned you in a comment ",
                                    data: { "type": "postCommented", "postId": postId, userImageUrl: userData.profilePic, imageUrl: postData[0].imageUrl1 },
                                    deviceType: (element && element.deviceInfo && element.deviceInfo.deviceType) ? element.deviceInfo.deviceType : "2"
                                }
                                fcm.notifyFcmTpic(request, () => { });

                                // let message = { alert: 'PostCommented', msg: request.msg };
                                // sendPushUtility.iosPushIfRequiredOnMessage({
                                //     targetId: element._id, message: message, userId: userId.toString(),
                                //     "type": "postCommented", "postId": postId, userImageUrl: userData.profilePic, imageUrl: postData[0].imageUrl1
                                // });
                            }
                        });
                        return resolve(true);
                    });
                });
            } else {
                return resolve(true);
            }
        });
    };
    //get userIds 
    const getUserIds = () => {
        return new Promise((resolve, reject) => {

            userListCollection.SelectWithSort({ userName: { $in: userNameArr } },
                { userName: 1 },
                { userName: 1 }, 0, 100,
                (e, d) => {
                    if (e) {
                        return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (d.length === 0) {
                        // let responseObj = { code: 201, message: 'no users found' };
                        return resolve(d);
                    } else {
                        return resolve(d);
                    }
                });
        });
    };
    //comment 
    const comment = (taggedUsers) => {
        return new Promise((resolve, reject) => {

            let dataToInsert = {
                commentedBy: userId,
                postId: ObjectID(req.payload.postId),
                commentedOn: new Date(),
                timeStamp: new Date().getTime(),
                comment: commentString,
                hashtags: hashtags,
                mentionedUsers: userTagIds,
                ip: req.payload.ip,
                city: req.payload.city,
                country: req.payload.country
            };

            commentCollection.Insert(dataToInsert, (e, d) => {
                if (e) {
                    reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    let result = [];
                    result.push(d);
                    result.push(taggedUsers);
                    resolve(result);
                }
            });
            postCollection.SelectOne({ _id: postId }, (err, result) => {
                if (result && result.userId.toString() != userId.toString()) {

                    commentCollection.Select({
                        commentedBy: userId,
                        postId: postId
                    }, (err, resss) => {
                        if (resss && resss.length == 1) {
                            // rabbitMq.sendToQueue(rabbitMq.comment_mqtt_queue, { "userId": userId.toString(), "userName": userData.userName, "userLang": req.headers.lang, "starUserId": result.userId.toString() }, () => { })
                        }
                    })
                    var fcmTopicUserId = result.userId;
                    var deviceType = (result && result.deviceInfo && result.deviceInfo.deviceType) ? result.deviceInfo.deviceType : "2"
                    userListCollection.SelectOne({ _id: userId }, (err, result) => {
                        if (result && result.userName) {
                            let request = {
                                fcmTopic: fcmTopicUserId,
                                // action: 1,
                                pushType: 2,
                                title: process.env.APP_NAME,
                                msg: result.userName + " just commented on your post on " + process.env.APP_NAME,
                                data: { "type": "postCommented", "postId": postId },
                                deviceType: deviceType
                            }
                            fcm.notifyFcmTpic(request, () => { })

                            // let message = { alert: 'PostCommented', msg: request.msg };
                            // sendPushUtility.iosPushIfRequiredOnMessage({
                            //     targetId: fcmTopicUserId, message: message, userId: userId.toString(),
                            //     "type": "postCommented", "postId": postId
                            // });
                        }
                    });
                    let condition = {
                        from: userId,
                        to: ObjectID(result.userId),
                        postId: ObjectID(result._id),
                        "type.status": 5
                    };
                    let dataToInsert = {
                        from: userId,
                        to: ObjectID(result.userId),
                        fromCollection: 'customer',
                        toCollection: 'posts',
                        postId: ObjectID(req.payload.postId),
                        timeStamp: moment().valueOf(),
                        start: new Date().getTime(),
                        end: false,
                        type: {
                            status: 5,
                            message: 'commented',
                            seen: false
                        }
                    };
                    notificationCollection.UpdateWithUpsert(condition, dataToInsert, (e) => { if (e) logger.error(e) });
                }
            });
            postCollection.UpdateWithIncrice({ _id: postId }, { commentCount: 1 }, (e) => { if (e) logger.error(e) });
        });
    };
    //mentioned in notification
    const mentionedInNotification = (commentData) => {
        return new Promise((resolve, reject) => {
            if (commentData[0].ops[0].mentionedUsers.length === 0) return resolve(commentData);
            else {
                let mentionedUsers = commentData[0].ops[0].mentionedUsers;
                let dataToInsert = {
                    from: userId,
                    fromCollection: 'customer',
                    toCollection: 'customer',
                    postId: ObjectID(req.payload.postId),
                    timeStamp: moment().valueOf(),
                    start: new Date().getTime(),
                    end: false,
                    type: {
                        status: 1,
                        message: 'mentioned',
                        seen: false
                    }
                };
                mentionedUsers.forEach((element) => {
                    dataToInsert.to = ObjectID(element._id);
                    notificationCollection.Insert(dataToInsert, (e) => {
                        if (e) {
                            return reject(e);
                        }
                    });
                });
                return resolve(commentData);
            }
        });
    };
    const addInPostNode = (commentData) => {
        return new Promise((resolve, reject) => {
            let condition = { _id: ObjectID(req.payload.postId) };
            userListCollection.SelectOne({ _id: userId }, (err, data) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else {

                    var comment = {
                        commentId: commentData[0].ops[0]._id,
                        comment: commentData[0].ops[0].comment,
                        commentedOn: commentData[0].ops[0].timeStamp,
                        commentedBy: data.userName,
                        commentedByUserId: userId,
                        commentedByProfilePic: data.profilePic,
                        ip: req.payload.ip,
                        city: req.payload.city,
                        country: req.payload.country
                    };

                    let dataToUpdate = {
                        comments: {
                            $each: [comment],
                            $position: 0
                        }
                    };

                    try {
                        // postES.UpdateWithIncrease(postId.toString(), "commentCount", 1, (e, r) => { if (e) { logger.error(e) } })
                        postES.Bulk([
                            { "update": { "_id": `${postId.toString()}`, "_type": "postList", "_index": "posts", "retry_on_conflict": 3 } },
                            { "script": { "source": `ctx._source.comments.add(${comment})`, "lang": "painless" } },
                            { "update": { "_id": `${postId.toString()}`, "_type": "postList", "_index": "posts", "retry_on_conflict": 3 } },
                            { "script": { "source": "ctx._source.commentCount+=1", "lang": "painless" } }
                        ], (e) => { if (e) logger.error(e) })

                    } catch (error) {
                        logger.error("error ===== : ", error)
                    }

                    postCollection.UpdateByIdWithPush(condition, dataToUpdate, (e) => {
                        if (e) {
                            return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                        } else {
                            hashTag();
                            return resolve(commentData);
                        }
                    });
                }
            });
        });
    };
    const hashTag = () => {
        return new Promise((resolve, reject) => {
            try {
                let hashTagArr = [];
                let hashTagString = '';
                if (req.payload.hashTags) {
                    let hashTags = req.payload.hashTags.trim().toLowerCase();

                    hashTagString = hashTags.replace(/,\s*$/, "");
                    hashTagArr = hashTagString.split(',');
                    hashTagArr.forEach((element) => {
                        try {
                            hashTagCollection.UpdateWithUpsert(
                                { _id: element },
                                { _id: element },
                                (e) => {
                                    if (e) throw e;
                                }
                            )
                        } catch (exception) {
                            console.log(exception);
                        }
                    });

                }
                resolve("---");


            } catch (error) {
                reject(error)
            }
        });
    };

    getUserData()
        .then(() => { return sendPushOnTagUsers(); })
        .then(() => { return getUserIds(); })
        .then((taggedUsers) => { return comment(taggedUsers); })
        .then((commentData) => { return mentionedInNotification(commentData); })
        .then((commentData) => { return addInPostNode(commentData); })
        .then((data) => {
            delete data[0].ops[0]["mentionedUsers"];
           
            return res({
                message: req.i18n.__('PostComment')['200'], data: {
                    ...data[0].ops[0],
                    commentedBy: userData.userName,
                    commentedByUserId: userData._id
                }
            }).code(200);
        })
        .catch((error) => { return res(error).code(error.code); });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['PostComment']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code


module.exports = { paramsValidator, payloadValidator, response, handler };