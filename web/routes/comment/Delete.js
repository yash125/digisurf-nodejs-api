'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const local = require('../../../locales');
const postES = require("../../../models/postES");
const postCollection = require("../../../models/posts");
const commentsCollection = require("../../../models/comments");

/**
 * @description API to get comments on a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} postId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const validator = Joi.object({
    // postId: Joi.string().min(24).max(24).required().description("mongodb id of post(eg.5a78059fdcf23c1284ea6581)"),
    commentId: Joi.array().required().description("mongodb id of comment(eg.5a78059fdcf23c1284ea6581)"),
}).required();

const handler = (req, res) => {
    console.log("req.payload : ", req.payload);
    const commentIds = req.payload.commentId.map(id => ObjectID(id));
    const postId = ObjectID(req.payload.postId);

    const setDeleteStatusInPostCollection = () => {
        return new Promise((resolve, reject) => {
            postCollection.UpdateByIdWithPull({ _id: postId.toString() }, {
                "comments": { commentId: { "$in": commentIds } }
            }, (err) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    return resolve({ code: 200, message: req.i18n.__('DeleteComment')['200'] });
                }
            });
        });
    };

    const setDeleteStatusInCommentCollection = () => {
        return new Promise((resolve, reject) => {


            let condition = { "_id": { "$in": commentIds } };
            let dataToUpdate = { "deleted": true, "deletedUserId": ObjectID(req.auth.credentials._id) };

            commentsCollection.Update(condition, dataToUpdate, (err) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    commentsCollection.Select({ postId: postId, deleted: { "$exists": false } }, (err, result) => {
                        if (err) {
                            return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                        }
                        result = (result && result.length) ? result.length : 0;
                        postCollection.Update({ _id: postId }, { "commentCount": result }, (err) => {
                            if (err) {
                                return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                            } else {
                                return resolve({ code: 200, message: req.i18n.__('DeleteComment')['200'] });
                            }
                        });
                    });
                }
            });
        });
    };
    const updateInElasticSearch = () => {
        return new Promise((resolve, reject) => {

            postCollection.SelectOne({ _id: postId }, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result && result._id) {
                    delete result._id;
                    postES.Update(postId.toString(), result, (e) => { if (e) { logger.error(e) } });
                
                }
                    return resolve({ code: 200, message: req.i18n.__('DeleteComment')['200'] });
                
            });
        });
    };

    setDeleteStatusInPostCollection()
        .then(() => { return setDeleteStatusInCommentCollection() })
        .then(() => { return updateInElasticSearch() })
        .then((data) => { return res({ message: data.message }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })

};

const response = {
    status: {
        200: { message: Joi.any().default(local['DeleteComment']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { validator, handler, response };