'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../locales');
const postCollection = require("../../../models/posts");

/**
 * @description API to get comments on a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} postId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const queryValidator = Joi.object({
    offset: Joi.string().description("offset"),
    postId: Joi.string().min(24).max(24).required().description("mongodb id of post(eg.5a78059fdcf23c1284ea6581)"),
    limit: Joi.string().description("limit")
}).unknown();


const handler = (req, res) => {
    const offset = parseInt(req.query.offset) || 0;
    const limit = parseInt(req.query.limit) || 20;
    const postId = ObjectID(req.query.postId);

    const get = () => {
        return new Promise((resolve, reject) => {

            let condition = [
                { "$match": { "_id": postId } },
                { "$lookup": { "from": "comments", "localField": '_id', "foreignField": "postId", "as": "commentData" } },
                {
                    "$project": {
                        "commentCount": { "$size": "$commentData" }, "commentData": 1, "imageUrl1": 1, "thumbnailUrl1": 1,
                        "mediaType": 1, "postedBy": "$userId", "postedOn": 1, "hashTags": 1
                    }
                },
                { "$unwind": "$commentData" },
                { "$match": { "commentData.deleted": { "$exists": 0 } } },
                {
                    "$project": {
                        "postId": "$_id",
                        "_id": 1, "imageUrl1": 1, "thumbnailUrl1": 1, "mediaType": 1, "postedBy": 1, "postedOn": 1, "hashTags": 1,
                        "commentId": "$commentData._id", "commentedByUserId": "$commentData.commentedBy", "commentedOn": "$commentData.commentedOn",
                        "timeStamp": "$commentData.timeStamp", "comment": "$commentData.comment", "mentionedUsers": "$commentData.mentionedUsers",
                        "hashtags": "$commentData.hashtags", "commentCount": 1
                    }
                },
                { "$sort": { "timeStamp": -1 } },
                { "$lookup": { "from": "customer", "localField": 'commentedByUserId', "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                {
                    "$project": {
                        "postId": 1,
                        "_id": 1, "imageUrl1": 1, "thumbnailUrl1": 1, "mediaType": 1, "postedBy": 1, "postedOn": 1, "hashTags": 1,
                        "commentId": 1, "commentedByUserId": 1, "commentedBy": "$userData.userName",
                        "profilePic": "$userData.profilePic", "commentedOn": 1, "timeStamp": 1, "comment": 1,
                        "hashtags": 1, "commentCount": 1, "starRequest": "$userData.starRequest"
                    }
                },
                { "$skip": offset },
                { "$limit": limit },
                {
                    "$lookup": {
                        "from": "comments_like", "localField": 'commentId', "foreignField": "commentId",
                        "as": "commentLikeList"
                    }
                },
            ];

            postCollection.Aggregate(condition, (err, data) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (!data) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {
                    data = data.map(e => {
                        
                        return {
                            ...e,
                            isStar: (e && e.starRequest && e.starRequest.starUserProfileStatusCode && e.starRequest.starUserProfileStatusCode == 4) ? true : false,
                            canDeleteComment: (e.postedBy.toString() == req.auth.credentials._id || req.auth.credentials._id == e.commentedByUserId.toString()) ? true : false,
                            likeCount: e.commentLikeList.length,
                            isLiked: (e.commentLikeList && e.commentLikeList.find(f => f.likedBy.toString() == req.auth.credentials._id) && e.commentLikeList.find(f => f.likedBy.toString() == req.auth.credentials._id).length) ? true : false
                        }
                    })
                    data.forEach(element => {
                        delete element["commentLikeList"]
                    });
                    return resolve({ code: 200, message: req.i18n.__('GetComment')['200'], data: data });
                }
            });
        });

    };

    get()
        .then((data) => {
            return res({ message: data.message, data: data.data }).code(data.code);
        })
        .catch((error) => { return res({ message: error.message }).code(error.code); })

};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetComment']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { queryValidator, handler, response };