const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const hideUsersPostCollection = require("../../../models/hideUsersPost");
const userCollection = require("../../../models/userList");


const validator = Joi.object({
    targetUserId: Joi.string().required().description("targetUserId, mongo id of the targetUserId"),
    hideMyPosts: Joi.bool().required().description("you can pass true or false")
})

const handler = (req, res) => {

    const userId = ObjectID(req.auth.credentials._id);
    const targetUserId = ObjectID(req.payload.targetUserId);
    const HTTP500ERROR = { message: req.i18n.__('genericErrMsg')['500'], code: 500 }
    const HTTP200ERROR = { message: req.i18n.__('PostHideMyPost')['200'], code: 200 }
    const HTTP405ERROR = { message: req.i18n.__('PostHideMyPost')['405'], code: 405 }
    const HTTP409ERROR = { message: req.i18n.__('genericErrMsg')['409'], code: 409 }
    const HTTP412ERROR = { message: req.i18n.__('PostHideMyPost')['412'], code: 412 }

    if (userId.toString() == targetUserId.toString()) return res({ message: HTTP405ERROR.message }).code(HTTP405ERROR.code);

    const updateInUserListCollection = () => {
        return new Promise((resolve, reject) => {

            switch (req.payload.hideMyPosts) {
                case true: {
                    userCollection.UpdateByIdWithAddToSet({ _id: userId.toString() }, { "hidemypostTo": targetUserId }, (err, result) => {
                        if (err) {
                            logger.error("OASASAKDSOOA : ", err);
                            return reject(HTTP500ERROR);
                        }
                        result = JSON.parse(result);
                        if (!result.nModified) { return reject(HTTP409ERROR); }

                    });
                    userCollection.UpdateByIdWithAddToSet({ _id: targetUserId.toString() }, { "hidePostBy": userId }, (err, result) => {
                        if (err) {
                            logger.error("OASASDSAAOOA : ", err);
                            return reject(HTTP500ERROR);
                        }
                        result = JSON.parse(result);
                        if (!result.nModified) {return reject(HTTP409ERROR); }

                        return resolve(true);
                    });
                    break;
                }
                case false: {
                    userCollection.UpdateByIdWithPull({ _id: userId.toString() }, { "hidemypostTo": targetUserId }, (err, result) => {
                        if (err) {
                            logger.error("OASASAKDSOOAASAS : ", err);
                            return reject(HTTP500ERROR);
                        }
                        result = JSON.parse(result);
                        if (!result.nModified) {
                            return reject(HTTP412ERROR);
                        }
                    });
                    userCollection.UpdateByIdWithPull({ _id: targetUserId.toString() }, { "hidePostBy": userId }, (err, result) => {
                        if (err) {
                            logger.error("ODASDSOOA : ", err);
                            return reject(HTTP500ERROR);
                        }
                        result = JSON.parse(result)
                        if (!result.nModified) {
                            return reject(HTTP412ERROR);
                        }

                        return resolve(true)
                    });
                    break;
                }
            }
        });
    };

    const updateInHideUserPostCollection = () => {
        return new Promise((resolve, reject) => {

            switch (req.payload.hideMyPosts) {
                case true: {
                    let dataToInsert = {
                        userId: userId,
                        targetUserId: targetUserId,
                        timestamp: new Date().getTime(),
                        timestampForMatabase: new Date()
                    };
                    hideUsersPostCollection.Insert(dataToInsert, (err) => {
                        if (err) {
                            logger.error("OASASAKDSOOAAAS : ", err);
                            return reject(HTTP500ERROR);
                        }
                    });
                    break;
                }
                case false: {
                    let condition = { userId: userId, targetUserId: targetUserId, isDeleted: { "$exists": false } };
                    let dataToUpdate = { isDeleted: true, deletedTimestamp: new Date().getTime() };
                    hideUsersPostCollection.Update(condition, dataToUpdate, (err) => {
                        if (err) {
                            logger.error("OASASAKDSAAAS : ", err);
                            return reject(HTTP500ERROR);
                        }
                    });
                    break;
                }
            }
            return resolve(HTTP200ERROR);
        });
    };

    updateInUserListCollection()
        .then(() => { return updateInHideUserPostCollection(); })
        .then((data) => { return res({ message: data.message }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })


};

const response = {
    status: {
        200: { message: Joi.any().default(local['PostFriend']['200']) },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        409: { message: Joi.any().default(local['genericErrMsg']['409']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { validator, handler, response };