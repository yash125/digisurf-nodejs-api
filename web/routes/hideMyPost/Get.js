const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userCollection = require("../../../models/userList");


const handler = (req, res) => {

    const userId = ObjectID(req.auth.credentials._id);
    const HTTP500ERROR = { message: req.i18n.__('genericErrMsg')['500'], code: 500 }
    const HTTP204ERROR = { message: req.i18n.__('genericErrMsg')['204'], code: 204 }

    const getUserList = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "_id": userId } },
                { "$unwind": "$hidemypostTo" },
                { "$lookup": { "from": "customer", "localField": "hidemypostTo", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                {
                    "$project": {
                        "_id": "$userData._id", "countryCode": "$userData.countryCode",
                        "number": "$userData.number", "userName": "$userData.userName",
                        "profilePic": "$userData.profilePic", "firstName": "$userData.firstName",
                        "lastName": "$userData.lastName", "status": "$userData.status"
                    }
                }
            ];
            userCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    logger.error("OKOKSAAAADSD : ", err);
                    return reject(HTTP500ERROR);
                } else if (result && result.length) {
                    return resolve({ message: req.i18n.__('GetHideMyPost')['200'], data: result })
                } else {
                    return reject(HTTP204ERROR)
                }
            });
        });
    };

    getUserList()
        .then((data) => { return res({ message: req.i18n.__('GetHideMyPost')['200'], data: data.data }).code(200); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetHideMyPost']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { handler, response };