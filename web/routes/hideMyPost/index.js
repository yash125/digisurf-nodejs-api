let headerValidator = require("../../middleware/validator")
let PostAPI = require('./Post');
let GetAPI = require('./Get');
let GetByAPI = require('./GetBy');

module.exports = [
    {
        method: 'Post',
        path: '/hideMyPost',
        handler: PostAPI.handler,
        config: {
            description: `This API use for set hide/visable my post to targetUser`,
            tags: ['api', 'hideMyPost'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
    {
        method: 'GET',
        path: '/hideMyPost',
        handler: GetAPI.handler,
        config: {
            description: `This API use for get userlist hide post by me`,
            tags: ['api', 'hideMyPost'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
    {
        method: 'GET',
        path: '/hidePostBy',
        handler: GetByAPI.handler,
        config: {
            description: `This API use for get userlist hide post by me`,
            tags: ['api', 'hideMyPost'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByAPI.response
        }
    }
];