let Get = require('./Get');
let headerValidator = require("../../middleware/validator")


module.exports = [
    {
        method: 'GET',
        path: '/isRegisteredEmail',
        handler: Get.handler,
        config: {
            description: `This API use to check emailId is available on db or not`,
            tags: ['api', 'isRegisteredEmail'],
            auth: "basic",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: Get.params,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: PostAPI.response
        }
    }
]