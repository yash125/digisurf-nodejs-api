let Get = require('./Get');
let headerValidator = require("../../middleware/validator")


module.exports = [
    {
        method: 'GET',
        path: '/isRegisteredNumber',
        handler: Get.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'isRegisteredNumber'],
            auth: {
                strategies: ['basic', 'guest']
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: Get.params,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: PostAPI.response
        }
    }
]