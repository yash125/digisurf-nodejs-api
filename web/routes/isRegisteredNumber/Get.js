'use strict'
const Joi = require("joi");

const local = require('../../../locales');
const userCollection = require("../../../models/userList");

const params = Joi.object({
    "countryCode": Joi.string().required().description('countryCode').error(new Error("countryCode is missing")),
    "phoneNumber": Joi.string().required().description('phoneNumber').error(new Error("phoneNumber is missing"))
})

const handler = (req, res) => {
    var number = req.query.phoneNumber;
    userCollection.SelectOne({ mobile: number, countryCode: req.query.countryCode }, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result && result._id) {
            return res({ message: req.i18n.__('genericErrMsg')['200'] }).code(200);
        } else {
            return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);
        }
    });
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetMyLikesPosts']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code

module.exports = { handler, response, params };