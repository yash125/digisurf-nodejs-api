'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const followCollection = require("../../../../models/follow");

const queryValidator = Joi.object({
    userId: Joi.string().description('userId'),
    skip: Joi.string().description('skip'),
    limit: Joi.string().description('limit'),
}).unknown();

/**
 * @method GET follow/all
 * @description This API returns user's followers and followee
 *
 * @headers {string} authorization : token
 * @headers {string} lang by defualt en
 *
 * @param {string} userId by default null
 * @param {string} offset by default 0
 * @param {string} limit  by default 20
 *
 * @author DipenAhir
 * @since 14-july-2020
 */

const handler = (req, res) => {
    const offset = parseInt(req.query.skip) || 0;
    const limit = parseInt(req.query.limit) || 20;
    var userId = (req.query.userId) ? ObjectID(req.query.userId) : ObjectID(req.auth.credentials._id);

    const followees = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$sort": { "_id": -1 } },
                {
                    "$match": {
                        "$or": [{ "followee": userId },
                        { "follower": userId }
                        ],
                        "type.status": 1
                    }
                }, {
                    "$project": {
                        "oppnentId": {
                            "$cond": {
                                "if": { "$eq": ["$followee", userId] },
                                "then": "$follower", "else": "$followee"
                            }
                        },
                        "follower": {
                            "$cond": {
                                "if": { "$eq": ["$followee", userId] },
                                "then": true, "else": false
                            }
                        },
                    }
                },
                { "$lookup": { "localField": "oppnentId", "from": "customer", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                { "$match": { "userData.userStatus": 1 } },
                {
                    "$project": {
                        "_id": 0,
                        "userId": "$userData.groupCallStreamId",
                        "userName": { "$concat": ["$userData.firstName", " ", "$userData.lastName"] },
                        "userIdentifier": "$userData.userName",
                        "userProfilePic": "$userData.profilePic",
                        "follower": 1
                    }
                },
                {
                    "$group": {
                        "_id": "$userId",
                        "userName": { "$first": "$userName" },
                        "userIdentifier": { "$first": "$userIdentifier" },
                        "userProfilePic": { "$first": "$userProfilePic" },
                        "follower": { "$first": "$follower" },
                        "followerList": { "$push": "$follower" },
                        "count": { "$sum": 1 }
                    }
                },
                {
                    "$project": {
                        "_id": 0,
                        "userId": "$_id",
                        "userName": 1,
                        "userIdentifier": 1,
                        "userProfilePic": 1,
                        "follower": 1,
                        "followerList": 1,
                        "count": 1
                    }
                },
                { "$match": { "userId": { "$exists": true }, "userId": { "$ne": null } } },
                { "$skip": offset }, { "$limit": limit }
            ];


            followCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else if (result.length === 0) {
                    return reject({ code: 204, message: local['genericErrMsg']['204'] });
                } else {
                    result = result.map(e => {
                        return {
                            ...e,
                            followStatus: (e.count > 1) ? 3 : (e.follower) ? 2 : 1
                        }
                    })
                    return resolve({ code: 200, message: local['GetFolloees']['200'], data: result });
                }
            });
        });
    };




    followees()
        .then((data) => {
            return res({
                message: data.message, data: data.data, "notes": [
                    "1) userId means callingStreamUserId , not a user's id",
                    `2) user A = calling api Get - follo/all

                    User A follow user B : followStatus : 1
                    User B follow user A : followStatus : 2
                    user A and B follow each other  : followStatus : 3`
                ]
            }).code(data.code);
        })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetFolloees']['200']),
            notes: Joi.any().example([
                "1) userId means callingStreamUserId , not a user's id"
            ]),
            data: Joi.any().example([

                {
                    "follower": false,
                    "callingStreamUserId": "5efb30ce93edd7000155abe3",
                    "userName": "Addidas Shoes Store",
                    "userIdentifier": "Addidas Shoes Store",
                    "userProfilePic": "https://s3.ap-south-1.amazonaws.com/dubly/profile_image/5ecf86cd527f7173bfb13a77"
                }
                ,
                {
                    "follower": false,
                    "callingStreamUserId": "5efb30c093edd70001ef9e83",
                    "userName": "Manish",
                    "userIdentifier": "mkmanishrocks",
                    "userProfilePic": "https://s3.ap-south-1.amazonaws.com/dubly/profile_image/5ecbaa8177cf6509afa941c0"
                }


            ])
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { queryValidator, handler, response };



/**
 *
user A = calling api Get - follow/all

 User A follow user B : followStatus : 1
 User B follow user A : followStatus : 2
 user A and B follow each other  : followStatus : 3


 Also in search API
 */
