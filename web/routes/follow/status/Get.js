const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const followCollection = require("../../../../models/follow");
const userList = require("../../../../models/userList");

const validator = Joi.object({
    userId: Joi.string().required().description("followingId, mongo id of the followee")
})

const handler = (req, res) => {


    const memberId = ObjectID(req.query.userId);
    const userId = ObjectID(req.auth.credentials._id);

    const getUserData = () => {
        return new Promise((resolve, reject) => {

            userList.SelectOne({ _id: memberId }, (err, result) => {
                if (err) {
                    return reject({ message: local['genericErrMsg']['500'], code: 500 });
                } else if (result && result._id) {
                    return resolve({ isPrivate: result.private });
                } else {
                    return reject({ message: local['genericErrMsg']['204'], code: 204 });
                }
            });
        });
    };

    const getMemberData = (data) => {
        return new Promise((resolve, reject) => {
            let condition = { follower: userId, followee: memberId, "type.status": { $ne: 3 } };

            followCollection.SelectOne(condition, (e, d) => {
                if (e) {
                    return reject({ message: local['genericErrMsg']['500'], code: 500 });
                    // return res({ message: local['genericErrMsg']['500'] }).code(500);
                } else if (d && d._id && d.type && d.type.status) {
                    let dataToSend = {
                        code: 200,
                        isPrivate: (data && data.isPrivate && data.isPrivate == 1) ? true : false,
                        following: (d && d.type && d.type.status == 1 || d.type.status == 2) ? true : false,
                        message: local['genericErrMsg']['200'], status: d.type.status,
                        description: [
                            { status: 0, message: "unfollowed" },
                            { status: 1, message: "started following" },
                            { status: 2, message: "requested" }
                        ]
                    }
                    return resolve(dataToSend)
                } else {
                    let dataToSend = {
                        code: 200,
                        isPrivate: (data && data.isPrivate && data.isPrivate == 1) ? true : false,
                        following: false,
                        message: local['genericErrMsg']['200'], status: 0,
                        description: [
                            { status: 0, message: "unfollowed" },
                            { status: 1, message: "started following" },
                            { status: 2, message: "requested" }
                        ]
                    }
                    return resolve(dataToSend)
                }
            });

        });
    };

    getUserData()
        .then(getMemberData)
        .then((data) => {
            if (data.code == 200) {
                return res({
                    message: data.message, status: data.status, description: data.description,
                    isPrivate: data.isPrivate, following: data.following
                }).code(200);
            } else {
                return res({ message: local['genericErrMsg']['204'] }).code(204);
            }
        })
        .catch(err => {
            console.log("error ", err);
            return res({ message: local['genericErrMsg']['500'] }).code(500);
        })



};

const response = {
    status: {
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        200: {
            message: Joi.any().default(local['genericErrMsg']['200']), description: Joi.any(),
            status: Joi.any(), isPrivate: Joi.boolean(), following: Joi.any()
        },
        422: { message: Joi.any().default(local['PostFollow']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { validator, handler, response };