let headerValidator = require("../../../middleware/validator")
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'Get',
        path: '/follow/status',
        handler: GetAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'follow'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: PostAPI.response
        }
    }
];