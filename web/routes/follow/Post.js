const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const fcm = require("../../../library/fcm");
// const rabbitMq = require('../../../library/rabbitMq');
const followCollection = require("../../../models/follow");
const userCollection = require("../../../models/userList");
const notificationCollection = require("../../../models/notification");
// var sendPushUtility = require("../../../library/iosPush/sendPush.js");

const validator = Joi.object({
    followingId: Joi.string().required().description("followingId, mongo id of the followee")
})

const handler1 = (req, res) => {

    const memberId = ObjectID(req.payload.followingId);
    const userId = ObjectID(req.auth.credentials._id);
    let result = {};
    let isAllFollow = false, isPrivate = 0;
    let condition = { follower: userId, followee: memberId, end: false };
    let dataToSend = { followStatus: 0, isPrivate: 0 }

    followCollection.SelectOne(condition, (e, d) => {
        if (e) {
            return res({ message: local['genericErrMsg']['500'] }).code(500);
        } else if (d && d._id) {
            return res({ message: local['PostFollow']['422'] }).code(422);
        } else {
            getUserData()
                .then(() => { return checkMemberProfile(); })
                .then((memberProfile) => { return follow(memberProfile); })
                .then(() => { return manageFollowArray(); })
                .then(() => { return checkIsAllFollow(); })
                .then(() => {
                    if (isPrivate) {
                        return res({ message: local['PostFollow']['200'], userId: memberId, isPrivate: dataToSend["isPrivate"], followStatus: dataToSend["followStatus"], isAllFollow: isAllFollow }).code(200);
                    } else {
                        return res({ message: local['PostFollow']['200'], userId: memberId, isPrivate: dataToSend["isPrivate"], followStatus: dataToSend["followStatus"], isAllFollow: isAllFollow }).code(201);
                    }
                })
                .catch((error) => {
                    console.log(error)
                    return res({ message: local['genericErrMsg']['500'] }).code(500);
                })
        }
    });

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }


    //Step2: Check if the followee / member profile is public or private
    const checkMemberProfile = () => {
        return new Promise((resolve, reject) => {

            let condition = {
                _id: memberId,
                private: { $exists: true }
            };
            userCollection.SelectOne(condition, (e, d) => {
                if (e) {
                    reject({ message: local['genericErrMsg']['500'] });
                } else {
                    resolve(d);
                }
            });
        });
    };
    /**
     * follower(from) is the user who will follow another user (followee / to)
     * main follow function 
     * if the meember i-e the followee profile is private, set follow status to 0 which means requested
     * else set follow stattus to 1 which means started following
     * @param {*} memberProfile 
     */
    const follow = (memberProfile) => {
        followCollection.Delete({
            follower: userId,
            followee: memberId
        }, (e, r) => {
            if (e) console.log("error ", e)
        })
        result.memberProfile = memberProfile;
        var end = false;
        var message = " started following you on " + process.env.APP_NAME;
        var type = {};

        if (memberProfile && memberProfile.private) {
            type.status = 2;
            type.message = ' requested to follow you'
            message = " wants to follow you on " + process.env.APP_NAME + ".";
            end = true;
            isPrivate = 1;
        } else {


            type.status = 1;
            type.message = ' started following you'
            userCollection.UpdateByIdWithAddToSet({ _id: userId.toString() }, { "follow": memberId }, (e) => { if (e) logger.error("ASWEC : ", e); });
            // userCollection.UpdateByIdWithAddToSet({ _id: memberId.toString() }, { "follow": userId }, (e, r) => { if (e) logger.error("ASWEC12 : ", e); });
            let dataToInsert = {
                to: memberId,
                from: userId,
                toCollection: 'customer',
                fromCollection: 'customer',
                start: new Date().getTime(),
                end: false,
                type: { "status": 3, "message": 'started following ' }
            };
            let condition = {
                to: memberId,
                from: userId,
                end: true,
                "$or": [{ "type.status": 3 }, { "type.status": 4 }, { "type.status": 6 }]
            };

            notificationCollection.UpdateWithUpsert(condition, dataToInsert, (e) => {
                if (e) {
                    let responseObj = { message: local['genericErrMsg']['500'] };
                    return responseObj;
                }
            });
            followCollection.GetFollowerCount(userId, (err, result) => {
                if (err) {
                    return { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                } else {
                    let followerCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                    userCollection.UpdateById(userId.toString(), { "count.followerCount": followerCount }, () => { })
                }
            });
            followCollection.GetFolloweeCount(memberId, (err, result) => {
                if (err) {
                    return { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                } else {
                    let followerCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                    userCollection.UpdateById(memberId.toString(), { "count.followerCount": followerCount }, () => { })
                }
            })
            followCollection.GetFolloweeCount(userId, (err, result) => {
                if (err) {
                    return { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                } else {
                    let followeeCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                    userCollection.UpdateById(userId.toString(), { "count.followeeCount": followeeCount }, () => { })
                }
            })
            followCollection.GetFolloweeCount(memberId, (err, result) => {
                if (err) {
                    return { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                } else {
                    let followeeCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                    userCollection.UpdateById(memberId.toString(), { "count.followeeCount": followeeCount }, () => { })
                }
            })
        }
        dataToSend["followStatus"] = type.status;
        dataToSend["isPrivate"] = (memberProfile && memberProfile.private) ? memberProfile.private : 0;

        let dataToInsert = {
            follower: userId,
            followee: memberId,
            start: new Date().getTime(),
            end: end,
            type: type
        };

        let condition = {
            follower: userId,
            followee: memberId
        };

        userCollection.SelectOne({ _id: userId }, (err, result) => {
            if (result && result._id) {
                let data = (dataToSend["isPrivate"]) ? { "type": "followRequest", "userId": userId } : { "type": "followed", "userId": userId };
                let request = {
                    fcmTopic: memberId.toString(),
                    // action: 1,
                    pushType: 2,
                    title: process.env.APP_NAME,
                    msg: result.userName + message,
                    data: data,
                    deviceType: (userData && userData.deviceInfo && userData.deviceInfo.deviceType) ? userData.deviceInfo.deviceType : "2"
                };

                fcm.notifyFcmTpic(request, (e) => { if (e) logger.error("SODKOSKDOS : ", e) })


                // let messageData = { alert: request.title, msg: request.msg };
                // if (dataToSend["isPrivate"]) {
                //     sendPushUtility.iosPushIfRequiredOnMessage({
                //         targetId: request.fcmTopic, message: messageData,
                //         "type": "followRequest", "userId": userId
                //     });
                // } else {
                //     sendPushUtility.iosPushIfRequiredOnMessage({
                //         targetId: request.fcmTopic, message: messageData,
                //         "type": "followed", "userId": userId
                //     });
                // }

            }
        });

        return new Promise((resolve, reject) => {

            followCollection.UpdateWithUpsert(condition, dataToInsert, (e, d) => {
                if (e) {
                    console.log("stap 4", e)
                    return reject(e);
                } else {
                    d = JSON.parse(d);
                    if (d.nModified == 0 && !dataToSend["isPrivate"]) {
                        // rabbitMq.sendToQueue(rabbitMq.follow_mqtt_queue, { "userId": req.auth.credentials._id.toString(), "userName": userData.userName || "user", "userLang": req.headers.lang, "starUserId": memberId.toString() }, (e) => {
                        //     if (e) {
                        //         logger.error("OSKDCO : ", e);
                        //     }
                        // });
                    }
                    return resolve(d);
                }
            });
        });
    };
    const checkIsAllFollow = () => {
        return new Promise((resolve, reject) => {
            let followIds = [], totalContacts = 0, totalFollow = 0;
            let condition = [
                { "$match": { "_id": ObjectID(userId) } },
                { "$unwind": "$contacts" },
                { "$project": { "contacts": 1 } },
                { "$lookup": { "from": "customer", "localField": "contacts", "foreignField": "number", "as": "Data" } },
                { "$unwind": "$Data" },
                {
                    "$group": {
                        "_id": "$_id",
                        "data": { "$addToSet": "$Data._id" }
                    }
                }];
            userCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    return reject(err);
                }

                if (result && result[0] && result[0]["data"]) {
                    totalContacts = result[0]["data"].length;
                    for (let index = 0; index < totalContacts; index++) {
                        followIds.push(ObjectID(result[0]["data"][index]));
                    }
                }

                condition = {
                    "followee": { "$in": followIds },
                    "follower": ObjectID(userId),
                    "type.status": { $in: [1, 2] }
                };
                followCollection.Select(condition, (err, result) => {
                    if (result) {
                        totalFollow = result.length;
                    }
                    if (totalFollow == totalContacts) {
                        isAllFollow = true;
                    }
                    return resolve("---");
                });
            });
        })
    }
    const manageFollowArray = () => {
        return new Promise((resolve, reject) => {

            let condition = { follower: userId, end: false, "type.status": 1, "followee": { "$ne": userId } };
            followCollection.Select(condition, (e, d) => {
                if (e) {
                    return reject({ message: local['genericErrMsg']['500'] });
                } else {
                    let myFollow = [];
                    d.forEach(element => {
                        myFollow.push(ObjectID(element.followee.toString()));
                    });
                    userCollection.Update({ _id: userId }, { follow: myFollow, "count.followeeCount": myFollow.length }, () => { });
                    followCollection.GetFolloweeCount(userId, (err, result) => {
                        if (err) {
                            return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                        } else {
                            let followeeCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                            userCollection.UpdateById(userId.toString(), { "count.followeeCount": followeeCount }, () => { })
                        }
                    });

                    return resolve(true);
                }
            });
        });
    };

};

const response = {
    status: {
        201: { message: Joi.any().default(local['PostFollow']['200']), userId: Joi.any(), data: Joi.any(), followStatus: Joi.any(), isPrivate: Joi.any(), isAllFollow: Joi.any() },
        200: { message: Joi.any().default(local['PostFollow']['200']), userId: Joi.any(), data: Joi.any(), followStatus: Joi.any(), isPrivate: Joi.any(), isAllFollow: Joi.any() },
        422: { message: Joi.any().default(local['PostFollow']['422']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { validator, handler1, response };