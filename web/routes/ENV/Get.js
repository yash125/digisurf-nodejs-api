'use strict'
const path = require('path');

const hander = (req, res) => {

    require('dotenv').config({ path: path.resolve(".env") });
    let dataToSend = {
        RUC_ON_SIGNUP: process.env.RUC_ON_SIGNUP,
        RUC_LIKER: process.env.RUC_LIKER,
        RUC_LIKE_STARUSER: process.env.RUC_LIKE_STARUSER,
        RUC_COMMENT_USER: process.env.RUC_COMMENT_USER,
        RUC_COMMENT_STARUSER: process.env.RUC_COMMENT_STARUSER,
        RUC_VIEW_POST_USER: process.env.RUC_VIEW_POST_USER,
        RUC_VIEW_POST_STARUSER: process.env.RUC_VIEW_POST_STARUSER,
        RUC_FOLLOW_USER: process.env.RUC_FOLLOW_USER,
        RUC_FOLLOW_STARUSER: process.env.RUC_FOLLOW_STARUSER,
        RUC_SIGNUP_USER: process.env.RUC_SIGNUP_USER,
        RUC_REFERRAL_USER: process.env.RUC_REFERRAL_USER,
        RUC_REFERRAL_NORMAL_USER: process.env.RUC_REFERRAL_NORMAL_USER,
        RUC_REFERRAL_STARUSER: process.env.RUC_REFERRAL_STARUSER,
        RUC_TRANSFER_EXPIRY_IN_SECONDS: process.env.RUC_TRANSFER_EXPIRY_IN_SECONDS
    }
    return res({ message: "sucess", data: dataToSend }).code(200);
};

const response = {
    status: {
        // 200: { message: Joi.any().default(local['GetActivity']['200']), data: Joi.any() },
        // 204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        // 400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code


module.exports = {  hander, response };