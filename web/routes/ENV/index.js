let headerValidator = require("../../middleware/validator")
let PutAPI = require('./Put');
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'PUT',
        path: '/ENV',
        handler: PutAPI.hander,
        config: {
            description: `This API use for update .env files and also update process.env values`,
            tags: ['api', 'ENV'],
            auth: 'basic',
            validate: {
                // headers: headerValidator.headerAuthValidator,
                payload: PutAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response
        }
    },
    {
        method: 'Get',
        path: '/ENV',
        handler: GetAPI.hander,
        config: {
            description: `This API use for update .env files and also update process.env values`,
            tags: ['api', 'ENV'],
            auth: 'basic',
            validate: {
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            }
        }
    }
];