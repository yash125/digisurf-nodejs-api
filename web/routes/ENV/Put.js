'use strict'
const Joi = require("joi");
const logger = require('winston');
const mqtt = require("../../../library/mqtt")
const path = require('path');

const validator = Joi.object({
    RUC_ON_SIGNUP: Joi.string().description(`e.g. : RUC_ON_SIGNUP : 100 `),
    RUC_LIKER: Joi.string().description(`e.g. : RUC_LIKER : 100 `),
    RUC_LIKE_STARUSER: Joi.string().description(`e.g. : RUC_LIKER : 100 `),
    RUC_COMMENT_USER: Joi.string().description(`e.g. : RUC_LIKER : 100 `),
    RUC_COMMENT_STARUSER: Joi.string().description(`e.g. : RUC_LIKER : 100 `),
    RUC_VIEW_POST_USER: Joi.string().description(`e.g. : RUC_LIKER : 100 `),
    RUC_VIEW_POST_STARUSER: Joi.string().description(`e.g. : RUC_LIKER : 100 `),
    RUC_FOLLOW_USER: Joi.string().description(`e.g. : RUC_LIKER : 100 `),
    RUC_FOLLOW_STARUSER: Joi.string().description(`e.g. : RUC_LIKER : 100 `),
    RUC_REFERRAL_NORMAL_USER: Joi.string().description(`e.g. : RUC_REFERRAL_NORMAL_USER : 100 `),
    RUC_REFERRAL_USER: Joi.string().description(`e.g. : RUC_LIKER : 100 `),
    RUC_REFERRAL_STARUSER: Joi.string().description(`e.g. : RUC_LIKER : 100 `),
    RUC_TRANSFER_EXPIRY_IN_SECONDS: Joi.string().description(`e.g. : RUC_TRANSFER_EXPIRY_IN_SECONDS : 100 `)
}).unknown();

const hander = (req, res) => {
    var myObject = {};
    require('dotenv').config({ path: path.resolve(".env") });

    if (req.payload.RUC_ON_SIGNUP) myObject["RUC_ON_SIGNUP"] = req.payload.RUC_ON_SIGNUP;

    if (req.payload.RUC_LIKER) myObject["RUC_LIKER"] = req.payload.RUC_LIKER

    if (req.payload.RUC_LIKE_STARUSER) myObject["RUC_LIKE_STARUSER"] = req.payload.RUC_LIKE_STARUSER

    if (req.payload.RUC_COMMENT_USER) myObject["RUC_COMMENT_USER"] = req.payload.RUC_COMMENT_USER

    if (req.payload.RUC_COMMENT_STARUSER) myObject["RUC_COMMENT_STARUSER"] = req.payload.RUC_COMMENT_STARUSER

    if (req.payload.RUC_VIEW_POST_USER) myObject["RUC_VIEW_POST_USER"] = req.payload.RUC_VIEW_POST_USER

    if (req.payload.RUC_VIEW_POST_STARUSER) myObject["RUC_VIEW_POST_STARUSER"] = req.payload.RUC_VIEW_POST_STARUSER

    if (req.payload.RUC_FOLLOW_USER) myObject["RUC_FOLLOW_USER"] = req.payload.RUC_FOLLOW_USER

    if (req.payload.RUC_FOLLOW_STARUSER) myObject["RUC_FOLLOW_STARUSER"] = req.payload.RUC_FOLLOW_STARUSER

    // if (req.payload.RUC_REFERRAL_NORMAL_USER) myObject["RUC_REFERRAL_NORMAL_USER"] = req.payload.RUC_REFERRAL_NORMAL_USER

    if (req.payload.RUC_REFERRAL_USER) myObject["RUC_REFERRAL_USER"] = req.payload.RUC_REFERRAL_USER

    if (req.payload.RUC_REFERRAL_NORMAL_USER) myObject["RUC_REFERRAL_NORMAL_USER"] = req.payload.RUC_REFERRAL_NORMAL_USER

    if (req.payload.RUC_REFERRAL_STARUSER) myObject["RUC_REFERRAL_STARUSER"] = req.payload.RUC_REFERRAL_STARUSER

    if (req.payload.RUC_TRANSFER_EXPIRY_IN_SECONDS) myObject["RUC_TRANSFER_EXPIRY_IN_SECONDS"] = req.payload.RUC_TRANSFER_EXPIRY_IN_SECONDS



    mqtt.publish("ENV", JSON.stringify(myObject), { qos: 0 }, (e) => {
        if (e) logger.error(e)
    })
    return res({ message: "sucess" }).code(200);
};

const response = {
    status: {
        // 200: { message: Joi.any().default(local['GetActivity']['200']), data: Joi.any() },
        // 204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        // 400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code


module.exports = { validator, hander, response };