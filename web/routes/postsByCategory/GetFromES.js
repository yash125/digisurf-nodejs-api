const Joi = require("joi");

const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const postES = require("../../../models/postES");
const userListCollection = require("../../../models/userList");
const businessProductTypeCollection = require("../../../models/businessProductType");

/**
 * @description API to update a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} userId
 * @property {string} imageUrl1
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const validator = Joi.object({
    categoryId: Joi.string().required().description('categoryId'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();

const handler = (req, res) => {

    let categoryId = req.query.categoryId;
    let limit = parseInt(req.query.limit) || 20;
    let skip = parseInt(req.query.offset) || 0;
    let userId = req.auth.credentials._id;

    var userData = {}, businessPostTypeLabel = {};

    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    function getBusinessPostTypeLabel() {
        return new Promise((resolve, reject) => {
            businessProductTypeCollection.Select({}, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    result.forEach(element => {
                        businessPostTypeLabel[element._id.toString()] = element.text[req.headers.lang]
                    });
                }
                return resolve(true);
            });
        });
    }

    const getPosts = () => {
        return new Promise((resolve, reject) => {
            let condition = {
                "must": [
                    { "match": { "postStatus": 1 } },
                    { "match": { categoryId: categoryId } }
                ]
            }
            postES.getPostDetails(condition, skip, limit, userData.follow, userId, (err, result) => {
                if (err) {
                    console.log("error ", err)
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(element => {
                        element["isBookMarked"] = (userData && userData.bookmarks && userData.bookmarks.map(e => e.toString()).includes(element.postId)) ? true : false;
                        if (element["business"] && element["business"]["businessPostType"]) element["business"]["businessPostTypeLabel"] = businessPostTypeLabel[element["business"]["businessPostType"]] || "";
                        if (element["channelId"] && element["channelId"].length) element["isChannelSubscribed"] = (userData && userData.subscribeChannels && userData.subscribeChannels.map(e => e.toString()).includes(element.channelId.toString())) ? 1 : 0;
                    });
                    let responseobj = { code: 200, message: req.i18n.__('GetHome')['200'], data: result };
                    return resolve(responseobj);
                }
            })
        });
    };

    getUserData()
        .then(() => { return getBusinessPostTypeLabel() })
        .then(() => { return getPosts() })
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); });
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetPostsByCategory']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code


module.exports = { validator, handler, response };