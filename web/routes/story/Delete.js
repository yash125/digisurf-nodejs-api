'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const storyCollection = require('../../../models/story');
const storyPostCollection = require('../../../models/storyPost');

const validator = Joi.object({
    storyId: Joi.string().required().min(24).max(24).description("storyId eg. 5ad49a317a7e5c33b4554ba7").error(new Error('storyId is missing or incoreect it must be 24 digit')),
}).required();

const handler = (req, res) => {
    let storyId = ObjectID(req.query.storyId);

    const removeDocAtStoryPostCollection = () => {
        return new Promise((resolve, reject) => {
            let condition = { _id: storyId };
            storyPostCollection.Delete(condition, (err) => {
                if (err) return reject(err);

                return resolve("--");
            });
        });
    };

    const changeStatusAtStoryCollection = () => {
        return new Promise((resolve, reject) => {
            let condition = { postId: storyId };
            let data = { status: "inActive" };
            // storyCollection.Update(condition, data, (err) => {
            //     if (err) return reject(err);
            //     return resolve(true);
            // });
            storyCollection.Delete(condition, (err) => {
                if (err) return reject(err);
                return resolve(true);
            });
            storyCollection.Delete({ _id: storyId }, (err) => {
                if (err) return reject(err);
                return resolve(true);
            });

        });
    };

    removeDocAtStoryPostCollection()
        .then(() => { return changeStatusAtStoryCollection(); })
        .then(() => { return res({ message: req.i18n.__('DeleteStory')['200'] }).code(200); })
        .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500); })

};

const response = {
    status: {
        200: { message: Joi.any().default(local['DeleteStory']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code


module.exports = { handler, response, validator };