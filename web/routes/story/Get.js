const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const followCollection = require("../../../models/follow");
const local = require('../../../locales');
const storyPostCollection = require('../../../models/storyPost');

const handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    let followeeIds = [];

    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            followCollection.Select({ "follower": userId, "end": false }, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(element => {
                        followeeIds.push(ObjectID(element.followee))
                    });
                    return resolve(true);
                }
            });
        });
    }

    const getStoryAtStoryPostCollection = () => {
        return new Promise((resolve, reject) => {

            let condition = [
                { "$match": { "userId": { "$in": followeeIds } } },
                { "$match": { "userId": { "$ne": userId } } },
                { "$lookup": { "from": "customer", "localField": "userId", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                { "$match": { "userData.userStatus": 1 } },
                { "$sort": { "_id": -1 } },
                {
                    "$project": {
                        "storyId": "$_id", "userId": 1, "type": 1, "thumbnail": 1, "urlPath": 1, "isPrivate": 1,
                        "firstName": "$userData.firstName", "duration": 1,
                        "lastName": "$userData.lastName", "userName": "$userData.userName", "profilePic": "$userData.profilePic",
                        "timestamp": 1, "uniqueViews": 1, "uniqueViewCount": { "$size": "$uniqueViews" },
                        "backgroundColor": 1, "statusMessage": 1, "fontType": 1, "caption": 1
                    }
                }, {
                    "$group": {
                        "_id": "$userId",
                        "firstName": { "$first": "$firstName" },
                        "lastName": { "$first": "$lastName" },
                        "userName": { "$first": "$userName" },
                        "profilePic": { "$first": "$profilePic" },
                        "posts": {
                            "$push": {
                                "storyId": "$storyId", "type": "$type", "thumbnail": "$thumbnail", "urlPath": "$urlPath",
                                "isPrivate": "$isPrivate", "duration": "$duration", "uniqueViews": "$uniqueViews",
                                "timestamp": "$timestamp", "uniqueViewCount": "$uniqueViewCount",
                                "backgroundColor": "$backgroundColor", "statusMessage": "$statusMessage", "fontType": "$fontType",
                                "caption": "$caption"
                            }
                        }
                    }
                }
            ];

            storyPostCollection.Aggregate(condition, (err, result) => {
                if (err) return reject(err);

                result.forEach(element => {
                    element.posts.map(ele => { ele["viewed"] = ele.uniqueViews.map(e => e.toString()).includes(userId.toString()) });
                    element["viewedAll"] = (typeof (element.posts.find(post => post.viewed == false)) == "undefined");
                });
                return resolve(result);
            });
        });
    };

    getFolloweeIds()
        .then(() => { return getStoryAtStoryPostCollection(); })
        .then((data) => { return res({ message: req.i18n.__('GetStory')['200'], data: data }).code(200); })
        .catch((error) => {
            console.log(error)
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        })

};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetStory']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code


module.exports = { handler, response };