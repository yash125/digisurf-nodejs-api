const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const followCollection = require("../../../models/follow");
const local = require('../../../locales');
const storyPostCollection = require('../../../models/story');


const validator = Joi.object({
    targetUserId: Joi.string().required().min(24).max(24).description("targetUserId eg. 5ad49a317a7e5c33b4554ba7").error(new Error('targetUserId is missing or incoreect it must be 24 digit')),
    skip: Joi.string().description("skip"),
    limit: Joi.string().description("limit")
}).unknown();

const handler = (req, res) => {
    var skip, limit;
    try {
        skip = parseInt(req.query.skip) || 0
    } catch (error) {
        console.log("PLEPCPD ", error);
        
        req.query.skip = 0;
    }
    try {
        limit = parseInt(req.query.limit) || 20
    } catch (error) { 
        console.log("PLEPCAAPD ", error);
        
        req.query.limit = 20;
    }

    let userId = ObjectID(req.auth.credentials._id);
    let targetUserId = ObjectID(req.query.targetUserId);
    var viewdIdList = [new ObjectID()];

    const getStoryAtStoryPostCollection = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "userId": targetUserId } },
                { "$skip": skip }, { "$limit": limit },
                { "$unwind": { "path": "$totalViews", "preserveNullAndEmptyArrays": true } },
                { "$lookup": { "from": "customer", "localField": "totalViews.userId", "foreignField": "_id", "as": "userData" } },
                { "$unwind": { "path": "$userData", "preserveNullAndEmptyArrays": true } },
                {
                    "$group": {
                        "_id": "$_id",
                        "userId": { "$first": "$userId" },
                        "type": { "$first": "$type" },
                        "thumbnail": { "$first": "$thumbnail" },
                        "isPrivate": { "$first": "$isPrivate" },
                        "timestamp": { "$first": "$timestamp" },
                        "totalViewsCount": { "$first": "$totalViewsCount" },
                        "uniqueViews": { "$first": "$uniqueViews" },
                        "urlPath": { "$first": "$urlPath" },
                        "duration": { "$first": "$duration" },
                        "backgroundColor": { "$first": "$backgroundColor" },
                        "statusMessage": { "$first": "$statusMessage" },
                        "fontType": { "$first": "$fontType" },
                        "caption": { "$first": "$caption" },
                        "viewdList": {
                            "$push": {
                                "userId": "$userData._id",
                                "number": "$userData.mobile",
                                "userName": "$userData.userName",
                                "profilePic": "$userData.profilePic",
                                "firstName": "$userData.firstName",
                                "timestamp": "$totalViews.timestamp"
                            }
                        }
                    }
                },
                { "$lookup": { "from": "customer", "localField": "userId", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                { "$sort": { "_id": -1 } },
                {
                    "$project": {
                        "viewdList": 1,
                        "storyId": "$_id", "userId": 1, "type": 1, "thumbnail": 1, "urlPath": 1, "isPrivate": 1, "firstName": "$userData.firstName",
                        "lastName": "$userData.lastName", "userName": "$userData.userName", "profilePic": "$userData.profilePic",
                        "timestamp": 1, "uniqueViews": 1, "totalViews": 1, "uniqueViewCount": { "$size": "$uniqueViews" }, "duration": 1,
                        "backgroundColor": 1, "statusMessage": 1, "fontType": 1, "caption": 1
                    }
                },
                {
                    "$group": {
                        "_id": "$userId",
                        "firstName": { "$first": "$firstName" },
                        "lastName": { "$first": "$lastName" },
                        "userName": { "$first": "$userName" },
                        "profilePic": { "$first": "$profilePic" },
                        "posts": {
                            "$push": {
                                "storyId": "$storyId", "type": "$type", "thumbnail": "$thumbnail", "urlPath": "$urlPath", "viewdList": "$viewdList",
                                "isPrivate": "$isPrivate", "timestamp": "$timestamp", "uniqueViewCount": "$uniqueViewCount", "duration": "$duration",
                                "backgroundColor": "$backgroundColor", "statusMessage": "$statusMessage", "fontType": "$fontType", "caption": "$caption"
                            }
                        }
                    }
                }
            ];

            storyPostCollection.Aggregate(condition, (err, result) => {
                if (err) return reject(err);

                if (result && result[0] && result[0]["posts"]) {
                    for (let index = 0; index < result[0]["posts"].length; index++) {
                        for (let viewdList = 0; viewdList < result[0]["posts"][index]["viewdList"].length; viewdList++) {
                            viewdIdList.push(ObjectID(result[0]["posts"][index]["viewdList"][viewdList]["userId"]));
                            if (!result[0]["posts"][index]["viewdList"][viewdList]["userId"]) {
                                result[0]["posts"][index]["viewdList"].splice(viewdList, 1);
                            }
                        }
                    }
                }
                return resolve(result);
            });
        });
    };
    const getFollowers = (postResult) => {
        return new Promise((resolve, reject) => {

            followCollection.Select({ "followee": userId, follower: { $in: viewdIdList } }, (err, result) => {
                if (err) {
                    logger.silly("OKWLCODOD ", err);
                    reject(err);
                } else {
                    if (result && result.length) {

                        if (postResult && postResult[0] && postResult[0]["posts"]) {
                            for (let index = 0; index < postResult[0]["posts"].length; index++) {
                                for (let viewdList = 0; viewdList < postResult[0]["posts"][index]["viewdList"].length; viewdList++) {
                                    postResult[0]["posts"][index]["viewdList"][viewdList]["followStatus"] = 0;
                                    try {
                                        let followStatus = result.find(e => {
                                            postResult[0]["posts"][index]["viewdList"][viewdList]["userId"] == e.follower.toString()
                                        })
                                        if (followStatus && followStatus.type && followStatus.type.status) {
                                            postResult[0]["posts"][index]["viewdList"][viewdList]["followStatus"] = followStatus.type.status
                                        } else {
                                            postResult[0]["posts"][index]["viewdList"][viewdList]["followStatus"] = 0;
                                        }
                                    } catch (error) {
                                        postResult[0]["posts"][index]["viewdList"][viewdList]["followStatus"] = 0;
                                        logger.error("PLEWKJIF : ", error)
                                    }
                                }
                            }
                        }
                    }
                }
                return resolve(postResult)
            });
        });
    }

    getStoryAtStoryPostCollection()
        .then(getFollowers)
        .then((data) => { return res({ message: req.i18n.__('GetStoryHistory')['200'], data: data }).code(200); })
        .catch((error) => {
            console.log(error)
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        })

};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetStoryHistory']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code


module.exports = { handler, response, validator };