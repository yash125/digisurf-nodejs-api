let headerValidator = require("../../middleware/validator")
let GetMyAPI = require('./GetMy');
let GetAPI = require('./Get');
let PostAPI = require("./Post");
let DeleteAPI = require("./Delete");
let PatchAPI = require("./Patch");
let GetHistoryAPI = require("./GetHistory");

module.exports = [
    {
        method: 'Get',
        path: '/storyHistory',
        handler: GetHistoryAPI.handler,
        config: {
            tags: ['api', 'story'],
            description: 'API to get all stories of a user',
            auth: 'userJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetHistoryAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetHistoryAPI.response
        }
    },
    
    {
        method: 'Get',
        path: '/myStory',
        handler: GetMyAPI.handler,
        config: {
            tags: ['api', 'story'],
            description: 'API to publish a new post on app',
            auth: 'userJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetMyAPI.response
        }
    },
    {
        method: 'Get',
        path: '/story',
        handler: GetAPI.handler,
        config: {
            tags: ['api', 'story'],
            description: 'API to publish a new post on app',
            auth: 'userJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetMyAPI.response
        }
    },
    {
        method: 'Post',
        path: '/story',
        handler: PostAPI.handler,
        config: {
            tags: ['api', 'story'],
            description: 'API to publish a new post on app',
            auth: 'userJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
    {
        method: 'DELETE',
        path: '/story',
        handler: DeleteAPI.handler,
        config: {
            tags: ['api', 'story'],
            description: 'API to publish a new post on app',
            auth: 'userJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: DeleteAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
    {
        method: 'Patch',
        path: '/story',
        handler: PatchAPI.handler,
        config: {
            tags: ['api', 'story'],
            description: 'API to publish a new post on app',
            auth: 'userJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: PatchAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PatchAPI.response
        }
    }
];