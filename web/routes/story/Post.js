const Joi = require("joi");
const moment = require('moment');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const fcm = require("../../../library/fcm");
const storyCollection = require("../../../models/story");
const storyPostCollection = require("../../../models/storyPost");
const userListCollection = require("../../../models/userList");


const validator = Joi.object({
    type: Joi.number().required().min(1).max(3).description("type : 1 = image,2 = video,3 = status message").error(new Error('type is missing or incoreect it must be 1(image) or 2(video) or  3(status message)')),
    thumbnail: Joi.string().allow("").description("thumbnail").error(new Error('thumbnail is missing')),
    urlPath: Joi.string().allow("").description("urlPath").error(new Error('urlPath is missing')),
    isPrivate: Joi.boolean().required().default(false).description("isPrivate").error(new Error('isPrivate is missing')),
    duration: Joi.string().allow("").description("duration").error(new Error('duration is missing')),
    backgroundColor: Joi.string().allow("").description("backgroundColor").error(new Error('backgroundColor is missing')),
    statusMessage: Joi.string().allow("").description("statusMessage").error(new Error('statusMessage is missing')),
    fontType: Joi.string().allow("").description("FontType").error(new Error('FontType is missing')),
    caption: Joi.string().allow("").description("caption").error(new Error('caption is missing')),
}).unknown();

const handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    let _idForStoryPostCollection = new ObjectID();//this id we are using in both collection as relation of primary(storyPostColl..) and foreign(storyColl..) key
    let timestamp = new Date().getTime();
    let expire = moment().add('days', 1).valueOf();

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    const createDocAtStoryPostCollection = () => {
        /**
         * in this collection appy TTL for 24 H
         * after 24H this document is deleted from database 
         * we also maintain expiry time, 
         * in case of when mongodb is not running and story's time is expired. 
         * so we delete all this storys in garbase story api
         */
        return new Promise((resolve, reject) => {
            let data = {
                _id: _idForStoryPostCollection,
                userId: userId,
                createdAt: new Date(),
                type: req.payload.type,
                thumbnail: req.payload.thumbnail || "",
                urlPath: req.payload.urlPath || "",
                isPrivate: req.payload.isPrivate,
                timestamp: timestamp,
                expire: expire,
                uniqueViews: [],
                totalViews: [],
                totalViewsCount: 0,
                duration: req.payload.duration,
                backgroundColor: req.payload.backgroundColor,
                statusMessage: req.payload.statusMessage,
                fontType: req.payload.fontType,
                caption: req.payload.caption,
            };
            storyPostCollection.Insert(data, (err) => {
                if (err) return reject(err);

                return resolve("--");
            });
        });
    };

    const createDocAtStoryCollection = () => {
        /**
         * in this collection TTL is not applyed
         * so document will not removed.here only change status active to inactive
         */
        return new Promise((resolve, reject) => {


            let data = {
                postId: _idForStoryPostCollection,
                userId: userId,
                type: req.payload.type,
                thumbnail: req.payload.thumbnail,
                urlPath: req.payload.urlPath,
                isPrivate: req.payload.isPrivate,
                status: "active",
                timestamp: timestamp,
                expire: expire,
                uniqueViews: [],
                totalViews: [],
                totalViewsCount: 0,
                duration: req.payload.duration,
                backgroundColor: req.payload.backgroundColor,
                statusMessage: req.payload.statusMessage,
                fontType: req.payload.fontType,
                caption: req.payload.caption,
            };
            storyCollection.Insert(data, (err) => {
                if (err) return reject(err);

                return resolve("--");
            });

        });
    };

    const sendNotification = () => {
        return new Promise((resolve, reject) => {
            if (req.user && userData.friendList) {
                userData.friendList.map(e => {
                    fcm.notifyFcmTpic({
                        fcmTopic: e.toString(),
                        title: process.env.APP_NAME,
                        msg: "Your friend " + userData.userName + " posted a new story."
                    }, (e) => {
                        if (e) return reject(e);
                    })
                });
            }
            return resolve("--");
        });
    };


    getUserData()
        .then(() => { return createDocAtStoryPostCollection(); })
        .then(() => { return sendNotification(); })
        .then(() => { return createDocAtStoryCollection(); })
        .then(() => {
            return res({
                message: req.i18n.__('PostStory')['200']
            }).code(200);
        })
        .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500); });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['PostStory']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response, validator };