let headerValidator = require("../../../middleware/validator")
let GetMyAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/postViews',
        handler: GetMyAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'post'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetMyAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: PostAPI.response
        }
    }
];