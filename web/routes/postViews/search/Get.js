'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const postsCollection = require("../../../../models/posts");
const followCollection = require("../../../../models/follow");

const validator = Joi.object({
    "skip": Joi.string().description('skip'),
    "limit": Joi.string().description('limit'),
    "postId": Joi.string().description('PostId'),
    "searchText": Joi.string().description('searchText'),
}).unknown();
const params = Joi.object({
})

const handler = (req, res) => {
    let _id = req.auth.credentials._id;
    let myFollowers = {};
    let searchText = req.query.searchText;
    let postId = ObjectID(req.query.postId);
    const skip = parseInt(req.query.skip) || 0;
    const limit = parseInt(req.query.limit) || 20;

    const getMyFollowers = () => {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(_id) };
            followCollection.Select(condition, (err, result) => {
                if (err) {
                    console.log("error: SPWEKFOEP ", err)
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else {
                    for (let index = 0; index < result.length; index++) {
                        myFollowers[result[index]["followee"]] = result[index]["type"];
                    }
                    return resolve("--");
                }
            });
        });
    }

    const getLikedUserListFromPost = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "_id": postId } },

                {
                    "$project":
                    {
                        "distinctViews": { "$reverseArray": "$distinctViews" }
                    }
                },
                { "$unwind": "$distinctViews" },
                { "$lookup": { "from": "customer", "localField": "distinctViews", "foreignField": "_id", "as": "customer" } },
                { "$unwind": "$userList" },
                { "$match": { "userList.userStatus": 1 } },
                {
                    "$project": {
                        "userId": "$userList._id",
                        "private": "$userList.private", "profilePic": "$userList.profilePic",
                        "userName": "$userList.userName", "businessProfile": "$userList.businessProfile",
                        "firstName": "$userList.firstName", "lastName": "$userList.lastName",
                        "fullName": { "$concat": ["$userList.firstName", "$userList.lastName"] },
                        "fullWithSpaceName": { "$concat": ["$userList.firstName", " ", "$userList.lastName"] },

                    }
                }
            ];

            if (searchText) {
                condition.push({
                    "$match": {
                        "userName": { "$exists": true },
                        "$or": [
                            { "userName": { "$regex": searchText, "$options": "gi" } },
                            { "firstName": { "$regex": searchText, "$options": "gi" } },
                            { "lastName": { "$regex": searchText, "$options": "gi" } },
                            { "fullName": { "$regex": searchText, "$options": "gi" } },
                            { "fullWithSpaceName": { "$regex": searchText, "$options": "gi" } }
                        ]

                    }
                })
            }
            condition.push({ "$skip": skip }, { "$limit": limit })

            postsCollection.Aggregate(condition, (e, d) => {
                if (e) {
                    console.log("error :PLPWPCD ", e)
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {

                    d.forEach(function (element) {
                        element["type"] = (myFollowers[element["userId"]]) ? myFollowers[element["userId"]] : { "status": 0, "message": "unfollowed" };
                        element["end"] = (element["type"]["status"] == 1) ? false : true;
                        element["follower"] = _id;
                        element["followee"] = (element["userId"] != _id) ? element["userId"] : "";
                        element["firstName"] = (element["firstName"]) ? element["firstName"] : "";
                        element["lastName"] = (element["lastName"]) ? element["lastName"] : "";

                    }, this);

                    return resolve({ code: 200, message: req.i18n.__('GetPostViews')['200'], data: d });
                }
            });
        });
    };

    getMyFollowers()
        .then(() => { return getLikedUserListFromPost() })
        .then((data) => {
            return res({ message: data.message, data: data.data }).code(data.code);
        })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetPostViews']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code

module.exports = { validator, handler, response, params };