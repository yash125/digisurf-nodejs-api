const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const storyPostCollection = require("../../../models/storyPostFromContact");
const storyCollection = require("../../../models/storyFromContact");
const local = require('../../../locales');
const moment = require('moment');

const validator = Joi.object({
    type: Joi.number().required().min(1).max(2).description("type : 1 = image,2 = video").error(new Error('type is missing or incoreect it must be 1(image) or 2(video)')),
    thumbnail: Joi.string().required().description("thumbnail").error(new Error('thumbnail is missing')),
    urlPath: Joi.string().required().description("urlPath").error(new Error('urlPath is missing')),
    isPrivate: Joi.boolean().required().default(false).description("isPrivate").error(new Error('isPrivate is missing')),
    duration: Joi.string().allow("").description("duration").error(new Error('duration is missing')),
}).required();

const handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    let _idForStoryPostCollection = new ObjectID();//this id we are using in both collection as relation of primary(storyPostColl..) and foreign(storyColl..) key
    let timestamp = new Date().getTime();
    let expire = moment().add('days', 1).valueOf();

    const createDocAtStoryPostCollection = () => {
        /**
         * in this collection appy TTL for 24 H
         * after 24H this document is deleted from database 
         * we also maintain expiry time, 
         * in case of when mongodb is not running and story's time is expired. 
         * so we delete all this storys in garbase story api
         */
        return new Promise((resolve, reject) => {
            let data = {
                _id: _idForStoryPostCollection,
                userId: userId,
                createdAt: new Date(),
                type: req.payload.type,
                thumbnail: req.payload.thumbnail,
                urlPath: req.payload.urlPath,
                isPrivate: req.payload.isPrivate,
                timestamp: timestamp,
                expire: expire,
                uniqueViews: [],
                totalViews: [],
                totalViewsCount: 0,
                duration: req.payload.duration
            };
            storyPostCollection.Insert(data, (err) => {
                if (err) return reject(err);

                return resolve("--");
            });
        });
    };

    const createDocAtStoryCollection = () => {
        /**
         * in this collection TTL is not applyed
         * so document will not removed.here only change status active to inactive
         */
        return new Promise((resolve, reject) => {


            let data = {
                postId: _idForStoryPostCollection,
                userId: userId,
                type: req.payload.type,
                thumbnail: req.payload.thumbnail,
                urlPath: req.payload.urlPath,
                isPrivate: req.payload.isPrivate,
                status: "active",
                timestamp: timestamp,
                expire: expire,
                uniqueViews: [],
                totalViews: [],
                totalViewsCount: 0,
                duration: req.payload.duration
            };
            storyCollection.Insert(data, (err) => {
                if (err) return reject(err);

                return resolve("--");
            });

        });
    };

    createDocAtStoryPostCollection()
        .then(() => { return createDocAtStoryCollection(); })
        .then(() => {
            return res({
                message: req.i18n.__('PostStory')['200']
            }).code(200);
        })
        .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500); });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['PostStory']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response, validator };