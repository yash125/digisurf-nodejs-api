const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const storyPostCollection = require('../../../models/storyPostFromContact');
const userListCollection = require('../../../models/userList');

const handler = (req, res) => {
    let userId = ObjectID(req.auth.credentials._id);
    let userIds = ["userids in ObjectID"];
    userData.contacts = userData.contacts || [];


    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    function getUserIdFromContactNumbers() {
        return new Promise((resolve, reject) => {

            userData.contacts.push("1234567890");
            userListCollection.Select({ number: { "$in": userData.contacts } }, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(element => {
                        userIds.push(ObjectID(element._id))
                    });
                    return resolve(true);
                }
            });
        });
    }

    const getStoryAtStoryPostCollection = () => {
        return new Promise((resolve, reject) => {


            let condition = [
                { "$match": { "userId": { "$in": userIds } } },
                { "$lookup": { "from": "customer", "localField": "userId", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                { "$sort": { "_id": 1 } },
                {
                    "$project": {
                        "storyId": "$_id", "userId": 1, "type": 1, "thumbnail": 1, "urlPath": 1, "isPrivate": 1,
                        "firstName": "$userData.firstName", "duration": 1,
                        "lastName": "$userData.lastName", "userName": "$userData.userName", "profilePic": "$userData.profilePic",
                        "timestamp": 1, "uniqueViews": 1, "uniqueViewCount": { "$size": "$uniqueViews" },
                    }
                }, {
                    "$group": {
                        "_id": "$userId",
                        "firstName": { "$first": "$firstName" },
                        "lastName": { "$first": "$lastName" },
                        "userName": { "$first": "$userName" },
                        "profilePic": { "$first": "$profilePic" },
                        "posts": {
                            "$push": {
                                "storyId": "$storyId", "type": "$type", "thumbnail": "$thumbnail", "urlPath": "$urlPath",
                                "isPrivate": "$isPrivate", "duration": "$duration", "uniqueViews": "$uniqueViews",
                                "timestamp": "$timestamp", "uniqueViewCount": "$uniqueViewCount"
                            }
                        }
                    }
                }
            ];

            storyPostCollection.Aggregate(condition, (err, result) => {
                if (err) return reject(err);

                result.forEach(element => {
                    element.posts.map(ele => { ele["viewed"] = ele.uniqueViews.map(e => e.toString()).includes(userId.toString()) });
                    element["viewedAll"] = (typeof (element.posts.find(post => post.viewed == false)) == "undefined");
                });
                return resolve(result);
            });
        });
    };

    getUserData()
        .then(() => { return getUserIdFromContactNumbers(); })
        .then(() => { return getStoryAtStoryPostCollection(); })
        .then((data) => { return res({ message: req.i18n.__('GetStory')['200'], data: data }).code(200); })
        .catch((error) => {
            console.log(error)
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        })

};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetStory']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code


module.exports = { handler, response };