const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const storyPostCollection = require('../../../models/storyPostFromContact');
const storyCollection = require('../../../models/storyFromContact');

const validator = Joi.object({
    storyId: Joi.string().required().min(24).max(24).description("storyId eg. 5ad49a317a7e5c33b4554ba7").error(new Error('storyId is missing or incoreect it must be 24 digit')),
}).required();

const handler = (req, res) => {
    let userId = ObjectID(req.auth.credentials._id);
    var storyId = ObjectID(req.query.storyId);

    const modifyDocAtStoryPostCollection = () => {

        return new Promise((resolve, reject) => {
            let condition = { "_id": storyId, "uniqueViews": { "$ne": userId }, "userId": { "$ne": userId } };
            let data = { "uniqueViews": userId }
            storyPostCollection.UpdateByIdWithAddToSet(condition, data, (err, result) => {
                if (err) return reject(err);

                result = JSON.parse(JSON.stringify(result));

                if (result.nModified) {
                    let condition = { "_id": storyId };
                    let data = { "totalViewsCount": 1 }
                    storyPostCollection.UpdateWithIncrice(condition, data, (err) => {
                        if (err) return reject(err);
                    });
                    condition = { "_id": storyId };
                    data = { "totalViews": { "userId": userId, timestamp: new Date().getTime() } }
                    storyPostCollection.UpdateByIdWithPush(condition, data, (err) => {
                        if (err) return reject(err);
                    });
                }
            });


            return resolve("--");
        });
    };

    const modifyDocAtStoryCollection = () => {
        return new Promise((resolve, reject) => {



            let condition = { "postId": storyId, "uniqueViews": { "$ne": userId }, "userId": { "$ne": userId } };
            let data = { "uniqueViews": userId };
            storyCollection.UpdateByIdWithAddToSet(condition, data, (err, result) => {
                if (err) return reject(err);

                result = JSON.parse(JSON.stringify(result));
                if (result && result.nModified) {
                    let condition = { "postId": storyId };
                    let data = { "totalViewsCount": 1 }
                    storyCollection.UpdateWithIncrice(condition, data, (err) => {
                        if (err) return reject(err);
                    });
                    condition = { "postId": storyId };
                    data = { "totalViews": { "userId": userId, timestamp: new Date().getTime() } };
                    storyCollection.UpdateWithPush(condition, data, (err) => {
                        if (err) return reject(err);
                    });
                }
            });
            return resolve("--");
        });
    };

    modifyDocAtStoryPostCollection()
        .then(() => { return modifyDocAtStoryCollection(); })
        .then(() => { return res({ message: req.i18n.__('PatchViewStory')['200'] }).code(200); })
        .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500); })

};

const response = {
    status: {
        200: { message: Joi.any().default(local['PatchViewStory']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response, validator };