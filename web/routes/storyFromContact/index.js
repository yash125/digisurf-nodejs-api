let headerValidator = require("../../middleware/validator")
let GetMyAPI = require('./GetMy');
let GetAPI = require('./Get');
let PostAPI = require("./Post");
let DeleteAPI = require("./Delete");
let PatchAPI = require("./Patch");

module.exports = [
    {
        method: 'Get',
        path: '/myStoryFromContact',
        handler: GetMyAPI.handler,
        config: {
            tags: ['api', 'storyFromContact'],
            description: 'API to publish a new post on app',
            auth: 'userJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetMyAPI.response
        }
    },
    {
        method: 'Get',
        path: '/storyFromContact',
        handler: GetAPI.handler,
        config: {
            tags: ['api', 'storyFromContact'],
            description: 'API to publish a new post on app',
            auth: 'userJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetMyAPI.response
        }
    },
    {
        method: 'Post',
        path: '/storyFromContact',
        handler: PostAPI.handler,
        config: {
            tags: ['api', 'storyFromContact'],
            description: 'API to publish a new post on app',
            auth: 'userJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
    {
        method: 'DELETE',
        path: '/storyFromContact',
        handler: DeleteAPI.handler,
        config: {
            tags: ['api', 'storyFromContact'],
            description: 'API to publish a new post on app',
            auth: 'userJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: DeleteAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
    {
        method: 'Patch',
        path: '/storyFromContact',
        handler: PatchAPI.handler,
        config: {
            tags: ['api', 'storyFromContact'],
            description: 'API to publish a new post on app',
            auth: 'userJWT',
            plugins: {
                'hapi-swagger': {
                    payloadType: 'form'
                }
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: PatchAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PatchAPI.response
        }
    }
];