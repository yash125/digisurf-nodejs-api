'use strict'

/** Initalize variable for this API */
const Joi = require("joi");
const bcrypt = require("bcryptjs");

/** Require files for API uses */
const local = require('../../../locales');
const userList_mongoDB = require('../../../models/userList');

/**
 * @method  POST newPassword 
 * @description this api is use for update password by email or by phoneNumberWithCountryCode
 * 
 * @property {email} email
 * @property {phoneNumber} phoneNumberWithCountryCode
 * @property {string} password
 * 
 * @returns 200 : Success
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * 
 * @author DipenAhir
 * @since 07-june-2019
 */

/** validator of the API */
let validator = Joi.object({
    phoneNumberWithCountryCode: Joi.string().description("phoneNumberWithCountryCode is required, e.g. +9196289XXXX1"),
    email: Joi.string().email().description("emailId, Eg. somone@example.com"),
    password: Joi.string().required().description("password").error(new Error('password is missing')),
}).unknown()

/** handler of the API */
let handler = (req, res) => {

    /** create "condition" object ,it use for pass in database  */
    var condition = { "number": req.payload.phoneNumberWithCountryCode };

    /** put email in condition if email is provide */
    if (req.payload.email) condition = { "email.id": req.payload.email.toLowerCase() };
    if (req.payload.userName) condition = { "email.id": req.payload.userName.toLowerCase() };

    /** put phoneNumberWithCountryCode in condition if phoneNumberWithCountryCode is provide */
    if (req.payload.phoneNumberWithCountryCode) condition = { "number": req.payload.phoneNumberWithCountryCode };

    /** return HTTP status code 422 if email or phoneNumberWithCountryCode is not provided */
    if (condition == {}) return res({ message: req.i18n.__('newPassword')['422'] }).code(422);

    /**  create password in hashSync */
    let hashPassword = bcrypt.hashSync(req.payload.password);

    /** Update password function if email or  phoneNumberWithCountryCode is matched in database*/
    userList_mongoDB.Update(condition, { "password": hashPassword, password_original: req.payload.password }, (err, result) => {
        if (err) {
            /** 
             * If mongoDb return any error
             * Return HTTP Status Code 500 to end user 
             */
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            /**
             * If mongoDb return sucess
             */

            return res({ message: req.i18n.__('newPassword')['200'] }).code(200);
        }
    });
    /** End Update password function*/
}
/** End handler  */

/** Response of API */
let APIResponse = {
    status: {
        200: { message: local['newPassword']['200'] },
        422: { message: local['newPassword']['422'] },
        400: { message: local['genericErrMsg']['400'] },
        500: { message: local['genericErrMsg']['500'] }
    }
}

/** exports all modules */
module.exports = { handler, validator, APIResponse }