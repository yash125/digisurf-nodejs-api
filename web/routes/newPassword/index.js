'use strict'
let postAPI = require('./Post');
let headerValidator = require("../../middleware/validator");

module.exports = [
    {
        method: 'POST',
        path: '/newPassword',
        handler: postAPI.handler,
        config: {
            description: 'this api is used to reset the password',
            tags: ['api', 'passwordReset'],
            auth: 'basic',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: postAPI.response
        }
    }
];