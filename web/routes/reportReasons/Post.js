const Joi = require("joi");
const ObjectID = require('mongodb').ObjectID;
const postsCollection = require("../../../models/posts");
const postReportCollection = require("../../../models/postReport");
const local = require('../../../locales');

/**
 * @method POST reportUser
 * @description This API use to POST reportUser.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 * @property {*} targetUserId targetUserId
 * @property {*} reason reason
 * @property {*} message message
 
 * @returns  200 : User reported successfully..
 * @returns 400 : targetUser doesnot exist, try with a different targetUserId.
 * @returns  412 : Reasons to report user doesnot exist.
 * @returns 422 : User cannot report his own targetUserId,please use a different targetUserId.
 * @returns  500 : An unknown error has occurred.
 */

const validator = Joi.object({
    postId: Joi.string().required().max(24).min(24).description("postId").example("5a096e680941c626e794bc5c").error(new Error('postId is missing || incorrect postId length must be 24 Character')),
    reason: Joi.string().required().description("reason").example("Duplicate User").error(new Error('reason is missing ')),
    message: Joi.string().required().description("message").example("message").error(new Error('message is missing'))
}).unknown();


let handler = (req, res) => {
    let _id = req.auth.credentials._id;
    let postId = ObjectID(req.payload.postId);
    let reason = req.payload.reason;
    let message = req.payload.message;

    postsCollection.SelectOne({ _id: postId }, (err, result) => {
        if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

        if (!result || !result["_id"] || result["_id"] == null) { return res({ message: req.i18n.__('PostReportReason')['412'] }).code(412); }

        let dataToInsert = {
            "userId": ObjectID(_id),
            "postId": postId,
            "reason": reason,
            "messsage": message,
            "creation": new Date().getTime()
        };

        postReportCollection.Insert(dataToInsert, (err) => {
            if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

            return res({ message: req.i18n.__('PostReportReason')['200'] }).code(200);
        });
    });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['PostReportReason']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        409: { message: Joi.any().default(local['genericErrMsg']['409']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { validator, handler, response };