'use strict'
const Joi = require("joi");
const logger = require('winston');

const local = require('../../../locales');
const userReportReasonsCollection = require("../../../models/userReportReasons");

/**
 * @method GET reportUserReasons
 * @description This API use to get reportUserReasons.
 
 * @param {*} req 
 * @param {*} res 
 
 * @property {*} authorization in header
 * @property {*} lang in header
 
 * @returns  200 : Reasons to report user sent successfully.
 * @example {
  "message": "Reasons to report user sent successfully.",
  "data": {
    "data": [
      "Duplicate user"
    ]
  }
}
 * @returns  412 : Reasons to report user doesnot exist.. 
 * @returns  500 : An unknown error has occurred.
 */
let handler = (req, res) => {
  let dataTosend = [];
  let lang = req.headers.lang;

  userReportReasonsCollection.SelectWithSort({ "status": 1 }, { "sequenceId": -1 }, (err, result) => {
    if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    if (!result.length) return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);

    for (let index = 0; index < result.length; index++) {
      try {

        if (result[index]["lang"][lang]) {
          dataTosend.push(result[index]["lang"][lang]);
        }
      } catch (error) {
        logger.error("APLDPW : ", error);
      }

    }
    return res({ message: req.i18n.__('GetUserReasons')['200'], data: dataTosend }).code(200);
  });
};


const response = {
  status: {
    200: { message: Joi.any().default(local['GetUserReasons']['200']), data: Joi.any()},
    204: { message: Joi.any().default(local['genericErrMsg']['204']) },
    400: { message: Joi.any().default(local['genericErrMsg']['400']) },
  }
}//swagger response code


module.exports = { handler, response };