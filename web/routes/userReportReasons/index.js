let headerValidator = require("../../middleware/validator")
let GetAPI = require('./Get');
let PostAPI = require('./Post');

module.exports = [
    {
        
        method: 'GET',
        path: '/userReportReasons',
        handler: GetAPI.handler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'reportReasons'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
    {
        method: 'POST',
        path: '/userReportReasons',
        handler: PostAPI.handler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'reportReasons'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
];