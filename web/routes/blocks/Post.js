'use strict'
//require npm modules
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

//require dependency
const local = require('../../../locales');
const userCollection = require("../../../models/userList");
const blockedUsersCollection = require("../../../models/blockedUsers");

//API params Validators
const paramsValidator = Joi.object({
    targetId: Joi.string().default("5dd7c91e162b8c758c2ad44c").required().min(24).max(24).description("targetId, mongo id of the target user"),
    type: Joi.string().default("block").allow(["block", "unblock"]).required().description("type  : block,unblock"),
}).unknown();

//API payload Validators
const payloadValidator = Joi.object({
    targetId: Joi.string().default("5dd7c91e162b8c758c2ad44c").example("5dd7c91e162b8c758c2ad44c").required().min(24).max(24).description("targetId, mongo id of the target user"),
    type: Joi.string().default("block").example("block").allow(["block", "unblock"]).required().description("type  : block,unblock"),
    reason: Joi.string().default("Spam/Fake Profile").example("Spam/Fake Profile").allow("").required().description("reason"),
}).unknown();

//Start API handler
const handler = (req, res) => {

    //API constance
    const type = req.payload.type;
    const userId = req.auth.credentials._id;
    const targetId = ObjectID(req.payload.targetId);

    //promises
    setInUserInUserListCollection()
        .then(() => { return setInUserInUserBlockedCollection(); })
        .then(() => { return res({ message: req.i18n.__('PostBlock')['200'] }).code(200); })
        .catch((err) => { return res({ message: err.message }).code(err.code); })

    //update in userlist collection
    function setInUserInUserListCollection() {
        return new Promise(function (resolve, reject) {
            if (type == 'block') {//if user wants to block
                //In blocked array add to set  with target id
                userCollection.UpdateByIdWithAddToSet({ _id: userId }, { "blocked": targetId }, (err) => {
                    if (err) {
                        //return 500 responce : if have any issues in  function
                        logger.error("error ", err);
                        return reject({ code: 500, message: local['genericErrMsg']['200'] })
                    } else {
                        return resolve(true);
                    }
                });
            } else {//if user wants to unblock

                //remove from blocked array 
                userCollection.UpdateByIdWithPull({ _id: userId }, { "blocked": targetId }, (err) => {
                    if (err) {
                        //return 500 responce : if have any issues in  function
                        logger.error("error ", err)
                        return reject({ code: 500, message: local['genericErrMsg']['200'] })
                    } else {
                        return resolve(true);
                    }
                });
            }

        });
    }
    //update in blocked collection
    function setInUserInUserBlockedCollection() {
        return new Promise(function (resolve, reject) {
            if (type == 'block') {//if user wants to block
                let dataToInsert = {
                    userId: ObjectID(userId),
                    targetUserId: targetId,
                    creation: new Date().getTime(),
                    reason: req.payload.reason
                };
                //Insert in block collection
                blockedUsersCollection.Insert(dataToInsert, (err) => {
                    if (err) {
                        //return 500 responce : if have any issues in Aggregate function
                        logger.error("error ", err)
                        return reject({ code: 500, message: local['genericErrMsg']['200'] })
                    } else {
                        return resolve(true);
                    }
                });
            } else {//if user wants to unblock
                let datatoDelete = {
                    userId: ObjectID(userId),
                    targetUserId: targetId
                };
                //remove from block collection
                blockedUsersCollection.Delete(datatoDelete, (err) => {
                    if (err) {
                        //return 500 responce : if have any issues in Aggregate function
                        logger.error("error ", err)
                        return reject({ code: 500, message: local['genericErrMsg']['200'] })
                    } else {
                        return resolve(true);
                    }
                });
            }

        });
    }
}//Start API handler


const response = {
    status: {
        200: { message: Joi.string().default(local['PostBlock']['200']).required().example(local['PostBlock']['200']) },
        400: { message: Joi.string().default(local['genericErrMsg']['400']).required().example(local['genericErrMsg']['400']) },
    }
}//swagger response code
module.exports = { paramsValidator, payloadValidator, handler, response };