'use strict'
const Joi = require("joi");

const local = require('../../../locales');
const blockReasonsCollection = require("../../../models/blockReasons");

/**
 * @method GET blocksReasons
 * @description This api returns blocksReasons
 * 
 * @headers authentication 
 * @headers lang
 * 
 * @author DipenAhir
 * @since 09-July-2019
 */

//Start API handler
let handler = (req, res) => {

    let dataTosend = [];
    let lang = req.headers.lang;

    //get block reasons form db
    blockReasonsCollection.SelectWithSort({ "status": 1 }, { sequenceId: -1 }, (err, result) => {
        if (err) return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);//return 500 responce : if have any issues in Aggregate function
        if (!result.length) return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);//return 204 responce : if data not found in db

        result.forEach(function (element) {
            dataTosend.push(element["lang"][lang]);
        }, this);
        //return sucess responce
        return res({ message: req.i18n.__('GetBlockReasons')['200'], data: dataTosend }).code(200);
    });
};//ENd API handler

const response = {
    status: {
        200: {
            message: Joi.string().default(local['GetBlockReasons']['200']).example(local['GetBlockReasons']['200']).required(),
            data: Joi.array().example([
                "Spam/Fake Profile",
                "Inappropriate Communication/Behavior",
                "Other"
            ]).required()
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']).example(local['genericErrMsg']['204']) }
    }
};//swagger response code

module.exports = { handler, response }