'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userListCollection = require("../../../models/userList");


/**
 * @method GET blocks
 * @description This API returns all users that I have blocked 
 * 
 * @headers authentication 
 * @headers lang
 * 
 * @author DipenAhir
 * @since 09-July-2019
 */

// Start API handler
let handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    // let offset = parseInt(req.query.offset) || 0;
    // let limit = parseInt(req.query.limit) || 20;

    getMatchdata()
        .then(function (value) {
            return res({ message: req.i18n.__('GetBlocks')['200'], data: value }).code(200);
        })
        .catch(function () {
            return res({ message: local['genericErrMsg']['204'] }).code(204);
        });

    //function for get all blocked users
    function getMatchdata() {
        return new Promise(function (resolve, reject) {

            //condition to get all block users
            let condition = [
                { "$match": { "_id": userId } },
                { "$unwind": "$blocked" },
                { "$lookup": { "from": "customer", "localField": "blocked", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                {
                    "$project": {
                        "_id": "$userData._id",
                        "countryCode": "$userData.countryCode",
                        "number": "$userData.mobile",
                        "userName": "$userData.userName",
                        "profilePic": "$userData.profilePic",
                        "firstName": "$userData.firstName",
                        "lastName": "$userData.lastName",
                        "status": "$userData.statusBio",
                        "email": "$userData.email"
                    }
                }
            ];

            //this function returns all blocked users
            userListCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    return reject(err);
                }
                if (result && result.length) {
                    //if have blocked users
                    return resolve(result);
                }
                //if did't have blocked users
                return reject(true);
            })
        });
    }
};// End API handler

const response = {
    status: {
        200: {
            message: Joi.string().default(local['GetBlocks']['200']).required().example(local['GetBlocks']['200']),
            data: Joi.array()
                .items({
                    _id: Joi.any().required().example("5dd7c91e162b8c758c2ad44c"),
                    countryCode: Joi.string().required().example("+91"),
                    number: Joi.string().required().example("+918318289606"),
                    userName: Joi.string().required().example("Ajio"),
                    profilePic: Joi.string().required().example("https://s3.ap-south-1.amazonaws.com/dubly/profile_image/5dd7c91e162b8c758c2ad44c"),
                    firstName: Joi.string().required().example("Ajio"),
                    lastName: Joi.string().example("Ajio").allow(["",null]),
                    status: Joi.string().required().example("Hey I am using this app").allow(["",null]),
                    email: Joi.string().required().example("alokjio@mobifyi.com").allow(["",null])
                })
                .example([{
                    "_id": "5dd7c91e162b8c758c2ad44c",
                    "countryCode": "+91",
                    "number": "+918318289606",
                    "userName": "Ajio",
                    "profilePic": "https://s3.ap-south-1.amazonaws.com/dubly/profile_image/5dd7c91e162b8c758c2ad44c",
                    "firstName": "Ajio",
                    "lastName": "",
                    "status": "Hey I am using Do Chat!",
                    "email": "alokjio@mobifyi.com"
                }]).
                required()
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']).example(local['genericErrMsg']['204']) }
    }
};//swagger response code

module.exports = { handler, response }