let headerValidator = require("../../middleware/validator")
let GetAPI = require('./Get');
let PostAPI = require('./Post');
let GetReasonsAPI = require('./GetReasons');

module.exports = [
    {
        method: 'Get',
        path: '/blockReasons',
        handler: GetReasonsAPI.handler,
        config: {
            description: `This api returns blocksReasons`,
            tags: ['api', 'blocks'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetReasonsAPI.response
        }
    },
    {
        method: 'POST',
        path: '/block',
        handler: PostAPI.handler,
        config: {
            description: `This API use for block or unblock a user`,
            tags: ['api', 'blocks'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
    {
        method: 'GET',
        path: '/blocks',
        handler: GetAPI.handler,
        config: {
            description: `This API returns all users that I have blocked`,
            tags: ['api', 'blocks'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];