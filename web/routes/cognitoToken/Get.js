
'use strict';
const joi = require('joi');
const logger = require('winston');
const errorMsg = require('../../../locales');
const cognitoUser = require('../../../library/uploadImage/Cognito');


const handler = (req, reply) => {
    cognitoUser.getCognitoTokens(req.auth.credentials._id)
        .then((data) => {
            return reply({
                message: req.i18n.__('cognitoTokenGet')['200']['message'],
                data: data
            }).code(200);
        }).catch((e) => {
            logger.error("cognitoToken error =>", e)
            return reply({ message: e.message }).code(500);
        });//get token
};//API handler

const responseCode = {
    status: {
        200: joi.object({
            message: joi.string().required().example(errorMsg['cognitoTokenGet']['200']['message']).description(errorMsg['cognitoTokenGet']['200']['description']),
            data: joi.object({
                IdentityId: joi.string().required().description(errorMsg['cognitoTokenGet']['responseDescription']['IdentityId']),
                Token: joi.string().required().description(errorMsg['cognitoTokenGet']['responseDescription']['Token']),
                bucket: joi.string().required().description(errorMsg['cognitoTokenGet']['responseDescription']['bucket']),
                region: joi.string().required().description(errorMsg['cognitoTokenGet']['responseDescription']['region']),
            }).example({
                "IdentityId": "ap-XXXXX-1:XXXXXXXXXX-95d8-4981-a76e-eafeed2d5000",
                "Token": "XXXXXXXXXXXXiJhcC1zb3V0aC0xMSIsInR5cCI6IkpXUyIsImFsZyI6IlJTNTEyIn0.eyJzdWIiOiJhcC1zb3V0aC0xOjczMTY3NDAxLTk1ZDgtNDk4MS1hNzZlLWVhZmVlZDJkNTAwMCIsImF1ZCI6ImFwLXNvdXRoLTE6N2I0YWM1NGYtODBmZS00Y2FjLWE3NmEtODlmYjNlYzgxNzUxIiwiYW1yIjpbImF1dGhlbnRpY2F0ZWQiLCJzZXJ2aWNlZ2VuaWUuM2VtYmVkIiwic2VydmljZWdlbmllLjNlbWJlZDphcC1zb3V0aC0xOjdiNGFjNTRmLTgwZmUtNGNhYy1hNzZhLTg5ZmIzZWM4MTc1MTptYWhlbmRyYSB2YWdoYW5pIl0sImlzcyI6Imh0dHBzOi8vY29nbml0by1pZGVudGl0eS5hbWF6b25hd3MuY29tIiwiZXhwIjoxNTgwMzgyNDY3LCJpYXQiOjE1ODAzODIzNDd9.RTAlTUUBvtG13iRfxYUZMTNwUqb-IgA8Pknodp9SRVZwLCP1pMxLlr_20FCpZfZWauZMWBsr12RhRc7mLXWS0plwdT7c7Tkk7iQl6GPusT7E0-3omnuPUJkkTl0cIRdNeiWR1OWN3DKQxIdWCAuUcL6SD7Ivw8NTzEvwIAz7DzqPd-iLA5G2xl6SSmM0rsJxo2ncpMc5aj1pg2HzXlG3_ySbr1zkbcZGM3NsfVdPIJhRwvAkCk8qy4Z4uuXZaHHDdtdmeO8KBL_KHU1Z0AkdXiGUka0PjHoy21Cw0IFpadq4lcFHGVA9nmCa7De89WO8IaV70FE9qQ6s8Xxi1VyajA",
                "bucket": "dublyXXXX",
                "region": "ap-XXXXX-1",
            }).label('data')
        }).label('200'),
        500: joi.object({ message: joi.string().required().example(errorMsg['cognitoTokenGet']['500']['message']).description(errorMsg['cognitoTokenGet']['500']['description']) }).label('500')
    }

}//swagger response code

module.exports = {
    handler,
    responseCode
};