let headerValidator = require("../../middleware/validator")
let GetAPI = require('./Get');
let PostAPI = require('./Post');

module.exports = [

    {
        method: 'GET',
        path: '/cognitoToken',
        handler: GetAPI.handler,
        config: {
            description: `This API is used to get all cognito details`,
            tags: ['api', 'cognitoToken'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.responseCode
        }
    },
    {
        method: 'POST',
        path: '/cognitoToken',
        handler: PostAPI.handler,
        config: {
            description: `This API is used to upload image in aws `,
            tags: ['api', 'cognitoToken'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
	        payload : PostAPI.payload,
                failAction: (req, reply, source, error) => {
           	     headerValidator.faildAction(req, reply, source, error)
               }
            },
  
  //        response: PostAPI.responseCode
        }
    }

];
