
'use strict';
const joi = require('joi');
const logger = require('winston');
const errorMsg = require('../../../locales');
const cognitoUser = require('../../../library/uploadImage/Cognito');

const payloadValidator = {
    maxBytes: 1000 * 1000 * 5, // 5 Mb
    output: 'stream',
    parse: true,
    allow: 'multipart/form-data'
};//payload of upload image api

const payload = {
    uploadTo: joi.number().allow(1, [1, 2]).required().description(errorMsg['imageUploadPost']['payload']['uploadTo']),
    folder: joi.string().required().description(errorMsg['imageUploadPost']['payload']['folder']),
    file: joi.any().meta({ swaggerType: 'file' }).required().description(errorMsg['imageUploadPost']['payload']['file']),
    fileName: joi.string().allow('').description(errorMsg['imageUploadPost']['payload']['fileName']),
}

/**
* @method POST /imageUpload
* @param {*} req
* @param {*} reply
*/

const handler = (req, reply) => {

    const uploadImage = () => {
        return new Promise((resolve, reject) => {
            switch (req.payload.uploadTo) {
                case 1:
			
                    cognitoUser.uploadImageInS3('defulat_user', req)
                        .then((data) => {
                            return resolve(data)
                        }).catch((e) => {
                            return reject(e);
                        });//get token
                case 2:
                    break;
            }

        });
    }
    uploadImage()
        .then(data => {
            return reply({
                message: req.i18n.__('imageUploadPost')['200']['message'],
                data: data
            }).code(200);
        }).catch(e => {
            logger.error("error while uploading image =>", e)
            return reply({ message: e.message }).code(e.code);
        });
};//API handler

const responseCode = {
    status: {
        200: joi.object({
            message: joi.string().required().example(errorMsg['imageUploadPost']['200']['message']).description(errorMsg['imageUploadPost']['200']['description']),
            data: joi.object({
                imageUrl: joi.string().required().description(errorMsg['imageUploadPost']['responseDescription']['imageUrl']),
            }).example({
                "imageUrl": "https://servicegenie.s3.ap-south-1.amazonaws.com/IMG_1580386991.png"
            }).label('data')
        }).label('200'),
        500: joi.object({ message: joi.string().required().example(errorMsg['imageUploadPost']['500']['message']).description(errorMsg['imageUploadPost']['500']['description']) }).label('500')
    }

}//swagger response code

module.exports = {
    payloadValidator,
    payload,
    handler,
    responseCode
};
