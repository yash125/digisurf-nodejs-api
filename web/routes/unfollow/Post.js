'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const followCollection = require("../../../models/follow");
const userCollection = require("../../../models/userList");
const notificationCollection = require("../../../models/notification");


const validator = Joi.object({
    followingId: Joi.string().required().description("followingId, mongo id of the followee")
}).unknown();

const handler = (req, res) => {
    let memberId = ObjectID(req.payload.followingId);
    let userId = ObjectID(req.auth.credentials._id);
    let dataToSend = { followStatus: 0, isPrivate: 0 }

    followCollection.GetFollowerCount(userId, (err, result) => {
        if (err) {
            return res({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
        } else {
            let followerCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
            userCollection.UpdateById(userId.toString(), { "count.followerCount": followerCount }, () => { })
        }
    })
    followCollection.GetFolloweeCount(userId, (err, result) => {
        if (err) {
            return res({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
        } else {
            let followeeCount = (result[0] && result[0]["total"]) ? result[0]["total"] - 1 : 0;
            userCollection.UpdateById(userId.toString(), { "count.followeeCount": followeeCount }, () => { })
        }
    });

    const checkFollowRelation = () => {
        return new Promise((resolve, reject) => {
            let condition = { "followee": memberId, "follower": userId, "end": false };

            followCollection.Select(condition, (e, d) => {
                if (e) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    return resolve(d);
                }
            });
        });
    };
    const unfollowMember = () => {
        return new Promise((resolve, reject) => {
            let condition = { "followee": memberId, "follower": userId };
            let update = { "end": true, 'type': { "status": 0, "message": "unfollowed" } };
            followCollection.Update(condition, update, (e) => {
                if (e) {
                    logger.error(e);
                    return reject(e);
                }
                userCollection.UpdateWithPull({ _id: userId }, { "follow": memberId }, (e) => { if (e) logger.error("AOKAO : ", e); });
                userCollection.UpdateWithPull({ _id: memberId }, { "follow": userId }, (e) => { if (e) logger.error("AOKAOPP : ", e); });
            });
            return resolve(true)
        });
    };
    const updateNotificationCollection = () => {
        return new Promise((resolve, reject) => {

            let condition = {
                "to": memberId, "from": userId, "end": false,
                "$or": [{ "type.status": 3 }, { "type.status": 4 }]
            };

            let update = { end: true };
            notificationCollection.Update(condition, update, (e) => {
                if (e) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    return resolve({ message: req.i18n.__('PostUnFollow')['200'], userId: memberId, isPrivate: dataToSend["isPrivate"], followStatus: dataToSend["followStatus"], isAllFollow: false });
                }
            });
        });
    };
    const isPrivateUser = () => {
        return new Promise((resolve, reject) => {
            let condition = { _id: memberId };
            userCollection.SelectOne(condition, (e, result) => {
                if (e) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                }

                if (result && result.private) {
                    dataToSend["isPrivate"] = result.private;
                } else {
                    dataToSend["isPrivate"] = 0;
                }
            });
            return resolve(true)
        });
    }

    checkFollowRelation()
        .then(isPrivateUser)
        .then(unfollowMember)
        .then(updateNotificationCollection)
        .then((data) => { return res(data).code(200); })
        .catch((error) => { return res(error).code(500); })
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostUnFollow']['200']),
            userId: Joi.any(), followStatus: Joi.any(),
            isPrivate: Joi.any(), isAllFollow: Joi.any()
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code

module.exports = { validator, handler, response };