const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userListCollection = require("../../../models/userList");

const payloadValidator = Joi.object({
    musicId: Joi.string().min(24).max(24).required().description('musicId'),
    isFavourite: Joi.boolean().required().description('isFavourite'),
}).unknown();


const hander = (req, res) => {
    let userId = req.auth.credentials._id;
    let musicId = ObjectID(req.payload.musicId);

    const setMyData = () => {
        return new Promise((resolve, reject) => {
            if (req.payload.isFavourite) {
                userListCollection.UpdateByIdWithAddToSet({ _id: userId }, { favouriteMusic: musicId }, (e) => {
                    if (e) {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    }
                    return resolve({ message: req.i18n.__('PostFavouriteMusic')['200'] });
                })
            } else {
                userListCollection.UpdateByIdWithPull({ _id: userId }, { favouriteMusic: musicId }, (e) => {
                    if (e) {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    }
                    return resolve({ message: req.i18n.__('PostFavouriteMusic')['200'] });
                })
            }
        });
    };

    setMyData()
        .then((data) => { return res(data).code(200); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostFavouriteMusic']['200'])
        }
    }
}//swagger response code


module.exports = { payloadValidator, hander, response };