const Joi = require("joi");
const Promise = require('promise');
const local = require('../../../locales');
const ObjectID = require('mongodb').ObjectID;
const userListCollection = require("../../../models/userList");

const hander = (req, res) => {
    let userId = req.auth.credentials._id;
    const getMyData = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "_id": ObjectID(userId) } },
                { "$unwind": "$favouriteMusic" },
                { "$lookup": { "from": "dublyAudio", "localField": "favouriteMusic", "foreignField": "_id", "as": "favouriteMusic" } },
                { "$project": { "favouriteMusic": 1 } }, { "$unwind": "$favouriteMusic" },
                {
                    "$group": {
                        "_id": null,
                        "favouriteMusic": { $push: "$favouriteMusic" }
                    }
                }

            ];
            userListCollection.Aggregate(condition, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d && d.length) {

                    d[0]["favouriteMusic"].forEach(element => {
                        element["isFavourite"] = 1;
                    });
                    return resolve({ message: req.i18n.__('GetFavouriteMusic')['200'], data: d[0]["favouriteMusic"] });
                } else {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    };

    getMyData()
        .then((data) => { return res(data).code(200); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetFavouriteMusic']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) }
    }
}//swagger response code


module.exports = { hander, response };