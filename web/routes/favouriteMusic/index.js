let headerValidator = require("../../middleware/validator")
let PostAPI = require('./Post');
let GetAPI = require('./Get');
// let DeleteAPI = require('./Delete');
module.exports = [
    // {
    //     method: 'Delete',
    //     path: '/favouriteMusic',
    //     handler: DeleteAPI.hander,
    //     config: {
    //         description: `This API use for signup or login from facebook with facebook token`,
    //         tags: ['api', 'Music'],
    //         auth: "user",
    //         validate: {
    //             headers: headerValidator.headerAuthValidator,
    //             payload: DeleteAPI.payloadValidator,
    //             failAction: (req, reply, source, error) => {
    //                 failAction: headerValidator.faildAction(req, reply, source, error)
    //             }
    //         },
    //         response: DeleteAPI.response
    //     }
    // },
    {
        method: 'POST',
        path: '/favouriteMusic',
        handler: PostAPI.hander,
        config: {
            description: `This API use for signup or login from facebook with facebook token`,
            tags: ['api', 'Music'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
    {
        method: 'GET',
        path: '/favouriteMusic',
        handler: GetAPI.hander,
        config: {
            description: `This API use for signup or login from facebook with facebook token`,
            tags: ['api', 'Music'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];