const Joi = require("joi");
const Promise = require('promise');
const local = require('../../../locales');
const ObjectID = require('mongodb').ObjectID;
const userListCollection = require("../../../models/userList");

const payloadValidator = Joi.object({
    musicId: Joi.string().required().description('musicId'),
}).required();


const hander = (req, res) => {
    let userId = req.auth.credentials._id;
    let musicId = ObjectID(req.payload.musicId);

    const setMyData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.UpdateByIdWithPull({ _id: userId }, { favouriteMusic: musicId }, (e) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                }
                return resolve({ message: req.i18n.__('PostFavouriteMusic')['200'] });

            })
        });
    };

    setMyData()
        .then((data) => { return res(data).code(200); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['PostFavouriteMusic']['200'])
        }
    }
}//swagger response code


module.exports = { payloadValidator, hander, response };