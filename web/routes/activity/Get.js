'use strict'
//require npm modules
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

//require dependency
const local = require('../../../locales');
const followCollection = require("../../../models/follow");
const notificationCollection = require("../../../models/notification");

//validator for this API
const validator = Joi.object({
    offset: Joi.string().default("0").example("0").description("offset, query parameter"),
    limit: Joi.string().default("20").example("20").description("limit, query parameter")
}).unknown();


/**
 * @method GET activity
 * @description This API returns user's activity
 * 
 * @headers {string} authorization : token
 * @headers {string} lang by defualt en
 * 
 * @param {string} offset by default 0
 * @param {string} limit  by default 20
 * 
 * @author DipenAhir
 * @since 09-July-2019
 */

//start API hender
const hander = (req, res) => {

    //API variables
    let userId = ObjectID(req.auth.credentials._id);
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;
    let myFollowers = [];

    /** This function returns all my followers */
    const getMyFollowers = () => {
        return new Promise((resolve, reject) => {
            //condition to get all my follers list
            let condition = { "follower": userId, end: false, "type.status": 1 };
            //Get all followers
            followCollection.Select(condition, (err, result) => {
                if (err) {
                    let responseObj = { code: 500, message: 'internal server error' };
                    return reject(responseObj);
                } else {
                    result.forEach(function (element) {
                        //push all my followers to "myFollowers" array
                        myFollowers.push(element["followee"].toString());
                    }, this);
                    return resolve(true);
                }
            });
        });
    };

    /**
     * This function return all my activity
     */
    const getActivity = () => {
        return new Promise((resolve, reject) => {

            //condition to get all my activity list
            let condition = [
                { '$match': { "to": userId, "end": false } },
                { "$lookup": { "from": "customer", "localField": "from", "foreignField": "_id", "as": "customer" } },
                { "$unwind": "$userList" },
                { '$match': { "userList.userStatus": 1 } },
                { "$lookup": { "from": "posts", "localField": "postId", "foreignField": "_id", "as": "postData" } },
                { "$unwind": { "path": "$postData", "preserveNullAndEmptyArrays": true } },
                { "$lookup": { "from": "customer", "localField": "postData.userId", "foreignField": "_id", "as": "postUserData" } },
                { "$unwind": { "path": "$postUserData", "preserveNullAndEmptyArrays": true } },
                {
                    "$project": {
                        "_id": 1, "timeStamp": "$start", "type": "$type.status", "message": "$type.message",
                        "userId": "$userList._id", "userName": "$userList.userName", "profilePic": "$userList.profilePic",
                        "postData": {
                            "postId": "$postData._id", "imageUrl1": "$postData.imageUrl1", "thumbnailUrl1": "$postData.thumbnailUrl1",
                            "imageUrl1Width": "$postData.imageUrl1Width", "imageUrl1Height": "$postData.imageUrl1Height",
                            "mediaType1": "$postData.mediaType1", "createdOn": "$postData.createdOn", "timeStamp": "$postData.timeStamp",
                            "title": "$postData.title", "categoryId": "$postData.categoryId", "channelId": "$postData.categoryId",
                            "phoneNumber": "$postData.mobile", "hashTags": { "$ifNull": ["$postData.hashTags", []] }, "likesCount": { '$ifNull': ['$postData.likesCount', 0] },
                            "profilepic": '$userData.profilePic',
                            "comments": { "$ifNull": ['$postData.comments', []] },
                            "totalComments": { "$size": { '$ifNull': ['$postData.comments', []] } },
                            "distinctViews": { "$size": { '$ifNull': ['$postData.distinctViews', []] } },
                            "location": { '$ifNull': ['$postData.location', ""] }, "place": { '$ifNull': ['$postData.place', ""] },
                            "city": { "$ifNull": ['$postData.city', ""] },
                            "userId": "$postUserData._id",
                            "userName": "$postUserData.userName", "profilePic": "$postUserData.profilePic"
                        }
                    }
                },
                { "$sort": { "timeStamp": -1 } },
                { "$skip": offset }, { "$limit": limit }
            ];


            notificationCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    //return 500 responce : if have any issues in Aggregate function
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result.length === 0) {
                    //return 204 responce : if data not found in db
                    return reject({ message: req.i18n.__('GetActivity')['204'], code: 204 });
                } else {
                    //change in "message", "am_I_Following" fields and  send sucess responce
                    result.forEach(function (element) {
                        element["firstName"] = (element["firstName"]) ? element["firstName"] : element["userName"]
                        element["lastName"] = (element["lastName"]) ? element["lastName"] : ""
                        switch (element.type) {
                            case 2: {
                                //if user liked my post
                                element["message"] = " liked your post";
                                break;
                            }
                            case 3: {
                                //if someone/user mention me in post or comment
                                delete element["postData"];
                                element["message"] += "you."
                                element["am_I_Following"] = myFollowers.includes(element["userId"].toString());
                                break;
                            }
                            case 5: {
                                //if someone/user put comment on my post
                                element["message"] += " on your post";
                                break;
                            }
                        }
                    }, this);

                    //return sucess responce
                    let responseObj = { code: 200, message: req.i18n.__('GetActivity')['200'], data: result };
                    return resolve(responseObj);
                }
            });
        });
    };

    getMyFollowers()
        .then(getActivity)
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};//end API hender

//start API response
const response = {
    status: {
        200: {
            message: Joi.string().default(local['GetActivity']['200']).required().example("Data Found successfully"),
            data: Joi.array().required().items({
                _id: Joi.any().example("5e0c45bd9bd027e35abf520a").required(),
                timeStamp: Joi.number().example(1577862589943).required(),
                type: Joi.number().example(3).required(),
                message: Joi.string().example("started following you.").required(),
                userId: Joi.any().example("5e0c45bd9bd027e35abf520a").required(),
                userName: Joi.string().example("telugurockers").required(),
                profilePic: Joi.string().example("https://s3.ap-south-1.amazonaws.com/dubly/profile_image/5e0c4099a8ad715033b122a7").required(),
                firstName: Joi.string().example("telugurockers").required(),
                lastName: Joi.string().example("telugurockers").allow(["", null]),
                am_I_Following: Joi.boolean().example(false).default(false),
                postData: Joi.any()
            }).example([
                {
                    "_id": "5e0c45bd9bd027e35abf520a",
                    "timeStamp": 1577862589943,
                    "type": 3,
                    "message": "started following you.",
                    "userId": "5e0c4099a8ad715033b122a7",
                    "userName": "telugurockers",
                    "profilePic": "https://s3.ap-south-1.amazonaws.com/dubly/profile_image/5e0c4099a8ad715033b122a7",
                    "firstName": "telugurockers",
                    "lastName": "",
                    "am_I_Following": false
                }
            ])
        },
        204: { message: Joi.string().default(local['genericErrMsg']['204']).default("No Content - data not available") },
        400: { message: Joi.string().default(local['genericErrMsg']['400']).required().example("<field> is missing") }
    }
}//end API response

//Export all constance
module.exports = { validator, hander, response };