'use strict'
//require npm modules
const Joi = require("joi");
//require dependency
const local = require('../../../locales');
const categoryollection = require("../../../models/category");

//API params validator
const validator = Joi.object({
    skip: Joi.string().description('skip'),
    limit: Joi.string().description('limit'),
}).required();

//Start API handler
const handler = (req, res) => {

    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;

    categoryollection.SelectWithSort({ categoryStatus: 1, categoryName: { "$nin": ["Star","Live"] } }, { sequenceId: -1 }, {}, offset, limit, (err, data) => {
        if (err) {
              //return 500 responce : if have any issues in Aggregate function
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (data.length === 0) {
            //return 204 responce : if data not found in db
            return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);
        } else {
            //return sucess responce
            return res({ message: req.i18n.__('GetCategory')['200'], data: data }).code(200);
        }
    });
};//End API handler

const response = {
    status: {
        200: { message: Joi.any().default(local['GetCategory']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { validator, handler, response };