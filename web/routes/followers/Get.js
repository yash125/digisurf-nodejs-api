const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const followCollection = require("../../../models/follow");

const queryValidator = Joi.object({
    skip: Joi.string().description('skip'),
    userId: Joi.string().required().description("userId"),
    limit: Joi.string().description('limit'),
}).unknown();

const handler = (req, res) => {
    let _id = req.auth.credentials._id;
    let myFollowers = {};

    let userId = ObjectID(req.query.userId);
    const offset = parseInt(req.query.skip) || 0;
    const limit = parseInt(req.query.limit) || 20;

    const getMyFollowers = () => {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(_id) };
            followCollection.Select(condition, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else {
                    for (let index = 0; index < result.length; index++) {
                        myFollowers[result[index]["followee"]] = result[index]["type"];
                    }
                    return resolve("--");
                }
            });
        });
    }

    const followers = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "followee": userId, "end": false, "type.status": 1 } },
                { "$match": { "follower": { "$ne": userId } } },
                { "$lookup": { "from": 'customer', "localField": 'follower', "foreignField": '_id', "as": 'user' } },
                { "$unwind": "$user" },
                { "$match": { "user.userStatus": 1 } },
                {
                    "$project": {
                        "_id": 1, "followee": 1, "follower": 1, "start": 1, "end": 1, "type": 1, "private": '$user.private', "followsBack": 1,
                        "profilePic": '$user.profilePic', "userName": '$user.userName', "businessProfile": '$user.businessProfile',
                        "firstName": "$user.firstName", "lastName": "$user.lastName",
                        "userId": '$user._id'
                    }
                },
                { "$sort": { "start": -1 } }, { "$skip": offset }, { "$limit": limit }
            ];
            followCollection.Aggregate(condition, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {

                    d.forEach(function (element) {

                        element["type"] = (myFollowers[element["follower"]]) ? myFollowers[element["follower"]] : { "status": 0, "message": "unfollowed" };
                        element["firstName"] = (element["firstName"] != "null" && element["firstName"] != null && typeof element["firstName"] != 'undefined') ? element["firstName"] : "";
                        element["lastName"] = (element["lastName"] != "null" && element["lastName"] != null && typeof element["lastName"] != 'undefined') ? element["lastName"] : "";

                    }, this);

                    return resolve({ code: 200, message: req.i18n.__('GetFollowers')['200'], data: d });
                }
            });
        });
    };

    getMyFollowers()
        .then(() => { return followers() })
        .then((data) => {
            return res({ message: data.message, data: data.data }).code(data.code);
        })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetFollowers']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code


module.exports = { queryValidator, handler, response };