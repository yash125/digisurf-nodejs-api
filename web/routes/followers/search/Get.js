'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const followCollection = require("../../../../models/follow");

const validator = Joi.object({
    "offset": Joi.string().description('skip'),
    "limit": Joi.string().description('limit'),
    "userId": Joi.string().required().min(24).max(24).description('userId ').error(new Error("userId is missing or invalid.")),
    "searchText": Joi.string().required().min(3).description('Search text').error(new Error("searchText is missing or invalid please enter min 3 characters.")),
}).unknown();


const handler = (req, res) => {
    let userId = ObjectID(req.query.userId);
    let searchText = req.query.searchText;

    const offset = parseInt(req.query.offset) || 0;
    const limit = parseInt(req.query.limit) || 20;

    const followees = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "followee": userId, "end": false, "type.status": 1 } },
                { "$match": { "follower": { "$ne": userId } } },
                { "$lookup": { "from": 'customer', "localField": 'follower', "foreignField": '_id', "as": 'user' } },
                { "$unwind": "$user" },
                { "$match": { "user.userStatus": 1 } },
                {
                    "$project": {
                        "_id": 1, "followee": 1, "follower": 1, "start": 1, "end": 1, "type": 1, "profilePic": '$user.profilePic',
                        "userName": '$user.userName', "businessProfile": '$user.businessProfile', "private": '$user.private',
                        "followsBack": 1, "firstName": "$user.firstName", "lastName": "$user.lastName",
                        "fullName": { "$concat": ["$user.firstName", "$user.lastName"] },
                        "fullWithSpaceName": { "$concat": ["$user.firstName", " ", "$user.lastName"] },
                        "userId": '$user._id'

                    }
                },
                {
                    "$match": {
                        "userName": { "$exists": true },
                        "$or": [
                            { "userName": { "$regex": searchText, "$options": "gi" } },
                            { "firstName": { "$regex": searchText, "$options": "gi" } },
                            { "lastName": { "$regex": searchText, "$options": "gi" } },
                            { "fullName": { "$regex": searchText, "$options": "gi" } },
                            { "fullWithSpaceName": { "$regex": searchText, "$options": "gi" } }
                        ]

                    }
                },
                { "$sort": { "start": -1 } },
                { "$skip": offset }, { "$limit": limit }
            ];

            followCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else if (result && result.length) {
                    result.forEach(function (element) {
                        element["type"] = { "status": 1, "message": "following" };
                        element["firstName"] = (element["firstName"] != "null" && element["firstName"] != null && typeof element["firstName"] != 'undefined') ? element["firstName"] : "";
                        element["lastName"] = (element["lastName"] != "null" && element["lastName"] != null && typeof element["lastName"] != 'undefined') ? element["lastName"] : "";
                    }, this);
                    return resolve({ code: 200, message: local['GetFolloeesSearch']['200'], data: result });
                } else {
                    return reject({ code: 204, message: local['GetFolloeesSearch']['204'] });
                }
            });
        });
    };

    followees()
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetFollowersSearch']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['GetFollowersSearch']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code

module.exports = { validator, handler, response };