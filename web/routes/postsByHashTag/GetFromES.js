const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const postESCollection = require("../../../models/postES");
const hashTags = require("../../../models/hashTags");
const userListCollection = require("../../../models/userList");
/**
 * @description API to update a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} userId
 * @property {string} imageUrl1
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const validator = Joi.object({
    hashTag: Joi.string().required().description('hashTag'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();

const handler = (req, res) => {

    let hashTag = (req.query.hashTag) ? req.query.hashTag.trim().toLowerCase() : "";
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    let userId = req.auth.credentials._id;


    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }
    function getPost() {
        return new Promise((resolve, reject) => {
            let condition = {
                "must": [
                    { "term": { "postStatus": 1 } },
                    { "match": { "hashTags": hashTag } },
                    // { "match": { "isPrivate": 0 } }
                ],
                "filter": {
                    "bool": {
                        "should": [
                            { "term": { "isPrivate": 0 } },
                            { "match": { "userId": req.auth.credentials._id } }
                        ]
                    }
                }
            };

            postESCollection.getPostDetailsWithTotalNumbers(condition, offset, limit, userData.follow, userId, (err, result) => {
                if (err) {
                    console.log("error ", err)
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {

                    result.result.forEach(element => {
                        if (element["channelId"] && element["channelId"].length) element["isChannelSubscribed"] = (userData && userData.subscribeChannels && userData.subscribeChannels.map(e => e.toString()).includes(element.channelId.toString())) ? 1 : 0;
                    });

                    hashTags.SelectOne({ _id: "#" + hashTag }, (e, r) => {


                        let responseobj = {
                            code: 200,
                            data: result.result,
                            totalPosts: result.total,
                            image: (r && r.image) ? r.image : "",
                            message: req.i18n.__('GetPostsByHashTag')['200']
                        };
                        return resolve(responseobj);
                    })
                }
            });
        });
    }
    getUserData()
        .then(() => { return getPost() })
        .then((data) => {
            return res({
                data: data.data,
                image: data.image,
                message: data.message,
                totalPosts: data.totalPosts
            }).code(data.code);
        })
        .catch((error) => { return res({ message: error.message }).code(error.code); });


};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetPostsByHashTag']['200']), data: Joi.any(), totalPosts: Joi.any(), image: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { validator, handler, response };