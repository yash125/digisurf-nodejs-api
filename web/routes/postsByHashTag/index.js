let headerValidator = require("../../middleware/validator")
let GetAPI = require('./GetFromES'); //require('./Get');

module.exports = [
    {
        method: 'Get',
        path: '/postsByHashTag',
        handler: GetAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'post'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];