let headerValidator = require("../../middleware/validator")
let PostAPI = require('./Post');

module.exports = [
   
    {
        method: 'Post',
        path: '/forgotPassword',
        handler: PostAPI.handler,
        config: {
            description: `This API used to for reset password`,
            tags: ['api', 'passwordReset'],
            auth: 'basic',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    }
];