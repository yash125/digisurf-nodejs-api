const Joi = require("joi");
const Promise = require('promise');

const local = require('../../../locales');
const userCollection = require("../../../models/userList");
const resetPasswordCollection = require('../../../models/verifyResetPassword');

const validator = Joi.object({
    email: Joi.string().required().description("enter a valid email id"),
    phone: Joi.string().required().description("enter a valid phone number"),
}).required();

let handler = (req, res) => {

    let email = req.payload.email;
    let phone = req.payload.phone;

    verifyEmailPhone()
        .then((result) => {
            return genVerificationCode(result);
        }).then((value) => {
            return sendEmail(value);
        })


    function verifyEmailPhone() {
        return new Promise((resolve) => {
            userCollection.SelectOne({ "email.id": email, "number": phone }, (err, result) => {
                if (err) {
                    return res({ message: local['genericErrMsg']['500'] }).code(500);
                } else {
                    if (!result) {
                        return res({ message: local['forgotPassword']['404'] }).code(404);
                    } else {
                        resolve({ email: result.email, firstName: result.firstName })
                    }
                }
            })

        })
    }

    function genVerificationCode(value) {
        return new Promise((resolve) => {
            let code = Math.floor(1000 + Math.random() * 9000);
            value.code = code;
            value.timeStamp = new Date().getTime();
            value.expireTime = new Date().getTime() + (10 * 60 * 1000);
            value.email = email;
            resetPasswordCollection.Insert(value, (err, result) => {
                if (err) {
                    
                    return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
                }
                else
                    resolve(result.ops[0])
            })
        })
    }

    function sendEmail(value) {
        var gRPC_Email_service = require("../../../library/gRPC/Email");
        gRPC_Email_service.sendEmail({
            userName: value.userName,
            trigger: "Email Verification",
            to: email,
            subject: "Verification code for " + process.env.APP_NAME,
            body: '<p><br/> Hello ' + value.userName + ', <br/><br/> Your verification code is 1111 <br/><br/> Please enter this on the app and set a new password. <br/><br/><br/>Cheers, <br/> Team ' + process.env.APP_NAME + '.</p>'
        })
        // body: '<br/> Hello ' + value.firstName + ',' + '<br/><br/> Your verification code is ' + value.code + '.' + '<br/><br/> Please enter this on the app and set a new password. <br/><br/><br/>Cheers, <br/> Team '+process.env.APP_NAME+'.</p>'
        // let data = {
        //     toEmail: email,
        //     subject: ' Verification code for '+process.env.APP_NAME,
        //     body: '<br/> Hello ' + value.firstName + ',' + '<br/><br/> Your verification code is ' + value.code + '.' + '<br/><br/> Please enter this on the app and set a new password. <br/><br/><br/>Cheers, <br/> Team '+process.env.APP_NAME+'.</p>'
        // }
        return res({ message: req.i18n.__('forgotPassword')['200'] }).code(200);
        // console.log(value);
        // request.post({
        //     headers: { 'content-type': 'application/json' },
        //     url: `http://13.233.24.146:5000/sendEmail`,
        //     body: data,
        //     json: true
        // }, function (error, response, body) {

        //     if (error)
        //         console.log('0000', error);
        //     else {
        //         console.log(body);
        //         return res({ message: req.i18n.__('forgotPassword')['200'] }).code(200);
        //     }
        // });
    }


};

const response = {
    status: {
        200: { message: Joi.any().default(local['forgotPassword']['200']) },
        404: { message: Joi.any().default(local['forgotPassword']['404']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },

    }
};//swagger response code

module.exports = { handler, response, validator }