'use strict'
/** initialize  variables for this API  */
const Joi = require("joi");
Joi.objectId = require('joi-objectid')(Joi);
const logger = require('winston');
const Promise = require('bluebird');
const collectionDb = Promise.promisifyAll(require('../../../models/collection'));
const postDb = Promise.promisifyAll(require('../../../models/posts'));
const userListCollection = require('../../../models/userList');
const ObjectID = require('mongodb').ObjectID;

const check = require('./../../../library/checkEmpty');

const local = require('../../../locales');


/** query Validators */
const queryValidator = Joi.object({
    offset: Joi.string().default("0").description("pagination offset"),
    limit: Joi.string().default("20").description("pagination limit"),
    collectionId: Joi.objectId().required().description("Id of the collection ").error(new Error("collectionId is missing or invalid"))
}).unknown()

/** params validator */
const paramsValidator = Joi.object({
    collectionId: Joi.objectId().required().description("Id of the collection ").error(new Error("collectionId is missing or invalid"))
})
/**
 * @method  GET AllBookmark
 * @description This api is used to get all bookmarked posts
 * 
 *  @property {string} offset
 *  @property {string} limit
 * @returns 200 : data fetched successfully
 * @returns 500 : Internal Server Error
 * 
 * @author Karthikb@mobifyi.com
 * @since 20-08-2019
 */


/**
    * get all posts that are not bookmarked in collection already
    */


/** start API Handler */
const hander = (req, res) => {

    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }


    /**get posts from the given collectionId */
    let getCollectionPost = async () => {
        try {
            let result = await collectionDb.SelectAsync({ _id: ObjectID(req.query.collectionId) })
            // logger.info('details here',result);
            return result.postIds;
        } catch (err) {
            logger.error('error caught', err);
            return { message: req.i18n.__('genericErrMsg')['500'], code: 500 }
        }
    }

    /**this will find all posts that are bookmarked but not in postIds */
    let getIsAddedToCollection = (postIds) => {
        return new Promise((resolve, reject) => {
            try {
                let postId = postIds.map(e => JSON.stringify(e))
                let notInCollection = userData.bookmarks.filter(e => !postId.includes(JSON.stringify(e)));

                resolve(notInCollection)
            } catch (error) {
                reject(error)
            }
        });
    }

    let getPostsDetail = async (postsNotInCollection) => {

        if (check.isEmpty(postsNotInCollection))
            return res({ message: req.i18n.__('getCollection')['204'] }).code(204);
        else {
            let detail = await postDb.SelectWithFilterAsync({ _id: { "$in": postsNotInCollection } }, { imageUrl1: 1 }, offset, limit)
            return res({
                message: req.i18n.__('getCollection')['200'], data: detail.map(e => {
                    return {
                        _id: e._id,
                        thumbnailUrl1: e.imageUrl1,
                        imageUrl1: e.imageUrl1
                    }
                })
            }).code(200);
        }
    }

    getUserData()
        .then(getCollectionPost)
        .then(getIsAddedToCollection)
        .then(getPostsDetail)
        .catch((err) => res({ message: err.message }).code(err.code))


};/** End API Handler */

const response = {
    status: {
        200: { message: Joi.string().default(local['getCollection']['200']).example(local['getCollection']['200']).required(),
         data: Joi.any() },
        204: { message: Joi.string().default(local['getCollection']['204']).example(local['getCollection']['204']) },
        500: { message: Joi.string().default(local['genericErrMsg']['500']).example(local['genericErrMsg']['500']) }
    }
}//swagger response code


module.exports = { hander, response, queryValidator, paramsValidator };