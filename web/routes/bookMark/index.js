let headerValidator = require("../../middleware/validator")
let postAPI = require('./Post');
let getAllAPI = require('./Get');
let deleteAPI = require('./Delete');
let GetByIdAPI = require('./GetById');
let statusCheckAPI = require('./status');
module.exports = [
    {
        method: 'POST',
        path: '/bookmark',
        handler: postAPI.hander,
        config: {
            description: `This API is used to add posts bookmark(it is added to AllPost collection)`,
            tags: ['api', 'bookmark'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: postAPI.response
        }
    },
    {
        method: 'GET',
        path: '/bookmark',
        handler: getAllAPI.hander,
        config: {
            description: `This API is used to get all bookmarks of a user`,
            tags: ['api', 'bookmark'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: getAllAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: getAllAPI.response
        }
    },
    {
        method: 'DELETE',
        path: '/bookmark',
        handler: deleteAPI.hander,
        config: {
            description: `This API is used to delete bookmark`,
            tags: ['api', 'bookmark'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: deleteAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: deleteAPI.response
        }
    },
    {
        method: 'GET',
        path: '/bookmarkById',
        handler: GetByIdAPI.hander,
        config: {
            description: `This API is used to get all bookmarked posts that are not in that collection`,
            tags: ['api', 'bookmark'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetByIdAPI.queryValidator,
                // params: GetByIdAPI.paramsValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByIdAPI.response
        }
    }
].concat(statusCheckAPI);