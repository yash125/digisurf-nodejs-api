'use strict'
/** initialize  variables for this API  */
const Joi = require("joi");
Joi.objectId = require('joi-objectid')(Joi);
const logger = require('winston');
var ObjectID = require('mongodb').ObjectID
const Promise = require('bluebird');
const collectionDb = Promise.promisifyAll(require('../../../models/collection'));
const userListDb = Promise.promisifyAll(require('../../../models/userList'));

const local = require('../../../locales');



/** query Validators */
const queryValidator = Joi.object({
    postId: Joi.objectId().example("5dd7c91e162b8c758c2ad44c").required().description("Id of the post that you want to delete as bookmarked").error(new Error("postId is missing or invalid")),
}).unknown()

/**
 * @method  DELETE Delete bookmark 
 * @description This API is used delete bookmarked posts
 * 
 * @property {string} collectionId
 * 
 * @returns 200 : Success
 * @returns 400 : Parameters missing
 * @returns 500 : Internal Server Error
 * 
 * @author Karthikb@mobifyi.com
 * @since 05-july-2019
 */

/** start API Handler */
const hander = async (req, res) => {

    /** delete from collection as well as userList */
    try {
        let condition = {
            _id: ObjectID(req.auth.credentials._id)
        }
        let data = ObjectID(req.query.postId);

        let result = await userListDb.DeleteFromArrayAsync(condition, data);

        /**if successfully deleted then delete it from collection as well */
        if (result.result.nModified == 1) {

            let newCondtion = {
                userId: ObjectID(req.auth.credentials._id)
            }
            let newData = ObjectID(req.query.postId);

            collectionDb.DeleteFromArrayAsync(newCondtion, newData);
            return res({ message: req.i18n.__('deleteBookmark')['200'] }).code(200);

        }
        else
            return res({}).code(204);
    } catch (err) {
        logger.error('delete bookmark error', err);
        return res({ message: req.i18n.__('deleteBookmark')['500'] }).code(500);
    }
};/** End API Handler */

const response = {
    status: {
        200: { message: Joi.string().default(local['deleteBookmark']['200']).example(local['deleteBookmark']['200']).required() },
        204: { message: Joi.string().default(local['deleteBookmark']['204']).example(local['deleteBookmark']['204']) },
        500: { message: Joi.string().default(local['genericErrMsg']['500']).example(local['genericErrMsg']['500']) }
    }
}//swagger response code


module.exports = { hander, response, queryValidator };