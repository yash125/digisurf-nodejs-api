'use strict'
/** initialize  variables for this API  */
const Joi = require("joi");
Joi.objectId = require('joi-objectid')(Joi);
const logger = require('winston');
const Promise = require('bluebird');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const check = require('./../../../library/checkEmpty');
const postDb = Promise.promisifyAll(require('../../../models/posts'));
const userList = Promise.promisifyAll(require('../../../models/userList'));

/** query Validators */
const queryValidator = Joi.object({
    offset: Joi.string().default("0").description("pagination offset"),
    limit: Joi.string().default("20").description("pagination limit")
}).unknown()

/**
 * @method  GET AllBookmark
 * @description This api is used to get all bookmarked posts
 * 
 *  @property {string} offset
 *  @property {string} limit
 * @returns 200 : data fetched successfully
 * @returns 500 : Internal Server Error
 * 
 * @author Karthikb@mobifyi.com
 * @since 08-july-2019
 */

/** start API Handler */
const hander = async (req, res) => {
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;
    /** 
     * fetch data from database 
     */
    try {
        const userData = await userList.SelectOneAsync({ "_id": ObjectID(req.auth.credentials._id) });
        /** if bookmark exists */
        if (userData && userData.bookmarks && userData.bookmarks.length) {
            /**get all bookmarks of the user*/
            let condition = {
                postStatus: 1,
                _id: { $in: userData.bookmarks }
            }
            const result = await postDb.SelectWithFilterAsync(condition, { thumbnailUrl1: 1 }, offset, limit);

            if (!check.isEmpty(result))
                return res({ message: req.i18n.__('getCollection')['200'], data: result }).code(200);
            else
                return res({}).code(204);
        }
        else {
            return res({}).code(204);
        }

    }
    catch (err) {
        logger.error('get collection api error', err);
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }

};/** End API Handler */

const response = {
    status: {
        200: {
            message: Joi.string().default(local['getCollection']['200']).example(local['getCollection']['200']).required(),
            data: Joi.array()
                .items({
                    _id: Joi.any().example("5e0f3c292e267f020a58fd37").required(),
                    thumbnailUrl1: Joi.string().example("http://res.cloudinary.com/devsqxnlx/image/upload/v1578056744/20200103063540PM.jpg").required(),
                })
                .example([
                    {
                        "_id": "5e0f3c292e267f020a58fd37",
                        "thumbnailUrl1": "http://res.cloudinary.com/devsqxnlx/image/upload/v1578056744/20200103063540PM.jpg"
                    }
                ])
        },
        204: { message: Joi.string().default(local['getCollection']['204']).example(local['getCollection']['204']) },
        500: { message: Joi.string().default(local['genericErrMsg']['500']).example(local['genericErrMsg']['500']) }
    }
}//swagger response code


module.exports = { hander, response, queryValidator };