'use strict'
/** initialize  variables for this API  */
const Joi = require("joi");
Joi.objectId = require('joi-objectid')(Joi);
const logger = require('winston');
var ObjectID = require('mongodb').ObjectID
const Promise = require('bluebird');
const userListDb = Promise.promisifyAll(require('../../../models/userList'));
const postDb = Promise.promisifyAll(require('../../../models/posts'));
const check = require('../../../library/checkEmpty');

const local = require('../../../locales');



/** query Validators */
const payloadValidator = Joi.object({
    postId: Joi.objectId().required().description("postId example:[\"5d1603176948965ac930ff6b\",\"5d1305459bdd292b1aaba82b\"]").error(new Error("postId is missing or invalid"))
}).unknown()

/**
 * @method  POST bookmark 
 * @description This API is used to add bookmark
 * 
 * @property {string} postId
 * 
 * @returns 200 : Success
 * @returns 400 : Parameters missing
 * @returns 204 : userId not found
 * @returns 500 : Internal Server Error
 * 
 * @author Karthikb@mobifyi.com
 * @since 08-july-2019
 */



/** start API Handler */
const hander = (req, res) => {

    const postId = ObjectID(req.payload.postId);

    const checkIfPostIdIsValid = async () => {
        try {
            let result = await postDb.SelectByIdAsync({ _id: postId }, { _id: 1 })
            if (result && result._id) return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);

            return res({ message: req.i18n.__('bookMark')['422'] }).code(422);
        } catch (error) {
            logger.error('post bookmark error', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }
    }

    const postBookmark = async () => {
        try {
            /**_id must match the given collectionId */
            let condition = { _id: req.auth.credentials._id }
            const result = await userListDb.UpdateByIdWithAddToSetAsync(condition, { "bookmarks": postId });

            /**if postId is already bookmarked it is not updated and 409 is sent back */
            if (result.result.n == 1 && result.result.nModified == 1)
                return res({ message: req.i18n.__('bookMark')['200'] }).code(200);
            else
                return res({ message: req.i18n.__('bookMark')['409'] }).code(409);
        }
        catch (err) {
            logger.error('post bookmark error', err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }
    }

    // checkIfPostIdIsValid()
    //     .then(postBookmark)
    postBookmark()

};

const response = {
    status: {
        200: { message: Joi.string().default(local['bookMark']['200']).example(local['bookMark']['200']).required() },
        409: { message: Joi.string().default(local['bookMark']['409']).example(local['bookMark']['409']).required() },
        422: { message: Joi.string().default(local['bookMark']['422']).example(local['bookMark']['422']).required() },
        500: { message: Joi.string().default(local['genericErrMsg']['500']).example(local['bookMark']['500']) }
    }
}//swagger response code


module.exports = { hander, response, payloadValidator };