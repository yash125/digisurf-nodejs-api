const statusAPI = require('./Get');
let headerValidator = require("../../../middleware/validator")

module.exports=[{
    method: 'GET',
    path: '/bookmarkStatus',
    handler: statusAPI.hander,
    config: {
        description: `This API is used to check if the post is in any collection or not`,
        tags: ['api', 'bookmark'],
        auth: "user",
        validate: {
            headers: headerValidator.headerAuthValidator,
            query:statusAPI.queryValidator,
            failAction: (req, reply, source, error) => {
                headerValidator.faildAction(req, reply, source, error)
            }
        },
        response: statusAPI.response
    }
}
]