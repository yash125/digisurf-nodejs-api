'use strict'
/** initialize  variables for this API  */
const Joi = require("joi");
Joi.objectId = require('joi-objectid')(Joi);
const logger = require('winston');
var ObjectID = require('mongodb').ObjectID
const Promise = require('bluebird');
const collectionDb = Promise.promisifyAll(require('../../../../models/collection'));
const check = require('../../../../library/checkEmpty');
const local = require('../../../../locales');


/** query Validators */
const queryValidator = Joi.object({
    postId: Joi.objectId().required().example("5dd7c91e162b8c758c2ad44c").description("post Id of the post to be unbookmarked").error(new Error("postId is missing or invalid"))
}).unknown()

/**
 * @method  GET BookmarkStatus 
 * @description This API is used to check if the post is in any collection or not
 * 
 *  @property {string} postId
 * @returns 200 : data fetched successfully
 * @returns 500 : Internal Server Error
 * 
 * @author Karthikb@mobifyi.com
 * @since 08-july-2019
 */



/** start API Handler */
const hander = async (req, res) => {

    let postId = req.query.postId;

    /** 
     * fetch data from database 
     * */

    try {
        /**get collection of the user*/
        let condition = {
            userId: ObjectID(req.auth.credentials._id),
            postIds: { $eq: ObjectID(postId) }
        }
        const result = await collectionDb.SelectAsync(condition);
        // result ? status = 1 : status = 0
        if (!check.isEmpty(result))
            return res({ message: req.i18n.__('bookmarkStatus')['200'] }).code(200);
        else
            return res({}).code(204);
    }
    catch (err) {
        logger.error('get collection api error', err);
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }
};/** End API Handler */

const response = {
    status: {
        200: { message: Joi.string().default(local['bookmarkStatus']['200']).example(local['bookmarkStatus']['200']).required() },
        204: { message: Joi.string().default(local['bookmarkStatus']['204']).example(local['bookmarkStatus']['204']) },
        500: { message: Joi.string().default(local['genericErrMsg']['500']).example(local['genericErrMsg']['500']) }
    }
}//swagger response code


module.exports = { hander, response, queryValidator };