const del = require('./delete');
const post = require('./post');
const get = require('./get');
const accessToken = require('./accessToken');

module.exports = [].concat(
    del,
    post,
    get,
     accessToken
);