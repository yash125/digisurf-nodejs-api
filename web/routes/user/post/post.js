'use strict';

const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);

const auth = require('../../../middleware/authentication');
const locales = require('../../../../locales');

const payload = joi.object({
    userId: joi.objectId().required().default('5a55ac46cad8ff011ab61a8d').description('Mongo Id of user'),
    userType: joi.string().required().default('admin').description('user type like customer, admin, provider'),
    deviceId: joi.string().required().default("111111111").description('Device Id'),
    deviceType: joi.string().required().default("IOS").allow(["IOS", "Android", "Web"]).description('Android, IOS, Web'),
    deviceMake: joi.string().required().default("Apple").description('Device Make'),
    deviceModel: joi.string().required().default("iPhone 6S").description('Device Model')
}).required();

const handler = (req, reply) => {
    const dbErrResponse = { message: req.i18n.__('genericErrMsg')['500'], code: 500 };

    auth.generateTokens(req.payload)
        .then((responseData) => {
            return reply({ message: req.i18n.__('genericErrMsg')['200'], data: responseData }).code(200);
        }).catch((err) => {
            console.log(err);
            return reply({ message: dbErrResponse.message }).code(dbErrResponse.code);
        });
};

const response = {
    status: {
        200: {
            message: joi.any().default(locales['genericErrMsg']['200']),
            data: joi.any()
        },
        500: {
            message: joi.any().default(locales['genericErrMsg']['500'])
        }
    }
}//swagger response code

module.exports = { payload, handler, response };