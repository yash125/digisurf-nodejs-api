
'use strict';

const entity = "user";
const api = require('./delete');
const header = require('../../../../middleware/validator');
module.exports = [
    /**
    * 'Stripe webhook for handling an event ',
    */
    {
        method: 'DELETE',
        path: '/' + entity,
        handler: api.handler,
        vhost: 'localhost',
        config: {
            tags: ['api', entity],
            // description: i18n.__('apiDescription')['userDelete'],
            // notes: i18n.__('apiDescription')['userDelete'],
            response: api.response,
            validate: {
                headers: header.headerLan,
                payload: api.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
]