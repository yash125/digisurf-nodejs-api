'use strict';

const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);

const auth = require('../../../../middleware/authentication');
const local = require('../../../../../locales');

const payload = joi.object({
    userId: joi.objectId().required().default('5a55ac46cad8ff011ab61a8d').description('Mongo Id of user'),
    userType: joi.string().required().default('admin').description('user type like customer, admin, provider')
}).required();

const handler = (req, reply) => {
    auth.removeRefreshToken(req.payload)
        .then(() => {
            return reply({ message: req.i18n.__('genericErrMsg')['200'] }).code(200);
        }).catch(e => {
            return reply({ message: e.message }).code(e.code);
        });
};

const response = {
    status: {
        200: {
            message: joi.any().default(local['genericErrMsg']['200'])
        },
        404: {
            message: joi.any().default(local['genericErrMsg']['404'])
        },
        500: {
            message: joi.any().default(local['genericErrMsg']['500'])
        }
    }
}//swagger response code

module.exports = { payload, handler, response };