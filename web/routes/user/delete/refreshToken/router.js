
'use strict';

const entity = "user";

const api = require('./delete');
const patch = require('./patch');
// const i18n = require('../../../../../locales');

const header = require('../../../../middleware/validator');
module.exports = [
    {
        method: 'DELETE',
        path: '/' + entity + '/refreshToken',
        handler: api.handler,
        vhost: 'localhost',
        config: {
            tags: ['api', entity],
            // description: i18n.__('apiDescription')['userDeleteRefresh'],
            // notes: i18n.__('apiDescription')['userDeleteRefresh'],
            response: api.response,
            validate: {
                headers: header.headerLan,
                payload: api.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    },
    {
        method: 'PATCH',
        path: '/' + entity + '/refreshToken',
        handler: patch.handler,
        vhost: 'localhost',
        config: {
            tags: ['api', entity],
            // description: i18n.__('apiDescription')['userActivateRefresh'],
            // notes: i18n.__('apiDescription')['userActivateRefresh'],
            response: patch.response,
            validate: {
                headers: header.headerLan,
                payload: patch.payload,
                failAction: (req, reply, source, error) => {
                    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
                }
            }
        }
    }
]