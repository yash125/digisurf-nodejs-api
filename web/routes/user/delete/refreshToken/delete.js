'use strict';

const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);

const auth = require('../../../../middleware/authentication');
const locales = require('../../../../../locales');

const payload = joi.object({
    userId: joi.objectId().required().default('5a55ac46cad8ff011ab61a8d').description('Mongo Id of user'),
    userType: joi.string().required().default('admin').description('user type like customer, admin, provider'),
    refreshToken: joi.string().required().default('e0b59810-429e-4551-8028-2042b99dff7b').description("refresh token,Eg. Key"),
    time: joi.number().required().default(0).description('Blacklist Token for (x) seconds, 0 for permanent')
}).required();

const handler = (req, reply) => {
    auth.blackListRefreshToken(req.payload)
        .then(() => {
            return reply({ message: req.i18n.__('genericErrMsg')['200'] }).code(200);
        }).catch(e => {
            return reply({ message: e.message }).code(e.code);
        });
};

const response = {
    status: {
        200: {
            message: joi.any().default(locales['genericErrMsg']['200'])
        },
        404: {
            message: joi.any().default(locales['genericErrMsg']['404'])
        },
        500: {
            message: joi.any().default(locales['genericErrMsg']['500'])
        }
    }
}//swagger response code

module.exports = { payload, handler, response };