'use strict';

const joi = require('joi');
const locales = require('../../../../locales');
const config = require('../../../../config');

const handler = (data,res) => {


    var options = {
        method: 'POST',
        url: config.auth.server + "/user/login",
        headers: {
            'lan': 'en',
            'content-type': 'application/json'
        },
        body: {
            "appName": config.appName,
            "userId": data.userId,
            "userType": data.userType,
            "multiLogin": config.auth.multiLogin,
            "immediateRevoke": config.auth.immediateRevoke,
            "deviceId": data.deviceId,
            "deviceType": data.deviceType,
            "deviceMake": data.deviceMake,
            "deviceModel": data.deviceModel,
            "accessTokenExpiry": config.auth.accessExpiry,
            "refreshTokenExpiry": config.auth.refreshExpiry
        },
        json: true
    };

    request(options, function (error, response, body) {
        if (error) {
            logger.error("Error while generating access and refresh token", error);
            res(error);
        } else if (response.statusCode == 200) {
            res(body.data);
        } else {
            body.code = response.statusCode;
            return res(body);
        }
    });
};

const response = {
    status: {
        200: {
            message: joi.any().default(locales['genericErrMsg']['200'])
        },
        401: {
            message: joi.any().default(locales['genericErrMsg']['401'])
        },
        406: {
            message: joi.any().default(locales['genericErrMsg']['406'])
        },
        500: {
            message: joi.any().default(locales['genericErrMsg']['500'])
        }
    }
}//swagger response code

module.exports = { handler, response };