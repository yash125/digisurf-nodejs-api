
const joi = require('joi');
joi.objectId = require('joi-objectid')(joi);
const logger = require('winston');
const request = require('request');
const config = require('../../../../../config');

const handler = (req, reply) => {


	var options = {
		method: 'Get',
		url: config.auth.server,
		headers: {
			'lan': 'en',
			'content-type': 'application/json',
			"authorization": req.headers.authorization,
			"refreshtoken": req.headers.refreshtoken
		},
		json: true
	};

	request(options, function (error, response, body) {
		if (error) {
			logger.error("Error while generating access and refresh token", error);
			return reply({ message: "internam server error" }).code(response.statusCode);
		} else if (response.statusCode == 200) {
			
			return reply({ message: body.message, data: body.data }).code(response.statusCode);
		} else {
			return reply({ message: body.message, data: body.data }).code(response.statusCode);
		}
	});
};

const response = {
	status: {
		// 200: {
		// 	message: joi.any().default(locales['genericErrMsg']['200']),
		// 	data: joi.object({
		// 		accessToken: joi.string().required().default('fdfsaf-fdasf-fdsaf-fasdf').description('Refresh Token'),
		// 		accessExpiry: joi.number().required().default(172800).description('Access Token Expiry TimeSTAMP')
		// 	}).required()
		// },
		// 401: {
		// 	message: joi.any().default(locales['genericErrMsg']['401'])
		// },
		// 403: {
		// 	message: joi.any().default(locales['genericErrMsg']['403'])
		// },
		// 417: {
		// 	message: joi.any().default(locales['genericErrMsg']['417'])
		// },
		// 500: {
		// 	message: joi.any().default(locales['genericErrMsg']['500'])
		// }
	}
};// swagger response code

module.exports = { handler, response };
