

const entity = 'user';

const api = require('./get');
const header = require('../../../../middleware/validator');

module.exports = [
	/**
    * 'Stripe webhook for handling an event ',
    */
	{
		method: 'GET',
		path: `/accessToken`,
		handler: api.handler,
		config: {
			tags: ['api', entity],
			response: api.response,
			plugins: {
				newTokenGenration: true
			},
			// auth: 'accessToken',
			validate: {
				headers: header.headerAuthRefresh,
				// eslint-disable-next-line max-len
				failAction: (req, reply, source, error) => reply({ message: error.output.payload.message }).code(error.output.statusCode)
			}
		}
	}
];
