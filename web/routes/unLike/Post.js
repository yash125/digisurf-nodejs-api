'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const postES = require("../../../models/postES");
const postsCollection = require("../../../models/posts");
const likesCollection = require("../../../models/likes");
const notificationCollection = require("../../../models/notification");

/**
 * @description API to un-like a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} userId
 * @property {string} postId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const payloadValidator = Joi.object({
    userId: Joi.string().required().description("userId, mongo id of the user"),
    postId: Joi.string().required().description("postId, mongo id of the post")
}).required();



const handler = (req, res) => {
    const userId = ObjectID(req.payload.userId);
    const postId = ObjectID(req.payload.postId);

    const checkIfLiked = () => {
        return new Promise((resolve, reject) => {
            // postES.UpdateWithPush(postId.toString(), "likes", userId.toString, (e, r) => { if (e) logger.error(e) })
            // postES.UpdateWithIncrease(postId.toString(), "likesCount", -1, (e, r) => { if (e) { logger.error(e) } });
            try {
                postES.Bulk([
                    { "update": { "_id": `${postId.toString()}`, "_type": "postList", "_index": "posts", "retry_on_conflict": 3 } },
                    { "script": { "source": "ctx._source.likes.remove(ctx._source.likes.indexOf('" + userId.toString() + "'))", "lang": "painless" } },
                    { "update": { "_id": `${postId.toString()}`, "_type": "postList", "_index": "posts", "retry_on_conflict": 3 } },
                    { "script": { "source": "ctx._source.likesCount+=-1", "lang": "painless" } }
                ], (e) => { if (e) logger.error(e) })

            } catch (error) {
                logger.error("error : ", error)
            }

            likesCollection.SelectOne({ "likedBy": userId, "postId": postId }, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (!d && !d._id) {
                    return reject({ code: 200, message: req.i18n.__('PostUnLike')['200'] });
                } else {
                    return resolve(d);
                }
            });
        });
    };
    const unlike = () => {
        return new Promise((resolve, reject) => {
            likesCollection.Delete({ "likedBy": userId, "postId": postId }, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    return resolve(d);
                }
            });
        });
    };
    const updateNotification = () => {
        return new Promise((resolve, reject) => {
            notificationCollection.Update({ "from": userId, "postId": postId, "end": false, "type.status": 2 }, { end: true }, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (!d) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {
                    return resolve({ code: 200, message: req.i18n.__('PostUnLike')['200'], data: d });
                }
            });
        });
    };
    const decrementLikeCountInPostCollection = (unlikeData) => {
        return new Promise((resolve, reject) => {
            postsCollection.UpdateByIdWithPull({ "_id": postId }, { "likes": userId }, (e) => { if (e) { logger.error(e); reject(e) } });
            postsCollection.UpdateWithIncrice({ "_id": postId, "likesCount": { "$gt": 0 } }, { likesCount: -1 },
                (e) => { if (e) { logger.error(e); reject(e) } });

            return resolve({ code: 200, message: req.i18n.__('PostUnLike')['200'], data: unlikeData });
        });
    };

    checkIfLiked()
        .then(() => { return unlike(); })
        .then((unlikeData) => { return updateNotification(unlikeData); })
        .then((unlikeData) => { return decrementLikeCountInPostCollection(unlikeData); })
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((data) => { return res({ message: data.message }).code(data.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['PostUnLike']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { payloadValidator, handler, response };