let headerValidator = require("../../middleware/validator")
let PostAPI = require('./Post');

module.exports = [
    {
        method: 'POST',
        path: '/unlike',
        handler: PostAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'like'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: PostAPI.response
        }
    }
];