'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userCollection = require("../../../models/userList");
const emailVerificationLogsCollection = require("../../../models/emailVerificationLogs");

/**
 * @description API to emailVerification a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} userId
 * @property {string} postId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const payloadValidator = Joi.object({
    starUserEmailId: Joi.string().required().description("starUserEmailId"),
    otp: Joi.number().required().description("starUserEmailId"),
    isVisible: Joi.bool().default(false).description("isVisible"),
}).unknown();

const handler = (req, res) => {
    const userId = ObjectID(req.auth.credentials._id);
    var emailId = req.payload.starUserEmailId;
    var otp = req.payload.otp;
    var isVisible = req.payload.isVisible;


    const VerificationMail = () => {
        return new Promise((resolve, reject) => {

            let condition = { emailId: emailId, otp: otp };

            emailVerificationLogsCollection.SelectWithSort(condition, { timestamp: -1 }, {}, 0, 1, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result && result.length) {
                    userCollection.Update({ _id: userId }, {
                        "starRequest.starUserEmail": emailId,
                        "verified.emailId": emailId,
                        "starRequest.isEmailVisible": isVisible
                    }, (e) => {
                        if (e) {
                            logger.error("OKOKASDAS : ", e)
                        }
                    })
                    return resolve({ code: 200, message: req.i18n.__('PostVerifyEmail')['200'] });
                } else {
                    return reject({ code: 412, message: req.i18n.__('PostVerifyEmail')['412'] });
                }
            })
        });
    };

    VerificationMail()
        .then((data) => { return res({ message: data.message }).code(data.code); })
        .catch((data) => { return res({ message: data.message }).code(data.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['PostVerifyEmail']['200']) },
        412: { message: Joi.any().default(local['PostVerifyEmail']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { payloadValidator, handler, response };