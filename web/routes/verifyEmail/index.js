let headerValidator = require("../../middleware/validator")
let PostAPI = require('./Post');

module.exports = [
    {
        method: 'POST',
        path: '/verifyEmail',
        handler: PostAPI.handler,
        config: {
            description: `This API use for verifyEmail`,
            tags: ['api', 'login'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    }
];