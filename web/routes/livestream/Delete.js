const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const dublyAudioCollection = require("../../../models/dublyAudio");
const userListCollection = require("../../../models/userList");

const validator = Joi.object({
    streamId: Joi.string().required().description("limit, query parameter")
}).unknown();


const hander = (req, res) => {

    userListCollection.UpdateWithPull({ _id: ObjectID(req.auth.credentials._id) }, {
        "streamHistory": { "streamId": ObjectID(req.query.streamId) }
    }, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }
        return res({ message: req.i18n.__('genericErrMsg')['200'] }).code(200);
    });

};

const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']) }
    }
}//swagger response code


module.exports = { validator, hander, response };