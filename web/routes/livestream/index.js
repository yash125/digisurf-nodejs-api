let DeleteAPI = require('./Delete');
let headerValidator = require("../../middleware/validator")

module.exports = [
    {
        method: 'Delete',
        path: '/livestream',
        handler: DeleteAPI.hander,
        config: {
            description: `This API is used for login`,
            tags: ['api', 'login'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: DeleteAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    }
];