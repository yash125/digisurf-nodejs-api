'use strict'
const Joi = require("joi");
const Promise = require('promise');
const local = require('../../../locales');
const userList = require("../../../models/userList");
var bcrypt = require("bcryptjs");
var ObjectID = require('mongodb').ObjectID;

const validator = Joi.object({
    password: Joi.string().required().description("password").error(new Error("password is required"))
}).unknown();

const hander = (req, res) => {

    const checkPassword = () => {
        return new Promise((resolve, reject) => {
            // Get data from database
            userList.SelectOne({ _id: ObjectID(req.auth.credentials._id.toString()) }, (err, data) => {
                if (err) {
                    // return reject if have any error 
                    return reject(err);
                } else if (data) {//check user is there otherwiase send in else

                    //check password is correct
                    if (bcrypt.compareSync(req.payload.password, data.password)) {
                        // if password is match then send 200
                        return resolve({ message: req.i18n.__('genericErrMsg')['200'], code: 200 });
                    } else {
                        // if password is not match then send 204
                        return resolve({ message: req.i18n.__('genericErrMsg')['204'], code: 204 });
                    }
                } else {//send 204 if user not found
                    return resolve({ message: req.i18n.__('genericErrMsg')['204'], code: 204 });
                }
            });
        });
    };

    checkPassword()
        .then((data) => { return res({ message: data.message }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']) },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code


module.exports = { validator, hander, response };