let headerValidator = require("../../middleware/validator")
let PostAPI = require('./Post');

module.exports = [
    {
        method: 'POST',
        path: '/isPasswordMatch',
        handler: PostAPI.hander,
        config: {
            description: `This API use for check password is match or not`,
            tags: ['api', 'password'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    }
];