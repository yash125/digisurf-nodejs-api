const Joi = require("joi");
const local = require("../../../../locales");
const addRewards = require('./../../../../models/addReward')

const validator = Joi.object({
  
  
    userId: Joi.string()
    .required()
    .max(24)
    .min(24)
    .description("userId")
    .example("5a096e680941c626e794bc5c")
    .error(
      new Error(
        "userId is missing || incorrect userId length must be 24 Character"
      )
    ),
  
    amount: Joi.string()
    .required()
    .description("message")
    .example("message")
    .error(new Error("message is missing")),

    trigger: Joi.string()
    .required()
    .description("message")
    .example("message")
    .error(new Error("message is missing")),

    type: Joi.string()
    .required()
    .description("message")
    .example("message")
    .error(new Error("message is missing"))
}).required();

let handler = async (req, res) => {
  try {
    var userId = req.payload.userId;
    var amount = Number(req.payload.amount);
    var trigger = req.payload.trigger;
    var type = req.payload.type;

   var result =  await addRewards(userId, amount, trigger, type);
   return res({ message: result }).code(200);

  } catch (error) {
    console.log("error =========> ", error);
    return res({ message: error.message }).code(500);
  }
};

const response = {
  status: {
    200: {
      message: Joi.any().default(local["PostUserReport"]["200"]),
      data: Joi.any()
    },
    400: { message: Joi.any().default(local["genericErrMsg"]["400"]) },
    500: { message: Joi.any().default(local["genericErrMsg"]["500"]) }
  }
}; //swagger response code

module.exports = { validator, handler, response };
