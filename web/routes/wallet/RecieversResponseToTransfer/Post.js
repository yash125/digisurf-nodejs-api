/**  
 * @method POST recieversResponseToTransfer
 * @description This API is called by the receiver to accept or deny a transfer
 * @author vinod@mobifyi.com
 * @param {string} messageId
 * @param {number} action
 * @property {string} language in header
 * ----------------------------------------------------------------------
 * MESSAGE COLLECTION DEFAULT transferStatus AND transferStatusText VALUES
 * -------------------------------------
 * transferStatus | transferStatusText |
 * -------------------------------------
 *       1        |   new              |
 *       2        |   confirm          |
 *       3        |   timeout          |
 *       4        |   denied           |
 *       5        |   canceled         |
 * -------------------------------------
 * 
 * --------------------------------------------------------
 * TRANSFER COLLECTION DEFAULT currentTransferStatus VALUES
 * --------------------------------------------------------
 * currentTransferStatus: -  pending / accepted / rejected 
 *                            / expired / sendercanceled
 * --------------------------------------------------------
 * 
 * 
 * CUSTOMERWALLET COLLECTION DEFAULT  txnType AND txnTypeCode VALUES
 * -----------------------------------------------------------------
 * txnType | txnTypeCode |
 * -----------------------
 *    1    |  CREDIT     |
 *    2    |  DEBIT      |
 *    3    |  PURCHASE   |
 *    4    |  TRANSFER   |
 * -----------------------
*/
const Joi = require("joi");
const Promise = require("bluebird");
const { ObjectID } = require("mongodb");
const _ = require('lodash');

var moment = require('moment');
var now = moment();

const mqtt = Promise.promisifyAll(require('./../../../../library/mqtt'));
const RUCWallet = Promise.promisifyAll(require("../../../../models/RUCWallet"));
const messages = Promise.promisifyAll(require("../../../../models/messages"));
const userList = Promise.promisifyAll(require("../../../../models/userList"));
const customerWallet = Promise.promisifyAll(
  require("../../../../models/customerWallet")
);
const transfers = Promise.promisifyAll(require("../../../../models/transfers"));
const local = require("../../../../locales");
var gRPC_Email_service = require("../../../../library/gRPC/Email");

const validator = Joi.object({
  messageId: Joi.string()
    .required()
    .max(24)
    .min(24)
    .description("messageId")
    .example("5a096e680941c626e794bc5c")
    .error(
      new Error(
        "messageId is missing || incorrect messageId length must be 24 Character"
      )
    ),
  action: Joi.number()
    .required()
    .description("1) For Accept 2) For Reject ")
    .example("message")
    .error(new Error("amount is missing"))
}).required();

let handler = async (req, res) => {
  try {
    let _id = req.auth.credentials._id;
    let action = req.payload.action;
    let messageId = ObjectID(req.payload.messageId);

    let mqttMessage;

    let user = await userList.SelectByIdAsync({ _id }, {});
    let message = await messages.SelectOneAsync({ _id: messageId, receiverId: ObjectID(_id) });


    if (!message || message.transferStatus != 1) {

      return res({ message: req.i18n.__("RecieversResponseToTransfer")["405"] }).code(405);
    }


    let sender = await userList.SelectByIdAsync({ _id: message.senderId }, {});


    if (action == 1) {

      let messageData = {
        transferStatus: 2,
        transferStatusText: "confirm",
      };

      await messages.UpdateByIdAsync(message._id, messageData);

      let transferData = {
        currentTransferStatus: "accepted",
        acceptedOn: new Date().getTime()
      };

      await transfers.UpdateAsync({ messageId }, transferData);
      let transfer = await transfers.SelectOneAsync({ messageId });
      let currentBalanceOfReciever = await RUCWallet.SelectByIdAsync({ _id: message.receiverId }, {});

      let currentClosingBalanceOfReciever = Number(currentBalanceOfReciever.RUCs.RUC + message.amount);

      if (transfer) {
        let customerWalletData = {
          txnId: "TXN-ID-" + new Date().getTime().toString(),
          userId: ObjectID(_id),
          txnType: "CREDIT",
          txnTypeCode: 1,
          trigger: "Payment Accepted",
          currency: process.env.CURRENCY_TYPE,
          currencySymbol: process.env.CURRENCY_TYPE,
          coinType: process.env.CURRENCY_TYPE,
          coinOpeingBalance: currentBalanceOfReciever.RUCs.RUC,
          cost: 0,
          coinAmount: message.amount,
          coinClosingBalance: currentClosingBalanceOfReciever,
          paymentType: process.env.CURRENCY_TYPE,
          timestamp: new Date().getTime(),
          paymentTxnId: transfer._id,
          initatedBy: "customer"
        };

        let createdCustomerWallet = await customerWallet.InsertAsync(customerWalletData);

        let acceptedMessage = await messages.SelectOneAsync({ _id: messageId, receiverId: ObjectID(_id) });
        mqttMessage = _.pick(acceptedMessage, ['messageId', 'chatId', 'payload', 'secretId', 'dTime', 'amount', 'messageType', 'timestamp',
          'expDtime', 'userImage', 'toDocId', 'transferStatus', 'transferStatusText', 'name', 'members']);

        mqttMessage.type = acceptedMessage.messageType.toString();
        mqttMessage.amount = mqttMessage.amount.toString();
        mqttMessage.transferStatus = mqttMessage.transferStatus.toString();
        mqttMessage.receiverIdentifier = sender.mobile;
        mqttMessage.from = sender._id.toString();
        mqttMessage.to = _id.toString();
        mqttMessage.id = acceptedMessage._id.toString();

        await mqtt.publishAsync("Message/" + mqttMessage.from, JSON.stringify(mqttMessage), { qos: 1 });
        await mqtt.publishAsync("Message/" + mqttMessage.to, JSON.stringify(mqttMessage), { qos: 1 });


        await RUCWallet.UpdateByIdAsync(currentBalanceOfReciever._id, {
          "RUCs.RUC": Number(createdCustomerWallet.ops[0].coinClosingBalance)
        });

        //Receiver Accepted - Message to sender

        let senderData = {
          userName: user.userName,
          trigger: "Money Transfer",
          to: sender.email.id,
          subject: `${user.userName} has accepted your money transfer`,
          body: '<p> Hello ' + sender.userName + ',' + '<br/><br/>' + user.userName + ' has accepted your transfer.' + ' The details are as follows:' + '<br/>AMOUNT:' + process.env.CURRENCY_TYPE + ' ' + transfer.Amount + '<br/>TRANSACTION ID: ' + customerWalletData.paymentTxnId + '<br/>SENDER NAME: ' + sender.userName + '<br/>SENDER PHONE: ' + sender.number + '<br/>TRANSFER DATE & TIME: ' + moment.utc(transfer.sentOn).format("Do MMMM YYYY, h:mm A") + '<br/>ACCEPTED DATE & TIME: ' + now.format("Do MMMM YYYY, h:mm A") + '<br/>Please do contact our support team in case you were not the one who initiated this. ' + '<br/><br/><br/>Cheers, <br/> Team '+process.env.APP_NAME+'.</p>'
        }
        gRPC_Email_service.sendEmail(senderData);

        //Receiver Accepted - Message to Receiver

        let ReceiverData = {
          userName: user.userName,
          trigger: "Money Transfer",
          to: user.email.id,
          subject: `You accepted the money transfer from ${sender.userName}`,
          body: '<p> Hello ' + user.userName + ',' + '<br/><br/>You have accepted the transfer from ' + sender.userName + ' The details are as follows:' + '<br/>AMOUNT:' + process.env.CURRENCY_TYPE + ' ' + transfer.Amount + '<br/>TRANSACTION ID: ' + customerWalletData.paymentTxnId + '<br/>SENDER NAME: ' + sender.userName + '<br/>SENDER PHONE: ' + sender.number + '<br/>TRANSFER DATE & TIME: ' + moment.utc(transfer.sentOn).format("Do MMMM YYYY, h:mm A") + '<br/>ACCEPTED DATE & TIME: ' + now.format("Do MMMM YYYY, h:mm A") + '<br/>Please do contact our support team in case you dont recognize this transfer.' + '<br/><br/><br/>Cheers, <br/> Team '+process.env.APP_NAME+'.</p>'
        }
        gRPC_Email_service.sendEmail(ReceiverData);
        return res({ message: req.i18n.__("RecieversResponseToTransfer")["200"] }).code(200);
      } else {
        console.log("no transfer found");

      }
    } else if (action == 2) {
      let messageData = {
        transferStatus: 4,
        transferStatusText: "denied",
      };

      await messages.UpdateByIdAsync(message._id, messageData);

      let transferData = {
        currentTransferStatus: "rejected",
        rejectedOn: new Date().getTime()
      };

      await transfers.UpdateAsync({ messageId }, transferData);
      let transfer = await transfers.SelectOneAsync({ messageId });
      let currentBalanceOfSender = await RUCWallet.SelectByIdAsync({ _id: message.senderId }, {});
      let currentClosingBalanceOfSender = Number(currentBalanceOfSender.RUCs.RUC + message.amount);

      if (transfer) {
        let customerWalletData = {
          txnId: "TXN-ID-" + new Date().getTime().toString(),
          userId: ObjectID(message.senderId),
          txnType: "CREDIT",
          txnTypeCode: 1,
          trigger: "Payment Rejected",
          currency: process.env.CURRENCY_TYPE,
          currencySymbol: process.env.CURRENCY_TYPE,
          coinType: process.env.CURRENCY_TYPE,
          coinOpeingBalance: currentBalanceOfSender.RUCs.RUC,
          cost: 0,
          coinAmount: message.amount,
          coinClosingBalance: currentClosingBalanceOfSender,
          paymentType: process.env.CURRENCY_TYPE,
          timestamp: new Date().getTime(),
          paymentTxnId: transfer._id,
          initatedBy: "customer"
        };

        let createdCustomerWallet = await customerWallet.InsertAsync(customerWalletData);


        let rejectedMessage = await messages.SelectOneAsync({ _id: messageId, receiverId: ObjectID(_id) });

        mqttMessage = _.pick(rejectedMessage, ['messageId', 'chatId', 'payload', 'secretId', 'dTime', 'amount', 'messageType', 'timestamp',
          'expDtime', 'userImage', 'toDocId', 'transferStatus', 'transferStatusText', 'name', 'members']);

        mqttMessage.type = rejectedMessage.messageType.toString();
        mqttMessage.amount = mqttMessage.amount.toString();
        mqttMessage.transferStatus = mqttMessage.transferStatus.toString();
        mqttMessage.receiverIdentifier = sender.number;
        mqttMessage.from = sender._id.toString();
        mqttMessage.to = _id.toString();
        mqttMessage.id = rejectedMessage._id.toString();

        await mqtt.publishAsync("Message/" + mqttMessage.from, JSON.stringify(mqttMessage), { qos: 1 });
        await mqtt.publishAsync("Message/" + mqttMessage.to, JSON.stringify(mqttMessage), { qos: 1 });

        await RUCWallet.UpdateByIdAsync(currentBalanceOfSender._id, {
          "RUCs.RUC": Number(createdCustomerWallet.ops[0].coinClosingBalance)
        });

        //Money Transfer - Receiver Rejected  - Message to sender

        let senderData = {
          userName: user.userName,
          trigger: "Money Transfer",
          to: sender.email.id,
          subject: `${user.userName} has rejected your money transfer`,
          body: '<p> Hello ' + sender.userName + ',' + '<br/><br/>' + user.userName + ' has rejected your transfer.' + ' The details are as follows:' + '<br/>AMOUNT:' + process.env.CURRENCY_TYPE + ' ' + transfer.Amount + '<br/>TRANSACTION ID: ' + customerWalletData.paymentTxnId + '<br/>SENDER NAME: ' + sender.userName + '<br/>SENDER PHONE: ' + sender.number + '<br/>TRANSFER DATE & TIME: ' + moment.utc(transfer.sentOn).format("Do MMMM YYYY, h:mm A") + '<br/>ACCEPTED DATE & TIME: ' + now.format("Do MMMM YYYY, h:mm A") + '<br/>Please do contact our support team in case you do not recognize this transfer.' + '<br/><br/><br/>Cheers, <br/> Team '+process.env.APP_NAME+'.</p>'
        }
        gRPC_Email_service.sendEmail(senderData);

        //Money Transfer - Receiver Rejected- Message to receiver
        let ReceiverData = {
          userName: user.userName,
          trigger: "Money Transfer",
          to: user.email.id,
          subject: `You rejected the money transfer from ${sender.userName}`,
          body: '<p> Hello ' + user.userName + ',' + '<br/><br/>You have rejected the transfer from ' + sender.userName + ' The details are as follows:' + '<br/>AMOUNT:' + process.env.CURRENCY_TYPE + ' ' + transfer.Amount + '<br/>TRANSACTION ID: ' + customerWalletData.paymentTxnId + '<br/>SENDER NAME: ' + sender.userName + '<br/>SENDER PHONE: ' + sender.number + '<br/>TRANSFER DATE & TIME: ' + moment.utc(transfer.sentOn).format("Do MMMM YYYY, h:mm A") + '<br/>ACCEPTED DATE & TIME: ' + now.format("Do MMMM YYYY, h:mm A") + '<br/>Please do contact our support team in case you dont recognize this transfer.' + '<br/><br/><br/>Cheers, <br/> Team '+process.env.APP_NAME+'.</p>'
        }
        gRPC_Email_service.sendEmail(ReceiverData);

        return res({ message: req.i18n.__("RecieversResponseToTransfer")["200"] }).code(200);
      }
    } else {
      return res({ message: req.i18n.__("RecieversResponseToTransfer")["405"] }).code(405);
    }
  } catch (error) {
    return res({ message: req.i18n.__("genericErrMsg")["500"] }).code(500);
  }
};

const response = {
  status: {
    200: {
      message: Joi.any().default(local["RecieversResponseToTransfer"]["200"]),
      data: Joi.any()
    },
    400: { message: Joi.any().default(local["genericErrMsg"]["400"]) },
    500: { message: Joi.any().default(local["genericErrMsg"]["500"]) }
  }
}; //swagger response code

module.exports = { validator, handler, response };
