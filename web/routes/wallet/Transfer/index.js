let headerValidator = require("../../../middleware/validator");
let PostAPI = require('./Post');

module.exports = [
    
    {
        method: 'POST',
        path: '/Transfer',
        handler: PostAPI.handler,
        config: {
            description: 'This API is used to initiate a transfer from one user to the other',
            tags: ['api', 'Wallet'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
];