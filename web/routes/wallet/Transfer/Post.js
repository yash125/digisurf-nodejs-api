/**  
 * @method POST transfer
 * @description This API is used to initiate a transfer from one user to the other
 * @author vinod@mobifyi.com,karthikb@mobifyi.com
 * @param {string} targetUserId
 * @param {string} amount
 * @param {string} reason
 * @property {string} language in header
 * ----------------------------------------------------------------------
 * MESSAGE COLLECTION DEFAULT transferStatus AND transferStatusText VALUES
 * -------------------------------------
 * transferStatus | transferStatusText |
 * -------------------------------------
 *       1        |   new              |
 *       2        |   confirm          |
 *       3        |   timeout          |
 *       4        |   denied           |
 *       5        |   canceled         |
 * -------------------------------------
 * 
 * --------------------------------------------------------
 * TRANSFER COLLECTION DEFAULT currentTransferStatus VALUES
 * --------------------------------------------------------
 * currentTransferStatus: -  pending / accepted / rejected 
 *                            / expired / sendercanceled
 * --------------------------------------------------------
 * 
 * 
 * CUSTOMERWALLET COLLECTION DEFAULT  txnType AND txnTypeCode VALUES
 * -----------------------------------------------------------------
 * txnType | txnTypeCode |
 * -----------------------
 *    1    |  CREDIT     |
 *    2    |  DEBIT      |
 *    3    |  PURCHASE   |
 *    4    |  TRANSFER   |
 * -----------------------
*/

const Joi = require("joi");
const Promise = require("bluebird");
const { ObjectID } = require("mongodb");
const _ = require('lodash');


var moment = require('moment');
var now = moment();



const mqtt = Promise.promisifyAll(require('./../../../../library/mqtt'));
const redisRepo = require('./../../../../library/redis/redis.repo');
const chatList = Promise.promisifyAll(require("../../../../models/chatList"));
const RUCWallet = Promise.promisifyAll(require("../../../../models/RUCWallet"));
const messages = Promise.promisifyAll(require("../../../../models/messages"));
const userList = Promise.promisifyAll(require("../../../../models/userList"));
const customerWallet = Promise.promisifyAll(
  require("../../../../models/customerWallet")
);
const transfers = Promise.promisifyAll(require("../../../../models/transfers"));
const local = require("../../../../locales");
var gRPC_Email_service = require("../../../../library/gRPC/Email");

const validator = Joi.object({
  targetUserId: Joi.string()
    .required()
    .max(24)
    .min(24)
    .description("targetUserId")
    .example("5a096e680941c626e794bc5c")
    .error(
      new Error(
        "targetUserId is missing || incorrect targetUserId length must be 24 Character"
      )
    ),
  reason: Joi.string().allow('').optional()
    .description("reason for transfer"),
  amount: Joi.string()
    .required()
    .description("message")
    .example("message")
    .error(new Error("amount is missing"))
}).required();

let handler = async (req, res) => {
  try {
    let _id = req.auth.credentials._id;
    let targetId = req.payload.targetUserId;
    let reason = req.payload.reason;
    let amount = Number(req.payload.amount);

    if (targetId == _id)
      return res({ message: req.i18n.__("PostTransfer")["405"] }).code(405);

    var currentBalance = await RUCWallet.SelectByIdAsync({ _id }, {});
    var receiverDetail = await userList.SelectOneAsync({ _id: ObjectID(targetId) })


    if (!currentBalance) {
      RUCWallet.InsertAsync({ _id: ObjectID(_id), "RUCs": { "RUC": 0 } })
      return res({ message: req.i18n.__("genericErrMsg")["500"] }).code(500);
    }
    let firstMemberId = 'members' + '.' + _id + '.memberId';
    let scoundMemberId = 'members' + '.' + targetId + '.memberId';
    if (currentBalance.RUCs.RUC >= amount) {
      let checkSenderAndReciver = {
        $and: [{ [firstMemberId]: ObjectID(_id) },
        { [scoundMemberId]: ObjectID(targetId) }
        ]
      };

      let messageId = new ObjectID();
      let chat = await chatList.SelectOneAsync(checkSenderAndReciver);

      let user = await userList.SelectByIdAsync({ _id }, {});
      let expireOn = new Date().getTime() + Number(process.env.RUC_TRANSFER_EXPIRY_IN_SECONDS); //24 * 60 *

      let messageData = {
        _id: messageId,
        // receiverIdentifier:user.number,
        chatId: chat ? chat._id : null,
        messageId: new Date().getTime().toString(),
        senderId: ObjectID(_id),
        secretId: "",
        dTime: -1,
        receiverId: chat ? ObjectID(req.payload.targetUserId) : null,
        payload: reason ? reason : "",
        amount: amount,
        messageType: "15",
        timestamp: new Date().getTime(),
        expDtime: expireOn,
        userImage: user.profilePic,
        toDocId: "",
        transferStatus: 1,
        transferStatusText: "new",
        name: user.firstName,
        members: {
          _id: {
            memberId: ObjectID(_id)
          },
          targetId: {
            memberId: ObjectID(targetId),
            status: 1
          }
        }
      };

      if (chat) {
        await chatList.UpdateMessageAsync(checkSenderAndReciver, {
          message: messageId
        });
        await messages.InsertAsync(messageData);
      } else {
        var createdChat = await chatList.InsertAsync({
          creationTs: new Date().getTime(),
          creationDate: new Date(),
          message: [messageId],
          members: {
            [_id]: {
              memberId: ObjectID(_id),
              status: "NormalMember"
            },
            [targetId]: {
              memberId: ObjectID(targetId),
              status: "NormalMember"
            }
          },
          senderId: ObjectID(_id),
          targetId: ObjectID(targetId),
          initiatedBy: ObjectID(_id),
          createdAt: new Date().getTime(),
          chatType: "NormalChat",
          secretId: ""
        });

        messageData.chatId = createdChat.ops[0]._id;
        messageData.receiverId = ObjectID(req.payload.targetUserId);
        await messages.InsertAsync(messageData);
      }
      let currentClosingBalance = Number(currentBalance.RUCs.RUC - amount);
      let transferData = {
        senderId: user._id,
        receiverId: ObjectID(targetId),
        messageId: messageId,
        Amount: Number(amount),
        currency: process.env.CURRENCY_TYPE,
        sentOn: new Date().getTime(),
        expiresOn: expireOn,
        currentTransferStatus: "pending",
        acceptedOn: 0,
        rejectedOn: 0,
        isExpired: false,
        senderCanceledOn: 0
      };

      let createdTransfer = await transfers.InsertAsync(transferData);

      if (createdTransfer) {
        let customerWalletData = {
          txnId: "TXN-ID-" + new Date().getTime().toString(),
          userId: ObjectID(_id),
          txnType: "TRANSFER",
          txnTypeCode: 4,
          trigger: "Payment Sent",
          currency: process.env.CURRENCY_TYPE,
          currencySymbol: process.env.CURRENCY_TYPE,
          coinType: process.env.CURRENCY_TYPE,
          coinOpeingBalance: currentBalance.RUCs.RUC,
          cost: 0,
          coinAmount: Number(amount),
          coinClosingBalance: currentClosingBalance,
          paymentType: process.env.CURRENCY_TYPE,
          timestamp: new Date().getTime(),
          paymentTxnId: createdTransfer.ops[0]._id,
          initatedBy: "customer"
        };

        let mqttMessage = _.pick(messageData, ['messageId', 'payload', 'chatId', 'secretId', 'dTime', 'amount', 'timestamp',
          'expDtime', 'userImage', 'toDocId', 'transferStatus', 'transferStatusText', 'name', 'members']);

        mqttMessage.type = messageData.messageType.toString();
        mqttMessage.amount = mqttMessage.amount.toString();
        mqttMessage.transferStatus = mqttMessage.transferStatus.toString();
        mqttMessage.receiverIdentifier = user.number;
        mqttMessage.from = _id.toString();
        mqttMessage.to = targetId.toString();
        mqttMessage.id = messageData._id.toString();

        await mqtt.publishAsync("Message/" + _id.toString(), JSON.stringify(mqttMessage), { qos: 1 });
        await mqtt.publishAsync("Message/" + targetId.toString(), JSON.stringify(mqttMessage), { qos: 1 });

        redisRepo.setReminder(createdTransfer.ops[0]._id.toString(), createdTransfer.ops[0]._id.toString(), process.env.RUC_TRANSFER_EXPIRY_IN_SECONDS); //86400
        // redisRepo.setReminder(createdTransfer.ops[0]._id.toString(), createdTransfer.ops[0]._id.toString(), 10); //10
        
        let createdCustomerWallet = await customerWallet.InsertAsync(customerWalletData);

        await RUCWallet.UpdateByIdAsync(currentBalance._id, {
          "RUCs.RUC": Number(createdCustomerWallet.ops[0].coinClosingBalance)
        });

        //code to send email to sender

        receiverDetail = await userList.SelectOneAsync({ _id: ObjectID(targetId) })

        let emailNote = reason ? reason : 'not specified'
        let senderData = {
          userName: receiverDetail.userName,
          trigger: "Money Transfer",
          to: user.email.id,
          subject: `You just sent money to ${receiverDetail.userName} `,
          body: '<p> Hello ' + user.firstName + ',' + '<br/><br/> You just initiated a transfer of ' + process.env.CURRENCY_TYPE + ' ' + amount + ' to ' + receiverDetail.userName + '.' + 'The details are as follows:' + '<br/>AMOUNT:' + process.env.CURRENCY_TYPE + ' ' + amount + '<br/>TRANSACTION ID: ' + customerWalletData.paymentTxnId + '<br/>RECEIVER NAME: ' + receiverDetail.userName + '<br/>RECEIVER PHONE: ' + receiverDetail.number + '<br/>DATE & TIME: ' + now.format("Do MMMM YYYY, h:mm A") + '<br/>NOTES: ' + emailNote + '<br/>Please do contact our support team in case you were not the one who initiated this. ' + '<br/><br/><br/>Cheers, <br/> Team '+process.env.APP_NAME+'.</p>'
        }
        gRPC_Email_service.sendEmail(senderData)

        // code to send email to receiver
        let receiverData = {
          userName: receiverDetail.userName,
          trigger: "Money Transfer",
          to: receiverDetail.email.id,
          subject: `You just received money from ${user.userName} `,
          body: '<p> Hello ' + receiverDetail.userName + ',' + '<br/><br/> You just received a transfer of ' + process.env.CURRENCY_TYPE + ' ' + amount + ' from ' + user.userName + '.' + 'The details are as follows:' + '<br/>AMOUNT:' + process.env.CURRENCY_TYPE + ' ' + amount + '<br/>TRANSACTION ID: ' + customerWalletData.paymentTxnId + '<br/>SENDER NAME: ' + user.userName + '<br/>SENDER PHONE: ' + user.number + '<br/>DATE & TIME: ' + now.format("Do MMMM YYYY, h:mm A") + '<br/>NOTES: ' + emailNote + '<br/>Please do contact our support team in case you do not recognize this transfer. ' + '<br/><br/><br/>Cheers, <br/> Team '+process.env.APP_NAME+'.</p>'
        }
        gRPC_Email_service.sendEmail(receiverData)

        return res({ message: req.i18n.__("PostTransfer")["200"] }).code(200);
      }
    } else {
      return res({ message: req.i18n.__("PostTransfer")["412"] }).code(412);
    }
  } catch (error) {
    console.log("error =========> ", error);
    return res({ message: req.i18n.__("genericErrMsg")["500"] }).code(500);
  }
};

const response = {
  status: {
    200: {
      message: Joi.any().default(local["PostUserReport"]["200"]),
      data: Joi.any()
    },
    400: { message: Joi.any().default(local["genericErrMsg"]["400"]) },
    500: { message: Joi.any().default(local["genericErrMsg"]["500"]) }
  }
}; //swagger response code

module.exports = { validator, handler, response };
