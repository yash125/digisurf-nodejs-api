/**  
 * @method POST cancelTransfer
 * @description This API is used to cancel a transfer from sender
 * @author vinod@mobifyi.com
 * @param {string} messageId
 * @property {string} language in header
 * ----------------------------------------------------------------------
 * MESSAGE COLLECTION DEFAULT transferStatus AND transferStatusText VALUES
 * ------------------------------------
 * transferStatus | transferStatusText
 * ------------------------------------
 *       1        |   new
 *       2        |   confirm
 *       3        |   timeout
 *       4        |   denied
 *       5        |   canceled
 * ------------------------------------
 * 
 * --------------------------------------------------------
 * TRANSFER COLLECTION DEFAULT currentTransferStatus VALUES
 * --------------------------------------------------------
 * currentTransferStatus: -  pending / accepted / rejected 
 *                            / expired / sendercanceled
 * --------------------------------------------------------
 * 
 * 
 * CUSTOMERWALLET COLLECTION DEFAULT  txnType AND txnTypeCode VALUES
 * -----------------------------------------------------------------
 * txnType | txnTypeCode
 * ---------------------
 *    1    |  CREDIT
 *    2    |  DEBIT
 *    3    |  PURCHASE
 *    4    |  TRANSFER
 * ---------------------
*/
const Joi = require("joi");
const Promise = require("bluebird");
const { ObjectID } = require("mongodb");
const _ = require('lodash');

const mqtt = Promise.promisifyAll(require('./../../../../library/mqtt'));
const userList = Promise.promisifyAll(require("../../../../models/userList"));
const RUCWallet = Promise.promisifyAll(require("../../../../models/RUCWallet"));
const messages = Promise.promisifyAll(require("../../../../models/messages"));
const customerWallet = Promise.promisifyAll(
  require("../../../../models/customerWallet")
);
const transfers = Promise.promisifyAll(require("../../../../models/transfers"));
const local = require("../../../../locales");

const validator = Joi.object({
  messageId: Joi.string()
    .required()
    .max(24)
    .min(24)
    .description("messageId")
    .example("5a096e680941c626e794bc5c")
    .error(
      new Error(
        "messageId is missing || incorrect messageId length must be 24 Character"
      )
    )
}).required();

let handler = async (req, res) => {
  try {
    let _id = req.auth.credentials._id;
    let messageId = ObjectID(req.payload.messageId);

    let message = await messages.SelectOneAsync({ _id: messageId, senderId: ObjectID(_id) });
    let sender = await userList.SelectByIdAsync({ _id }, {});
    if (message && message.transferStatus == 1) {
      let messageData = {
        transferStatus: 5,
        transferStatusText: "canceled"
      };

      await messages.UpdateByIdAsync(message._id, messageData);

      let transferData = {
        currentTransferStatus: "senderCanceled",
        senderCanceledOn: new Date().getTime()
      };

      await transfers.UpdateAsync({ messageId }, transferData);
      let transfer = await transfers.SelectOneAsync({ messageId });
      let currentBalanceOfSender = await RUCWallet.SelectByIdAsync(
        { _id: message.senderId },
        {}
      );
      let currentClosingBalanceOfSender = Number(
        currentBalanceOfSender.RUCs.RUC + message.amount
      );

      if (transfer) {
        let customerWalletData = {
          txnId: "TXN-ID-" + new Date().getTime().toString(),
          userId: ObjectID(_id),
          txnType: "CREDIT",//"TRANSFER",
          txnTypeCode: 1,
          trigger: "Payment Canceled",
          currency: process.env.CURRENCY_TYPE,
          currencySymbol: process.env.CURRENCY_TYPE,
          coinType: process.env.CURRENCY_TYPE,
          coinOpeingBalance: currentBalanceOfSender.RUCs.RUC,
          cost: 0,
          coinAmount: message.amount,
          coinClosingBalance: currentClosingBalanceOfSender,
          paymentType: process.env.CURRENCY_TYPE,
          timestamp: new Date().getTime(),
          transctionTime: new Date().getTime(),
          transctionDate: new Date().getDate(),
          paymentTxnId: transfer._id,
          initatedBy: "customer"
        };

        let createdCustomerWallet = await customerWallet.InsertAsync(customerWalletData);

        let canceledMessage = await messages.SelectOneAsync({ _id: messageId, senderId: ObjectID(_id) });
        let mqttMessage = _.pick(canceledMessage, ['messageId', 'chatId', 'payload', 'secretId', 'dTime', 'amount', 'messageType', 'timestamp',
          'expDtime', 'userImage', 'toDocId', 'transferStatus', 'transferStatusText', 'name', 'members']);

        mqttMessage.type = canceledMessage.messageType.toString();
        mqttMessage.amount = mqttMessage.amount.toString();
        mqttMessage.transferStatus = mqttMessage.transferStatus.toString();
        mqttMessage.receiverIdentifier = sender.number;
        mqttMessage.from = canceledMessage.senderId.toString();
        mqttMessage.to = canceledMessage.receiverId.toString();
        mqttMessage.id = canceledMessage._id.toString();

        await mqtt.publishAsync("Message/"+mqttMessage.from, JSON.stringify(mqttMessage), { qos: 1 });
        await mqtt.publishAsync("Message/"+mqttMessage.to, JSON.stringify(mqttMessage), { qos: 1 });

        await RUCWallet.UpdateByIdAsync(currentBalanceOfSender._id, {
          "RUCs.RUC": Number(createdCustomerWallet.ops[0].coinClosingBalance)
        });
        return res({ message: req.i18n.__("genericErrMsg")["200"] }).code(200);
      }
    } else {
      return res({ message: req.i18n.__("genericErrMsg")["405"] }).code(405);
    }

  } catch (error) {
    console.log("error =========> ", error);
    return res({ message: req.i18n.__("genericErrMsg")["500"] }).code(500);
  }
};

const response = {
  status: {
    200: {
      message: Joi.any().default(local["PostUserReport"]["200"]),
      data: Joi.any()
    },
    400: { message: Joi.any().default(local["genericErrMsg"]["400"]) },
    500: { message: Joi.any().default(local["genericErrMsg"]["500"]) }
  }
}; //swagger response code

module.exports = { validator, handler, response };
