const transfer = require('./Transfer');
const recieversResponseToTransfer = require('./RecieversResponseToTransfer');
const WalletBalance = require('./WalletBalance');
const WalletStatement = require('./WalletStatement');
const cancelTransfer = require('./cancelTransfer');
const addReward = require('./addReward');
const getMessageDetails = require('./getMessageDetails');

module.exports = [].concat(transfer, recieversResponseToTransfer, WalletBalance, WalletStatement, cancelTransfer, addReward, getMessageDetails);