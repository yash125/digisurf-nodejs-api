let headerValidator = require("../../../middleware/validator");
let GetAPI = require('./Get');

module.exports = [
    
    {
        method: 'GET',
        path: '/WalletBalance',
        handler: GetAPI.handler,
        config: {
            description: 'This API will return the current balance of the users wallet and also the pending balance ( the amount the user has not accepted yet )',
            tags: ['api', 'Wallet'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
];