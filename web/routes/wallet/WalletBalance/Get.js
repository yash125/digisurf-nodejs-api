const Joi = require("joi");
const Promise = require("bluebird");
const { ObjectID } = require("mongodb");

const RUCWallet = Promise.promisifyAll(require("../../../../models/RUCWallet"));
const messages = Promise.promisifyAll(require("../../../../models/messages"));
const local = require("../../../../locales");

let handler = async (req, res) => {
  try {
    let _id = ObjectID(req.auth.credentials._id);
    let currentBalance = await RUCWallet.SelectByIdAsync({ _id }, {});
    if (!currentBalance) {
      currentBalance = { RUCs: { RUC: 0 } };
      RUCWallet.Insert({ _id: _id, RUCs: { RUC: 0 } }, () => { });
    }
    let message = await messages.SelectAsync({ receiverId: _id, transferStatus: 1 });
    let pendingBalance = 0;
    message.forEach((eachMessage) => { pendingBalance = pendingBalance + eachMessage.amount });

    return res({ message: req.i18n.__("WalletBalance")["200"], data: { balance: currentBalance.RUCs.RUC, pendingBalance } }).code(200);

  } catch (error) {
    console.log("error =========> ", error);
    return res({ message: req.i18n.__("genericErrMsg")["500"] }).code(500);
  }
};

const response = {
  status: {
    200: {
      message: Joi.any().default(local["WalletBalance"]["200"]),
      data: Joi.any()
    },
    400: { message: Joi.any().default(local["genericErrMsg"]["400"]) },
    500: { message: Joi.any().default(local["genericErrMsg"]["500"]) }
  }
}; //swagger response code

module.exports = { handler, response };
