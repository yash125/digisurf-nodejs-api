let headerValidator = require("../../../middleware/validator");
let GetAPI = require('./Get');

module.exports = [
    
    {
        method: 'GET',
        path: '/WalletStatement',
        handler: GetAPI.handler,
        config: {
            description: 'This API will return all the txns on the users wallet',
            tags: ['api', 'Wallet'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query : GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
];