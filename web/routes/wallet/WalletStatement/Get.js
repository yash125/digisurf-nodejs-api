const Joi = require("joi");
const Promise = require("bluebird");
const { ObjectID } = require("mongodb");

const customerWallet = Promise.promisifyAll(require("../../../../models/customerWallet"));
const local = require("../../../../locales");

const paramValidator = {
}
const queryValidator = {
  skip: Joi.string().description('skip').error((new Error('skip is missing'))),
  limit: Joi.string().description('limit').error((new Error('limit is missing'))),
  debitOrCredit: Joi.number().required().description('0 for all, 1 for debit and 2 for credit ').error((new Error('debitOrCredit is missing')))
}

let handler = async (req, res) => {
  try {

    let skip = req.query.skip ? parseInt(req.query.skip) : 0;
    let limit = req.query.limit ? parseInt(req.query.limit) : 20;
    let debitOrCredit = req.query.debitOrCredit ? req.query.debitOrCredit : 0;

    var query = {};
    query.skip = skip;
    query.limit = limit;

    let _id = ObjectID(req.auth.credentials._id);

    let walletStatement;
    switch (debitOrCredit) {
      case 0:
        walletStatement = await customerWallet.selectWithPaginationAsync({ userId: _id }, query);
        return res({ message: req.i18n.__("WalletStatement")["200"], data: walletStatement }).code(200);

      case 1:
        walletStatement = await customerWallet.selectWithPaginationAsync({ userId: _id, "$or": [{ txnType: "DEBIT" }, { txnType: "TRANSFER" }] }, query);
        return res({ message: req.i18n.__("WalletStatement")["200"], data: walletStatement }).code(200);

      case 2:
        walletStatement = await customerWallet.selectWithPaginationAsync({ $and: [{ userId: _id }, { txnType: "CREDIT" }] }, query);
        return res({ message: req.i18n.__("WalletStatement")["200"], data: walletStatement }).code(200);

      default:
        return res({ message: "debitOrCredit value is Invalid" }).code(422);
    }
  } catch (error) {
    console.log("error =========> ", error);
    return res({ message: req.i18n.__("genericErrMsg")["500"] }).code(500);
  }
};

const response = {
  status: {
    200: {
      message: Joi.any().default(local["WalletStatement"]["200"]),
      data: Joi.any()
    },
    400: { message: Joi.any().default(local["genericErrMsg"]["400"]) },
    500: { message: Joi.any().default(local["genericErrMsg"]["500"]) },
    422: { message: Joi.any().default("invalid data!") }
  }
}; //swagger response code

module.exports = { paramValidator, handler, response, queryValidator };
