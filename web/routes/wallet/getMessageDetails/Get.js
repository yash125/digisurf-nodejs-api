const Joi = require("joi");
const Promise = require("bluebird");
//paymentTxnId
const messages = Promise.promisifyAll(require("../../../../models/messages"));
const userList = Promise.promisifyAll(require("../../../../models/userList"));
const customerWallet = Promise.promisifyAll(require("../../../../models/customerWallet"));
const transfers = Promise.promisifyAll(require("../../../../models/transfers"));
const local = require("../../../../locales");

const paramValidator = {
  messageId: Joi.string().required().description('_id of message').error((new Error('messageId is missing or invalid')))
}

let handler = async (req, res) => {
  try {
    //status of transaction
    //message created date
    //transferId
    //to
    //from
    //amount
    let messageDetails = {};

    let messageId = req.query.messageId;

    let message = await messages.SelectOneAsync({ messageId: messageId });

    if (message) {
      // let transfer = await transfers.SelectOneAsync({ messageId: message._id });
      //  if (transfer) {
      //      let cWallet = await customerWallet.SelectOneAsync({ paymentTxnId: transfer._id });
      //       let senderUser = await userList.SelectByIdAsync({ _id: message.senderId }, { firstName: 1, lastName: 1 });
      //        let recieverUser = await userList.SelectByIdAsync({ _id: message.receiverId }, { firstName: 1, lastName: 1 });

      var isSenderUser = (req.auth.credentials._id == message.senderId);

      messageDetails.messageId = message.messageId;
      messageDetails.transactionId = (isSenderUser) ? message.fromTxnId : message.toTxnId;
      messageDetails.txnId = (isSenderUser) ? message.fromTxnId : message.toTxnId;
      messageDetails.transferStatus = 1;
      messageDetails.messageCreatedDate = message.timestamp;
      messageDetails.from = message.fromName;
      messageDetails.to = message.toName;
      messageDetails.amount = message.amount;
      messageDetails.currencySymbol = message.currencySymbol;

      return res({ message: req.i18n.__("getMessageDetails")["200"], data: messageDetails }).code(200);
    } else {
      return res({ message: req.i18n.__("getMessageDetails")["404"] }).code(404);
    }
    // } 
  } catch (error) {
    console.log("error =========> ", error);
    return res({ message: req.i18n.__("genericErrMsg")["500"] }).code(500);
  }
};

const response = {
  status: {
    200: {
      message: Joi.any().default(local["WalletBalance"]["200"]),
      data: Joi.any()
    },
    404: { message: Joi.any().default(local["genericErrMsg"]["404"]) },
    500: { message: Joi.any().default(local["genericErrMsg"]["500"]) }
  }
}; //swagger response code

module.exports = { paramValidator, handler, response };
