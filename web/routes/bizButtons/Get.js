'use strict'
//require npm modules
const Joi = require("joi");
//require dependency
const local = require('../../../locales');
const bizButtonsCollection = require("../../../models/bizButtons");

//validator for this API
const validator = Joi.object({
    offset: Joi.string().default("0").description("offset, query parameter"),
    limit: Joi.string().default("20").description("limit, query parameter")
}).unknown();


/**
 * @method GET bizButtons
 * @description This API returns button text and button color
 * 
 * @headers {string} authorization : token
 * @headers {string} lang by defualt en
 * 
 * @param {string} offset by default 0
 * @param {string} limit  by default 20
 * 
 * @author DipenAhir
 * @since 09-July-2019
 */

//start API hender
const hander = (req, res) => {
    let offset = Number(req.query.offset) || 0;
    let limit = Number(req.query.limit) || 20;

    bizButtonsCollection.SelectWithSort({ "status": 1 }, { _id: -1 }, {}, offset, limit, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result && result.length) {
            return res({
                message: req.i18n.__('GetBizButtons')['200'], data: result.map(e => {
                    return {
                        buttonText: e.text[req.headers.lang],
                        buttonColor: e.colorCode
                    }
                })
            }).code(200);
        } else {
            return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);
        }
    });
};//end API hender

//start API response
const response = {
    status: {
        200: {
            message: Joi.string().default(local['GetBizButtons']['200']).example(local['GetBizButtons']['200']).required(),
            data: Joi.array().required().items({
                buttonText: Joi.string().example("Buy now").required(),
                buttonColor: Joi.string().example("#EB104B").required()
            }).example([{
                "buttonText": "Buy now",
                "buttonColor": "#EB104B"
            }])
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']).example(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']).example(local['genericErrMsg']['400']) }
    }
}//end API response

//Export all constance
module.exports = { validator, hander, response };