let headerValidator = require("../../middleware/validator")
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/bizButtons',
        handler: GetAPI.hander,
        config: {
            description: `This API returns button text and button color`,
            tags: ['api'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];