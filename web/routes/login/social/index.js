let PostAPI = require('./post');

module.exports = [
    {
        method: 'POST',
        path: '/login/social',
        handler: PostAPI.handler,
        config: {
            description: `This API is used for login`,
            tags: ['api', 'login'],
            auth: 'basic',
            validate: {
                // headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
               /*  failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                } */
            },
            response: PostAPI.response
        }
    }
];