let PostAPI = require('./post');
let headerValidator = require("../../middleware/validator")

module.exports = [
    {
        method: 'POST',
        path: '/login',
        handler: PostAPI.handler,
        config: {
            description: `This API is used for login`,
            tags: ['api', 'login'],
            auth: {
                strategies: ['basic', 'guest']
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    }
].concat(require("./social"));