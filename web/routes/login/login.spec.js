'use strict'
const chai = require('chai');
const { expect } = require('chai');
const chaiHttp = require('chai-http');
const logger = require('winston');
const request = require("request");
chai.use(chaiHttp);
/*
 * Test the /signUp
 */
describe('/POST login', () => {
    it('it should successfully loggedin the user with status code 200', (done) => {

        let data = {
            userName: 'stream',
            password: '1234567890'

        }

        logger.silly('dataaaaaaaaaaaaaaaaaa=>',data);
        request.post({
            headers: { 'content-type': 'application/json', 'lan': '1' },
            url: 'http://localhost:5007/login',
            body: data,
            json: true
        }, function (error, response, body) {
            logger.silly("body------",body);
            logger.error("error------>", error);
            expect(response.statusCode).to.eql(200);
            done();
        });

    });
    

    it('it should successfully loggedin the user with status code 200', (done) => {

        let data = {
            password: '1234567890',
            number: '5555555555',
            countryCode: '+91'

        }

        request.post({
            headers: { 'content-type': 'application/json', 'lan': '1' },
            url: 'http://localhost:5007/login',
            body: data,
            json: true
        }, function (error, response, body) {
            logger.silly("body------",body);
            logger.error("error------>", error);
            expect(response.statusCode).to.eql(200);
            done();
        });

    });
    it('it should say wrong password with status code 412', (done) => {

        let data = {
            password: '1234566',
            number: '5555555555',
            countryCode: '+91'

        }

        request.post({
            headers: { 'content-type': 'application/json', 'lan': '1' },
            url: 'http://localhost:5007/login',
            body: data,
            json: true
        }, function (error, response, body) {
            logger.silly("body------",body);
            logger.error("error------>", error);
            expect(response.statusCode).to.eql(412);
            done();
        });

    });
    it('it should say Phone number not registered with status code 404', (done) => {

        let data = {
            password: '1234566',
            number: '5555555555',
            countryCode: '+90'

        }

        request.post({
            headers: { 'content-type': 'application/json', 'lan': '1' },
            url: 'http://localhost:5007/login',
            body: data,
            json: true
        }, function (error, response, body) {
            logger.silly("body------",body);
            logger.error("error------>", error);
            expect(response.statusCode).to.eql(404);
            done();
        });

    });


});

