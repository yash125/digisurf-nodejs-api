'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
var bcrypt = require("bcryptjs")
var ObjectID = require("mongodb").ObjectID
const userList = require("../../../models/userList");
const deviceInfo = require("../../../models/deviceInfo");
var jwtValidator = require('./../../middleware/auth');
var fcm = require('./../../../library/fcm');
const check = require('../../../library/checkEmpty');

const request = require('request');
//models
const signUpModel = require('./../../../models/signUp');

/**
 * @property {string} lang - language
 * @property {string} userId
 * @property {string} postId
 * @returns 200 : Success
 * @returns 201 : signedUp sucessfully
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */



const payloadValidator = Joi.object({
    userName: Joi.string().min(3).default("").description("please enter starChat Id"),
    password: Joi.string().required().description("enter the password").error(new Error('please enter a valid password')),
    number: Joi.string().default("").description("PhoneNumber without countrycode"),
    countryCode: Joi.string().default("").description("country code"),
    // deviceType: Joi.number().description("deviceType is required"),
    deviceId: Joi.string().default("1").description("deviceId is required").error(new Error("deviceId is missing")),
    deviceName: Joi.string().default("1").description("deviceName is required").error(new Error("deviceName is missing")),
    deviceOs: Joi.string().default("1").description("deviceOs is required").error(new Error("deviceOs is missing")),
    modelNumber: Joi.string().default("1").description("modelNumber is required").error(new Error("modelNumber is missing")),
    deviceType: Joi.string().default("1").description("deviceType is required").error(new Error("deviceType is missing")),
    appVersion: Joi.string().default("1").description("appVersion is required").error(new Error("appVersion is missing")),
    countryName: Joi.string().description("countryName is required").error(new Error("countryName is missing"))
}).unknown();

/**
 * handler starts here 
 */
const handler = (req, res) => {
    var dataToSend = { "code": 200, "message": "success", "response": {} };
    let data = {  }
    check.isEmpty(req.payload.userName) ? data.mobile = req.payload.number : data.userName = req.payload.userName;

    /**
     * checks if user is in database
     */
    let checkUserName = () => {
        return new Promise((resolve, reject) => {
            req.payload.mobile = req.payload.number;
            signUpModel.checkUser(data, (err, result) => {
                if (err) return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });

                if (result && result.length) {
                    return resolve(result)
                } else {
                    return (req.payload.number !== "") ?
                        reject({ code: 404, message: req.i18n.__('login')['404'] })
                        : reject({ code: 422, message: req.i18n.__('login')['422'] })

                }
            })
        })
    }

    /**
     * check if the password entered by the user is correct 
     */
    let verifyPassword = (data) => {
        return new Promise((resolve, reject) => {
            try {

                let password = false;
                if (data[0] && data[0].userStatus != 1) {   //
                    return reject({ code: 403, message: "admin has " + data[0]["userStatusText"] + " you" });
                }

                if (data[0].password) {
                    password = bcrypt.compareSync(req.payload.password, data[0].password);
                    dataToSend["response"]["userId"] = data[0]._id.toString()
                    dataToSend["response"]["token"] = ""
                    dataToSend["response"]["private"] = (data[0].private) ? 1 : 0;

                    dataToSend["response"]["businessProfile"] = (data[0].businessProfile) ? data[0].businessProfile : [];
                    dataToSend["response"]["isActiveBusinessProfile"] = (data[0].isActiveBusinessProfile) ? true : false

                    dataToSend["response"]["profilePic"] = data[0].profilePic
                    dataToSend["response"]["userName"] = data[0].userName
                    dataToSend["response"]["firstName"] = data[0].firstName
                    dataToSend["response"]["lastName"] = data[0].lastName
                    dataToSend["response"]["profileVideo"] = data[0].profileVideo
                    dataToSend["response"]["profileVideoThumbnail"] = data[0].profileVideoThumbnail
                    dataToSend["response"]["authToken"] = ""
                    dataToSend["response"]["willTopic"] = ""


                    dataToSend["response"]["groupCallStreamId"] = data[0].groupCallStreamId || "";
                    dataToSend["response"]["accountId"] = process.env.accountId
                    dataToSend["response"]["keysetId"] = process.env.keysetId
                    dataToSend["response"]["projectId"] = process.env.projectId
                    dataToSend["response"]["licenseKey"] = process.env.licenseKey
                    dataToSend["response"]["keysetName"] = process.env.keysetName
                    dataToSend["response"]["rtcAppId"] = process.env.rtcAppId
                    dataToSend["response"]["arFiltersAppId"] = process.env.arFiltersAppId


                    // dataToSend["response"]["qrCode"] = data[0].qrCode.url;//
                }


                if (password || (data && data[0] && data[0].password_original && data[0].password_original == req.payload.password)) {
                    return resolve(data[0]);
                } else {
                    return reject({ code: 412, message: req.i18n.__('login')['412'] })
                }



            } catch (err) {
                logger.info("error ", err)
                return reject(err)
            }
        })
    }

    /**
     * generate auth token and send it to user
     */
    let login = (payload) => {
        return new Promise((resolve, reject) => {
            delete payload.password,
                delete payload.profilePicture
            payload.accessKey = String(Math.floor(1000 + Math.random() * 9000));
            payload.type = 'user';
            payload.userStatus = 1;
            payload.userId = payload._id;

            var deviceInformation = {
                "userId": ObjectID(payload._id),
                "deviceName": req.payload.deviceName,
                "deviceOs": req.payload.deviceOs,
                "modelNumber": req.payload.modelNumber,
                "deviceType": req.payload.deviceType,
                "appVersion": req.payload.appVersion,
                "deviceId": req.payload.deviceId,
                "timestamp": new Date().getTime(),
                "creationDate": new Date(),
                "_id": new ObjectID()
            };

            deviceInfo.Insert(deviceInformation, (e) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] })
                }
            });

            userList.Update({ _id: ObjectID(payload._id) }, {
                "deviceInfo": deviceInformation
            }, (e) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] })
                }
            });
            dataToSend["response"]["currencySymbol"] = process.env.CURRENCY_SYMBOL || "";
            dataToSend["response"]["currency"] = process.env.CURRENCY_CODE || "";

            jwtValidator.generateTokens(
                {
                    userId: payload._id.toString(),
                    userType: "user",
                    multiLogin: "true",
                    allowedMax: "1",
                    immediateRevoke: "true",
                    metaData: {
                        "deviceId": req.payload.deviceId,
                        "sessionId": "",
                        "storeId": (data[0] && data[0].storeId) ? data[0].storeId : ""
                    },
                    accessTTL: process.env.AUTH_ACCESS_EXPIRY_TIME,
                    refreshTTL: process.env.AUTH_REFRESH_EXPIRY_TIME
                }).then((data) => {


                    dataToSend["response"]["token"] = data.accessToken;
                    dataToSend["response"]["refreshToken"] = data.refreshToken;
                    dataToSend["response"]["accessExpiry"] = data.accessExpiry;
                    dataToSend["response"]["authToken"] = data.accessToken;
                    dataToSend["response"]["willTopic"] = data.accessToken;

                    //stream
                    let streamData = {
                        userName: dataToSend["response"]["userName"],
                        firstName: payload.firstName,
                        id: payload._id,
                        lastName: payload.lastName || payload.firstName,
                        userType: 1,
                        profilePic: payload.profilePic,
                        deviceType: 'Android',//get it from payload
                        mqttTopic: payload._id,
                        fcmTopic: payload._id
                    }


                    request.post({
                        headers: { 'content-type': 'application/json', 'lan': 'en' },
                        url: `${process.env.LIVE_STREAM_API}/user`,
                        body: streamData,
                        json: true
                    }, function (error, response, body) {
                        if (error)
                            logger.error('0000', error);
                        else {
                            if (response.statusCode == 200) {
                                let stream = body.data;
                                dataToSend["response"]["stream"] = stream;
                            }
                        }
                        return resolve(dataToSend)
                    });
                    fcm.notifyFcmTpic({
                        fcmTopic: payload._id,
                        // action: 1,
                        pushType: 2,
                        title: process.env.APP_NAME,
                        msg: "login",
                        data: { "type": "login", "deviceId": req.payload.deviceId },
                        deviceType: req.payload.deviceType
                    }, (err, result) => {
                        if (err) console.log(err)
                    });

                    //group call api
                    request.post({
                        headers: { 'content-type': 'application/json', 'lan': 'en' },
                        url: `${process.env.GROUP_CALL_API}/loginSignUp`,
                        body: {
                            id: payload._id,
                            userType: 1,
                            firstName: payload.firstName,
                            lastName: payload.lastName || payload.firstName,
                            profilePic: payload.profilePic,
                            deviceType: req.payload.deviceType,
                            pushKitToken: payload._id,
                            mqttTopic: payload._id,
                            fcmTopic: payload._id
                        },
                        json: true
                    }, function (error) {
                        if (error) {
                            logger.error('KLAPWDD : ', error);
                        }
                    });



                    /**
                    *  call Ecome API
                    */
                    // request.post({
                    //     headers: {
                    //         'content-type': 'application/json',
                    //         'authorization': '{"userId":"' + payload._id + '","userType":"user","metaData":{}}',
                    //         'platform': 1,
                    //         'currencysymbol': '$',
                    //         'currencycode': 'USD',
                    //         'language': 'en'
                    //     },
                    //     url: `${process.env.ECOME_API}/v1/ecomm/customerSyncDatum`,
                    //     body: {
                    //         "userId": payload._id,
                    //         "firstName": payload.firstName,
                    //         "lastName": payload.lastName,
                    //         "email": payload.email,
                    //         "password": "123456",
                    //         "countryCode": payload.countryCode,
                    //         "mobile": req.payload.mobile,
                    //         "dateOfBirth": "1997-06-08",
                    //         "profilePicture": payload.profilePic,
                    //         "latitude": "0",
                    //         "longitude": "0",
                    //         "ipAddress": "0.0.0.0",
                    //         "city": "Bangalore",
                    //         "country": "India"
                    //     },
                    //     json: true
                    // }, function (error, response, body) {
                    //     if (error) console.log("eror : KOIPLOIHDH ", error)
                    // });

                    delete payload._id


                });
        });
    }

    checkUserName()
        .then((data) => verifyPassword(data))
        .then((data) => login(data))
        .then((data) => {
            return res(data).code(data.code)
        })
        .catch((data) => {
            return res({ message: data.message }).code(data.code);
        })
}

const response = {
    // status: {
    //     201: { message: Joi.any().default(local['login']['200']) },
    //     400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    //     403: { message: Joi.any().default(local['genericErrMsg']['403']) },

    // }
}//swagger response code

module.exports = { payloadValidator, handler, response };