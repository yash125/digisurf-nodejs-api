const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const postsCollection = require("../../../models/posts");
const placesCollection = require("../../../models/places");
const postESCollection = require("../../../models/postES");
const channelCollection = require("../../../models/channel");
const hashTagCollection = require("../../../models/hashTags");
const categoryCollection = require("../../../models/category");
const dublyAudioCollection = require("../../../models/dublyAudio");
const messagesCollection = require("../../../models/messages");
const userListCollection = require("../../../models/userList");
const request = require('request');

/**
 * @description API to publish a post 
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} userId
 * @property {string} imageUrl1
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const validator = Joi.object({
    postId: Joi.string().required().description("postId"),
    title: Joi.string().default("").allow("").description("title"),
    categoryId: Joi.string().default("").allow("").description("categoryId"),
    channelId: Joi.string().default("").allow("").description("channelId"),
    hashTags: Joi.string().default("").allow("").description("hashTags"),
    longitude: Joi.number().default(0).description("longitude"),
    latitude: Joi.number().default(0).description("latitude"),
    place: Joi.string().default("").allow("").description("place"),
    placeId: Joi.string().default("").allow("").description("placeId"),
    countrySname: Joi.string().default("").allow("").description("countrySname. Eg. AU, US, IN, UK etc."),
    city: Joi.string().default("").allow("").description("city"),
    musicId: Joi.string().default("").allow("").description("musicId"),
    businessPostTypeId: Joi.string().allow("").description("businessPostTypeId"),
    businessPrice: Joi.number().description("businessPrice"),
    businessUrl: Joi.string().allow("").description("businessUrl"),
    businessCurrency: Joi.string().allow("").description("businessCurrency"),
    businessButtonText: Joi.string().allow("").description("businessButtonText"),
    businessButtonColor: Joi.string().allow("").description("businessButtonColor"),

    allowDownload: Joi.boolean().allow(null).default(true).description("allowDownload"),
    allowComment: Joi.boolean().allow(null).default(true).description("allowComment"),
    productIds: Joi.array().allow(null).default([]).description("productIds")

}).unknown();

const handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);

    let hashTagArr = [];
    let hashTagString = '';
    let hashTags = req.payload.hashTags.trim().toLowerCase();
    hashTagString = hashTags.replace(/,\s*$/, "");
    hashTagArr = hashTagString.split(',');
    hashTagArr = hashTagArr.map(e => {
        if (e[0] != "#") {
            return "#" + e;
        } else {
            return e;
        }
    });
    var channelImageUrl = "", channelName = "", channelStatus = "";
    var categoryUrl = "", categoryName = "", musicData = {}


    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve(true);
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    const getChannelDetaisIfrequred = () => {
        return new Promise((resolve, reject) => {
            let channelId = (req.payload.channelId) ? ObjectID(req.payload.channelId) : ObjectID();
            channelCollection.SelectOne({ _id: channelId }, (err, result) => {
                if (err) {
                    logger.error("at sendPushOnTagUsers: ", err);
                    return reject(err);
                } else {
                    if (result && result._id) {
                        channelImageUrl = result.channelImageUrl;
                        channelStatus = result.channelStatus;
                        channelName = result.channelName;
                    }
                    return resolve(true);
                }
            });
        });
    }
    const getCategoryDetails = () => {
        return new Promise((resolve, reject) => {
            let categoryId = (req.payload.categoryId) ? ObjectID(req.payload.categoryId) : ObjectID();
            categoryCollection.SelectOne({ _id: categoryId }, (err, result) => {
                if (err) {
                    logger.error("OAKOAKSAAA : ", err);
                    return reject(err);
                } else {
                    if (result && result._id) {
                        categoryUrl = result.categoryActiveIconUrl;
                        categoryName = result.categoryName;
                    }
                    return resolve(true);
                }
            });
        });
    }
    const getMusicDetails = () => {
        return new Promise((resolve, reject) => {
            let musicId = (req.payload.musicId) ? ObjectID(req.payload.musicId) : ObjectID();
            dublyAudioCollection.SelectOne({ _id: musicId }, (err, result) => {
                if (err) {
                    logger.error("OAKOAKSAAA : ", err);
                    return reject(err);
                } else {
                    if (result && result._id) {
                        musicData = result;
                    }
                    return resolve(true);
                }
            });
        });
    }
    const updatePost = () => {
        return new Promise((resolve, reject) => {

            let condition = { _id: ObjectID(req.payload.postId), userId: userId };
            let data = {};
            data["title"] = (req.payload.title) ? req.payload.title : "";
            if (req.payload.categoryId) data["categoryId"] = ObjectID(req.payload.categoryId);

            if (req.payload.businessPostTypeId) {
                data["business"] = {
                    businessPostType: ObjectID(req.payload.businessPostTypeId),
                    businessPrice: req.payload.businessPrice,
                    businessUrl: req.payload.businessUrl,
                    businessCurrency: req.payload.businessCurrency,
                    businessButtonText: req.payload.businessButtonText,
                    businessButtonColor: req.payload.businessButtonColor,
                    businessName: (userData.businessProfile && userData.businessProfile[0] &&
                        userData.businessProfile[0].businessName) ? userData.businessProfile[0].businessName : "",
                    businessProfilePic: (userData.businessProfile && userData.businessProfile[0] &&
                        userData.businessProfile[0].businessProfilePic) ? userData.businessProfile[0].businessProfilePic : userData.profilePic,
                    businessProfileCoverImage: (userData.businessProfile && userData.businessProfile[0] &&
                        userData.businessProfile[0].businessProfileCoverImage) ? userData.businessProfile[0].businessProfileCoverImage : userData.profileCoverImage
                }
            } else if (req.payload.businessPostTypeId == "5d709a21a463b51b6054894f") {
                data["business"] = {
                    businessPostType: req.payload.businessPostTypeId
                }
            }
            if (req.payload.channelId) {
                data["channelId"] = ObjectID(req.payload.channelId);
                data["channelImageUrl"] = channelImageUrl;
                data["channelName"] = channelName;
                data["channelStatus"] = channelStatus;
            } else {
                data["channelId"] = ""
                data["channelImageUrl"] = ""
                data["channelName"] = "";
                data["channelStatus"] = "";
            }
            if (req.payload.hashTags) data["hashTags"] = hashTagArr;
            if (req.payload.longitude) data["location.longitude"] = req.payload.longitude;
            if (req.payload.latitude) data["location.latitude"] = req.payload.latitude;
            // if (req.payload.place) data["place"] = req.payload.place;
            if (req.payload.countrySname) data["countrySname"] = req.payload.countrySname;
            if (req.payload.city) data["city"] = req.payload.city;
            if (categoryUrl != "") data["categoryUrl"] = categoryUrl;
            if (categoryName != "") data["categoryName"] = categoryName;
            if (musicData) data["musicData"] = musicData;
            if (req.payload && req.payload.productIds) data["productIds"] = req.payload.productIds;
            // if (req.payload.placeId) data["placeId"] = req.payload.placeId;

            data["isPrivate"] = userData.private || 0;
            // data["postStatus"] = 6;
            data["postStatus"] = 1;
            data["allowDownload"] = (req.payload && (req.payload.allowDownload || req.payload.allowDownload == false)) ? req.payload.allowDownload : false;
            data["allowComment"] = (req.payload && (req.payload.allowComment || req.payload.allowComment == false)) ? req.payload.allowComment : false;
            data["allowDuet"] = (req.payload && (req.payload.allowDuet || req.payload.allowDuet == false)) ? req.payload.allowDuet : false;
            data["productIds"] = (req.payload && req.payload.productIds) ? req.payload.productIds : [];

            data["place"] = req.payload.place;
            data["placeId"] = req.payload.placeId;

            postESCollection.Update(req.payload.postId, data, (e) => { console.log("error es ", e) })
            postsCollection.Update(condition, data, (e) => {
                if (e) {
                    let responseObj = {
                        message: req.i18n.__('genericErrMsg')['500']
                    };
                    return reject(responseObj);
                } else {
                    let responseObj = {
                        code: 200,
                        message: req.i18n.__('PutPost')['200']
                    };
                    hashTag();
                    return resolve(responseObj);
                }
            });

            try {
                request.post({
                    headers: {
                        'content-type': 'application/json',
                        'authorization': 'Basic M2VtYmVkOjAwNyQ=',
                        'language': 'en'
                    },
                    "rejectUnauthorized": false,
                    url: `https://api.dubly.xyz/content/moderator`,
                    body: {
                        "post_id": req.payload.postId,
                        "post_url": data.imageUrl1,
                        "post_content_type": (data.mediaType1 == 0) ? 1 : 2,
                        "post_text": data.title
                    },
                    json: true
                }, function (error, response, body) {
                    if (error) console.log("eror : KOIPLOIHDH ", error)

                    
                });

            } catch (error) {
                console.log("AWOPLOSP : ", error);
            }


            messagesCollection.Update({
                "postId": ObjectID(req.payload.postId)
            }, { "postTitle": data["title"] }, (e) => {
                if (e) {
                    logger.error(e)
                }
            })

            if (req.payload.place) {
                placesCollection.UpdateWithUpsert(
                    { place: req.payload.place },
                    {
                        placeId: req.payload.placeId,
                        place: req.payload.place, countrySname: req.payload.countrySname, city: req.payload.city
                    }, (e) => {
                        if (e) {
                            logger.error("  placesCollection.UpdateWithUpsert ", e)
                        }
                    });
            }

        });
    };
    const hashTag = (postData) => {
        return new Promise((resolve, reject) => {

            if (req.payload.hashTags) {

                hashTagArr.forEach((element) => {
                    try {
                        element = (element[0] == "#") ? element : "#" + element;
                        hashTagCollection.Update(
                            { _id: element },
                            { _id: element },
                            (e) => {
                                if (e) reject(e);
                            }
                        )
                    } catch (exception) {
                        console.log(exception);
                        reject(exception)
                    }
                });

            } return resolve(postData);

        });
    };

    getUserData()
        .then(() => { return getChannelDetaisIfrequred(); })
        .then(() => { return getCategoryDetails(); })
        .then(() => { return getMusicDetails(); })
        .then((data) => { return updatePost(data); })
        .then((data) => { return hashTag(data); })
        .then((data) => { return res({ message: data.message }).code(data.code); })
        .catch((error) => { return res(error).code(500); })

};

const response = {
    status: {
        200: { message: Joi.any().default(local['PutPost']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { validator, handler, response };