const Joi = require("joi");
const async = require("async");

const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../../locales');
// const rabbitMq = require('../../../library/rabbitMq');
const postsCollection = require("../../../../models/posts");


/**
 * @description API to delete a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} userId
 * @property {string} imageUrl1
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const queryValidator = Joi.object({
    postId: Joi.string().description('postId')
}).unknown();

const handler = (req, res) => {

    const userId = ObjectID(req.auth.credentials._id);
    const postId = req.query.postId;

    let condition = { "_id": ObjectID(postId) };

    postsCollection.Select(condition, (err, result) => {
        if (err) {
            console.log("err : ", err)
            let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
            return reject(responseobj);
        } else {
            return res({
                message: req.i18n.__('GetPost')['200'], data: {
                    allowDownload: (data && data.allowDownload && data.allowDownload == true) ? true : false,
                    allowComment: (data && data.allowComment && data.allowComment == true) ? true : false
                }
            });
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetPost']['200']), data: Joi.any().example(
                {
                    allowDownload: true,
                    allowComment: true
                })
        },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}//swagger response code


module.exports = { handler, response, queryValidator };