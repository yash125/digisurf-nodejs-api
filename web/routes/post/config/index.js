let headerValidator = require("../../../middleware/validator")
let GetAPI = require("./Get");

module.exports = [

    {
        method: 'Get',
        path: '/post/config',
        handler: GetAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'post'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },

 
];