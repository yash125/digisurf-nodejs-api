const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const moment = require("moment");
const local = require('../../../locales');
const fcm = require("../../../library/fcm");
const postES = require("../../../models/postES")
const postsCollection = require("../../../models/posts");
const placesCollection = require("../../../models/places");
const channelCollection = require("../../../models/channel");
const hashTagCollection = require("../../../models/hashTags");
const categoryCollection = require("../../../models/category");
const userListCollection = require("../../../models/userList");
const dublyAudioCollection = require("../../../models/dublyAudio");
const request = require('request');

// var sendPushUtility = require("../../../library/iosPush/sendPush.js");

/**
 * @description API to publish a post 
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} userId
 * @property {string} imageUrl1
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const validator = Joi.object({
    imageUrl1: Joi.string().required().description("imageUrl1"),
    thumbnailUrl1: Joi.string().required().description("thumbnailUrl1"),
    title: Joi.string().allow("").description("title"),
    hasAudio1: Joi.number().description("hasAudio1 (0: no audio, 1 : audio)"),
    mediaType1: Joi.number().required().description("mediaType1. 0 : Photo, 1 : Video"),
    musicId: Joi.string().description("musicId"),
    cloudinaryPublicId1: Joi.string().required().description("cloudinaryPublicId1"),
    categoryId: Joi.string().description("categoryId"),
    channelId: Joi.string().description("channelId"),
    story: Joi.boolean().required().description("story. false, true"),
    hashTags: Joi.string().description("hashTags"),
    orientation: Joi.number().default(0).description("orientation"),
    latitude: Joi.number().description("latitude"),
    longitude: Joi.number().description("longitude"),
    location: Joi.string().description("location"),
    placeId: Joi.string().description("placeId"),
    countrySname: Joi.string().default("").allow("").description("countrySname. Eg. AU, US, IN, UK etc."),
    city: Joi.string().default("").allow("").description("city"),
    taggedUserId: Joi.string().description('taggedUserId (comma separated ids)'),
    taggedUserCoordinates: Joi.string().description('taggedUserCoordinates (comma separated ids)'),
    imageUrl1Width: Joi.string().description('imageUrl1Width'),
    imageUrl1Height: Joi.string().description('imageUrl1Height'),
    cloudinary_public_id: Joi.string().description('cloudinary_public_id'),
    userTags: Joi.array().allow(null).description("userTag"),
    businessPostTypeId: Joi.string().allow("").description("businessPostTypeId"),
    businessPrice: Joi.number().description("businessPrice"),
    businessUrl: Joi.string().allow("").description("businessUrl"),
    businessCurrency: Joi.string().allow("").description("businessCurrency"),
    businessButtonText: Joi.string().allow("").description("businessButtonText"),
    businessButtonColor: Joi.string().allow("").description("businessButtonColor"),
    businessButtonColor: Joi.string().allow("").description("businessButtonColor"),
    allowDownload: Joi.boolean().allow(null).default(true).description("allowDownload"),
    allowComment: Joi.boolean().allow(null).default(true).description("allowComment"),
    productIds: Joi.array().allow(null).default([]).description("productIds"),
    allowDuet: Joi.boolean().allow(null).default(true).description("allowDuet"),

    cityForPost: Joi.string().allow(["", null]).default("").description("country"),
    countryForPost: Joi.string().allow(["", null]).default("").description("country"),
    latForPost: Joi.number().allow(null).default(0).description("latForPost"),
    longForPost: Joi.number().allow(null).default(0).description("longForPost"),

    orderDetails: Joi.any().allow(["", null]).default("").description("orderDetails"),

    buyerId: Joi.any().allow(["", null]).default("").description("buyerId"),
    sellerId: Joi.any().allow(["", null]).default("").description("sellerId"),
    orderId: Joi.any().allow(["", null]).default("").description("orderId"),
    visibleOnSellerProfile: Joi.boolean().allow([null]).default(false).description("visibleOnSellerProfile")


}).unknown();

const handler = (req, res) => {

    var userTagIds = [];
    const postId = new ObjectID();
    var userId = ObjectID(req.auth.credentials._id);
    var channelImageUrl = "", channelName = "", channelStatus = "";
    var categoryUrl = "", categoryName = "", musicData = {}

    if (req.payload && req.payload.orderDetails && req.payload.orderDetails.videoFor && req.payload.orderDetails.videoFor.userId) {
        userId = ObjectID(req.payload.orderDetails.videoFor.userId);
    }

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve(true);
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }
    const getChannelDetaisIfrequred = () => {
        return new Promise((resolve, reject) => {
            let channelId = (req.payload.channelId) ? ObjectID(req.payload.channelId) : ObjectID();
            channelCollection.SelectOne({ _id: channelId }, (err, result) => {
                if (err) {
                    logger.error("at sendPushOnTagUsers: ", err);
                    reject(err)
                } else {
                    if (result && result._id) {
                        channelImageUrl = result.channelImageUrl;
                        channelStatus = result.channelStatus;
                        channelName = result.channelName;
                    }
                    return resolve(true);
                }
            });
        });
    }
    const getCategoryDetails = () => {
        return new Promise((resolve, reject) => {
            let categoryId = (req.payload.categoryId) ? ObjectID(req.payload.categoryId) : "";
            categoryCollection.SelectOne({ _id: categoryId }, (err, result) => {
                if (err) {
                    logger.error("OAKOAKSAAA : ", err);
                    reject(err);
                } else {
                    if (result && result._id) {
                        categoryUrl = result.categoryActiveIconUrl;
                        categoryName = result.categoryName;
                    }
                    return resolve(true);
                }
            });
        });
    }
    const getMusicDetails = () => {
        return new Promise((resolve, reject) => {
            let musicId = (req.payload.musicId) ? ObjectID(req.payload.musicId) : ObjectID();
            dublyAudioCollection.SelectOne({ _id: musicId }, (err, result) => {
                if (err) {
                    logger.error("OAKOAKSAAA : ", err);
                    reject(err)
                } else {
                    if (result && result._id) {
                        musicData = result;
                    }
                    return resolve(true);
                }
            });
        });
    }
    const post = () => {
        return new Promise((resolve, reject) => {

            // postStatus: 6 == pending,7 == rejected

            var data = {


                title: req.payload.title || "",
                imageUrl1: req.payload.imageUrl1.trim(),
                thumbnailUrl1: req.payload.thumbnailUrl1.trim(),
                mediaType1: parseInt(req.payload.mediaType1),
                userId: userId,
                createdOn: new Date(),
                timeStamp: new Date().getTime(),
                distinctViews: [],
                totalViews: [],
                comments: [],
                imageUrl1Width: req.payload.imageUrl1Width || null,
                imageUrl1Height: req.payload.imageUrl1Height || null,
                likesCount: 0,
                mentionedUsers: userTagIds,
                // postStatus: 6,
                postStatus: 1,
                shareCount: 0,
                commentCount: 0,
                cloudinary_public_id: req.payload.cloudinary_public_id,

                userName: userData.userName,
                firstName: userData.firstName,
                lastName: userData.lastName,
                fullName: userData.firstName + userData.lastName,
                fullNameWithSpace: userData.firstName + " " + userData.lastName,
                profilepic: userData.profilePic,
                profileCoverImage: userData.profileCoverImage || userData.profilePic,

                channelId: (req.payload.channelId) ? ObjectID(req.payload.channelId) : "",
                channelImageUrl: channelImageUrl,
                channelName: channelName,
                channelStatus: channelStatus,

                categoryId: (req.payload.categoryId) ? ObjectID(req.payload.categoryId) : "",
                categoryName: categoryName,
                categoryUrl: categoryUrl,
                musicId: (req.payload.musicId) ? ObjectID(req.payload.musicId) : '',
                musicData: musicData,

                location: {
                    longitude: parseFloat(req.payload.longitude) || 0,
                    latitude: parseFloat(req.payload.latitude) || 0
                },
                place: req.payload.location || '',
                countrySname: req.payload.countrySname || '',
                city: req.payload.city || '',
                placeId: req.payload.placeId || "",
                likes: [],
                orientation: req.payload.orientation,
                isStar: (userData && userData.starRequest && userData.starRequest.starUserProfileStatusCode && userData.starRequest.starUserProfileStatusCode == 4),

                trending_score: 0,
                allowDownload: (req.payload && (req.payload.allowDownload || req.payload.allowDownload == false)) ? req.payload.allowDownload : false,
                allowComment: (req.payload && (req.payload.allowComment || req.payload.allowComment == false)) ? req.payload.allowComment : false,
                allowDuet: (req.payload && (req.payload.allowDuet || req.payload.allowDuet == false)) ? req.payload.allowDuet : false,

                productIds: (req.payload && req.payload.productIds) ? req.payload.productIds : [],

                cityForPost: req.payload.cityForPost,
                countryForPost: req.payload.countryForPost,

                latForPost: req.payload.latForPost,
                longForPost: req.payload.longForPost,
                locationForPost: {
                    lat: req.payload.latForPost,
                    lon: req.payload.longForPost
                },
                isPrivate: userData.private || 0,
                shoutOutFor: (req.payload && req.payload.orderDetails && req.payload.orderDetails.videoFor && req.payload.orderDetails.videoFor.userId) ? req.payload.orderDetails.videoFor.userId : "",

                buyerId: req.payload.buyerId,
                sellerId: req.payload.sellerId,
                orderId: req.payload.orderId,
                visibleOnSellerProfile: req.payload.visibleOnSellerProfile
            };          
            



            if (req.payload.businessPostTypeId) {
                data["business"] = {
                    businessPostType: ObjectID(req.payload.businessPostTypeId),
                    businessPrice: req.payload.businessPrice,
                    businessUrl: req.payload.businessUrl,
                    businessCurrency: req.payload.businessCurrency,
                    businessButtonText: req.payload.businessButtonText,
                    businessButtonColor: req.payload.businessButtonColor,
                    businessName: (userData.businessProfile && userData.businessProfile[0] &&
                        userData.businessProfile[0].businessName) ? userData.businessProfile[0].businessName : "",
                    businessProfilePic: (userData.businessProfile && userData.businessProfile[0] &&
                        userData.businessProfile[0].businessProfilePic) ? userData.businessProfile[0].businessProfilePic : userData.profilePic,
                    businessProfileCoverImage: (userData.businessProfile && userData.businessProfile[0] &&
                        userData.businessProfile[0].businessProfileCoverImage) ? userData.businessProfile[0].businessProfileCoverImage : userData.profileCoverImage
                }
            }

            if (req.payload.taggedUserId && req.payload.taggedUserCoordinates) {
                let taggedUserIdArr = req.payload.taggedUserId.split(',');
                let taggedUserCoordinatesArr = req.payload.taggedUserCoordinates.split(',');
                let flag = taggedUserIdArr.length === taggedUserCoordinatesArr.length ? true : false;
                if (!flag) reject({ message: "tagged users coordinate and tagged users don't match" });
                data.taggedUsers = [];
                let time = moment().valueOf();
                for (let i = 0; i < taggedUserIdArr.length; i++) {
                    data.taggedUsers.push({
                        userId: ObjectID(taggedUserIdArr[i].trim()),
                        coordinates: taggedUserCoordinatesArr[i],
                        created: time
                    });
                }
            }

            let hashTagString = '';
            let hashTagArr = [];
            if (req.payload.hashTags) {
                hashTagString = req.payload.hashTags.replace(/\s/g, '').toLowerCase();
                hashTagString = hashTagString.replace(/,\s*$/, ""); //remove comma and blank space from end of string
                hashTagArr = hashTagString.split(',');
                hashTagArr = hashTagArr.map(e => {
                    if (e[0] != "#") {
                        return "#" + e;
                    } else {
                        return e;
                    }
                });
                data.hashTags = hashTagArr;
            }
            if (req.payload.mediaType1 && req.payload.mediaType1 === 1) data.hasAudio1 = req.payload.hasAudio1 || 0;


            postES.Insert(postId.toString(), data, (e, r) => {
                if (e) {
                    console.log("error ES ", e)
                    data["locationForPost"]["lat"] = 0;
                    data["locationForPost"]["lon"] = 0;

                    postES.Insert(postId.toString(), data, (e, r) => {
                        if (e) {
                            console.log("error ES ", e)
                        }
                    })
                }
            });
            data["_id"] = postId;
            data["orderDetails"] = req.payload.orderDetails || {}
            postsCollection.Insert(data, (e, d) => {
                if (e) {
                    let responseObj = { message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseObj);
                } else {
                    if (!req.payload.channelId) {

                        postsCollection.GetPostCount({ userId: userId, "postStatus": 1, channelId: "" }, (err, result) => {
                            if (err) {
                                return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                            } else {
                                userListCollection.UpdateById(userId.toString(), { "count.postsCount": result }, (e) => { logger.error((e) ? e : "") });
                            }
                        });
                    }
                    let responseObj = {
                        message: req.i18n.__('PostAPI')['200'],
                        data: d,
                        newData: {
                            "isPrivate": userData.private || 0,
                            "_id": data._id,
                            "title": data.title,
                            "imageUrl1": data.imageUrl1,
                            "thumbnailUrl1": data.thumbnailUrl1,
                            "mediaType1": data.mediaType1,
                            "userId": data.userId,
                            "createdOn": data.createdOn,
                            "timeStamp": data.timeStamp,
                            "distinctViews": 0,
                            "totalViews": [],
                            "comments": [],
                            "imageUrl1Width": data.imageUrl1Width,
                            "imageUrl1Height": data.imageUrl1Height,
                            "likesCount": 0,
                            "mentionedUsers": [],
                            "postStatus": 1,
                            "shareCount": 0,
                            "commentCount": 0,
                            "cloudinary_public_id": data.cloudinary_public_id,
                            "userName": data.userName,
                            "firstName": data.firstName,
                            "lastName": data.lastName,
                            "fullName": data.fullName,
                            "fullNameWithSpace": data.fullNameWithSpace,
                            "profilepic": data.profilepic,
                            "profileCoverImage": data.profileCoverImage,
                            "channelId": data.channelId,
                            "channelImageUrl": data.channelImageUrl,
                            "channelName": data.channelName,
                            "channelStatus": data.channelStatus,
                            "categoryId": data.categoryId,
                            "categoryName": data.categoryName,
                            "categoryUrl": data.categoryUrl,
                            "musicId": data.musicId,
                            "location": data.location,
                            "place": data.place,
                            "countrySname": data.countrySname,
                            "city": data.city,
                            "placeId": data.placeId,
                            "orientation": data.orientation,
                            "totalViewCount": 0,
                            "distinctViewsCount": 0,
                            "allowDownload": data.allowDownload,
                            "allowDuet": data.allowDuet,
                            "postStatusText": "active",
                            "hashTags": data.hashTags,
                            "isBookMarked": false,
                            "followStatus": 0,
                            "liked": false,
                            "postId": data._id
                        }
                    };
                    hashTag();
                    return resolve(responseObj);
                }
            });
            if (req.payload.location) {
                placesCollection.UpdateWithUpsert(
                    { place: req.payload.location },
                    {
                        placeId: req.payload.placeId,
                        place: req.payload.location, countrySname: req.payload.countrySname, city: req.payload.city,
                        location: {
                            longitude: parseFloat(req.payload.longitude) || 0,
                            latitude: parseFloat(req.payload.latitude) || 0
                        }
                    }, (e) => {
                        if (e) {
                            logger.error("  placesCollection.UpdateWithUpsert ", e)
                        }
                    });
            }

            try {
                request.post({
                    headers: {
                        'content-type': 'application/json',
                        'authorization': 'Basic M2VtYmVkOjAwNyQ=',
                        'language': 'en'
                    },
                    "rejectUnauthorized": false,
                    url: `https://api.dubly.xyz/content/moderator`,
                    body: {
                        "post_id": postId,
                        "post_url": data.imageUrl1,
                        "post_content_type": (data.mediaType1 == 0) ? 1 : 2,
                        "post_text": data.title
                    },
                    json: true
                }, function (error, response, body) {
                    if (error) console.log("eror : KOIPLOIHDH ", error)

                });

            } catch (error) {
                console.log("AWOPLOSP : ", error);
            }



        });
    };
    const hashTag = (postData) => {
        return new Promise((resolve, reject) => {
            let hashTagArr = [];
            let hashTagString = '';
            if (req.payload.hashTags) {
                let hashTags = req.payload.hashTags.trim().toLowerCase();

                hashTagString = hashTags.replace(/,\s*$/, "");
                hashTagArr = hashTagString.split(',');
                hashTagArr.forEach((element) => {
                    try {
                        element = (element[0] == "#") ? element : "#" + element;
                        hashTagCollection.UpdateWithUpsert(
                            { _id: element },
                            { _id: element },
                            (e) => {
                                if (e) throw e;
                            }
                        )
                    } catch (exception) {
                        console.log(exception);
                        reject(exception)
                    }
                });

            } resolve(postData);

        });
    };
    const sendPushOnTagUsers = () => {
        return new Promise((resolve, reject) => {
            if (req.payload.userTags) {
                userListCollection.Select({ "userName": { "$in": req.payload.userTags } }, (err, result) => {
                    if (err) {
                        logger.error("at sendPushOnTagUsers: ", err);
                        reject(err);
                    }
                    result.forEach(element => {
                        if (element._id != userId.toString()) {

                            userTagIds.push({ userName: element.userName, userId: ObjectID(element._id.toString()) })
                            let request = {
                                fcmTopic: element._id,
                                // action: 1,
                                pushType: 2,
                                title: process.env.APP_NAME,
                                msg: userData.userName + " tagged you in a post on " + process.env.APP_NAME,
                                data: { "type": "newPost", "postId": postId, userImageUrl: userData.profilePic, imageUrl: req.payload.imageUrl1.trim() },
                                deviceType: (element && element.deviceInfo && element.deviceInfo.deviceType) ? element.deviceInfo.deviceType : "2"
                            }
                            fcm.notifyFcmTpic(request, () => { })

                            // let message = { alert: 'NewPost', msg: request.msg };
                            // sendPushUtility.iosPushIfRequiredOnMessage({
                            //     targetId: element._id, message: message, userId: userData._id.toString(),
                            //     "type": "newPost", "postId": postId, userImageUrl: userData.profilePic, imageUrl: req.payload.imageUrl1.trim()
                            // });
                        }
                    });
                    return resolve(true);
                });
            } else {
                return resolve(true);
            }
        });
    };

    getUserData()
        .then(() => { return sendPushOnTagUsers(); })
        .then(() => { return getChannelDetaisIfrequred(); })
        .then(() => { return getCategoryDetails(); })
        .then(() => { return getMusicDetails(); })
        .then(() => { return post(); })
        .then((data) => { return hashTag(data); })
        .then((data) => { return res(data).code(200); })
        .catch((error) => { return res(error).code(500); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['PostAPI']['200']), data: Joi.any(), newData: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { validator, handler, response };