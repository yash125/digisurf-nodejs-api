let headerValidator = require("../../middleware/validator")
let PutAPI = require('./Put');
let DeleteAPI = require('./Delete');
let PostAPI = require("./Post");
let GetAPI = require("./Get");

module.exports = [
    {
        method: 'Put',
        path: '/post',
        handler: PutAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'post'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response
        }
    },
    {
        method: 'Get',
        path: '/post',
        handler: GetAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'post'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
    {
        method: 'Post',
        path: '/post/media',
        handler: PostAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'post'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: PostAPI.response
        }
    },
    {
        method: 'Delete',
        path: '/post',
        handler: DeleteAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'post'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: DeleteAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    }
].concat(require("./config"));