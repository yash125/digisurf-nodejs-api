const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const postsCollection = require("../../../models/posts");
const userListCollection = require("../../../models/userList");
const messagesCollection = require("../../../models/messages");
const postESCollection = require("../../../models/postES");
const local = require('../../../locales');
const mqtt = require('../../../library/mqtt');

var cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
    api_key: process.env.CLOUDINARY_API_KEY,
    api_secret: process.env.CLOUDINARY_API_SECRET
});


/**
 * @description API to delete a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @returns 200 : Success
 * @returns 400 : Parameters missing
 * @returns 500 : Internal Server Error
 */
const validator = Joi.object({
    postId: Joi.string().required().description("postId"),
}).required();

const handler = (req, res) => {
    let userId = ObjectID(req.auth.credentials._id);

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve(true);
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }
    /* chnage postStatus = 2 in elasticsearc and in mongodb */
    const DeletePost = () => {
        return new Promise((resolve, reject) => {



            let condition = { _id: ObjectID(req.query.postId), userId: userId }
            let data = { postStatus: 2, deletedOnTs: new Date().getTime(), deletedOn: new Date() };
            /* update in elastic search */
            postESCollection.Update(req.query.postId, data, (e) => { console.log("AOKOAKS ES : ", e) })
            /*update in mongo DB */
            postsCollection.Update(condition, data, (e) => {
                if (e) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                }
                return resolve(true);
            });
        });
    };
    /* update in messsage   collection */
    const UpdateMessagesCollection = () => {
        return new Promise((resolve, reject) => {
            let condition = { postId: ObjectID(req.query.postId) };
            let dataToUpdate = { "messageType": "11", "payload": "VGhlIG1lc3NhZ2UgaGFzIGJlZW4gcmVtb3ZlZA==", "removedAt": "" + new Date().getTime() }
            messagesCollection.Update(condition, dataToUpdate, (e) => {
                if (e) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    return resolve({ message: req.i18n.__('DeletePost')['200'] });
                }
            });

        });
    };

    /* update in messsage   collection */
    const sendInMqtt = () => {
        return new Promise((resolve, reject) => {
            let condition = { postId: ObjectID(req.query.postId) };
            messagesCollection.Select(condition, (e, d) => {
                if (e) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                }
                else if (d) {

                    d.forEach(element => {
                        mqtt.publish("Message/" + element.senderId, JSON.stringify(
                            {
                                receiverIdentifier: element.name,
                                from: element.senderId,
                                to: element.receiverId,
                                payload: 'VGhlIG1lc3NhZ2UgaGFzIGJlZW4gcmVtb3ZlZA==',
                                toDocId: element.toDocId,
                                timestamp: "" + new Date().getTime(),
                                id: element.messageId,
                                name: element.name,
                                type: '11',
                                removedAt: "" + new Date().getTime(),
                                userImage: element.userImage,
                                isStar: false
                            }
                        ), { qos: 1 }, (e) => {
                            if (e) logger.error(e)
                        });

                        mqtt.publish("Message/" + element.receiverId, JSON.stringify(
                            {
                                receiverIdentifier: userData.userName,
                                from: element.senderId,
                                to: element.receiverId,
                                payload: 'VGhlIG1lc3NhZ2UgaGFzIGJlZW4gcmVtb3ZlZA==',
                                toDocId: element.toDocId,
                                timestamp: "" + new Date().getTime(),
                                id: element.messageId,
                                name: element.name,
                                type: '11',
                                removedAt: "" + new Date().getTime(),
                                userImage: element.userImage,
                                isStar: false
                            }
                        ), { qos: 1 }, (e) => {
                            if (e) logger.error(e)
                        });
                    });
                    return resolve({ message: req.i18n.__('DeletePost')['200'] });
                }
                else {
                    return resolve({ message: req.i18n.__('DeletePost')['200'] });
                }
            });
        });
    };

    /* remove post from cloudinary */
    const removePostFromCloudinary = () => {
        return new Promise((resolve, reject) => {
            /*update in mongo DB */
            postsCollection.SelectOne({ _id: ObjectID(req.query.postId), userId: userId }, (e, result) => {
                if (e) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                }
                if (result && result._id && result.cloudinary_public_id) {
                    cloudinary.v2.uploader.destroy(result.cloudinary_public_id, function (error, result) {
                        console.log(result, error)
                    });
                }
                return resolve(true);
            });

        });
    };

    getUserData()
        .then(() => { return removePostFromCloudinary(); })
        .then(() => { return DeletePost(); })
        .then(() => { return sendInMqtt(); })
        .then(() => { return UpdateMessagesCollection(); })
        .then((data) => { return res(data).code(200); })
        .catch((error) => { return res(error).code(500); })

};

const response = {
    status: {
        200: { message: Joi.any().default(local['DeletePost']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { validator, handler, response };