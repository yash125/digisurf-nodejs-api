const Joi = require("joi");
const async = require("async");

const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

// const rabbitMq = require('../../../library/rabbitMq');
const postsCollection = require("../../../models/posts");
const postsES = require("../../../models/postES");
const userCollection = require("../../../models/userList");
const followCollection = require("../../../models/follow");
const postDistinctViews = require("../../../models/postDistinctViews");
const businessProductTypeCollection = require("../../../models/businessProductType");

/**
 * @description API to delete a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} userId
 * @property {string} imageUrl1
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const queryValidator = Joi.object({
    postId: Joi.string().required().description('postId'),
    city: Joi.string().description('city'),
    country: Joi.string().description('country'),
    ipaddress: Joi.string().description('ipaddress'),
    latitude: Joi.string().description('latitude'),
    longitude: Joi.string().description('longitude')
}).unknown();

const handler = (req, res) => {

    const userId = ObjectID(req.auth.credentials._id);
    const postId = req.query.postId;
    let timestamp = new Date().getTime();
    var followStatus = {}, businessPostTypeLabel = {};
    var isPostVideo = 1;

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    console.log("error : ", err)
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })

        });
    }
    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(userId), "end": false, "followee": { "$nin": userData.blocked || [] } };
            followCollection.Select(condition, (e, result) => {
                if (e) {
                    console.log("error ", e)
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        followStatus[element.followee.toString()] = element.type
                    }, this);
                    return resolve(true);
                }
            });
        });
    }
    function getBusinessPostTypeLabel() {
        return new Promise((resolve, reject) => {
            businessProductTypeCollection.Select({}, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    result.forEach(element => {
                        businessPostTypeLabel[element._id.toString()] = element.text[req.headers.lang]
                    });
                }
                return resolve(true);
            });
        });
    }
    const getPosts = () => {
        return new Promise((resolve, reject) => {

            let condition = { "_id": ObjectID(postId) };

            postsCollection.Select(condition, (err, result) => {
                if (err) {
                    console.log("err : ", err)
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    var postByUserId = "";

                    result.forEach(function (element) {
                        if (element["business"] && element["business"]["businessPostType"]) element["business"]["businessPostTypeLabel"] = businessPostTypeLabel[element["business"]["businessPostType"]] || "";
                        element["comments"] = (element["comments"]) ? element["comments"].slice(element["comments"].length - 3).reverse() : [];
                        element["hashTags"] = [];
                        element["totalViews"] = [];
                        element["mentionedUsers"] = [];
                        element["isBookMarked"] = (userData && userData.bookmarks && userData.bookmarks.map(e => e.toString()).includes(element._id.toString())) ? true : false;
                        isPostVideo = element["mediaType1"] || 0;
                        element["distinctViews"] = (element["distinctViews"] && element["distinctViews"].length) ? element["distinctViews"].length : 0
                        element["followStatus"] = (followStatus[element["userId"].toString()]) ? followStatus[element["userId"].toString()]["status"] : 0;
                        element["liked"] = (element["likes"]) ? element["likes"].map(id => id.toString()).includes(userId.toString()) : false;
                        postByUserId = element["userId"];
                        element["postId"] = element["_id"];
                        if (element["musicId"] == "" || element["musicId"] == null) delete element["musicData"]
                        if (element["channelId"] && element["channelId"].length) element["isChannelSubscribed"] = (userData && userData.subscribeChannels && userData.subscribeChannels.map(e => e.toString()).includes(element.channelId.toString())) ? 1 : 0;
                        delete element["likes"];
                    }, this);

                    async.series([
                        function (callback) {
                            let dataToUpdate = {
                                totalViews: {
                                    userId: userId,
                                    timestamp: timestamp
                                }
                            };
                            let condition = {
                                _id: ObjectID(postId)
                            };
                            postsCollection.UpdateByIdWithPush(condition, dataToUpdate, (e, d) => {
                                callback(e, d);
                            });
                        },
                        function (callback) {
                            let dataToUpdate = {
                                "distinctViews": userId
                            };
                            let condition = {
                                _id: ObjectID(postId)
                            };
                            postsCollection.UpdateByIdWithAddToSet(condition, dataToUpdate, (e, d) => {

                                d = JSON.parse(d);
                                if (d.nModified && isPostVideo) {
                                    postsES.UpdateWithPush(postId, "distinctViews", userId.toString(), () => { });
                                    // rabbitMq.sendToQueue(rabbitMq.view_post_mqtt_queue, { "userId": userId.toString(), "userName": userData.userName || "user", "userLang": req.headers.lang, "starUserId": postByUserId }, () => { })
                                }
                                if (d.nModified) {
                                    postsES.UpdateWithPush(postId, "distinctViews", userId.toString(), () => { });
                                    postsCollection.UpdateWithIncrice({ _id: ObjectID(postId) }, { "distinctViewsCount": 1 }, (err) => {
                                        if (err) {
                                            console.log("err   AASASAD : ", err)
                                        }
                                    });
                                    postDistinctViews.Insert({
                                        postId: ObjectID(postId),
                                        timestamp: new Date().getTime(),
                                        createAt: new Date(),
                                        city: req.query.city,
                                        country: req.query.country,
                                        ipaddress: req.query.ipaddress,
                                        longitude: req.query.longitude,
                                        latitude: req.query.latitude
                                    }, (err) => {
                                        if (err) {
                                            console.log("err APLEOCKD : ", err)
                                        }
                                    })
                                }
                                postsCollection.UpdateWithIncrice({ _id: ObjectID(postId) }, { "totalViewCount": 1 }, (err) => {
                                    if (err) {
                                        console.log("err : ", err)
                                    }
                                });
                                callback(e, d);
                            });
                        }
                    ]);
                    let responseobj = { message: req.i18n.__('GetPost')['200'], data: result[0] };
                    return resolve(responseobj);
                }
            });
        });
    };

    getUserData()
        .then(() => { return getBusinessPostTypeLabel() })
        .then(() => { return getFolloweeIds() })
        .then(() => { return getPosts() })
        .then((data) => { return res({ message: req.i18n.__('GetPost')['200'], data: data.data }).code(200); })
        .catch((error) => {
            console.log("error", error)
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        })
};

const response = {
    // status: {
    //     200: {
    //         message: Joi.any().default(local['GetPost']['200']), data: Joi.any()
    //     },
    //     400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    //     500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    // }
}//swagger response code


module.exports = { handler, response, queryValidator };