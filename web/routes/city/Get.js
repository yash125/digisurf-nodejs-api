'use strict'
const Joi = require("joi");
const Promise = require('promise');
const local = require('../../../locales');
var cities = require('cities');

const validator = Joi.object({
    ip: Joi.string().required().description("ip, query parameter").error(new Error("ip is required")),
    lat: Joi.string().description("lat, query parameter"),
    long: Joi.string().description("long, query parameter"),
});

const hander = (req, res) => {

    let ip = req.query.ip;
    let lat = parseInt(req.query.lat) || 0;
    let long = parseInt(req.query.long) || 0;


    const getMyCityCountry = () => {
        return new Promise((resolve, reject) => {

            const iplocation = require("iplocation").default;
            if (lat == 0 || long == 0) {
                iplocation(ip, [], (error, result) => {

                    if (result && result.city) {
                        let data = {
                            city: result.city,
                            country: result.country,
                            countryCode: result.countryCode
                        }
                        return resolve({ message: req.i18n.__('GetCity')['200'], data: data, code: 200 })
                    } else {
                        return reject({ message: req.i18n.__('GetCity')['204'], code: 204 })
                    }
                });
            } else {
                let result = cities.gps_lookup(lat, long);
                if (result && result.city) {
                    let data = {
                        city: result.city
                    }
                    return resolve({ message: req.i18n.__('GetCity')['200'], data: data, code: 200 })
                } else {
                    return reject({ message: req.i18n.__('GetCity')['204'], code: 204 })
                }

            }
        });
    };

    getMyCityCountry()
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetCity']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['GetCity']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code


module.exports = { validator, hander, response };