const ENV = require('./ENV');
const auth = require('./auth');
const home = require('./home');
const post = require('./post');
const like = require('./like');
const story = require('./story');
const login = require('./login');
// const wallet = require('./wallet');
const logout = require('./logout')
const unLike = require('./unLike');
const blocks = require('./blocks');
const follow = require('./follow');
const friend = require('./friend');
const profile = require('./profile');
const channel = require('./channel');
const comment = require('./comment');
const userTag = require('./userTag');
const activity = require('./activity');
const hashTags = require('./hashTags');
const category = require('./category');
const unfollow = require('./unfollow');
const postViews = require('./postViews');
const followers = require('./followers');
const followees = require('./followees');
const followAll = require('./followAll');
const musicList = require('./musicList');
const newPassword = require('./newPassword');
const starRequest = require('./starRequest');
const hashTagList = require('./hashTagList');
const signUp = require('./../routes/signUp');
const verifySignUp = require('./verifySignUp');
const verifyResetOtp = require('./verifyOTPByEmail');
const postsByPlace = require('./postsByPlace');
const referralCode = require('./referralCode');
const postsByMusic = require('./postsByMusic');
const searchPeople = require('./searchPeople');
const followRequest = require('./followRequest');
const musicCategory = require('./musicCategory');
const searchExplore = require('./searchExplore');
const reportReasons = require('./reportReasons');
const forgotPassword = require('./forgotPassword');
const searchDiscover = require('./searchDiscover');
const favouriteMusic = require('./favouriteMusic');
const postsByHashTag = require('./postsByHashTag');
const postsByChannel = require('./postsByChannel');
const postsByCategory = require('./postsByCategory');
const storyFromContact = require('./storyFromContact');
const subscribeChannel = require('./subscribeChannel');
const followingActivity = require('./followingActivity');
const userReportReasons = require('./userReportReasons');
const requestedChannels = require('./requestedChannels');
const followersFollowee = require('./followersFollowee');
const unSubscribeChannel = require('./unSubscribeChannel');
const searchByStarChatId = require('./searchByStarChatId');
const starCategories = require('./starCategories');
const requestEmailVerification = require('./requestEmailVerification');
const starStatus = require('./starStatus');
const hideMyPost = require('./hideMyPost');
const verifyEmail = require('./verifyEmail');
const verifyStarNumber = require('./verifyStarNumber');
const starUsers = require('./starUsers');
const starUserChat = require('./starUserChat');
const city = require('./city');
const user = require('./user');
const Hola_API_MODULE = require('../../Hola_API_MODULE');
const password = require('./password');
const stream = require('./stream');


module.exports = [].concat(
    require('./admin'),
    require('./cognitoToken'),
    require('./livestream'),
    require('./latlong'),
    require('./isExists'),
    require('./groupCallStreamId'),
    require('./iceServers'),
    require('./isPasswordMatch'),
    require('./googleKey'),
    require('./currency'),
    require('./isRegisteredNumber'),
    require("./bussiness"),
    require("./bookMark"),
    require("./collection"),
    require("./bizButtons"),
    require("./webhook"),
    require("./section"),
    verifySignUp, user,
    activity, auth, blocks, channel, comment, searchDiscover, searchExplore,
    follow, unfollow, followers, followees, followAll, hashTags, hashTagList, home, like, unLike,
    postsByPlace, post, postsByHashTag, postsByCategory, postsByChannel, reportReasons,
    requestedChannels, story, subscribeChannel, unSubscribeChannel, profile,
    searchPeople, category, followingActivity, Hola_API_MODULE, userTag,
    followRequest, userReportReasons, favouriteMusic, musicCategory, musicList, postsByMusic,
    storyFromContact, referralCode, friend, searchByStarChatId, starRequest,
    starCategories, starStatus, hideMyPost, requestEmailVerification, verifyEmail,
    starUsers, verifyStarNumber, starUserChat, ENV, city, password,
    followersFollowee, signUp, login, postViews, forgotPassword, verifyResetOtp, newPassword, logout,
    stream

);