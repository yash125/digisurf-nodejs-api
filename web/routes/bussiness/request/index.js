let headerValidator = require("../../../middleware/validator");
let PostAPI = require('./Post');
let PUTAPI = require('./Put');

module.exports = [
    {
        method: 'POST',
        path: '/BusinessRequest',
        handler: PostAPI.handler,
        config: {
            description: `this api is used to submit a request for a bussiness categories`,
            tags: ['api', 'businessProfile'],
            auth: "user",
            validate: {
                payload:PostAPI.payloadValidator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
    {
        method: 'PUT',
        path: '/BusinessRequest',
        handler: PUTAPI.handler,
        config: {
            description: `this api is used to edit bussiness Profile`,
            tags: ['api', 'businessProfile'],
            auth: "user",
            validate: {
                payload:PUTAPI.payloadValidator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PUTAPI.response
        }
    }
];