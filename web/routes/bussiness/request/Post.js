'use strict'
const Joi = require("joi");
Joi.objectId = require('joi-objectid')(Joi);
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const check = require('../../../../library/checkEmpty');

var uploadFile = require("../../../../library/uploadImage").upload;
const local = require('../../../../locales');
const userModel = require('../../../../models/userList');
const bussinessModel = require('../../../../models/bussinessCategory');
/**
 * @method Post BusinessRequest
 * @description API to create a business profile
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} postId
 * @property {string} bussinessBio
 * @property {string} businessName
 * @property {string} email
 * @property {string} phone
 * @property {string} countryCode
 * @property {string} businessCategory
 * @property {bool} privateAccount
 * @property {string} websiteURL
 * @property {string} address
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 500 : Internal Server Error
 * @returns 412 : businessId is invalid
 * @returns 409 : email already used
 */

/**payload */
const payloadValidator = Joi.object({
    businessBio: Joi.string().description("bussinesss bio"),
    businessName: Joi.string().required().description("name of the bussiness").error(new Error('name of the bussiness is required')),
    email: Joi.string().email({ minDomainAtoms: 2 }).description("email").required().error(new Error('email is required or not valid')),
    phone: Joi.string().description("phone number").required().error(new Error('phone number is required')),
    countryCode: Joi.string().description("country code").required().error(new Error('country code is required')),
    businessCategoryId: Joi.objectId().description("id of the business category").required().error(new Error('businessCategoryId must be an valid objectId')),
    privateAccount: Joi.bool().default('false').description('is it a private account or not'),
    websiteURL: Joi.string().description("website link"),
    address: Joi.string().description("address is required").required().error(new Error('address is required')),
    isPhoneNumberVerified: Joi.number().description("is phone verified").required().error(new Error('verifed is required')),
    isEmailVerified: Joi.number().description("is email verified").required().error(new Error('verifed is required')),
    isVisibleEmail: Joi.number().description("is visible email ").required().error(new Error('is visible email  required')),
    isVisiblePhone: Joi.number().description("is visible phone").required().error(new Error('is visible phone  required')),

    businessProfilePic: Joi.string().default("").allow("").description("businessProfilePic"),
    businessProfileCoverImage: Joi.string().default("").allow("").description("businessProfileCoverImage"),

    businessStreet: Joi.string().default("").allow("").description("businessStreet"),
    businessCity: Joi.string().default("").allow("").description("businessCity"),
    businessZipCode: Joi.string().default("").allow("").description("businessZipCode"),
    businessLat: Joi.string().default("").allow("").description("businessLat"),
    businessLng: Joi.string().default("").allow("").description("businessLng")
}).unknown();


/*handler starts here*/
const handler = (req, res) => {

    let Email = req.payload.email;
    let BusinessName = req.payload.businessName;
    let Phone = req.payload.phone;
    let countryCode = req.payload.countryCode;
    var businessCategoryId = ObjectID(req.payload.businessCategoryId);
    let websiteURL = req.payload.websiteURL || '';
    var _id = req.auth.credentials._id;
    let address = req.payload.address;

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userModel.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;

                    // if (req.payload.businessProfilePic == "" || req.payload.businessProfilePic == null) {
                    //     req.payload.businessProfilePic = userData.profilePic
                    // }
                    // if (req.payload.businessProfileCoverImage == "" || req.payload.businessProfileCoverImage == null) {
                    //     req.payload.businessProfileCoverImage = userData.profileCoverImage
                    // }
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }
    //email should be unique in whole db
    let checkEmail = () => {
        return new Promise((resolve, reject) => {
            userModel.Select({ $or: [{ "email.id": Email }, { "businessProfile.email.id": Email }], _id: { "$ne": ObjectID(req.auth.credentials._id) }, "userStatus": 1 }, (err, result) => {
                if (err) {
                    logger.error('check email error', JSON.stringify(err));
                    reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                }
                else {
                    if (result && result.length) {
                        reject({ message: req.i18n.__('BusinessRequest')["409"]['forEmail'], code: 409 });
                    } else {
                        resolve();

                    }
                }
            })
        });
    }
    /**this function checks to see if phone number is unique */
    let checkPhone = () => {
        return new Promise((resolve, reject) => {
            let number = countryCode + Phone;
            userModel.Select({ $or: [{ "number": number }, { "businessProfile.phone.number": Phone }], _id: { "$ne": ObjectID(req.auth.credentials._id) }, "userStatus": 1 }, (err, result) => {
                if (err) {
                    //logger.error('check phone number error',err);
                    reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                }
                else {
                    if (result && result.length) {
                        reject({ message: req.i18n.__('BusinessRequest')['409']["forPhone"], code: 409 });
                    } else {
                        resolve();
                    }
                }
            })
        })

    }
    /** check if bussinessCategoryId is valid*/
    let checkBussinessCategory = () => {
        return new Promise((resolve, reject) => {
            bussinessModel.Select({ _id: ObjectID(req.payload.businessCategoryId) }, (err, result) => {
                if (err) {
                    reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                } else {
                    
                    if (check.isEmpty(result)) {
                        reject({ message: req.i18n.__('BusinessRequest')['412'], code: 412 });
                    } else {
                        let image = "data:image/png;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxEHEBISEBASFhIVEBAQEBMPEhAQEBAQFhIWFxUVFhoYHiggGBolGxMVITEhJikrLi4uFyAzODMtNygtLisBCgoKDQ0OFRAPFisdFR0tLS4tLS0tNy0rKystLS0rLSstLSsrKy0tLSstLS0rLS0tKy0rLS0tLSstLSstLS4tK//AABEIAOEA4QMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABQYBAwQCB//EADcQAQACAAMFBQUIAQUBAAAAAAABAgMEEQUhMUFREjJhcZETIoGhwRRCUnKSsdHhYhUzQ4KiBv/EABUBAQEAAAAAAAAAAAAAAAAAAAAB/8QAFhEBAQEAAAAAAAAAAAAAAAAAABEB/9oADAMBAAIRAxEAPwD7iAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMTaK8Z9Wq2ZpXjevrANw01zWHbhevrDbW0W4TE+QMgAAAAAAAAAAAAAAAAAAAAADEzohNobUnE1rhzpXnbnPl0gEhm9o0y27XW3Sv16IrH2riYvCezH+PH1cIDN7zfjMz5zMsAAzW004TMeUzDADtwNqYmFxntR/lx9UrlNp0zG6fdt0tz8pV0BbxA5Dac4Pu331686/zCdpaLxrE6xPCYBkAAAAAAAAAAAAAAAAEftjNewp2Y71tY8YjnIOLa2e9tM0rPuxO+fxT/CNAAAAAAAAAB3bLz32aezbuTP6Z6+ThAW+N4jNi5r2texM768PGv9JMAAAAAAAAAAAAAABWdo4/2jEtPKJ7MeULBm8T2WHa3Ss+vJVgAAAAAAAAAAAAbspjfZ71t0nf5TxWlUFm2die1wqT4aT8NwOkAAAAAAAAAAAAAHDtmdMG3jNY/wDUK8sG2/8AZn81f3V8AAAAAAAAAAAABPbCnXCnwvMftP1QKc2DGmHaet5/aASYAAAAAAAAAAAAAOXadPaYV48NfSdforS3Wr2omJ5xoqeLhzhWms8pmAeQAAAAAAAAAAAFi2PTsYUeMzb5q9Ws3mIjjM6QteFh+yrFY5REA9gAAAAAAAAAAAAAIXbmW7MxeOE7refKU08Y2FGNWazwmNAVMbc1gTlrTWfhPWOrUAAAAAAAAAD3gYU49orXjPyjqDu2Ll/aX7c8K8PzJ5qy2BGXrFY5fOectoAAAAAAAAAAAAAAAAObO5SM3XSd0x3Z6f0ruPgWy89m0aT8p8YWtqzGXrmY0tGvTrHkCqjvzey74O+vvV8O9HwcAAAAAAO3KbMvj7592vWeM+UA5cLCnGmIrGsrDkMlGUjrae9P0jwbMrla5WNKx5zPGW8AAAAAAAAAAAAAAAHm9opGszERHGZ3QD01Y2YpgRra0R58UVndrzbdh7o/FPH4Qi7Wm86zMzPWZ1lBbKXjEiJidYnhMPSr5TOXys+7O7nWeE/wnMptGmZ3a9m3S306g7GnHytMfvVifHhPq3CiLxNi0nu2tHnpMNM7En8cekpoBC/6Jb8cekt2HsWsd69p8tISgDRgZPDwO7WNes759W8AGLWisazw56ubN5+mW4zrb8Mcfj0Qeczt83x3V5Vjh8eqCx4WNXGjWtomPCdXtUsO84c61mYnrG5LZLa+u7E/VHD4wCXGInXgyoAAAAAAAAAxaezGs8OYPOLiRgxNrTpEcVdz+etm56V5R9Z8Wdo5z7Xbd3Y7sdfFyIACgADqy+0MTA4W1jpbfH8u/C21We/SY8a6TCGEgslNpYV/vxHnrDbGaw7cL1/VCrCi0zmaR9+v6oar7RwqffifLWf2VsBNYu2qx3azPnpEODMbRxMf72kdK7vnxcgkABQAB27P2hOVnSd9OnOvjH8LBS8YkRMTrE74mFSd+y899mns27kz+mevkgsACgAAAAAAh9t5v/jr53+kJLNY8Zek2nlw8Z5Qq97TeZmeMzrPmgwAoAAAAAAAAAAAAAAAAAAm9jZv2kdi0747uvOvT4JRU8HEnBtFo4xOq04GLGNWLRwmIlB7AUAAAYtbsxMzwiNZBC7dx+1aKRy96fOeHy/dFveNie2ta085mf4eAAAAAAAAAAAAAAAAAAAAAExsLH71J/NX6/RDtuUxvYXrbpMa+XMFqCN4AAA4tr4ns8K3j7vrx+WrtRH/ANBfuV/Nb00j6ghwAAAAAAAAAAAAAAAAAAAAAAAWXZuL7bCrPPTSfON30dSL2DfWto6W19Y/pKGAAAhNu9+v5Z/cARoAAAAAAAAAAAAAAAAAAAAAAAJXYPG//X6pgAAAf//Z"
                        if (req.payload && req.payload.businessProfilePic == "") {
                            uploadFile(image, "business_profile_image/" + _id + "_" + businessCategoryId.toString());
                        }
                        if (req.payload && req.payload.businessProfileCoverImage == "") {
                            uploadFile(image, "business_cover_image/" + _id + "_" + businessCategoryId.toString());
                        }
                        return resolve();
                    }
                }
            })
        })

    }
    /**check if the business with that id is already present */
    let checkBusinessCategoryValidity = () => {
        return new Promise((resolve, reject) => {
            /** if the bussiness Id from payload is already present in his profile,reject */
            if (userData.businessProfile && userData.businessProfile.length) {
                userData.businessProfile.forEach(element => {

                    if (element.businessCategoryId.toString() == businessCategoryId.toString() &&
                        (element.statusCode == 0 || element.statusCode == 1)) {
                        // not allow to create business profile with same businessCategoryId
                        reject({ message: req.i18n.__('BusinessRequest')['409']['forId'], code: 409 });
                    } else if (element && element.statusCode && element.statusCode == 1) {
                        // user not allow to create more then one business profile
                        reject({ message: req.i18n.__('BusinessRequest')['409']['ForTwoBusinessProfile'], code: 409 });
                    }
                });
                resolve();
            }
            else {
                resolve();
            }
        });
    }
    /**this function adds the bussiness profile into the database */
    let addBusinessRequest = () => {
        let myData = {
            businessName: BusinessName,
            websiteURL: websiteURL,
            statusText: 'Not approved',
            statusCode: 0,
            businessBio: req.payload.businessBio || '',
            phone: {
                number: Phone,
                countryCode: countryCode,
                verified: req.payload.isPhoneNumberVerified,
                isVisible: (req.payload.isVisiblePhone) ? 1 : 0
            },
            email: {
                id: Email,
                verified: req.payload.isEmailVerified,
                isVisible: (req.payload.isVisibleEmail) ? 1 : 0
            },
            businessCategoryId: businessCategoryId,
            address: address,
            businessStreet: req.payload.businessStreet,
            businessCity: req.payload.businessCity,
            businessZipCode: req.payload.businessZipCode,
            businessLat: req.payload.businessLat,
            businessLng: req.payload.businessLng,
            businessProfilePic: `${process.env.AWS_S3_PROFILEPIC_BASE_URL}business_profile_image/${_id}_${businessCategoryId.toString()}`,
            businessProfileCoverImage: `${process.env.AWS_S3_PROFILEPIC_BASE_URL}business_cover_image/${_id}_${businessCategoryId.toString()}`,
            timestamp: new Date().getTime(),
            createdOn: new Date()
        }
        userModel.Update({ _id: ObjectID(_id) }, { businessProfile: [myData] }, (err, result) => {
            if (err) {
                logger.error('updated bussiness profile error', err)
                return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
            } else {
                
                return res({
                    message: req.i18n.__('BusinessRequest')['200'], data: {
                        businessProfilePic: `${process.env.AWS_S3_PROFILEPIC_BASE_URL}business_cover_image/${_id}_${businessCategoryId.toString()}`,
                        businessProfileCoverImage: `${process.env.AWS_S3_PROFILEPIC_BASE_URL}business_profile_image/${_id}_${businessCategoryId.toString()}`
                    }
                }).code(200);
            }
        });
    }

    getUserData()
        .then(() => { return checkEmail(); })
        .then(() => { return checkPhone(); })
        .then(() => { return checkBussinessCategory(); })
        .then(() => { return checkBusinessCategoryValidity(); })
        .then(() => { return addBusinessRequest(); })
        .catch((err) => {
            logger.info('===========', err);
            res({ message: err.message }).code(err.code)
        })

};


const response = {
    status: {
        200: { message: Joi.any().default(local['BusinessRequest']['200']), data: Joi.any() },
        409: { message: Joi.any().default(local['BusinessRequest']['409']) },
        412: { message: Joi.any().default(local['BusinessRequest']['412']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
};//swagger response code


module.exports = { payloadValidator, response, handler };