'use strict'
const Joi = require("joi");
const logger = require('winston');
Joi.objectId = require('joi-objectid')(Joi);
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../../locales');
const userListCollection = require('.././../../../models/userList');

/**
 * @method Put bussinessProfile
 * @description API to update bussinessProfile
 * @property {string} authorization
 * @property {string} lang
 * @property {string} postId
 * @property {string} BusinessName
 * @property {string} Email
 * @property {string} Phone
 * @property {string} countryCode
 * @property {string} privateAccount
 * @property {string} address
 * @returns 200 : Success
 * @returns 400 : Parameters missing
 * @returns 500 : Internal Server Error
 */

//API payloadValidatior
const payloadValidator = Joi.object({
    isPhoneNumberVisible: Joi.number().default(0).example(0).description("isPhoneNumberVisible  : 0/1").required().error(new Error('isPhoneNumberVisible is missing')),
    isEmailVisible: Joi.number().default(0).example(0).description("isEmailVisible : 0/1 ").required().error(new Error('isEmailVisible is missing')),
    businessCategoryId: Joi.string().example("5d71f402a463b51b606377e3").description("businessCategoryId ").required().error(new Error('businessCategoryId is missing')),
}).unknown();


//start API handler
const handler = (req, res) => {

    let condition = { _id: ObjectID(req.auth.credentials._id), "businessProfile": { $elemMatch: { businessCategoryId: ObjectID(req.payload.businessCategoryId) } } };
    let dataToUpdate = {
        "businessProfile.$.phone.isVisible": (req.payload.isPhoneNumberVisible) ? 1 : 0,
        "businessProfile.$.email.isVisible": (req.payload.isEmailVisible) ? 1 : 0
    };

    userListCollection.Update(condition, dataToUpdate, (err) => {
        if (err) {
            logger.info('error', err);
            res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            return res({ message: req.i18n.__('BussinessProfile')['200'] })
        }
    });

};//end API hander


const response = {
    status: {
        200: { message: Joi.any().default(local['BussinessProfile']['200']).example(local['BussinessProfile']['200']).required() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']).example(local['genericErrMsg']['400']) },
    }
};//swagger response code


module.exports = { payloadValidator, response, handler };