'use strict'
const Joi = require("joi");
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const mail = require('../../../../library/gRPC/Email');
const userListCollection = require("../../../../models/userList");
const emailVerificationLogsCollection = require("../../../../models/emailVerificationLogs");
const bussinessEmailVerificationTemplates = require('../../../../library/sendEmail/templates/bussinessEmailVerification');



/**
 * @method Post bussinessEmailVerification
 * @description API to Verify email
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} bussinessEmailId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const payloadValidator = Joi.object({
    bussinessEmailId: Joi.string().example("3embed@example.com").required().description("bussinessEmailId").error(new Error('bussiness email id is required')),
}).unknown();

/**
 * handler starts here
 */
const handler = (req, res) => {
    const userId = ObjectID(req.auth.credentials._id);
    var emailId = req.payload.bussinessEmailId;
    var randomCode = (process.env.NODE_ENV == "production") ? Math.floor(1000 + Math.random() * 9000) : 1111;

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    if (!userData.businessProfile && !userData.businessProfile.length)
                        return res({ message: req.i18n.__('BussinessEmail')['422'] }).code(422);

                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }
    /*
        let checkProfileInDb = ()=>{
    
    
            return new Promise((resolve,reject)=>{
    
                        if (
                            userData.businessProfile.find(function(element){
                                return element.email.id == emailId
                            })
                        ) {
                            let dataToInsert = {
                                userId: userId,
                                bussinessEmail: emailId,
                                code: randomCode,
                                timestamp: new Date().getTime(),
                                timestampMetaBase: new Date()
                            }
                            emailVerificationLogsCollection.Insert(dataToInsert, (e, r) => {
                                if (e) {
                                    logger.error("email verification logs", e);
                                    // reject(res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500));
                                    reject({ message: req.i18n.__('genericErrMsg')['500'],code:500 });
                                }else{
                                    logger.info('result',r);
                                }
                            });
                            return resolve(userData);
                        }else{
                            // return reject(res({ message: req.i18n.__('BussinessEmail')['412'] }).code(412));
                            return reject({ message: req.i18n.__('BussinessEmail')['412'],code:412 });
                        }
            });
        } */

    const sendVerificationMail = () => {
        let dataToInsert = {
            userId: userId,
            bussinessEmail: emailId,
            code: randomCode,
            timestamp: new Date().getTime(),
            timestampMetaBase: new Date()
        }
        emailVerificationLogsCollection.Insert(dataToInsert, (e, r) => {
            if (e) {
                logger.error("email verification logs", e);
                // reject(res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500));
                reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
            } else {
                logger.info('result', r);
            }
        });

        let value = bussinessEmailVerificationTemplates.bussinessEmail({
            //    username: userDetail.userName,
            randomCode
        })

        let data = {
            emailId: emailId,
            subject: value.subject,//"Email Verification code for "+process.env.APP_NAME+" bussiness Profile",
            body: value.body,
            userName: userData.userName,
            trigger: "Email Verification"
        }
        mail.sendEmail(data);
        return res({ message: req.i18n.__('BussinessEmail')['200'] });
    };

    /* getUserData()
        .then((userDetail) => checkProfileInDb())
        .then((userDetail) => sendVerificationMail(userDetail))
        .catch((err) => {
            res({ message: err.message }).code(err.code)
        }) */
    getUserData().then(() => {
        sendVerificationMail()
    })

};

const response = {
    status: {
        200: { message: Joi.any().default(local['BussinessEmail']['200']).example(local['BussinessEmail']['200']).required() },
        412: { message: Joi.any().default(local['BussinessEmail']['412']).example(local['BussinessEmail']['412']).required() },
        422: { message: Joi.any().default(local['BussinessEmail']['422']).example(local['BussinessEmail']['422']).required() },
    }
}//swagger response code

module.exports = { payloadValidator, handler, response };