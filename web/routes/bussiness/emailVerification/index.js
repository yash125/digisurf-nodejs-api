let headerValidator = require('../../../middleware/validator');
// let PostAPI = require('./post');
let PostAPI = require('./post');
let PostOtpAPI = require('./verify');

module.exports = [
    {
        method: 'POST',
        path: '/businessEmailVerification',
        handler: PostAPI.handler,
        config: {
            description: `This API use for sending otp to bussiness email`,
            tags: ['api', 'businessProfile'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    }
].concat(PostOtpAPI);