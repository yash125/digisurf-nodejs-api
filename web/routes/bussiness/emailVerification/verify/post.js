'use strict'
const Joi = require("joi");
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../../locales');
const userCollection = require("../../../../../models/userList");
const emailVerificationLogsCollection = require("../../../../../models/emailVerificationLogs");

/**
 * @method Post bussinessEmailOtpVerify
 * @description API to bussinessEmailVerification a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} bussinessEmailId
 * @property {number} otp
 * @returns 200 : Success
 * @returns 412 : email does not match
 * @returns 500 : Internal Server Error
 */

const payloadValidator = Joi.object({
    bussinessEmailId: Joi.string().example("3embed@example.com").required().description("bussinessEmailId").error(new Error('bussiness email id is required')),
    otp: Joi.number().example(1111).required().description("opt").error(new Error('opt is required')),
    isVisible: Joi.boolean().example(true).required().default(true).description("is email visible or not").error(new Error('is visible is required')),
}).unknown();

const handler = (req, res) => {
    const userId = ObjectID(req.auth.credentials._id);
    var emailId = req.payload.bussinessEmailId;
    var otp = req.payload.otp;


    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    if (!userData.businessProfile && !userData.businessProfile.length)
                        return res({ message: req.i18n.__('BussinessEmail')['422'] }).code(422);

                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    const updateEmailId = () => {
        return new Promise((resolve, reject) => {

            let condition = { userId: userId, bussinessEmail: emailId, code: otp };

            /**get the latest valid otp that was sent on email */
            emailVerificationLogsCollection.SelectWithSort(condition, { timestamp: -1 }, {}, 0, 1, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 })
                } else if (result && result.length) {

                    userCollection.Update({ _id: userId },
                        {
                            "businessProfile.0.email.id": emailId,
                            "businessProfile.0.email.verified": 1,
                            "businessProfile.0.email.isVisible": (req.payload.isVisible) ? 1 : 0
                        }, (e, r) => {
                            if (e) {
                                logger.error("OKOKASDAS : ", e)
                            } else {
                                logger.info('yy', r.result);
                            }
                        });

                    if (userData && userData.parentId) {
                        //means this is bussiness profile
                        userCollection.Update({ _id: userId },
                            {
                                "email.id": emailId,
                                "verified": 1,
                                "isVisible": (req.payload.isVisible) ? 1 : 0
                            }, (e, r) => {
                                if (e) {
                                    logger.error("OKOKASDAS : ", e)
                                } else {
                                    logger.info('yy', r.result);
                                }
                            });
                        userCollection.Update({ _id: userData.parentId },
                            {
                                "businessProfile.0.email.id": emailId,
                                "businessProfile.0.email.verified": 1,
                                "businessProfile.0.email.isVisible": (req.payload.isVisible) ? 1 : 0
                            }, (e, r) => {
                                if (e) {
                                    logger.error("OKOKASDAS : ", e)
                                } else {
                                    logger.info('yy', r.result);
                                }
                            });

                    } else if (userData && userData.businessProfile && userData.businessProfile[0]
                        && userData.businessProfile[0].businessProfileId) {
                        userCollection.Update({ _id: userData.businessProfile[0].businessProfileId },
                            {
                                "email.id": emailId,
                                "verified": 1,
                                "isVisible": (req.payload.isVisible) ? 1 : 0,
                                "businessProfile.0.email.id": emailId,
                                "businessProfile.0.email.verified": 1,
                                "businessProfile.0.email.isVisible": (req.payload.isVisible) ? 1 : 0
                            }, (e, r) => {
                                if (e) {
                                    logger.error("OKOKASDAS : ", e)
                                } else {
                                    logger.info('yy', r.result);
                                }
                            });
                    }
                    return resolve({ message: req.i18n.__('BussinessEmailVerify')['200'], code: 200 })
                } else {
                    return reject({ message: req.i18n.__('BussinessEmailVerify')['412'], code: 412 })
                }
            })

        });
    }

    getUserData()
        .then(updateEmailId)
        .then(data => {
            return res({ message: data.message }).code(data.code)
        })
        .catch(data => {
            console.log("error ", data)
            return res({ message: data.message }).code(data.code)
        })

};

const response = {
    status: {
        200: { message: Joi.string().default(local['BussinessEmailVerify']['200']).example(local['BussinessEmailVerify']['200']) },
        412: { message: Joi.string().default(local['BussinessEmailVerify']['412']).example(local['BussinessEmailVerify']['412']) }
    }
}//swagger response code

module.exports = { payloadValidator, handler, response };