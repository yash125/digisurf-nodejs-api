let headerValidator = require('../../../../middleware/validator')
// let PostAPI = require('./post');;
let PostAPI = require('./post');
module.exports = [
    {
        method: 'POST',
        path: '/businessEmailOtpVerify',
        handler: PostAPI.handler,
        config: {
            description: `This API used to verify opt of bussiness email`,
            tags: ['api', 'businessProfile'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    }
];