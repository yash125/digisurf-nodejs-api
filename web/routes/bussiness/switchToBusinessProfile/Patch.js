'use strict'
const Joi = require("joi");
const { ObjectID } = require('mongodb');
const logger = require('winston');
const local = require('../../../../locales');
const userList = require("../../../../models/userList");
const deviceInfo = require("../../../../models/deviceInfo");
const request = require('request');
var jwtValidator = require('./../../../middleware/auth');

const validator = Joi.object({
    isActiveBusinessProfile: Joi.boolean().description('isActiveBusinessProfile : true / false').required().error(new Error("isActiveBusinessProfile is missing or invalid")),
    businessCategoryId: Joi.string().allow([""]).description('businessCategoryId').error(new Error("businessCategoryId is missing or invalid")),
}).unknown();

const handler = (req, res) => {
    var dataToSend = { "code": 200, "message": "success", "response": {} };
    const isActiveBusinessProfile = req.payload.isActiveBusinessProfile;



    var userData = {}, swichData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userList.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    console.log("error : ", err)
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;

                    if (isActiveBusinessProfile) {

                        //check business profile is approved or not
                        if (userData && userData.businessProfile.find(e => e.businessCategoryId.toString() == req.payload.businessCategoryId) &&
                            userData.businessProfile.find(e => e.businessCategoryId.toString() == req.payload.businessCategoryId).statusCode == 1) {
                            //send success - becuase business profile is approved
                            return resolve();
                        } else {
                            //send 405 - action not allowed becuase business profile is not approved
                            return reject({ message: req.i18n.__('genericErrMsg')['405'], code: 405 });
                        }
                    } else {
                        return resolve();
                    }

                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })

        });
    }
    //return reject({ message: req.i18n.__('genericErrMsg')['204'] });
    const updateStatus = () => {
        return new Promise((resolve, reject) => {
            if (isActiveBusinessProfile) {

                if (req.payload.businessCategoryId == "" || req.payload.businessCategoryId.length !== 24)
                    return res({ message: req.i18n.__('PatchBussiness')['400'] }).code(400);

                const businessCategoryId = ObjectID(req.payload.businessCategoryId);

                userList.Update(
                    { _id: ObjectID(req.auth.credentials._id) },
                    {
                        "isActiveBusinessProfile": isActiveBusinessProfile,
                        "businessProfile.0.isActive": true
                    },
                    (err) => {
                        if (err) {
                            return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 })
                        }
                    });
                if (userData && userData.businessProfile[0] && userData.businessProfile[0].businessProfileId) {

                    userList.Update(
                        { _id: ObjectID(userData.businessProfile[0].businessProfileId.toString()) },
                        {
                            "isActiveBusinessProfile": isActiveBusinessProfile,
                            "businessProfile.0.isActive": true
                        },
                        (err) => {
                            if (err) {
                                return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 })
                            }
                            return resolve(true)
                        });
                } else if (userData && userData.parentId) {
                    userList.Update(
                        { _id: ObjectID(userData.parentId.toString()) },
                        {
                            "isActiveBusinessProfile": isActiveBusinessProfile,
                            "businessProfile.0.isActive": true
                        },
                        (err) => {
                            if (err) {
                                return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 })
                            }
                            return resolve(true)
                        });
                }


            } else {


                userList.Update(
                    { _id: ObjectID(userData._id.toString()) },
                    {
                        "isActiveBusinessProfile": false
                    },
                    (err) => {
                        if (err) {
                            return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 })
                        }
                    });

                userList.Update(
                    { _id: ObjectID(userData._id.toString()) },
                    {
                        "isActiveBusinessProfile": false,
                        "businessProfile.0.isActive": false
                    },
                    (err) => {
                        if (err) {
                            return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 })
                        }
                    });

                try {
                    userList.Update(
                        { _id: ObjectID(userData.businessProfile[0].businessProfileId.toString()) },
                        {
                            "isActiveBusinessProfile": false,
                            "businessProfile.0.isActive": false
                        },
                        (err) => {
                            if (err) {
                                return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 })
                            }
                        });
                } catch (error) {
                    console.log("error OKEOWKO : ", err)
                }








                userList.Update(
                    { _id: ObjectID(userData.parentId) },
                    {
                        "isActiveBusinessProfile": false
                    },
                    (err) => {
                        if (err) {
                            return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 })
                        }
                    });

                userList.Update(
                    { _id: ObjectID(userData.parentId), "businessProfile": { "$elemMatch": { "isActive": true } } },
                    {
                        "isActiveBusinessProfile": false,
                        "businessProfile.$.isActive": false
                    },
                    (err) => {
                        if (err) {
                            return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 })
                        }
                        return resolve(true)
                    });

            }
        });
    }

    const getSwitchData = () => {
        return new Promise((resolve, reject) => {

            var _id = (userData && userData.parentId) ? userData.parentId : "";
            if (isActiveBusinessProfile) {
                if (userData.businessProfile.find(e => {
                    return e.businessCategoryId.toString() == req.payload.businessCategoryId
                }) && userData.businessProfile.find(e => {
                    return e.businessCategoryId.toString() == req.payload.businessCategoryId
                }).businessProfileId) {
                    _id = userData.businessProfile.find(e => {
                        return e.businessCategoryId.toString() == req.payload.businessCategoryId
                    }).businessProfileId.toString();
                }

            } else {
                _id = userData.parentId;
            }

            userList.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
                if (err) {
                    console.log("error : ", err)
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    swichData = result;
                    return resolve();
                } else {
                    swichData = userData
                    return resolve();
                    // return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })

        });
    }

    const mapResponse = () => {
        return new Promise((resolve, reject) => {
            dataToSend["response"]["userId"] = swichData._id.toString();
            dataToSend["response"]["token"] = "";
            dataToSend["response"]["private"] = (swichData.private) ? 1 : 0;
            dataToSend["response"]["businessProfile"] = (swichData.businessProfile) ? swichData.businessProfile : [];
            dataToSend["response"]["isActiveBusinessProfile"] = (swichData.isActiveBusinessProfile) ? true : false;
            dataToSend["response"]["profilePic"] = swichData.profilePic;
            dataToSend["response"]["userName"] = swichData.userName;
            dataToSend["response"]["firstName"] = swichData.firstName;
            dataToSend["response"]["lastName"] = swichData.lastName;
            dataToSend["response"]["countryCode"] = swichData.countryCode;
            dataToSend["response"]["number"] = swichData.number;
            dataToSend["response"]["profileVideo"] = swichData.profileVideo;
            dataToSend["response"]["profileVideoThumbnail"] = swichData.profileVideoThumbnail;
            dataToSend["response"]["authToken"] = "";
            dataToSend["response"]["willTopic"] = "";

            dataToSend["response"]["number"] = swichData.number;
            dataToSend["response"]["email"] = (swichData && swichData.email && swichData.email.id) ? swichData.email.id : "";

            //dataToSend["response"]["groupCallStreamId"] = result[0].groupCallStreamId || "";
            dataToSend["response"]["accountId"] = process.env.accountId
            dataToSend["response"]["keysetId"] = process.env.keysetId
            dataToSend["response"]["projectId"] = process.env.projectId
            dataToSend["response"]["licenseKey"] = process.env.licenseKey
            dataToSend["response"]["keysetName"] = process.env.keysetName
            dataToSend["response"]["rtcAppId"] = process.env.rtcAppId
            dataToSend["response"]["arFiltersAppId"] = process.env.arFiltersAppId



            return resolve();
        });
    }

    const mapTokenAndOtherResponse = () => {
        return new Promise((resolve, reject) => {
            var deviceInformation = {
                "userId": ObjectID(swichData._id),
                "deviceName": swichData.deviceInfo.deviceName,
                "deviceOs": swichData.deviceInfo.deviceOs,
                "modelNumber": swichData.deviceInfo.modelNumber,
                "deviceType": swichData.deviceInfo.deviceType,
                "appVersion": swichData.deviceInfo.appVersion,
                "deviceId": swichData.deviceInfo.deviceId,
                "timestamp": new Date().getTime(),
                "creationDate": new Date(),
                "_id": new ObjectID()
            };

            deviceInfo.Insert(deviceInformation, (e) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] })
                }
            });

            userList.Update({ _id: ObjectID(swichData._id) }, {
                "deviceInfo": deviceInformation
            }, (e) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] })
                }
            });

            dataToSend["response"]["currencySymbol"] = process.env.CURRENCY_SYMBOL || "";
            dataToSend["response"]["currency"] = process.env.CURRENCY_CODE || "";

            // jwtValidator.generateTokens(
            //     {
            //         userId: req.auth.credentials._id,
            //         userType: "userJWT",
            //         multiLogin: 'true',
            //         allowedMax: "1",
            //         immediateRevoke: 'true',
            //         metaData: {},
            //         accessTTL: process.env.AUTH_ACCESS_EXPIRY_TIME,
            //         refreshTTL: process.env.AUTH_REFRESH_EXPIRY_TIME
            //     }).then((data) => {
            //         console.log("create new token for " + req.auth.credentials._id)
            //     })
            //     .catch(e => {
            //         console.log("not created new token for " + req.auth.credentials._id)
            //         console.log("error ", e)
            //     });

            jwtValidator.generateTokens(
                {
                    userId: swichData._id.toString(),
                    userType: "user",
                    multiLogin: "true",
                    allowedMax: "1",
                    immediateRevoke: "true",
                    metaData: {},
                    accessTTL: process.env.AUTH_ACCESS_EXPIRY_TIME,
                    refreshTTL: process.env.AUTH_REFRESH_EXPIRY_TIME
                }).then((data) => {


                    dataToSend["response"]["token"] = data.accessToken;
                    dataToSend["response"]["refreshToken"] = data.refreshToken;
                    dataToSend["response"]["accessExpiry"] = data.accessExpiry;
                    dataToSend["response"]["authToken"] = data.accessToken;
                    dataToSend["response"]["willTopic"] = data.accessToken;

                    //group call api
                    request.post({
                        headers: { 'content-type': 'application/json', 'lan': 'en' },
                        url: `${process.env.GROUP_CALL_API}/loginSignUp`,
                        body: {
                            id: swichData._id,
                            userType: 1,
                            firstName: swichData.firstName,
                            lastName: swichData.lastName || swichData.firstName,
                            profilePic: swichData.profilePic,
                            deviceType: swichData.deviceInfo.deviceType,
                            pushKitToken: swichData._id,
                            mqttTopic: swichData._id,
                            fcmTopic: swichData._id
                        },
                        json: true
                    }, function (error) {
                        if (error) {
                            logger.error('KLAPWDD : ', error);
                        }
                    });

                    //stream
                    let streamData = {
                        userName: dataToSend["response"]["userName"],
                        firstName: swichData.firstName,
                        id: swichData._id,
                        lastName: swichData.lastName || swichData.firstName,
                        userType: 1,
                        profilePic: swichData.profilePic,
                        deviceType: 'Android',//get it from payload
                        mqttTopic: swichData._id,
                        fcmTopic: swichData._id
                    }

                    request.post({
                        headers: { 'content-type': 'application/json', 'lan': 'en' },
                        url: `${process.env.LIVE_STREAM_API}/user`,
                        body: streamData,
                        json: true
                    }, function (error, response, body) {
                        if (error)
                            logger.error('0000', error);
                        else {
                            if (response.statusCode == 200) {
                                let stream = body.data;
                                dataToSend["response"]["stream"] = stream;
                            }
                        }
                        return resolve(dataToSend)
                    });

                });
        });
    }

    getUserData()
        .then(updateStatus)
        .then(getSwitchData)
        .then(mapResponse)
        .then(mapTokenAndOtherResponse)
        .then((data) => { return res(data).code(data.code) })
        .catch((err) => {
            console.log("error : ", err)
            return res({ message: err.message }).code(err.code)
        });
};

const response = {
    status: {
        200: { message: Joi.any().default(local['PatchBussiness']['200']), code: Joi.any(), response: Joi.any() },
        405: { message: Joi.any().default(local['genericErrMsg']['405']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { validator, handler, response };