let headerValidator = require("../../../middleware/validator")
let PatchAPI = require('./Patch');

module.exports = [
    {
        method: 'PATCH',
        path: '/business',
        handler: PatchAPI.handler,
        config: {
            description: `This API is used to active or de-active businessprofile`,
            tags: ['api', 'businessProfile'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload : PatchAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PatchAPI.response
        }
    }
];