let headerValidator = require("../../../middleware/validator")
let GetAPI = require('./Get');//require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/business/search',
        handler: GetAPI.handler,
        config: {
            description: `This API use for search business profiles`,
            tags: ['api', 'search'],
            auth: "user",
            validate: {
                query: GetAPI.validator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];