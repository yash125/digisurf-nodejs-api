const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../../locales');
const userListCollection = require("../../../../models/userList")
const userListES = require("../../../../models/userListES")
const followCollection = require("../../../../models/follow")


/**
 * @description API to subscribe to a channel
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token
 * @property {mongodb object} channelId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const validator = Joi.object({
    businessName: Joi.string().allow("").default("").description("businessName to be searched"),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit")
}).required();

const handler = (req, res) => {

    var userId = ObjectID(req.auth.credentials._id);
    let businessName = (req.query.businessName) ? req.query.businessName.trim().toLowerCase() : "";
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;
    var followeeIdObj = {};

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    const getMyFollowee = () => {
        return new Promise((resolve, reject) => {
            followCollection.Select({ follower: userId }, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    for (let index = 0; index < result.length; index++) {
                        followeeIdObj[result[index].followee.toString()] = result[index]["type"];
                    }
                    return resolve(true);
                }
            })
        });
    };
    const getUserList = () => {
        return new Promise((resolve, reject) => {

            if (businessName !== "") {
                condition = {
                    "must": [
                        { "match_phrase_prefix": { "businessProfile.businessName": businessName } },
                        { "match": { "userStatus": 1 } },
                        { "match": { "businessProfile.statusCode": 1 } },
                        {
                            "exists": {
                                "field": "businessProfile"
                            }
                        }
                    ],
                    "must_not": [
                        { "match": { "businessProfile.businessName": " " } },
                        { "match": { "businessProfile.businessName": "null" } },
                    ]
                };
                userListES.getDetails(condition, offset, limit, userData.follow, req.auth.credentials._id, (e, d) => {
                    if (e) {
                        console.log("error ", e)
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (d.length === 0) {
                        return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                    } else {
                        let dataToSend = [];
                        d.forEach(element => {
                            element["followStatus"] = (followeeIdObj[element._id.toString()]) ? followeeIdObj[element._id.toString()]["status"] : 0;
                            element["followStatus"] = (element["followStatus"] == 3) ? 0 : element["followStatus"];
                            element["friendStatusCode"] = (userData.friendList && userData.friendList.map(e => e.toString()).includes(element["_id"].toString())) ? 2 : (userData.myFriendRequest && userData.myFriendRequest.map(e => e.toString()).includes(element["_id"].toString())) ? 3 : 1;
                            element["friendStatusText"] = (element["friendStatusCode"] == 2) ? "friends" : (element["friendStatusCode"] == 3) ? "userSentARequestToTargetUser" : "notAFirend";
                            try {
                                element["isStar"] = (element["starRequest"] && element["starRequest"]["starUserProfileStatusCode"] == 4) ? true : false;
                            } catch (error) {
                                element["isStar"] = false;
                                console.log("error ", error)
                            }
                            if (element.businessProfile) {
                                dataToSend.push({
                                    _id: element._id,
                                    firstName: element.firstName,
                                    registeredOn: element.registeredOn,
                                    private: (element.private) ? 1 : 0,
                                    businessProfile: element.businessProfile,
                                    userName: element.userName,
                                    profilePic: element.profilePic,
                                    lastName: element.lastName,
                                    followStatus: element["followStatus"],
                                    friendStatusCode: element["friendStatusCode"],
                                    friendStatusText: element["friendStatusText"],
                                    isStar: element["isStar"]
                                });
                            }
                        });
                        return resolve({ code: 200, message: req.i18n.__('GetSearchPeople')['200'], data: dataToSend });
                    }
                });

            } else {
                let condition = [
                    { "$group": { "_id": "$followee", "total": { "$sum": 1 } } },
                    { "$sort": { "total": -1, "_id": -1 } },
                    { "$lookup": { "from": "customer", "localField": "_id", "foreignField": "_id", "as": "userData" } },
                    { "$unwind": "$userData" },
                    // { "$match": { "$exists": { "$userData.businessProfile": 1 } } },
                    { "$match": { "userData.businessProfile.statusCode": 1, "userData.userStatus": 1 } },
                    {
                        "$project": {
                            "_id": 1, "registeredOn": "$userData.registeredOn", "private": "$userData.private",
                            "businessProfile": "$userData.businessProfile",
                            "userName": "$userData.userName", "firstName": "$userData.firstName", "lastName": "$userData.lastName",
                            "profilePic": "$userData.profilePic", "userStatus": "$userData.userStatus", "blocked": "$userData.blocked",
                            "starRequest": "$userData.starRequest"
                        }
                    },
                    { "$match": { "_id": { "$ne": userId } } },
                    { "$match": { "userStatus": 1, "blocked": { "$ne": userId } } },
                    { "$skip": offset }, { "$limit": limit },
                ];
                followCollection.Aggregate(condition, (e, d) => {
                    if (e) {
                        console.log("PALDPE ", e)
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (d.length === 0) {
                        return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                    } else {
                        for (let index = 0; index < d.length; index++) {
                            d[index]["followStatus"] = (followeeIdObj[d[index]._id.toString()]) ? followeeIdObj[d[index]._id.toString()]["status"] : 0
                            d[index]["followStatus"] = (d[index]["followStatus"] == 3) ? 0 : d[index]["followStatus"];
                            d[index]["private"] = d[index]["private"] || 0;
                            try {
                                d[index]["isStar"] = (d[index]["starRequest"] && d[index]["starRequest"]["starUserProfileStatusCode"] == 4) ? true : false;
                            } catch (error) {
                                d[index]["isStar"] = false;
                                console.log("error ", error)
                            }
                        }
                        return resolve({ code: 200, message: req.i18n.__('GetSearchPeople')['200'], data: d });
                    }
                });
            }
        })
    };

    getUserData()
        .then(getMyFollowee)
        .then(getUserList)
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetSearchPeople']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { validator, response, handler };

