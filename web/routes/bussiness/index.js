let request = require('./request');
let bussinessProfile = require('./Categories')
let emailVerification = require('./emailVerification');
let phoneVerification = require('./phoneVerification');
let switchToBusiness = require('./switchToBusinessProfile');

module.exports = [].concat(
    require('./productType'),
    require('./search'),
    bussinessProfile, emailVerification, phoneVerification, request, switchToBusiness);
