'use strict'
const Joi = require("joi");
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../../locales');
const userCollection = require("../../../../../models/userList");
const otpVerificationLogsCollection = require('../../../../../models/otps');

/**
 * @method post bussinessPhoneOtpVerify
 * @description API to bussinessEmailVerification a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} businessPhone
 * @property {string} countryCode
 * @property {number} otp
 * @returns 200 : Success
 * @returns 412 : email does not match
 * @returns 500 : Internal Server Error
 */

const payloadValidator = Joi.object({
    businessPhone: Joi.string().example("9090909090").required().description("bussinessPhone without country code").error(new Error('bussinessPhone is required')),
    countryCode: Joi.string().example("+91").required().description("country code").error(new Error('countrycode is required')),
    otp: Joi.number().example(1111).required().description("opt").error(new Error('otp is required')),
    isVisible: Joi.boolean().example(true).required().default(true).description("is email visible or not").error(new Error('is visible is required')),
}).unknown();

const handler = (req, res) => {
    const userId = ObjectID(req.auth.credentials._id);

    
    var phone = req.payload.businessPhone;
    var otp = req.payload.otp;
    let condition = { userId: userId, businessPhone: phone, code: otp };

    otpVerificationLogsCollection.SelectWithSort(condition, { timestamp: -1 }, {}, 0, 1, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result && result.length) {

            userCollection.Update({ _id: userId, businessProfile: { "$elemMatch": { "phone.number": phone } } },
                {
                    "businessProfile.$.phone.verified": 1,
                    "businessProfile.$.phone.isVisible": (req.payload.isVisible) ? 1 : 0
                }, (e, r) => {
                    if (e) {
                        logger.error("OKOKASDAS : ", e)
                    } else {
                        logger.info('yy', r.result);
                    }
                })
            return res({ message: req.i18n.__('BussinessPhoneVerify')['200'] }).code(200);
        } else {
            
            return res({ message: req.i18n.__('BussinessPhoneVerify')['412'] }).code(412);
        }
    })

};

const response = {
    status: {
        200: { message: Joi.string().default(local['BussinessPhoneVerify']['200']).example(local['BussinessPhoneVerify']['200']).required() },
        412: { message: Joi.string().default(local['BussinessPhoneVerify']['412']).example(local['BussinessPhoneVerify']['412']).required()  },
        400: { message: Joi.string().default(local['genericErrMsg']['400']).example(local['genericErrMsg']['400'])  },
    }
}//swagger response code

module.exports = { payloadValidator, handler, response };