let headerValidator = require("../../../../middleware/validator")
let PostOtpAPI = require('./post');

module.exports = [
    {
        method: 'POST',
        path: '/businessPhoneOtpVerify',
        handler: PostOtpAPI.handler,
        config: {
            description: `This API used to verify opt of bussiness phone`,
            tags: ['api', 'businessProfile'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostOtpAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostOtpAPI.response
        }
    }
];