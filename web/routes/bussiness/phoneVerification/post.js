'use strict'
const Joi = require("joi");
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const phoneVerificationLogsCollection = require('../../../../models/otps');
const gRPC_SMS_service = require('../../../../library/gRPC/SMS');
/**
 * @method Post phoneVerification
 * @description API to phoneVerification a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} businessPhone
 * @property {string} countryCode
 * @returns 200 : Success
 * @returns 412 :bussiness phone number doesnt match
 * @returns 500 : Internal Server Error
 * @returns 404 : bussiness profile not found
 */


const payloadValidator = Joi.object({
    businessPhone: Joi.string().example("9090909090").required().description("businessPhone").error(new Error('businessPhone is required')),
    countryCode: Joi.string().example("+91").required().description("CountryCode").error(new Error('country code is required'))
}).unknown();


/**
 * 
 *handler starts here
 */
const handler = (req, res) => {
    const userId = ObjectID(req.auth.credentials._id);
    
    var businessPhone = req.payload.businessPhone;


    /* var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    
                    if (!userData.businessProfile && !userData.businessProfile.length)
                        return res({ message: req.i18n.__('BussinessPhone')['422'] }).code(422);
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }
   
    let checkProfileInDb = () => {

        return new Promise((resolve, reject) => {

            let result = userData;
            if (result.businessProfile.find(function (element) {
                return element.phone.number == businessPhone
            })) {
               
                let dataToInsert = {
                    userId: userId,
                    businessPhone: businessPhone,
                    code: randomCode,
                    timestamp: new Date().getTime(),
                    timestampMetaBase: new Date()
                }
                phoneVerificationLogsCollection.Insert(dataToInsert, (e, r) => {
                    if (e) {
                        logger.error("OASDLASKMASDD : ", e);
                        reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
                    } else {
                        // logger.info('result of insert opt', r)
                    }
                });
                return resolve(result);
            }
            reject({ message: req.i18n.__('BussinessPhone')['412'], code: 412 });
        });
    } */
    /**
     *send verification code to phone number
     */
    const sendVerificationMail = () => {

        var randomCode = (process.env.NODE_ENV == "production") ? Math.floor(1000 + Math.random() * 9000) : 1111;


        let dataToInsert = {
            userId: userId,
            businessPhone: businessPhone,
            code: randomCode,
            timestamp: new Date().getTime(),
            timestampMetaBase: new Date()
        }
        phoneVerificationLogsCollection.Insert(dataToInsert, (e) => {
            if (e) {
                logger.error("OASDLASKMASDD : ", e);
                reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 });
            }
        });



        let number = businessPhone + req.payload.countryCode;
        if (process.env.NODE_ENV == "production") {
            gRPC_SMS_service.sendSMS({ to: number, body: randomCode + ` is your registration code on ${process.env.APP_NAME}.` })
            // twilio.sendSms({
            //     "from": process.env.CELL_PHONE_NUMBER,
            //     "to": number,
            //     "body": randomCode + " is your registration code on otp for business phone verification."
            // }, (e, r) => {
            //     if (e) logger.error('twillio error', e);
            //     else
            //         logger.info('twillio', r);
            // });
        }

        return res({ message: req.i18n.__('BussinessPhone')['200'] });
    };

    /*  getUserData()
         .then((userDetail) => checkProfileInDb(userDetail))
         .then((userDetail) => sendVerificationMail(userDetail))
         .catch((err) => {
             logger.info('===========', err);
             res({ message: err.message }).code(err.code)
         }) */

    sendVerificationMail()
};

const response = {
    status: {
        200: { message: Joi.string().default(local['BussinessPhone']['200']).example(local['BussinessPhone']['200']).required() },
        412: { message: Joi.string().default(local['BussinessPhone']['412']).example(local['BussinessPhone']['412']).required() },
        422: { message: Joi.string().default(local['BussinessPhone']['422']).example(local['BussinessPhone']['422']).required() }
    }
}//swagger response code

module.exports = { payloadValidator, handler, response };