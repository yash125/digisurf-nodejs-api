let headerValidator = require("../../../middleware/validator")
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/businessProductType',
        handler: GetAPI.hander,
        config: {
            description: `This API use for get all business product type`,
            tags: ['api', 'business'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query : GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];