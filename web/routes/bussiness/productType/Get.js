'use strict'
//require npm modules
const Joi = require("joi");
//require dependency
const local = require('../../../../locales');
const businessProductType = require("../../../../models/businessProductType");


//validator for this API
const validator = Joi.object({
    offset: Joi.string().default("0").description("offset, query parameter"),
    limit: Joi.string().default("20").description("limit, query parameter")
}).unknown();


/**
 * @method GET businessProductType
 * @description This API returns businessProductType with language support
 * 
 * @headers {string} authorization : token
 * @headers {string} lang by defualt en
 * 
 * @param {string} offset by default 0
 * @param {string} limit  by default 20
 * 
 * @author DipenAhir
 * @since 09-Sep-2019
 */

//start API hender
const hander = (req, res) => {

    //API variables
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;

    /**
    * This function return all bussiness product type
    **/
    businessProductType.SelectWithSort({ "status": 1 }, { _id: -1 }, {}, offset, limit, (err, result) => {
        if (err) {
            //return 500 responce : if have any issues in function
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result && result.length) {
            return res({
                message: req.i18n.__('GetBusinessProductType')['200'], data: result.map(e => {
                    return {
                        _id: e._id,
                        text: e.text[req.headers.lang]
                    }
                })
            }).code(200);
        } else {
            //return 204 responce : if data not found in db
            return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);
        }
    })


};//end API hender

//start API response
const response = {
    status: {
        200: {
            message: Joi.string().default(local['GetBusinessProductType']['200']).example(local['GetBusinessProductType']['200']).required(),
            data: Joi.array().example([
                {
                    "_id": "5d71f402a463b51b606377e3",
                    "text": "Add Link"
                },
                {
                    "_id": "5d709a21a463b51b6054894f",
                    "text": "Regular"
                }
            ])
            .items({
                _id : Joi.any().example("5d71f402a463b51b606377e3").required(),
                text : Joi.string().example("Add Link").required()
            })
        },
        204: { message: Joi.string().default(local['genericErrMsg']['204']).example(local['genericErrMsg']['204']) },
        400: { message: Joi.string().default(local['genericErrMsg']['400']).example(local['genericErrMsg']['400']) }
    }
}//end API response

//Export all constance
module.exports = { validator, hander, response };