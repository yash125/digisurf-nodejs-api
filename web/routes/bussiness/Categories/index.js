let headerValidator = require("../../../middleware/validator");
let GetAPI = require('./Get');


module.exports = [
    {
        method: 'GET',
        path: '/businessCategories',
        handler: GetAPI.handler,
        config: {
            description: `this api is used to get bussiness categories`,
            tags: ['api', 'businessProfile'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];