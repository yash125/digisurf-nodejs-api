'use strict'
const Joi = require("joi");
const logger = require('winston');
const local = require('../../../../locales');
const bussinessModel = require('../../../../models/bussinessCategory');

/**
 * @method Get businessProfile
 * @description API is used to get all valid categories
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 500 : Internal Server Error
 */

/**handler starts here */
const handler = (req, res) => {

    /**this selects all categeries from database */
    bussinessModel.Select({ "statusCode": 1 }, (err, result) => {
        if (err) {
            logger.silly(err);
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500)
        } else if (result && result.length)
            return res({ message: req.i18n.__('Category')['200'], data: result }).code(200);
        /**if not categeries found it returns 204 */
        else
            return res({ message: req.i18n.__('Category')['204'] }).code(204);
    });

}
const response = {
    status: {
        200: {
            message: Joi.string().default(local['Category']['200']).example(local['Category']['200']).required(),
            data: Joi.array()
                .items({
                    "_id": Joi.any().example("5cebc4328dfa9531e407e244").required(),
                    "type": Joi.string().example("Music").required(),
                    "statusCode": Joi.number().example(1),
                })
                .example([
                    {
                        "_id": "5cebc4328dfa9531e407e244",
                        "type": "Music",
                        "statusCode": 1
                    }]).required()
        },
        204: { message: Joi.any().default(local['Category']['204']).example(local['Category']['204']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']).example(local['genericErrMsg']['500']) },
    }
};//swagger response code

module.exports = { handler, response }