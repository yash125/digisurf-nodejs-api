let PostAPI = require('./Post');
let headerValidator = require("../../middleware/validator")

module.exports = [
    {
        method: 'POST',
        path: '/verifySignUp',
        handler: PostAPI.handler,
        config: {
            description: `This API used for verifySignUp it check all fieilds are unique`,
            tags: ['api', 'signUp'],
            auth: {
                strategies: ['basic', 'guest']
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    }
];