'use strict'
var Joi = require("joi");
var logger = require('winston');
var Promise = require('promise');
var local = require('../../../locales');

//models
var signUpModel = require('../../../models/signUp');
var userList = require('../../../models/userList');

/**
 * @property {string} lang - language
 * @property {string} userId
 * @property {string} postId
 * @returns 200 : Success
 * @returns 201 : signedUp sucessfully
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const payloadValidator = Joi.object({
    userName: Joi.string().min(3).required().description("please enter a unique username").error(new Error('username must be atleast 3 charector and is required')),
    email: Joi.string().required().description("please enter your email").error(new Error('enter a valid email')),
    // userReferralCode: Joi.string().description("please enter a userReferralCode").allow("").default(""),
    number: Joi.string().required().description("PhoneNumber without countrycode").error(new Error('number must be of 10 digits')),
    countryCode: Joi.string().min(1).required().description("country code").error(new Error('countryCode must be valid')),
}).unknown();


const handler = (req, res) => {

    var dataToSend = { "code": 200, "message": "success" };

    try {
        req.payload.userReferralCode = (req.payload.userReferralCode) ? req.payload.userReferralCode.toUpperCase() : "";
    } catch (error) {
        logger.silly("error :  ", error)
    }
    logger.silly("payload :  ", req.payload);

    let checkUserNameAndPhoneNumber = () => {
        return new Promise((resolve, reject) => {
            signUpModel.verifyUniqueness(req.payload, (err, result) => {
                if (err) return reject();

                if (result && result.length) {
                    if (result[0].userName == req.payload.userName) {
                        return reject({ code: 422, message: req.i18n.__('signUp')['422'] })
                    } else if (result[0].email && result[0].email.id == req.payload.email) {
                        return reject({ code: 412, message: req.i18n.__('signUp')['412'] })
                    } else {
                        return reject({ code: 409, message: req.i18n.__('signUp')['409'] })
                    }
                } else {
                    return resolve(true)
                }
            })
        });
    }

    let checkReferralCode = () => {
        return new Promise((resolve, reject) => {
            if (process.env.IS_REFERRAL_CODE_REQUIRED == "true" || process.env.IS_REFERRAL_CODE_REQUIRED == true) {
                logger.silly("in if IS_REFERRAL_CODE_REQUIRED ", process.env.IS_REFERRAL_CODE_REQUIRED)
                userList.SelectOne({ "userStatus": 1, referralCode: req.payload.userReferralCode.toUpperCase() }, (err, result) => {
                    if (err) return reject({ message: req.i18n.__('genericErrMsg')['500'], code: 500 })

                    if (result && result._id) {
                        return resolve(true)
                    } else {
                        return reject({ code: 406, message: req.i18n.__('signUp')['406'] });
                    }
                });
            } else {
                logger.silly("in else IS_REFERRAL_CODE_REQUIRED ", process.env.IS_REFERRAL_CODE_REQUIRED)
                return resolve(true)
            }
        });
    }


    checkUserNameAndPhoneNumber()
        .then(() => checkReferralCode())
        .then((data) => {
            logger.silly(data);
            return res(dataToSend).code(data.code)
        })
        .catch((data) => { return res({ message: data.message }).code(data.code); })
}

const response = {
    status: {
        200: { code: Joi.any(), message: Joi.any().default(local['signUp']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        409: { message: Joi.any().default(local['signUp']['409']) },
        406: { message: Joi.any().default(local['signUp']['406']) },
        412: { message: Joi.any().default(local['signUp']['412']) },
        422: { message: Joi.any().default(local['signUp']['422']) },
    }
}//swagger response code

module.exports = { payloadValidator, handler, response };