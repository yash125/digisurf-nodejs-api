const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const bluebird = require('bluebird');

const local = require('../../../locales');
const postES = bluebird.promisifyAll(require("../../../models/postES"));
const userList = bluebird.promisifyAll(require("../../../models/userList"));

const validator = Joi.object({
    type: Joi.string().required().description("type : 1 = buyer , 2 = seller").error(new Error('type is missing or incoreect')),
    "skip": Joi.string().description('skip'),
    "limit": Joi.string().description('limit')
}).unknown();

const handler = async (req, res) => {
    try {
        var userId = ObjectID(req.auth.credentials._id);
        const limit = parseInt(req.query.limit) || 20;
        const skip = parseInt(req.query.skip) || 0;

        var userData = await userList.SelectOneAsync({ _id: userId })

        var condition = { "must": [{ "match": { "buyerId": uaserId } }, { "match": { "postStatus": 1 } }] }
        if (req.query.type == "1") {
            condition = { "must": [{ "match": { "buyerId": uaserId } }, { "match": { "postStatus": 1 } }] }
        } else if (req.query.type == "2") {
            condition = { "must": [{ "match": { "sellerId": uaserId } }, { "match": { "postStatus": 1 } }] }
        }

        var dataToSend = await postES.getPostDetails(condition, skip, limit, userData.follow, userId)

        if (dataToSend && dataToSend.length) {
            dataToSend.forEach(element => {
                element["isBookMarked"] = (userData && userData.bookmarks && userData.bookmarks.map(e => e.toString()).includes(element.postId)) ? true : false;
                if (element["business"] && element["business"]["businessPostType"]) element["business"]["businessPostTypeLabel"] = businessPostTypeLabel[element["business"]["businessPostType"]] || "";
                if (element["comments"] && element["comments"].length) element["comments"].reverse();
                if (element["channelId"] && element["channelId"].length) element["isChannelSubscribed"] = 1;//send 1 default, becuse post are here cause user has subscribe a channel
            });

            return res({ message: req.i18n.__('genericErrMsg')['200'] }).code(200);
        } else {
            return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);
        }


    } catch (error) {
        return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
    }
};


const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response, validator };