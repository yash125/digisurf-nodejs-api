let headerValidator = require("../../middleware/validator")
let GetAPI = require('./Get');
let entity = 'shoutout'
module.exports = [
    {
        method: 'Get',
        path: `/${entity}`,
        handler: GetAPI.handler,
        config: {
            tags: ['api', 'section'],
            description: 'This API used to get shoutout by buyer or seller',
            auth: 'user',
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
]