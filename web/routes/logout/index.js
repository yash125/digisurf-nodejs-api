let headerValidator = require("../../middleware/validator")
let PostAPI = require('./post');

module.exports = [
    {
        method: 'POST',
        path: '/logout',
        handler: PostAPI.handler,
        config: {
            description: `This API is used to logout`,
            tags: ['api', 'logout'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                // payload: PostAPI.payloadValidator,
                /* failAction: (req, reply, source, error) => {
                    failAction: headerValidator.faildAction(req, reply, source, error)
                } */
            },
            response: PostAPI.response
        }
    }
];