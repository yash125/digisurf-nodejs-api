'use strict'
const Joi = require("joi");
const logger = require('winston');
const ObjectID = require('mongodb').ObjectID;
const request = require("request");
const local = require('../../../locales');
const userList = require("../../../models/userList");

/**
 * @property {string} lang - language
 * @property {string} userId
 * @property {string} postId
 * @returns 200 : Success
 * @returns 201 : signedUp sucessfully
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const payloadValidator = Joi.object({
    streamToken: Joi.string().default("123").description("token of stream api")
}).unknown();

const handler = (req, res) => {

    userList.Update({ _id: ObjectID(req.auth.credentials._id) }, { accessKey: '0000' }, (err) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500)
        }
    });
    try {
        //stream
        request.post({
            headers: { 'content-type': 'application/json', 'lan': 'en', 'authorization': req.headers.authorization },
            url: `${process.env.LIVE_STREAM_API}/signOut`,
            json: true
        }, function (error) {
            if (error)
                console.log('0000', error);
        });
        //group call api
        request.post({
            headers: { 'content-type': 'application/json', 'lan': 'en','authorization':  req.headers.authorization },
            url: `${process.env.GROUP_CALL_API}/signOut`,
            json: true
        }, function (error) {
            if (error) {
                logger.error('KLAPWDD : ', error);
            }
        });
    } catch (error) {
        logger.error("error : ", error)
    }
    return res({ message: req.i18n.__('logout')['200'] }).code(200);

}

const response = {
    status: {
        200: { message: Joi.any().default(local['logout']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { payloadValidator, handler, response };