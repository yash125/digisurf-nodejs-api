'use strict'
//require npm modules
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

//require dependency
const local = require('../../../../locales');

const adminUsersCollection = require("../../../../models/adminUsers");

//validator for this API
const validator = Joi.object({
    userName: Joi.string().description("name"),
    email: Joi.string().description("emailId"),
    roleId: Joi.string().description("roleId"),
    password: Joi.string().description("password")
}).unknown();


/**
 * @method Post admin/user
 * @description This API create  user's roles in admin
 * 
 * @headers {string} authorization : token
 * @headers {string} lang by defualt en

 * @author Dipen Ahir
 * @since 18-Nov-2020
 */

//start API hender
const hander = (req, res) => {

    const createRole = () => {
        return new Promise((resolve, reject) => {

            adminUsersCollection.Insert({
                "email": req.payload.email,
                "userName": req.payload.userName,
                "roleId": req.payload.roleId,
                // "password": req.payload.password,
                "password": Buffer.from(req.payload.password).toString('base64'),
                "password_original": req.payload.password,
                "timestamp": new Date().getTime(),
                "status": "Active"
            }, (err, result) => {
                if (err) {
                    let responseObj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseObj);

                } else {
                    let responseObj = { code: 200, message: req.i18n.__('genericErrMsg')['200'] };
                    return resolve(responseObj);

                }
            });
        });
    };


    createRole()
        .then((data) => { return res({ message: data.message, }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};//end API hender

//start API response
const response = {
    status: {
        200: { message: Joi.string().default(local['genericErrMsg']['200']).required() },
        400: { message: Joi.string().default(local['genericErrMsg']['400']).required().example("<field> is missing") }
    }
}//end API response

//Export all constance
module.exports = { validator, hander, response };