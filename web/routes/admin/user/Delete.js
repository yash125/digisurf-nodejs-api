'use strict'
//require npm modules
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

//require dependency
const local = require('../../../../locales');

const adminUserCollection = require("../../../../models/adminUsers");

/**
 * @method Get admin/role
 * @description This API return all roles in admin
 *
 * @headers {string} authorization : token
 * @headers {string} lang by defualt en

 * @author Dipen Ahir
 * @since 18-Nov-2020
 */


//validator for this API
const validator = Joi.object({
    id: Joi.string().description("id")
}).unknown();


//start API hender
const hander = (req, res) => {

    /** This function returns all my followers */
    const getRole = () => {
        return new Promise((resolve, reject) => {

            var condition = { _id: ObjectID(req.query.id) }

            adminUserCollection.Delete(condition, (err, result) => {
                if (err) {
                    let responseObj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseObj);
                } else {
                    let responseObj = { code: 200, message: req.i18n.__('genericErrMsg')['200'] };
                    return resolve(responseObj);
                }
            });
        });
    };


    getRole()
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};//end API hender

//start API response
const response = {
    status: {
        200: { message: Joi.string().default(local['genericErrMsg']['200']).required(), data: Joi.any() },
        204: { message: Joi.string().default(local['genericErrMsg']['204']).required() },
        400: { message: Joi.string().default(local['genericErrMsg']['400']).required().example("<field> is missing") }
    }
}//end API response

//Export all constance
module.exports = { validator, hander, response };
