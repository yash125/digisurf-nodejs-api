let headerValidator = require("../../../../middleware/validator")
let PutAPI = require('./Patch');


module.exports = [
  
    {
        method: 'Put',
        path: '/admin/user/password',
        handler: PutAPI.hander,
        config: {
            description: `This API is use for update adminuser.`,
            tags: ['api', 'user'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response
        }
    }
];