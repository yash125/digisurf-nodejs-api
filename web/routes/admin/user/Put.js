'use strict'
//require npm modules
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

//require dependency
const local = require('../../../../locales');

const adminUsersCollection = require("../../../../models/adminUsers");

//validator for this API
const validator = Joi.object({
    id: Joi.string().required().description("id"),
    status: Joi.string().required().allow(["Active","Suspend","Delete"]).description("status")
}).unknown();


/**
 * @method Put admin/user
 * @description This API update status of admin user
 * 
 * @headers {string} authorization : token
 * @headers {string} lang by defualt en

 * @author Dipen Ahir
 * @since 18-Nov-2020
 */

//start API hender
const hander = (req, res) => {

    const updateRoles = () => {
        return new Promise((resolve, reject) => {

            adminUsersCollection.Update({ _id: ObjectID(req.payload.id) }, {
                "status": req.payload.status                
            }, (err, result) => {
                if (err) {
                    let responseObj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseObj);

                } else {
                    let responseObj = { code: 200, message: req.i18n.__('genericErrMsg')['200'] };
                    return resolve(responseObj);

                }
            });
        });
    };


    updateRoles()
        .then((data) => { return res({ message: data.message, }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })

};//end API hender

//start API response
const response = {
    status: {
        200: { message: Joi.string().default(local['genericErrMsg']['200']).required() },
        400: { message: Joi.string().default(local['genericErrMsg']['400']).required().example("<field> is missing") }
    }
}//end API response

//Export all constance
module.exports = { validator, hander, response };