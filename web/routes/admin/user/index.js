let headerValidator = require("../../../middleware/validator")
let PostAPI = require('./Post');
let GetAPI = require('./Get');
let PutAPI = require('./Put');
let DeleteAPI = require('./Delete');

module.exports = [
  
    {
        method: 'Put',
        path: '/admin/user',
        handler: PutAPI.hander,
        config: {
            description: `This API is use for update adminuser.`,
            tags: ['api', 'user'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response
        }
    },
    {
        method: 'Delete',
        path: '/admin/user',
        handler: DeleteAPI.hander,
        config: {
            description: `This API is use for delete adminUser.`,
            tags: ['api', 'user'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: DeleteAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
  
    {
        method: 'Get',
        path: '/admin/user',
        handler: GetAPI.hander,
        config: {
            description: `This API is use for get adminUsers.`,
            tags: ['api', 'user'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
    {
        method: 'POST',
        path: '/admin/user',
        handler: PostAPI.hander,
        config: {
            description: `This API is use for create adminUser`,
            tags: ['api', 'user'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    }
].concat(require("./password"));