let headerValidator = require("../../../middleware/validator")
let PostAPI = require('./Post');
let GetAPI = require('./Get');
let PutAPI = require('./Put');
let DeleteAPI = require('./Delete');

module.exports = [
  
    {
        method: 'Put',
        path: '/admin/role',
        handler: PutAPI.hander,
        config: {
            description: `This API is use for update roles in admin`,
            tags: ['api', 'role'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response
        }
    },
    {
        method: 'Delete',
        path: '/admin/role',
        handler: DeleteAPI.hander,
        config: {
            description: `This API is use for delete roles in admin`,
            tags: ['api', 'role'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: DeleteAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
  
    {
        method: 'Get',
        path: '/admin/role',
        handler: GetAPI.hander,
        config: {
            description: `This API is use for get roles in admin`,
            tags: ['api', 'role'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
    {
        method: 'POST',
        path: '/admin/role',
        handler: PostAPI.hander,
        config: {
            description: `This API is use for create roles in admin`,
            tags: ['api', 'role'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    }
];