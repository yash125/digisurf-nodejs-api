'use strict'
//require npm modules
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

//require dependency
const local = require('../../../../locales');

const adminRolesCollection = require("../../../../models/adminRoles");

//validator for this API
const validator = Joi.object({
    id: Joi.string().required().description("id"),
    title: Joi.string().required().description("title"),
    pages: Joi.array().required().description(`pages : [
{"pageName":"userDetails", canRead : true, canWrite:true,canEdit:true,canDetele:true }

    ]`)

}).unknown();


/**
 * @method Put admin/role
 * @description This API create  user's roles in admin
 * 
 * @headers {string} authorization : token
 * @headers {string} lang by defualt en

 * @author Dipen Ahir
 * @since 17-Nov-2020
 */

//start API hender
const hander = (req, res) => {




    /** This function returns all my followers */
    const updateRoles = () => {
        return new Promise((resolve, reject) => {

            adminRolesCollection.Update({ _id: ObjectID(req.payload.id) }, {
                "title": req.payload.title,
                "pages": req.payload.pages
            }, (err, result) => {
                if (err) {
                    let responseObj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseObj);

                } else {
                    let responseObj = { code: 200, message: req.i18n.__('genericErrMsg')['200'] };
                    return resolve(responseObj);

                }
            });
        });
    };


    updateRoles()
        .then((data) => { return res({ message: data.message, }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
        
};//end API hender

//start API response
const response = {
    status: {
        200: { message: Joi.string().default(local['genericErrMsg']['200']).required() },
        400: { message: Joi.string().default(local['genericErrMsg']['400']).required().example("<field> is missing") }
    }
}//end API response

//Export all constance
module.exports = { validator, hander, response };