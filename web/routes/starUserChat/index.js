let headerValidator = require("../../middleware/validator")
let PatchAPI = require('./Patch');

module.exports = [
    {
        method: 'PATCH',
        path: '/starUserChat',
        handler: PatchAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'starUsers'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PatchAPI.payloadValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PatchAPI.response
        }
    }
];