'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userCollection = require("../../../models/userList");


/**
 * @description API to staruser update
 * @property {string} authorization - authorization
 * @property {string} lang - language
  * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 * @author dipen
 */


const payloadValidator = Joi.object({
    isChatVisible: Joi.bool().default(false).required().description("isChatVisible"),
}).required();

const handler = (req, res) => {
    const userId = ObjectID(req.auth.credentials._id);
    var isChatVisible = req.payload.isChatVisible // has the star user allowed other users to chat with them or not

    const setStarUserChatInUserCollection = () => {
        return new Promise((resolve, reject) => {
            userCollection.Update({ _id: userId }, { "starRequest.isChatVisible": isChatVisible }, (e) => { if (e) {logger.error(e);reject(e) }});
            return resolve({ code: 200, message: req.i18n.__('PatchStarUserChat')['200'] });
        });
    };

    setStarUserChatInUserCollection()
        .then((data) => { return res({ message: data.message }).code(data.code); })
        .catch((data) => { return res({ message: data.message }).code(data.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['PatchStarUserChat']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { payloadValidator, handler, response };