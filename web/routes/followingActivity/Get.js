const Joi = require("joi");

const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const followCollection = require("../../../models/follow");

const notificationCollection = require("../../../models/notification");


const validator = Joi.object({
    offset: Joi.string().description("offset, query parameter"),
    limit: Joi.string().description("limit, query parameter")
}).required();

const hander = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;
    let myFollowers = [];

    const getMyFollowers = () => {
        return new Promise((resolve, reject) => {

            let condition = [
                { "$match": { "follower": userId, "end": false } },
                { "$project": { "followee": 1 } }
            ];

            followCollection.Aggregate(condition, (e, d) => {
                if (e) {
                    let responseObj = { message: 'internal server error' };
                    return reject(responseObj);
                } else if (d.length === 0) {
                    return reject("---");
                } else {
                    for (let index = 0; index < d.length; index++) {
                        myFollowers.push(ObjectID(d[index]["followee"]));
                    }
                    return resolve("--");
                }
            });
        });
    };

    const getActivity = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { '$match': { "from": { "$in": myFollowers }, "end": false } },
                { "$lookup": { "from": "customer", "localField": "from", "foreignField": "_id", "as": "customer" } },
                // { "$unwind": "$userList" },
                { "$unwind": { "path": "$userList", "preserveNullAndEmptyArrays": true } },
                { "$lookup": { "from": "customer", "localField": "to", "foreignField": "_id", "as": "userListTo" } },
                { "$unwind": { "path": "$userListTo", "preserveNullAndEmptyArrays": true } },
                { "$lookup": { "from": "posts", "localField": "postId", "foreignField": "_id", "as": "postData" } },
                { "$unwind": { "path": "$postData", "preserveNullAndEmptyArrays": true } },
                { "$lookup": { "from": "customer", "localField": "postData.userId", "foreignField": "_id", "as": "postUserData" } },
                { "$unwind": { "path": "$postUserData", "preserveNullAndEmptyArrays": true } },
                {
                    "$project": {
                        "_id": 1, "timeStamp": "$start", "type": "$type.status", "message": "$type.message",
                        "userId": "$userList._id", "userName": "$userList.userName", "profilePic": "$userList.profilePic",
                        "targetUserName": "$userListTo.userName",
                        "targetProfilePic": "$userListTo.profilePic",
                        "targetId": "$userListTo._id",
                        "postData": {
                            "postId": "$postData._id", "imageUrl1": "$postData.imageUrl1", "thumbnailUrl1": "$postData.thumbnailUrl1",
                            "imageUrl1Width": "$postData.imageUrl1Width", "imageUrl1Height": "$postData.imageUrl1Height",
                            "mediaType1": "$postData.mediaType1", "createdOn": "$postData.createdOn", "timeStamp": "$postData.timeStamp",
                            "title": "$postData.title", "categoryId": "$postData.categoryId", "channelId": "$postData.categoryId",
                            "phoneNumber": "$postData.number", "hashTags": { "$ifNull": ["$postData.hashTags", []] }, "likesCount": { '$ifNull': ['$postData.likesCount', 0] },
                            "profilepic": '$userData.profilePic',
                            "comments": { "$ifNull": ['$postData.comments', []] },
                            "totalComments": { "$size": { '$ifNull': ['$postData.comments', []] } },
                            "distinctViews": { "$size": { '$ifNull': ['$postData.distinctViews', []] } },
                            "location": { '$ifNull': ['$postData.location', ""] }, "place": { '$ifNull': ['$postData.place', ""] },
                            "city": { "$ifNull": ['$postData.city', ""] },
                            "userId": "$postUserData._id",
                            "userName": "$postUserData.userName", "profilePic": "$postUserData.profilePic"
                        }
                    }
                },
                { "$match": { "userId": { "$ne": userId }, "targetId": { "$ne": userId } } },
                {
                    "$match": {
                        "$or": [
                            { "type": 1, "targetUserName": { "$exists": true } },
                            { "type": 2, "targetUserName": { "$exists": true } },
                            { "type": 3, "targetUserName": { "$exists": true } }
                        ]
                    }
                },
                { "$sort": { "timeStamp": -1 } }, { "$skip": offset }, { "$limit": limit }
            ];
            notificationCollection.Aggregate(condition, (e, d) => {
                if (e) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    let responseObj = { message: req.i18n.__('genericErrMsg')['204'], code: 204 };
                    return reject(responseObj);
                } else {

                    for (let index = 0; index < d.length; index++) {
                        d[index]["firstName"] = (d[index]["firstName"]) ? d[index]["firstName"] : d[index]["userName"]
                        d[index]["lastName"] = (d[index]["lastName"]) ? d[index]["lastName"] : ""
                        switch (d[index].type) {
                            case 2: {
                                d[index]["message"] = " liked post ";
                                break;
                            }
                            case 5: {
                                d[index]["message"] = " commented on post ";
                                break;
                            }
                            case 3: {
                                delete d[index]["postData"];
                                break;
                            }
                        }
                    }
                    let responseObj = { message: req.i18n.__('GetFollowingActivity')['200'], data: d };
                    return resolve(responseObj);
                }
            });
        });
    };

    getMyFollowers()
        .then(getActivity)
        .then((data) => { return res(data).code(200); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetFollowingActivity']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },

    }
}//swagger response code


module.exports = { validator, hander, response };