const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const userListCollection = require("../../../models/userList")
const followCollection = require("../../../models/follow")


/**
 * @description API to search people 
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token
 * @property {string} username
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 400 : Parameters missing
 * @returns 500 : Internal Server Error
 * @author Dipen
 * @date 23rd May 2019
  */


const validator = Joi.object({
    username: Joi.string().allow("").default("").description("username to be searched"),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit")
}).required();

const handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    let username = (req.query.username) ? req.query.username.trim().toLowerCase() : "";
    let offset = parseInt(req.query.offset) || 0; // this is used to denote the start point of the page
    let limit = parseInt(req.query.limit) || 20; // this is for the page size , if the client does not send the page size , the default page size is 20
    var followeeIdObj = {}; // this variable is used to determine the following status of a profile , if user a follows user b , user a is followed by user b or user a has requested to follow user b where user B is a private profile abnd has not accepted user a's follow request

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }


    // this method is used to determine the users a particular user is following or who is following the user 

    const getMyFollowee = () => {
        return new Promise((resolve, reject) => {
            followCollection.Select({ follower: userId }, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    for (let index = 0; index < result.length; index++) {
                        followeeIdObj[result[index].followee.toString()] = result[index]["type"];
                    }
                    return resolve(true);
                }
            })
        });
    };

    // this generates the results of the user search 

    const getUserList = () => {
        return new Promise((resolve, reject) => {

            if (username !== "") {
                let condition = [
                    {
                        "$project": {
                            "_id": 1, "firstName": 1, "registeredOn": 1, "private": 1, "businessProfile": 1,
                            "userName": 1, "profilePic": 1, "userStatus": 1, "blocked": 1, "starRequest": 1, "verified": 1,
                            "lastName": { '$ifNull': ['$lastName', " "] },
                            "fullName": { "$concat": ["$firstName", " ", "$lastName"] },
                            "legacyFullName": { "$concat": ["$firstName", "$lastName"] }

                        }
                    },
                    {
                        "$match": {
                            "userName": { "$exists": true },
                            "$or": [{ "userName": { "$regex": username, "$options": "i" } },
                            // { "firstName": { "$regex": username, "$options": "i" } },
                            // { "lastName": { "$regex": username, "$options": "i" } },
                            { "fullName": { "$regex": username, "$options": "i" } },
                            { "legacyFullName": { "$regex": username, "$options": "i" } },
                            ],
                            "userStatus": 1, "blocked": { "$ne": userId }
                        }
                    },
                    {
                        "$project": {
                            "_id": 1, "firstName": 1, "registeredOn": 1, "private": 1, "businessProfile": 1,
                            "userName": 1, "profilePic": 1, "starRequest": 1, "verified": 1,
                            "lastName": { '$ifNull': ['$lastName', " "] },

                        }
                    },
                    { "$match": { "_id": { "$ne": userId } } },
                    { "$skip": offset },
                    { "$limit": limit }
                ];

                // "e" denotes "error" , "d" denotes data

                userListCollection.Aggregate(condition, (e, d) => {
                    if (e) {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (d.length === 0) {
                        return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                    } else {

                        for (let index = 0; index < d.length; index++) {
                            d[index]["lastName"] = d[index]["lastName"] || "";
                            d[index]["followStatus"] = (followeeIdObj[d[index]._id.toString()]) ? followeeIdObj[d[index]._id.toString()]["status"] : 0
                            d[index]["followStatus"] = (d[index]["followStatus"] == 3) ? 0 : d[index]["followStatus"];
                            d[index]["private"] = d[index]["private"] || 0;
                            d[index]["friendStatusCode"] = (userData.friendList && userData.friendList.map(e => e.toString()).includes(d[index]["_id"].toString())) ? 2 : (userData.myFriendRequest && userData.myFriendRequest.map(e => e.toString()).includes(d[index]["_id"].toString())) ? 3 : 1;
                            d[index]["friendStatusText"] = (d[index]["friendStatusCode"] == 2) ? "friends" : (d[index]["friendStatusCode"] == 3) ? "userSentARequestToTargetUser" : "notAFirend";
                            try {
                                d[index]["isStar"] = (d[index]["starRequest"] && d[index]["starRequest"]["starUserProfileStatusCode"] == 4) ? true : false;
                            } catch (error) {
                                d[index]["isStar"] = false;
                                console.log("error ", error)
                            }

                        }

                        return resolve({ code: 200, message: req.i18n.__('GetSearchPeople')['200'], data: d });
                    }
                });
            } else {
                let condition = [
                    { "$group": { "_id": "$followee", "total": { "$sum": 1 } } },
                    { "$sort": { "total": -1, "_id": -1 } },
                    { "$lookup": { "from": "customer", "localField": "_id", "foreignField": "_id", "as": "userData" } },
                    { "$unwind": "$userData" },
                    {
                        "$project": {
                            "_id": 1, "registeredOn": "$userData.registeredOn", "private": "$userData.private",
                            "businessProfile": "$userData.businessProfile",
                            "userName": "$userData.userName", "firstName": "$userData.firstName", "lastName": "$userData.lastName",
                            "profilePic": "$userData.profilePic", "userStatus": "$userData.userStatus", "blocked": "$userData.blocked",
                            "starRequest": "$userData.starRequest"
                        }
                    },
                    { "$match": { "_id": { "$ne": userId } } },
                    { "$match": { "userStatus": 1, "blocked": { "$ne": userId } } },
                    { "$skip": offset }, { "$limit": limit },
                ];
                followCollection.Aggregate(condition, (e, d) => {
                    if (e) {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (d.length === 0) {
                        return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                    } else {
                        for (let index = 0; index < d.length; index++) {
                            d[index]["followStatus"] = (followeeIdObj[d[index]._id.toString()]) ? followeeIdObj[d[index]._id.toString()]["status"] : 0
                            d[index]["followStatus"] = (d[index]["followStatus"] == 3) ? 0 : d[index]["followStatus"];
                            d[index]["private"] = d[index]["private"] || 0;
                            try {
                                d[index]["isStar"] = (d[index]["starRequest"] && d[index]["starRequest"]["starUserProfileStatusCode"] == 4) ? true : false;
                            } catch (error) {
                                d[index]["isStar"] = false;
                                console.log("error ", error)
                            }
                        }
                        return resolve({ code: 200, message: req.i18n.__('GetSearchPeople')['200'], data: d });
                    }
                });
            }
        })
    };

    // improvement : get the relationship between users after generating the user list and not beforre

    getUserData()
        .then(getMyFollowee)
        .then(getUserList)
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetSearchPeople']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { validator, response, handler };

