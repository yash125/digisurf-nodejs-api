const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const postESCollection = require("../../../models/postES");
const followCollection = require("../../../models/follow");
const userListCollection = require("../../../models/userList");
const local = require('../../../locales');

/**
 * @description API to update a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} userId
 * @property {string} imageUrl1
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */
const validator = Joi.object({
}).required();

const queryValidator = Joi.object({
    place: Joi.string().required().description('channelId'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();

const handler = (req, res) => {
    let userId = ObjectID(req.auth.credentials._id);
    let limit = parseInt(req.query.limit) || 20;
    let offset = parseInt(req.query.offset) || 0;
    var followStatus = {};

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(userId), "end": false, "followee": { "$nin": userData.blocked || [] } };
            followCollection.Select(condition, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        followStatus[element.followee] = element.type
                    }, this);
                    return resolve(true);
                }
            });
        });
    }
    const getPosts = () => {
        return new Promise((resolve, reject) => {

            let condition = {
                "must": [
                    { "match": { "postStatus": 1 } },
                    { "match": { "placeId": req.query.place } }
                ]
            };

            postESCollection.getPostDetails(condition, offset, limit, userData.follow, userId.toString(), (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        element["followStatus"] = (followStatus[element["userId"]]) ? followStatus[element["userId"]]["status"] : 0;
                        if (element["channelId"] && element["channelId"].length) element["isChannelSubscribed"] = (userData && userData.subscribeChannels && userData.subscribeChannels.map(e => e.toString()).includes(element.channelId.toString())) ? 1 : 0;
                    }, this);
                    let responseobj = { message: req.i18n.__('GetPostsByPlace')['200'], data: result };
                    return resolve(responseobj);
                }
            });
        });
    }

    getUserData()
    .then(() => { return getFolloweeIds(); })    
    .then(() => { return getPosts(); })
        .then((data) => { return res(data).code(200); })
        .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500); })
}


const response = {
    status: {
        200: { message: Joi.any().default(local['GetHome']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { validator, queryValidator, handler, response };