const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const postsCollection = require("../../../models/posts");
const followCollection = require("../../../models/follow");
const userListCollection = require("../../../models/userList");
const local = require('../../../locales');

/**
 * @description API to update a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} userId
 * @property {string} imageUrl1
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */
const queryValidator = Joi.object({
    place: Joi.string().required().description('channelId'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).unknown();

const handler = (req, res) => {
    let userId = ObjectID(req.auth.credentials._id);
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    var followStatus = {};

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }
    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(userId), "end": false, "followee": { "$nin": userData.blocked || [] } };
            followCollection.Select(condition, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        followStatus[element.followee] = element.type
                    }, this);
                    return resolve(true);
                }
            });
        });
    }
    const getPosts = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "place": req.query.place } },
                { "$lookup": { "from": "customer", "localField": "userId", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                { "$lookup": { "from": "channel", "localField": "channelId", "foreignField": "_id", "as": "channleData" } },
                { "$unwind": { "path": "$channleData", "preserveNullAndEmptyArrays": true } },
                { "$lookup": { "from": "category", "localField": "categoryId", "foreignField": "_id", "as": "categoryData" } },
                { "$unwind": { "path": "$categoryData", "preserveNullAndEmptyArrays": true } },
                { "$lookup": { "from": "dublyAudio", "localField": "musicId", "foreignField": "_id", "as": "musicData" } },
                { "$unwind": { "path": "$musicData", "preserveNullAndEmptyArrays": true } },
                {
                    "$project": {
                        "postId": '$_id', "shareCount": "$shareCount", "userName": '$userData.userName', "title": '$title',
                        "imageUrl1": '$imageUrl1', "thumbnailUrl1": '$thumbnailUrl1',
                        "likes": '$likes', "userId": '$userId', "musicData": 1,
                        "mediaType1": '$mediaType1', "timeStamp": '$timeStamp',
                        "createdOn": '$createdOn', "phoneNumber": '$number', "profilepic": '$userData.profilePic',

                        "city": { "$ifNull": ['$city', ""] },
                        "place": { '$ifNull': ['$place', ""] },
                        "hashTags": { '$ifNull': ['$hashTags', []] },
                        "comments": { "$ifNull": ['$comments', []] },
                        "location": { '$ifNull': ['$location', ""] },
                        "likesCount": { '$ifNull': ['$likesCount', 0] },
                        "channelImageUrl": { '$ifNull': ["$channelImageUrl", ""] },
                        "categoryName": { '$ifNull': ["$categoryName", ""] },
                        "categoryUrl": { '$ifNull': ["$categoryActiveIconUrl", ""] },
                        "channelName": { '$ifNull': ["$channelName", ""] },
                        "categoryId": { '$ifNull': ['$categoryId', ""] },
                        "channelId": { '$ifNull': ['$channelId', ""] },
                        "imageUrl1Width": { "$ifNull": ["$imageUrl1Width", ""] },
                        "imageUrl1Height": { "$ifNull": ["$imageUrl1Height", ""] },
                        "totalComments": { "$size": { '$ifNull': ['$comments', []] } },
                        "distinctViews": { "$size": { '$ifNull': ['$distinctViews', []] } },
                    }
                },
                { "$sort": { "_id": -1 } }, { "$skip": offset }, { "$limit": limit },
            ];
            postsCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {

                    result.forEach(function (element) {
                        element["followStatus"] = (followStatus[element["userId"]]) ? followStatus[element["userId"]]["status"] : 0;
                        element["liked"] = (element["likes"]) ? element["likes"].map(id => id.toString()).includes(userId.toString()) : false;
                        delete element["likes"];
                    }, this);

                    let responseobj = { message: req.i18n.__('GetPostsByPlace')['200'], data: result };
                    return resolve(responseobj);
                }
            });
        });
    }

    getUserData()
        .then(() => { return getFolloweeIds(); })
        .then(() => { return getPosts(); })
        .then((data) => { return res(data).code(200); })
        .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500); })
}


const response = {
    status: {
        200: { message: Joi.any().default(local['GetHome']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { queryValidator, handler, response };