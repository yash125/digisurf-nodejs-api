let headerValidator = require("../../../middleware/validator")
let GetAPI = require('./Get');
let entity = 'section'
module.exports = [
    {
        method: 'Get',
        path: `/${entity}/featured`,
        handler: GetAPI.handler,
        config: {
            tags: ['api', 'section'],
            description: 'This API used to get entities',
            auth: {
                strategies: ['user', 'guest']
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];