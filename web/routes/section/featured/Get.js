const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const featuredSections = require("../../../../models/featuredSections");


const validator = Joi.object({
    //    entityName: Joi.string().required().description("entityName").error(new Error('entityName is missing or incoreect')),
}).unknown();

const handler = (req, res) => {

    const getData = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$unwind": "$entityIds" },
                {
                    "$lookup": {
                        from: "customer",
                        localField: "entityIds",
                        foreignField: "_id",
                        as: "entityData"
                    }
                },
                {
                    $unwind:
                    {
                        path: "$entityData",
                        preserveNullAndEmptyArrays: true
                    }
                },
                {
                    "$group": {
                        "_id": "$_id",
                        "entityName": { "$first": "$entityName" },
                        "imgUrl": { "$first": "$imgUrl" },
                        "type": { "$first": "$type" },
                        "sequanceId": { "$first": "$sequanceId" },
                        "entityData": {
                            "$push": {
                                "_id": "$entityData._id", "userName": "$entityData.userName",
                                "firstName": "$entityData.firstName", "lastName": "$entityData.lastName",
                                "profilePic": "$entityData.profilePic", "starUserKnownBy": "$entityData.starRequest.starUserKnownBy"
                            }
                        },
                        "entityIds": { "$push": "$entityIds" }
                    }
                }, { "$sort": { sequanceId: 1 } }
            ];

            featuredSections.Aggregate(condition, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else if (result && result.length) {


                    result = result.map(e => {
                        if (e.type == "Products") {
                            return {
                                ...e,
                                "entityData": []
                            }
                        } else {
                            return e
                        }

                    })
                    let responseobj = { code: 200, message: req.i18n.__('genericErrMsg')['200'], data: result };
                    return resolve(responseobj);
                } else {
                    let responseobj = { code: 204, message: req.i18n.__('genericErrMsg')['204'] };
                    return reject(responseobj);
                }
            })
        });
    }



    getData()
        .then((data) => {
            return res({
                message: data.message,
                data: data.data
            }).code(data.code);
        })
        .catch((err) => {
            return res({
                message: err.message
            }).code(err.code);
        });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response, validator };