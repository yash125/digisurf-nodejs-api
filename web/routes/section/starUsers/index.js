let headerValidator = require("../../../middleware/validator")
let GetAPI = require('./Get');
let entity = 'section'
module.exports = [
    {
        method: 'Get',
        path: `/${entity}/starUsers`,
        handler: GetAPI.handler,
        config: {
            tags: ['api', 'section'],
            description: 'This API used to get starUsers',
            auth: 'basic',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];