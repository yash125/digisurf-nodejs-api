const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const featuredSections = require("../../../../models/featuredSections");
const userList = require("../../../../models/userList");


const validator = Joi.object({
    entityId: Joi.string().required().description("entityId").error(new Error('entityId is missing')),
    searchText: Joi.string().allow(["", null]).default("").description("searchText").error(new Error('searchText is missing')),
    skip: Joi.string().allow(["", null]).default(0).description("skip").error(new Error('skip is missing')),
    limit: Joi.string().allow(["", null]).default(20).description("limit").error(new Error('limit is missing')),
}).unknown();

const handler = (req, res) => {

    var entityList = [];
    var skip = req.query.skip ? parseInt(req.query.skip) : 0
    var limit = req.query.limit ? parseInt(req.query.limit) : 20

    const getEntityData = () => {
        return new Promise((resolve, reject) => {

            featuredSections.SelectOne({ _id: ObjectID(req.query.entityId) }, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else if (result && result._id) {
                    entityList = (result && result.entityIds && result.entityIds.length) ? result.entityIds.map(e => ObjectID(e.toString())) : [];

                    // let responseobj = { code: 200, message: req.i18n.__('genericErrMsg')['200'], data: result };
                    return resolve(true);
                } else {
                    let responseobj = { code: 204, message: req.i18n.__('genericErrMsg')['204'] };
                    return reject(responseobj);
                }
            })
        });
    }

    const getStarUserData = () => {
        return new Promise((resolve, reject) => {

            userList.SelectWithSort({
                _id: { "$nin": entityList }, "starRequest.starUserProfileStatusCode": 4,
                userStatus: 1,
                userName: { $regex: req.query.searchText }
            }, { _id: -1 }, {
                userName: 1, _id: 1, starRequest: 1, profilePic: 1, firstName: 1, lastName: 1, email: 1
            }, skip, limit, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else if (result && result.length) {

                    result = result.map(e => {
                        return {
                            _id: e._id,
                            userName: e.userName,
                            starUserEmail: e.starRequest.starUserEmail,
                            starUserPhoneNumber: e.starRequest.starUserPhoneNumber,
                            starUserKnownBy: e.starRequest.starUserKnownBy,
                            profilePic: e.profilePic,
                            firstName: e.firstName,
                            lastName: e.lastName,
                            userEmail: e.email.id || "",
                        }
                    })

                    let responseobj = { code: 200, message: req.i18n.__('genericErrMsg')['200'], data: result };
                    return resolve(responseobj);
                } else {
                    let responseobj = { code: 204, message: req.i18n.__('genericErrMsg')['204'] };
                    return reject(responseobj);
                }
            })
        });
    }



    getEntityData()
        .then(getStarUserData)
        .then((data) => {
            return res({
                message: data.message,
                data: data.data
            }).code(data.code);
        })
        .catch((err) => {
            return res({
                message: err.message
            }).code(err.code);
        });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response, validator };