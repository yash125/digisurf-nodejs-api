const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const featuredSections = require("../../../../models/featuredSections");


const validator = Joi.object({
    entityId: Joi.string().required().description("entityId").error(new Error('entityId is missing')),
    entityIdToAdd: Joi.string().required().description("entityId").error(new Error('entityId is missing')),
}).unknown();

const handler = (req, res) => {

    const insertData = () => {
        return new Promise((resolve, reject) => {
            featuredSections.UpdateByIdWithPush(
                { "_id": ObjectID(req.payload.entityId) },
                {
                    entityIds: ObjectID(req.payload.entityIdToAdd)

                }, (err, result) => {
                    if (err) {
                        let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                        return reject(responseobj);
                    } else {
                        return resolve(true);
                    }
                })
        });
    }



    insertData()
        .then(() => {
            return res({
                message: req.i18n.__('genericErrMsg')['200']
            }).code(200);
        })
        .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500); });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response, validator };