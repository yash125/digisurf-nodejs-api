const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const featuredSections = require("../../../../models/featuredSections");


const validator = Joi.object({
    entityId: Joi.string().required().description("entityId").error(new Error('entityId is missing or incorrect')),
}).unknown();

const handler = (req, res) => {

    const getData = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "_id": ObjectID(req.query.entityId) } },
                { "$unwind": "$entityIds" },
                {
                    "$lookup": {
                        from: "customer",
                        localField: "entityIds",
                        foreignField: "_id",
                        as: "userData"
                    }
                }, { "$unwind": "$userData" },
                {
                    "$project": {
                        "_id": "$userData._id", "userName": "$userData.userName",
                        "firstName": "$userData.firstName",
                        "lastName": "$userData.lastName",
                        "profilePic": "$userData.profilePic"
                    }
                }
            ];

            featuredSections.Aggregate(condition, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else if (result && result.length) {

                    let responseobj = { code: 200, message: req.i18n.__('genericErrMsg')['200'], data: result };
                    return resolve(responseobj);
                } else {
                    let responseobj = { code: 204, message: req.i18n.__('genericErrMsg')['204'] };
                    return reject(responseobj);
                }
            })
        });
    }



    getData()
        .then((data) => {
            return res({
                message: data.message,
                data: data.data
            }).code(data.code);
        })
        .catch((err) => {
            return res({
                message: err.message
            }).code(err.code);
        });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response, validator };