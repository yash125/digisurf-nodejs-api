const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const featuredSections = require("../../../../models/featuredSections");


const validator = Joi.object({
    entityId: Joi.string().required().description("entityId").error(new Error('entityId is missing or incorrect')),
    entityIdForDelete: Joi.string().required().description("entityIdForDelete").error(new Error('entityIdForDelete is missing or incorrect')),
}).unknown();

const handler = (req, res) => {

    const DeleteData = () => {
        return new Promise((resolve, reject) => {
            let condition = { "_id": ObjectID(req.query.entityId) };
            let dataToDelete = { "entityIds": ObjectID(req.query.entityIdForDelete) }

            featuredSections.UpdateByIdWithPull(condition, dataToDelete, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    let responseobj = { code: 200, message: req.i18n.__('genericErrMsg')['200'] };
                    return resolve(responseobj);
                }
            })
        });
    }



    DeleteData()
        .then((data) => {
            return res({
                message: data.message
            }).code(data.code);
        })
        .catch((err) => {
            return res({
                message: err.message
            }).code(err.code);
        });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']) },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response, validator };