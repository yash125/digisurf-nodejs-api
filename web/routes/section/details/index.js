let headerValidator = require("../../../middleware/validator")
let GetAPI = require('./Get');
let DeleteAPI = require('./Delete');
let PostAPI = require('./Post');
let PutAPI = require('./Put');
let entity = 'section';


module.exports = [
    {
        method: 'Put',
        path: `/${entity}/details`,
        handler: PutAPI.handler,
        config: {
            tags: ['api', 'section'],
            description: 'This API used to reorder entities in details',
            auth: 'Admin',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response
        }
    },

    {
        method: 'Post',
        path: `/${entity}/details`,
        handler: PostAPI.handler,
        config: {
            tags: ['api', 'section'],
            description: 'This API used to add entities in details',
            auth: 'Admin',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },

    {
        method: 'Delete',
        path: `/${entity}/details`,
        handler: DeleteAPI.handler,
        config: {
            tags: ['api', 'section'],
            description: 'This API used to remove entities in details',
            auth: 'Admin',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: DeleteAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
    {
        method: 'Get',
        path: `/${entity}/details`,
        handler: GetAPI.handler,
        config: {
            tags: ['api', 'section'],
            description: 'This API used to get entities in details',
            auth: 'Admin',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }

].concat(require("./productDetails"));