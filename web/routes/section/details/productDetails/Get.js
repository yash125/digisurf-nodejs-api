const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../../locales');
const featuredSections = require("../../../../../models/featuredSections");
var request = require('request');


const validator = Joi.object({
    entityId: Joi.string().required().description("entityId").error(new Error('entityId is missing or incorrect')),
}).unknown();

const handler = (req, res) => {

    var productIds = []

    const getEntityData = () => {
        return new Promise((resolve, reject) => {

            featuredSections.SelectOne({ "_id": ObjectID(req.query.entityId) }, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else if (result && result._id) {
                    productIds = result.entityIds
                    let responseobj = { code: 200, message: req.i18n.__('genericErrMsg')['200'], data: result };
                    return resolve(responseobj);
                } else {
                    let responseobj = { code: 204, message: req.i18n.__('genericErrMsg')['204'] };
                    return reject(responseobj);
                }
            })
        });
    }

    const getProductData = () => {
        return new Promise((resolve, reject) => {

            var options = {
                'method': 'POST',
                // 'url':'https://pyfilterapi.dubly.xyz/python/product/list',
                'url': process.env.PYTHON_PRODUCT + 'python/product/list',
                'headers': {
                    // 'Authorization': `${req.headers.admintoken}`,
                    'Authorization': `{"metaData":{},"type":"Admin","userId":"","userType":"Admin"}`,
                    'language': 'en',
                    'Content-Type': 'application/json'
                },
                body: { "productIds": productIds },
                json: true
            };
            request(options, function (error, response, body) {
                if (error) throw new Error(error);

                if (body && body.data) {

                    let result = body.data.data.map(ele => {
                        return {
                            _id: ele.parentProductId,
                            productName: ele.productName,
                            image: ele.images[0].small
                        }
                    })
                    let dataToSend = [];

                    productIds.map(e => {

                        if (result && result.find(f => f._id.toString() == e.toString())) {
                            dataToSend.push(result.find(f => f._id.toString() == e.toString()) )
                        }

                    })

                    let responseobj = { code: 200, message: req.i18n.__('genericErrMsg')['200'], data: dataToSend };
                    return resolve(responseobj);

                } else {

                    let responseobj = { code: 204, message: req.i18n.__('genericErrMsg')['204'] };
                    return reject(responseobj);
                }

            });



        });
    }



    getEntityData()
        .then(getProductData)
        .then((data) => {
            return res({
                message: data.message,
                data: data.data
            }).code(data.code);
        })
        .catch((err) => {
            return res({
                message: err.message
            }).code(err.code);
        });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response, validator };