let headerValidator = require("../../../../middleware/validator")
let GetAPI = require('./Get');
let entity = 'section';


module.exports = [
   
    {
        method: 'Get',
        path: `/${entity}/details/productDetails`,
        handler: GetAPI.handler,
        config: {
            tags: ['api', 'section'],
            description: 'This API used to get entities in details for product',
            auth: 'Admin',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }

];