const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const featuredSections = require("../../../models/featuredSections");


const validator = Joi.object({
    currentId: Joi.string().required().description("currentId").error(new Error('currentId is missing or incoreect')),
    nextId: Joi.string().required().description("nextId").error(new Error('nextId is missing or incoreect')),
}).unknown();

const handler = (req, res) => {

    const updateData = () => {
        return new Promise((resolve, reject) => {

            featuredSections.Select({
                "_id": {
                    "$in": [
                        ObjectID(req.payload.currentId),
                        ObjectID(req.payload.nextId)
                    ]
                }
            }, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else if (result && result.length && result.length == 2) {

                    var currentId_sequence = 0, nextId_sequence = 0;

                    currentId_sequence = result.find(e=>e._id.toString()==req.payload.currentId).sequanceId
                    nextId_sequence = result.find(e=>e._id.toString()==req.payload.nextId).sequanceId

                    featuredSections.Update({_id: ObjectID(req.payload.currentId)},{sequanceId : nextId_sequence},(err,result)=>{

                    });

                    
                    featuredSections.Update({_id: ObjectID(req.payload.nextId)},{sequanceId : currentId_sequence},(err,result)=>{
                        
                    })


                    return resolve(true);
                }
                else {
                    return resolve(true);
                }
            });
        });
    }


    updateData()
        .then(() => {
            return res({
                message: req.i18n.__('genericErrMsg')['200']
            }).code(200);
        })
        .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500); });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response, validator };