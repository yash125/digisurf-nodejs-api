const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const featuredSections = require("../../../models/featuredSections");


const validator = Joi.object({
    entityName: Joi.string().required().description("entityName").error(new Error('entityName is missing or incoreect')),
    imgUrl: Joi.string().allow([null, ""]).default("").description("imgUrl").error(new Error('imgUrl is missing')),
    type: Joi.string().allow(["Stars", "Products"]).description("type").error(new Error('type is missing')),
    entityIds: Joi.array().allow([null, []]).default([]).description("entityIds").error(new Error('entityIds is missing')),
}).unknown();

const handler = (req, res) => {

    const insertData = () => {
        return new Promise((resolve, reject) => {
            featuredSections.Insert({
                entityName: req.payload.entityName,
                imgUrl: req.payload.imgUrl,
                type: req.payload.type,
                sequanceId: new Date().getTime(),
                entityIds: req.payload.entityIds ? req.payload.entityIds.map(e => ObjectID(e)) : [],
            }, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    return resolve(true);
                }
            })
        });
    }



    insertData()
        .then(() => {
            return res({
                message: req.i18n.__('genericErrMsg')['200']
            }).code(200);
        })
        .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500); });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response, validator };