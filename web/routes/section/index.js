let headerValidator = require("../../middleware/validator")
let GetAPI = require('./Get');
let PostAPI = require("./Post");
let DeleteAPI = require("./Delete");
let PatchAPI = require("./Patch");
let PutAPI = require("./Put");
let entity = 'section'
module.exports = [
    {
        method: 'Get',
        path: `/${entity}`,
        handler: GetAPI.handler,
        config: {
            tags: ['api', 'section'],
            description: 'This API used to get entities',
            auth: 'basic',
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
    {
        method: 'Delete',
        path: `/${entity}`,
        handler: DeleteAPI.handler,
        config: {
            tags: ['api', 'section'],
            description: 'This API used to delete entity',
            auth: 'basic',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: DeleteAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
    {
        method: 'Put',
        path: `/${entity}`,
        handler: PutAPI.handler,
        config: {
            tags: ['api', 'section'],
            description: 'This API used to update order',
            auth: 'basic',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response
        }
    },
    {
        method: 'Patch',
        path: `/${entity}`,
        handler: PatchAPI.handler,
        config: {
            tags: ['api', 'section'],
            description: 'This API used to update entity',
            auth: 'basic',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PatchAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PatchAPI.response
        }
    },
    {
        method: 'Post',
        path: `/${entity}`,
        handler: PostAPI.handler,
        config: {
            tags: ['api', 'section'],
            description: 'This API used to create entity',
            auth: 'basic',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
].concat(require("./featured"))
    .concat(require("./details"),require("./starUsers"));