const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const featuredSections = require("../../../models/featuredSections");


const validator = Joi.object({
    _id: Joi.string().required().min(24).max(24).description("_id").error(new Error('_id is missing or incoreect')),
}).unknown();

const handler = (req, res) => {

    const deleteData = () => {
        return new Promise((resolve, reject) => {

            featuredSections.Delete(
                { _id: ObjectID(req.query._id) }, (err, result) => {
                    if (err) {
                        let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                        return reject(responseobj);
                    } else {
                        return resolve(true);
                    }
                })
        });
    }



    deleteData()
        .then(() => {
            return res({
                message: req.i18n.__('genericErrMsg')['200']
            }).code(200);
        })
        .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500); });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response, validator };