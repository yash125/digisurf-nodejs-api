let PostAPI = require('./Post');

module.exports = [
    {
        method: 'POST',
        path: '/webhook/mqtt',
        handler: PostAPI.handler,
        config: {
            description: `This API used for get webhook from mqtt`,
            tags: ['api', 'mqtt'],
            auth: false
        }
    }
];