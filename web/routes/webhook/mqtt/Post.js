'use strict'
var Joi = require("joi");
var logger = require('winston');
var Promise = require('promise');
var local = require('../../../../locales');
var rabbitMq = require('../../../../library/rabbitMq');


const handler = (req, res) => {

    let checkUserNameAndPhoneNumber = () => {
        return new Promise((resolve, reject) => {
            if (req.payload && req.payload.topic) {

                logger.silly("req.payload.topic ", req.payload.topic)
                switch (req.payload.topic) {
                    case String(req.payload.topic.match(/^Message.*/)):
                    case String(req.payload.topic.match(/^Acknowledgement.*/)):
                    case String(req.payload.topic.match(/^GroupChat.*/)):
                    case String(req.payload.topic.match(/^GroupChatAcknowledgement.*/)):
                        {
                            let dataToSend = new Buffer.from(req.payload.payload, 'base64').toString();
                            dataToSend = JSON.parse(dataToSend);
                            dataToSend["topic"] = req.payload.topic;
                            rabbitMq.sendToQueue(rabbitMq.mqtt_insert_Message, dataToSend, (err) => { })
                            break;
                        }
                }
            } else {
                logger.silly("req.payload ", req.payload)
            }
            return resolve({});
        });
    }

    checkUserNameAndPhoneNumber()
        .then((data) => {
            return res({ message: req.i18n.__('genericErrMsg')['200'] }).code(200)
        })
        .catch((data) => {
            console.log("error - data ", data)
            return res({ message: req.i18n.__('genericErrMsg')['200'] }).code(200);
        })
}


module.exports = { handler };
