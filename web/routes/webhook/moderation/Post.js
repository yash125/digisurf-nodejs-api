'use strict'
var Joi = require("joi");
var logger = require('winston');
var Promise = require('promise');
var ObjectID = require('mongodb').ObjectID;
var local = require('../../../../locales');
var rabbitMq = require('../../../../library/rabbitMq');
const postES = require("../../../../models/postES");
const postsCollection = require("../../../../models/posts");

const handler = (req, res) => {

    let moderation = () => {
        return new Promise((resolve, reject) => {
            logger.silly("req.payload ", req.payload)
            if (req.payload && req.payload.post_id &&
                req.payload.content &&
                req.payload.content.moderate_type &&
                req.payload &&
                req.payload.text &&
                req.payload.text.text_obscenity_prediction) {
                postsCollection.Update({ _id: ObjectID(req.payload.post_id) }, { "postStatus": 1, "moderateData": req.payload }, (err, result) => {
                    if (err) console.log("err SDCDWEWSE : ", err)
                });
                postES.Update(req.payload.post_id, {
                    "postStatus": 1
                }, (err, result) => {
                    if (err) console.log("err SDCDWEAZSWSE : ", err)
                });
                
            } else {
                postsCollection.Update({ _id: ObjectID(req.payload.post_id) }, {
                    "postStatus": 7, "moderateData": req.payload,
                    "rejectionReason": (req.payload && req.payload && req.payload.rejection_reason) ? req.payload.rejection_reason : "unsafe",
                    "rejectionTs": new Date().getTime()
                }, (err, result) => {
                    if (err) console.log("err AZSXDWSD : ", err)
                });
                postES.Update(req.payload.post_id, {
                    "postStatus": 7
                }, (err, result) => {
                    if (err) console.log("err SDCDWEAZSWSE : ", err)
                })
            }

            return resolve({});
        });
    }

    moderation()
        .then((data) => {
            return res({ message: req.i18n.__('genericErrMsg')['200'] }).code(200)
        })
        .catch((data) => {
            console.log("error - data ", data)
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        })
}


module.exports = { handler };
