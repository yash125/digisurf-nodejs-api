let PostAPI = require('./Post');

module.exports = [
    {
        method: 'POST',
        path: '/webhook/moderation',
        handler: PostAPI.handler,
        config: {
            description: `This API used for get webhook from moderation`,
            tags: ['api', 'moderation'],
            auth: false
        }
    }
];