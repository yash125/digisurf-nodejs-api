let headerValidator = require("../../middleware/validator")
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/searchByStarChatId',
        handler: GetAPI.handler,
        config: {
            description: `This API use for search people by starchatId or by phone number`,
            tags: ['api', 'search'],
            auth: "user",
            validate: {
                query: GetAPI.validator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];