const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const userListCollection = require("../../../models/userList")
const followCollection = require("../../../models/follow")


/**
 * @description API to subscribe to a channel
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token
 * @property {mongodb object} channelId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const validator = Joi.object({
    starChatId: Joi.string().allow("").default("").description("starChatId/phoneNumber to be searched"),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit")
}).required();

const handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    let starChatId = (req.query.starChatId) ? req.query.starChatId.trim().toLowerCase() : "";
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;
    var followeeIdObj = {};

    const getMyFollowee = () => {
        return new Promise((resolve, reject) => {
            followCollection.Select({ follower: userId }, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    for (let index = 0; index < result.length; index++) {
                        followeeIdObj[result[index].followee.toString()] = result[index]["type"];
                    }
                    return resolve(true);
                }
            })
        });
    };
    const getUserList = () => {
        return new Promise((resolve, reject) => {

            let condition = [
                {
                    "$project": {
                        "_id": 1, "firstName": 1, "registeredOn": 1, "private": 1, "businessProfile": 1,
                        "userName": 1, "profilePic": 1, "userStatus": 1, "blocked": 1, "starChatId": 1, "number": 1,
                        "lastName": { '$ifNull': ['$lastName', " "] },
                        "fullName": { "$concat": ["$firstName", " ", "$lastName"] },
                        "legacyFullName": { "$concat": ["$firstName", "$lastName"] }

                    }
                },
                {
                    "$match": {
                        "userName": { "$exists": true },
                        "$or": [
                            { "starChatId": { "$regex": starChatId, "$options": "i" } },
                            { "number": { "$regex": starChatId, "$options": "i" } }
                        ],
                        "userStatus": 1,
                        //"blocked": { "$ne": userId }
                    }
                },
                {
                    "$project": {
                        "_id": 1, "firstName": 1, "registeredOn": 1, "private": 1, "businessProfile": 1,
                        "userName": 1, "profilePic": 1,
                        "lastName": { '$ifNull': ['$lastName', " "] },

                    }
                },
                { "$skip": offset },
                { "$limit": limit }
            ];
            userListCollection.Aggregate(condition, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {
                    for (let index = 0; index < d.length; index++) {
                        d[index]["lastName"] = d[index]["lastName"] || "";
                        d[index]["followStatus"] = (followeeIdObj[d[index]._id.toString()]) ? followeeIdObj[d[index]._id.toString()]["status"] : 0
                        d[index]["followStatus"] = (d[index]["followStatus"] == 3) ? 0 : d[index]["followStatus"];
                        d[index]["private"] = d[index]["private"] || 0;
                    }
                    return resolve({ code: 200, message: req.i18n.__('GetSearchByStarChatId')['200'], data: d });
                }
            });

        })
    };

    getMyFollowee()
        .then(getUserList)
        .then((data) => {
            return res({ message: data.message, data: data.data }).code(data.code);
        }).catch((error) => {
            return res({ message: error.message }).code(error.code);
        });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetSearchByStarChatId']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { validator, response, handler };