'use strict'
//require npm modules
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

//require dependency
const local = require('../../../../locales');
const userListCollection = require("../../../../models/userList");

//validator for this API
const validator = Joi.object({
    userName: Joi.string().description("userName")
}).unknown();


/**
 * @method GET isExists/userName
 * @description This API returns user's activity
 * 
 * @headers {string} authorization : token
 * @headers {string} lang by defualt en
 * 
 * @param {string} userName
 * 
 * @author DipenAhir
 * @since 14-July-2020
 */

//start API hender
const hander = (req, res) => {

    userListCollection.SelectOne({ userName: req.query.userName, "userStatus": 1 }, (err, result) => {
        if (err) {
            //return 500 responce : if have any issues in  function
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result && result._id) {
            //return 200 responce : if data found in db
            return res({
                message: req.i18n.__('genericErrMsg')['200']
            }).code(200);
        } else {
            //return 204 responce : if data not found in db
            return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);
        }
    })


};//end API hender

//start API response
const response = {
    status: {
        200: { message: Joi.string().default(local['genericErrMsg']['200']).required().example("Data Found successfully") },
        204: { message: Joi.string().default(local['genericErrMsg']['204']).default("No Content - data not available") },
        400: { message: Joi.string().default(local['genericErrMsg']['400']).required().example("<field> is missing") }
    }
}//end API response

//Export all constance
module.exports = { validator, hander, response };