let headerValidator = require("../../../middleware/validator")
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/isExists/emailId',
        handler: GetAPI.hander,
        config: {
            description: `This API is use for check this emailId isExists or not`,
            tags: ['api', 'isExists'],
            auth: {
                strategies: ['basic', 'guest']
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];