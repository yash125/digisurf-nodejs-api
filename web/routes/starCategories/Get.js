'use strict'
const Joi = require("joi");
const Promise = require('promise');

const local = require('../../../locales');
const starUserCategories = require("../../../models/starUserCategories");

/**
 * @description API to un-like a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} userId
 * @property {string} postId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */



const handler = (req, res) => {
    let lang = req.headers.lang

    const checkIfLiked = () => {
        return new Promise((resolve, reject) => {

            starUserCategories.SelectWithSort({ "statusCode": 1 }, { _id: 1 }, {}, 0, 10000, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    let dataToSend = [];
                    result.forEach(element => {
                        dataToSend.push({
                            _id : element._id,
                            categorie : (element.categorieInOtherLang[lang])?element.categorieInOtherLang[lang]:element.categorie,
                            icon:element.icon
                        })
                    });
                    return resolve({ code: 200, message: req.i18n.__('GetStarCategories')['200'], data: dataToSend });
                } else {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    };


    checkIfLiked()
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((data) => { return res({ message: data.message }).code(data.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetStarCategories']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { handler, response };