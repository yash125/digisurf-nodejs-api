const Joi = require("joi");
const local = require('../../../locales');
const postCollection = require("../../../models/posts");


const validator = Joi.object({
    hashtag: Joi.string().required().description("hashtag to be searched"),
    skip: Joi.string().description('skip'),
    limit: Joi.string().description('limit'),
}).required();

const handler = (req, res) => {

    let hashTag = req.query.hashtag.trim().toLowerCase();
    hashTag = (hashTag[0] == "#") ? hashTag : "#" + hashTag;

    let skip = parseInt(req.query.skip) || 0;
    let limit = parseInt(req.query.limit) || 20;
    let condition = [
        { "$match": { "hashTags": { "$exists": true } } },
        { "$match": { "hashTags": { "$regex": hashTag, "$options": "i" }, "postStatus": 1 } },
        { "$unwind": "$hashTags" },
        { "$match": { "hashTags": { "$regex": hashTag, "$options": "i" } } },
        { "$lookup": { "from": "customer", "localField": "userId", "foreignField": "_id", "as": "data" } },
        { "$unwind": "$data" },
        { "$match": { $or: [{ "data.private": 0 }, { "data.private": false }] } },
        { "$project": { 'tags': '$hashTags' } },
        { "$group": { "_id": '$tags', "count": { "$sum": 1 } } }
    ];

    postCollection.Aggregate(condition, (e, d) => {
        if (e) {
            let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
            return res(responseobj).code(500);
        } else if (d.length === 0) {
            let responseobj = { message: req.i18n.__('genericErrMsg')['204'] };
            return res(responseobj).code(204);
        } else {
            let responseobj = { message: req.i18n.__('GetHashTagList')['200'], data: d };
            return res(responseobj).code(200);
        }
    });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetHashTags']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { validator, handler, response };