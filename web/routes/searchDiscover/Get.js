
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../locales');
const postsCollection = require('../../../models/posts');

/**
 * @description API to subscribe to a channel
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token
 * @property {mongodb object} channelId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const validator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit")
}).required();

const handler = (req, res) => {
    const userId = ObjectID(req.auth.credentials._id);
    const offset = parseInt(req.query.offset) || 0;
    const limit = parseInt(req.query.limit) || 20;

    const discoverPosts = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                {
                    $match: { userId: { $ne: userId }, channelId: { $ne: null } }
                },
                {
                    $lookup: {
                        from: 'channel',
                        localField: 'channelId',
                        foreignField: '_id',
                        as: 'channelData'
                    }
                },
                {
                    $unwind: '$channelData'
                },
                {
                    $lookup: {
                        from: 'customer',
                        localField: 'userId',
                        foreignField: '_id',
                        as: 'userData'
                    }
                },
                {
                    $unwind: '$userData'
                },
                {
                    $project: {
                        _id: 1, imageUrl1: 1, thumbnailUrl1: 1,
                        mediaType1: 1, userId: 1, createdOn: 1, timeStamp: 1,
                        title: 1, channelId: 1, hashTags: 1,
                        channelName: '$channelData.channelName',
                        channelPrivate: '$channelData.private',
                        categoryId: '$channelData.categoryId',
                        channelImageUrl: '$channelData.channelImageUrl',
                        channelDescription: '$channelData.description',
                        postedByUserId: '$userData._id', postedByUserName: '$userData.userName',
                        postedByUserProfilePic: '$userData.profilePic'
                    }
                },
                {
                    $sort: { timeStamp: -1, channelName: 1 }
                },
                {
                    $skip: offset
                },
                {
                    $limit: limit
                }
            ];
            postsCollection.Aggregate(condition, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {
                    return resolve({ code: 200, message: req.i18n.__('GetSearchDiscover')['200'], data: d });
                }
            });
        });
    };

    discoverPosts()
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((data) => { return res({ message: data.message }).code(data.code); })
};


const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetSearchDiscover']['200']), data: Joi.any().example([
                {
                    "_id": "5ad8b112608de63ee376bd63",
                    "imageUrl1": "http://res.cloudinary.com/howdoo/image/upload/v1524150545/sg1bsm2xmum3szftww4j.jpg",
                    "thumbnailUrl1": "http://res.cloudinary.com/howdoo/image/upload/v1524150545/sg1bsm2xmum3szftww4j.jpg",
                    "mediaType1": 0,
                    "userId": "5acf951997d5d51273717dab",
                    "createdOn": "2018-04-19T15:09:06.296Z",
                    "timeStamp": 1524150546296,
                    "title": "Zeroin Event - small but Jamed with good people and exciting tech.....o and Supaman gave us a shout out in his live stream.  ",
                    "channelId": "5acf96a0bf669aba8f6f62b6",
                    "channelName": "howdoo",
                    "channelPrivate": false,
                    "categoryId": "5acdbfccbf669aba8f665900",
                    "channelImageUrl": "http://res.cloudinary.com/howdoo/image/upload/v1523553952/zwxmavqrfsa8e72pzxys.jpg",
                    "postedByUserId": "5acf951997d5d51273717dab",
                    "postedByUserName": "brierley",
                    "postedByUserProfilePic": "http://res.cloudinary.com/howdoo/image/upload/v1523554005/wqpjv6qttgmozvmrftnk.jpg"
                },
                {
                    "_id": "5ad8b047608de63ee376bd61",
                    "imageUrl1": "http://res.cloudinary.com/howdoo/image/upload/v1524150342/hiqdzbmj5crgg5a3gczc.jpg",
                    "thumbnailUrl1": "http://res.cloudinary.com/howdoo/image/upload/v1524150342/hiqdzbmj5crgg5a3gczc.jpg",
                    "mediaType1": 0,
                    "userId": "5acf951997d5d51273717dab",
                    "createdOn": "2018-04-19T15:05:43.182Z",
                    "timeStamp": 1524150343182,
                    "title": "filipe CIO at utrust.io. great guy and tech. ",
                    "channelId": "5acf96a0bf669aba8f6f62b6",
                    "channelName": "howdoo",
                    "channelPrivate": false,
                    "categoryId": "5acdbfccbf669aba8f665900",
                    "channelImageUrl": "http://res.cloudinary.com/howdoo/image/upload/v1523553952/zwxmavqrfsa8e72pzxys.jpg",
                    "postedByUserId": "5acf951997d5d51273717dab",
                    "postedByUserName": "brierley",
                    "postedByUserProfilePic": "http://res.cloudinary.com/howdoo/image/upload/v1523554005/wqpjv6qttgmozvmrftnk.jpg"
                },
            ])
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
};//swagger response code

module.exports = {
    validator,
    response,
    handler
};

