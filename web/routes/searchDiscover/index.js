let headerValidator = require("../../middleware/validator")
let GetAPI = require('./Get');
// let PostAPI = require('./Post');

module.exports = [
    {
        method: 'GET',
        path: '/search/discover',
        handler: GetAPI.handler,
        config: {
            description: `This API use for signup or login from facebook with facebook tokenXXX`,
            tags: ['api', 'search'],
            auth: "user",
            validate: {
                query: GetAPI.validator,
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];