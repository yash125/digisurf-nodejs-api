const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const followCollection = require("../../../models/follow");


let handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;
    let totalRequest = 0;

    getTotalRequestCount()
        .then(getMatchdata)
        .then(function (value) {
            return res({ message: req.i18n.__('GetFolloeRequest')['200'], totalRequest: totalRequest, data: value }).code(200);
        })
        .catch(function (err) {
            logger.error(err)
            return res({ message: local['genericErrMsg']['204'] }).code(204);
        })


    function getMatchdata() {
        return new Promise(function (resolve, reject) {
            let condition = [
                { "$match": { "followee": userId, "type.status": 2 } },
                { "$lookup": { "from": "customer", "localField": "follower", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                { "$sort": { _id: -1 } },
                {
                    "$project": {
                        "_id": "$userData._id", "countryCode": "$userData.countryCode", "number": "$userData.mobile",
                        "userName": "$userData.userName", "profilePic": "$userData.profilePic",
                        "firstName": "$userData.firstName", "lastName": "$userData.lastName"
                    }
                },

                { "$skip": offset }, { "$limit": limit }
            ];

            followCollection.Aggregate(condition, (err, result) => {
                if (result.length) {
                    return resolve(result);
                } else {
                    return reject(result);
                }
            })
        });
    }
    function getTotalRequestCount() {
        return new Promise(function (resolve, reject) {
            let condition = { "followee": userId, "type.status": 2 };
            followCollection.Select(condition, (err, result) => {
                if (err) {
                    return reject(err);
                } else {
                    totalRequest = (result && result.length) ? result.length : 0;
                    return resolve(result);
                }
            })
        });
    }
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetFolloeRequest']['200']), totalRequest: Joi.any(), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
    }
};//swagger response code

module.exports = { handler, response }