let headerValidator = require("../../middleware/validator")
let GetAPI = require('./Get');
let PostAPI = require('./Post');

module.exports = [
    {
        method: 'GET',
        path: '/followRequest',
        handler: GetAPI.handler,
        config: {
            description: `This API use for signup or login from facebook with facebook tokenXXX`,
            tags: ['api', 'follow'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
    {
        method: 'Post',
        path: '/followRequest',
        handler: PostAPI.handler,
        config: {
            description: `This API use for signup or login from facebook with facebook tokenXXX`,
            tags: ['api', 'follow'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    }
];