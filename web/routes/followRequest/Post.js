const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const fcm = require("../../../library/fcm");
const rabbitMq = require('../../../library/rabbitMq');
const followCollection = require("../../../models/follow");
const userCollection = require("../../../models/userList");
const notificationCollection = require("../../../models/notification");
// var sendPushUtility = require("../../../library/iosPush/sendPush.js");

const validator = Joi.object({
    targetId: Joi.string().required().min(24).max(24).description("targetId is missing or incorrect"),
    status: Joi.number().required().allow(0, [1]).description("status : 1:accept, 0:deny"),
}).required();


let handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    let targetId = ObjectID(req.payload.targetId);
    let status = (req.payload.status) ? 1 : 3;
    let message = (status == 1) ? " started following" : " reject follow";
    let end = (status == 1) ? false : true;

    // if (status == 1) {
    //     rabbitMq.sendToQueue(rabbitMq.follow_mqtt_queue, { "userId": targetId.toString(), "userName": "user", "userLang": req.headers.lang, "starUserId": userId.toString() }, () => { })
    // }


    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }
    function getMatchdata() {
        return new Promise(function (resolve, reject) {
            let condition = { followee: userId, "type.status": 2, follower: targetId };
            let dataToUpdate = { type: { status: status, message: message }, end: end };

            followCollection.Update(condition, dataToUpdate, (e) => {
                if (e) {
                    logger.error("AOKAOAKA : ", e);
                    reject(e);
                }
            });

            if (status == 1) {
                userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                    if (err) {
                        return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (result) {
                        if (userData && userData.userName) {
                            let request = {
                                fcmTopic: targetId,
                                // action: 1,
                                pushType: 2,
                                title: process.env.APP_NAME,
                                msg: userData.userName + "  has accepted your follow request.",
                                data: { "type": "followed", "userId": userId },
                                deviceType: (result && result.deviceInfo && result.deviceInfo.deviceType) ? result.deviceInfo.deviceType : "2"
                            };
                            fcm.notifyFcmTpic(request, () => { })
                        }
                    }
                })
                
                userCollection.UpdateByIdWithAddToSet({ _id: targetId.toString() }, { "follow": userId }, (e) => { if (e) logger.error("AOKAOKA : ", e); });
                userCollection.UpdateByIdWithAddToSet({ _id: userId.toString() }, { "follow": targetId }, (e) => {
                    if (e) logger.error("AOKAOAKA : ", e)

                    // d = JSON.parse(d);
                    // if (d.nModified == 0) {
                    // rabbitMq.sendToQueue(rabbitMq.follow_mqtt_queue, {
                    //     "userId": targetId.toString(),
                    //     "userName": userData.userName || "user", "userLang": req.headers.lang,
                    //     "starUserId": req.auth.credentials._id.toString()
                    // }, (e) => {
                    //     if (e) {
                    //         logger.error("OSKDCO : ", e);
                    //     }
                    // });
                    // }
                });

            }
            return resolve("");
        });
    }
    const createNotification = () => {
        return new Promise((resolve, reject) => {

            followCollection.GetFollowerCount(userId, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    let followerCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                    userCollection.UpdateById(userId.toString(), { "count.followerCount": followerCount }, () => { })
                }
            });
            followCollection.GetFolloweeCount(userId, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    let followerCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                    userCollection.UpdateById(userId.toString(), { "count.followerCount": followerCount }, () => { })
                }
            });
            followCollection.GetFollowerCount(targetId, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    let followerCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                    userCollection.UpdateById(targetId.toString(), { "count.followerCount": followerCount }, () => { })
                }
            });
            followCollection.GetFolloweeCount(targetId, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    let followerCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                    userCollection.UpdateById(targetId.toString(), { "count.followerCount": followerCount }, () => { })
                }
            });




            let dataToInsert = {
                to: userId,
                from: targetId,
                toCollection: 'customer',
                fromCollection: 'customer',
                start: new Date().getTime(),
                end: false,
                type: { "status": 3, "message": 'started following ' }
            };
            let condition = {
                to: userId,
                from: targetId,
                end: true,
                "$or": [{ "type.status": 3 }, { "type.status": 4 }, { "type.status": 6 }]
            };
            notificationCollection.UpdateWithUpsert(condition, dataToInsert, (e) => {
                if (e) {
                    let responseObj = { message: local['genericErrMsg']['500'] };
                    return reject(responseObj);
                } else {
                    return resolve(true);
                }
            });
        });
    };

    getUserData()
        .then(() => { return getMatchdata(); })
        .then(() => { return createNotification(); })
        .then(() => {
            return res({ message: req.i18n.__('PostFolloeRequest')['200'] }).code(200);
        }).catch(function (err) {
            logger.error(err)
            return res({ message: local['genericErrMsg']['500'] }).code(500);
        })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['PostFolloeRequest']['200']) },

    }
};//swagger response code

module.exports = { handler, response, validator }