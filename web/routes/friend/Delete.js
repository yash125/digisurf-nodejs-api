const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const mqtt = require("../../../library/mqtt");
const userListES = require("../../../models/userListES");
const userCollection = require("../../../models/userList");
const followCollection = require("../../../models/follow");
const friendDetailsCollection = require("../../../models/friendDetails");

const validator = Joi.object({
    targetUserId: Joi.string().required().description("targetUserId, mongo id of the targetUserId, it must be 24 characters")
})

const handler = (req, res) => {

    const userId = ObjectID(req.auth.credentials._id);
    const targetUserId = ObjectID(req.query.targetUserId);
    const HTTP500ERROR = { message: req.i18n.__('genericErrMsg')['500'], code: 500 }

    let removeFriend = {
        SentBy: userId,
        SentTo: targetUserId,
        removedOn: new Date().getTime(),
        type: "remove Friend"
    }
    mqtt.publish(userId.toString(), JSON.stringify(removeFriend), { qos: 1 }, (e) => { if (e) logger.error(e) });
    mqtt.publish(targetUserId.toString(), JSON.stringify(removeFriend), { qos: 1 }, (e) => { if (e) logger.error(e) });

    userListES.UpdateWithPull(userId.toString(), "friendList", targetUserId.toString(), (e) => { if (e) logger.error(e) })
    userListES.UpdateWithPull(targetUserId.toString(), "friendList", userId.toString(), (e) => { if (e) logger.error(e) })

    const updateInUserListCollection = () => {
        return new Promise((resolve, reject) => {
            let condition = { _id: userId };
            let dataToUpdate = { friendList: targetUserId };

            userCollection.UpdateByIdWithPull(condition, dataToUpdate, (err) => {
                if (err) {
                    logger.error("OKPLQQPADSD : ", err);
                    return reject(HTTP500ERROR);
                }
            });

            let conditionTargetUser = { _id: targetUserId };
            let dataToUpdateTargetUser = { friendList: userId };

            userCollection.UpdateByIdWithPull(conditionTargetUser, dataToUpdateTargetUser, (err) => {
                if (err) {
                    logger.error("OKPERLPADSD : ", err);
                    return reject(HTTP500ERROR);
                }
            });
            return resolve(true);
        });
    };
    const updateInFriendDetailsCollection = () => {
        return new Promise((resolve, reject) => {
            let condition = {
                "$or": [
                    { "friendRequestSentBy": userId, "friendRequestSentTo": targetUserId },
                    { "friendRequestSentBy": targetUserId, "friendRequestSentTo": userId },
                ],
                "friendRequestStatusCode": 2
            };
            let dataToUpdate = {
                "friendUnfriendedOnTimestamp": new Date().getTime(),
                "friendUnfriendedBy": userId
            };

            friendDetailsCollection.Update(condition, dataToUpdate, (err) => {
                if (err) {
                    logger.error("OKPLZXPADSD : ", err);
                    return reject(HTTP500ERROR);
                } else {
                    return resolve(true)
                }
            });
        });
    };
    const updateInFollowCollection = () => {
        return new Promise((resolve, reject) => {
            let condition = {
                "$or": [
                    { "followee": userId, "follower": targetUserId },
                    { "followee": targetUserId, "follower": userId },
                ]
            };
            let dataToUpdate = {
                end: true,
                "type": {
                    status: 3,
                    "message": "rejected"
                }
            };

            followCollection.Update(condition, dataToUpdate, (err) => {
                if (err) {
                    logger.error("OKPLZXPADSD : ", err);
                    return reject(HTTP500ERROR);
                } else {
                    return resolve({ message: req.i18n.__('DeleteFriend')['200'] })
                }
            });
        });
    };

    updateInUserListCollection()
        .then(() => { return updateInFriendDetailsCollection(); })
        .then(() => { return updateInFollowCollection(); })
        .then(() => { return res({ message: req.i18n.__('DeleteFriend')['200'] }).code(200); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['DeleteFriend']['200']) },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { handler, response, validator };