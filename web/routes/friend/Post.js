const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const fcm = require("../../../library/fcm");
const mqtt = require("../../../library/mqtt");
const userListES = require("../../../models/userListES");
const userCollection = require("../../../models/userList");
const followCollection = require("../../../models/follow");
const friendDetailsCollection = require("../../../models/friendDetails");
const notificationCollection = require("../../../models/notification");


const validator = Joi.object({
    targetUserId: Joi.string().required().description("targetUserId, mongo id of the targetUserId"),
    message: Joi.string().allow("").description("message")
})

const handler = (req, res) => {

    const userId = ObjectID(req.auth.credentials._id);
    const targetUserId = ObjectID(req.payload.targetUserId);
    var deviceType = "2"

    const HTTP500ERROR = { message: req.i18n.__('genericErrMsg')['500'], code: 500 }
    const HTTP409ERROR = { message: req.i18n.__('PostFriend')['409'], code: 409 }
    const HTTP405ERROR = { message: req.i18n.__('PostFriend')['405'], code: 405 }
    const HTTP412ERROR = { message: req.i18n.__('PostFriend')['412'], code: 412 }

    let isPrivate = false;
    var messageToShow = "Added as a friend."



    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    if (userId.toString() == targetUserId.toString()) return res({ message: HTTP405ERROR.message }).code(HTTP405ERROR.code);
                    if (userData && userData.friendRequestBy && userData.friendRequestBy.map(e => e.toString()).includes(targetUserId.toString())) return res({ message: HTTP412ERROR.message }).code(HTTP412ERROR.code);
                    if (userData && userData.friendList && userData.friendList.map(e => e.toString()).includes(targetUserId.toString())) return res({ message: HTTP409ERROR.message }).code(HTTP409ERROR.code);

                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    const checkTargetUserIsPrivate = () => {
        return new Promise((resolve, reject) => {

            userCollection.SelectOne({ _id: targetUserId }, (err, result) => {
                if (err) {
                    logger.error("OKFDODKSFO : ", err);
                    return reject(HTTP500ERROR);
                } else if (result._id) {
                    isPrivate = result.private || false;
                    deviceType = (result && result.deviceInfo && result.deviceInfo.deviceType) ? result.deviceInfo.deviceType : "2";
                    if (result && result.myFriendRequest && result.myFriendRequest.map(e => e.toString()).find(e => e == userId.toString())) {

                        return reject({ message: req.i18n.__('genericErrMsg')['405'], code: 405 })
                    } else if (result && result.friendList && result.friendList.map(e => e.toString()).find(e => e == userId.toString())) {

                        return reject({ message: req.i18n.__('genericErrMsg')['409'], code: 409 })
                    } else {

                        return resolve(true)
                    }
                } else {
                    logger.error("OSADKEOXOL : target user not found in db");
                    return reject({ message: req.i18n.__('genericErrMsg')['204'], code: 204 })
                }
            });
        });
    };
    const postFriend = () => {
        return new Promise((resolve, reject) => {
            let newFriendRequest = {
                friendRequestSentBy: userId,
                friendRequestSentTo: targetUserId,
                frtiendRequestSentOn: new Date().getTime(),
                timestampForMetabase: new Date(),
                friendRequestStatus: (isPrivate) ? "new Request" : "Accept",
                friendRequestStatusCode: (isPrivate) ? 1 : 2,
                messageForTargetUser: req.payload.message
            }


            friendDetailsCollection.Insert(newFriendRequest, (err) => {
                if (err) {
                    logger.error("SJDISAAS : ", err);
                    return reject(HTTP500ERROR);
                } else {
                    let request = {
                        fcmTopic: targetUserId.toString(),
                        // action: 1,
                        pushType: 2,
                        title: process.env.APP_NAME,
                        msg: "You just got a new friend request from " + userData.userName,
                        data: {
                            "type": "new Friend", message: req.payload.message,
                            userId: userData._id.toString(),
                            userName: userData.userName,
                            firstName: userData.firstName,
                            lastName: userData.lastName || "",
                            profilePic: userData.profilePic,
                            mobileNumber: userData.number,
                        },
                        deviceType: deviceType
                    }
                    if (!isPrivate) request["msg"] = userData.userName + " added you as friend."

                    if (isPrivate) request["data"]["type"] = "newFriendRequest"
                    if (isPrivate) messageToShow = "Friend request sent."

                    fcm.notifyFcmTpic(request, () => { })
                    newFriendRequest["type"] = "new Friend"
                    mqtt.publish(userId.toString(), JSON.stringify(newFriendRequest), { qos: 1 }, (e) => { if (e) logger.error(e) });
                    mqtt.publish(targetUserId.toString(), JSON.stringify(newFriendRequest), { qos: 1 }, (e) => { if (e) logger.error(e) });
                    return resolve(true);
                }
            })
        });
    };
    const postFollow = () => {
        return new Promise((resolve, reject) => {
            let condition = {
                follower: userId,
                followee: targetUserId
            };
            let newFollowRequest = {
                follower: userId,
                followee: targetUserId,
                start: new Date().getTime(),
                end: (isPrivate) ? true : false,
                type: {
                    status: (isPrivate) ? 2 : 1,
                    message: (isPrivate) ? "requested to follow you" : "started following you",
                }
            }
            followCollection.UpdateWithUpsert(condition, newFollowRequest, (err) => {
                if (err) {
                    logger.error("SODKOASD : ", err);
                    return reject(HTTP500ERROR);
                }
            });
            if (!isPrivate) {
                let condition2 = {
                    follower: targetUserId,
                    followee: userId
                };
                let newFollowRequest2 = {
                    follower: targetUserId,
                    followee: userId,
                    start: new Date().getTime(),
                    end: (isPrivate) ? true : false,
                    type: {
                        status: (isPrivate) ? 2 : 1,
                        message: (isPrivate) ? "requested to follow you" : "started following you",
                    }
                }
                followCollection.UpdateWithUpsert(condition2, newFollowRequest2, (err) => {
                    if (err) {
                        logger.error("SODKOASD : ", err);
                        return reject(HTTP500ERROR);
                    }
                });
                return resolve(true);
            } else {
                userCollection.UpdateByIdWithAddToSet(userId.toString(), { "follow": targetUserId }, (e, d) => {

                    d = JSON.parse(d);
                    if (d.nModified) {
                        userCollection.UpdateWithIncreaseById(userId.toString(), { "count.followeeCount": 1 }, () => { });
                        userCollection.UpdateWithIncreaseById(targetUserId.toString(), { "count.followerCount": 1 }, () => { });
                    }
                })

                userCollection.UpdateByIdWithAddToSet(targetUserId.toString(), { "follow": userId }, (e, d) => {

                    d = JSON.parse(d);
                    if (d.nModified) {
                        userCollection.UpdateWithIncreaseById(userId.toString(), { "count.followerCount": 1 }, () => { });
                        userCollection.UpdateWithIncreaseById(targetUserId.toString(), { "count.followeeCount": 1 }, () => { });
                    }
                    return resolve(true);
                })


            }
        });
    };
    const createNotification = () => {
        return new Promise((resolve, reject) => {

            if (isPrivate) return resolve(true)

            let dataToInsert_User = {
                to: targetUserId,
                from: userId,
                toCollection: 'customer',
                fromCollection: 'customer',
                start: new Date().getTime(),
                end: false,
                type: { "status": 3, "message": 'started following ' }
            };
            let condition_user = {
                to: targetUserId,
                from: userId,
                "$or": [{ "type.status": 3 }, { "type.status": 4 }, { "type.status": 6 }]
            };

            notificationCollection.UpdateWithUpsert(condition_user, dataToInsert_User, (e) => {
                if (e) {
                    let responseObj = { message: local['genericErrMsg']['500'] };
                    return reject(responseObj);
                }
            });
            let dataToInsert_targetUser = {
                to: userId,
                from: targetUserId,
                toCollection: 'customer',
                fromCollection: 'customer',
                start: new Date().getTime(),
                end: false,
                type: { "status": 3, "message": 'started following ' }
            };
            let condition_targetUser = {
                to: userId,
                from: targetUserId,
                "$or": [{ "type.status": 3 }, { "type.status": 4 }, { "type.status": 6 }]
            };

            notificationCollection.UpdateWithUpsert(condition_targetUser, dataToInsert_targetUser, (e) => {
                if (e) {
                    let responseObj = { message: local['genericErrMsg']['500'] };
                    return reject(responseObj);
                }
            });
            return resolve(true)
        });
    }
    const postInMyUserListCollection = () => {
        return new Promise((resolve, reject) => {
            let condition = { _id: userId.toString() };// on UpdateByIdWithPush function we converting _id to objectID so i apply toString() here.
            let newFriendRequest = (isPrivate) ? { "myFriendRequest": targetUserId } : { "friendList": targetUserId };
            if (!isPrivate) { userCollection.UpdateByIdWithAddToSet(condition, { "follow": targetUserId }, (e) => { if (e) logger.error(e) }); }

            if (!isPrivate) {
                userListES.UpdateWithPush(userId.toString(), "friendList", targetUserId.toString(), (e) => { if (e) logger.error(e) })
            }
            userCollection.UpdateByIdWithAddToSet(condition, newFriendRequest, (err, result) => {
                if (err) {
                    logger.error("SOKDOKWA : ", err);
                    return reject(HTTP500ERROR);
                } else {
                    result = JSON.parse(result);
                    if (!result.nModified) { return reject(HTTP409ERROR); }

                    return resolve(true);
                }
            })
        });
    };
    const postInTargetUserListCollection = () => {
        return new Promise((resolve, reject) => {
            let condition = { _id: targetUserId.toString() };// on UpdateByIdWithPush function we converting _id to objectID so i apply toString() here.
            let newFriendRequest = (isPrivate) ? { "friendRequestBy": userId } : { "friendList": userId };
            if (!isPrivate) { userCollection.UpdateByIdWithAddToSet(condition, { "follow": userId }, (e) => { if (e) logger.error(e) }); }

            if (!isPrivate) {
                userListES.UpdateWithPush(targetUserId.toString(), "friendList", userId.toString(), (e) => { if (e) logger.error(e) })
            }
            userCollection.UpdateByIdWithAddToSet(condition, newFriendRequest, (err) => {
                if (err) {
                    logger.error("OKOSKDSOOA : ", err);
                    return reject(HTTP500ERROR);
                } else {
                    return resolve(true);
                }
            })
        });
    };

    getUserData()
        .then(() => { return checkTargetUserIsPrivate() })
        .then(() => { return postInMyUserListCollection() })
        .then(() => { return postFriend() })
        .then(() => { return postFollow() })
        .then(() => { return createNotification() })
        .then(() => { return postInTargetUserListCollection() })
        .then(() => { return res({ message: req.i18n.__('PostFriend')['200'], "messageToShow": messageToShow }).code(200); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })


};

const response = {
    status: {
        200: { message: Joi.any().default(local['PostFriend']['200']), messageToShow: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        409: { message: Joi.any().default(local['PostFriend']['409']) },
        405: { message: Joi.any().default(local['genericErrMsg']['405']) },
        412: { message: Joi.any().default(local['PostFriend']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { validator, handler, response };