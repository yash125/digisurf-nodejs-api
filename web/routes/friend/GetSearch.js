const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userCollection = require("../../../models/userList");

const queryValidator = Joi.object({
    searchText: Joi.string().required().description("searchText"),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit")
})

const handler = (req, res) => {

    const userId = ObjectID(req.auth.credentials._id);
    const searchText = req.query.searchText;
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;
    const HTTP500ERROR = { message: req.i18n.__('genericErrMsg')['500'], code: 500 }
    var HTTP200ERROR = { message: req.i18n.__('GetSearchFriend')['200'], code: 200, data: [] };
    const HTTP204ERROR = { message: req.i18n.__('genericErrMsg')['204'], code: 204 };


    const getFriendList = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "_id": userId } },
                { "$unwind": "$friendList" },
                { "$lookup": { "from": "customer", "localField": "friendList", "foreignField": "_id", "as": "friendData" } },
                { "$unwind": "$friendData" },
                {
                    "$project": {
                        "_id": "$friendData._id", "countryCode": "$friendData.countryCode", "starChatId": "$friendData.starChatId",
                        "number": "$friendData.mobile", "userName": "$friendData.userName",
                        "profilePic": "$friendData.profilePic", "firstName": "$friendData.firstName",
                        "lastName": "$friendData.lastName", "status": "$friendData.status",
                        "fullName": { "$concat": ["$friendData.firstName", "$friendData.lastName"] },
                        "fullNameWithSpace": { "$concat": ["$friendData.firstName", " ", "$friendData.lastName"] },
                        "timestamp": "$friendData.frtiendRequestSentOn", "starRequest": "$friendData.starRequest",
                        "verified": "$friendData.verified"
                    }
                },
                {
                    "$match": {
                        "$or": [
                        { "userName": { "$regex": searchText, "$options": "i" } },
                        // { "firstName": { "$regex": searchText, "$options": "i" } },
                        // { "lastName": { "$regex": searchText, "$options": "i" } },
                        // { "fullName": { "$regex": searchText, "$options": "i" } },
                        // { "fullNameWithSpace": { "$regex": searchText, "$options": "i" } },
                        // { "starChatId": { "$regex": searchText, "$options": "i" } },
                        { "number": { "$regex": searchText, "$options": "i" } }
                        ]
                    }
                },
                {
                    "$project": {
                        "_id": 1, "countryCode": 1, "starChatId": 1, "number": 1, "userName": 1, "profilePic": 1,
                        "firstName": 1, "lastName": 1, "status": 1, "timestamp": 1, "starRequest": 1, "verified": 1
                    }
                },
                { "$skip": offset }, { "$limit": limit }
            ]
            userCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    logger.error("OKOKSASASD : ", err);
                    return reject(HTTP500ERROR);
                } else if (result && result.length) {
                    result.forEach(element => {
                        if (element["firstName"] == null) element["firstName"] = element["userName"];
                        if (element["lastName"] == null) element["lastName"] = "";
                        try {
                            element["isStar"] = (element["starRequest"] && element["starRequest"]["starUserProfileStatusCode"] == 4)?true:false;
                            element["timestamp"] = (element["timestamp"]) ? element["timestamp"] : new Date().getTime();
                        } catch (error) {
                            element["isStar"]=false
                            console.log("error : ", error)
                        }

                    });
                    HTTP200ERROR["data"] = result;
                    return resolve(HTTP200ERROR)
                } else {
                    return reject(HTTP204ERROR)
                }
            });
        });
    };

    getFriendList()
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetSearchFriend']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { handler, response, queryValidator };