const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const mqtt = require('../../../library/mqtt');
const local = require('../../../locales');
const userCollection = require("../../../models/userList");
const userListES = require("../../../models/userListES");
const followCollection = require("../../../models/follow");


const handler = (req, res) => {

    const userId = ObjectID(req.auth.credentials._id);
    const HTTP500ERROR = { message: req.i18n.__('genericErrMsg')['500'], code: 500 }
    let followStatus = {};

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(userId) };
            followCollection.Select(condition, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };

                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        followStatus[element.followee] = element.type
                    }, this);

                    resolve();
                }
            });
        });
    }
    function getFriendList() {
        return new Promise((resolve, reject) => {

            let condition = {
                "should": userData.friendList
                    .map(e => e.toString())
                    .map(e => {
                        return { "match": { "_id": e } }
                    })
            };
            userListES.Select(condition, 1, 1000, req.follow, userId.toString(), (err, result) => {
                if (err) {
                    logger.error("OKOKSADSD : ", err);
                    return reject(HTTP500ERROR);
                } else if (result && result.length) {
                    let dataToSend = [];
                    result.forEach(element => {

                        element["profilePic"] = (element["profilePic"] && element["profilePic"].includes("http")) ? element["profilePic"] : "https://res.cloudinary.com/dafszph29/image/upload/c_thumb,w_200,g_face/v1551361911/default/profile_pic.png";
                        element["profileVideoThumbnail"] = (element["profileVideoThumbnail"] && element["profileVideoThumbnail"].includes("http")) ? element["profileVideoThumbnail"] : "";
                        element["profileVideo"] = (element["profileVideo"] && element["profileVideo"].includes("http")) ? element["profileVideo"] : "";
                        element["private"] = (element["private"]) ? 1 : 0;

                        if (element["firstName"] == null) element["firstName"] = element["userName"];
                        if (element["lastName"] == null) element["lastName"] = "";
                        try {
                            element["isStar"] = (element["starRequest"] && element["starRequest"]["starUserProfileStatusCode"] == 4) ? true : false;
                        } catch (error) {
                            console.log(error);
                            element["isStar"] = false
                        }
                        element["timestamp"] = (element["timestamp"]) ? element["timestamp"] : new Date().getTime();
                        element["followStatus"] = (followStatus[element["_id"]]) ? followStatus[element["_id"]]["status"] : 0;
                        element["follow"] = (followStatus[element["_id"]]) ? followStatus[element["_id"]] : { "type": 0, "message": "not following" };
                        element["friendStatusCode"] = 2;
                        element["friendStatusText"] = "friends";
                        dataToSend.push({
                            "firstName": element["firstName"], "lastName": element["lastName"], "isStar": element["isStar"],
                            "timestamp": element["timestamp"], "followStatus": element["followStatus"], "follow": element["follow"],
                            "friendStatusCode": element["friendStatusCode"], "friendStatusText": element["friendStatusText"],
                            "_id": element["_id"], "countryCode": element["countryCode"], "number": element["number"],
                            "userName": element["userName"], "localNumber": element["localNumber"], "private": element["private"],
                            "profileVideo": element["profileVideo"], "profileVideoThumbnail": element["profileVideoThumbnail"],
                            "socialStatus": element["socialStatus"], "profilePic": element["profilePic"]
                        });
                    });

                    mqtt.publish("ContactSync/" + userId.toString(), JSON.stringify({ "contacts": dataToSend }), { qos: 1 }, (e) => { if (e) logger.error("ASASASASA :", e) });
                    return resolve({ message: req.i18n.__('GetFriend')['200'], data: dataToSend });
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'], code: 204 });
                }
            });
        });
    }

    getUserData()
        .then(() => { return getFolloweeIds() })
        .then(() => { return getFriendList() })
        .then((data) => { return res({ message: req.i18n.__('GetFriend')['200'], data: data.data }).code(200); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetFriend']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { handler, response };