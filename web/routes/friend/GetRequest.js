const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');

const friendDetailsCollection = require("../../../models/friendDetails");


const handler = (req, res) => {

    const userId = ObjectID(req.auth.credentials._id);
    let searchText = (req.query && req.query.searchText) ? req.query.searchText : "";
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;

    const HTTP500ERROR = { message: req.i18n.__('genericErrMsg')['500'], code: 500 };
    var HTTP200ERROR = { message: req.i18n.__('genericErrMsg')['200'], code: 200, data: { friendRequestBy: [], myFriendRequest: [] } };
    const HTTP204ERROR = { message: req.i18n.__('genericErrMsg')['204'], code: 204 };


    const getPendingFriendList = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "friendRequestSentBy": userId, "friendRequestStatusCode": 1 } },
                { "$lookup": { "from": "customer", "localField": "friendRequestSentTo", "foreignField": "_id", "as": "friendData" } },
                { "$unwind": "$friendData" },
                {
                    "$project": {
                        "_id": "$friendData._id", "countryCode": "$friendData.countryCode",
                        "number": "$friendData.mobile", "userName": "$friendData.userName",
                        "profilePic": "$friendData.profilePic", "firstName": "$friendData.firstName",
                        "lastName": "$friendData.lastName", "status": "$friendData.status", "message": "$messageForTargetUser"
                    }
                }
            ];
            if (searchText != "") {
                condition.push({
                    "$match": {
                        "$or": [{ "userName": { "$regex": searchText, "$options": "i" } },
                        { "firstName": { "$regex": searchText, "$options": "i" } },
                        { "lastName": { "$regex": searchText, "$options": "i" } },
                        { "number": { "$regex": searchText, "$options": "i" } }
                        ]
                    }
                })
            }
            condition.push({ "$skip": offset })
            condition.push({ "$limit": limit });

            friendDetailsCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    logger.error("OKOKSADSD : ", err);
                    return reject(HTTP500ERROR);
                } else if (result && result.length) {
                    HTTP200ERROR["data"]["friendRequestBy"] = result
                }
                return resolve(true)
            });
        });
    };

    const getWhoSendMeFriendRequestList = () => {
        return new Promise((resolve, reject) => {
            let condition = [

                { "$match": { "friendRequestSentTo": userId, "friendRequestStatusCode": 1 } },
                { "$lookup": { "from": "customer", "localField": "friendRequestSentBy", "foreignField": "_id", "as": "friendData" } },
                { "$unwind": "$friendData" },
                {
                    "$project": {
                        "_id": "$friendData._id", "countryCode": "$friendData.countryCode",
                        "number": "$friendData.mobile", "userName": "$friendData.userName",
                        "profilePic": "$friendData.profilePic", "firstName": "$friendData.firstName",
                        "lastName": "$friendData.lastName", "status": "$friendData.status",
                        "message": "$messageForTargetUser", "timestamp": "$friendData.frtiendRequestSentOn"
                    }
                }
            ];
            if (searchText != "") {
                condition.push({
                    "$match": {
                        "$or": [{ "userName": { "$regex": searchText, "$options": "i" } },
                        { "firstName": { "$regex": searchText, "$options": "i" } },
                        { "lastName": { "$regex": searchText, "$options": "i" } },
                        { "number": { "$regex": searchText, "$options": "i" } }
                        ]
                    }
                })
            }
            condition.push({ "$skip": offset })
            condition.push({ "$limit": limit });
            friendDetailsCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    logger.error("OKOKAASASDSD : ", err);
                    return reject(HTTP500ERROR);
                } else if (result && result.length) {
                    result.forEach(element => {


                        element["countryCode"] = (element["countryCode"]) ? element["countryCode"] : "";
                        element["number"] = (element["number"]) ? element["countryCode"] : "";
                        element["localNumber"] = (element["localNumber"]) ? element["localNumber"] : element["number"];
                        element["private"] = (element["private"]) ? 1 : 0;
                        element["profileVideo"] = (element["profileVideo"]) ? element["profileVideo"] : "";
                        element["socialStatus"] = (element["socialStatus"]) ? element["socialStatus"] : "";
                        element["profilePic"] = (element["profilePic"] && element["profilePic"].includes("http")) ? element["profilePic"] : "https://res.cloudinary.com/dafszph29/image/upload/c_thumb,w_200,g_face/v1551361911/default/profile_pic.png";
                        element["profileVideoThumbnail"] = (element["profileVideoThumbnail"] && element["profileVideoThumbnail"].includes("http")) ? element["profileVideoThumbnail"] : "";
                        element["profileVideo"] = (element["profileVideo"] && element["profileVideo"].includes("http")) ? element["profileVideo"] : "";
                        element["starRequest"] = (element["starRequest"]) ? element["starRequest"] : {};
                        element["timestamp"] = (element["timestamp"]) ? element["timestamp"] : new Date().getTime();
                        element["verified"] = (element["verified"]) ? element["verified"] : false;



                        if (element["firstName"] == null) element["firstName"] = element["userName"];
                        if (element["lastName"] == null) element["lastName"] = "";
                        element["timestamp"] = (element["timestamp"]) ? element["timestamp"] : new Date().getTime();
                    });
                    HTTP200ERROR["data"]["myFriendRequest"] = result
                }
                return ((HTTP200ERROR["data"]["myFriendRequest"] && HTTP200ERROR["data"]["myFriendRequest"].length) || (HTTP200ERROR["data"]["friendRequestBy"] && HTTP200ERROR["data"]["friendRequestBy"].length)) ? resolve(HTTP200ERROR) : reject(HTTP204ERROR);
            });
        });
    };

    getPendingFriendList()
        .then(() => { return getWhoSendMeFriendRequestList() })
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};


const queryValidator = Joi.object({
    searchText: Joi.string().description("searchText"),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit")
})
const response = {
    status: {
        200: { message: Joi.any().default(local['GetFriend']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { handler, response, queryValidator };