const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const fcm = require("../../../library/fcm");
const mqtt = require("../../../library/mqtt");
const followCollection = require("../../../models/follow");
const userCollection = require("../../../models/userList");
const userListES = require("../../../models/userListES");
const friendDetailsCollection = require("../../../models/friendDetails");
const notificationCollection = require("../../../models/notification");


const validator = Joi.object({
    targetUserId: Joi.string().required().description("targetUserId, mongo id of the targetUserId"),
    status: Joi.number().required().allow([1, 2]).description("status = 1/Accept, 2/Denied")
})

const handler = (req, res) => {

    const userId = ObjectID(req.auth.credentials._id);
    const targetUserId = ObjectID(req.payload.targetUserId);
    const HTTP500ERROR = { message: req.i18n.__('genericErrMsg')['500'], code: 500 }

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }


    if (req.payload.status == 1) {
        userListES.UpdateWithPush(userId.toString(), "friendList", targetUserId.toString(), (e) => { if (e) logger.error(e) })
        userListES.UpdateWithPush(targetUserId.toString(), "friendList", userId.toString(), (e) => { if (e) logger.error(e) });
        createNotification();
    }

    const checkTargetUserIsExists = () => {
        return new Promise((resolve, reject) => {

            userCollection.SelectOne({ _id: targetUserId }, (err, result) => {
                if (err) {
                    logger.error("OKSOKDSD : ", err);
                    return reject(HTTP500ERROR);
                } else if (result._id) {
                    return resolve(true)
                } else {
                    logger.error("ASDKOASDOK : target user not found in db");
                    return reject({ message: req.i18n.__('genericErrMsg')['204'], code: 204 })
                }
            });
        });
    };
    const updateFriendDetailsCollection = () => {
        return new Promise((resolve, reject) => {
            let condition = {
                "$or": [
                    { "friendRequestSentBy": userId, "friendRequestSentTo": targetUserId },
                    { "friendRequestSentBy": targetUserId, "friendRequestSentTo": userId },
                ],
                "friendRequestStatusCode": 1
            }
            // friendRequestStatus : new Request  / Accept / Denied / Unfriended / Deleted
            // friendRequestStatusCode : 1       /  2     /   3    /     4      /    5
            let friendRequest = {
                friendRequestStatus: (req.payload.status == 1) ? "Accept" : "Denied",
                friendRequestStatusCode: (req.payload.status == 1) ? 2 : 3,
                friendRequestResponseTimeStamp: new Date().getTime()
            }
            friendDetailsCollection.Update(condition, friendRequest, (err) => {
                if (err) {
                    logger.error("OSOKDOSWWA : ", err);
                    return reject(HTTP500ERROR);
                } else {
                    return resolve(true);
                }
            })
        });
    };
    const updateInFollowCollection = () => {
        return new Promise((resolve, reject) => {
            let condition = {
                follower: userId,
                followee: targetUserId
            };
            let followRequest = {
                follower: userId,
                followee: targetUserId,
                start: new Date().getTime(),
                end: (req.payload.status == 1) ? false : true,
                type: {
                    status: (req.payload.status == 1) ? 1 : 3,
                    message: (req.payload.status == 1) ? "started following you" : "rejected"
                }
            }
            followCollection.UpdateWithUpsert(condition, followRequest, (err) => {
                if (err) {
                    logger.error("OKSDO3SD : ", err);
                    return reject(HTTP500ERROR);
                }
            })

            let condition2 = {
                follower: targetUserId,
                followee: userId
            };
            let followRequest2 = {
                follower: targetUserId,
                followee: userId,
                start: new Date().getTime(),
                end: (req.payload.status == 1) ? false : true,
                type: {
                    status: (req.payload.status == 1) ? 1 : 3,
                    message: (req.payload.status == 1) ? "started following you" : "rejected"
                }
            }
            followCollection.UpdateWithUpsert(condition2, followRequest2, (err) => {
                if (err) {
                    logger.error("OKSDO3SD : ", err);
                    return reject(HTTP500ERROR);
                }
            });
            return resolve(true);
        });
    };
    const removeRequestFromMyUserListCollection = () => {
        return new Promise((resolve, reject) => {
            let condition = { _id: userId.toString() };// on UpdateByIdWithPush function we converting _id to objectID so i apply toString() here.
            let friendRequest = { "friendRequestBy": targetUserId };

            userCollection.UpdateByIdWithPull(condition, friendRequest, (err) => {
                if (err) {
                    logger.error("SOASAKDOKWA : ", err);
                    return reject(HTTP500ERROR);
                } else {
                    return resolve(true);
                }
            })
        });
    };
    const removeRequestFromTargetUserListCollection = () => {
        return new Promise((resolve, reject) => {
            let condition = { _id: targetUserId.toString() };// on UpdateByIdWithPush function we converting _id to objectID so i apply toString() here.
            var newFriendRequest = { "myFriendRequest": userId };

            userCollection.UpdateByIdWithPull(condition, newFriendRequest, (err) => {
                if (err) {
                    logger.error("OKOSKDSASASQWOOA : ", err);
                    return reject(HTTP500ERROR);
                } else {
                    return resolve(true);
                }
            })
        });
    };
    const PostFriendIfRequired = () => {
        return new Promise((resolve, reject) => {
            switch (req.payload.status) {
                case 1: {
                    let condition = { _id: targetUserId.toString() }; // on UpdateByIdWithPush function we converting _id to objectID so i apply toString() here.
                    let newFriendRequest = { "friendList": userId };
                    userCollection.UpdateByIdWithAddToSet(condition, newFriendRequest, (err) => {
                        if (err) {
                            logger.error("OKOSKQWWQA : ", err);
                            return reject(HTTP500ERROR);
                        }
                    })
                    userCollection.UpdateByIdWithAddToSet(condition, { "follow": userId }, (e, d) => {

                        d = JSON.parse(d);
                        if (d.nModified) {
                            userCollection.UpdateWithIncreaseById(userId.toString(), { "count.followeeCount": 1 }, () => { });
                            userCollection.UpdateWithIncreaseById(targetUserId.toString(), { "count.followerCount": 1 }, () => { });
                        }
                    });

                    let conditionUser = { _id: userId.toString() }; // on UpdateByIdWithPush function we converting _id to objectID so i apply toString() here.
                    let newFriendRequestUser = { "friendList": targetUserId };
                    userCollection.UpdateByIdWithAddToSet(conditionUser, newFriendRequestUser, (err) => {
                        if (err) {
                            logger.error("OKOSKQSQWQA : ", err);
                            return reject(HTTP500ERROR);
                        }
                    });
                    userCollection.UpdateByIdWithAddToSet(conditionUser, { "follow": targetUserId }, (e, d) => {

                        d = JSON.parse(d);
                        if (d.nModified) {
                            userCollection.UpdateWithIncreaseById(userId.toString(), { "count.followerCount": 1 }, () => { });
                            userCollection.UpdateWithIncreaseById(targetUserId.toString(), { "count.followeeCount": 1 }, () => { });
                        }
                    });
                    let newFriendRequestMqtt = {
                        friendRequestSentBy: targetUserId,
                        friendRequestSentTo: userId,
                        frtiendRequestSentOn: new Date().getTime(),
                        friendRequestStatus: "Accept",
                        friendRequestStatusCode: 2,
                        type: "new Friend"
                    }
                    mqtt.publish(userId.toString(), JSON.stringify(newFriendRequestMqtt), { qos: 1 }, (e) => { if (e) logger.error(e) });
                    mqtt.publish(targetUserId.toString(), JSON.stringify(newFriendRequestMqtt), { qos: 1 }, (e) => { if (e) logger.error(e) });



                    userCollection.SelectOne({ _id: ObjectID(targetUserId.toString()) }, (err, result) => {
                        let request = {
                            fcmTopic: targetUserId.toString(),
                            // action: 1,
                            pushType: 2,
                            title: process.env.APP_NAME,
                            msg: userData.userName + " just accepted your friend request , you can now chat and call freely with  " + userData.userName,
                            data: { "type": "new Friend" },
                            deviceType: (result && result.deviceInfo && result.deviceInfo.deviceType) ? result.deviceInfo.deviceType : "2"
                        }
                        fcm.notifyFcmTpic(request, () => { })

                    });

                    break;
                }
                case 2: {

                    let newFriendRequestMqtt = {
                        friendRequestSentBy: targetUserId,
                        friendRequestSentTo: userId,
                        frtiendRequestSentOn: new Date().getTime(),
                        friendRequestStatus: "Denied",
                        friendRequestStatusCode: 3,
                        type: "remove Friend"
                    }
                    mqtt.publish(userId.toString(), JSON.stringify(newFriendRequestMqtt), { qos: 1 }, (e) => { if (e) logger.error(e) });
                    mqtt.publish(targetUserId.toString(), JSON.stringify(newFriendRequestMqtt), { qos: 1 }, (e) => { if (e) logger.error(e) });

                    break;
                }
            }
            return resolve(true);
        });
    };

    function createNotification() {

        let dataToInsert_User = {
            to: targetUserId,
            from: userId,
            toCollection: 'customer',
            fromCollection: 'customer',
            start: new Date().getTime(),
            end: false,
            type: { "status": 3, "message": 'started following ' }
        };
        let condition_user = {
            to: targetUserId,
            from: userId,
            "$or": [{ "type.status": 3 }, { "type.status": 4 }, { "type.status": 6 }]
        };

        notificationCollection.UpdateWithUpsert(condition_user, dataToInsert_User, (e) => {
            if (e) {
                logger.error(e);
            }
        });
        let dataToInsert_targetUser = {
            to: userId,
            from: targetUserId,
            toCollection: 'customer',
            fromCollection: 'customer',
            start: new Date().getTime(),
            end: false,
            type: { "status": 3, "message": 'started following ' }
        };
        let condition_targetUser = {
            to: userId,
            from: targetUserId,
            "$or": [{ "type.status": 3 }, { "type.status": 4 }, { "type.status": 6 }]
        };

        notificationCollection.UpdateWithUpsert(condition_targetUser, dataToInsert_targetUser, (e) => {
            if (e) {
                logger.error(e);
            }
        });

    }


    getUserData()
        .then(() => { return checkTargetUserIsExists() })
        .then(() => { return updateFriendDetailsCollection() })
        .then(() => { return updateInFollowCollection() })
        .then(() => { return removeRequestFromMyUserListCollection() })
        .then(() => { return removeRequestFromTargetUserListCollection() })
        .then(() => { return PostFriendIfRequired() })
        .then(() => { return res({ message: req.i18n.__('PostFriendRequestResponce')['200'] }).code(200); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })


};

const response = {
    status: {
        200: { message: Joi.any().default(local['PostFriendRequestResponce']['200']) },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { validator, handler, response };