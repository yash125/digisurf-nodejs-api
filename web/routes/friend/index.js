let PostAPI = require('./Post');
let DeleteAPI = require('./Delete');
let GetSearchAPI = require('./GetSearchES');// require('./GetSearch');
let GetRequestAPI = require('./GetRequest');
let headerValidator = require("../../middleware/validator")
let PostRequestResponceAPI = require('./PostRequestResponce');

module.exports = [
    {
        method: 'Post',
        path: '/friendRequestResponce',
        handler: PostRequestResponceAPI.handler,
        config: {
            description: `This API use for send friend request responce`,
            tags: ['api', 'friend'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostRequestResponceAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostRequestResponceAPI.response
        }
    },
    {
        method: 'Delete',
        path: '/friend',
        handler: DeleteAPI.handler,
        config: {
            description: `This API use for send friend request responce`,
            tags: ['api', 'friend'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: DeleteAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
    {
        method: 'Post',
        path: '/friend',
        handler: PostAPI.handler,
        config: {
            description: `This API use for add friend`,
            tags: ['api', 'friend'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    },
    // {
    //     method: 'Get',
    //     path: '/friend',
    //     handler: GetAPI.handler,
    //     config: {
    //         description: `This API use for get friends`,
    //         tags: ['api', 'friend'],
    //         auth: "user",
    //         validate: {
    //             headers: headerValidator.headerAuthValidator,
    //             failAction: (req, reply, source, error) => {
    //                 console.log("error =====> ", error);

    //                 failAction: headerValidator.faildAction(req, reply, source, error)
    //             }
    //         },
    //         response: GetAPI.response
    //     }
    // },
    {
        method: 'Get',
        path: '/friend',
        handler: GetSearchAPI.handler,
        config: {
            description: `This API use for search my friends`,
            tags: ['api', 'friend'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetSearchAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetSearchAPI.response
        }
    },
    {
        method: 'Get',
        path: '/friendRequest',
        handler: GetRequestAPI.handler,
        config: {
            description: `This API use for get friends`,
            tags: ['api', 'friend'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetRequestAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetRequestAPI.response
        }
    }
    // {
    //     method: 'Get',
    //     path: '/friendRequest',
    //     handler: GetRequestAPI.handler,
    //     config: {
    //         description: `This API use for get friends`,
    //         tags: ['api', 'friend'],
    //         auth: "user",
    //         validate: {
    //             headers: headerValidator.headerAuthValidator,
    //             query: GetRequestAPI.queryValidator,
    //             failAction: (req, reply, source, error) => {
    //                 failAction: headerValidator.faildAction(req, reply, source, error)
    //             }
    //         },
    //         response: GetRequestAPI.response
    //     }
    // }
];