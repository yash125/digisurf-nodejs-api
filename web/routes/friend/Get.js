const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const mqtt = require('../../../library/mqtt');
const local = require('../../../locales');
const userCollection = require("../../../models/userList");
const followCollection = require("../../../models/follow");


const handler = (req, res) => {

    const userId = ObjectID(req.auth.credentials._id);
    const HTTP500ERROR = { message: req.i18n.__('genericErrMsg')['500'], code: 500 }
    let followStatus = {};

    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(userId) };
            followCollection.Select(condition, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };

                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        followStatus[element.followee] = element.type
                    }, this);

                    resolve();
                }
            });
        });
    }
    function getFriendList() {
        return new Promise((resolve, reject) => {

            let condition = [
                { "$match": { "_id": userId } },
                { "$unwind": "$friendList" },
                { "$lookup": { "from": "customer", "localField": "friendList", "foreignField": "_id", "as": "friendData" } },
                { "$unwind": "$friendData" },
                {
                    "$project": {
                        "_id": "$friendData._id", "countryCode": "$friendData.countryCode",
                        "number": "$friendData.mobile", "userName": "$friendData.userName",
                        "localNumber": "$friendData.mobile", "private": "$friendData.private",
                        "profileVideo": "", "profileVideoThumbnail": "", "socialStatus": "$friendData.status",
                        "profilePic": "$friendData.profilePic", "firstName": "$friendData.firstName",
                        "lastName": "$friendData.lastName", "starRequest": "$friendData.starRequest",
                        "timestamp": "$friendData.frtiendRequestSentOn", "verified": "$friendData.verified"
                    }
                }
            ];
            userCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    logger.error("OKOKSADSD : ", err);
                    return reject(HTTP500ERROR);
                } else if (result && result.length) {

                    const map = new Map();

                    result.forEach(element => {

                        element["countryCode"] = (element["countryCode"]) ? element["countryCode"] : "";
                        element["number"] = (element["number"]) ? element["countryCode"] : "";
                        element["localNumber"] = (element["localNumber"]) ? element["localNumber"] : element["number"];
                        element["private"] = (element["private"]) ? 1 : 0;
                        element["profileVideo"] = (element["profileVideo"]) ? element["profileVideo"] : "";
                        element["socialStatus"] = (element["socialStatus"]) ? element["socialStatus"] : "";
                        element["profilePic"] = (element["profilePic"] && element["profilePic"].includes("http")) ? element["profilePic"] : "XXXXXXX";
                        element["starRequest"] = (element["starRequest"]) ? element["starRequest"] : {};
                        element["timestamp"] = (element["timestamp"]) ? element["timestamp"] : new Date().getTime();
                        element["verified"] = (element["verified"]) ? element["verified"] : false;

                        if (element["firstName"] == null) element["firstName"] = element["userName"];
                        if (element["lastName"] == null) element["lastName"] = "";
                        try {
                            element["isStar"] = (element["starRequest"] && element["starRequest"]["starUserProfileStatusCode"] == 4) ? true : false;
                        } catch (error) {
                            console.log(error);
                            element["isStar"] = false
                        }

                        element["timestamp"] = (element["timestamp"]) ? element["timestamp"] : new Date().getTime();
                        element["followStatus"] = (followStatus[element["_id"]]) ? followStatus[element["_id"]]["status"] : 0;
                        element["follow"] = (followStatus[element["_id"]]) ? followStatus[element["_id"]] : { "type": 0, "message": "not following" };
                        element["friendStatusCode"] = 2;
                        element["friendStatusText"] = "friends";
                    });

                    var dataToSend = result.map(e => {
                        if (!map.has(e._id.toString())) {
                            map.set(e._id.toString(), true);
                            return e;
                        }
                    }).filter(e => e != null);

                    mqtt.publish("ContactSync/" + userId.toString(), JSON.stringify({ "contacts": dataToSend }), { qos: 1 }, (e) => {
                        if (e) logger.error("ASASASASA :", e)
                    })
                    return resolve({ message: req.i18n.__('GetFriend')['200'], data: dataToSend });
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'], code: 204 });
                }
            });
        });
    }

    getFolloweeIds()
        .then(() => { return getFriendList() })
        .then((data) => { return res({ message: req.i18n.__('GetFriend')['200'], data: data.data }).code(200); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetFriend']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { handler, response };