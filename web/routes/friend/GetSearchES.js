const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userListES = require("../../../models/userListES");
const userListCollection = require("../../../models/userList");
const followCollection = require("../../../models/follow");
const friendDetailsCollection = require("../../../models/friendDetails");

const queryValidator = Joi.object({
    searchText: Joi.string().description("searchText"),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit")
})

const handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    let searchText = req.query.searchText || "";
    // let offset = parseInt(req.query.offset) || 0;
    // let limit = parseInt(req.query.limit) || 20;
    let HTTP500ERROR = { message: req.i18n.__('genericErrMsg')['500'], code: 500 }
    let HTTP200ERROR = { message: req.i18n.__('GetSearchFriend')['200'], code: 200, data: [] };
    let HTTP204ERROR = { message: req.i18n.__('genericErrMsg')['204'], code: 204 };

    let followStatus = {}, newFriendRequests = 0;

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }
    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(userId) };
            followCollection.Select(condition, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };

                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        followStatus[element.followee] = element.type
                    }, this);

                    resolve();
                }
            });
        });
    }
    const getFriendList = () => {
        return new Promise((resolve, reject) => {
            var condition;
            if (searchText) {
                condition = {
                    "must": [
                        { "match_phrase_prefix": { "userName": searchText } },
                        { "match": { "friendList": userId.toString() } },
                        { "match": { "userStatus": 1 } }
                    ]
                };
            } else {
                condition = {
                    "must": [
                        { "match": { "friendList": userId.toString() } },
                        { "match": { "userStatus": 1 } }
                    ]
                };
            }


            userListES.Select(condition, 0, 1000, req.follow, userId.toString(), (err, result) => {
                if (err) {
                    logger.error("OKOKSADSD : ", err);
                    return reject(HTTP500ERROR);
                } else if (result && result.length) {
                    let dataToSend = [];
                    result.forEach(element => {

                        element["profilePic"] = (element["profilePic"] && element["profilePic"].includes("http")) ? element["profilePic"] : "https://res.cloudinary.com/dafszph29/image/upload/c_thumb,w_200,g_face/v1551361911/default/profile_pic.png";
                        element["profileVideoThumbnail"] = (element["profileVideoThumbnail"] && element["profileVideoThumbnail"].includes("http")) ? element["profileVideoThumbnail"] : "";
                        element["profileVideo"] = (element["profileVideo"] && element["profileVideo"].includes("http")) ? element["profileVideo"] : "";
                        element["private"] = (element["private"]) ? 1 : 0;

                        if (element["firstName"] == null) element["firstName"] = element["userName"];
                        if (element["lastName"] == null) element["lastName"] = "";
                        try {
                            element["isStar"] = (element["starRequest"] && element["starRequest"]["starUserProfileStatusCode"] == 4) ? true : false;
                        } catch (error) {
                            console.log(error);
                            element["isStar"] = false
                        }
                        element["timestamp"] = (element["timestamp"]) ? element["timestamp"] : new Date().getTime();
                        element["followStatus"] = (followStatus[element["_id"]]) ? followStatus[element["_id"]]["status"] : 0;
                        element["follow"] = (followStatus[element["_id"]]) ? followStatus[element["_id"]] : { "type": 0, "message": "not following" };
                        element["friendStatusCode"] = 2;
                        element["friendStatusText"] = "friends";
                        dataToSend.push({
                            "firstName": element["firstName"], "lastName": element["lastName"], "isStar": element["isStar"],
                            "timestamp": element["timestamp"], "followStatus": element["followStatus"], "follow": element["follow"],
                            "friendStatusCode": element["friendStatusCode"], "friendStatusText": element["friendStatusText"],
                            "_id": element["_id"], "countryCode": element["countryCode"], "number": element["number"],
                            "userName": element["userName"], "localNumber": element["localNumber"], "private": element["private"],
                            "profileVideo": element["profileVideo"], "profileVideoThumbnail": element["profileVideoThumbnail"],
                            "socialStatus": element["socialStatus"], "profilePic": element["profilePic"],
                            "isBlocked": (userData.blocked && userData.blocked.map(id => id.toString()).includes(element["_id"].toString())) ? 1 : 0
                        });
                    });
                    HTTP200ERROR["data"] = dataToSend;
                    return resolve(HTTP200ERROR)
                } else {
                    return reject(HTTP204ERROR)
                }
            });
        });
    };
    const getWhoSendMeFriendRequestList = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "friendRequestSentTo": userId, "friendRequestStatusCode": 1 } },
                { "$lookup": { "from": "customer", "localField": "friendRequestSentBy", "foreignField": "_id", "as": "friendData" } },
                { "$unwind": "$friendData" },
                {
                    "$project": {
                        "_id": "$friendData._id", "countryCode": "$friendData.countryCode",
                        "number": "$friendData.number", "userName": "$friendData.userName",
                        "profilePic": "$friendData.profilePic", "firstName": "$friendData.firstName",
                        "lastName": "$friendData.lastName", "status": "$friendData.status",
                        "message": "$messageForTargetUser", "timestamp": "$friendData.frtiendRequestSentOn"
                    }
                }
            ];


            friendDetailsCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    logger.error("OKOKAASASDSD : ", err);
                    return reject(HTTP500ERROR);
                } else if (result && result.length) {
                    newFriendRequests = result.length

                }
                return resolve(true);
            });
        });
    };

    getUserData()
        .then(getFolloweeIds)
        .then(getWhoSendMeFriendRequestList)
        .then(getFriendList)
        .then((data) => { return res({ message: data.message, data: data.data, newFriendRequests: newFriendRequests }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetSearchFriend']['200']),
            newFriendRequests : Joi.number().default(0).example(0).description("newFriendRequests").required(),
            data: Joi.any().example([
                {
                    "_id": "5c53f5fd21d398391df8a6df",
                    "countryCode": "+91",
                    "starChatId": "hardikka",
                    "number": "+919106661755",
                    "userName": "hardikka",
                    "profilePic": "http://res.cloudinary.com/dafszph29/image/upload/v1549435289/alc65omlu4bukckqa4zq.jpg",
                    "firstName": "hardik",
                    "lastName": " ",
                    "status": "Hey! I am using Dubly"
                },
                {
                    "_id": "5c53f721feb8833922c2bd90",
                    "countryCode": "+91",
                    "starChatId": "hardikk",
                    "number": "+919099369059",
                    "userName": "hardikk",
                    "profilePic": "http://res.cloudinary.com/dafszph29/image/upload/v1549379539/fpkjmki18yk0l91hjqzo.jpg",
                    "firstName": "hardik",
                    "lastName": " ",
                    "status": "Hey! I am using Dubly"
                }
            ])
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { handler, response, queryValidator };