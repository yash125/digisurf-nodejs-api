let headerValidator = require("../../../middleware/validator")
let GetMyAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/followees',
        handler: GetMyAPI.handler,
        config: {
            description: `This API use for search data in followees`,
            tags: ['api', 'followees'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetMyAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetMyAPI.response
        }
    }
];