'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const followCollection = require("../../../models/follow");

const queryValidator = Joi.object({
    userId: Joi.string().required().description("userId"),
    skip: Joi.string().description('skip'),
    limit: Joi.string().description('limit'),
}).unknown();


const handler = (req, res) => {
    const offset = parseInt(req.query.skip) || 0;
    const limit = parseInt(req.query.limit) || 20;
    let userId = ObjectID(req.query.userId);
    let myFollowees = {};
    let _id = req.auth.credentials._id;


    const getMyFollowees = () => {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(_id), "type.status": 1 };
            followCollection.Select(condition, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else {
                    for (let index = 0; index < result.length; index++) {
                        myFollowees[result[index]["followee"]] = result[index]["type"];
                    }
                    return resolve("--");
                }
            });
        });
    }

    const followees = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "follower": userId, "end": false, "type.status": 1 } },
                { "$match": { "followee": { "$ne": userId } } },
                { "$lookup": { "from": 'customer', "localField": 'followee', "foreignField": '_id', "as": 'user' } },
                { "$unwind": "$user" },
                { "$match": { "user.userStatus": 1 } },
                {
                    "$project": {
                        "_id": 1, "followee": 1, "follower": 1, "start": 1, "end": 1, "type": 1, "profilePic": '$user.profilePic',
                        "userName": '$user.userName', "businessProfile": '$user.businessProfile', "private": '$user.private',
                        "followsBack": 1, "firstName": "$user.firstName", "lastName": "$user.lastName",
                        "userId": '$user._id'
                    }
                },
                { "$sort": { "start": -1 } },
                { "$skip": offset }, { "$limit": limit }
            ];

            followCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else if (result.length === 0) {
                    return reject({ code: 204, message: local['genericErrMsg']['204'] });
                } else {

                    result.forEach(function (element) {
                        // element["userName"] = element["userName"] + "      " + i++;// element["type"] = (myFollowees[element["followee"]]) ? myFollowees[element["followee"]] : { "status": 0, "message": "unfollowed" };
                        element["type"] = { "status": 1, "message": "following" };
                        element["firstName"] = (element["firstName"] != "null" && element["firstName"] != null && typeof element["firstName"] != 'undefined') ? element["firstName"] : "";
                        element["lastName"] = (element["lastName"] != "null" && element["lastName"] != null && typeof element["lastName"] != 'undefined') ? element["lastName"] : "";

                    }, this);

                    return resolve({ code: 200, message: local['GetFolloees']['200'], data: result });
                }
            });
        });
    };

    getMyFollowees()
        .then(() => { return followees() })
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetFolloees']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { queryValidator, handler, response };