const tagged = require('./tagged');
const channels = require('./channels');
const memberPosts = require('./memberPosts');
const member = require('./member');
const posts = require('./posts');
const users = require('./users');
const shoutout = require('./shoutout');

module.exports = [].concat(tagged, channels, memberPosts, member, posts, users, shoutout);