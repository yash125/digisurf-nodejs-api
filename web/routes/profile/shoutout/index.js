let headerValidator = require("../../../middleware/validator")
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/profile/shoutout',
        handler: GetAPI.handler,
        config: {
            description: 'This API will return shoutout post only',
            tags: ['api', 'profile'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];