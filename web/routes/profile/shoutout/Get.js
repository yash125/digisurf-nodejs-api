const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../../locales');
const userCollection = require("../../../../models/userList");
const postESCollection = require("../../../../models/postES");
const businessProductTypeCollection = require("../../../../models/businessProductType");

/**
 * @description API use to get user  active posts 
 * @property {string} authorization - authorization
 * @property {string} lang - language
 
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 * @author DipenAhir
 */


const queryValidator = Joi.object({
    skip: Joi.number().description("skip"),
    limit: Joi.number().description("limit")
}).required();

const handler = (req, res) => {

    const userId = ObjectID(req.auth.credentials._id);
    const offset = parseInt(req.query.skip) || 0;
    const limit = parseInt(req.query.limit) || 20;
    var userIDES = [], filter = [], businessPostTypeLabel = {};


    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    function getBusinessPostTypeLabel() {
        return new Promise((resolve, reject) => {
            businessProductTypeCollection.Select({}, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    result.forEach(element => {
                        businessPostTypeLabel[element._id.toString()] = element.text[req.headers.lang]
                    });
                }
                return resolve(true);
            });
        });
    }

    const getUserPosts = () => {
        return new Promise((resolve, reject) => {





            userIDES.push({ "match": { userId: userId.toString() } })
            //   filter.push({ "match": { "channelId": "" } });


            let condition = {
                "must": [
                    { "term": { "postStatus": 1 } },
                    { "match": { "shoutOutFor": req.auth.credentials._id } }
                ]
            }

            postESCollection.getPostDetails(condition, offset, limit, userData.follow || [], userId, (err, result) => {
                if (err) {
                    console.log("error ", err)
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else if (result && result.length) {

                    result.forEach(element => {
                        element["isBookMarked"] = (userData && userData.bookmarks && userData.bookmarks.map(e => e.toString()).includes(element.postId)) ? true : false;
                        if (element["business"] && element["business"]["businessPostType"]) element["business"]["businessPostTypeLabel"] = businessPostTypeLabel[element["business"]["businessPostType"]] || "";
                        if (element["comments"] && element["comments"].length) element["comments"].reverse();
                        if (element["channelId"] && element["channelId"].length) element["isChannelSubscribed"] = (userData && userData.subscribeChannels && userData.subscribeChannels.map(e => e.toString()).includes(element.channelId.toString())) ? 1 : 0;
                    });

                    let responseobj = { code: 200, message: req.i18n.__('GetProfilePosts')['200'], data: result };
                    return resolve(responseobj);
                } else {
                    let responseobj = { code: 204, message: req.i18n.__('genericErrMsg')['204'] };
                    return reject(responseobj);
                }
            });
        });
    };

    getUserData()
        .then(getBusinessPostTypeLabel)
        .then(getUserPosts)
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetProfilePosts']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { response, handler, queryValidator };