const Joi = require("joi");
var QRCode = require('qrcode')
const logger = require('winston');
const Promise = require('promise');
const cloudinary = require('cloudinary');

const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../../locales');
const userCollection = require("../../../../models/userList");
const postCollection = require("../../../../models/posts");
const followCollection = require("../../../../models/follow");
const profileViews = require("../../../../models/profileViews");
var bussinessCategory = require('../../../../models/bussinessCategory')
const storiesCollection = require("../../../../models/storyPost")
var config = require('../../../../config')
cloudinary.config({
    cloud_name: config.cloudinary.CLOUD_NAME,
    api_key: config.cloudinary.API_KEY,
    api_secret: config.cloudinary.API_SECRET
});

/**
 * @description API to user data for profile page
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const paramsValidator = Joi.object({
}).required();

const queryValidator = Joi.object({
    memberId: Joi.string().required().description("memberId required"),
    skip: Joi.number().description("skip"),
    limit: Joi.number().description("limit")
}).required();

const handler = (req, res) => {

    var memberId = req.query.memberId;
    var qrCode = "";
    var followeeCount = 0, postCount = 0, totalLike = 0, followerCount = 0, followStatus, haveStories = false;
    const userId = ObjectID(req.auth.credentials._id);

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            });
            profileViews.Insert({
                userId: userId,
                timeStamp: new Date().getTime(),
                createOn: new Date(),
                viewBy: ObjectID(req.query.memberId)
            }, () => { });
            QRCode.toDataURL(memberId, function (err, url) {
                qrCode = url;
            });
        });
    }



    const getMemberId = () => {
        return new Promise((resolve, reject) => {
            if (req.query.memberId.length == 24) {
                return resolve();
            } else {
                userCollection.SelectOne({ userName: req.query.memberId }, (err, result) => {
                    if (err) {
                        return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (result && result._id) {
                        memberId = result._id.toString();
                        req.query.memberId = result._id.toString();
                        if (result && result.userStatus && result.userStatus == 2) {
                            return reject({ message: req.i18n.__('GetProfileMember')['403'], "code": 403 });
                        } else {
                            return resolve();
                        }


                    } else {
                        return reject({ message: req.i18n.__('genericErrMsg')['204'], "code": 204 });
                    }
                })
            }
        });
    }

    const getStoriesDetails = (data) => {
        return new Promise((resolve, reject) => {
            storiesCollection.Select({ _id: ObjectID(memberId) }, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    if (result && result.length) {
                        haveStories = true
                    }
                    return resolve();
                }
            })
        });
    };


    const findMemberId = () => {
        return new Promise((resolve, reject) => {
            if (req.query.memberId.length == 24) {
                memberId = ObjectID(req.query.memberId)
                return resolve("--")
            } else {
                memberId = req.query.memberId;
                memberId = memberId.replace(/[^a-zA-Z0-9]/g, '');
                userCollection.SelectOne({ "userName": memberId }, (err, result) => {
                    if (err) {
                        logger.silly("it got error " + err);
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (result) {
                        memberId = ObjectID(result._id);
                        return resolve(true);
                    } else {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    }
                })
            }
        });
    }
    const getfollowStatus = () => {
        return new Promise((resolve, reject) => {
            let condition = {
                follower: userId, followee: memberId
            };
            followCollection.Select(condition, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    followStatus = d;
                    return resolve(d);
                }
            });
        });
    };
    const getPostCount = () => {
        return new Promise((resolve, reject) => {
            postCollection.Aggregate([
                { "$match": { "userId": memberId, postStatus: 1 } },
                {
                    "$group": {
                        _id: "$userId",
                        totalLikes: { "$sum": "$likesCount" }
                    }
                }
            ], (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result && result[0] && result[0].totalLikes) {
                    totalLike = result[0].totalLikes;
                }
            });

            postCollection.GetPostCount({ userId: memberId, "postStatus": 1, channelId: "" }, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    postCount = result;
                    return resolve(true);
                }
            });
        });
    };
    const getFollowerCount = () => {
        return new Promise((resolve, reject) => {
            followCollection.GetFollowerCount(memberId, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    followerCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                    return resolve(true);
                }
            })
        });
    };
    const getFolloweeCount = () => {
        return new Promise((resolve, reject) => {
            followCollection.GetFolloweeCount(memberId, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    followeeCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                    return resolve(true);
                }
            })
        });
    };
    const getUserBasics = () => {
        return new Promise((resolve, reject) => {
            let projection = {
                "countryCode": 1, "number": 1, "registeredOn": 1, "private": 1, "firstName": 1,
                "lastName": 1, "userName": 1, "profilePic": 1, "following": 1, "followers": 1, "qrCode": 1,
                "verified": 1, "starRequest": 1, "profileCoverImage": 1, "status": 1,
                "isActiveBusinessProfile": 1, "businessProfile": 1
            };
            let condition = { _id: memberId };
            userCollection.SelectWithSort(condition, { "_id": -1 }, projection, 0, 1, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result.length === 0) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {
                    result = result[0];
                    if (result.qrCode == undefined) {
                        cloudinary.uploader.upload(qrCode, function (resultCloud) {
                            userCollection.Update({ _id: memberId }, { qrCode: { url: resultCloud.url } },
                                (e) => {
                                    if (e) logger.error(e)
                                    qrCode = resultCloud.url;
                                    result["haveStories"] = haveStories;
                                    result["followers"] = followerCount;
                                    result["following"] = followeeCount;
                                    result["postsCount"] = postCount;
                                    result["totalLike"] = totalLike;
                                    result["liked"] = false;
                                    result["isBlocked"] = (userData.blocked && userData.blocked.map(id => id.toString()).includes(memberId.toString())) ? 1 : 0;
                                    result["followStatus"] = (followStatus[0] && followStatus[0].type) ? followStatus[0].type.status : 0;
                                    result["qrCode"] = qrCode
                                    result["friendStatusCode"] = (userData.friendList && userData.friendList.map(e => e.toString()).includes(memberId.toString())) ? 2 : (userData.myFriendRequest && userData.myFriendRequest.map(e => e.toString()).includes(memberId.toString())) ? 3 : 1;
                                    result["friendStatusText"] = (result["friendStatusCode"] == 2) ? "friends" : (result["friendStatusCode"] == 3) ? "userSentARequestToTargetUser" : "notAFirend";
                                    result["isStar"] = (result["starRequest"] && result["starRequest"]["starUserProfileStatusCode"] == 4) ? true : false;

                                    if (result["starRequest"] && result.verified) {
                                        result["starRequest"]["description"] = result.verified.description || "";
                                        result["starRequest"]["emailId"] = result.verified.emailId || "";
                                        result["starRequest"]["phoneNumber"] = result.verified.phoneNumber || "";
                                        result["starRequest"]["countryCode"] = result.verified.countryCode || "";
                                        result["starRequest"]["number"] = result.verified.number || "";
                                    }

                                    if (result["businessProfile"] && result["businessProfile"][0] && result["businessProfile"][0]["businessCategoryId"]) {
                                        bussinessCategory.Select({ _id: ObjectID(result.businessProfile[0].businessCategoryId.toString()) }, (err, resultBusiness) => {
                                            if (err) {
                                                return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                                            } else if (resultBusiness && resultBusiness.length) {
                                                result["businessProfile"][0]["businessCategory"] = resultBusiness[0].type || "";
                                            }
                                            if (result && result.isActiveBusinessProfile &&
                                                result.businessProfile && result.businessProfile[0] &&
                                                result.businessProfile[0].businessProfileId &&
                                                result.businessProfile[0].businessProfileId.toString() == result._id.toString() &&
                                                result.businessProfile[0].statusCode &&
                                                result.businessProfile[0].statusCode == 1
                                            ) {
                                                //means all is good
                                            } else {
                                                result["isActiveBusinessProfile"] = false
                                                // result["businessProfile"] = []
                                            }
                                            return resolve({
                                                code: 200,
                                                message: req.i18n.__('GetProfileMember')['200'],
                                                data: [result],
                                                followData: followStatus
                                            });
                                        });
                                    } else {
                                        return resolve({
                                            code: 200,
                                            message: req.i18n.__('GetProfileMember')['200'],
                                            data: [result],
                                            followData: followStatus
                                        });
                                    }
                                });
                        });
                    } else {
                        qrCode = result.qrCode.url;
                        result["haveStories"] = haveStories;
                        result["followers"] = followerCount;
                        result["following"] = followeeCount;
                        result["postsCount"] = postCount;
                        result["totalLike"] = totalLike;                        
                        result["liked"] = false;
                        result["isBlocked"] = (userData.blocked && userData.blocked.map(id => id.toString()).includes(memberId.toString())) ? 1 : 0;
                        result["followStatus"] = (followStatus[0] && followStatus[0].type) ? followStatus[0].type.status : 0;
                        result["qrCode"] = qrCode
                        result["friendStatusCode"] = (userData.friendList && userData.friendList.map(e => e.toString()).includes(memberId.toString())) ? 2 : (userData.myFriendRequest && userData.myFriendRequest.map(e => e.toString()).includes(memberId.toString())) ? 3 : 1;
                        result["friendStatusText"] = (result["friendStatusCode"] == 2) ? "friends" : (result["friendStatusCode"] == 3) ? "userSentARequestToTargetUser" : "notAFirend";
                        result["isStar"] = (result["starRequest"] && result["starRequest"]["starUserProfileStatusCode"] == 4) ? true : false;

                        if (result["starRequest"] && result.verified) {
                            result["starRequest"]["description"] = result.verified.description || "";
                            result["starRequest"]["emailId"] = result.verified.emailId || "";
                            result["starRequest"]["phoneNumber"] = result.verified.phoneNumber || "";
                            result["starRequest"]["countryCode"] = result.verified.countryCode || "";
                            result["starRequest"]["number"] = result.verified.number || "";
                        }

                        if (result["businessProfile"] && result["businessProfile"][0] && result["businessProfile"][0]["businessCategoryId"]) {
                            bussinessCategory.Select({ _id: ObjectID(result.businessProfile[0].businessCategoryId.toString()) }, (err, resultBusiness) => {
                                if (err) {
                                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                                } else if (resultBusiness && resultBusiness.length) {
                                    result["businessProfile"][0]["businessCategory"] = resultBusiness[0].type || "";
                                }
                                if (result && result.isActiveBusinessProfile &&
                                    result.businessProfile && result.businessProfile[0] &&
                                    result.businessProfile[0].businessProfileId &&
                                    result.businessProfile[0].businessProfileId.toString() == result._id.toString() &&
                                    result.businessProfile[0].statusCode &&
                                    result.businessProfile[0].statusCode == 1
                                ) {
                                    //means all is good
                                } else {
                                    result["isActiveBusinessProfile"] = false
                                    // result["businessProfile"] = []
                                }
                                return resolve({
                                    code: 200,
                                    message: req.i18n.__('GetProfileMember')['200'],
                                    data: [result],
                                    followData: followStatus
                                });
                            });
                        } else {
                            return resolve({
                                code: 200,
                                message: req.i18n.__('GetProfileMember')['200'],
                                data: [result],
                                followData: followStatus
                            });
                        }
                    }
                }
            });
        });
    };



    getMemberId()
        .then(() => { return getUserData(); })
        .then(() => { return getStoriesDetails(); })
        .then(() => { return findMemberId(); })
        .then(() => { return getfollowStatus(); })
        .then(() => { return getPostCount(); })
        .then(() => { return getFollowerCount(); })
        .then(() => { return getFolloweeCount(); })
        .then((followData) => { return getUserBasics(followData); })
        .then((data) => { return res({ message: data.message, data: data.data, followData: data.followData }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};


const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetProfileMember']['200']), data: Joi.any(), followData: Joi.any()
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        403: { message: Joi.any().default(local['GetProfileMember']['403']) },
    }
};//swagger response code

module.exports = {
    response,
    handler,
    queryValidator,
    paramsValidator
};