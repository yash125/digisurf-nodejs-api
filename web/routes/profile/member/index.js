let headerValidator = require("../../../middleware/validator")
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/profile/member',
        handler: GetAPI.handler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'profile'],
            auth: {
                strategies: ['basic', 'userJWT', 'guest']
            },
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: GetAPI.response
        }
    }
];