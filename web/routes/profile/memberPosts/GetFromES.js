const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../../locales');
const postESCollection = require("../../../../models/postES");
const userCollection = require("../../../../models/userList");
const followCollection = require("../../../../models/follow");
const businessProductTypeCollection = require("../../../../models/businessProductType");

/**
 * @description API to user data for profile page
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const paramsValidator = Joi.object({
}).required();

const queryValidator = Joi.object({
    memberId: Joi.string().required().description("memberId required"),
    skip: Joi.number().description("skip"),
    limit: Joi.number().description("limit")
}).required();

const handler = (req, res) => {

    const offset = parseInt(req.query.skip) || 0;
    const limit = parseInt(req.query.limit) || 20;
    var memberId = "";
    const userId = ObjectID(req.auth.credentials._id);
    var isPrivate = 0, amIFollow = 0;
    var userIDES = [], filter = [], follow = [], businessPostTypeLabel = {};


    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }
    const findMemberId = () => {
        return new Promise((resolve, reject) => {
            if (req.query.memberId.length == 24) {
                memberId = ObjectID(req.query.memberId)
                userCollection.SelectOne({ "_id": memberId }, (err, result) => {
                    if (err) {
                        logger.error("it got error " + err);
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (result) {
                        isPrivate = result.private || 0;
                        // isMemberActiveBusinessProfile = (result && result.isActiveBusinessProfile);
                        if (result && result.userStatus && result.userStatus == 2) {
                            return reject({ message: req.i18n.__('GetProfileMemberPosts')['403'], "code": 403 });
                        } else {
                            return resolve();
                        }
                    } else {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    }
                });
            } else {
                memberId = req.query.memberId;
                memberId = memberId.replace(/[^a-zA-Z0-9]/g, '');
                userCollection.SelectOne({ "userName": memberId }, (err, result) => {
                    if (err) {
                        logger.error("it got error " + err);
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (result) {
                        memberId = ObjectID(result._id);
                        // isMemberActiveBusinessProfile = (result && result.isActiveBusinessProfile);
                        isPrivate = result.private || 0;

                        if (result && result.userStatus && result.userStatus == 2) {
                            return reject({ message: req.i18n.__('genericErrMsg')['403'], "code": 403 });
                        } else {
                            return resolve();
                        }

                    } else {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    }
                });
            }
        });
    }
    const getFollowStatus = () => {
        return new Promise((resolve, reject) => {
            let condition = { "follower": userId, "followee": memberId, "end": false, "type.status": 1 };
            followCollection.Select(condition, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    amIFollow = 0;
                    if (memberId == userId.toString()) amIFollow = 1;
                    return resolve(d);
                } else {
                    amIFollow = 1;
                    return resolve(d);
                }
            });
        });
    };
    function getBusinessPostTypeLabel() {
        return new Promise((resolve, reject) => {
            businessProductTypeCollection.Select({}, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    result.forEach(element => {
                        businessPostTypeLabel[element._id.toString()] = element.text[req.headers.lang]
                    });
                }
                return resolve(true);
            });
        });
    }
    const getUserPosts = () => {
        return new Promise((resolve, reject) => {

            if (isPrivate && !amIFollow) {
                return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
            }
            userIDES.push({ "match": { userId: memberId.toString() } })
            // filter.push({ "match": { "channelId": "" } });

            let condition = {
                "must": [
                    { "term": { "postStatus": 1 } },
                    { "match": { "userId": memberId.toString() } }
                ],
                "must_not": [
                    {
                        "wildcard": {
                            "channelId": "*"
                        }
                    }
                ]
            }




            postESCollection.getPostDetails(condition, offset, limit, userData.follow, userId, (err, result) => {
                if (err) {
                    console.log("error ", err)
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else if (result && result.length) {

                    result.forEach(element => {
                        element["isBookMarked"] = (userData && userData.bookmarks && userData.bookmarks.map(e => e.toString()).includes(element.postId)) ? true : false;
                        if (element["business"] && element["business"]["businessPostType"]) element["business"]["businessPostTypeLabel"] = businessPostTypeLabel[element["business"]["businessPostType"]] || "";
                        if (element["comments"] && element["comments"].length) element["comments"].reverse();
                        if (element["channelId"] && element["channelId"].length) element["isChannelSubscribed"] = (userData && userData.subscribeChannels && userData.subscribeChannels.map(e => e.toString()).includes(element.channelId.toString())) ? 1 : 0;
                    });

                    let responseobj = { code: 200, message: req.i18n.__('GetHome')['200'], data: result };
                    return resolve(responseobj);
                } else {
                    let responseobj = { code: 204, message: req.i18n.__('genericErrMsg')['204'] };
                    return reject(responseobj);
                }
            });
        });
    };

    getUserData()
        .then(() => { return getBusinessPostTypeLabel(); })
        .then(() => { return findMemberId(); })
        .then(() => { return getFollowStatus(); })
        .then((profileData) => { return getUserPosts(profileData); })
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => {
            console.log("error ", error)
            return res({ message: error.message }).code(error.code);
        })
};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetProfileMemberPosts']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        403: { message: Joi.any().default(local['GetProfileMemberPosts']['403']) },
    }
};//swagger response code

module.exports = { response, handler, queryValidator, paramsValidator };