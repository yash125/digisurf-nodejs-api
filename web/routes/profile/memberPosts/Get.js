const Joi = require("joi");

const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../../locales');

const userCollection = require("../../../../models/userList");
const followCollection = require("../../../../models/follow");
/**
 * @description API to user data for profile page
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const queryValidator = Joi.object({
    memberId: Joi.string().required().description("memberId required"),
    skip: Joi.number().description("skip"),
    limit: Joi.number().description("limit")
}).required();

const handler = (req, res) => {

    const offset = parseInt(req.query.skip) || 0;
    const limit = parseInt(req.query.limit) || 20;
    var memberId = "";
    const userId = ObjectID(req.auth.credentials._id);
    var isPrivate = 0, amIFollow = 0;

    const findMemberId = () => {
        return new Promise((resolve, reject) => {
            if (req.query.memberId.length == 24) {
                memberId = ObjectID(req.query.memberId)
                userCollection.SelectOne({ "_id": memberId }, (err, result) => {
                    if (err) {
                        logger.error("it got error " + err);
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (result) {
                        isPrivate = result.private || 0;
                        return resolve("---");
                    } else {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    }
                });
            } else {
                memberId = req.query.memberId;
                memberId = memberId.replace(/[^a-zA-Z0-9]/g, '');
                userCollection.SelectOne({ "userName": memberId }, (err, result) => {
                    if (err) {
                        logger.error("it got error " + err);
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (result) {
                        memberId = ObjectID(result._id);
                        isPrivate = result.private || 0;
                        return resolve("---");
                    } else {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    }
                });
            }
        });
    }
    const getFollowStatus = () => {
        return new Promise((resolve, reject) => {
            let condition = { "follower": userId, "followee": memberId, "end": false, "type.status": 1 };
            followCollection.Select(condition, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    amIFollow = 0;
                    if (memberId == userId.toString()) amIFollow = 1;
                    return resolve(d);
                } else {
                    amIFollow = 1;
                    return resolve(d);
                }
            });
        });
    };
    const getUserPosts = () => {
        return new Promise((resolve, reject) => {

            if (isPrivate && !amIFollow) {
                return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
            }

            let condition = [
                { "$match": { "_id": memberId, } },
                { "$lookup": { "from": 'posts', "localField": '_id', "foreignField": 'userId', "as": 'postsData' } },
                { "$unwind": "$postsData" },
                { "$lookup": { "from": "channel", "localField": "postsData.channelId", "foreignField": "_id", "as": "channleData" } },
                { "$unwind": { "path": "$channleData", "preserveNullAndEmptyArrays": true } },
                { "$lookup": { "from": "category", "localField": "postsData.categoryId", "foreignField": "_id", "as": "categoryData" } },
                { "$unwind": { "path": "$categoryData", "preserveNullAndEmptyArrays": true } },
                {
                    "$match": {
                        "$or": [
                            { 'postsData.channelId': null },
                            { 'postsData.channelId': "" }
                        ]
                    }
                },
                { "$lookup": { "from": "dublyAudio", "localField": "postsData.musicId", "foreignField": "_id", "as": "musicData" } },
                { "$unwind": { "path": "$musicData", "preserveNullAndEmptyArrays": true } },
                {
                    "$project": {
                        "countryCode": 1, "number": 1, "registeredOn": 1, "private": 1,
                        "businessProfile": 1, "postId": '$postsData._id', "imageUrl1": '$postsData.imageUrl1',
                        "thumbnailUrl1": '$postsData.thumbnailUrl1', "mediaType1": '$postsData.mediaType1',
                        "userId": '$postsData.userId', "createdOn": '$postsData.createdOn', "timeStamp": '$postsData.timeStamp',
                        "title": '$postsData.title',
                        "likesCount": '$postsData.likesCount',
                        "hashTags": '$postsData.hashTags', "userName": 1, "profilePic": 1, "city": '$postsData.city',
                        "place": '$postsData.place', "imageUrl1Height": '$postsData.imageUrl1Height',
                        "location": '$postsData.location', "imageUrl1Width": '$postsData.imageUrl1Width',
                        "categoryActiveIconUrl": "$categoryData.categoryActiveIconUrl",
                        "likes": '$postsData.likes', "musicData": 1,

                        "channelImageUrl": { '$ifNull': ["$channleData.channelImageUrl", ""] },
                        "categoryName": { '$ifNull': ["$categoryData.categoryName", ""] },
                        "categoryUrl": { '$ifNull': ["$categoryData.categoryActiveIconUrl", ""] },
                        "channelName": { '$ifNull': ["$channleData.channelName", ""] },
                        "categoryId": { '$ifNull': ['$postsData.categoryId', ""] },
                        "channelId": { '$ifNull': ['$postsData.channelId', ""] },
                        "distinctViews": { "$size": { '$ifNull': ['$postsData.distinctViews', []] } },
                        "totalComments": { "$size": { '$ifNull': ['$postsData.comments', []] } },
                    }
                },
                { "$sort": { "createdOn": -1 } }, { "$skip": offset }, { "$limit": limit }
            ];

            userCollection.Aggregate(condition, (e, d) => {
                if (e) {
                    console.log("e ", e)
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                } else {

                    d.forEach(function (element) {
                        element["followStatus"] = amIFollow;//(followStatus[element["userId"]]) ? followStatus[element["userId"]]["status"] : 0;
                        element["liked"] = (element["likes"]) ? element["likes"].map(id => id.toString()).includes(userId.toString()) : false;
                        delete element["likes"];
                    }, this);

                    return resolve({
                        code: 200,
                        message: req.i18n.__('GetProfileMemberPosts')['200'],
                        data: d
                    });
                }
            });
        });
    };

    findMemberId()
        .then(() => { return getFollowStatus(); })
        .then((profileData) => { return getUserPosts(profileData); })
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => {
            console.log("error ", error)
            return res({ message: error.message }).code(error.code);
        })
};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetProfileMemberPosts']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        403: { message: Joi.any().default(local['GetProfileMemberPosts']['403']) },
    }
};//swagger response code

module.exports = { response, handler, queryValidator };