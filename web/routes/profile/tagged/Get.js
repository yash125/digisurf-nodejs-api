const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../../locales');
const postCollection = require("../../../../models/posts");

/**
 * @description API to user data for profile page
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const paramsValidator = Joi.object({
}).required();

const queryValidator = Joi.object({
    person: Joi.string().required().description('eg. me or member'),
    memberId: Joi.string().description('memberId'),
    skip: Joi.number().description("skip"),
    limit: Joi.number().description("limit")
}).unknown();

const handler = (req, res) => {
    // const offset = parseInt(req.query.offset) || 0;
    // const limit = parseInt(req.query.limit) || 20;
    const userId = ObjectID(req.auth.credentials._id);

    const photosOfMe = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { 'taggedUsers.userId': userId } }
            ];
            postCollection.Aggregate(condition, (e, d) => {
                if (e) return reject(e);
                else return resolve(d);
            });
        });
    }

    const photosOfMember = (memberId) => {
        return new Promise((resolve, reject) => {
            let condition = [{ "$match": { 'taggedUsers.userId': memberId } }];
            postCollection.Aggregate(condition, (e, d) => {
                if (e) return reject(e);
                else return resolve(d);
            });
        });
    };

    switch (req.query.person) {
        case 'me':
            photosOfMe()
                .then((data) => { return res({ message: req.i18n.__('GetProfileTagged')['200'], data: data }).code(200); })
                .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500); })
            break;
        case 'member':
            if (!req.query.memberId) return res({ message: req.i18n.__('genericErrMsg')['400'] }).code(400);
            var memberId = ObjectID(req.query.memberId);
            photosOfMember(memberId)
                .then((data) => { return res({ message: req.i18n.__('GetProfileTagged')['200'], data: data }).code(200); })
                .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500); })

            break;
        default:
            return res({ message: req.i18n.__('GetProfileTagged')['409'] }).code(409);
    }

};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetProfileTagged']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        409: { message: Joi.any().default(local['GetProfileTagged']['409']) },
    }
};//swagger response code

module.exports = { response, handler, queryValidator, paramsValidator };