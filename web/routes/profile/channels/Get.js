const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
const local = require('../../../../locales');
const userCollection = require("../../../../models/userList");
const channelCollection = require("../../../../models/channel");
const businessProductTypeCollection = require("../../../../models/businessProductType");

/**
 * @description API to user data for profile page
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const queryValidator = Joi.object({
    skip: Joi.number().description("skip"),
    limit: Joi.number().description("limit")
}).required();

const handler = (req, res) => {
    const offset = parseInt(req.query.offset) || 0;
    const limit = parseInt(req.query.limit) || 20;
    const userId = ObjectID(req.auth.credentials._id);
    var businessPostTypeLabel = {};

    function getBusinessPostTypeLabel() {
        return new Promise((resolve, reject) => {
            businessProductTypeCollection.Select({}, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    result.forEach(element => {
                        businessPostTypeLabel[element._id.toString()] = element.text[req.headers.lang]
                    });
                }
                return resolve(true);
            });
        });
    }



    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    const getPostData = () => {
        return new Promise(() => {
            let condition = [
                { "$match": { "userId": userId, "channelStatus": 1 } },
                { "$lookup": { "from": 'customer', "localField": 'userId', "foreignField": '_id', "as": 'userData' } },
                { "$unwind": '$userData' },
                { "$lookup": { "from": 'posts', "localField": '_id', "foreignField": 'channelId', "as": 'postData' } },
                { "$unwind": '$postData' },
                { "$match": { 'postData.userId': userId, 'postData.postStatus': 1 } },
                { "$sort": { 'postData.timeStamp': -1 } },
                {
                    "$group": {
                        "_id": '$_id',
                        "channelName": { "$first": '$channelName' },
                        "createdOn": { "$first": '$createdOn' },
                        "private": { "$first": '$private' },
                        "channelImageUrl": { "$first": '$channelImageUrl' },
                        "description": { "$first": '$description' },
                        "categoryId": { "$first": '$postData.categoryId' },
                        "categoryName": { "$first": "$postData.categoryName" },
                        "categoryUrl": { "$first": "$postData.categoryUrl" },
                        "data": {
                            "$push": {
                                "postId": '$postData._id',
                                "business": "$postData.business",
                                "imageUrl1": '$postData.imageUrl1', "thumbnailUrl1": '$postData.thumbnailUrl1',
                                "imageUrl1Width": { "$ifNull": ["$postData.imageUrl1Width", ""] },
                                "imageUrl1Height": { "$ifNull": ["$postData.imageUrl1Height", ""] },
                                "mediaType1": '$postData.mediaType1', "createdOn": '$postData.createdOn', "timeStamp": '$postData.timeStamp',
                                "title": '$postData.title', "categoryId": '$postData.categoryId', "categoryName": "$postData.categoryName",
                                "categoryUrl": "$postData.categoryUrl", "channelName": '$postData.channelName',
                                "channelId": '$postData.channelId', "userId": '$postData.userId', "phoneNumber": '$postData.mobile',
                                "hashTags": { '$ifNull': ['$postData.hashTags', []] }, "likesCount": { '$ifNull': ['$postData.likesCount', 0] },
                                "profilepic": '$userData.profilePic',
                                "userName": '$userData.userName', "comments": { "$ifNull": ['$postData.comments', []] },
                                "totalComments": { "$size": { '$ifNull': ['$postData.comments', []] } },
                                "distinctViews": { "$size": { '$ifNull': ['$postData.distinctViews', []] } },
                                "location": { '$ifNull': ['$postData.location', ""] }, "place": { '$ifNull': ['$postData.place', ""] },
                                "city": { "$ifNull": ['$postData.city', ""] },
                            }
                        }
                    }
                }, { "$skip": offset }, { "$limit": limit }
            ];
            channelCollection.Aggregate(condition, (e, d) => {
                if (e) {
                    return res({ messgae: req.i18n.__('genericErrMsg')['500'] }).code(500);
                } else if (d.length === 0) {
                    return res({ messgae: req.i18n.__('genericErrMsg')['204'] }).code(204);
                } else {
                    d.forEach(element => {
                        element["isBookMarked"] = (userData && userData.bookmarks && userData.bookmarks.map(e => e.toString()).includes(element.postId)) ? true : false;
                        if (element["business"] && element["business"]["businessPostType"]) element["business"]["businessPostTypeLabel"] = businessPostTypeLabel[element["business"]["businessPostType"]] || "";
                        if (element["comments"] && element["comments"].length) element["comments"].reverse();
                    });

                    return res({
                        messgae: req.i18n.__('GetProfileChannels')['200'],
                        data: d
                    }).code(200);
                }
            });
        });
    };

    getBusinessPostTypeLabel()
        .then(getUserData)
        .then(getPostData)
        .catch(() => {
            return res({ messgae: req.i18n.__('genericErrMsg')['500'] }).code(500);
        })



};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetProfileChannels']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) }
    }
};//swagger response code

module.exports = { handler, response, queryValidator };