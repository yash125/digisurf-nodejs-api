let headerValidator = require("../../../middleware/validator")
let GetByIdAPI = require('./GetById');
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/profile/channel',
        handler: GetByIdAPI.handler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'profile'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetByIdAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByIdAPI.response
        }
    },
    {
        method: 'GET',
        path: '/profile/channels',
        handler: GetAPI.handler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'profile'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: GetAPI.response
        }
    }
];