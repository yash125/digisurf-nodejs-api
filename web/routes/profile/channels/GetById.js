const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../../locales');
const userCollection = require("../../../../models/userList");
const followCollection = require("../../../../models/follow");
const channelCollection = require("../../../../models/channel");
const businessProductTypeCollection = require("../../../../models/businessProductType");


/**
 * @description API to user data for profile page
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const paramsValidator = Joi.object({
}).required();

const queryValidator = Joi.object({
    skip: Joi.number().description("skip"),
    userId: Joi.string().required().description("userId required"),
    limit: Joi.number().description("limit")
}).required();

const handler = (req, res) => {

    const offset = parseInt(req.query.offset) || 0;
    const limit = parseInt(req.query.limit) || 20;
    const userId_login = ObjectID(req.auth.credentials._id);
    var userId = req.query.userId;
    // const memberId = ObjectID(req.query.userId);
    var followStatus = {};
    var businessPostTypeLabel = {};

    function getBusinessPostTypeLabel() {
        return new Promise((resolve, reject) => {
            businessProductTypeCollection.Select({}, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    result.forEach(element => {
                        businessPostTypeLabel[element._id.toString()] = element.text[req.headers.lang]
                    });
                }
                return resolve(true);
            });
        });
    }


    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(userId) };
            followCollection.Select(condition, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        followStatus[element.followee] = element.type
                    }, this);
                    return resolve(true);
                }
            });
        });
    }
    const findMemberId = () => {
        return new Promise((resolve, reject) => {
            if (req.query.userId.length == 24) {
                userId = ObjectID(req.query.userId)
                return resolve("--")
            } else {
                userId = req.query.userId;
                userId = userId.replace(/[^a-zA-Z0-9]/g, '');
                userCollection.SelectOne({ "userName": userId }, (err, result) => {
                    if (err) {
                        logger.silly("it got error " + err);
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (result) {
                        userId = ObjectID(result._id);
                        return resolve("---");
                    } else {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    }
                })
            }
        });
    }
    const getChannels = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "userId": ObjectID(userId), "channelStatus": 1 } },
                { "$lookup": { "from": 'customer', "localField": 'userId', "foreignField": '_id', "as": 'userData' } },
                { "$unwind": '$userData' },
                { "$lookup": { "from": 'posts', "localField": '_id', "foreignField": 'channelId', "as": 'postData' } },
                { "$unwind": { "path": "$postData", "preserveNullAndEmptyArrays": true } },
                { "$match": { 'postData.postStatus': 1 } },
                { "$lookup": { "from": 'category', "localField": 'categoryId', "foreignField": '_id', "as": 'categoryData' } },
                { "$unwind": { "path": "$categoryData", "preserveNullAndEmptyArrays": true } },
                { "$lookup": { "from": "dublyAudio", "localField": "postData.musicId", "foreignField": "_id", "as": "musicData" } },
                { "$unwind": { "path": "$musicData", "preserveNullAndEmptyArrays": true } },
                {
                    "$group": {
                        "_id": '$_id',
                        "userId": { "$first": "$userId" },
                        "createdOn": { "$first": '$createdOn' },
                        "private": { "$first": '$private' },
                        "description": { "$first": '$description' },
                        "confirm": { "$first": "$confirm" },
                        "reject": { "$first": "$reject" },
                        "request": { "$first": "$request" },
                        "channelImageUrl": { "$first": "$channelImageUrl" },
                        "categoryName": { "$first": "$categoryData.categoryName" },
                        "categoryUrl": { "$first": "$categoryData.categoryActiveIconUrl" },
                        "channelName": { "$first": "$channelName" },
                        "categoryId": { "$first": "$categoryId" },
                        "channelId": { "$first": "$_id" },
                        "data": {
                            "$push": {
                                "postId": '$postData._id', "imageUrl1": '$postData.imageUrl1', "thumbnailUrl1": '$postData.thumbnailUrl1',
                                "mediaType1": '$postData.mediaType1', "createdOn": '$postData.createdOn', "timeStamp": '$postData.timeStamp',
                                "title": '$postData.title', "userId": '$postData.userId',
                                "phoneNumber": '$postData.mobile', "profilepic": '$userData.profilePic', "musicData": "$musicData",
                                "userName": '$userData.userName', "business": "$postData.business",


                                "city": { "$ifNull": ['$postData.city', ""] },
                                "place": { '$ifNull': ['$postData.place', ""] },
                                "comments": { "$ifNull": ['$postData.comments', []] },
                                "hashTags": { '$ifNull': ['$postData.hashTags', []] },
                                "location": { '$ifNull': ['$postData.location', ""] },
                                "likesCount": { '$ifNull': ['$postData.likesCount', 0] },
                                "shareCount": { '$ifNull': ['$postData.shareCount', 0] },
                                "imageUrl1Width": { "$ifNull": ["$postData.imageUrl1Width", ""] },
                                "imageUrl1Height": { "$ifNull": ["$postData.imageUrl1Height", ""] },
                                "totalComments": { "$size": { '$ifNull': ['$postData.comments', []] } },
                                "distinctViews": { "$size": { '$ifNull': ['$postData.distinctViews', []] } },

                                "channelImageUrl": { '$ifNull': ["$channelImageUrl", ""] },
                                "categoryName": { '$ifNull': ["$categoryData.categoryName", ""] },
                                "categoryUrl": { '$ifNull': ["$categoryData.categoryActiveIconUrl", ""] },
                                "channelName": { '$ifNull': ["$channelName", ""] },
                                "categoryId": { '$ifNull': ['$categoryData._id', ""] },
                                "channelId": { '$ifNull': ['$channelId', ""] },
                            }
                        }
                    }
                },
                { $match: { "data.postId": { $exists: true } } },
                { "$sort": { 'createdOn': -1 } }, { "$skip": offset }, { "$limit": limit }
            ];

            channelCollection.Aggregate(condition, (e, d) => {
                if (e) {
                    return reject({ messgae: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length === 0) {
                    return reject({ messgae: req.i18n.__('genericErrMsg')['204'] });
                } else {
                    /* 
                    subscribeStatus 
                    0 : none,
                    1 : confirm,
                    2 : request
                    3 : reject
                */

                    d.forEach(element => {

                        element["isBookMarked"] = (userData && userData.bookmarks && userData.bookmarks.map(e => e.toString()).includes(element.postId)) ? true : false;
                        if (element["business"] && element["business"]["businessPostType"]) element["business"]["businessPostTypeLabel"] = businessPostTypeLabel[element["business"]["businessPostType"]] || "";
                        if (element["comments"] && element["comments"].length) element["comments"].reverse();


                        var subscribeStatus = 0;
                        if (element["confirm"] && element["confirm"].map(id => id.userId.toString()).includes(userId_login.toString())) subscribeStatus = 1;
                        if (element["request"] && element["request"].map(id => id.userId.toString()).includes(userId_login.toString())) subscribeStatus = 2;
                        if (element["reject"] && element["reject"].map(id => id.userId.toString()).includes(userId_login.toString())) subscribeStatus = 3;
                        element["subscriber"] = (element["confirm"]) ? element["confirm"].length : 0;
                        element["isSubscribed"] = subscribeStatus;

                        delete element["confirm"];
                        delete element["request"];
                        delete element["reject"];

                        element.data.map(ele => { ele["followStatus"] = (followStatus[ele["userId"]]) ? followStatus[ele["userId"]]["status"] : 0; });
                        element.data.map(ele => { ele["liked"] = (ele["likes"]) ? ele["likes"].map(id => id.toString()).includes(userId_login.toString()) : false });
                        element.data.map(ele => { ele["likes"] = [] });
                        // if (!element.data[0]["postId"]) delete element
                    });
                    return resolve({ message: req.i18n.__('GetProfileChannels')['200'], data: d });
                }
            });
        });
    };

    getBusinessPostTypeLabel()
        .then(() => { return getUserData() })
        .then(() => { return findMemberId() })
        .then(() => { return getFolloweeIds() })
        .then(() => { return getChannels() })
        .then((data) => {
            return res(data).code(200);
        })
        .catch((err) => { 
            console.log("err : ",err);            
            return res({ message: req.i18n.__('genericErrMsg')['204'], }).code(204); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetProfileChannels']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        // 500: { message: Joi.any().default(local['genericErrMsg']['500']) },
    }
};//swagger response code

module.exports = { queryValidator, response, handler, paramsValidator };