'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const postsCollection = require("../../../../models/postES");
const userCollection = require("../../../../models/userList");
const followCollection = require("../../../../models/follow");


const validator = Joi.object({
    "skip": Joi.string().description('skip'),
    "limit": Joi.string().description('limit'),
    "userName": Joi.string().description('userName without @ symbole')
}).unknown();
const params = Joi.object({
})

const handler = (req, res) => {
    var userName = req.query.userName, userId = "";
    var limit = parseInt(req.query.limit) || 20;
    var offset = parseInt(req.query.skip) || 0;
    var followeeIds = [], followStatus = {};

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }


    function getUserId() {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ "userName": userName, userStatus: 1 }, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    userId = ObjectID(result._id.toString());
                    if (result && result.userStatus && result.userStatus == 2) {
                        return reject({ message: req.i18n.__('GetProfileMemberPosts')['403'], "code": 403 });
                    } else {
                        return resolve();
                    }
                    return resolve(true);
                }
            });
        });
    }
    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(userId), "end": false, "type.status": 1, "followee": { "$nin": userData.blocked || [] } };
            followCollection.Select(condition, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        followeeIds.push(ObjectID(element.followee));
                        followStatus[element.followee] = element.type
                    }, this);
                    followeeIds.push(userId);
                    return resolve(true);
                }
            });
        });
    }
    function getPost() {
        return new Promise((resolve, reject) => {

            let condition = {
                from: offset, size: limit,
                "query": {
                    "nested": {
                        "path": "mentionedUsers",
                        "query": {
                            "match": {
                                "mentionedUsers.userId": userId.toString()
                            }
                        }
                    }
                }
            };
            postsCollection.CustomeSelect(condition, userData.follow, userId.toString(), (err, result) => {
                if (err) {
                    console.log("error ", err)
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    
                    result.forEach(function (element) {
                        element["followStatus"] = (followStatus[element["userId"]]) ? followStatus[element["userId"]]["status"] : 0;
                    }, this);
                    let responseobj = { code: 200, message: req.i18n.__('GetHome')['200'], data: result };
                    return resolve(responseobj);
                }
            });
        });
    }

    getUserData()
        .then(() => { return getUserId(); })
        .then(() => { return getFolloweeIds(); })
        .then(() => { return getPost(); })
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); });
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetMentionUserPost']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
        403: { message: Joi.any().default(local['GetMentionUserPost']['403']) }
    }
}//swagger response code

module.exports = { validator, handler, response, params };