'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId

const local = require('../../../../locales');
const postCollection = require("../../../../models/posts");
const followCollection = require("../../../../models/follow");
const channelCollection = require("../../../../models/channel");
const userCollection = require("../../../../models/userList");
const userListESCollection = require("../../../../models/userListES");

/**
 * @description API to user data for profile page
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const handler = (req, res) => {

    var userId = ObjectID(req.auth.credentials._id);


    const updateStatusInPostCollection = () => {
        return new Promise((resolve, reject) => {

            postCollection.Update({ userId: userId }, { "postStatus": 2 }, (err) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    return resolve(true);
                }
            });
        });
    };
    const updateStatusInFollowCollection = () => {
        return new Promise((resolve, reject) => {
            followCollection.Update({ "$or": [{ "followee": userId }, { "follower": userId }] },
                {
                    "end": true, "type": {
                        "status": 0,
                        "message": "unfollowed",
                        "deletedUser": userId
                    }
                }, (err) => {
                    if (err) {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    } else {

                        return resolve(true);
                    }
                })
        });
    };
    const updateInChannelCollection = () => {
        return new Promise((resolve, reject) => {

            channelCollection.Update({ userId: userId }, { "channelStatus": 2, channelStatusText: "inactive" }, (err) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    return resolve(true);
                }
            });
        });
    };
    const updateInUserCollection = () => {
        return new Promise((resolve, reject) => {

            userCollection.Update({ _id: userId }, { "userStatus": 2, "profileStatus": 2, "profileStatusText": "inactive" }, (err) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    return resolve(true);
                }
            });
            userListESCollection.Update(userId.toString(), { "userStatus": 2, "userStatus": "deleted" }, (err) => {
                if (err) {
                    console.log("err : ", err);
                }
            });
        });
    };


    updateStatusInPostCollection()
        .then(() => { return updateStatusInFollowCollection() })
        .then(() => { return updateInChannelCollection() })
        .then(() => { return updateInUserCollection() })
        .then((data) => { return res({ message: data.message }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['DeleteUser']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = {
    response, handler
};