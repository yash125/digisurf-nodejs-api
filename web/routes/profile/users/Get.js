'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;//to convert stringId to mongodb's objectId
var QRCode = require('qrcode')

const local = require('../../../../locales');
var uploadFile = require("../../../../library/uploadImage").upload;
var userListCollection = require('../../../../models/userList')
const followCollection = require("../../../../models/follow");
const channelCollection = require("../../../../models/channel");
var postsCollection = require("../../../../models/posts");
var bussinessCategory = require('../../../../models/bussinessCategory')

/**
 * @description API to user data for profile page
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const queryValidator = Joi.object({
    skip: Joi.number().description("skip"),
    limit: Joi.number().description("limit")
}).unknown();

const handler = (req, res) => {

    QRCode.toDataURL(req.auth.credentials._id.toString(), function (err, url) {
        uploadFile(url, "qr_code/" + req.auth.credentials._id.toString());
    });
    var dataToSend = {}, followerCount = 0, followeeCount = 0, postCount = 0, privateChannel = 0, publicChannel = 0;
    var totalLike = 0;

    const userId = ObjectID(req.auth.credentials._id);

    var HTTP_500 = { message: req.i18n.__('genericErrMsg')['500'], code: 500 };
    var HTTP_204 = { message: req.i18n.__('genericErrMsg')['204'], code: 204 };



    function getLatestData() {
        return new Promise((resolve, reject) => {


            

            postsCollection.Aggregate([
                { "$match": { "userId": userId, postStatus: 1 } },
                {
                    "$group": {
                        _id: "$userId",
                        totalLikes: { "$sum": "$likesCount" }
                    }
                }
            ], (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result && result[0] && result[0].totalLikes) {
                    totalLike = result[0].totalLikes;
                }
                postsCollection.GetPostCount({ userId: userId, "postStatus": 1, channelId: "" }, (err, result) => {
                    if (err) {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    } else {
                        postCount = result;
                    }
                    followCollection.GetFolloweeCount(userId, (err, result) => {
                        if (err) {
                            return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                        } else {
                            followeeCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                        }
                        channelCollection.Select({ userId: userId, "channelStatus": 1 }, (err, data) => {
                            if (err) {
                                return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                            } else if (data && data.length) {
                                privateChannel = data.length - data.filter(e => e.private == true).length
                                publicChannel = data.length - data.filter(e => e.private == false).length
                            }
                            return resolve(true);
                        });
                    });
                    
                });
            });

            followCollection.GetFollowerCount(userId, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    followerCount = (result[0] && result[0]["total"]) ? result[0]["total"] : 0;
                }
            });
          
        });
    }
    
    

    var getUserDetails = () => {
        return new Promise((resolve, reject) => {

            userListCollection.UpdateById(userId.toString(), {
                "count.totalChannel": privateChannel + publicChannel,
                "count.publicChannel": publicChannel,
                "count.privateChannel": privateChannel,
                "count.postsCount": postCount,
                "count.followeeCount": followeeCount,
                "count.followerCount": followerCount

            }, (e) => { console.error((e) ? e : "") });


            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject(HTTP_500);
                } else if (result) {
                    dataToSend = {
                        "_id": req.auth.credentials._id,
                        "number": result.countryCode+result.mobile,
                        "countryCode": result.countryCode,
                        "userName": result.userName,
                        "firstName": result.firstName,
                        "lastName": result.lastName,
                        "profilePic": result.profilePic || "",
                        "profileCoverImage": result.profileCoverImage || "",
                        "qrCode": result.qrCode.url,
                        "status": result.status || "",
                        "private": (result.private) ? 1 : 0,
                        "referralCode": result.referralCode,
                        "starChatId": result.starChatId || result.userName,
                        "email": (result && result.email) ? result.email : "",
                        "followStatus": 0,
                        "countryName": result.countryName || "",
                        "profileVideo": result.profileVideo,
                        "profileVideoThumbnail": result.profileVideoThumbnail,
                        "followers": followerCount,
                        "following": followeeCount,
                        "postsCount": postCount,
                        "isStar": (result.starRequest && result.starRequest.starUserProfileStatusCode == 4),
                        "starRequest": (result.starRequest) ? result.starRequest : {},
                        "businessProfile": (result.businessProfile) ? result.businessProfile : [],
                        "isActiveBusinessProfile": (result.isActiveBusinessProfile) ? true : false,
                        "isBusinessProfileApproved": (result.businessProfile && result.businessProfile.find(e => e.statusCode == 1)) ? true : false,
                        "totalLike": totalLike
                    }
                    if (dataToSend["starRequest"] && result.verified) {
                        dataToSend["starRequest"]["description"] = result.verified.description || "";
                        dataToSend["starRequest"]["emailId"] = result.verified.emailId || "";
                        dataToSend["starRequest"]["phoneNumber"] = result.verified.phoneNumber || "";
                        dataToSend["starRequest"]["countryCode"] = result.verified.countryCode || "";
                        dataToSend["starRequest"]["number"] = result.verified.number || "";
                    }
                    try {
                        dataToSend["isStar"] = (dataToSend["starRequest"] && dataToSend["starRequest"]["starUserProfileStatusCode"] == 4) ? true : false;
                    } catch (error) {
                        console.log(error);
                        dataToSend["isStar"] = false
                    }
                    return resolve(true);
                } else {
                    return reject(HTTP_204);
                }
            });
        });
    }

    var getBusinessType = () => {
        return new Promise((resolve, reject) => {
            try {
                if (!dataToSend.businessProfile || dataToSend.businessProfile[0].businessCategoryId == 'undefined') {
                    return resolve();
                }

                bussinessCategory.Select({ _id: ObjectID(dataToSend.businessProfile[0].businessCategoryId.toString()) }, (err, result) => {
                    if (err) {
                        return reject(HTTP_500);
                    } else if (result && result.length) {
                        dataToSend.businessProfile[0]["businessCategory"] = result[0].type || "";
                    }
                    return resolve();
                });
            } catch (error) {
                return resolve();
            }
        });
    }

    getLatestData()
        .then(getUserDetails)
        .then(getBusinessType)
        .then(() => {
            console.log("dataToSend ",dataToSend)
            return res({ message: req.i18n.__('GetProfileUsers')['200'], data: [dataToSend] }).code(200);
        })
        .catch((data) => {
            return res({ message: data.message }).code(data.code);
        });
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetProfileUsers']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = {
    response,
    handler,
    queryValidator
};