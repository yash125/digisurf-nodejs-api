'use strict'
const Joi = require("joi");

const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const postsCollection = require("../../../../models/posts");
const userCollection = require("../../../../models/userList");
const followCollection = require("../../../../models/follow");


const validator = Joi.object({
    "skip": Joi.string().description('skip'),
    "limit": Joi.string().description('limit'),
    "userName": Joi.string().description('userName without @ symbole')
}).unknown();

const handler = (req, res) => {
    var userName = req.query.userName, userId = "";
    var limit = parseInt(req.query.limit) || 20;
    var offset = parseInt(req.query.skip) || 0;
    var followeeIds = [], followStatus = {};



    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }


    function getUserId() {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ "userName": userName }, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    userId = ObjectID(result._id.toString());
                    return resolve(true);
                }
            });
        });
    }
    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(userId), "end": false, "type.status": 1, "followee": { "$nin": userData.blocked || [] } };
            followCollection.Select(condition, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        followeeIds.push(ObjectID(element.followee));
                        followStatus[element.followee] = element.type
                    }, this);
                    followeeIds.push(userId);
                    return resolve(true);
                }
            });
        });
    }
    function getPost() {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$unwind": "$mentionedUsers" },
                { "$match": { "mentionedUsers.userId": userId, "postStatus": 1 } },
                { "$sort": { "timeStamp": -1 } },
                { "$lookup": { "from": "customer", "localField": "userId", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                { "$lookup": { "from": "channel", "localField": "channelId", "foreignField": "_id", "as": "channleData" } },
                { "$unwind": { "path": "$channleData", "preserveNullAndEmptyArrays": true } },
                // { "$match": { "$or": [{ "channelId": null }, { "channleData.confirm.userId": userId }, { "channleData.userId": userId }] } },
                { "$lookup": { "from": "category", "localField": "categoryId", "foreignField": "_id", "as": "categoryData" } },
                { "$unwind": { "path": "$categoryData", "preserveNullAndEmptyArrays": true } },
                { "$lookup": { "from": "dublyAudio", "localField": "musicId", "foreignField": "_id", "as": "musicData" } },
                { "$unwind": { "path": "$musicData", "preserveNullAndEmptyArrays": true } },
                {
                    "$project": {
                        "postId": '$_id', "imageUrl1": '$imageUrl1', "thumbnailUrl1": '$thumbnailUrl1',
                        "mediaType1": '$mediaType1', "createdOn": '$createdOn', "timeStamp": '$timeStamp',
                        "title": '$title', "musicData": "$musicData", "shareCount": "$shareCount",
                        "userId": '$userId', "phoneNumber": '$number', "profilepic": '$userData.profilePic',
                        "userName": '$userData.userName', "starRequest": '$userData.starRequest', "likes": '$likes',

                        "channelImageUrl": { '$ifNull': ["$channleData.channelImageUrl", ""] },
                        "categoryName": { '$ifNull': ["$categoryData.categoryName", ""] },
                        "categoryUrl": { '$ifNull': ["$categoryData.categoryActiveIconUrl", ""] },
                        "channelName": { '$ifNull': ["$channleData.channelName", ""] },
                        "categoryId": { '$ifNull': ['$categoryId', ""] },
                        "channelId": { '$ifNull': ['$channelId', ""] },
                        "city": { "$ifNull": ['$city', ""] },
                        "totalComments": { "$size": { '$ifNull': ['$comments', []] } },
                        "distinctViews": { "$size": { '$ifNull': ['$distinctViews', []] } },
                        "location": { '$ifNull': ['$location', ""] },
                        "place": { '$ifNull': ['$place', ""] },
                        "comments": { "$ifNull": ['$comments', []] },
                        "hashTags": { '$ifNull': ['$hashTags', []] },
                        "likesCount": { '$ifNull': ['$likesCount', 0] },
                        "imageUrl1Width": { "$ifNull": ["$imageUrl1Width", null] },
                        "imageUrl1Height": { "$ifNull": ["$imageUrl1Height", null] },
                    }
                }, { "$skip": offset }, { "$limit": limit },
            ];

            postsCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {

                    result.forEach(function (element) {
                        element["followStatus"] = (followStatus[element["userId"]]) ? followStatus[element["userId"]]["status"] : 0;
                        element["liked"] = (element["likes"]) ? element["likes"].map(id => id.toString()).includes(userId.toString()) : false;
                        element["isStar"] = (element["starRequest"] && element["starRequest"]["starUserProfileStatusCode"] == 4) ? true : false;

                        delete element["likes"];
                    }, this);
                    let responseobj = { code: 200, message: req.i18n.__('GetHome')['200'], data: result };
                    return resolve(responseobj);
                }
            });
        });
    }

    getUserData()
        .then(() => { return getUserId(); })
        .then(() => { return getFolloweeIds(); })
        .then(() => { return getPost(); })
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => { return res({ message: error.message }).code(error.code); });
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetMentionUserPost']['200']), data: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code

module.exports = { validator, handler, response };