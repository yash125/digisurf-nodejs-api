let GetAPI = require('./Get');
let DeleteAPI = require('./Delete');
let GetListAPI = require('./GetListFromEs'); //require('./GetList');
let headerValidator = require("../../../middleware/validator")

module.exports = [
    {
        method: 'DELETE',
        path: '/profile/users',
        handler: DeleteAPI.handler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'profile'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: DeleteAPI.response
        }
    },
    {
        method: 'GET',
        path: '/profile/users',
        handler: GetAPI.handler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'profile'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
    {
        method: 'GET',
        path: '/profile/users/mention',
        handler: GetListAPI.handler,
        config: {
            description: 'This API will be used by the passport feature to update the user’s location so that the future search runs off this new location',
            tags: ['api', 'profile'],
            auth: 'userJWT',
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetListAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetListAPI.response
        }
    }
];