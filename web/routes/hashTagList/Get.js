const Joi = require("joi");
const local = require('../../../locales');
const hashtags_related_posts = require("../../../models/hashtags_related_posts");
const hashTagCollection = require("../../../models/hashTags");
const GET_REDIS_HASH_TAGS = "GET_REDIS_HASH_TAGS";
const SET_EXPIRE = 120;

const validator = Joi.object({
    hashtag: Joi.string().allow("").default("").description("hashtag to be searched"),
    skip: Joi.string().description('skip'),
    limit: Joi.string().description('limit'),
}).unknown();


const handler = (req, res) => {

    var redis = require("redis");
    try {

        var redisClient = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_IP, {
            retry_strategy: function (options) {
                // console.log('retry strategy check');
                // console.log(options);
                // reconnect after
                return 10000;
            }
        });
        redisClient.auth(process.env.REDIS_PASS);

        redisClient.on('error', function (err) {
            // console.log({ err: err }, 'Listener.redis.client error: %s', err);
            redisClient.quit();
            let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
            return res(responseobj).code(503);

        });
    } catch (error) {
        // let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
        // return res(responseobj).code(503);
    }






    let hashTag = (req.query.hashtag) ? req.query.hashtag.trim().toLowerCase() : "";
    hashTag = (hashTag[0] == "#") ? hashTag : "#" + hashTag;
    let skip = parseInt(req.query.skip) || 0;
    let limit = parseInt(req.query.limit) || 20;

    //default Image for HashTag : http://res.cloudinary.com/dafszph29/image/upload/v1556876492/fnk4iujlanlmnkbjjlnp.png

    if (hashTag != "" && hashTag != "#") {
        // "statusCode": { "$ne": 2 }

        redisClient.get(GET_REDIS_HASH_TAGS + "_" + hashTag, (err, data) => {
            if (data != null && data != '[]') {
                let responseobj = { message: req.i18n.__('GetHashTagList')['200'], data: JSON.parse(data) };
                saveDataInRedis(GET_REDIS_HASH_TAGS + "_" + hashTag)
                return res(responseobj).code(200);
            } else {
                let condition = [
                    { "$match": { "_id": { "$ne": "" } } },
                    { "$match": { "_id": { "$regex": hashTag, "$options": "i" } } },
                    { "$lookup": { "from": "posts", "localField": "_id", "foreignField": "hashTags", "as": "data" } },
                    { "$unwind": "$data" },
                    { "$match": { "data.postStatus": 1 } },
                    { "$lookup": { "from": "customer", "localField": "data.userId", "foreignField": "_id", "as": "data" } },
                    { "$unwind": "$data" },
                    { "$match": { "$or": [{ "data.private": 0 }, { "data.private": false }] } },
                    {
                        "$group": {
                            _id: "$_id",
                            "hashTags": { "$first": "$_id" },
                            "image": { "$first": "$image" },
                            "totalPublicPosts": { "$sum": 1 },
                        }
                    },
                    { "$match": { "totalPublicPosts": { "$ne": 0 }, "hashTags": { "$ne": null } } },
                    { "$sort": { "totalPublicPosts": -1 } },
                    { "$skip": skip }, { "$limit": limit }
                ];
                hashTagCollection.Aggregate(condition, (e, d) => {
                    if (e) {
                        redisClient.quit();
                        let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
                        return res(responseobj).code(500);
                    } else if (d && d.length) {
                        d.forEach(element => {
                            element["totalPublicPosts"] = (element["totalPublicPosts"]) ? element["totalPublicPosts"] : 0;
                            element["image"] = (element["image"]) ? element["image"] : process.env.DEFAULT_HASH_TAG_IMAGE;
                        });
                        redisClient.setex(GET_REDIS_HASH_TAGS + "_" + hashTag, SET_EXPIRE, JSON.stringify(d), (err, result) => {
                            if (err) console.log("In redis : ALAOPOO12WOSD ", err)

                            redisClient.quit();
                        })
                        let responseobj = { message: req.i18n.__('GetHashTagList')['200'], data: d };
                        return res(responseobj).code(200);
                    } else {
                        redisClient.setex(GET_REDIS_HASH_TAGS + "_" + hashTag, SET_EXPIRE, JSON.stringify([]), (err, result) => {
                            if (err) console.log("In redis : ALAOPO34WOSD ", err)
                            
                            redisClient.quit();
                        })
                        let responseobj = { message: req.i18n.__('genericErrMsg')['204'] };
                        return res(responseobj).code(204);
                    }
                });
            }
        });
    } else {
        redisClient.get(GET_REDIS_HASH_TAGS, (err, data) => {
            if (data != null && data != '[]') {
                let responseobj = { message: req.i18n.__('GetHashTagList')['200'], data: JSON.parse(data) };
                redisClient.quit();
                saveDataInRedis(GET_REDIS_HASH_TAGS)
                return res(responseobj).code(200);
            } else {
                let condition = [
                    { "$match": { "_id": { "$ne": "" } } },
                    { "$lookup": { "from": "posts", "localField": "_id", "foreignField": "hashTags", "as": "data" } },
                    { "$unwind": "$data" },
                    { "$match": { "data.postStatus": 1 } },
                    { "$lookup": { "from": "customer", "localField": "data.userId", "foreignField": "_id", "as": "data" } },
                    { "$unwind": "$data" },
                    { "$match": { "$or": [{ "data.private": 0 }, { "data.private": false }] } },
                    {
                        "$group": {
                            _id: "$_id",
                            "hashTags": { "$first": "$_id" },
                            "image": { "$first": "$image" },
                            "totalPublicPosts": { "$sum": 1 },
                        }
                    },
                    { "$match": { "totalPublicPosts": { "$ne": 0 }, "hashTags": { "$ne": null } } },
                    { "$sort": { "totalPublicPosts": -1 } },
                    { "$skip": skip }, { "$limit": limit }
                ];
                hashTagCollection.Aggregate(condition, (e, d) => {
                    if (e) {
                        let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
                        return res(responseobj).code(500);
                    } else if (d && d.length) {
                        d.forEach(element => {
                            element["totalPublicPosts"] = (element["totalPublicPosts"]) ? element["totalPublicPosts"] : 0;
                            element["image"] = (element["image"]) ? element["image"] : process.env.DEFAULT_HASH_TAG_IMAGE;
                        });
                        redisClient.setex(GET_REDIS_HASH_TAGS, SET_EXPIRE, JSON.stringify(d), (err, result) => {
                            if (err) console.log("In redis : ALAOPOO12WOSD ", err)
                            redisClient.quit();
                        })
                        let responseobj = { message: req.i18n.__('GetHashTagList')['200'], data: d };
                        return res(responseobj).code(200);
                    } else {
                        redisClient.setex(GET_REDIS_HASH_TAGS, SET_EXPIRE, JSON.stringify([]), (err, result) => {
                            if (err) console.log("In redis : ALAOPO34WOSD ", err)
                            redisClient.quit();
                        })
                        let responseobj = { message: req.i18n.__('genericErrMsg')['204'] };
                        return res(responseobj).code(204);
                    }
                });
            }
        });
    }
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetHashTagList']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { validator, handler, response };




var saveDataInRedis = (KeyToSave) => {

    var redis = require("redis");
    try {

        var redisClient = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_IP, {
            retry_strategy: function (options) {
                // console.log('retry strategy check in pubsub');
                // console.log(options);
                // reconnect after
                return 10000;
            }
        });
        redisClient.auth(process.env.REDIS_PASS);

        redisClient.on('error', function (err) {
            // console.log({ err: err }, 'Listener.redis.client error: %s', err);
             redisClient.quit();
            let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
            return res(responseobj).code(503);

        });
    } catch (error) {
        // let responseobj = { message: req.i18n.__('genericErrMsg')['500'] };
        // return res(responseobj).code(503);
    }


    let condition = [
        { "$match": { "_id": { "$ne": "" } } },
        { "$lookup": { "from": "posts", "localField": "_id", "foreignField": "hashTags", "as": "data" } },
        { "$unwind": "$data" },
        { "$match": { "data.postStatus": 1 } },
        { "$lookup": { "from": "customer", "localField": "data.userId", "foreignField": "_id", "as": "data" } },
        { "$unwind": "$data" },
        { "$match": { "$or": [{ "data.private": 0 }, { "data.private": false }] } },
        {
            "$group": {
                _id: "$_id",
                "hashTags": { "$first": "$_id" },
                "image": { "$first": "$image" },
                "totalPublicPosts": { "$sum": 1 },
            }
        },
        { "$match": { "totalPublicPosts": { "$ne": 0 }, "hashTags": { "$ne": null } } },
        { "$sort": { "totalPublicPosts": -1 } },
        { "$limit": 1000 }
    ]
    hashTagCollection.Aggregate(condition, (e, d) => {
        if (e) {
            console.log("In redis : ALOPWOSD ", e)
        } else if (d && d.length) {
            redisClient.setex(KeyToSave, SET_EXPIRE, JSON.stringify(d), (err, result) => {
                if (err) console.log("In redis : ALAOPOOPWOSD ", err)
                redisClient.quit();
            })
        } else {
            redisClient.setex(KeyToSave, SET_EXPIRE, JSON.stringify([]), (err, result) => {
                if (err) console.log("In redis : ALAAAOPWOSD ", err)
                redisClient.quit();
            })
        }
    });
}