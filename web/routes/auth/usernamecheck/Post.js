const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
let userCollection = require("../../../../models/userList");

//API validators 
const validator = Joi.object({
    username: Joi.string().required().description("username")
}).required();

/**
 * @method Post usernameCheck
 * @description This api check provided username is already in use or not
 * 
 * @headers authentication 
 * @headers lang
 * 
 * @param {string} username
 * 
 * @author DipenAhir
 * @since 09-July-2019
 */

//Start API handler
const handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);

    // check username already exists 
    const get = () => {
        return new Promise((resolve, reject) => {

            let username = req.payload.username.trim().toLowerCase(); //trim username with lowercase
            let condition = { userName: username, _id: { "$ne": userId } }; //condition to get username

            //select data from db
            userCollection.Select(condition, (err, result) => {
                if (err) {
                    //return 500 responce : if have any issues in Aggregate function
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result && result.length) {
                    //return 409 responce : if user already in db
                    return resolve({ code: 409, message: req.i18n.__('PostUserNameCheck')['409'] });
                } else {
                    //return 200 responce : user not exists
                    return resolve({ code: 200, message: req.i18n.__('PostUserNameCheck')['200'] });
                }
            });
        });
    };

    get()
        .then((data) => { return res({ data: data.message }).code(data.code); })
        .catch((data) => { return res({ data: data.message }).code(data.code); })
};//End API handler

const response = {
    status: {
        200: { message: Joi.any().default(local['PostUserNameCheck']['200']).required().example(local['PostUserNameCheck']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']).required().example(local['PostUserNameCheck']['400']) },
        409: { message: Joi.any().default(local['PostUserNameCheck']['409']).required().example(local['PostUserNameCheck']['409']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']).required().example(local['PostUserNameCheck']['500']) }
    }
};//swagger response code

module.exports = {
    handler,
    validator,
    response
};