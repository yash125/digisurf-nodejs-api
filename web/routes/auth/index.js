let headerValidator = require("../../middleware/validator")
let PostAPI = require('./usernamecheck/Post');

module.exports = [
    {
        method: 'POST',
        path: '/auth/usernameCheck',
        handler: PostAPI.handler,
        config: {
            description: `This api check provided username is already in use or not`,
            tags: ['api', 'auth'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    }
];