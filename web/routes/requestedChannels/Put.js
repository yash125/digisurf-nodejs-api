const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const channelCollection = require("../../../models/channel");
const userListCollection = require("../../../models/userList");
const local = require('../../../locales');
const fcm = require("../../../library/fcm");

const ObjectID = require('mongodb').ObjectID;


let handler = (req, res) => {

    function sendfcmIfRequired() {
        return new Promise((resolve, reject) => {
            if (req.payload.isAccepted && (req.payload.isAccepted == true || req.payload.isAccepted == 1)) {
                channelCollection.SelectOne({ _id: ObjectID(req.payload.channelId) }, (err, result) => {
                    if (result && result._id && result.channelName) {
                        let request = {
                            fcmTopic: req.payload.userId,
                            // action: 1,
                            pushType: 2,
                            title: process.env.APP_NAME,
                            msg: `Your request for subscription of channel ${result.channelName} got accepted`,
                            // data: { "type": "postLiked" },
                            deviceType: 1
                        }
                        fcm.notifyFcmTpic(request, () => { });
                    }
                })
            } else {
                return resolve()
            }
        });
    }


    function applyOnChannal() {
        return new Promise((resolve, reject) => {

            let condition = { _id: ObjectID(req.payload.channelId) };
            let dataToPull = { "request": { "userId": ObjectID(req.payload.userId) } };

            channelCollection.UpdateByIdWithPull(condition, dataToPull, (err) => {
                if (err) return reject(err);

                if (req.payload.isAccepted) {

                    dataToPull = { "confirm": { "userId": ObjectID(req.payload.userId), timestamp: new Date().getTime() } };

                    userListCollection.UpdateByIdWithAddToSet({ _id: req.payload.userId }, { "subscribeChannels": ObjectID(req.payload.channelId) }, (e) => { if (e) logger.error("OAKSDOKASOK : ", e) })
                    channelCollection.UpdateByIdWithPush(condition, dataToPull, (err, result) => {
                        if (err) return resolve(result);
                        return resolve(result)
                    });

                } else {

                    dataToPull = { "reject": { "userId": ObjectID(req.payload.userId), timestamp: new Date().getTime() } };

                    channelCollection.UpdateByIdWithPush(condition, dataToPull, (err, result) => {
                        if (err) return resolve(result);
                        return resolve(result)
                    });
                }


            });

        });
    }

    applyOnChannal()
        .then(sendfcmIfRequired)
        .then(() => { return res({ message: req.i18n.__('PutRequestedChannels')['200'] }).code(200); })
        .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204); })
};

const validator = Joi.object({
    channelId: Joi.string().required().description("channelId"),
    userId: Joi.string().required().description("userId"),
    isAccepted: Joi.boolean().required().description("type"),
}).required();

const response = {
    status: {
        200: { message: Joi.any().default(local['PutRequestedChannels']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response, validator };