const Joi = require("joi");

const Promise = require('promise');
const channelCollection = require("../../../models/channel");
const local = require('../../../locales');
const ObjectID = require('mongodb').ObjectID;


let handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);

    function getChannels() {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "userId": userId } },
                { "$unwind": "$request" },
                {
                    "$project": {
                        "channelName": 1, "channelCreatedOn": "$createdOn", "private": 1, "channelImageUrl": 1,
                        "requestedUser": "$request.userId", "requestedTimestamp": "$request.timestamp"
                    }
                },
                { "$lookup": { "from": "customer", "localField": "requestedUser", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                {
                    "$group": {
                        "_id": "$_id",
                        "channelName": { "$first": "$channelName" },
                        "private": { "$first": "$private" },
                        "channelImageUrl": { "$first": "$channelImageUrl" },
                        "channelCreatedOn": { "$first": "$channelCreatedOn" },
                        "requestedUserList": {
                            "$push": {
                                "_id": "$userData._id", "countryCode": "$userData.countryCode", "number": "$userData.mobile",
                                "profilePic": "$userData.profilePic", "userName": "$userData.userName",
                                "requestedTimestamp": "$requestedTimestamp"
                            }
                        }
                    }
                }
            ];
            channelCollection.Aggregate(condition, (err, result) => {
                if (err) return reject(result);

                return resolve(result)
            });

        });
    }

    getChannels()
        .then((data) => { return res({ message: req.i18n.__('GetRequestedChannels')['200'], data: data }).code(200); })
        .catch(() => {
            return res({
                message: req.i18n.__('genericErrMsg')['500']
            }).code(500);
        })
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetRequestedChannels']['200']), data: Joi.any()
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response };