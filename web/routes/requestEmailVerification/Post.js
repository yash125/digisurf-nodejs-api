'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const userCollection = require("../../../models/userList");
const verifyResetEmail = require("../../../models/verifyResetPassword");
var gRPC_Email_service = require("../../../library/gRPC/Email");
const emailVerificationLogsCollection = require("../../../models/emailVerificationLogs");


/**
 * @description API to emailVerification a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} userId
 * @property {string} postId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const payloadValidator = Joi.object({
    starUserEmailId: Joi.string().required().description("starUserEmailId"),
    userId: Joi.string().allow("").description("userId"),
    type: Joi.number().default(1).description("starUserEmailId : 1 for star user, 2 for verify email, 3 : send email on new email id "),
}).unknown();

const handler = (req, res) => {
    // const userId = ObjectID(req.auth.credentials._id);
    var emailId = req.payload.starUserEmailId.toLowerCase();
    var randomCode = (process.env.NODE_ENV == "production") ? Math.floor(1000 + Math.random() * 9000) : 1111;
    
    const checkEmailIdInMongoDB = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: { "$ne": ObjectID(req.payload.userId) }, $or: [{ "email.id": emailId, "userStatus": 1 }, { "starRequest.starUserEmail": emailId, "userStatus": 1 }] }, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result && result._id) {
                    if (req.payload.type == 3) {
                        return reject({ code: 409, message: req.i18n.__('genericErrMsg')['409'] })
                    } else {
                        return resolve(result);
                    }
                } else {
                    return resolve(result);
                    // if (req.payload.type == 3) {
                    //     userCollection.SelectOne({ _id: ObjectID(req.payload.userId) }, (err, result) => {
                    //         if (result && result._id) {
                    //             return resolve(result);
                    //         } else {
                    //             return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                    //         }
                    //     });
                    // } else {
                    //     return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                    // }
                }
            });
        });
    };

    const sendVerificationMail = (result) => {
        return new Promise((resolve, reject) => {

            userCollection.SelectOne({
                "$or": [{ _id: ObjectID(req.payload.userId) }, { "email.id": req.payload.starUserEmailId },
                { "starRequest.starUserEmail": req.payload.starUserEmailId }]
            }, (err, result) => {
                if (err) {
                    reject(err);
                } else if (req.payload.type == 2) {

                    try {
                        let sendMailParams = {
                            userName: result.userName,
                            trigger: "Email Verification",
                            "to": emailId,
                            "subject": "Verification code for " + process.env.APP_NAME,
                            "body": '<p> Hello ' + result.firstName + ', <br/><br/>Your email verification code is : ' + randomCode + '.' + '<br/>Please enter this on the app to set a new password..' + '<br/><br/><br/>Cheers, <br/> Team ' + process.env.APP_NAME + '.</p>'
                        };
                        gRPC_Email_service.sendEmail(sendMailParams)
                    } catch (error) {
                        console.error("error AWWLOP:", error)
                    }



                    let dataToInsert = {
                        // userId: userId,
                        email: emailId,
                        code: randomCode,
                        timestamp: new Date().getTime(),
                        timestampMetaBase: new Date()
                    }
                    verifyResetEmail.Insert(dataToInsert, (e) => {
                        if (e) {
                            logger.error("OASDLASKMASDD : ", e);
                        }
                    });


                } else if (req.payload.type == 1 || req.payload.type == 3) {


                    try {
                        let sendMailParams = {
                            userName: result.userName,
                            trigger: "Email Verification",
                            "to": emailId,
                            "subject": "Email Verification code for " + process.env.APP_NAME,
                            "body": '<p> Hello ' + result.firstName + ', <br/><br/>Your email verification code is : ' + randomCode + '.' + '<br/>Please enter this on the app to confirm your email address.' + '<br/><br/><br/>Cheers, <br/> Team ' + process.env.APP_NAME + '.</p>'
                        };

                        gRPC_Email_service.sendEmail(sendMailParams);
                    } catch (error) {
                        console.error("error AWWAALOP:", error)
                    }

                    let dataToInsert = {
                        // userId: userId,
                        emailId: emailId,
                        otp: randomCode,
                        timestamp: new Date().getTime(),
                        timestampMetaBase: new Date()
                    }
                    emailVerificationLogsCollection.Insert(dataToInsert, (e) => {
                        if (e) {
                            logger.error("OAAKDSDLASKMASDD : ", e);
                        }
                    });
                    let dataToInsert1 = {
                        // userId: userId,
                        email: emailId,
                        code: randomCode,
                        timestamp: new Date().getTime(),
                        timestampMetaBase: new Date()
                    }
                    verifyResetEmail.Insert(dataToInsert1, (e) => {
                        if (e) {
                            logger.error("OASDLASKMASDD : ", e);
                        }
                    });
                }
                return resolve({ code: 200, message: req.i18n.__('PostRequestEmailVerification')['200'] });
            });
        });
    };

    checkEmailIdInMongoDB()
        .then((data) => { return sendVerificationMail(data) })
        .then((data) => { return res({ message: data.message }).code(data.code); })
        .catch((data) => { return res({ message: data.message }).code(data.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['PostRequestEmailVerification']['200']) },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }        
    }
}//swagger response code

module.exports = { payloadValidator, handler, response };