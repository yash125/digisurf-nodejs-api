const Joi = require("joi");
const Promise = require('promise');
const local = require('../../../locales');
const musicCategoryCollection = require("../../../models/musicCategory");


const hander = (req, res) => {
    
    const getMyData = () => {
        return new Promise((resolve, reject) => {
            musicCategoryCollection.SelectWithSort({"musiccategoryStatus" : 1}, { sequenceId: -1 }, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d.length) {
                    return resolve({ message: req.i18n.__('GetMusicCategory')['200'], data: d });
                } else {
                    return resolve({ code: 204, message: req.i18n.__('GetMusicCategory')['204'] });
                }
            })
        });
    };

    getMyData()
        .then((data) => { return res(data).code(200); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetMusicCategory']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) }
    }
}//swagger response code


module.exports = { hander, response };