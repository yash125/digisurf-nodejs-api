let headerValidator = require("../../middleware/validator")
let GetAPI = require('./Get');


module.exports = [
    {
        method: 'GET',
        path: '/musicCategory',
        handler: GetAPI.hander,
        config: {
            description: `This API use for signup or login from facebook with facebook token`,
            tags: ['api', 'Music'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];