const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const postsCollection = require("../../../models/posts");
const followCollection = require("../../../models/follow");
const userListCollection = require("../../../models/userList");
const dublyAudioCollection = require("../../../models/dublyAudio");

/**
 * @description API to update a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} userId
 * @property {string} imageUrl1
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const validator = Joi.object({
    musicId: Joi.string().required().description('musicId'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();

const handler = (req, res) => {

    let musicId = ObjectID(req.query.musicId)
    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    let userId = ObjectID(req.auth.credentials._id);
    var followStatus = {}, dataToSend = [], musicData = {};

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }


    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(userId), "end": false, "followee": { "$nin": userData.blocked || [] } };
            followCollection.Select(condition, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        followStatus[element.followee] = element.type
                    }, this);
                    return resolve(true);
                }
            });
        });
    }
    const getPosts = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "musicId": musicId } },
                { "$sort": { "timeStamp": -1 } },
                { "$lookup": { "from": "customer", "localField": "userId", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                { "$lookup": { "from": "channel", "localField": "channelId", "foreignField": "_id", "as": "channleData" } },
                { "$unwind": { "path": "$channleData", "preserveNullAndEmptyArrays": true } },
                { "$match": { "$or": [{ "channelId": null }, { "channleData.confirm.userId": userId }, { "channleData.userId": userId }] } },
                { "$lookup": { "from": "category", "localField": "categoryId", "foreignField": "_id", "as": "categoryData" } },
                { "$unwind": { "path": "$categoryData", "preserveNullAndEmptyArrays": true } },
                { "$lookup": { "from": "dublyAudio", "localField": "musicId", "foreignField": "_id", "as": "musicData" } },
                { "$lookup": { "from": "dublyAudio", "localField": "musicId", "foreignField": "_id", "as": "musicDataForSize" } },
                { "$unwind": { "path": "$musicData", "preserveNullAndEmptyArrays": true } },
                {
                    "$project": {
                        "postId": '$_id', "imageUrl1": '$imageUrl1', "thumbnailUrl1": '$thumbnailUrl1',
                        "mediaType1": '$mediaType1', "createdOn": '$createdOn', "timeStamp": '$timeStamp',
                        "title": '$title', "musicData": "$musicData", "shareCount": "$shareCount",
                        "userId": '$userId', "phoneNumber": '$number', "profilepic": '$userData.profilePic',
                        "userName": '$userData.userName', "likes": '$likes',

                        "channelImageUrl": { '$ifNull': ["$channleData.channelImageUrl", ""] },
                        "categoryName": { '$ifNull': ["$categoryData.categoryName", ""] },
                        "categoryUrl": { '$ifNull': ["$categoryData.categoryActiveIconUrl", ""] },
                        "channelName": { '$ifNull': ["$channleData.channelName", ""] },
                        "categoryId": { '$ifNull': ['$categoryId', ""] },
                        "channelId": { '$ifNull': ['$channelId', ""] },
                        "city": { "$ifNull": ['$city', ""] },
                        "totalComments": { "$size": { '$ifNull': ['$comments', []] } },
                        "distinctViews": { "$size": { '$ifNull': ['$distinctViews', []] } },
                        "location": { '$ifNull': ['$location', ""] },
                        "place": { '$ifNull': ['$place', ""] },
                        "comments": { "$ifNull": ['$comments', []] },
                        "hashTags": { '$ifNull': ['$hashTags', []] },
                        "likesCount": { '$ifNull': ['$likesCount', 0] },
                        "imageUrl1Width": { "$ifNull": ["$imageUrl1Width", null] },
                        "imageUrl1Height": { "$ifNull": ["$imageUrl1Height", null] },
                    }
                }, { "$sort": { "timeStamp": -1 } }, { "$skip": offset }, { "$limit": limit },
            ];

            postsCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {

                    result.forEach(function (element) {
                        element["followStatus"] = (followStatus[element["userId"]]) ? followStatus[element["userId"]]["status"] : 0;
                        element["liked"] = (element["likes"]) ? element["likes"].map(id => id.toString()).includes(userId.toString()) : false;
                        delete element["likes"];
                    }, this);

                    dataToSend = result;

                    return resolve(true);
                }
            });
        });
    };
    const getMusicData = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "_id": musicId } },
                { "$lookup": { "from": "musicCategory", "localField": "musicCategory", "foreignField": "_id", "as": "musicCategory" } },
                { "$unwind": { "path": "$musicCategory", "preserveNullAndEmptyArrays": true } },
                {
                    "$project": {
                        "name": 1, "duration": 1, "path": 1, "imageUrl": 1, "musicCategory_id": "$musicCategory._id",
                        "musicCategoryName": "$musicCategory.name", "musicCategoryImageUrl": "$musicCategory.imageUrl",
                        "artist": 1
                    }
                },
                { "$lookup": { "from": "posts", "localField": "_id", "foreignField": "musicId", "as": "musicData" } },
                {
                    "$project": {
                        "name": 1, "duration": 1, "path": 1, "imageUrl": 1, "musicCategory_id": 1,
                        "musicCategoryName": 1, "musicCategoryImageUrl": 1, "totalVideos": { "$size": "$musicData" },
                        "artist": 1
                    }
                }
            ];

            dublyAudioCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    if (result && result[0]) {
                        musicData = (result && result[0]) ? result[0] : {};
                        musicData["isFavourite"] = (userData.favouriteMusic && userData.favouriteMusic.map(id => id.toString()).includes(musicData._id.toString())) ? 1 : 0;
                    }
                    return resolve(true);
                }
            });
        });
    };

    getUserData()
        .then(() => { return getFolloweeIds() })
        .then(() => { return getPosts() })
        .then(() => { return getMusicData() })
        .then(() => {
            return res({ message: req.i18n.__('GetPostByMusicId')['200'], data: dataToSend, musicData: musicData }).code(200);
        })
        .catch((error) => {
            console.log("error", error)
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        })
};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetPostByMusicId']['200']), data: Joi.any(), musicData: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code


module.exports = { validator, handler, response };