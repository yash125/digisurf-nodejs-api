const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const postsCollection = require("../../../models/postES");
const followCollection = require("../../../models/follow");
const userListCollection = require("../../../models/userList");
const dublyAudioCollection = require("../../../models/dublyAudio");

/**
 * @description API to update a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} userId
 * @property {string} imageUrl1
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const validator = Joi.object({
    musicId: Joi.string().required().description('musicId'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();

const handler = (req, res) => {

    let musicId = ObjectID(req.query.musicId)
    let limit = parseInt(req.query.limit) || 20;
    let skip = parseInt(req.query.offset) || 0;
    let userId = ObjectID(req.auth.credentials._id);
    var followStatus = {}, dataToSend = [], musicData = {};


    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(userId), "end": false, "followee": { "$nin": userData.blocked || [] } };
            followCollection.Select(condition, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        followStatus[element.followee] = element.type
                    }, this);
                    return resolve(true);
                }
            });
        });
    }
    const getPosts = () => {
        return new Promise((resolve, reject) => {
            let condition = {
                "must": [
                    { "match": { "postStatus": 1 } },
                    { "match": { "musicId": musicId.toString() } }
                ]
            };
            postsCollection.getPostDetails(condition, skip, limit, userData.follow, userId.toString(), (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        element["followStatus"] = (followStatus[element["userId"]]) ? followStatus[element["userId"]]["status"] : 0;
                        if (element["channelId"] && element["channelId"].length) element["isChannelSubscribed"] = (userData && userData.subscribeChannels && userData.subscribeChannels.map(e => e.toString()).includes(element.channelId.toString())) ? 1 : 0;
                    }, this);

                    dataToSend = result;

                    return resolve(true);
                }
            });
        });
    };
    const getMusicData = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                { "$match": { "_id": musicId } },
                { "$lookup": { "from": "musicCategory", "localField": "musicCategory", "foreignField": "_id", "as": "musicCategory" } },
                { "$unwind": { "path": "$musicCategory", "preserveNullAndEmptyArrays": true } },
                {
                    "$project": {
                        "name": 1, "duration": 1, "path": 1, "imageUrl": 1, "musicCategory_id": "$musicCategory._id",
                        "musicCategoryName": "$musicCategory.name", "musicCategoryImageUrl": "$musicCategory.imageUrl",
                        "artist": 1
                    }
                },
                { "$lookup": { "from": "posts", "localField": "_id", "foreignField": "musicId", "as": "musicData" } },
                { "$unwind": "$musicData" },
                { "$match": { "musicData.postStatus": 1 } },
                {
                    "$project": {
                        "name": 1, "duration": 1, "path": 1, "imageUrl": 1,
                        "musicCategory_id": 1,
                        "musicCategoryName": 1, "musicCategoryImageUrl": 1,
                        "artist": 1
                    }
                },
                {
                    "$group": {
                        "_id": "$_id",
                        "name": { "$last": "$name" },
                        "duration": { "$last": "$duration" },
                        "path": { "$last": "$path" }, "imageUrl": { "$last": "$imageUrl" },
                        "musicCategory_id": { "$last": "$musicCategory_id" },
                        "musicCategoryName": { "$last": "$musicCategoryName" },
                        "musicCategoryImageUrl": { "$last": "$musicCategoryImageUrl" },
                        "totalVideos": { "$sum": 1 },
                        "artist": { "$first": "$artist" }
                    }
                }
            ];

            dublyAudioCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    if (result && result[0]) {
                        musicData = (result && result[0]) ? result[0] : {};
                        musicData["isFavourite"] = (userData.favouriteMusic && userData.favouriteMusic.map(id => id.toString()).includes(musicData._id.toString())) ? 1 : 0;
                        if (musicData["artist"] == null) musicData["artist"] = "";
                    }
                    return resolve(true);
                }
            });
        });
    };

    getUserData()
        .then(() => { return getFolloweeIds() })
        .then(() => { return getPosts() })
        .then(() => { return getMusicData() })
        .then(() => {
            return res({ message: req.i18n.__('GetPostByMusicId')['200'], data: dataToSend, musicData: musicData }).code(200);
        })
        .catch((error) => {
            console.log("error", error)
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        })
};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetPostByMusicId']['200']), data: Joi.any(), musicData: Joi.any() },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//swagger response code


module.exports = { validator, handler, response };