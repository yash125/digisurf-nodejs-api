'use strict'
//require npm modules
const Joi = require("joi");

//require dependency
const local = require('../../../locales');
const currencyCollection = require("../../../models/currency");


//validator for this API
const validator = Joi.object({
    offset: Joi.string().default("0").description("offset, query parameter"),
    limit: Joi.string().default("20").description("limit, query parameter")
}).unknown();


/**
 * @method GET currency
 * @description This API returns all currency
 * 
 * @headers {string} authorization : token
 * @headers {string} lang by defualt en
 * 
 * @param {string} offset by default 0
 * @param {string} limit  by default 20
 * 
 * @author DipenAhir
 * @since 06-Sep-2019
 */

//start API hender
const hander = (req, res) => {

    //API variables
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;


    currencyCollection.SelectWithSort({}, { _id: -1 }, {}, offset, limit, (err, result) => {
        if (err) {
            //return 500 responce : if have any issues in Aggregate function
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result && result.length) {
            //return 200 responce : if data found in db
            return res({
                message: req.i18n.__('GetCurrency')['200'], data: result.map(e => {
                    return { "symbol": e.symbol, "currency": e.currency }
                })
            }).code(200);
        } else {
            //return 204 responce : if data not found in db
            return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);
        }
    });
};//end API hender

//start API response
const response = {
    status: {
        200: { message: Joi.any().default(local['GetCurrency']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//end API response

//Export all constance
module.exports = { validator, hander, response };