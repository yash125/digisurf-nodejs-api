'use strict'
// const {handler} = require('./edit');
// const server= require('../../../../index')
const chai = require('chai');
const { expect } = require('chai');
let chaiHttp = require('chai-http');
const request=require("request");
chai.use(chaiHttp);

let baseUrl = `${process.env.LOCALHOST}:${process.env.SERVER_PORT}`
/*
 * To edit comment
 * Test /edit
 */
describe('/comment/{postId} to add a comment to a post', () => {
    it('should say comment added sucessfully', (done) => {
        
        let data = {
            postId : '5c5ae843a1ab1d33229bcde8',
            comment : 'this is to test a comment without parent comment(from spec.test)'
        };

        request.post({
            headers: {'content-type': 'application/json','lang': 'en'},
            url: `${baseUrl}/comment/${data.postId}`,
            body: data.comment,
           json: true
          }, function(error, response, body){
            console.log("error------>",error);
            
            expect(response.statusCode).to.eql(200);
            expect(body.message).to.eql('success');
            done();
        });
        
    });

   
      
  
});

