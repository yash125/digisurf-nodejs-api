'use strict'
/** initialize  variables for this API  */
const Joi = require("joi");
const local = require('../../../locales');
const verifyResetPassword = require('../../../models/verifyResetPassword');

var moment = require("moment")

/** API Validators */
let validator = Joi.object({
    otp: Joi.number().required().description("otp eg:5678").error(new Error('otp is missing')),
    email: Joi.string().email().required().description("emailId, Eg. somone@example.com").error(new Error('email is missing or invalid')),
});

/**
 * @method  POST verifyOTPByEmail 
 * @description this api is use for verify OTP by email id
 * 
 * @property {email} email
 * @property {number} OTP
 * 
 * @returns 200 : Success
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * 
 * @author DipenAhir
 * @since 07-june-2019
 */

/** start API Handler */
let handler = (req, res) => {

    /** 
     * Start verifyResetPassword.SelectOne() function
     * Select one recored where email and otp are match
     * */
    verifyResetPassword.SelectOne({ email: req.payload.email.toLowerCase(), code: req.payload.otp }, (err, result) => {
        if (err) { /** if MongoDB return error */
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result) { /** if email and OTP matched */

            /** check OTP if not expired */
            // if (result.timeStamp && result.timeStamp + 300000 <= new Date().getTime()) {
            if (result.timestamp && moment(result.timestamp).add(5, 'minutes').valueOf() >= new Date().getTime()) {
                /** if OTP not expired send sucess with HTTP Status Code 200 */
                return res({ message: req.i18n.__('verifyOtp')['200'] }).code(200);
            } else {
                /** if OTP has expired send 422 as a HTTP Status Code */
                return res({ message: req.i18n.__('verifyOtp')['412'] }).code(422);

            }
        }
        else {
            /** if email and OTP has not match in database send 422 as HTTP Status Code */
            return res({ message: req.i18n.__('verifyOtp')['422'] }).code(422);
        }
    }); /** 
    * End verifyResetPassword.SelectOne() function
    * */

}/** End API Handler */

/** API Response  */
let APIResponse = {
    status: {
        200: { message: local['verifyOtp']['200'] },
        422: { message: local['verifyOtp']['422'] },
        400: { message: local['genericErrMsg']['400'] },
        500: { message: local['genericErrMsg']['500'] }
    }
}

/** Export API modules */
module.exports = { handler, validator, APIResponse }