'use strict'
let postAPI = require('./Post');
let headerValidator = require("../../middleware/validator");

module.exports = [
    {
        method: 'POST',
        path: '/verifyOTPByEmail',
        handler: postAPI.handler,
        config: {
            description: 'This API will is used to verify whether otp entered by user is valid or not ',
            tags: ['api', 'passwordReset'],
            auth: 'basic',
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: postAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: postAPI.response
        }
    }
];