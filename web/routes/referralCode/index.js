let headerValidator = require("../../middleware/validator")
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'Get',
        path: '/referralCode',
        handler: GetAPI.handler,
        config: {
            description: `This API use for check referralCode is exists in db or not`,
            tags: ['api', 'referralCode'],
            auth: 'basic',
            validate: {
                // headers: headerValidator.headerAuthValidator,
                query: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            // response: GetAPI.response
        }
    }
];