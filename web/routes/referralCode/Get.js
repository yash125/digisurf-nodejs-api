const Joi = require("joi");
const local = require('../../../locales');
const userCollection = require("../../../models/userList");

const validator = Joi.object({
    referralCode: Joi.string().required().description("referralCode")
})

const handler = (req, res) => {

    userCollection.SelectOne({ referralCode: req.query.referralCode }, (err, result) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result && result._id) {
            return res({ message: req.i18n.__('GetReferralCode')['200'] }).code(200);
        } else {
            return res({ message: req.i18n.__('GetReferralCode')['412'] }).code(412);
        }
    })

};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetReferralCode']['200']) },
        412: { message: Joi.any().default(local['GetReferralCode']['412']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { validator, handler, response };