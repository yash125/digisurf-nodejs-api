'use strict'
/** initialize  variables for this API  */
const Joi = require("joi");
var ObjectID = require('mongodb').ObjectID
const local = require('../../../../locales');
const userListCollection = require('../../../../models/userList');

/** query Validators */
const queryValidator = Joi.object({
    offset: Joi.string().description("pagination offset"),
    limit: Joi.string().description("pagination limit"),
    userId: Joi.string().description("userId")
}).unknown()

/**
 * @method  GET streamHistory 
 * @description This api is used to get all streamHistory of an user
 * 
 * 
 * @returns 200 : data fetched successfully
 * @returns 204 : no contant
 * @returns 500 : Internal Server Error
 * 
 * @author DipenAhir
 * @since 28-August-2019
 */

/** start API Handler */
const hander = async (req, res) => {

    const offset = parseInt(req.query.offset) || 0;
    const limit = parseInt(req.query.limit) || 20;

    var userId = (req.query && req.query.userId) ? ObjectID(req.query.userId) : ObjectID(req.auth.credentials._id);

    /** 
     * fetch data from database 
     **/
    userListCollection.SelectOne({ _id: userId }, (err, result) => {
        if (err) {
            //if have any error with mongodb
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result && result.streamHistory) {
            // if we get streamHistory with particualr user
            return res({ message: req.i18n.__('GetStreamHistory')['200'], data: result.streamHistory.reverse().slice(offset, limit) }).code(200);
        } else {
            // if we not get streamHistory for particualr user
            return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);
        }
    })


};/** End API Handler */

const response = {
    status: {
        200: { message: Joi.any().default(local['GetStreamHistory']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['getCollection']['204']) },
        500: { message: Joi.any().default(local['genericErrMsg']['500']) }
    }
}//swagger response code


module.exports = { hander, response, queryValidator };