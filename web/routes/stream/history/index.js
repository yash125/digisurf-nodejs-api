let headerValidator = require("../../../middleware/validator")
let GetAPI = require("./Get")


module.exports = [
    {
        method: 'Get',
        path: '/streamHistory',
        handler: GetAPI.hander,
        config: {
            description: `This API is used to save stream data in userlist collection`,
            tags: ['api', 'stream'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },

        }
    }
];