'use strict'
const userList = require("../../../models/userList");
var Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
/**
 * @method stream
 * @property {string} lang - language
 * @property {string} streamStatus
 * @property {string} streamName
 * @property {string} streamId
 * @property {string} userId
 * @returns 200 : Success
 * @returns 500 : Internal Server Error
 */


const payloadValidator = Joi.object({
    streamStatus: Joi.string().required().allow(["start", "stop"]).description('1-start, 2-stop'),
    streamName: Joi.string().required().description("enter name of the stream"),
    streamId: Joi.objectId().required().description("please enter a valid stream Id"),
    userId: Joi.objectId().required().description("please enter a valid stream Id"),
}).unknown()

/**
 * 
 *handler starts from here
 */

const handler = (req, res) => {

    let dataToUpdate = {
        streamStatus: req.payload.streamStatus,
        streamName: req.payload.streamName,
        streamId: req.payload.streamId,
        userId: req.payload.userId
    };

    /**
     * mongodb operation to update userList with the data from payload
     */
    userList.UpdateById(req.payload.userId, { stream: dataToUpdate }, (err) => {
        if (err) {
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        }
        return res({ message: req.i18n.__('stream')['200'] }).code(200);

    })


}



module.exports = { payloadValidator, handler };