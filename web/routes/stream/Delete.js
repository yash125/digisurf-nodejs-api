'use strict'
var Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);
const userList = require("../../../models/userList");
const { ObjectID } = require("mongodb"); 
/**
 * @method stream
 * @property {string} lang - language
 * @property {string} userId
 * @returns 200 : Success
 * @returns 500 : Internal Server Error
 */

const validator = Joi.object({
    streamId: Joi.objectId().required().description("please enter a valid stream Id")
}).unknown()

/** 
 *handler starts from here
 */
const handler = (req, res) => {

    /**
     * mongodb operation to update userList with the data from payload
     */
    userList.UpdateByIdWithPull({ _id: req.auth.credentials._id },
        { "streamHistory": { "streamId": ObjectID(req.query.streamId) } }, (err) => {
            if (err) {
                return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
            }
            return res({ message: req.i18n.__('stream')['200'] }).code(200);
        });
}

module.exports = { validator, handler };