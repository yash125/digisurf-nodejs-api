let headerValidator = require("../../middleware/validator")
let PostAPI = require('./post');
let Delete = require('./Delete');
let streamHistory = require('./history');

module.exports = [
    {
        method: 'POST',
        path: '/stream',
        handler: PostAPI.handler,
        config: {
            description: `This API is used to save stream data in userlist collection`,
            tags: ['api', 'stream'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PostAPI.payloadValidator,
                /*  failAction: (req, reply, source, error) => {
                     failAction: headerValidator.faildAction(req, reply, source, error)
                 } */
            }
        }
    },
    {
        method: 'DELETE',
        path: '/stream',
        handler: Delete.handler,
        config: {
            description: `This API is used to delete stream data in userlist collection`,
            tags: ['api', 'stream'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: Delete.validator,
                /*  failAction: (req, reply, source, error) => {
                     failAction: headerValidator.faildAction(req, reply, source, error)
                 } */
            }
        }
    }
].concat(streamHistory);