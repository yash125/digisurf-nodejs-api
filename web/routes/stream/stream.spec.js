'use strict'
const chai = require('chai');
const { expect } = require('chai');
const chaiHttp = require('chai-http');
const faker = require('faker');
const logger = require('winston');
const request = require("request");
chai.use(chaiHttp);
var ObjectID = require('mongodb').ObjectID;

require('dotenv').config()
let token = process.env.token


/*
 * Test the /signUp
 */
describe('/POST stream', () => {
    it('it should say stream started/stopped successfully status code 200', (done) => {

        
        let data = {
            streamStatus:'start',
            streamName: faker.fake("{{name.firstName}}"),
            streamId:ObjectID("5c7a7e3b5a2aad41a8ff477a"),
            userId: ObjectID("5c7a7e3b5a2aad41a8ff477a")
        };

        logger.info('dataaaaaaaaaaaaaaaaaa=>',data);
        request.post({
            headers: { 'content-type': 'application/json','authorization':token, 'lang': 'en' },
            url: 'http://localhost:5007/stream',
            body: data,
            json: true
        }, function (error, response, body) {
            logger.silly("body------",body);
            logger.error("error------>", error);
            expect(response.statusCode).to.eql(200);
            done();
        });

    });
    
    it('it should say data missing  status code 400', (done) => {

        
        let data = {
            streamStatus:'start',
            streamId: '5c7a7e3b5a2aad41a8ff477a',
            userId: '5c7a7e3b5a2aad41a8ff477a'
        };

        request.post({
            headers: { 'content-type': 'application/json','authorization':token, 'lang': 'en' },
            url: 'http://localhost:5007/stream',
            body: data,
            json: true
        }, function (error, response, body) {
            logger.silly("body------",body);
            logger.error("error------>", error);
            expect(response.statusCode).to.eql(400);
            done();
        });

    });
   


});

