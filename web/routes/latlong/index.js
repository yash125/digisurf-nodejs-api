let headerValidator = require("../../middleware/validator")
let PatchAPI = require('./Patch');

module.exports = [
    {
        method: 'Patch',
        path: '/latlong',
        handler: PatchAPI.hander,
        config: {
            description: `This API use for update lat , long for user`,
            tags: ['api', 'latlong'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PatchAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PatchAPI.response
        }
    }
];