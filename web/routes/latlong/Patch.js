'use strict'
const Joi = require("joi");
const local = require('../../../locales');
const userList = require("../../../models/userList");
var ObjectID = require('mongodb').ObjectID;

const validator = Joi.object({
    lat: Joi.number().required().description("lat").error(new Error("lat is required")),
    long: Joi.number().required().description("long").error(new Error("long is required"))
}).unknown();

/**
 * @method Post latlong
 * @description This API returns user's activity
 * 
 * @headers {string} authorization : token
 * @headers {string} lang by defualt en
 * 
 * @param {number} lat
 * @param {number} long
 * 
 * @author DipenAhir
 * @since 16-July-2020
 */



const hander = (req, res) => {

    //update in mongodb
    userList.Update({ _id: ObjectID(req.auth.credentials._id.toString()) }, {
        "location": {
            "latitude": req.payload.lat,
            "longitude": req.payload.long,
        }
    }, (e, r) => {
        if (e) {
            // return error
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else {
            // return success message
            return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(200);
        }
    })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']).example(local['genericErrMsg']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']).example(local['genericErrMsg']['400']) }
    }
}//swagger response code


module.exports = { validator, hander, response };