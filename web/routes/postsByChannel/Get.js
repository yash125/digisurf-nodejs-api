const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const userCollection = require("../../../models/userList");
const followCollection = require("../../../models/follow");
const channelCollection = require("../../../models/channel");

const businessProductTypeCollection = require("../../../models/businessProductType");
/**
 * @description API to update a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token 
 * @property {string} userId
 * @property {string} imageUrl1
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */

const validator = Joi.object({
    channelId: Joi.string().required().description('channelId'),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
}).required();

const handler = (req, res) => {
    let userId = ObjectID(req.auth.credentials._id);
    let channelId = ObjectID(req.query.channelId);

    const limit = parseInt(req.query.limit) || 20;
    const offset = parseInt(req.query.offset) || 0;
    var followStatus = {};

    var businessPostTypeLabel = {};

    function getBusinessPostTypeLabel() {
        return new Promise((resolve, reject) => {
            businessProductTypeCollection.Select({}, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {
                    result.forEach(element => {
                        businessPostTypeLabel[element._id.toString()] = element.text[req.headers.lang]
                    });
                }
                return resolve(true);
            });
        });
    }

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    function getFolloweeIds() {
        return new Promise((resolve, reject) => {
            let condition = { "follower": ObjectID(userId) };
            followCollection.Select(condition, (e, result) => {
                if (e) {
                    let responseobj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseobj);
                } else {
                    result.forEach(function (element) {
                        followStatus[element.followee] = element.type
                    }, this);
                    return resolve(true);
                }
            });
        });
    }
    const getPosts = () => {
        return new Promise((resolve, reject) => {
            let aggregationQuery = [
                { "$match": { "_id": channelId } },
                { "$lookup": { "from": 'posts', "localField": '_id', "foreignField": 'channelId', "as": 'postData' } },
                { "$unwind": { "path": "$postData", "preserveNullAndEmptyArrays": true } },
                { "$sort": { 'postData.timeStamp': -1 } },
                {
                    "$match": {
                        "$or": [
                            { "postData.postStatus": 1 },
                            { "postData.postStatus": { "$exists": 0 } }
                        ]
                    }
                },
                { "$lookup": { "from": 'customer', "localField": 'postData.userId', "foreignField": '_id', "as": 'userData' } },
                { "$unwind": { "path": "$userData", "preserveNullAndEmptyArrays": true } },
                { "$lookup": { "from": "channel", "localField": "channelId", "foreignField": "_id", "as": "channleData" } },
                { "$unwind": { "path": "$channleData", "preserveNullAndEmptyArrays": true } },
                { "$match": { "$or": [{ "channelId": null }, { "channleData.confirm.userId": userId }, { "channleData.userId": userId }] } },
                { "$lookup": { "from": "category", "localField": "categoryId", "foreignField": "_id", "as": "categoryData" } },
                { "$unwind": { "path": "$categoryData", "preserveNullAndEmptyArrays": true } },
                { "$lookup": { "from": "dublyAudio", "localField": "postData.musicId", "foreignField": "_id", "as": "musicData" } },
                { "$unwind": { "path": "$musicData", "preserveNullAndEmptyArrays": true } },
                {
                    "$group": {
                        "_id": '$_id',
                        "channelName": { "$first": '$channelName' },
                        "createdOn": { "$first": '$createdOn' },
                        "private": { "$first": '$private' },
                        "userId": { "$first": '$userId' },
                        "channelImageUrl": { "$first": '$channelImageUrl' },
                        "description": { "$first": '$description' },
                        "confirm": { "$first": "$confirm" },
                        "reject": { "$first": "$reject" },
                        "request": { "$first": "$request" },
                        // postData: { "$first": "$postData" },
                        "data": {
                            "$push": {
                                "postId": '$postData._id', "imageUrl1": '$postData.imageUrl1', "thumbnailUrl1": '$postData.thumbnailUrl1',
                                "mediaType1": '$postData.mediaType1', "createdOn": '$postData.createdOn', "timeStamp": '$postData.timeStamp',
                                "title": '$postData.title', "musicData": "$postData.musicData", "shareCount": "$postData.shareCount",
                                "userId": '$postData.userId', "phoneNumber": '$userData.mobile', "profilepic": '$userData.profilePic',
                                "userName": '$userData.userName', "likes": '$postData.likes', "business": "$postData.business",

                                "channelImageUrl": { '$ifNull': ["$channleData.channelImageUrl", ""] },
                                "categoryName": { '$ifNull': ["$categoryData.categoryName", ""] },
                                "categoryUrl": { '$ifNull': ["$categoryData.categoryActiveIconUrl", ""] },
                                "channelName": { '$ifNull': ["$channleData.channelName", ""] },
                                "categoryId": { '$ifNull': ['$categoryData.categoryId', ""] },
                                "channelId": { '$ifNull': ['$channleData.channelId', ""] },
                                "city": { "$ifNull": ['$postData.city', ""] },
                                "totalComments": { "$size": { '$ifNull': ['$postData.comments', []] } },
                                "distinctViews": { "$size": { '$ifNull': ['$postData.distinctViews', []] } },
                                "location": { '$ifNull': ['$postData.location', ""] },
                                "place": { '$ifNull': ['$postData.place', ""] },
                                "comments": { "$ifNull": ['$postData.comments', []] },
                                "hashTags": { '$ifNull': ['$postData.hashTags', []] },
                                "likesCount": { '$ifNull': ['$postData.likesCount', 0] },
                                "imageUrl1Width": { "$ifNull": ["$postData.imageUrl1Width", null] },
                                "imageUrl1Height": { "$ifNull": ["$postData.imageUrl1Height", null] },
                            }
                        }
                    }
                },
                { "$skip": offset }, { "$limit": limit }
            ];

            channelCollection.Aggregate(aggregationQuery, (e, d) => {
                if (e) {
                    let responseObj = { code: 500, message: req.i18n.__('genericErrMsg')['500'] };
                    return reject(responseObj);
                } else {
                    let channelName = "",
                        createdOn = 0,
                        channelUserId = d[0]["userId"],
                        _private = false,
                        channelImageUrl = "",
                        description = "",
                        data = [],
                        subscriber = 0,
                        subscribeStatus = 0;
                    /* 
                        subscribeStatus 
                        0 : none,
                        1 : confirm,
                        2 : request     
                        3 : reject
                    */
                    channelName = d[0]["channelName"];
                    createdOn = d[0]["createdOn"];
                    channelImageUrl = d[0]["channelImageUrl"];
                    description = d[0]["description"];
                    _private = d[0]["private"];

                    subscribeStatus = 0;
                    if (d[0]["reject"] && d[0]["reject"].map(id => id.userId.toString()).includes(userId.toString())) subscribeStatus = 3;
                    if (d[0]["request"] && d[0]["request"].map(id => id.userId.toString()).includes(userId.toString())) subscribeStatus = 2;
                    if (d[0]["confirm"] && d[0]["confirm"].map(id => id.userId.toString()).includes(userId.toString())) subscribeStatus = 1;

                    subscriber = (d[0]["confirm"]) ? d[0]["confirm"].length : 0
                    delete d[0]["confirm"];
                    delete d[0]["request"];
                    delete d[0]["reject"];

                    d[0].data.forEach(function (element) {

                        if (element["postId"]) data.push(element)
                        element["isBookMarked"] = (userData && userData.bookmarks && userData.bookmarks.map(e => e.toString()).includes(element.postId)) ? true : false;
                        if (element["business"] && element["business"]["businessPostType"]) element["business"]["businessPostTypeLabel"] = businessPostTypeLabel[element["business"]["businessPostType"]] || "";
                        if (element["comments"] && element["comments"].length) element["comments"].reverse();
                        element["followStatus"] = (followStatus[element["userId"]]) ? followStatus[element["userId"]]["status"] : 0;
                        element["liked"] = (element["likes"]) ? element["likes"].map(id => id.toString()).includes(userId.toString()) : false;
                        delete element["likes"];
                    }, this);

                    let responseObj = {
                        code: 200,
                        message: req.i18n.__('GetPostsByChannel')['200'],
                        data: {
                            channelName: channelName,
                            createdOn: createdOn,
                            private: _private,
                            channelImageUrl: channelImageUrl,
                            description: description || "",
                            isSubscribed: subscribeStatus,
                            data: data,
                            subscriber: subscriber,
                            userId: channelUserId,
                            _id: channelId
                        }
                    };
                    return resolve(responseObj);
                }
            });
        });
    };

    getBusinessPostTypeLabel()
        .then(() => { return getUserData() })
        .then(() => { return getFolloweeIds() })
        .then(() => { return getPosts() })
        .then((data) => {
            return res({ message: data.message, data: data.data }).code(data.code);
        })
        .catch((error) => { return res(error).code(500); })
};

const response = {
    // status: {
    //     200: {
    //         message: Joi.any().default(local['GetPostsByChannel']['200']), data: Joi.any()
    //     },
    //     400: { message: Joi.any().default(local['genericErrMsg']['400']) },

    // }
}//swagger response code


module.exports = { validator, handler, response };