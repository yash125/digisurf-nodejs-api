const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const dublyAudioCollection = require("../../../models/dublyAudio");

const userListCollection = require("../../../models/userList");


const queryValidator = Joi.object({
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit"),
    categoryId: Joi.string().required().description('categoryId'),
}).unknown();



const hander = (req, res) => {
    let categoryId = ObjectID(req.query.categoryId)
    const limit = parseInt(req.query.limit) || 20;
    const skip = parseInt(req.query.offset) || 0;

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    const getMyData = () => {
        return new Promise((resolve, reject) => {
            dublyAudioCollection.SelectWithProject({ "musicCategory": categoryId, "audioStatus": 1 }, skip, limit, { _id: -1 }, (e, d) => {
                if (e) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else if (d) {

                    d.forEach(element => {
                        element["isFavourite"] = (userData.favouriteMusic && userData.favouriteMusic.map(id => id.toString()).includes(element._id.toString())) ? 1 : 0
                    });
                    return resolve({ message: req.i18n.__('GetMusicCategory')['200'], data: d });
                } else {
                    return resolve({ code: 204, message: req.i18n.__('GetMusicCategory')['204'] });
                }
            })
        });
    };

    getUserData()
        .then(() => { return getMyData() })
        .then((data) => { return res(data).code(200); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetMusicCategory']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) }
    }
}//swagger response code


module.exports = { queryValidator, hander, response };