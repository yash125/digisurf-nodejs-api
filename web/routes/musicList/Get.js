const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;
const local = require('../../../locales');
const dublyAudioCollection = require("../../../models/dublyAudio");
const userListCollection = require("../../../models/userList");

const validator = Joi.object({
    offset: Joi.string().description("offset, query parameter"),
    limit: Joi.string().description("limit, query parameter")
}).unknown();


const hander = (req, res) => {
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;

    var userData = {};
    const getUserData = () => {
        return new Promise((resolve, reject) => {
            userListCollection.SelectOne({ _id: ObjectID(req.auth.credentials._id) }, (err, result) => {
                if (err) {
                    return reject({ message: req.i18n.__('genericErrMsg')['500'] });
                } else if (result) {
                    userData = result;
                    return resolve();
                } else {
                    return reject({ message: req.i18n.__('genericErrMsg')['204'] });
                }
            })
        });
    }

    const getMyData = () => {
        return new Promise((resolve, reject) => {

            dublyAudioCollection.SelectWithProject({ "audioStatus": 1 }, offset, limit, { sequenceId: -1 }, (e, d) => {
                if (e) {
                    let responseObj = { message: 'internal server error' };
                    return reject(responseObj);
                }
                d.forEach(element => {
                    element["isFavourite"] = (userData.favouriteMusic && userData.favouriteMusic.map(id => id.toString()).includes(element._id.toString())) ? 1 : 0
                });
                return resolve({ message: "success", data: d });
            })
        });
    };

    getUserData()
        .then(() => { return getMyData() })
        .then((data) => { return res(data).code(200); })
        .catch((error) => { return res({ message: error.message }).code(error.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetActivity']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },

    }
}//swagger response code


module.exports = { validator, hander, response };