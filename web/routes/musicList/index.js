let headerValidator = require("../../middleware/validator")
let GetAPI = require('./Get');
let GetByCategoryIdAPI = require('./GetByCategoryId');


module.exports = [
    {
        method: 'GET',
        path: '/musicList',
        handler: GetAPI.hander,
        config: {
            description: `This API use for signup or login from facebook with facebook tokenXXX`,
            tags: ['api', 'Music'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    },
    // {
    //     method: 'GET',
    //     path: '/musicList',
    //     handler: GetAPI.hander,
    //     config: {
    //         description: `This API use for signup or login from facebook with facebook tokenXXX`,
    //         tags: ['api', 'Music'],
    //         auth: "user",
    //         validate: {
    //             headers: headerValidator.headerAuthValidator,
    //             query: GetAPI.validator,
    //             failAction: (req, reply, source, error) => {
    //                 failAction: headerValidator.faildAction(req, reply, source, error)
    //             }
    //         },
    //         response: GetAPI.response
    //     }
    // },
    {
        method: 'GET',
        path: '/musicList/byCategory',
        handler: GetByCategoryIdAPI.hander,
        config: {
            description: `This API use for signup or login from facebook with facebook tokenXXX`,
            tags: ['api', 'Music'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                query: GetByCategoryIdAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetByCategoryIdAPI.response
        }
    }
].concat(require("./search"));