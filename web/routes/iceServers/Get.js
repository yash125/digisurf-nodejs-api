'use strict'
//require npm modules
const Joi = require("joi");

//require dependency
const local = require('../../../locales');
const iceServers = require("../../../models/iceServers");


/**
 * @method GET iceServers
 * @description This API returns all currency
 * 
 * @headers {string} authorization : token
 * @headers {string} lang by defualt en
 * 
 * @author DipenAhir
 * @since 27-Feb-2020
 */

//start API hender
const hander = (req, res) => {
    iceServers.Select({}, (err, result) => {
        if (err) {
            //return 500 responce : if have any issues in  function
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);
        } else if (result && result.length) {
            //return 200 responce : if data found in db
            return res({
                message: req.i18n.__('genericErrMsg')['200'], data: result
            }).code(200);
        } else {
            //return 204 responce : if data not found in db
            return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);
        }
    });
};//end API hender

//start API response
const response = {
    status: {
        200: { message: Joi.any().default(local['genericErrMsg']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) }
    }
}//end API response

//Export all constance
module.exports = { hander, response };