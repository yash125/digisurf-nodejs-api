let headerValidator = require("../../middleware/validator")
let GetAPI = require('./Get');

module.exports = [
    {
        method: 'GET',
        path: '/iceServers',
        handler: GetAPI.hander,
        config: {
            description: `This API use for get  iceServers `,
            tags: ['api', 'iceServers'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: GetAPI.response
        }
    }
];