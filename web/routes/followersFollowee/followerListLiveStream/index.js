let headerValidator = require("../../../middleware/validator")
let PostAPI = require('./Post');

module.exports = [
    {
        method: 'Post',
        path: '/followerList/forLiveStream',
        handler: PostAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'follow'],
            auth: false,
            validate: {
                // headers: headerValidator.headerAuthValidator,
                payload: PostAPI.queryValidator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PostAPI.response
        }
    }
];