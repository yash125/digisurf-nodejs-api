const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const mqtt = require("../../../../library/mqtt");
const followCollection = require("../../../../models/follow");
const userList = require("../../../../models/userList");

const queryValidator = Joi.object({
    userId: Joi.string().description('userId'),
    followIds: Joi.array().description('userId')
}).unknown();

const handler = (req, res) => {
    let _id = ObjectID(req.payload.userId);

    const getMyFollowersAndFollowee = () => {
        return new Promise((resolve, reject) => {
            let condition = {
                "follower": _id,
                followee: {
                    "$in": req.payload.followIds.map(e => ObjectID(e))
                },
                "type.status": { "$ne": 3 }
            };

            followCollection.Select(condition, (err, result) => {
                if (err) {
                    console.log("err ", err)
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else if (result.length) {
                    return resolve({ code: 200, message: local['GetFollowersFollowees']['200'], data: result });
                } else {
                    return reject({ code: 204, message: local['genericErrMsg']['204'] });
                }
            });
        });
    }
    const getPrivicyStatus = (dataToSend) => {
        return new Promise((resolve, reject) => {
            let condition = {
                _id: {
                    "$in": dataToSend.data.map(e => ObjectID(e.followee.toString()))
                }
            };

            userList.Select(condition, (err, result) => {
                if (err) {
                    console.log("err ", err)
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else if (result.length) {
                    dataToSend.data.forEach(element => {
                        element["isPrivate"] = (result && result.find(e => e._id.toString() == element.followee.toString())
                            && result.find(e => e._id.toString() == element.followee.toString()).private)
                    });
                    return resolve(dataToSend);
                } else {
                    return resolve(dataToSend);
                }
            });
        });
    }

    getMyFollowersAndFollowee()
        .then(getPrivicyStatus)
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => {
            console.log("error ", error)
            return res({ message: error.message }).code(error.code);
        })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetFollowersFollowees']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { queryValidator, handler, response };