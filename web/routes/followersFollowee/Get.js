const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const mqtt = require("../../../library/mqtt");
const followCollection = require("../../../models/follow");

const queryValidator = Joi.object({
    skip: Joi.string().description('skip'),
    limit: Joi.string().description('limit'),
}).required();

const handler = (req, res) => {
    let _id = ObjectID(req.auth.credentials._id);
    // const offset = parseInt(req.query.skip) || 0;
    // const limit = parseInt(req.query.limit) || 20;

    const getMyFollowersAndFollowee = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                {
                    "$match": {
                        "$or": [
                            { "followee": _id, "type.status": 1 },
                            { "follower": _id, "type.status": 1 }
                        ]
                    }
                },
                {
                    "$project": {
                        "targetId": {
                            "$cond": {
                                "if": { "$eq": ["$followee", _id] },
                                "then": "$follower", "else": "$followee"
                            }
                        }
                    }
                },
                {
                    "$group": {
                        "_id": "$targetId"
                    }
                },
                { "$match": { "_id": { "$ne": _id } } },
                { "$lookup": { "from": "customer", "localField": "_id", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                { "$match": { "userData.userStatus": 1 } },
                {
                    "$project": {
                        "_id": "$userData._id",
                        "number": "$userData.mobile", "countryCode": "$userData.countryCode",
                        "userStatus": "$userData.userStatus", "profilePic": "$userData.profilePic",
                        "firstName": "$userData.firstName", "status": "$userData.status",
                        "lastName": "$userData.lastName", "userName": "$userData.userName",
                    }
                }
            ];

            followCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    console.log("err ", err)
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else if (result.length) {
                    result = result.map(e => {
                        return {
                            ...e,
                            number: e.countryCode + e.number
                        }
                    })


                    mqtt.publish("ContactSync/" + _id.toString(), JSON.stringify({ contacts: result }), { qos: 1 }, () => {

                    });
                    return resolve({ code: 200, message: local['GetFollowersFollowees']['200'], data: result });
                } else {
                    mqtt.publish("ContactSync/" + _id.toString(), JSON.stringify({ contacts: [] }), { qos: 1 }, () => {

                    });
                    return reject({ code: 204, message: local['genericErrMsg']['204'] });
                }
            });
        });
    }

    getMyFollowersAndFollowee()
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => {
            console.log("error ", error)
            return res({ message: error.message }).code(error.code);
        })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetFollowersFollowees']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { queryValidator, handler, response };