const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../../locales');
const mqtt = require("../../../../library/mqtt");
const followCollection = require("../../../../models/follow");
const userCollection = require("../../../../models/userList");

const queryValidator = Joi.object({
    userId: Joi.string().description('userId')
}).unknown();

const handler = (req, res) => {
    let _id = ObjectID(req.query.userId);
    let followData = [], isPrivateUser = false;


    const getUserInfo = () => {
        return new Promise((resolve, reject) => {


            userCollection.SelectOne({ _id: _id }, (err, result) => {
                if (err) {
                    console.log("err ", err)
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else if (result && result._id) {
                    isPrivateUser = (result && result.private) ? true : false;
                    return resolve({ code: 200, message: local['GetFollowersFollowees']['200'] });
                } else {
                    return reject({ code: 204, message: local['genericErrMsg']['204'] });
                }
            });
        });
    }
    const getFollowers = () => {
        return new Promise((resolve, reject) => {

            let condition = [
                {
                    "$match": {
                        "$or": [
                            { "followee": _id, "type.status": 1 },
                            // { "follower": _id, "type.status": 1 }
                        ]
                    }
                },
                {
                    "$project": {
                        "type": 1,
                        "targetId": {
                            "$cond": {
                                "if": { "$eq": ["$followee", _id] },
                                "then": "$follower", "else": "$followee"
                            }
                        }
                    }
                },
                {
                    "$group": {
                        "_id": "$targetId"
                    }
                },
                { "$lookup": { "from": "customer", "localField": "_id", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                {
                    "$project": {
                        "_id": "$userData._id",
                        "number": "$userData.mobile", "countryCode": "$userData.countryCode",
                        "userStatus": "$userData.userStatus", "profilePic": "$userData.profilePic",
                        "firstName": "$userData.firstName", "status": "$userData.status",
                        "lastName": "$userData.lastName", "userName": "$userData.userName",
                    }
                }
            ];

            followCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    console.log("err ", err)
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else if (result.length) {
                    result.forEach(element => {
                        followData.push({ ...element, following: true, followStatus: 1, isPrivateUser });
                    });
                    return resolve({ code: 200, message: local['GetFollowersFollowees']['200'] });
                } else {
                    return resolve({ code: 200, message: local['GetFollowersFollowees']['200'] });
                }
            });
        });
    }
    const getFollowees = () => {
        return new Promise((resolve, reject) => {
            let condition = [
                {
                    "$match": {
                        "$or": [
                            // { "followee": _id, "type.status": 1 },
                            // { "follower": _id, "type.status": 1 }
                            { "follower": _id }
                        ]
                    }
                },
                {
                    "$project": {
                        "type": 1,
                        "targetId": {
                            "$cond": {
                                "if": { "$eq": ["$followee", _id] },
                                "then": "$follower", "else": "$followee"
                            }
                        }
                    }
                },
                {
                    "$group": {
                        "_id": "$targetId"
                    }
                },
                { "$lookup": { "from": "customer", "localField": "_id", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                {
                    "$project": {
                        "_id": "$userData._id",
                        "number": "$userData.mobile", "countryCode": "$userData.countryCode",
                        "userStatus": "$userData.userStatus", "profilePic": "$userData.profilePic",
                        "firstName": "$userData.firstName", "status": "$userData.status",
                        "lastName": "$userData.lastName", "userName": "$userData.userName",
                    }
                }
            ];

            followCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    console.log("err ", err)
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else if (result.length) {
                    result.forEach(element => {
                        if (!followData.find(e => e._id == element._id.toString())) {
                            followData.push({
                                ...element,
                                following: (element.type && element.type.status && element.type.status == 1) ? true : false,
                                followStatus: (element.type && element.type.status && element.type.status) ? element.type.status : 0,
                                isPrivateUser
                            });
                        }
                    });
                    return resolve({ code: 200, message: local['GetFollowersFollowees']['200'] });
                } else {
                    return resolve({ code: 200, message: local['GetFollowersFollowees']['200'] });
                }
            });
        });
    }


    const getMyFollowersAndFollowee = () => {
        return new Promise((resolve, reject) => {

            let condition = [
                {
                    "$match": {
                        "$or": [
                            { "followee": _id, "type.status": 1 },
                            { "follower": _id, "type.status": 1 }
                        ]
                    }
                },
                {
                    "$project": {
                        "type": 1,
                        "targetId": {
                            "$cond": {
                                "if": { "$eq": ["$followee", _id] },
                                "then": "$follower", "else": "$followee"
                            }
                        }
                    }
                },
                {
                    "$group": {
                        "_id": "$targetId"
                    }
                },
                { "$lookup": { "from": "customer", "localField": "_id", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                {
                    "$project": {
                        "_id": "$userData._id",
                        "number": "$userData.mobile", "countryCode": "$userData.countryCode",
                        "userStatus": "$userData.userStatus", "profilePic": "$userData.profilePic",
                        "firstName": "$userData.firstName", "status": "$userData.status",
                        "lastName": "$userData.lastName", "userName": "$userData.userName",
                    }
                }
            ];

            followCollection.Aggregate(condition, (err, result) => {
                if (err) {
                    console.log("err ", err)
                    return reject({ code: 500, message: local['genericErrMsg']['500'] });
                } else if (result.length) {
                    return resolve({ code: 200, message: local['GetFollowersFollowees']['200'], data: result });
                } else {
                    return reject({ code: 204, message: local['genericErrMsg']['204'] });
                }
            });
        });
    }
    getUserInfo()
       .then(getFollowers)
    //    .then(getFollowees)
        .then((data) => { return res({ message: data.message, data: followData }).code(200); })
        .catch((error) => {
            console.log("error ", error)
            return res({ message: error.message }).code(error.code);
        })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['GetFollowersFollowees']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { queryValidator, handler, response };