'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const starUserRequest = require("../../../models/starUserRequest");
const starUserCategoriesCollection = require("../../../models/starUserCategories");
const userList = require("../../../models/userList");

/**
 * @description API to un-like a post
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} userId
 * @property {string} postId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const payloadValidator = Joi.object({
    categorieId: Joi.string().min(24).max(24).required().description("mongoId").error(new Error('categorieId is missing or enter invalid value.')),
    starUserEmail: Joi.string().email().required().description("starUserEmail").error(new Error('starUserEmail is missing.')),
    starUserPhoneNumber: Joi.string().required().description("starUserPhoneNumber").error(new Error('starUserPhoneNumber is missing.')),
    starUserIdProof: Joi.string().required().description("Id proof as image link").error(new Error('starUserIdProof is missing.')),
    starUserKnownBy: Joi.string().required().description("Nik Name").error(new Error('starUserKnownBy is missing.')),
    description: Joi.string().allow("").description("description").error(new Error('description is missing.'))
}).unknown();


const handler = (req, res) => {
    const userId = ObjectID(req.auth.credentials._id);
    var categoryName = ""

    const HTTP500ERROR = { message: req.i18n.__('genericErrMsg')['500'], code: 500 }
    const HTTP200ERROR = { message: req.i18n.__('PostStarRequest')['200'], code: 200 }
    const HTTP412ERROR = { message: req.i18n.__('PostStarRequest')['412'], code: 412 }
    const HTTP405ERROR = { message: req.i18n.__('PostStarRequest')['405'], code: 405 }
    const HTTP409ERROR = { message: req.i18n.__('PostStarRequest')['409'], code: 409 }


    const isCategorieIdExistsInStarCategorieCollection = () => {
        return new Promise((resolve, reject) => {

            starUserCategoriesCollection.SelectOne({ _id: ObjectID(req.payload.categorieId) }, (err, result) => {
                if (err) {
                    return reject(HTTP500ERROR);
                } else if (result && result._id) {
                    categoryName = result.categorie;
                    return resolve(HTTP200ERROR);
                } else {
                    return reject(HTTP412ERROR);
                }
            });
        });
    };
    const isRequestAlreadyInStarUserRequestCollection = () => {
        return new Promise((resolve, reject) => {

            starUserRequest.SelectOne({ userId: userId, categorieId: ObjectID(req.payload.categorieId), starUserProfileStatusCode: { "$in": [1, 4] } }, (err, result) => {
                if (err) {
                    return reject(HTTP500ERROR);
                } else if (result && result._id) {
                    if (result && result.deletedOn) {
                        return resolve(HTTP412ERROR);
                    } else {
                        return (result.starUserProfileStatusCode == 1) ? reject(HTTP409ERROR) : reject(HTTP405ERROR);
                    }
                } else {
                    return resolve(HTTP412ERROR);
                }
            });
        });
    };
    const saveInUserList = () => {
        return new Promise((resolve, reject) => {
            // starUserProfileStatusCode	1,         2,        3,      4
            // starUserProfileStatusText	pending,notApproved,suspend,approved
            let dataToSave = {
                userId: userId,
                categoryName: categoryName,
                categorieId: ObjectID(req.payload.categorieId),
                starUserEmail: req.payload.starUserEmail,
                starUserPhoneNumber: req.payload.starUserPhoneNumber,
                starUserIdProof: req.payload.starUserIdProof,
                starUserKnownBy: req.payload.starUserKnownBy,
                description: req.payload.description,
                timestamp: new Date().getTime(),
                starUserProfileStatusText: "pending",
                starUserProfileStatusCode: 1,
                approvalDate: new Date().getTime(),
            };

            userList.Update({ _id: userId }, { starRequest: dataToSave }, (err) => {
                if (err) {
                    return reject(HTTP500ERROR);
                } else {
                    return resolve({ code: 200, message: req.i18n.__('PostStarRequest')['200'] });
                }
            });
        });
    };
    const saveInStarUserRequest = () => {
        return new Promise((resolve, reject) => {
            // starUserProfileStatusCode	1,         2,        3,      4
            // starUserProfileStatusText	pending,notApproved,suspend,approved
            let dataToSave = {
                userId: userId,
                categoryName: categoryName,
                categorieId: ObjectID(req.payload.categorieId),
                starUserEmail: req.payload.starUserEmail,
                starUserPhoneNumber: req.payload.starUserPhoneNumber,
                starUserIdProof: req.payload.starUserIdProof,
                starUserKnownBy: req.payload.starUserKnownBy,
                description: req.payload.description,
                starUserProfileStatusText: "approved",
                starUserProfileStatusCode: 4,
                timestamp: new Date().getTime()
            }
            starUserRequest.Insert(dataToSave, (err) => {
                if (err) {
                    return reject(HTTP500ERROR);
                } else {
                    return resolve({ code: 200, message: req.i18n.__('PostStarRequest')['200'] });
                }
            });
        });
    };

    isCategorieIdExistsInStarCategorieCollection()
        .then(() => { return isRequestAlreadyInStarUserRequestCollection() })
        .then(() => { return saveInUserList() })
        .then(() => { return saveInStarUserRequest() })
        .then((data) => { return res({ message: data.message }).code(data.code); })
        .catch((data) => { return res({ message: data.message }).code(data.code); })
};

const response = {
    status: {
        200: { message: Joi.any().default(local['PostStarRequest']['200']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { payloadValidator, handler, response };
