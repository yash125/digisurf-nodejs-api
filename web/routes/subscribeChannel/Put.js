'use strict'
const Joi = require("joi");
const logger = require('winston');
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const fcm = require("../../../library/fcm");

const userCollection = require("../../../models/userList");
const channelCollection = require("../../../models/channel");


const handler = (req, res) => {

    let _id = req.auth.credentials._id;

    let channelId = req.payload.channelId;
    let channelUserId = "--";
    let channelName = "--";

    function isPrivateChannel() {
        return new Promise((resolve, reject) => {
            channelCollection.SelectOne({ _id: ObjectID(channelId) }, (err, result) => {
                if (result && result._id) {
                    channelUserId = result.userId;
                    channelName = result.channelName;
                    return resolve(result.private)
                } else {
                    return reject("--");
                }
            })
        });
    }

    function setSubscribeOrRequestTheChannel(isPrivateChannel) {
        return new Promise((resolve, reject) => {
            if (isPrivateChannel) {
                sendNotification({ msg: " request for subscribe " + channelName + " channel", "type": "channelRequest" });
                channelCollection.UpdateByIdWithAddToSet({ _id: ObjectID(channelId), "request.userId": { "$ne": ObjectID(_id) } },
                    { "request": { "userId": ObjectID(_id), "timestamp": new Date().getTime() } },
                    (err) => {
                        if (err) {
                            return reject(err)
                        } else {
                            return resolve("--")
                        }
                    });

            } else {
                sendNotification({ msg: " has subscribes to your channel " + channelName, "type": "channelSubscribe" });
                userCollection.UpdateByIdWithAddToSet({ _id: _id }, { "subscribeChannels": ObjectID(channelId) }, (e) => { if (e) logger.error("OKAOKSOAKSW : ", e) })
                channelCollection.UpdateByIdWithAddToSet({ _id: ObjectID(channelId), "confirm.userId": { "$ne": ObjectID(_id) } },
                    { "confirm": { "userId": ObjectID(_id), "timestamp": new Date().getTime() } },
                    (err) => {
                        if (err) {
                            return reject(err)
                        } else {
                            return resolve("--")
                        }


                    });

            }
        });
    }

    isPrivateChannel()
        .then((isPrivateChannel) => { return setSubscribeOrRequestTheChannel(isPrivateChannel); })
        .then(() => { return res({ message: req.i18n.__('PutSubscribeChannel')['200'] }).code(200); })
        .catch(() => { return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204); });


    function sendNotification(data) {
        userCollection.SelectOne({ _id: ObjectID(channelUserId) }, (err, result) => {
           var deviceType = (result && result.deviceInfo && result.deviceInfo.deviceType) ? result.deviceInfo.deviceType : "2";

            userCollection.SelectOne({ _id: ObjectID(_id) }, (err, result) => {
                if (result && result.userName) {
                    let request = {
                        fcmTopic: channelUserId,
                        // action: 1,
                        pushType: 2,
                        title: process.env.APP_NAME,
                        msg: result.userName + data.msg,
                        data: { "type": data.type, "channelId": channelId },
                        deviceType: deviceType
                    }
                    fcm.notifyFcmTpic(request, (err, result) => { if (err) console.log(err) });
                }
            });
        });
    }
};


const validator = Joi.object({
    channelId: Joi.string().description("channelId")
}).required();

const response = {
    status: {
        200: { message: Joi.any().default(local['PutSubscribeChannel']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code

module.exports = { validator, handler, response };