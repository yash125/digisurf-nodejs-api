let headerValidator = require("../../middleware/validator")
let PutAPI = require('./Put');

module.exports = [
    {
        method: 'PUT',
        path: '/subscribeChannel',
        handler: PutAPI.handler,
        config: {
            description: `This API use for`,
            tags: ['api', 'channel'],
            auth: "user",
            validate: {
                headers: headerValidator.headerAuthValidator,
                payload: PutAPI.validator,
                failAction: (req, reply, source, error) => {
                    headerValidator.faildAction(req, reply, source, error)
                }
            },
            response: PutAPI.response
        }
    }
];