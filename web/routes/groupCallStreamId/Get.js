const Joi = require("joi");
const ObjectID = require("mongodb").ObjectID;
const local = require('../../../locales');
const userListCollection = require("../../../models/userList");


/**
 * @method GET groupCallStreamId
 * @description This API returns user's groupCallStreamId
 * 
 * @headers {string} authorization : token
 * @headers {string} lang by defualt en
 * @author DipenAhir
 * @since 11-July-2020
 */
const handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);//get userId

    userListCollection.SelectOne({ _id: userId }, (err, result) => {
        if (err) {
            //send error message
            return res({ message: req.i18n.__('genericErrMsg')['500'] }).code(500);

        } else if (result && result.groupCallStreamId) {//check groupCallStreamId is exists

            //send success response with data object
            return res({ message: req.i18n.__('genericErrMsg')['200'], data: { groupCallStreamId: result.groupCallStreamId } }).code(200);

        } else {// if groupCallStreamId is not exists

            //send 204 as Http response code
            return res({ message: req.i18n.__('genericErrMsg')['204'] }).code(204);
        }
    });
};

const response = {
    status: {
        200: {
            message: Joi.any().default(local['GetHashTags']['200']), data: Joi.any().example({
                groupCallStreamId: "<groupCallStreamId>"
            })
        },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
}//swagger response code


module.exports = { handler, response };

