'use strict'
const Joi = require("joi");
const Promise = require('promise');
const ObjectID = require('mongodb').ObjectID;

const local = require('../../../locales');
const followCollection = require("../../../models/follow")
const userListCollection = require("../../../models/userList")


/**
 * @description API to subscribe to a channel
 * @property {string} authorization - authorization
 * @property {string} lang - language
 * @property {string} token
 * @property {mongodb object} channelId
 * @returns 200 : Success
 * @returns 204 : No Data
 * @returns 422 : Parameters missing
 * @returns 500 : Internal Server Error
 * @returns 409 : Duplicate Entry
 */


const validator = Joi.object({
    userName: Joi.string().allow("").default("").description("username to be searched"),
    offset: Joi.number().description("offset"),
    limit: Joi.number().description("limit")
}).required();

const handler = (req, res) => {

    let userId = ObjectID(req.auth.credentials._id);
    let username = (req.query.userName) ? req.query.userName.trim().toLowerCase() : "";
    let offset = parseInt(req.query.offset) || 0;
    let limit = parseInt(req.query.limit) || 20;
    let followIds = [];

    const getMyFollowee = () => {
        return new Promise((resolve, reject) => {
            followCollection.Select({ "$or": [{ followee: userId, end: false }, { follower: userId, end: false }] }, (err, result) => {
                if (err) {
                    return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                } else {

                    result.forEach(element => {
                        if (element.followee == userId) {
                            followIds.push(element.follower.toString());
                        } else {
                            followIds.push(element.followee.toString());
                        }
                    });

                    return resolve("----");
                }
            })
        });
    };
    const getUserList = () => {
        return new Promise((resolve, reject) => {

            let condition = [
                { "$group": { "_id": "$followee", "total": { "$sum": 1 } } },
                { "$sort": { "total": -1, "_id": -1 } },
                { "$lookup": { "from": "customer", "localField": "_id", "foreignField": "_id", "as": "userData" } },
                { "$unwind": "$userData" },
                { "$match": { "userData.userStatus": 1 } },
                { "$limit": 20 },
                {
                    "$project": {
                        "_id": 1, "registeredOn": "$userData.registeredOn", "private": "$userData.private",
                        "businessProfile": "$userData.businessProfile",
                        "userName": "$userData.userName", "firstName": "$userData.firstName", "lastName": "$userData.lastName", "profilePic": "$userData.profilePic"
                    }
                },
            ];

            if (username !== "") {
                condition = { "$and": [{ "userName": { $exists: true } }, { "userName": { $regex: username, "$options": "i" } }],"userStatus":1 };

                let project = {
                    _id: 1, firstName: 1, lastName: 1, registeredOn: 1, private: 1, businessProfile: 1, userName: 1,
                    profilePic: 1
                };

                userListCollection.SelectWithSort(condition, { userName: 1 }, project, offset, limit, (e, d) => {
                    if (e) {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (d.length === 0) {
                        return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                    } else {
                        return resolve({ code: 200, message: req.i18n.__('GetUserTag')['200'], data: d });
                    }
                });
            } else {
                followCollection.Aggregate(condition, (e, d) => {
                    if (e) {
                        return reject({ code: 500, message: req.i18n.__('genericErrMsg')['500'] });
                    } else if (d.length === 0) {
                        return reject({ code: 204, message: req.i18n.__('genericErrMsg')['204'] });
                    } else {
                        return resolve({ code: 200, message: req.i18n.__('GetUserTag')['200'], data: d });
                    }
                });
            }
        })
    };

    getMyFollowee()
        .then(getUserList)
        .then((data) => { return res({ message: data.message, data: data.data }).code(data.code); })
        .catch((error) => {
            return res({ message: error.message }).code(error.code);
        });
};


const response = {
    status: {
        200: { message: Joi.any().default(local['GetUserTag']['200']), data: Joi.any() },
        204: { message: Joi.any().default(local['genericErrMsg']['204']) },
        400: { message: Joi.any().default(local['genericErrMsg']['400']) },
    }
};//swagger response code

module.exports = { validator, response, handler };