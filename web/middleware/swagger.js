
const inert = require('inert');
const vision = require('vision');

const swagger = {
    register:require('hapi-swagger'),
    'options':{
        grouping:'tags',
        payloadType:'form',
        schemes:["http"]
    }
}

module.exports = { inert, vision, swagger };