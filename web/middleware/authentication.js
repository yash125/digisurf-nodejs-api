'use strict';

// const HAPI_AUTH_JWT = require('hapi-auth-jwt2');
const logger = require('winston');
const moment = require('moment');
const request = require('request');
const jwt = require('./hapi-jwt-jwe');
const config = require('../../config');

var auth = module.exports = {};

/**
 * Method to generate a Access and refresh token
 * @param {Object} data - the claims to be included in the token
 * @param {ObjectID} data.userId
 * @param {String} data.userType
 * @param {String} data.deviceId
 * @param {String} data.deviceType
 * @param {String} data.deviceMake
 * @param {String} data.deviceModel
 */
auth.generateAccessAndRefresh = (data) => new Promise((resolve, reject) => {
    var options = {
        method: 'POST',
        url: config.auth.server + "/user/login",
        headers: {
            'lan': 'en',
            'content-type': 'application/json'
        },
        body: {
            "appName": config.appName,
            "userId": data.userId,
            "userType": data.userType,
            "multiLogin": config.auth.multiLogin,
            "immediateRevoke": config.auth.immediateRevoke,
            "deviceId": data.deviceId,
            "deviceType": data.deviceType,
            "deviceMake": data.deviceMake,
            "deviceModel": data.deviceModel,
            "accessTokenExpiry": config.auth.accessExpiry,
            "refreshTokenExpiry": config.auth.refreshExpiry
        },
        json: true
    };

    request(options, function (error, response, body) {
        if (error) {
            logger.error("Error while generating access and refresh token", error);
            reject(error);
        } else if (response.statusCode == 200) {
            resolve(body.data);
        } else {
            body.code = response.statusCode;
            return reject(body);
        }
    });
});

/**
 * Method to BlackList a Refresh token for certain time
 * @param {Object} data
 * @param {ObjectID} data.userId
 * @param {String} data.userType
 * @param {String} data.refreshToken
 * @param {Number} data.time
 */
auth.blackListRefreshToken = (data) => new Promise((resolve, reject) => {
    var options = {
        method: 'DELETE',
        url: config.auth.server + "/user/refreshToken",
        headers: {
            'lan': 'en',
            'content-type': 'application/json'
        },
        body: {
            "appName": config.appName,
            "userId": data.userId,
            "userType": data.userType,
            "refreshToken": data.refreshToken,
            "time": data.time
        },
        json: true
    };
    request(options, function (error, response, body) {
        if (error) {
            logger.error("Error while generating access and refresh token", error);
            reject(error);
        } else if (response.statusCode == 200) {
            return resolve(body);
        } else {
            body.code = response.statusCode;
            return reject(body);
        }
    });
});

/**
 * Method to BlackList a Refresh token for certain time
 * @param {Object} data
 * @param {ObjectID} data.userId
 * @param {String} data.userType
 * @param {String} data.refreshToken
 */
auth.activateRefreshToken = (data) => new Promise((resolve, reject) => {
    var options = {
        method: 'PATCH',
        url: config.auth.server + "/user/refreshToken",
        headers: {
            'lan': 'en',
            'content-type': 'application/json'
        },
        body: {
            "appName": config.appName,
            "userId": data.userId,
            "userType": data.userType,
            "refreshToken": data.refreshToken
        },
        json: true
    };
    request(options, function (error, response, body) {
        if (error) {
            logger.error("Error while generating access and refresh token", error);
            reject(error);
        } else if (response.statusCode == 200) {
            return resolve(body);
        } else {
            body.code = response.statusCode;
            return reject(body);
        }
    });
});

/**
 * Method to remove all Refresh token for user
 * @param {Object} data
 * @param {ObjectID} data.userId
 * @param {String} data.userType
 */
auth.removeUser = (data) => new Promise((resolve, reject) => {
    var options = {
        method: 'DELETE',
        url: config.auth.server + "/user",
        headers: {
            'lan': 'en',
            'content-type': 'application/json'
        },
        body: {
            "appName": config.appName,
            "userId": data.userId,
            "userType": data.userType
        },
        json: true
    };
    request(options, function (error, response, body) {
        if (error) {
            logger.error("Error while generating access and refresh token", error);
            reject(error);
        } else if (response.statusCode == 200) {
            return resolve(body);
        } else {
            body.code = response.statusCode;
            return reject(body);
        }
    });
});


/**
 * Method to validate the Access Token
 * @param {*} decoded - decoded token
 * @param {*} req - request object
 * @param {*} cb - callback
 */
const validateAccessToken = (decoded, req, cb) => {
    const unAuth = { message: req.i18n.__('genericErrMsg')['401'], code: 401 };
    // const invalidCode = { message: req.i18n.__('genericErrMsg')['417 '], code: 417 };
    const expired = { message: req.i18n.__('genericErrMsg')['406'], code: 406 };

    if (decoded.exp < moment().unix()) {
        return cb(expired, false);
    } else {
        if (decoded.multiLogin == false || decoded.immediateRevoke == true) {
            var options = {
                method: 'GET',
                url: config.auth.server + "/app/validate",
                headers: {
                    'authorization': req.headers.authorization,
                    'lan': 'en',
                    'content-type': 'application/json'
                },
                json: true
            };
            request(options, function (error, response, body) {
                if (error) {
                    logger.error("Error while generating access and refresh token", error);
                    return cb(unAuth, false);
                } else if (response.statusCode == 200) {
                    
                    req["decoded"] = decoded;
                    return cb(null, true, decoded);
                } else {
                    body.code = response.statusCode;
                    return cb(body, false);
                }
            });
        } else {
            req["decoded"] = decoded;
            return cb(null, true, decoded);
        }
    }
};

auth.accessToken = {
    key: jwt.publicCert,
    validateFunc: validateAccessToken,
    verifyOptions: { algorithms: ['RS256'], ignoreExpiration: false }
}

auth.authJWT = jwt;

module.exports = auth;