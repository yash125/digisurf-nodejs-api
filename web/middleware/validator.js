
const joi = require('joi');
const lan = require('./localization');
const logger = require('winston');

const headerAuth = joi.object({
    'authorization': joi.string().required().description("authorization token,Eg. Key"),
    lan: joi.string().default(lan.defaultLan).description("Language(English-0),Eg. 0")
}).options({ allowUnknown: true });

const headerAuthValidator = joi.object({
    'authorization': joi.string().required().description("authorization token,Eg. Key"),
    lang: joi.string().default(lan.defaultLan).description("Language(English-0),Eg. 0")
}).options({ allowUnknown: true });

const headerAuthRefresh = joi.object({
    'authorization': joi.string().required().description("authorization token,Eg. Key"),
    'refreshtoken': joi.string().required().description("refresh token,Eg. Key"),
    lang: joi.string().required().default(lan.defaultLan).description("Language(English-0),Eg. 0")
}).options({ allowUnknown: true });

const headerLan = joi.object({
    'lan': joi.string().required().default(lan.defaultLan).description("Language(English-0),Eg. 0")
}).options({ allowUnknown: true });

const faildAction = function faildAction(req, reply, source, error) {
    if (error) logger.error("ASASASS : ", error);
    // if (error) logger.error("ASASASS : ", JSON.stringify(error));

    return reply({ message: error.output.payload.message }).code(error.output.statusCode);
}

module.exports = { headerLan, headerAuth, headerAuthRefresh, headerAuthValidator, faildAction };