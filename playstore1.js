// Imports the Google Cloud client library
const {PubSub} = require('@google-cloud/pubsub');

async function quickstart(
  projectId = 'dubly2', // Your Google Cloud Platform project ID
  subscriptionName = 'dublypurchase' // Name of our subscription
) {
  // Instantiates a client
  const pubsub = new PubSub({projectId});

  // Get our created subscription
  const subscription = pubsub.subscription(subscriptionName);
  console.log(`Subscription ${subscription.name} found.`);

  // Receive callbacks for new messages on the subscription
  subscription.on('message', message => {
    console.log('Received message:', message.data.toString());
   // process.exit(0);
  });

  // Receive callbacks for errors on the subscription
  subscription.on('error', error => {
    console.error('Received error:', error);
    process.exit(1);
  });
}

console.log('Running');
quickstart();